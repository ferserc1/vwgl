Pod::Spec.new do |s|
	s.name     = 'vwgl'
	s.version  = '0.0.1'
	s.license  = 'MIT'
	s.summary  = 'Base modular framework for iOS & Mac apps'
	s.homepage = 'https://bitbucket.org/ferserc1/vwgl'
	s.authors  = { 'José Manuel Sánchez' => 'buscarini@gmail.com' }
	s.source   = { :git => 'https://bitbucket.org/ferserc1/vwgl.git', :tag => "0.0.1", :submodules => true }

	s.ios.deployment_target = '7.0'
	s.osx.deployment_target = '10.8'
	s.requires_arc = false
  
  s.library = 'c++'
  s.xcconfig = {
       'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
       'CLANG_CXX_LIBRARY' => 'libc++',
        'HEADER_SEARCH_PATHS' => '"$(PODS_ROOT)/vwgl/cplusplus/include" "$(PODS_ROOT)/vwgl/cplusplus/thirdparty/jsonbox/include" "$(PODS_ROOT)/vwgl/cplusplus/thirdparty/irrxml/src"'
  }

  # s.resource_bundles = {
  #   'Shaders' => ['cplusplus/shaders/**/*.{fsh,vsh}'],
  #   'Resources' => ['cplusplus/resources/**/*']
  # }
  
  s.resources = [ 'cplusplus/resources/**/*','cplusplus/shaders/**/*.{fsh,vsh}', ]
  
  s.prepare_command = <<-CMD
                    mv cplusplus/include/vwgl/platform.hpp.in cplusplus/include/vwgl/platform.hpp
                    sed -i '' 's/@PLATFORM_WINDOWS@/0/g' cplusplus/include/vwgl/platform.hpp
                    sed -i '' 's/@PLATFORM_MAC@/1/g' cplusplus/include/vwgl/platform.hpp 
                    sed -i '' 's/@PLATFORM_LINUX@/0/g' cplusplus/include/vwgl/platform.hpp 
                     CMD

  s.preserve_paths = "cplusplus/src/**"
  s.preserve_paths = "cplusplus/include/**"
  s.preserve_paths = "cplusplus/thirdparty/**"
  
#  s.public_header_files = "cplusplus/include/**/*.{h,hpp,in}"
  
  s.subspec 'include' do |sp|
#    sp.private_header_files = "cplusplus/include/**/*.{h,hpp}"
#    sp.source_files = "cplusplus/include/*.{h,hpp}"

    sp.subspec 'GL' do |sp|
      # sp.xcconfig = {
#            'HEADER_SEARCH_PATHS' => '"$(PODS_ROOT)/vwgl/cplusplus/include" "$(PODS_ROOT)/vwgl/cplusplus/thirdparty/jsonbox/include" "$(PODS_ROOT)/vwgl/cplusplus/thirdparty/irrxml/src"'
#       }
#
      sp.private_header_files = "cplusplus/include/GL/*.{h,hpp}"
#      sp.source_files = "cplusplus/include/GL/*.{h,hpp}"
      sp.preserve_paths = "cplusplus/include/vwgl/GL"
    end

    sp.subspec 'vwgl' do |sp|
      sp.private_header_files = "cplusplus/include/vwgl/*.{h,hpp,in}"
#      sp.source_files = "cplusplus/include/vwgl/*.{h,hpp,in}"

      sp.preserve_paths = "cplusplus/include/vwgl"
      
      sp.subspec 'app' do |sp|
        sp.private_header_files = "cplusplus/include/vwgl/app/*.{h,hpp}"
#         sp.source_files = "cplusplus/include/vwgl/app/*.{h,hpp}"
         sp.preserve_paths = "cplusplus/include/vwgl/app"
      end
      
      sp.subspec 'collada' do |sp|
        sp.private_header_files = "cplusplus/include/vwgl/collada/*.{h,hpp}"
#         sp.source_files = "cplusplus/include/vwgl/collada/*.{h,hpp}"
         sp.preserve_paths = "cplusplus/include/vwgl/collada"
      end
      
      sp.subspec 'library' do |sp|
#        sp.public_header_files = "cplusplus/include/vwgl/library/*.{h,hpp}"
        sp.private_header_files = "cplusplus/include/vwgl/library/*.{h,hpp}"
         sp.preserve_paths = "cplusplus/include/vwgl/library"
      end
      
      sp.subspec 'physics' do |sp|
        sp.private_header_files = "cplusplus/include/vwgl/physics/*.{h,hpp}"
#        sp.source_files = "cplusplus/include/vwgl/physics/*.{h,hpp}"
         sp.preserve_paths = "cplusplus/include/vwgl/physics"
      end
      
    end
    
  end
  
  s.subspec 'src' do |sp|
    sp.public_header_files = "cplusplus/src/*.{h,hpp}"
    # sp.private_header_files = "cplusplus/src/*.{h,hpp}"
    sp.preserve_paths = "cplusplus/src"
    sp.ios.private_header_files = "cplusplus/src/ios/*.{h,hpp}"
    sp.source_files = "cplusplus/src/*.{cpp,m,mm}"
    sp.ios.source_files = 'cplusplus/src/ios/*.{cpp,m,mm}'
    sp.osx.source_files = 'cplusplus/src/mac/*.{cpp,m,mm}'
    sp.exclude_files = "cplusplus/src/image.cpp"
  end
  
  s.subspec 'thirdparty' do |sp|
    
    sp.subspec 'irrxml' do |sp|
      sp.subspec 'src' do |sp|
        sp.source_files = "cplusplus/thirdparty/irrxml/src/**/*.{c,cpp}"
      end
    end

    sp.subspec 'jsonbox' do |sp|
      
      sp.subspec 'include' do |sp|
        sp.private_header_files = "cplusplus/thirdparty/jsonbox/include/*.{h,hpp}"
        sp.preserve_paths = "cplusplus/thirdparty/jsonbox/include"
        sp.subspec 'JsonBox' do |sp|
          
          sp.private_header_files = "cplusplus/thirdparty/jsonbox/include/JsonBox/*.{h,hpp}"
          
          sp.preserve_paths = "cplusplus/thirdparty/jsonbox/include/JsonBox"
        end
      end
      sp.subspec 'src' do |sp|
        sp.source_files = "cplusplus/thirdparty/jsonbox/src/**/*.{c,cpp}"      
      end
    end
  
    # sp.subspec 'soil' do |sp|
#       sp.subspec 'src' do |sp|
#         sp.osx.source_files = "cplusplus/thirdparty/soil/src/*.{c,cpp}"
#         sp.osx.public_header_files = "cplusplus/thirdparty/soil/src/*.{h,hpp}"
#
#         sp.subspec 'original' do |sp|
#           sp.osx.source_files = "cplusplus/thirdparty/soil/src/original/*.{c}"
#         end
#
#       end
#     end
    
  end
     
end