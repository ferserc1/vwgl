/**
 * @file
 */
#ifndef JB_VALUE_H
#define JB_VALUE_H

#include <string>
#include <iostream>

namespace JsonBox {
	class Array;
	class Object;

	class Value {
		friend std::ostream &operator<<(std::ostream &output, const Value &v);
	public:
		enum Type {
		    STRING,
		    INTEGER,
		    DOUBLE,
		    OBJECT,
		    ARRAY,
		    BOOLEAN,
			NULL_VALUE,
			UNKNOWN
		};
		
		static std::string escapeMinimumCharacters(const std::string &str);
		static std::string escapeAllCharacters(const std::string &str);
		static const std::string escapeToUnicode(char charToEscape);

		Value();
		Value(std::istream &input);
		Value(const std::string &newString);
		Value(const char *newCString);
		Value(int newInt);
		Value(double newDouble);
		Value(const Object &newObject);
		Value(const Array &newArray);
		Value(bool newBoolean);
		Value(const Value &src);
		~Value();

		Value &operator=(const Value &src);
		bool operator==(const Value &rhs) const;
		bool operator!=(const Value &rhs) const;
		bool operator <(const Value &rhs) const;
		bool operator <=(const Value &rhs) const;
		bool operator >(const Value &rhs) const;
		bool operator >=(const Value &rhs) const;
		Value &operator[](const std::string &key);
		Value &operator[](const char *key);
		Value &operator[](size_t index);

		Type getType() const;
		bool isString() const;
		bool isInteger() const;
		bool isDouble() const;
		bool isObject() const;
		bool isArray() const;
		bool isBoolean() const;
		bool isNull() const;

		const std::string &getString() const;
		void setString(const std::string &newString);
		int getInt() const;
		void setInt(int newInt);
		double getDouble() const;
		void setDouble(double newDouble);
		const Object &getObject() const;
		void setObject(const Object &newObject);
		const Array &getArray() const;
		void setArray(const Array &newArray);
		bool getBoolean() const;
		void setBoolean(bool newBoolean);
		void setNull();
public:
		void loadFromString(const std::string &json);
		void loadFromStream(std::istream &input);
		void loadFromFile(const std::string &filePath);
		void writeToStream(std::ostream &output, bool indent = true, bool escapeAll = false) const;
		void writeToFile(const std::string &filePath, bool indent = true, bool escapeAll = false) const;
		
	private:
		/**
		 * Union used to contain the pointer to the value's data.
		 */
		union ValueDataPointer {
			std::string *stringValue;
			int *intValue;
			double *doubleValue;
			Object *objectValue;
			Array *arrayValue;
			bool *boolValue;

			/**
			 * Default constructor. Puts the pointers at NULL.
			 */
			ValueDataPointer();

			/**
			 * Parameterized constructor.
			 * @param newConstStringValue Pointer to set to the string pointer.
			 */
			ValueDataPointer(std::string *newStringValue);

			/**
			 * Parameterized constructor.
			 * @param newConstIntValue Pointer to set to the int pointer.
			 */
			ValueDataPointer(int *newIntValue);

			/**
			 * Parameterized constructor.
			 * @param newConstDoubleValue Pointer to set to the double pointer.
			 */
			ValueDataPointer(double *newDoubleValue);

			/**
			 * Parameterized constructor.
			 * @param newConstObjectValue Pointer to set to the object pointer.
			 */
			ValueDataPointer(Object *newObjectValue);

			/**
			 * Parameterized constructor.
			 * @param newConstArrayValue Pointer to set to the array pointer.
			 */
			ValueDataPointer(Array *newArrayValue);

			/**
			 * Parameterized constructor.
			 * @param newConstBoolValue Pointer to set to the bool pointer.
			 */
			ValueDataPointer(bool *newBoolValue);
		};

		/**
		 * Empty string returned by getString() when the value doesn't contain a
		 * string.
		 * @see JsonBox::Value::getString
		 */
		static const std::string EMPTY_STRING;

		/**
		 * Default int value returned by getInt() when the value doesn't contain
		 * an integer.
		 * @see JsonBox::Value::getInt
		 */
		static const int EMPTY_INT = 0;

		/**
		 * Default double value returned by getDouble() when the value doesn't
		 * contain a double.
		 * @see JsonBox::Value::getDouble
		 */
		static const double EMPTY_DOUBLE;

		/**
		 * Default empty object value returned by getObject() when the value
		 * doesn't contain an object.
		 * @see JsonBox::Value::getObject
		 */
		static const Object EMPTY_OBJECT;

		/**
		 * Default empty array value returned by getArray() when the value
		 * doesn't contain an array.
		 * @see JsonBox::Value::getArray
		 */
		static const Array EMPTY_ARRAY;

		/**
		 * Default boolean value returned by getBoolean() when the value doesn't
		 * contain a boolean.
		 * @see JsonBox::Value::getBoolean
		 */
		static const bool EMPTY_BOOL = false;

		/**
		 * Checks if the char given is a hex digit.
		 * @return True if the char contains an hexadecimal digit (0-9, a-f or
		 * A-F).
		 */
		static bool isHexDigit(char digit);

		/**
		 * Checks if the char given is a JSON whitespace.
		 * @return True if the char is either a space, a horizontal tab, a line
		 * feed or a carriage return.
		 */
		static bool isWhiteSpace(char whiteSpace);

		/**
		 * Reads a JSON string from an input stream.
		 * @param input Input stream to read the string value from.
		 * @param result UTF-8 string read from the input stream.
		 */
		static void readString(std::istream &input, std::string &result);

		/**
		 * Reads a JSON object from an input stream.
		 * @param input Input stream to read the object from.
		 * @param result Object read from the input stream.
		 */
		static void readObject(std::istream &input, Object &result);

		/**
		 * Reads a JSON array from an input stream.
		 * @param input Input stream to read the array from.
		 * @param result Array read from the input stream.
		 */
		static void readArray(std::istream &input, Array &result);

		/**
		 * Reads a JSON number from an input stream.
		 * @param input Input stream to read the array from.
		 * @param result Value containing the integer or the double read from
		 * the input stream.
		 */
		static void readNumber(std::istream &input, Value &result);

		/**
		 * Advances through the input stream until it reaches a character that
		 * is not a whitespace.
		 * @param input Input stream to read the whitespace characters from.
		 * @param currentCharacter Char in which each character read is
		 * temporarily stored. After the method is called, this char contains
		 * the first non white space character reached.
		 */
		static void readToNonWhiteSpace(std::istream &input,
		                                char &currentCharacter);

		/**
		 * Frees up the dynamic memory allocated by the value.
		 */
		void clear();

		/**
		 * Outputs the value in JSON format.
		 * @param output Output stream used to output the value in JSON format.
		 * @param indent Specifies if the JSON being output must be indented or
		 * not. False is to output the JSON in compact format.
		 * @param escapeAll Specifies if the strings must escape all characters
		 * or only the minimum.
		 * @see JsonBox::Value::escapeAllCharacters(const std::string str)
		 * @see JsonBox::Value::escapeMinimumCharacters(const std::string str)
		 * @see JsonBox::Value::output(std::ostream& output, unsigned int& level, bool indent, bool escapeAll)
		 */
		void output(std::ostream &output, bool indent = true,
		            bool escapeAll = false) const;

		/**
		 * Type of data the value contains.
		 */
		Type type;

		/**
		 * Pointer to the Value's data.
		 */
		ValueDataPointer data;
	};
}

#endif
