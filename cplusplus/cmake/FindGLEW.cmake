#
# Try to find GLEW library and include path.
# Once done this will define
#
# GLEW_FOUND
# GLEW_INCLUDE_PATH
# GLEW_LIBRARY
# GLEW_DLL_PATH
# GLEW_DLL
# 

IF (WIN32)
	FIND_PATH( GLEW_INCLUDE_PATH GL/glew.h
		${PROJECT_SOURCE_DIR}/../../glew/include/
		$ENV{PROGRAMFILES}/GLEW/include
		${PROJECT_SOURCE_DIR}/src/nvgl/glew/include
		DOC "The directory where GL/glew.h resides")

	IF (CMAKE_CL_64)
		SET(GLEW_LIB_PLATFORM_FOLDER ${PROJECT_SOURCE_DIR}/../../glew/lib/Release/x64)
		SET(GLEW_DLL_PLATFORM_FOLDER ${PROJECT_SOURCE_DIR}/../../glew/bin/Release/x64)
		MESSAGE(STATUS "Finding GLEW 64 bits")
	ELSE (CMAKE_CL_64)
		SET(GLEW_LIB_PLATFORM_FOLDER ${PROJECT_SOURCE_DIR}/../../glew/lib/Release/Win32)
		SET(GLEW_DLL_PLATFORM_FOLDER ${PROJECT_SOURCE_DIR}/../../glew/bin/Release/Win32)
		MESSAGE(STATUS "Finding GLEW 32 bits")
	ENDIF (CMAKE_CL_64)

	FIND_LIBRARY( GLEW_LIBRARY
		NAMES glew GLEW glew32 glew32s
		PATHS
		${GLEW_LIB_PLATFORM_FOLDER}
		$ENV{PROGRAMFILES}/GLEW/lib
		${PROJECT_SOURCE_DIR}/src/nvgl/glew/bin
		${PROJECT_SOURCE_DIR}/src/nvgl/glew/lib
		DOC "The GLEW library")

	FIND_PATH( GLEW_DLL_PATH glew32.dll
		PATHS
		${GLEW_DLL_PLATFORM_FOLDER})

	SET(GLEW_DLL "${GLEW_DLL_PATH}/glew32.dll" CACHE STRING "GLEW dll file to copy it to the binary folder")

ELSE (WIN32)
	FIND_PATH( GLEW_INCLUDE_PATH GL/glew.h
		/usr/include
		/usr/local/include
		/sw/include
		/opt/local/include
		DOC "The directory where GL/glew.h resides")
	FIND_LIBRARY( GLEW_LIBRARY
		NAMES GLEW glew
		PATHS
		/usr/lib64
		/usr/lib
		/usr/local/lib64
		/usr/local/lib
		/sw/lib
		/opt/local/lib
		DOC "The GLEW library")
ENDIF (WIN32)

IF (GLEW_INCLUDE_PATH)
	SET( GLEW_FOUND 1 CACHE STRING "Set to 1 if GLEW is found, 0 otherwise")
ELSE (GLEW_INCLUDE_PATH)
	SET( GLEW_FOUND 0 CACHE STRING "Set to 1 if GLEW is found, 0 otherwise")
ENDIF (GLEW_INCLUDE_PATH)

IF (GLEW_FOUND)
	MESSAGE(STATUS "GLEW library found")
ENDIF (GLEW_FOUND)