
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>

class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::PolyList> _polyList;
	vwgl::ptr<vwgl::Shader> _shader;
	float _rot;

	GLuint _vertexAttribLocation;
		
public:

	void initGL() {
		std::string shaderPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(shaderPath);
	
		glClearColor(0.2,0.5,1,1.0);
		glEnable(GL_DEPTH_TEST);
		
		_polyList = new vwgl::PolyList();
		
		_polyList->addVertex(vwgl::Vector3(-1.0,-1.0,0.0));	// 0
		_polyList->addVertex(vwgl::Vector3( 1.0,-1.0,0.0));	// 1
		_polyList->addVertex(vwgl::Vector3( 1.0, 1.0,0.0));	// 2
		_polyList->addVertex(vwgl::Vector3(-1.0, 1.0,0.0));	// 3
		
		_polyList->addTriangle(0,1,2);
		_polyList->addTriangle(2,3,0);
		
		_polyList->buildPolyList();
		
		_shader = new vwgl::Shader();
		bool status = _shader->loadAndAttachShader(vwgl::Shader::kTypeVertex, "basic.vsh");
		status = status && _shader->loadAndAttachShader(vwgl::Shader::kTypeFragment, "basic.fsh");
		status = status && _shader->link();
		if (!status) {
			std::cerr << "Error loading shader" << std::endl;
			exit(1);
		}
		else {
			_vertexAttribLocation = _shader->getAttribLocation("aVertexPosition");
			_shader->initUniformLocation("uMVMatrix");
			_shader->initUniformLocation("uPMatrix");
		}
		_rot = 0.0;
	}
		
	void display() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		vwgl::Viewport vp = vwgl::State::get()->getViewport();
		vwgl::State::get()->projectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		
		vwgl::State::get()->viewMatrix().identity().translate(0.0,0.0,-5.0);
		vwgl::State::get()->modelMatrix().identity().rotate(vwgl::Math::degreesToRadians(_rot),0.0,1.0,0.0);
		_rot++;
		
		_shader->bind();
			
		glBindBuffer(GL_ARRAY_BUFFER, _polyList->getVertexBuffer());
		glEnableVertexAttribArray(_vertexAttribLocation);
		glVertexAttribPointer(_vertexAttribLocation,3,GL_FLOAT,false,0,0);
			
		_shader->setUniform("uMVMatrix",vwgl::State::get()->modelViewMatrix());
		_shader->setUniform("uPMatrix",vwgl::State::get()->projectionMatrix());
			
		_polyList->drawElements();
			
		glDisableVertexAttribArray(_vertexAttribLocation);
			
		_shader->unbind();
		glutSwapBuffers();
	}

	void reshape(int width, int height) {
		vwgl::State::get()->setViewport(vwgl::Viewport(0,0,width,height));
	}
		
	void idle() {
		glutPostRedisplay();
	}
	
	void destroy() {
		_polyList = nullptr;
		_shader = nullptr;
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

