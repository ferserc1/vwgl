
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>


class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::Group> _sceneRoot;
	vwgl::ptr<vwgl::DrawNodeVisitor> _drawVisitor;
	vwgl::ptr<vwgl::Camera> _camera;
	
	vwgl::ptr<vwgl::MouseTargetManipulator> _nodeManipulator;
	
	float _lightRot;
	float _lightElevation;
	
	bool _zoom;
	
public:
	
	
	void initGL() {
		_lightRot = 0.0f;
		_lightElevation = -25.0f;
		_zoom = false;
		
		std::string execPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(execPath);
		vwgl::System::get()->setResourcesPath(execPath);
		
		glClearColor(0.0,0.0,0.0,1.0);
		glEnable(GL_DEPTH_TEST);

		
		_sceneRoot = new vwgl::Group();
		
		_drawVisitor = new vwgl::DrawNodeVisitor();
		
		
		vwgl::Light * light = new vwgl::Light();
		vwgl::Matrix4 trxMatrix = vwgl::Matrix4::makeRotation(vwgl::Math::degreesToRadians(_lightElevation), 1.0, 0.0, 0.0).translate(0.0, 0.0, 10.0);
		vwgl::TransformNode * lightTrx = new vwgl::TransformNode(trxMatrix);
		lightTrx->addChild(light);
		_sceneRoot->addChild(lightTrx);
		
		
		vwgl::Drawable * cube = new vwgl::Cube(1.0,1.0,1.0);
		vwgl::GenericMaterial * cubeMat = new vwgl::GenericMaterial();
		cubeMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		cubeMat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png"));
		cube->setMaterial(cubeMat);
		vwgl::TransformNode * cubeTrx = new vwgl::TransformNode();
		cubeTrx->addChild(new vwgl::DrawableNode(cube));
		_sceneRoot->addChild(cubeTrx);
		
		vwgl::Drawable * floor = new vwgl::Plane(5.0);
		vwgl::GenericMaterial * floorMat = new vwgl::GenericMaterial();
		floorMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		floorMat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png"));
		floorMat->setTextureScale(vwgl::Vector2(4));
		floorMat->setNormalMapScale(vwgl::Vector2(4));
		floor->setMaterial(floorMat);
		
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, -1.5, 0.0));
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		
		_sceneRoot->addChild(floorTrx);
		
		_camera = new vwgl::Camera();
		vwgl::TransformNode * cameraNode = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 5.0));
		cameraNode->addChild(_camera.getPtr());
		_sceneRoot->addChild(cameraNode);
		
		_nodeManipulator = new vwgl::MouseTargetManipulator(cameraNode);
		
		_nodeManipulator->setCenter(cubeTrx->getTransform().getPosition());
		
		_nodeManipulator->setTransform();
	}
	
	void display() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		vwgl::LightManager::get()->prepareFrame();
		
		vwgl::CameraManager::get()->applyTransform(_camera.getPtr());
		
		_drawVisitor->visit(_sceneRoot.getPtr());
		
		swapBuffers();
	}
	
	void reshape(int width, int height) {
		vwgl::Viewport vp = vwgl::Viewport(0,0,width,height);
		vwgl::State::get()->setViewport(vp);
		
		_camera->getProjectionMatrix().perspective(45.0, vp.aspectRatio(), 0.1, 100.0);
	}
	
	void idle() {
		vwgl::LightManager * lm = vwgl::LightManager::get();
		vwgl::Light * l = lm->getLightList().front();
		
		// Light::getTransform() returns the closest vwgl::TransformNode transform matrix, but the light will
		// be affected by all the transform nodes over it's node. Use this carefully
		vwgl::Matrix4 & lightTrx = l->getTransform();
		lightTrx.identity()
			.rotate(vwgl::Math::degreesToRadians(_lightRot),0.0, 1.0, 0.0)
			.rotate(vwgl::Math::degreesToRadians(_lightElevation), 1.0, 0.0, 0.0)
			.translate(0.0, 0.0, 10.0);
		_lightRot++;

		glutPostRedisplay();
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_drawVisitor = nullptr;
		_camera = nullptr;
		_nodeManipulator = nullptr;
		vwgl::Loader::destroy();
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
	
	void mouse(int button, int state, int x, int y) {
		unsigned int keyModifiers = glutGetModifiers();
		
		if (state==oglut::Mouse::kDownState) {
			// ObjectGLUT PC/Mac version doesn't track the mouseWheel events, so, we use the alt key to emulate the middle button
			// to support users who haven't a three button mouse (Apple Magic Mouse, laptop trackpads etc.)
			if (button==oglut::Mouse::kLeftButton && (keyModifiers & GLUT_ACTIVE_ALT)) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
			else if (button==oglut::Mouse::kLeftButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationRotate);
			}
			else if (button==oglut::Mouse::kRightButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationDrag);
			}
			else if (button==oglut::Mouse::kMiddleButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
		}
	}
	
	void motion(int x, int y) {
		_nodeManipulator->mouseMove(vwgl::Position2Di(x,y));
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

