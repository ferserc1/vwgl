
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>


class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::Group> _sceneRoot;
	vwgl::ptr<vwgl::DrawNodeVisitor> _drawVisitor;
	vwgl::ptr<vwgl::TransformNode> _cubeTrx;
	vwgl::ptr<vwgl::Camera> _camera;
	float _lightElevation;
	
	vwgl::ptr<vwgl::ColorPicker> _colorPicker;
	vwgl::ptr<vwgl::Drawable> _selectedDrawable;
	vwgl::ptr<vwgl::Solid> _selectedSolid;

public:
	
	
	void initGL() {
		std::string execPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(execPath);
		vwgl::System::get()->setResourcesPath(execPath);
		
		glClearColor(0.2,0.5,1,1.0);
		glEnable(GL_DEPTH_TEST);
		
		_sceneRoot = new vwgl::Group();
		_drawVisitor = new vwgl::DrawNodeVisitor();
		
		vwgl::Light * light = new vwgl::Light();
		vwgl::TransformNode * lightTrx = new vwgl::TransformNode(vwgl::Matrix4::makeRotation(vwgl::Math::degreesToRadians(_lightElevation), 1.0, 0.0, 0.0).translate(0.0, 0.0, 10.0));
		lightTrx->addChild(light);
		_sceneRoot->addChild(lightTrx);
		
		vwgl::Drawable * sphere = new vwgl::Sphere(0.5,30);
		vwgl::GenericMaterial * sphereMat = new vwgl::GenericMaterial();
		sphereMat->setTexture(vwgl::Loader::get()->loadTexture("leather_03.jpg"));
		sphereMat->setNormalMap(vwgl::Loader::get()->loadTexture("leather_03_nm.png"));
		sphere->setMaterial(sphereMat);
		vwgl::TransformNode * sphereTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 1.5, 0.0));
		sphereTrx->addChild(new vwgl::DrawableNode(sphere));
		_sceneRoot->addChild(sphereTrx);
		
		vwgl::Drawable * cube = new vwgl::Cube(1.0,1.0,1.0);
		vwgl::GenericMaterial * cubeMat = new vwgl::GenericMaterial();
		cubeMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		cubeMat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png"));
		cube->setMaterial(cubeMat);
		_cubeTrx = new vwgl::TransformNode();
		_cubeTrx->addChild(new vwgl::DrawableNode(cube));
		_sceneRoot->addChild(_cubeTrx.getPtr());
		
		vwgl::Drawable * floor = new vwgl::Plane(5.0);
		vwgl::GenericMaterial * floorMat = new vwgl::GenericMaterial();
		floorMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		floorMat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png"));
		floor->setMaterial(floorMat);
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, -1.5, 0.0));
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		_sceneRoot->addChild(floorTrx);
		
		_colorPicker = new vwgl::ColorPicker();
		_colorPicker->assignPickId(sphere);
		_colorPicker->assignPickId(cube);
		_colorPicker->assignPickId(floor);
		
		_camera = new vwgl::Camera();
		vwgl::TransformNode * cameraNode = new vwgl::TransformNode();
		cameraNode->addChild(_camera.getPtr());
		_camera->getTransform()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(45.0), 0.0, 1.0, 0.0)
			.translate(0.0, 0.0, 5.0);
		_sceneRoot->addChild(cameraNode);
	}
	
	void display() {
		glClearColor(0.2, 0.5, 1.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		vwgl::LightManager::get()->prepareFrame();
		vwgl::CameraManager::get()->applyTransform();
		_drawVisitor->visit(_sceneRoot.getPtr());
		
		swapBuffers();
	}
	
	void reshape(int width, int height) {
		vwgl::Viewport vp = vwgl::Viewport(0,0,width,height);
		vwgl::State::get()->setViewport(vp);
		
		_camera->getProjectionMatrix().perspective(45.0, vp.aspectRatio(), 0.1, 100.0);
	}
	
	void idle() {
		_cubeTrx->getTransform().rotate(vwgl::Math::degreesToRadians(1.0), 1.0, 0.0, 0.0);
				
		glutPostRedisplay();
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_drawVisitor = nullptr;
		_cubeTrx = nullptr;
		_camera = nullptr;
		_selectedSolid = nullptr;
		_selectedDrawable = nullptr;
		_colorPicker = nullptr;
		vwgl::Loader::destroy();
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
	
	void mouse(int button, int state, int x, int y) {
		if (button==oglut::Mouse::kLeftButton && state==oglut::Mouse::kUpState) {
			if (_selectedSolid.valid()) {
				vwgl::GenericMaterial * mat = _selectedSolid->getGenericMaterial();
				if (mat) {
					mat->setSelectedMode(false);
				}
			}
			_selectedDrawable = nullptr;
			_selectedSolid = nullptr;
			
			vwgl::Viewport viewport = vwgl::State::get()->getViewport();
			_colorPicker->pick(_sceneRoot.getPtr(),
							   x,
							   viewport.height() - y,
							   viewport.width(),
							   viewport.height(),
							   vwgl::CameraManager::get()->getMainCamera());
			if ((_selectedDrawable=_colorPicker->getDrawable()).valid()) {
				// Drawable selected
			}
			if ((_selectedSolid=_colorPicker->getSolid()).valid()) {
				vwgl::GenericMaterial * mat = _selectedSolid->getGenericMaterial();
				if (mat) {
					mat->setSelectedMode(true);
				}
			}
			display();
		}
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

