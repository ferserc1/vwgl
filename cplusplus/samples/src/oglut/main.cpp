
#include <iostream>
#include <oglut/oglut.h>

class MyApp : public oglut::Application {
public:
	void initGL() {
		
	}
	
	void display() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	
	void reshape(int width, int height) {
		glViewport(0,0,width,height);
	}
	
	void idle() {
		//console.log("Idle");
		//glutPostRedisplay();
	}
	
	void keyboard(unsigned char key, int x, int y) {
		std::cout << "key down: " << key << std::endl;
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		std::cout << "key up: " << key << std::endl;
		
		// Esc key
		if (key==27) exit(0);
	}
	
	void special(int key, int x, int y) {
		std::cout << "special down: " << key << std::endl;
	}
	
	void specialUp(int key, int x, int y) {
		std::cout << "special up: " << key << std::endl;
	}
	
	void mouse(int button, int state, int x, int y) {
		std::cout << "Mouse button: " << button << ", state:" << state << ", position: x=" << x << ", y=" << y << std::endl;
	}
	
	void motion(int x, int y) {
		std::cout << "Mouse motion: x=" << x << ", y=" << y << std::endl;
	}

	void motionPassive(int x, int y) {

	}

	void mouseWheel(int delta, int x, int y) {
		std::cout << "Mouse wheel. Delta:" << delta << std::endl;
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);

	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

