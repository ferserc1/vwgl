
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>

class BasicMaterial : public vwgl::Material {
public:
	BasicMaterial() :Material() { initShader(); }
	BasicMaterial(const std::string & name) :Material(name) { initShader(); }
	
	virtual void initShader() {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "basic.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "basic.fsh");
		getShader()->link("Basic_material");
		
		loadVertexAttrib("aVertexPosition");
		getShader()->initUniformLocation("uMVMatrix");
		getShader()->initUniformLocation("uPMatrix");
		
		_initialized = true;
	}

protected:
	virtual void setupUniforms() {
		getShader()->setUniform("uMVMatrix", modelViewMatrix());
		getShader()->setUniform("uPMatrix", projectionMatrix());
	}
};

class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::PolyList> _polyList;
	vwgl::ptr<vwgl::Material> _material;
	float _rot;
	
	GLuint _vertexAttribLocation;
	
public:
	
	void initGL() {
		std::string shaderPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(shaderPath);
		
		glClearColor(0.2,0.5,1,1.0);
		glEnable(GL_DEPTH_TEST);
		
		_polyList = new vwgl::PolyList();
		
		_polyList->addVertex(vwgl::Vector3(-1.0,-1.0,0.0));	// 0
		_polyList->addVertex(vwgl::Vector3( 1.0,-1.0,0.0));	// 1
		_polyList->addVertex(vwgl::Vector3( 1.0, 1.0,0.0));	// 2
		_polyList->addVertex(vwgl::Vector3(-1.0, 1.0,0.0));	// 3
		
		_polyList->addTriangle(0,1,2);
		_polyList->addTriangle(2,3,0);
		
		_polyList->buildPolyList();
		
		_material = new BasicMaterial();
		_material->setCullFace(false);
		
		_rot = 0.0;
	}
	
	void display() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		vwgl::Viewport vp = vwgl::State::get()->getViewport();
		vwgl::State::get()->projectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		
		vwgl::State::get()->viewMatrix().identity().translate(0.0,0.0,-5.0);
		vwgl::State::get()->modelMatrix().identity().rotate(vwgl::Math::degreesToRadians(_rot),0.0,1.0,0.0);
		_rot++;
		
		
		_material->bindPolyList(_polyList.getPtr());
		_material->activate();
		_polyList->drawElements();
		_material->deactivate();
		
		glutSwapBuffers();
	}
	
	void reshape(int width, int height) {
		vwgl::State::get()->setViewport(vwgl::Viewport(0,0,width,height));
	}
	
	void idle() {
		glutPostRedisplay();
	}
	
	void destroy() {
		_polyList = nullptr;
		_material = nullptr;
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

