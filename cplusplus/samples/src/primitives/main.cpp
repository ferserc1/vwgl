
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>


class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::Group> _sceneRoot;
	vwgl::ptr<vwgl::DrawNodeVisitor> _drawVisitor;
	vwgl::ptr<vwgl::TransformNode> _cubeTrx;
	vwgl::ptr<vwgl::Camera> _camera;
	float _cameraRot;

public:
	
	
	void initGL() {
		std::string execPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(execPath);
		vwgl::System::get()->setResourcesPath(execPath);
		
		glClearColor(0.2,0.5,1,1.0);
		glEnable(GL_DEPTH_TEST);
		
		vwgl::Vector3 lightDirection(0.0,-1.0,-1.0);
		lightDirection.normalize();
		
		_sceneRoot = new vwgl::Group();
		
		_drawVisitor = new vwgl::DrawNodeVisitor();
		
		vwgl::Drawable * sphere = new vwgl::Sphere(0.5,30);
		vwgl::BasicPhongMaterial * sphereMat = new vwgl::BasicPhongMaterial();
		sphereMat->setDiffuse(vwgl::Color::yellow());
		sphereMat->setLightDirection(lightDirection);
		sphereMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		sphere->setMaterial(sphereMat);
		
		vwgl::DrawableNode * sphereNode = new vwgl::DrawableNode(sphere);
		
		vwgl::TransformNode * sphereTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 1.5, 0.0));
		sphereTrx->addChild(sphereNode);
		
		_sceneRoot->addChild(sphereTrx);
		
		vwgl::Drawable * cube = new vwgl::Cube(1.0,1.0,1.0);
		vwgl::BasicPhongMaterial * cubeMat = new vwgl::BasicPhongMaterial();
		cubeMat->setDiffuse(vwgl::Color::green());
		cubeMat->setLightDirection(lightDirection);
		cubeMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		cube->setMaterial(cubeMat);
		
		vwgl::DrawableNode * cubeNode = new vwgl::DrawableNode(cube);
		
		_cubeTrx = new vwgl::TransformNode();
		_cubeTrx->addChild(cubeNode);
		
		_sceneRoot->addChild(_cubeTrx.getPtr());
		
		vwgl::Drawable * floor = new vwgl::Plane(5.0);
		vwgl::BasicPhongMaterial * floorMat = new vwgl::BasicPhongMaterial();
		floorMat->setDiffuse(vwgl::Color::red());
		floorMat->setLightDirection(lightDirection);
		floor->setMaterial(floorMat);
		
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, -1.5, 0.0));
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		
		_sceneRoot->addChild(floorTrx);
		
		_camera = new vwgl::Camera();
		vwgl::TransformNode * cameraNode = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 5.0));
		cameraNode->addChild(_camera.getPtr());
		_sceneRoot->addChild(cameraNode);
		
		_cameraRot = 0.0;
	}
	
	void display() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		vwgl::CameraManager::get()->applyTransform(_camera.getPtr());
		
		_drawVisitor->visit(_sceneRoot.getPtr());
		
		glutSwapBuffers();
	}
	
	void reshape(int width, int height) {
		vwgl::Viewport vp = vwgl::Viewport(0,0,width,height);
		vwgl::State::get()->setViewport(vp);
		
		_camera->getProjectionMatrix().perspective(45.0, vp.aspectRatio(), 0.1, 100.0);
	}
	
	void idle() {
		_cubeTrx->getTransform().rotate(vwgl::Math::degreesToRadians(1.0), 1.0, 0.0, 0.0);
		
		_camera->getTransform()
					.identity()
					.rotate(vwgl::Math::degreesToRadians(_cameraRot), 0.0, 1.0, 0.0)
					.translate(0.0, 0.0, 8.0);
		_cameraRot++;
	
		glutPostRedisplay();
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_drawVisitor = nullptr;
		_cubeTrx = nullptr;
		_camera = nullptr;
		vwgl::Loader::destroy();
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

