
#include <iostream>
#include <oglut/oglut.h>

#include <vwgl/vwgl.hpp>

class MyApp : public oglut::Application {
public:
	void initGL() {
		// In the C++ API this paths are predefined.
		std::string shaderPath = vwgl::System::get()->getDefaultShaderPath();
		std::string resourcesPath = vwgl::System::get()->getResourcesPath();
		
		std::cout << "Shader path: " << shaderPath << std::endl;
		std::cout << "Resources Path: " << resourcesPath << std::endl;
		
		// System utilities
		if (vwgl::System::get()->isMac()) std::cout << "System is a Mac" << std::endl;
		if (vwgl::System::get()->isWindows()) std::cout << "System is a Windows" << std::endl;
		if (vwgl::System::get()->isIOS()) std::cout << "System is a iOS" << std::endl;
		if (vwgl::System::get()->isIOSSimulator()) std::cout << "System is iOS Simulator" << std::endl;
		if (vwgl::System::get()->isBigEndian()) std::cout << "endianess is big endian" << std::endl;
		if (vwgl::System::get()->isLittleEndian()) std::cout << "endianess is little endian" << std::endl;
		
		// Register the reader and writter plugins before load or write a model
		vwgl::Loader::get()->registerReader(new vwgl::ColladaLoader());
		vwgl::Loader::get()->registerWritter(new vwgl::VwglbWritter());
		
		// Load resources using the following functions
		vwgl::Loader::get()->loadDrawable("drawablePath");
		vwgl::Loader::get()->loadTexture("texturePath");
		vwgl::Loader::get()->loadImage("image");
		vwgl::Loader::get()->loadCubemap("posx", "negx", "posy", "negy", "posz", "negz");
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		std::cout << "key up: " << key << std::endl;
		
		// Esc key
		if (key==27) exit(0);
	}
	
	void destroy() {
		// Call Loader::destroy() to release the model loaders
		vwgl::Loader::destroy();
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

