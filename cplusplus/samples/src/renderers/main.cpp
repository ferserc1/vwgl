
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>


class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::Group> _sceneRoot;
	vwgl::ptr<vwgl::Camera> _camera;
	vwgl::ptr<vwgl::MouseTargetManipulator> _nodeManipulator;
	
	vwgl::ptr<vwgl::Renderer> _renderer;
	
	bool _zoom;
	
public:
	
	
	void initGL() {
		_zoom = false;
		
		std::string execPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(execPath);
		vwgl::System::get()->setResourcesPath(execPath);
		
		glClearColor(0.0,0.0,0.0,1.0);
		glEnable(GL_DEPTH_TEST);
		
		_sceneRoot = new vwgl::Group();
		
//		_renderer = new vwgl::ForwardRenderer();
		_renderer = new vwgl::DeferredRenderer();
		_renderer->build(_sceneRoot.getPtr(), 1024, 1024);
		_renderer->setClearColor(vwgl::Color::black());
		
		vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader());
		
		// In the C++ API it's necesary to pass the full path to load models
		vwgl::Drawable * star = vwgl::Loader::get()->loadDrawable(execPath + "star.vwglb");
		vwgl::TransformNode * starTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 0.0));
		starTrx->addChild(new vwgl::DrawableNode(star));
		_sceneRoot->addChild(starTrx);
		
		
		vwgl::Light * light = new vwgl::Light();
		vwgl::Matrix4 trxMatrix = vwgl::Matrix4::makeRotation(vwgl::Math::degreesToRadians(-35.0), 1.0, 0.0, 0.0).translate(0.0, 0.0, 10.0);
		vwgl::TransformNode * lightTrx = new vwgl::TransformNode(trxMatrix);
		lightTrx->addChild(light);
		_sceneRoot->addChild(lightTrx);
		
		vwgl::Drawable * floor = new vwgl::Plane(10.0);
		vwgl::GenericMaterial * floorMat = new vwgl::GenericMaterial();
		floorMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		floorMat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png"));
		floorMat->setTextureScale(vwgl::Vector2(5));
		floorMat->setNormalMapScale(vwgl::Vector2(5));
		floorMat->setShininess(50.0);
		floorMat->setReceiveProjections(true);
		floor->setMaterial(floorMat);
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 0.0));
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		_sceneRoot->addChild(floorTrx);
		
		_camera = new vwgl::Camera();
		vwgl::TransformNode * cameraNode = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 5.0));
		cameraNode->addChild(_camera.getPtr());
		_sceneRoot->addChild(cameraNode);
		
		_nodeManipulator = new vwgl::MouseTargetManipulator(cameraNode);
		_nodeManipulator->setCenter(vwgl::Vector3(0.0,1.0,0.0));
		_nodeManipulator->setTransform();
	}
	
	void display() {
		_renderer->draw();
		swapBuffers();
	}
	
	void reshape(int width, int height) {
		vwgl::Viewport vp = vwgl::Viewport(0,0,width,height);
		_renderer->setViewport(vp);
		
		_camera->getProjectionMatrix().perspective(45.0, vp.aspectRatio(), 0.1, 100.0);
	}
	
	void idle() {
		glutPostRedisplay();
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_renderer = nullptr;
		_camera = nullptr;
		_nodeManipulator = nullptr;
		vwgl::Loader::destroy();
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
	
	void mouse(int button, int state, int x, int y) {
		unsigned int keyModifiers = glutGetModifiers();
		
		if (state==oglut::Mouse::kDownState) {
			// ObjectGLUT PC/Mac version doesn't track the mouseWheel events, so, we use the alt key to emulate the middle button
			// to support users who haven't a three button mouse (Apple Magic Mouse, laptop trackpads etc.)
			if (button==oglut::Mouse::kLeftButton && (keyModifiers & GLUT_ACTIVE_ALT)) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
			else if (button==oglut::Mouse::kLeftButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationRotate);
			}
			else if (button==oglut::Mouse::kRightButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationDrag);
			}
			else if (button==oglut::Mouse::kMiddleButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
		}
	}
	
	void motion(int x, int y) {
		_nodeManipulator->mouseMove(vwgl::Position2Di(x,y));
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

