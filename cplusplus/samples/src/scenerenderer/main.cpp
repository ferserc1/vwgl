
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>


class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::Group> _sceneRoot;
	vwgl::ptr<vwgl::Camera> _camera;
	
	vwgl::ptr<vwgl::MouseTargetManipulator> _nodeManipulator;
	
	vwgl::ptr<vwgl::SceneRenderer> _sceneRenderer;
	vwgl::ptr<vwgl::InitVisitor> _initVisitor;
	
	vwgl::ptr<vwgl::ProjTextureDeferredMaterial> _projectorMaterial;
	
	float _lightRot;
	float _lightElevation;
	
	bool _zoom;
	
public:
	
	
	void initGL() {
		_lightRot = 0.0f;
		_lightElevation = -25.0f;
		_zoom = false;
		
		std::string execPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(execPath);
		vwgl::System::get()->setResourcesPath(execPath);
		
		glClearColor(0.0,0.0,0.0,1.0);
		glEnable(GL_DEPTH_TEST);
		
		_sceneRoot = new vwgl::Group();
		
		_sceneRenderer = new vwgl::SceneRenderer();
		_initVisitor = new vwgl::InitVisitor();
		_projectorMaterial = new vwgl::ProjTextureDeferredMaterial();
		
		vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader());
		
		// In the C++ API it's necesary to pass the full path to load models
		vwgl::Drawable * star = vwgl::Loader::get()->loadDrawable(execPath + "star.vwglb");
		for (int i=0; i<vwgl::Loader::get()->getWarningMessages().size(); ++i) {
			std::cout << vwgl::Loader::get()->getWarningMessages()[i] << std::endl;
		}
		vwgl::TransformNode * starTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 0.0));
		starTrx->addChild(new vwgl::DrawableNode(star));
		_sceneRoot->addChild(starTrx);
		
		
		vwgl::Light * light = new vwgl::Light();
		vwgl::Matrix4 trxMatrix = vwgl::Matrix4::makeRotation(vwgl::Math::degreesToRadians(_lightElevation), 1.0, 0.0, 0.0).translate(0.0, 0.0, 10.0);
		vwgl::TransformNode * lightTrx = new vwgl::TransformNode(trxMatrix);
		lightTrx->addChild(light);
		_sceneRoot->addChild(lightTrx);
		
		vwgl::Drawable * floor = new vwgl::Plane(10.0);
		vwgl::GenericMaterial * floorMat = new vwgl::GenericMaterial();
		//floorMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		floorMat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png"));
		floorMat->setTextureScale(vwgl::Vector2(10));
		floorMat->setNormalMapScale(vwgl::Vector2(10));
		floorMat->setShininess(50.0);
		
		floorMat->setReceiveProjections(true);
		
		floor->setMaterial(floorMat);
		
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 0.0));
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		
		_sceneRoot->addChild(floorTrx);
		
		_camera = new vwgl::Camera();
		vwgl::TransformNode * cameraNode = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 5.0));
		cameraNode->addChild(_camera.getPtr());
		_sceneRoot->addChild(cameraNode);
		
		_nodeManipulator = new vwgl::MouseTargetManipulator(cameraNode);
		_nodeManipulator->setCenter(vwgl::Vector3(0.0,1.0,0.0));
		_nodeManipulator->setTransform();
	}
	
	void display() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		_initVisitor->visit(_sceneRoot.getPtr());
		vwgl::LightManager::get()->prepareFrame();
		vwgl::CameraManager::get()->applyTransform(_camera.getPtr());
		
		_sceneRenderer->getNodeVisitor()->setMaterial(nullptr);
		_sceneRenderer->getNodeVisitor()->setUseMaterialSettings(false);
		
		// Setup a little offset, to render the shadows over the color buffer
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(1.0,1.0);
		
		// Render the scene
		_sceneRenderer->draw(_sceneRoot.getPtr(),true);
		
		// Disable polygon offset
		glDisable(GL_POLYGON_OFFSET_FILL);
		
		// Now is time to render the projector shadows
		glDepthMask(GL_FALSE);		// We have our depth buffer ready in the previous render pass, so now we disable the depth buffer writting
		
		// Setup the projected texture material to the scene renderer. One adventage of using a scene renderer
		// is that it allows to render a scene using other material.
		_sceneRenderer->getNodeVisitor()->setMaterial(_projectorMaterial.getPtr());	// Render all objects using this._projectorMat...
		_sceneRenderer->getNodeVisitor()->setUseMaterialSettings(true);		// But use each object settings: for instance, an object may not receive projected textures
		
		// Render one pass for each projector
		vwgl::ProjectorManager::ProjectorList projectorList = vwgl::ProjectorManager::get()->getProjectorList();
		for (int i=0; i<projectorList.size(); ++i) {
			glEnable(GL_BLEND);	// Enable blend
			glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);	// Blend the projector shadow with the current color buffer
			
			// Setup the projector to the projected texture material
			_projectorMaterial->setProjector(projectorList[i]);
			_sceneRenderer->draw(_sceneRoot.getPtr(),true);
		}
		
		glDisable(GL_BLEND);	// Disable blend
		glDepthMask(GL_TRUE);	// Enable depth buffer writting again
		swapBuffers();
	}
	
	void reshape(int width, int height) {
		vwgl::Viewport vp = vwgl::Viewport(0,0,width,height);
		vwgl::State::get()->setViewport(vp);
		
		_camera->getProjectionMatrix().perspective(45.0, vp.aspectRatio(), 0.1, 100.0);
	}
	
	void idle() {
		vwgl::LightManager * lm = vwgl::LightManager::get();
		vwgl::Light * l = lm->getLightList().front();
		
		// Light::getTransform() returns the closest vwgl::TransformNode transform matrix, but the light will
		// be affected by all the transform nodes over it's node. Use this carefully
		vwgl::Matrix4 & lightTrx = l->getTransform();
		lightTrx.identity()
		.rotate(vwgl::Math::degreesToRadians(_lightRot),0.0, 1.0, 0.0)
		.rotate(vwgl::Math::degreesToRadians(_lightElevation), 1.0, 0.0, 0.0)
		.translate(0.0, 0.0, 10.0);
		_lightRot++;
		
		glutPostRedisplay();
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_sceneRenderer = nullptr;
		_camera = nullptr;
		_nodeManipulator = nullptr;
		_projectorMaterial = nullptr;
		_initVisitor = nullptr;
		vwgl::Loader::destroy();
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
	
	void mouse(int button, int state, int x, int y) {
		unsigned int keyModifiers = glutGetModifiers();
		
		if (state==oglut::Mouse::kDownState) {
			// ObjectGLUT PC/Mac version doesn't track the mouseWheel events, so, we use the alt key to emulate the middle button
			// to support users who haven't a three button mouse (Apple Magic Mouse, laptop trackpads etc.)
			if (button==oglut::Mouse::kLeftButton && (keyModifiers & GLUT_ACTIVE_ALT)) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
			else if (button==oglut::Mouse::kLeftButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationRotate);
			}
			else if (button==oglut::Mouse::kRightButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationDrag);
			}
			else if (button==oglut::Mouse::kMiddleButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
		}
	}
	
	void motion(int x, int y) {
		_nodeManipulator->mouseMove(vwgl::Position2Di(x,y));
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

