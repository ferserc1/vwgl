
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>


class CanvasMaterial : public vwgl::Material {
public:
	CanvasMaterial() :Material(), _radius(1.0) {
		initShader();
	}
	
	virtual void initShader() {
		if (_initialized) return;
		_initialized = true;
		
		bool status = getShader()->attachShader(vwgl::Shader::kTypeVertex, CanvasMaterial::_vshader);
		status = getShader()->attachShader(vwgl::Shader::kTypeFragment, CanvasMaterial::_fshader) && status;
		status = getShader()->link("test_canvas_material") && status;
		
		loadVertexAttrib("a_position");
		loadTexCoord0Attrib("a_texCoord");
		
		getShader()->initUniformLocation("uMVMatrix");
		getShader()->initUniformLocation("uPMatrix");
		getShader()->initUniformLocation("uTexture");
		getShader()->initUniformLocation("uTextureSize");
		getShader()->initUniformLocation("uKernel");
		getShader()->initUniformLocation("uRadius");
	}
	
	void setRadius(float r) { _radius = r; }
	float getRadius() const { return _radius; }
	
	void setTexture(vwgl::Texture * tex) { _texture = tex; }
	vwgl::Texture * getTexture() { return _texture.getPtr(); }
	
protected:
	void setupUniforms() {
		getShader()->setUniform("uMVMatrix", modelViewMatrix());
		getShader()->setUniform("uPMatrix", projectionMatrix());
		getShader()->setUniform("uTexture", _texture.getPtr(), vwgl::Texture::kTexture0);
		
		vwgl::Viewport vp = vwgl::State::get()->getViewport();
		vwgl::Size2D size = vwgl::Size2D(vp.width(),vp.height());
		getShader()->setUniform("uTextureSize", size);
		getShader()->setUniform("uRadius", _radius);
		
		float blurKernel[] = {
						  0.045, 0.122, 0.045,
						  0.122, 0.332, 0.122,
						  0.045, 0.122, 0.045
						  };
		getShader()->setUniform1v("uKernel", 9, blurKernel);
	}
	
	static std::string _vshader;
	static std::string _fshader;
	vwgl::ptr<vwgl::Texture> _texture;
	float _radius;
};

std::string CanvasMaterial::_vshader = "#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	attribute vec4 a_position;\n\
	attribute vec2 a_texCoord;\n\
	\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
	\n\
	varying vec2 v_texCoord;\n\
	\n\
	void main(){\n\
		gl_Position = uPMatrix * uMVMatrix * a_position;\n\
		v_texCoord = a_texCoord;\n\
	}";

std::string CanvasMaterial::_fshader = "#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	uniform sampler2D uTexture;\n\
	uniform vec2 uTextureSize;\n\
	uniform float uKernel[9];\n\
	uniform float uRadius;\n\
	\n\
	// the texCoords passed in from the vertex shader.\n\
	varying vec2 v_texCoord;\n\
	\n\
	void main() {\n\
		vec2 onePixel = vec2(1.0, 1.0) / uTextureSize * uRadius;\n\
		vec4 colorSum =\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2(-1, -1)) * uKernel[0] +\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2( 0, -1)) * uKernel[1] +\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2( 1, -1)) * uKernel[2] +\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2(-1,  0)) * uKernel[3] +\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2( 0,  0)) * uKernel[4] +\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2( 1,  0)) * uKernel[5] +\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2(-1,  1)) * uKernel[6] +\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2( 0,  1)) * uKernel[7] +\n\
		texture2D(uTexture, v_texCoord + onePixel * vec2( 1,  1)) * uKernel[8] ;\n\
		float kernelWeight =\n\
		uKernel[0] +\n\
		uKernel[1] +\n\
		uKernel[2] +\n\
		uKernel[3] +\n\
		uKernel[4] +\n\
		uKernel[5] +\n\
		uKernel[6] +\n\
		uKernel[7] +\n\
		uKernel[8] ;\n\
		\n\
		if (kernelWeight <= 0.0) {\n\
		kernelWeight = 1.0;\n\
		}\n\
		\n\
		// Divide the sum by the weight but just use rgb\n\
		// we'll set alpha to 1.0\n\
		gl_FragColor = vec4((colorSum / kernelWeight).rgb, 1.0);\n\
	}";

class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::Group> _sceneRoot;
	
	vwgl::ptr<vwgl::InitVisitor> _initVisitor;
	vwgl::ptr<vwgl::ProjTextureDeferredMaterial> _projectorMaterial;
	
	vwgl::ptr<vwgl::Camera> _camera;
	
	float _lightRot;
	float _lightElevation;
	
	bool _zoom;
	
	vwgl::ptr<vwgl::MouseTargetManipulator> _nodeManipulator;
	
	vwgl::ptr<vwgl::RenderPass> _renderPass;
	vwgl::ptr<vwgl::RenderCanvas> _renderCanvas;
	vwgl::ptr<vwgl::GenericMaterial> _renderMaterial;
	
public:
	
	void createCanvas() {
		vwgl::GenericMaterial * renderMaterial = new vwgl::GenericMaterial();
		CanvasMaterial * canvasMaterial = new CanvasMaterial();
		_renderPass = new vwgl::RenderPass(_sceneRoot.getPtr(), renderMaterial, new vwgl::FramebufferObject(1024,1024));
		canvasMaterial->setTexture(_renderPass->getFbo()->getTexture());
		canvasMaterial->setRadius(3.0);
		_renderCanvas = new vwgl::RenderCanvas();
		_renderCanvas->setMaterial(canvasMaterial);
		_renderMaterial = renderMaterial;
	}

	void resizeCanvas(const vwgl::Viewport & vp) {
		_renderPass->getFbo()->resize(vp.width(), vp.height());
	}
	
	void initGL() {
		_lightRot = 45.0f;
		_lightElevation = -35.0f;
		_zoom = false;
		
		std::string execPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(execPath);
		vwgl::System::get()->setResourcesPath(execPath);
		
		glClearColor(0.0,0.0,0.0,1.0);
		glEnable(GL_DEPTH_TEST);
		
		_sceneRoot = new vwgl::Group();
		
		_initVisitor = new vwgl::InitVisitor();
		_projectorMaterial = new vwgl::ProjTextureDeferredMaterial();
		
		vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader());
		
		// In the C++ API it's necesary to pass the full path to load models
		vwgl::Drawable * star = vwgl::Loader::get()->loadDrawable(execPath + "star.vwglb");
		vwgl::TransformNode * starTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 0.0));
		starTrx->addChild(new vwgl::DrawableNode(star));
		_sceneRoot->addChild(starTrx);
		
		
		vwgl::Light * light = new vwgl::Light();
		vwgl::Matrix4 trxMatrix = vwgl::Matrix4::makeRotation(vwgl::Math::degreesToRadians(_lightElevation), 1.0, 0.0, 0.0).translate(0.0, 0.0, 10.0);
		vwgl::TransformNode * lightTrx = new vwgl::TransformNode(trxMatrix);
		lightTrx->addChild(light);
		_sceneRoot->addChild(lightTrx);
		
		vwgl::Drawable * floor = new vwgl::Plane(10.0);
		vwgl::GenericMaterial * floorMat = new vwgl::GenericMaterial();
		floorMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		floorMat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png"));
		floorMat->setTextureScale(vwgl::Vector2(10));
		floorMat->setNormalMapScale(vwgl::Vector2(10));
		floorMat->setShininess(50.0);
		floorMat->setReceiveProjections(true);
		floor->setMaterial(floorMat);
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 0.0));
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		_sceneRoot->addChild(floorTrx);
		
		_camera = new vwgl::Camera();
		vwgl::TransformNode * cameraNode = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0, 0.0, 5.0));
		cameraNode->addChild(_camera.getPtr());
		_sceneRoot->addChild(cameraNode);
		
		_nodeManipulator = new vwgl::MouseTargetManipulator(cameraNode);
		_nodeManipulator->setCenter(vwgl::Vector3(0.0,1.0,0.0));
		_nodeManipulator->setTransform();
		
		createCanvas();
	}
	
	void drawScene() {
		vwgl::Viewport vp = vwgl::State::get()->getViewport();
		_camera->getProjectionMatrix().perspective(45.0, vp.aspectRatio(), 0.1, 100.0);
		
		vwgl::LightManager::get()->prepareFrame();
		vwgl::CameraManager::get()->applyTransform();
		
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(1.0,1.0);
		_renderPass->setClearBuffers(true);
		_renderPass->setRenderTransparentObjects(true);
		_renderPass->setRenderOpaqueObjects(true);
		_renderPass->updateTexture(true);
		glDisable(GL_POLYGON_OFFSET_FILL);
		

		glDepthMask(GL_FALSE);
		_renderPass->setMaterial(_projectorMaterial.getPtr());
		_renderPass->setClearBuffers(false);
		
		// Render one pass for each projector
		vwgl::ProjectorManager::ProjectorList projectorList = vwgl::ProjectorManager::get()->getProjectorList();
		for (int i=0; i<projectorList.size(); ++i) {
			glEnable(GL_BLEND);	// Enable blend
			glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);	// Blend the projector shadow with the current color buffer
			
			// Setup the projector to the projected texture material
			_projectorMaterial->setProjector(projectorList[i]);
			_renderPass->updateTexture(true);
		}
		glEnable(GL_BLEND);
		glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
		
		_renderPass->setMaterial(_renderMaterial.getPtr());
		_renderPass->setRenderTransparentObjects(true);
		_renderPass->setRenderOpaqueObjects(false);
		_renderPass->updateTexture(true);
	
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
		
		_renderPass->setClearBuffers(true);
		
		swapBuffers();
	}
	
	void drawCanvas() {
		vwgl::Viewport vp = vwgl::State::get()->getViewport();
		_renderCanvas->draw(vp.width(), vp.height());
	}
	
	void display() {
		_initVisitor->visit(_sceneRoot.getPtr());
		drawScene();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		drawCanvas();
	}
	
	void reshape(int width, int height) {
		vwgl::Viewport vp = vwgl::Viewport(0,0,width,height);
		vwgl::State::get()->setViewport(vp);
		
		resizeCanvas(vp);
	}
	
	void idle() {
		glutPostRedisplay();
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_renderPass = nullptr;
		_renderMaterial = nullptr;
		_renderCanvas = nullptr;
		_camera = nullptr;
		_nodeManipulator = nullptr;
		_projectorMaterial = nullptr;
		_initVisitor = nullptr;
		vwgl::Loader::destroy();
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
	
	void mouse(int button, int state, int x, int y) {
		unsigned int keyModifiers = glutGetModifiers();
		
		if (state==oglut::Mouse::kDownState) {
			// ObjectGLUT PC/Mac version doesn't track the mouseWheel events, so, we use the alt key to emulate the middle button
			// to support users who haven't a three button mouse (Apple Magic Mouse, laptop trackpads etc.)
			if (button==oglut::Mouse::kLeftButton && (keyModifiers & GLUT_ACTIVE_ALT)) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
			else if (button==oglut::Mouse::kLeftButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationRotate);
			}
			else if (button==oglut::Mouse::kRightButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationDrag);
			}
			else if (button==oglut::Mouse::kMiddleButton) {
				_nodeManipulator->mouseDown(vwgl::Position2Di(x,y), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
		}
	}
	
	void motion(int x, int y) {
		_nodeManipulator->mouseMove(vwgl::Position2Di(x,y));
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

