#include <iostream>
#include <vwgl/vwgl.hpp>
#include <oglut/oglut.h>

class MyApp : public oglut::Application {
public:
	void initGL() {
		vwgl::OpenGL::init();
		
		vwgl::Loader::get()->registerReader(new vwgl::VwgltLoader());
		vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader());
		vwgl::Loader::get()->registerWritter(new vwgl::VwglbWritter());
		vwgl::Loader::get()->registerReader(new vwgl::ColladaLoader());
		
		loadGizmo();
		createSceneSet();
		createRenderers();
		createScene();
	}
	
	void loadGizmo() {
		_gizmo = new vwgl::PlaneGizmo();
		_colorPicker = new vwgl::ColorPicker();
	}
	
	void createSceneSet() {
		_sceneSet = new vwgl::SceneSet();
		_sceneSet->buildDefault();
	}
	
	void createRenderers() {
	//	_renderer = new vwgl::ForwardRenderer();
		_renderer = new vwgl::DeferredRenderer();
		_renderer->build(_sceneSet->getSceneRoot(), 1024, 1024);
		_renderer->setClearColor(vwgl::Color::black());
	}
	
	void createScene() {
		std::string resourcesPath = vwgl::System::get()->getResourcesPath();
		glEnable(GL_DEPTH_TEST);
		
		vwgl::Group * root = _sceneSet->getSceneRoot();
		
		vwgl::Drawable * star = vwgl::Loader::get()->loadDrawable(resourcesPath + "star.vwglb");
		vwgl::TransformNode * starTrx = new vwgl::TransformNode();
		starTrx->addChild(new vwgl::DrawableNode(star));
		root->addChild(starTrx);
		_colorPicker->assignPickId(star);
		
		vwgl::Drawable * sphere = new vwgl::Sphere(0.5,40,40);
		_cubeMap = new vwgl::DynamicCubemap();
		_cubeMap->createCubemap(vwgl::Size2Di(512));
		vwgl::GenericMaterial * sphereMat = new vwgl::GenericMaterial();
		sphereMat->setCubeMap(_cubeMap->getCubeMap());
		sphereMat->setReflectionAmount(0.9);
		sphere->setMaterial(sphereMat);
		vwgl::TransformNode * sphereTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(2.0, 1.0, 0.0));
		sphereTrx->addChild(new vwgl::DrawableNode(sphere));
		sphereTrx->addChild(_cubeMap.getPtr());
		root->addChild(sphereTrx);
		_colorPicker->assignPickId(sphere);
		
		vwgl::Drawable * floor = new vwgl::Plane(10);
		vwgl::GenericMaterial * floorMat = new vwgl::GenericMaterial();
		
		floorMat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg"));
		floorMat->setTextureScale(vwgl::Vector2(5.0));
		floorMat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm_2.png"));
		floorMat->setNormalMapScale(vwgl::Vector2(5.0));
		floorMat->setShininess(50.0);
		floorMat->setReceiveProjections(true);
		floor->setMaterial(floorMat);
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode();
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		root->addChild(floorTrx);
		
		vwgl::TransformNode * cameraNode = _sceneSet->getMainCameraTransform();
		_nodeManipulator = new vwgl::MouseTargetManipulator(cameraNode);
		_nodeManipulator->setCenter(vwgl::Vector3(0.0,1.0,0.0));
		_nodeManipulator->setTransform();
	}
	
	void display() {
		//vwgl::DynamicCubemapManager::get()->update(_sceneSet->getSceneRoot());
		vwgl::DynamicCubemapManager::get()->updateAllCubemaps(_sceneSet->getSceneRoot());
		
		_renderer->draw();
		
		this->swapBuffers();
	}
	
	void reshape(int w, int h) {
		vwgl::Viewport vp = vwgl::Viewport(0,0,w,h);
		_sceneSet->getMainCamera()->getProjectionMatrix().perspective(45.0, vp.aspectRatio(), 0.1, 100.0);
		_renderer->setViewport(vp);
		
		vwgl::DeferredRenderer * rend = dynamic_cast<vwgl::DeferredRenderer*>(_renderer.getPtr());
		if (rend) {
			rend->resize(vwgl::Size2Di(w,h));
		}
	}
	
	void idle() {
		glutPostRedisplay();
	}
	
	void mouse(int button, int state, int x, int y) {
		unsigned int keyModifiers = glutGetModifiers();
		vwgl::Position2Di pos(x,y);
		
		if (state==oglut::Mouse::kDownState) {
			vwgl::Viewport vp = vwgl::State::get()->getViewport();
			_colorPicker->pick(_sceneSet->getSceneRoot(), x, vp.height() - y, vp.width(), vp.height(),
							   vwgl::CameraManager::get()->getMainCamera());
			
			if (vwgl::GizmoManager::get()->checkItemPicked(_colorPicker->getSolid())) {
				_doPick = false;
				_doManipulate = true;
				vwgl::GizmoManager::get()->beginMouseEvents(vwgl::Position2Di(x,vp.height() - y), vwgl::CameraManager::get()->getMainCamera(), vp);
			}
			else {
				_doPick = true;
				_doManipulate = false;
				if (button==0 && keyModifiers==0) _nodeManipulator->mouseDown(pos, vwgl::MouseTargetManipulator::kManipulationRotate);
				else if (button==0 && (keyModifiers | GLUT_ACTIVE_ALT)) _nodeManipulator->mouseDown(pos, vwgl::MouseTargetManipulator::kManipulationDrag);
				else if (button==2) _nodeManipulator->mouseDown(pos, vwgl::MouseTargetManipulator::kManipulationZoom);
			}
		}
		else if (state==oglut::Mouse::kUpState && _doManipulate) {
			_doManipulate = false;
			vwgl::GizmoManager::get()->endMouseEvents();
		}
		else if (state==oglut::Mouse::kUpState && _doPick) {
			if (_selectedSolid.valid()) {
				vwgl::GenericMaterial * mat = _selectedSolid->getGenericMaterial();
				if (mat) {
					mat->setSelectedMode(false);
				}
			}
			_selectedSolid = NULL;
			_selectedDrawable = NULL;
			
			
			if ((_selectedDrawable = _colorPicker->getDrawable()).valid()) {
				vwgl::DrawableNode * drw = _selectedDrawable->getParent();
				vwgl::TransformNode * trx = dynamic_cast<vwgl::TransformNode*>(drw->getParent());
				if (trx) {
					vwgl::GizmoManager::get()->enableGizmo(trx, _gizmo.getPtr());
				}
			}
			else {
				vwgl::GizmoManager::get()->disableGizmo();
			}
			if ((_selectedSolid=_colorPicker->getSolid()).valid()) {
				vwgl::GenericMaterial * mat = _selectedSolid->getGenericMaterial();
				if (mat) {
					mat->setSelectedMode(true);
				}
			}
		}
	}
	
	void motion(int x, int y) {
		_doPick = false;
		if (_doManipulate) {
			vwgl::Viewport vp = vwgl::State::get()->getViewport();
			vwgl::Position2Di pos = vwgl::Position2Di(x, vp.height() - y);
			vwgl::GizmoManager::get()->mouseEvent(pos);
		}
		else {
			vwgl::Position2Di pos = vwgl::Position2Di(x,y);
			_nodeManipulator->mouseMove(pos);
		}
	}
	
	void keyboard(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
	
	void destroy() {
		vwgl::State::destroy();
		vwgl::Loader::destroy();
		vwgl::GizmoManager::destroy();
		_nodeManipulator = NULL;
		_sceneSet = NULL;
		_renderer = NULL;
		_cubeMap = NULL;
		_gizmo = NULL;
		_colorPicker = NULL;
		_selectedDrawable = NULL;
		_selectedSolid = NULL;
	}
	
protected:
	vwgl::ptr<vwgl::MouseTargetManipulator> _nodeManipulator;
	vwgl::ptr<vwgl::SceneSet> _sceneSet;
	vwgl::ptr<vwgl::Renderer> _renderer;
	
	vwgl::ptr<vwgl::DynamicCubemap> _cubeMap;
	
	vwgl::ptr<vwgl::Gizmo> _gizmo;
	vwgl::ptr<vwgl::ColorPicker> _colorPicker;
	bool _doPick;
	bool _doManipulate;
	vwgl::ptr<vwgl::Drawable> _selectedDrawable;
	vwgl::ptr<vwgl::Solid> _selectedSolid;
};

int main(int argc, char * argv[]) {
	oglut::MainLoop * mainLoop = oglut::MainLoop::singleton();
	mainLoop->setWindow(new oglut::Window());
	
	
	return mainLoop->run(argc, argv, new MyApp());
}
