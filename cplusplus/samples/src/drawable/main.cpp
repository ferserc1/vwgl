
#include <iostream>
#include <oglut/oglut.h>
#include <vwgl/vwgl.hpp>

class BasicMaterial : public vwgl::Material {
public:
	BasicMaterial() :Material() { initShader(); }
	BasicMaterial(const std::string & name) :Material(name) { initShader(); }
	
	virtual void initShader() {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "basic_color.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "basic_color.fsh");
		getShader()->link("Basic_color_material");
		
		loadVertexAttrib("aVertexPosition");
		getShader()->initUniformLocation("uMVMatrix");
		getShader()->initUniformLocation("uPMatrix");
		getShader()->initUniformLocation("uColor");
		
		_initialized = true;
	}
	
	void setColor(const vwgl::Color & color) { _color = color; }
	const vwgl::Color & getColor() const { return _color; }
	vwgl::Color & getColor() { return _color; }
	
protected:
	virtual ~BasicMaterial() {}

	virtual void setupUniforms() {
		getShader()->setUniform("uMVMatrix", modelViewMatrix());
		getShader()->setUniform("uPMatrix", projectionMatrix());
		getShader()->setUniform("uColor", _color);
	}
	
	vwgl::Color _color;
};

class MyApp : public oglut::Application {
private:
	vwgl::ptr<vwgl::Drawable> _drawable;
	vwgl::Vector2 _rot;
	
	GLuint _vertexAttribLocation;
	
public:
	vwgl::PolyList * getPolyList() {
		vwgl::ptr<vwgl::PolyList> polyList = new vwgl::PolyList();
		
		polyList->addVertex(vwgl::Vector3(-1.0,-1.0, 1.0));	// 0
		polyList->addVertex(vwgl::Vector3( 1.0,-1.0, 1.0));	// 1
		polyList->addVertex(vwgl::Vector3( 1.0, 1.0, 1.0));	// 2
		polyList->addVertex(vwgl::Vector3(-1.0, 1.0, 1.0));	// 3
		polyList->addVertex(vwgl::Vector3(-1.0,-1.0,-1.0));	// 4
		polyList->addVertex(vwgl::Vector3( 1.0,-1.0,-1.0));	// 5
		polyList->addVertex(vwgl::Vector3( 1.0, 1.0,-1.0));	// 6
		polyList->addVertex(vwgl::Vector3(-1.0, 1.0,-1.0));	// 7
		
		return polyList.release();
	}

	void buildCube() {
		_drawable = new vwgl::Drawable();
		
		// Front face
		vwgl::PolyList * front = getPolyList();
		front->addTriangle(0,1,2);
		front->addTriangle(2,3,0);
		front->buildPolyList();
		_drawable->addSolid(new vwgl::Solid(front));
		
		// Right face
		vwgl::PolyList * right = getPolyList();
		right->addTriangle(1,5,6);
		right->addTriangle(6,2,1);
		right->buildPolyList();
		_drawable->addSolid(new vwgl::Solid(right));
		
		// Back face
		vwgl::PolyList * back = getPolyList();
		back->addTriangle(5,4,7);
		back->addTriangle(7,6,5);
		back->buildPolyList();
		_drawable->addSolid(new vwgl::Solid(back));
		
		// Left face
		vwgl::PolyList * left = this->getPolyList();
		left->addTriangle(4,0,3);
		left->addTriangle(3,7,4);
		left->buildPolyList();
		_drawable->addSolid(new vwgl::Solid(left));
		
		// Top face
		vwgl::PolyList * top = getPolyList();
		top->addTriangle(3,2,6);
		top->addTriangle(6,7,3);
		top->buildPolyList();
		_drawable->addSolid(new vwgl::Solid(top));
		
		// Bottom face
		vwgl::PolyList * bottom = getPolyList();
		bottom->addTriangle(4,5,1);
		bottom->addTriangle(1,0,4);
		bottom->buildPolyList();
		_drawable->addSolid(new vwgl::Solid(bottom));
	}
	
	void initGL() {
		std::string shaderPath = vwgl::System::get()->getExecutablePath();
		vwgl::System::get()->setDefaultShaderPath(shaderPath);
		
		glClearColor(0.2,0.5,1,1.0);
		glEnable(GL_DEPTH_TEST);
		
		buildCube();
		
		vwgl::Color colors [] = {
					  vwgl::Color::red(),
					  vwgl::Color::blue(),
					  vwgl::Color::green(),
					  vwgl::Color::yellow(),
					  vwgl::Color::violet(),
					  vwgl::Color::orange()
					  };
		for (int i=0;i<_drawable->getSolidList().size();++i) {
			BasicMaterial * mat = new BasicMaterial();
			mat->setColor(colors[i]);
			mat->setCullFace(false);
			_drawable->getSolidList()[i]->setMaterial(mat);
		}
		
		_rot = 0.0;
	}
	
	void display() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		vwgl::Viewport vp = vwgl::State::get()->getViewport();
		vwgl::State::get()->projectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		
		vwgl::State::get()->viewMatrix().identity().translate(0.0,0.0,-5.0);
		vwgl::State::get()->modelMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(_rot.x()),1,0,0)
			.rotate(vwgl::Math::degreesToRadians(_rot.y()),0,1,0);
		_rot.add(vwgl::Vector2(1));
		
		_drawable->draw();

		glutSwapBuffers();
	}
	
	void reshape(int width, int height) {
		vwgl::State::get()->setViewport(vwgl::Viewport(0,0,width,height));
	}
	
	void idle() {
		glutPostRedisplay();
	}
	
	void destroy() {
		_drawable = nullptr;
	}
	
	void keyboardUp(unsigned char key, int x, int y) {
		if (key==27) exit(0);
	}
};

int main(int argc, char * argv[]) {
	oglut::Window * win = new oglut::Window();
	win->setSize(800, 600);
	
	oglut::MainLoop::singleton()->setWindow(win);
    return oglut::MainLoop::singleton()->run(argc, argv, new MyApp());
}

