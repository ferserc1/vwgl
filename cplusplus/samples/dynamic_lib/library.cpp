
#include <vwgl/dynamic_library.hpp>

#include <iostream>

#include "library.hpp"

extern "C" {

VWGL_LIBRARY void sayHello() {
	std::cout << "Hello world from sample_dynamiclib" << std::endl;
}

}