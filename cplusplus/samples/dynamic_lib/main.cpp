
#include <vwgl/vwgl.hpp>

#include "library.hpp"

int main(int argc, char ** argv) {
	std::cout << "Searching libraries at the following paths: " << std::endl;
	vwgl::DynamicLibrary::searchPaths([](const std::string &path) {
		std::cout << path << std::endl;
	});

	vwgl::ptr<vwgl::DynamicLibrary> library = vwgl::DynamicLibrary::load("samplelib");

	if (!library.valid()) {
		std::cerr << "Library not found" << std::endl;
		return -1;
	}

	callback_t sayHello = reinterpret_cast<callback_t>(library->getProcAddress("sayHello"));
	if (!sayHello) {
		std::cerr << "No such symbol" << std::endl;
		return -1;
	}

	sayHello();

	library = nullptr;
	sayHello = nullptr;

	return 0;
}