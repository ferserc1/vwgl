 #include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

static std::string s_vshader =
"#version 150\n"
"in vec4 in_Position;\n"
"in vec4 in_Color;\n"

"uniform mat4 uMVMatrix;\n"
"uniform mat4 uPMatrix;\n"

"out vec4 ex_Color;\n"
"void main(void) {\n"
"	gl_Position = uPMatrix * uMVMatrix * in_Position;\n"
"	ex_Color = in_Color;\n"
"}\n";


static std::string s_fshader =
"#version 150\n"

"// precision highp float\n"

"in vec4 ex_Color;\n"
"out vec4 out_Color;\n"

"void main(void) {\n"
"	out_Color = ex_Color;\n"
"}\n";

class MyWindowController : public vwgl::app::WindowController {
public:
	
	virtual void initGL() {
		_rot = 0;
		vwgl::Graphics::get()->initContext();
		
		glClearColor(0.2f, 0.5f, 1.0f, 1.0f);
		glEnable(GL_DEPTH_TEST);


		_polyList = new vwgl::PolyList();
		
		_polyList->addVertex(vwgl::Vector3(-1.0f, -1.0f, 0.0f));
		_polyList->addVertex(vwgl::Vector3( 1.0f, -1.0f, 0.0f));
		_polyList->addVertex(vwgl::Vector3( 1.0f,  1.0f, 0.0f));
		_polyList->addVertex(vwgl::Vector3(-1.0f,  1.0f, 0.0f));
		
		_polyList->addColor(vwgl::Color::red());
		_polyList->addColor(vwgl::Color::green());
		_polyList->addColor(vwgl::Color::blue());
		_polyList->addColor(vwgl::Color::yellow());
		
		_polyList->addTriangle(0, 1, 2);
		_polyList->addTriangle(2, 3, 0);
		
		_polyList->buildPolyList();
		
		_shader = new vwgl::Shader();
		bool status = _shader->attachShader(vwgl::Shader::kTypeVertex, s_vshader);
		status = status && _shader->attachShader(vwgl::Shader::kTypeFragment, s_fshader);
		if (!status) {
			std::cerr << "Shader compilation error: " << _shader->getCompileError() << std::endl;
			exit(-1);
		}
		_shader->setOutputParameterName(vwgl::Shader::kOutTypeFragmentDataLocation, "out_Color");
		
		status = _shader->link();
		if (!status) {
			std::cerr << "Shader link error:" << _shader->getLinkError() << std::endl;
		}
		else {
			_vertexAttribLocation = _shader->getAttribLocation("in_Position");
			_colorAttribLocation = _shader->getAttribLocation("in_Color");
			_shader->initUniformLocation("uMVMatrix");
			_shader->initUniformLocation("uPMatrix");
		}
	}

	virtual void draw() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		vwgl::State::get()->projectionMatrix().perspective(45.0f, _viewport.aspectRatio(), 0.1f, 100.0f);
		vwgl::State::get()->viewMatrix().identity().translate(0.0f, 0.0f, -5.0f);
		vwgl::State::get()->modelMatrix().identity().rotate(vwgl::Math::degreesToRadians(_rot), 0.0f, 1.0f, 0.0f);
		++_rot;
		
		_shader->bind();
	
		_shader->bindBuffer(vwgl::Shader::kBindArrayBuffer, _polyList->getVertexBuffer());
		_shader->enableVertexAttribArray(_vertexAttribLocation);
		_shader->vertexAttribPointer(_vertexAttribLocation, 3, vwgl::Shader::kPointerTypeFloat, false, 0, nullptr);

		_shader->bindBuffer(vwgl::Shader::kBindArrayBuffer, _polyList->getColorBuffer());
		_shader->enableVertexAttribArray(_colorAttribLocation);
		_shader->vertexAttribPointer(_colorAttribLocation, 4, vwgl::Shader::kPointerTypeFloat, false, 0, nullptr);
				
		_shader->setUniform("uMVMatrix", vwgl::State::get()->modelViewMatrix());
		_shader->setUniform("uPMatrix", vwgl::State::get()->projectionMatrix());
		
		_polyList->drawElements();
		
		_shader->unbind();
		
		window()->glContext()->swapBuffers();
	}

	virtual void reshape(int w, int h) {
		glViewport(0,0,w,h);
		_viewport = vwgl::Viewport(0,0,w,h);
	}

	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
protected:
	vwgl::ptr<vwgl::PolyList> _polyList;
	vwgl::ptr<vwgl::Shader> _shader;
	float _rot;
	vwgl::Viewport _viewport;
	
	int _vertexAttribLocation;
	int _colorAttribLocation;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("A very basic test: polylist");

	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}

	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
