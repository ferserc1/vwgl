
#include <vwgl/vwgl.hpp>

/*
 *	This sample shows the usage of the InputMediator class to draw icon gizmos, manipulation gizmos and
 *	to manage the app::CommandManager to undo and redo commands
 *	Key topics:
 *		- gizmos
 *		- manipulation gizmos
 *		- input mediator
 *		- CommandManager: implementation of Memento and Command design pattern
 */
class MyWindowController :	public vwgl::app::WindowController {
public:
	MyWindowController() { }
	
	vwgl::scene::Node * getCube(const std::string & name, const vwgl::Color & color, const vwgl::Vector3 & position) {
		vwgl::scene::Node * cubeNode = new vwgl::scene::Node(name);
		cubeNode->addComponent(vwgl::scene::PrimitiveFactory::cube());
		cubeNode->addComponent(new vwgl::scene::Transform());
		vwgl::TRSTransformStrategy * trs = new vwgl::TRSTransformStrategy();
		trs->translate(position);
		cubeNode->getComponent<vwgl::scene::Transform>()->getTransform().setTransformStrategy(trs);
		cubeNode->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0)->setDiffuse(color);
		cubeNode->getComponent<vwgl::scene::Drawable>()->getPolyList(0)->setName(name);
		cubeNode->addComponent(new vwgl::manipulation::Selectable());
		return cubeNode;
	}
	
	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBModelReader());
		
		
		_sceneRoot = new vwgl::scene::Node();
		
		vwgl::scene::Node * cubeGroup = new vwgl::scene::Node();
		cubeGroup->addChild(getCube("Red cube", vwgl::Color::red(), vwgl::Vector3(-1.6f,0.0f,0.0f)));
		cubeGroup->addChild(getCube("Blue cube", vwgl::Color::blue(), vwgl::Vector3(0.0f,0.0f,0.0f)));
		vwgl::scene::Node * aCube = getCube("Yellow cube", vwgl::Color::yellow(), vwgl::Vector3(1.6f,0.0f,0.0f));
		vwgl::scene::InputChainJoint * joint = new vwgl::scene::InputChainJoint();
		joint->getJoint()->setOffset(vwgl::Vector3(0.5f, 0.0f, 0.0f));
		aCube->addComponent(joint);
		cubeGroup->addChild(aCube);
		_sceneRoot->addChild(cubeGroup);

		vwgl::scene::Node * camNode = new vwgl::scene::Node();
		camNode->addComponent(new vwgl::scene::Camera());
		camNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeRotation(vwgl::Math::degreesToRadians(25.0f), -1.0f, 0.0f, 0.0f)
														 .translate(0.0f, 0.0f, 7.0f)));
		camNode->addComponent(new vwgl::manipulation::TargetInputController());
		_sceneRoot->addChild(camNode);
		
		vwgl::scene::Node * lightNode = new vwgl::scene::Node("Light 1");
		lightNode->addComponent(new vwgl::scene::Light());
		lightNode->getComponent<vwgl::scene::Light>()->setDiffuse(vwgl::Color(1.0f,0.4f, 0.3f, 1.0f));
		lightNode->getComponent<vwgl::scene::Light>()->setType(vwgl::Light::kTypePoint);
		lightNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(2.0f, 1.0f, -2.0f)));
		lightNode->addComponent(new vwgl::manipulation::Selectable());
		_sceneRoot->addChild(lightNode);
		
		lightNode = new vwgl::scene::Node("Light 2");
		lightNode->addComponent(new vwgl::scene::Light());
		lightNode->getComponent<vwgl::scene::Light>()->setType(vwgl::scene::Light::kTypePoint);
		lightNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(-2.0f, 1.0f, -2.0f)));
		lightNode->addComponent(new vwgl::manipulation::Selectable());
		_sceneRoot->addChild(lightNode);
		
		lightNode = new vwgl::scene::Node("Light 3");
		lightNode->addComponent(new vwgl::scene::Light());
		lightNode->getComponent<vwgl::scene::Light>()->setType(vwgl::scene::Light::kTypeSpot);
		lightNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.1f, 0.1f, 2.5f)));
		lightNode->addComponent(new vwgl::manipulation::Selectable());
		_sceneRoot->addChild(lightNode);
		

		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathDeferred, _sceneRoot.getPtr());

		// Note: If the renderer changes during the application execution, it's important to tell about it to our InputMediator
		_inputMediator.setRenderer(_renderer.getPtr());
		
		_inputMediator.init();
		_renderer->init();
	}
	
	virtual void frame(float delta) {
		_renderer->frame(delta);
	}
	
	virtual void draw() {
		_renderer->update();
		_renderer->draw();
		_inputMediator.drawGizmos();
		
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		vwgl::Size2Di size(w, h);
		_renderer->resize(size);
		_inputMediator.resize(size);
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			destroy();
			vwgl::app::MainLoop::get()->quit(0);
		}

		// 3D Max/Maya/Modo gizmo hotkeys:
		else if (evt.keyboard().key() == vwgl::Keyboard::kKeyQ) {
			_inputMediator.setGizmo(vwgl::app::InputMediator::kGizmoNone);
		}
		else if (evt.keyboard().key() == vwgl::Keyboard::kKeyW) {
			_inputMediator.setGizmo(vwgl::app::InputMediator::kGizmoTranslate);
		}
		else if (evt.keyboard().key() == vwgl::Keyboard::kKeyE) {
			_inputMediator.setGizmo(vwgl::app::InputMediator::kGizmoRotate);
		}
		else if (evt.keyboard().key() == vwgl::Keyboard::kKeyR) {
			_inputMediator.setGizmo(vwgl::app::InputMediator::kGizmoScale);
		}
		
		// Undo/redo
		else if (evt.keyboard().key()==vwgl::Keyboard::kKeyBack) {
			_inputMediator.commandManager().undo();
		}
		else if (evt.keyboard().key()==vwgl::Keyboard::kKeyReturn) {
			_inputMediator.commandManager().redo();
		}

		else {
			_inputMediator.keyUp(evt);
		}
	}
	
	// Let the InputManager to manage the user input events
	virtual void keyDown(const vwgl::app::KeyboardEvent & evt) { _inputMediator.keyDown(evt); }
	virtual void mouseDown(const vwgl::app::MouseEvent & evt) { _inputMediator.mouseDown(evt); }
	virtual void mouseUp(const vwgl::app::MouseEvent & evt) { _inputMediator.mouseUp(evt); }
	virtual void mouseMove(const vwgl::app::MouseEvent & evt) { _inputMediator.mouseMove(evt); }
	virtual void mouseDrag(const vwgl::app::MouseEvent & evt) { _inputMediator.mouseDrag(evt); }
	virtual void mouseWheel(const vwgl::app::MouseEvent & evt) { _inputMediator.mouseWheel(evt); }

	void destroy() {
		vwgl::scene::Light::clearShadowMap();
		vwgl::TextureManager::get()->finalize();
	}
	
protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
	vwgl::app::InputMediator _inputMediator;

	virtual ~MyWindowController() { }
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);
	
	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	if (!window) {
		exit(-1);
	}
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("Sample Gizmo");
	window->create();
	
	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
