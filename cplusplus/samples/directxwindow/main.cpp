
#include <Windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>

#include <iostream>
#include <vwgl/vwgl.hpp>

//#include <vwgl/directx.hpp>

class MyWindowController : public vwgl::app::WindowController {
public:
	MyWindowController() :
		_red(0.0f),
		_green(0.0f),
		_blue(0.0f),
		_colormodr(1),
		_colormodg(2),
		_colormodb(3)
	{}

	virtual void initGL() {
		vwgl::Graphics::get()->initContext();

	}

	virtual void frame(float delta) {
		_red += static_cast<float>(_colormodr)* 0.00005f;
		_green += static_cast<float>(_colormodg)* 0.00005f;
		_blue += static_cast<float>(_colormodb)* 0.00005f;

		if (_red >= 1.0f || _red <= 0.0f) _colormodr *= -1;
		if (_green >= 1.0f || _green <= 0.0f) _colormodg *= -1;
		if (_blue >= 1.0f || _blue <= 0.0f) _colormodb *= -1;
	}

	virtual void draw() {
		D3DXCOLOR color(_red,_blue,_green,1.0);

		vwgl::app::DirectXContext * context = dynamic_cast<vwgl::app::DirectXContext*>(window()->context());
		if (context) {
			ID3D11DeviceContext * deviceContext = vwgl::native_cast<ID3D11DeviceContext*>(context->getDeviceContext());
			ID3D11RenderTargetView * renderTargetView = vwgl::native_cast<ID3D11RenderTargetView*>(context->getRenderTargetView());

			deviceContext->ClearRenderTargetView(renderTargetView, color);
		}

		window()->context()->swapBuffers();
	}

	virtual void reshape(int w, int h) {
		
	}

	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}

protected:
	float _red;
	float _green;
	float _blue;
	int _colormodr;
	int _colormodg;
	int _colormodb;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiDirectX);

	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	if (!window) {
		exit(-1);
	}
	window->setWindowController(new MyWindowController());

	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("DirecX Window test");
	window->create();

	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
	return 0;
}