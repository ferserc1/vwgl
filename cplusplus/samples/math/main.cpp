//
//  main.cpp
//  math
//
//  Created by Fernando Serrano Carpena on 08/08/14.
//
//

#include <iostream>
#include <vwgl/math.hpp>

int main(int argc, const char * argv[]) {
	std::cout << "Compute orthonormal vectors of three points:\n";
	
	vwgl::Vector3 Pa(-1.25,-1.25,-1.25);
	vwgl::Vector3 Pb( 1.25,-1.25,-1.25);
	vwgl::Vector3 Pc(-1.25, 1.25,-1.25);
	
	vwgl::Vector3 Vr, Vu, Vn;
	
	Vr = Pb - Pa;
	Vr.normalize();
	
	Vu = Pc - Pa;
	Vu.normalize();
	
	Vn = Vr ^ Vu;
	Vn.normalize();
	
	std::cout << "Pa:" << Pa.toString() << std::endl;
	std::cout << "Pb:" << Pb.toString() << std::endl;
	std::cout << "Pc:" << Pc.toString() << std::endl << std::endl;
	
	std::cout << "Result:" << std::endl;
	std::cout << "Vr:" << Vr.toString() << std::endl;
	std::cout << "Vu:" << Vu.toString() << std::endl;
	std::cout << "Vn:" << Vn.toString() << std::endl;
	
    return 0;
}
