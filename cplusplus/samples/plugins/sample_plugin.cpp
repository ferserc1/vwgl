
#include <vwgl/scene/component_plugin.hpp>
#include <vwgl/scene/scene_component.hpp>
#include <vwgl/scene/transform.hpp>

#include <iostream>


class RotationComponent : public vwgl::scene::SceneComponent {
public:
	RotationComponent() :_rotation(0.0f) {}
	
	virtual void frame(float delta) {
		if (transform()) {
			transform()->getTransform().getMatrix()
				.identity()
				.rotate(_rotation, 0.0f, 1.0f, 0.0f);
			_rotation += vwgl::Math::kPiOver4 * delta;
		}
	}

protected:
	virtual ~RotationComponent() {}
	
	float _rotation;
	
};

class MyComponentPlugin : public vwgl::scene::ComponentPlugin {
public:
	virtual void initComponents() {
		registerComponent<RotationComponent>("sampleplugin::RotationComponent");
	}

protected:
	virtual ~MyComponentPlugin() {}
};

extern "C" {

VWGL_LIBRARY vwgl::scene::ComponentPlugin * getComponentPlugin() {
	return new MyComponentPlugin();
}

}