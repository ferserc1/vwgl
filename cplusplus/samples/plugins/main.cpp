#include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

// In this case, will not unload the library until the application ends.
// The library pointer is defined globally to prevent unload it
vwgl::ptr<vwgl::DynamicLibrary> s_pluginLib;

class MyWindowController : public vwgl::app::WindowController {
public:
	
	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		
		vwgl::State::get()->setClearColor(vwgl::Color(0.2f,0.5f,1.0f,1.0f));
		vwgl::State::get()->enableDepthTest();

		_sceneRoot = new vwgl::scene::Node();
		
		vwgl::scene::Node * lightNode = new vwgl::scene::Node();
		vwgl::scene::Light * light = new vwgl::scene::Light();
		lightNode->addComponent(light);
		lightNode->addComponent(new vwgl::scene::Transform());
		lightNode->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(65.0f), 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(75.0f), -1.0f, 0.0f, 0.0f);
		_sceneRoot->addChild(lightNode);
		
		_cubeNode = new vwgl::scene::Node();
		_cubeNode->addComponent(new vwgl::scene::Transform());
		_cubeNode->addComponent(vwgl::scene::PrimitiveFactory::cube(1.0f,1.0f,1.0f));
		_sceneRoot->addChild(_cubeNode.getPtr());
		
		// Add a camera to the scene
		vwgl::scene::Node * cameraNode = new vwgl::scene::Node();
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform());
		cameraNode->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.rotate(vwgl::Math::kPiOver4, 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0)
			.translate(vwgl::Vector3(0.0f, 0.0f, 5.0f));
		
		_sceneRoot->addChild(cameraNode);
		
		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathForward, _sceneRoot.getPtr());
		_renderer->init();
	}
	
	virtual void frame(float delta) {
		_renderer->frame(delta);
	}
	
	virtual void draw() {
		_renderer->update();
		_renderer->draw();
		
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		_renderer->resize(vwgl::Size2Di(w,h));
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
		
		// Load the plugin (only once) when the user press the space bar.
		if (evt.keyboard().key() == vwgl::Keyboard::kKeySpace && !s_pluginLib.valid()) {
			s_pluginLib = vwgl::DynamicLibrary::load("sampleplugin");
			
			if (s_pluginLib.valid()) {
				_componentPlugin = vwgl::scene::ComponentPlugin::loadFromLibrary(s_pluginLib.getPtr());
			}
			
			if (_componentPlugin.valid()) {
				_componentPlugin->eachComponent([&](const std::string & name) {
					std::cout << "New component loaded from plugin: " << name << std::endl;
					vwgl::scene::Component * comp = vwgl::scene::ComponentRegistry::get()->instantiate(name);
					_cubeNode->addComponent(comp);
					comp->init();
				});

			}
		}
	}
	
protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Node> _cubeNode;
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
	vwgl::ptr<vwgl::scene::ComponentPlugin> _componentPlugin;
};

int main(int argc, char ** argv) {
	int status = 0;

	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

	{
		vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
		window->setWindowController(new MyWindowController());
		window->setSize(640, 480);
		window->setPosition(100, 100);
		window->setTitle("Plugins sample");
		
		if (!window || !window->create()) {
			std::cout << "Error creating window" << std::endl;
			exit(-1);
		}
		
		vwgl::app::MainLoop::get()->setWindow(window);
		status = vwgl::app::MainLoop::get()->run();
	}
	
	s_pluginLib = nullptr;	// Unload library after the application is finished
	
	return status;
}
