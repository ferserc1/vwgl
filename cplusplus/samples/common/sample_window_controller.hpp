#ifndef _SAMPLE_WINDOW_CONTROLLER_HPP_
#define _SAMPLE_WINDOW_CONTROLLER_HPP_

#include <vwgl/sceneset.hpp>
#include <vwgl/node_manipulator.hpp>
#include <vwgl/deferredrenderer.hpp>
#include <vwgl/forwardrenderer.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/graphics.hpp>


#include <vwgl/app/window_controller.hpp>
#include <vwgl/app/main_loop.hpp>

namespace sample {

class WindowController : public vwgl::app::WindowController {
public:
	enum RenderPath {
		kRenderPathForward,
		kRenderPathDeferred
	};
	
	WindowController() :_renderPath(kRenderPathDeferred ) {}

	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		glEnable(GL_DEPTH_TEST);
		
		registerPlugins();
		prepareScene();
		createRenderer();
	}
	
	virtual void frame(float delta) {
	}
	
	virtual void draw() {
		_renderer->draw();
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		vwgl::Viewport vp = vwgl::Viewport(0, 0, w, h);
		_renderer->setViewport(vp);
		_renderer->resize(vwgl::Size2Di(w, h));

		vwgl::Camera * cam = _sceneSet->getMainCamera();
		vwgl::OpticalProjectionMethod * method = dynamic_cast<vwgl::OpticalProjectionMethod*>(cam->getProjectionMethod());
		if (method) {
			method->setViewport(vp);
			method->updateMatrix();
		}
		else {
			cam->getProjectionMatrix().perspective(60.0f, vp.aspectRatio(), 0.1f, 100.0f);
		}
		
		
		_viewport = vp;
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
	virtual void mouseDown(const vwgl::app::MouseEvent & evt) {
		if (evt.mouse().getButtonStatus(vwgl::Mouse::kLeftButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationRotate);
		}
		else if (evt.mouse().getButtonStatus(vwgl::Mouse::kRightButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationDrag);
		}
		else if (evt.mouse().getButtonStatus(vwgl::Mouse::kMiddleButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationZoom);
		}
	}
	
	virtual void mouseDrag(const vwgl::app::MouseEvent & evt) {
		_cameraManipulator->mouseMove(evt.pos());
	}
	
	virtual void mouseWheel(const vwgl::app::MouseEvent & evt) {
		_cameraManipulator->mouseWheel(evt.delta());
	}
	
	inline void setRenderPath(RenderPath path) { _renderPath = path; }
	inline RenderPath getRenderPath() const { return _renderPath; }
	
protected:
	virtual ~WindowController() {}
	
	vwgl::ptr<vwgl::SceneSet> _sceneSet;
	vwgl::ptr<vwgl::MouseTargetManipulator> _cameraManipulator;
	vwgl::ptr<vwgl::Renderer> _renderer;
	RenderPath _renderPath;
	vwgl::Viewport _viewport;
	
	const vwgl::Viewport & viewport() const { return _viewport; }
	
	virtual void createRenderer() {
		if (getRenderPath()==kRenderPathDeferred) {
			_renderer = new vwgl::DeferredRenderer();
		}
		else {
			_renderer = new vwgl::ForwardRenderer();
		}
		_renderer->build(_sceneSet->getSceneRoot(), 1024, 1024);
		configureRenderer(_renderer.getPtr());
	}
	
	virtual void configureRenderer(vwgl::Renderer * renderer) {
	}
	
	virtual void registerPlugins() {
	}
	
	virtual void prepareScene() {
		_sceneSet = new vwgl::SceneSet();
		_sceneSet->buildDefault(vwgl::Size2Di(4096));
		vwgl::ShadowLight * sl = dynamic_cast<vwgl::ShadowLight*>(_sceneSet->getMainLight());
		if (sl) {
			sl->getProjectionMatrix().ortho(-6.0f, 6.0f, -6.0f, 6.0f, 0.5f, 90.0f);
			sl->setShadowStrength(0.5f);
		}
		
		createScene(_sceneSet.getPtr());
		
		_cameraManipulator = new vwgl::MouseTargetManipulator(_sceneSet->getMainCameraTransform());
		configureCameraManipulator(_cameraManipulator.getPtr());
		_cameraManipulator->setTransform();
	}
	
	virtual void configureCameraManipulator(vwgl::MouseTargetManipulator * manipulator) {
	}

	virtual void createScene(vwgl::SceneSet * sceneSet) {
	}
};

}

#endif
