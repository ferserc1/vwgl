#include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

class MyWindowController : public vwgl::app::WindowController {
public:
	MyWindowController(const std::string & scenePath) :_scenePath(scenePath) {}
	
	vwgl::scene::Node * createScene() {
		vwgl::scene::Node * sceneRoot = new vwgl::scene::Node("SceneRoot");
		
		vwgl::scene::Node * cameraNode = new vwgl::scene::Node("Camera Node");
		sceneRoot->addChild(cameraNode);
		vwgl::scene::Camera * camera = new vwgl::scene::Camera();
		cameraNode->addComponent(camera);
		cameraNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, 0.0f, 5.0f)));
		
		vwgl::scene::Node * group1 = new vwgl::scene::Node("Group 1");
		sceneRoot->addChild(group1);
		group1->addComponent(new vwgl::scene::Chain());
		vwgl::scene::Transform * transform1 = new vwgl::scene::Transform();
		transform1->getTransform().getMatrix().identity();
		transform1->getTransform().setTransformStrategy(new vwgl::TRSTransformStrategy());
		group1->addComponent(transform1);
		
		vwgl::scene::Drawable * sphere = vwgl::scene::PrimitiveFactory::sphere(1.0,40,40);
		group1->addComponent(sphere);
		sphere->getGenericMaterial(0)->setReflectionAmount(0.8f);
		
		vwgl::scene::Projector * shadow = new vwgl::scene::Projector();
		shadow->setTexture(vwgl::Loader::get()->loadTexture("round_shadow.jpg",true));
		shadow->getRawTransform().identity().rotate(vwgl::Math::kPiOver2, -1.0f, 0.0f, 0.0f);
		shadow->getProjection().ortho(-2.0f, 2.0f, -2.0f, 2.0f, 0.1f, 100.0f);
		group1->addComponent(new vwgl::scene::Cubemap());
		group1->addComponent(shadow);
		sceneRoot->addChild(group1);

		
		vwgl::scene::Node * n1 = new vwgl::scene::Node("n1");
		n1->addComponent(new vwgl::scene::InputChainJoint());
		n1->getComponent<vwgl::scene::InputChainJoint>()->getJoint()->setOffset(vwgl::Vector3(1.0f,2.0f,3.0f));
		n1->getComponent<vwgl::scene::InputChainJoint>()->getJoint()->setPitch(0.12f);
		
		sceneRoot->addChild(n1);
		
		vwgl::scene::Node * n2 = new vwgl::scene::Node("n2");
		n2->addComponent(new vwgl::scene::OutputChainJoint());
		n2->getComponent<vwgl::scene::OutputChainJoint>()->getJoint()->setOffset(vwgl::Vector3(2.0f,4.0f,6.0f));
		n2->getComponent<vwgl::scene::OutputChainJoint>()->getJoint()->setRoll(1.32f);
		
		sceneRoot->addChild(n2);
		
		
		vwgl::scene::Drawable * floor = vwgl::scene::PrimitiveFactory::plane(20,20);
		floor->getGenericMaterial(0)->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg",true));
		floor->getGenericMaterial(0)->setTextureScale(vwgl::Vector2(10,10));
		floor->getGenericMaterial(0)->setReceiveProjections(true);
		vwgl::scene::Node * floorNode = new vwgl::scene::Node("Floor node");
		floorNode->addComponent(floor);
		floorNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, -1.2f, 0.0f)));
		sceneRoot->addChild(floorNode);
		
		vwgl::scene::Node * light = new vwgl::scene::Node("Main light");
		light->addComponent(new vwgl::scene::Light());
		light->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeRotation(vwgl::Math::degreesToRadians(20.0f), -1.0f, 0.0f, 0.0f).translate(0.0f, 0.0f, 5.0f)));
		light->getComponent<vwgl::scene::Light>()->getProjectionMatrix().ortho(-6.0f, 6.0f, -6.0f, 6.0f, 0.1f, 1000.0f);
		sceneRoot->addChild(light);
		
		return sceneRoot;
	}

	virtual void initGL() {
		std::string scenePath = _scenePath;
		
		vwgl::Graphics::get()->initContext();
		
		// Required to load scenes and its resources
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBModelReader());
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBPrefabReader());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBModelWritter());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBPrefabWritter());
		vwgl::Loader::get()->registerReader(new vwgl::SceneReader());
		vwgl::Loader::get()->registerWritter(new vwgl::SceneWritter());

		// Optional: use it to load Collada files. The scenes only contains native models, so this plugin
		// is not required to load and save scenes
		vwgl::Loader::get()->registerReader(new vwgl::ColladaModelReader());
		
		
		vwgl::ptr<vwgl::scene::Node> loadScene = createScene();

		
		scenePath = vwgl::System::get()->addPathComponent(scenePath, "scene.vitbundle");
		vwgl::Loader::get()->write(scenePath, loadScene.getPtr());
		
		_sceneRoot = vwgl::Loader::get()->loadScene(scenePath);
		
		_sceneRoot->iterateChildren([&](vwgl::scene::Node * node) {
			vwgl::scene::InputChainJoint * inJoint = node->getComponent<vwgl::scene::InputChainJoint>();
			vwgl::scene::OutputChainJoint * outJoint = node->getComponent<vwgl::scene::OutputChainJoint>();
			if (inJoint) {
				std::cout << "inJoint:" << std::endl;
				std::cout << "Offset: " << inJoint->getJoint()->offset().toString() << std::endl;
				std::cout << "Rotation: " << inJoint->getJoint()->eulerRotation().x() << ","
										<< inJoint->getJoint()->eulerRotation().y() << ","
										<< inJoint->getJoint()->eulerRotation().z() << std::endl;
			}
			if (outJoint) {
				std::cout << "outJoint:" << std::endl;
				std::cout << "Offset: " << outJoint->getJoint()->offset().toString() << std::endl;
				std::cout << "Rotation: " << outJoint->getJoint()->eulerRotation().x() << ","
										<< outJoint->getJoint()->eulerRotation().y() << ","
										<< outJoint->getJoint()->eulerRotation().z() << std::endl;
			}
		});

		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathForward, _sceneRoot.getPtr());
		_renderer->init();
	}
	
	virtual void frame(float delta) {
		_renderer->frame(delta);
	}
	
	virtual void draw() {
		_renderer->update();
		_renderer->draw();
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		_renderer->resize(vwgl::Size2Di(w,h));
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			destroy();
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_cubeNode = nullptr;
		_renderer = nullptr;
		vwgl::scene::Light::clearShadowMap();
		vwgl::TextureManager::get()->finalize();
	}
	
protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Node> _cubeNode;
	
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
	
	std::string _scenePath;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGL);
	
	std::string scenePath = "/Users/fernando/Desktop";
	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController(scenePath));
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("Scene files test");
	
	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}
	
	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
