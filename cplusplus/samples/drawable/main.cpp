 #include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

static std::string s_vshader =
"#version 150\n"
"in vec4 in_Position;\n"
"in vec4 in_Color;\n"

"uniform mat4 uMVMatrix;\n"
"uniform mat4 uPMatrix;\n"

"out vec4 ex_Color;\n"
"void main(void) {\n"
"	gl_Position = uPMatrix * uMVMatrix * in_Position;\n"
"	ex_Color = in_Color;\n"
"}\n";


static std::string s_fshader =
"#version 150\n"

"// precision highp float\n"

"in vec4 ex_Color;\n"
"out vec4 out_Color;\n"

"void main(void) {\n"
"	out_Color = ex_Color;\n"
"}\n";

class BasicMaterial : public vwgl::Material {
public:
	BasicMaterial() {
		setReuseShaderKey("sample_basic_material");
		initShader();
	}
	
	virtual void initShader() {
		// Use getShader()->loadAndAttachShader(vwgl::Shader::ShaderType shaderType, const std::string &path) to load and attach shader text files
		getShader()->attachShader(vwgl::Shader::kTypeVertex, s_vshader);
		getShader()->attachShader(vwgl::Shader::kTypeFragment, s_fshader);
		getShader()->setOutputParameterName(vwgl::Shader::kOutTypeFragmentDataLocation, "out_Color");
		getShader()->link();
		
		loadVertexAttrib("in_Position");
		loadColorAttrib("in_Color");
		
		getShader()->initUniformLocation("uMVMatrix");
		getShader()->initUniformLocation("uPMatrix");
	}
	
protected:
	virtual ~BasicMaterial() {}
	
	virtual void setupUniforms() {
		getShader()->setUniform("uMVMatrix", vwgl::State::get()->modelViewMatrix());
		getShader()->setUniform("uPMatrix", vwgl::State::get()->projectionMatrix());
	}
};


class MyWindowController : public vwgl::app::WindowController {
public:
	vwgl::PolyList * getPolyList() {
		vwgl::ptr<vwgl::PolyList> polyList = new vwgl::PolyList();
		
		polyList->addVertex(vwgl::Vector3(-1.0,-1.0, 1.0));	// 0
		polyList->addVertex(vwgl::Vector3( 1.0,-1.0, 1.0));	// 1
		polyList->addVertex(vwgl::Vector3( 1.0, 1.0, 1.0));	// 2
		polyList->addVertex(vwgl::Vector3(-1.0, 1.0, 1.0));	// 3
		polyList->addVertex(vwgl::Vector3(-1.0,-1.0,-1.0));	// 4
		polyList->addVertex(vwgl::Vector3( 1.0,-1.0,-1.0));	// 5
		polyList->addVertex(vwgl::Vector3( 1.0, 1.0,-1.0));	// 6
		polyList->addVertex(vwgl::Vector3(-1.0, 1.0,-1.0));	// 7
		
		polyList->addColor(vwgl::Color::red());
		polyList->addColor(vwgl::Color::green());
		polyList->addColor(vwgl::Color::blue());
		polyList->addColor(vwgl::Color::yellow());
		polyList->addColor(vwgl::Color::brown());
		polyList->addColor(vwgl::Color::pink());
		polyList->addColor(vwgl::Color::orange());
		polyList->addColor(vwgl::Color::violet());
		
		return polyList.release();
	}

	void buildCube() {
		_drawable = new vwgl::scene::Drawable();
		vwgl::ptr<vwgl::Material> mat = new BasicMaterial();
		
		vwgl::PolyList * front = getPolyList();
		front->addTriangle(0, 1, 2);
		front->addTriangle(2, 3, 0);
		front->buildPolyList();
		_drawable->addPolyList(front, mat.getPtr());
		
		vwgl::PolyList * right = getPolyList();
		right->addTriangle(1, 5, 6);
		right->addTriangle(6, 2, 1);
		right->buildPolyList();
		_drawable->addPolyList(right,mat.getPtr());
		
		vwgl::PolyList * back = getPolyList();
		back->addTriangle(5, 4, 7);
		back->addTriangle(7, 6, 5);
		back->buildPolyList();
		_drawable->addPolyList(back,mat.getPtr());
		
		vwgl::PolyList * left = getPolyList();
		left->addTriangle(4, 0, 3);
		left->addTriangle(3, 7, 4);
		left->buildPolyList();
		_drawable->addPolyList(left,mat.getPtr());
		
		vwgl::PolyList * top = getPolyList();
		top->addTriangle(3, 2, 6);
		top->addTriangle(6, 7, 3);
		top->buildPolyList();
		_drawable->addPolyList(top,mat.getPtr());
		
		vwgl::PolyList * bottom = getPolyList();
		bottom->addTriangle(4, 5, 1);
		bottom->addTriangle(1, 0, 4);
		bottom->buildPolyList();
		_drawable->addPolyList(bottom,mat.getPtr());
		
	}
	
	virtual void initGL() {
		_rot.set(0.0f,0.0f);
		vwgl::Graphics::get()->initContext();
		
		glClearColor(0.2f, 0.5f, 1.0f, 1.0f);
		glEnable(GL_DEPTH_TEST);

		buildCube();
	}

	virtual void draw() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		vwgl::State::get()->projectionMatrix().perspective(45.0f, _viewport.aspectRatio(), 0.1f, 100.0f);
		vwgl::State::get()->viewMatrix().identity().translate(0.0f, 0.0f, -5.0f);
		vwgl::State::get()->modelMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(_rot.x()), 1.0f, 0.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(_rot.y()), 0.0f, 1.0f, 0.0f);
		_rot.add(vwgl::Vector2(1.0,1.0));
		
		_drawable->draw();
	
		window()->glContext()->swapBuffers();
	}

	virtual void reshape(int w, int h) {
		glViewport(0,0,w,h);
		_viewport = vwgl::Viewport(0,0,w,h);
	}

	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
protected:
	vwgl::ptr<vwgl::scene::Drawable> _drawable;
	vwgl::ptr<vwgl::GenericMaterial> _material;
	vwgl::Vector2 _rot;
	vwgl::Viewport _viewport;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("A very basic test: drawable");

	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}

	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
