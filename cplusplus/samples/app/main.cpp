#include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

class MyWindowController : public vwgl::app::WindowController {
public:
	virtual void initGL() {
		glEnable(GL_DEPTH_TEST);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	}

	virtual void frame(float delta) {
	}

	virtual void draw() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		window()->glContext()->swapBuffers();
	}

	virtual void reshape(int w, int h) {
		std::cout << "Reshape: " << vwgl::Vector2(static_cast<float>(w),static_cast<float>(h)).toString() << std::endl;
	}

	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}

	virtual void keyDown(const vwgl::app::KeyboardEvent & evt) {
		std::cout << "Key Down" << std::endl;
	}

	virtual void mouseDown(const vwgl::app::MouseEvent & evt) {
		std::cout << "MouseDown" << std::endl;
	}

	virtual void mouseUp(const vwgl::app::MouseEvent & evt) {
		std::cout << "MouseUp" << std::endl;
	}

	virtual void mouseMove(const vwgl::app::MouseEvent & evt) {
		std::cout << "Mouse move: " << evt.pos().toString() << std::endl;
	}

	virtual void mouseDrag(const vwgl::app::MouseEvent & evt) {
		std::cout << "Mouse drag: " << evt.pos().toString() << std::endl;
	}

	virtual void mouseWheel(const vwgl::app::MouseEvent & evt) {
		std::cout << "Mouse wheel: " << evt.delta().x() << std::endl;
	}
};

int main(int argc, char ** argv) {
	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("Test OpenGL Window");

	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}

	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
