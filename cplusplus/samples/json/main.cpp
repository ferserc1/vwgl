//
//  main.cpp
//  math
//
//  Created by Fernando Serrano Carpena on 08/08/14.
//
//

#include <iostream>
#include <vwgl/vwgl.hpp>

#include <iostream>

using namespace vwgl;
void tab(int tabs) {
	for (int i=0; i<tabs; ++i) {
		std::cout << "\t";
	}
}

void printObject(Dictionary * value,int tabs = 0);

void printArray(Dictionary * obj,int tabs = 0) {
	Dictionary::DictionaryArray::iterator it;
	for (it=obj->getArray().begin(); it!=obj->getArray().end(); ++it) {
		Dictionary * value = (*it).getPtr();
		printObject(value, tabs);
	}
}

void printObjectMap(Dictionary * obj, int tabs=0) {
	Dictionary::DictionaryMap::iterator it;
	for (it=obj->getObject().begin(); it!=obj->getObject().end(); ++it) {
		std::string key = it->first;
		Dictionary * value = it->second.getPtr();
		tab(tabs);
		std::cout << key << ":" << std::endl;
		printObject(value,tabs + 1);
	}
}

void printObject(Dictionary * value,int tabs) {
	switch (value->getType()) {
		case Dictionary::kTypeString:
			tab(tabs);
			std::cout << value->getString() << std::endl;
			break;
		case Dictionary::kTypeBool:
			tab(tabs);
			std::cout << (value->getBoolean() ? "true":"false") << std::endl;
			break;
		case Dictionary::kTypeNumber:
			tab(tabs);
			std::cout << value->getNumber() << std::endl;
			break;
		case Dictionary::kTypeArray:
			printArray(value,tabs);
			break;
		case Dictionary::kTypeObject:
			printObjectMap(value, tabs);
			break;
		default:
			break;
	}
}

int main(int argc, const char * argv[]) {
	// Build a dictionary
	ptr<Dictionary> root = new Dictionary();
	
	root->getObject()["key1"] = new Dictionary("Value1");
	root->getObject()["key2"] = new Dictionary();
	root->getObject()["key2"]->getObject()["key21"] = new Dictionary("object 1");
	root->getObject()["key2"]->getObject()["key22"] = new Dictionary();
	root->getObject()["key2"]->getObject()["key22"]->getArray().push_back(new Dictionary("array value 1"));
	root->getObject()["key2"]->getObject()["key22"]->getArray().push_back(new Dictionary("array value 2"));
	root->getObject()["key3"] = new Dictionary(true);
	root->getObject()["key4"] = new Dictionary(false);
	root->getObject()["key5"] = new Dictionary(12);
	root->getObject()["key6"] = new Dictionary(33.5);

	// Register the json dictioary writter (only once in the application life cycle):
	vwgl::Loader::get()->registerWritter(new vwgl::DictionaryWritter());
	
	// Build the output file path
	std::string filePath = vwgl::System::get()->getExecutablePath();
	filePath = vwgl::System::get()->addPathComponent(filePath, "dictionary.json");
	
	// Write the dictionary to a json file
	vwgl::Loader::get()->writeDictionary(filePath, root.getPtr());
	
	// Register the json dictionary reader:
	vwgl::Loader::get()->registerReader(new vwgl::DictionaryLoader());
	// Read the file again
	ptr<Dictionary> readedDictionary = vwgl::Loader::get()->loadDictionary(filePath);
	
	if (readedDictionary.valid()) {
		// Print the dictionary
		printObject(readedDictionary.getPtr());
	}
	
    return 0;
}
