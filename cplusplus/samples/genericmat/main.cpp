 #include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

class MyWindowController : public vwgl::app::WindowController {
public:
	
	virtual void initGL() {
		_rot = 0;
		vwgl::Graphics::get()->initContext();
		
		glClearColor(0.2f, 0.5f, 1.0f, 1.0f);
		glEnable(GL_DEPTH_TEST);
		
		vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader());
		
		std::string resPath = vwgl::System::get()->getResourcesPath("test_shape.vwglb");
		_drawable = vwgl::Loader::get()->loadDrawable(resPath);
		
		// The generic material requires one light
		_mainLight = new vwgl::Light();
		
		_material = new vwgl::GenericMaterial();
		
		_material->setDiffuse(vwgl::Color(1.0f,0.5f,0.3f,1.0f));
		_material->setSpecular(vwgl::Color(0.5f,0.5f,1.0f,1.0f));
		_material->setShininess(10.0f);
		
		_material->setTexture(vwgl::Loader::get()->loadTexture(vwgl::System::get()->getResourcesPath("bricks.jpg")));
		_material->setNormalMap(vwgl::Loader::get()->loadTexture(vwgl::System::get()->getResourcesPath("bricks_nm.png")));
		
		// Some properties does not work in forward render path (see deferred render sample)
		
		_drawable->setMaterial(_material.getPtr());
	}

	virtual void draw() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		vwgl::State::get()->projectionMatrix().perspective(45.0f, _viewport.aspectRatio(), 0.1f, 100.0f);
		vwgl::State::get()->viewMatrix().identity().translate(0.0f, 0.0f, -5.0f);
		vwgl::State::get()->modelMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(_rot.x()), 1.0f, 0.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(_rot.y()), 0.0f, 1.0f, 0.0f);
		_rot.add(vwgl::Vector2(1.0,1.0));
		
		vwgl::LightManager::get()->prepareFrame();
		
		_drawable->draw();
	
		window()->glContext()->swapBuffers();
	}

	virtual void reshape(int w, int h) {
		glViewport(0,0,w,h);
		_viewport = vwgl::Viewport(0,0,w,h);
	}

	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
protected:
	vwgl::ptr<vwgl::Drawable> _drawable;
	vwgl::ptr<vwgl::GenericMaterial> _material;
	vwgl::ptr<vwgl::Light> _mainLight;
	vwgl::Vector2 _rot;
	vwgl::Viewport _viewport;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("A very basic test: drawable");

	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}

	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
