#include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

class CameraRotation : public vwgl::scene::Component {
public:
	CameraRotation() :_rot(0.0f) {}

	virtual void frame(float delta) {
		vwgl::scene::Transform * trx = sceneObject()->getComponent<vwgl::scene::Transform>();

		if (trx) {
			trx->getTransform().getMatrix()
				.identity()
				.rotate(vwgl::Math::degreesToRadians(_rot), 0.0f, 1.0f, 0.0f)
				.rotate(vwgl::Math::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0f)
				.translate(0.0f, 0.0f, 8.0f);
		}
		--_rot;
	}

protected:
	virtual ~CameraRotation() {}

	float _rot;
};


class MyWindowController : public vwgl::app::WindowController {
public:
	
	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBModelReader());
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBPrefabReader());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBModelWritter());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBPrefabWritter());

		vwgl::Loader::get()->registerReader(new vwgl::ColladaModelReader());

		_sceneRoot = new vwgl::scene::Node();
		
		vwgl::scene::Light::setShadowMapSize(vwgl::Size2Di(2048));

		
		vwgl::scene::Node * lightNode = new vwgl::scene::Node();
		vwgl::scene::Light * light = new vwgl::scene::Light();
		light->getProjectionMatrix().ortho(-6.0f, 6.0f, -6.0f, 6.0f, 0.5f, 90.0f);
		lightNode->addComponent(light);
		lightNode->addComponent(new vwgl::scene::Transform());
		lightNode->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(-20.0f), 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(-67.0f), 1.0f, 0.0f, 0.0f)
			.translate(0.0f, 0.0f, 5.0f);
		_sceneRoot->addChild(lightNode);
		
		vwgl::scene::Node * floorNode = new vwgl::scene::Node();
		floorNode->addComponent(vwgl::scene::PrimitiveFactory::plane(15.0f, 15.0f));
		floorNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, -1.0f, 0.0f)));
		_sceneRoot->addChild(floorNode);
		vwgl::GenericMaterial * mat = floorNode->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0);
		mat->setReceiveProjections(true);
		mat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg", true));
		mat->setTextureScale(vwgl::Vector2(4.0f, 4.0f));
		mat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png", true));
		mat->setNormalMapScale(vwgl::Vector2(4.0f, 4.0f));
		mat->setShininess(50.0f);
		mat->setSpecular(vwgl::Color::white());
		
		vwgl::scene::Node * sphere = new vwgl::scene::Node();
		sphere->addComponent(vwgl::scene::PrimitiveFactory::sphere(1.1f));
		vwgl::scene::Cubemap * cubemap = new vwgl::scene::Cubemap();
		sphere->addComponent(cubemap);

		_sceneRoot->addChild(sphere);
		mat = sphere->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0);
		mat->setCubeMap(cubemap->getCubeMap());
		mat->setReflectionAmount(0.4f);
		sphere->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(2.0f, 0.0f, 0.0f)));
	
		vwgl::scene::Node * cube = new vwgl::scene::Node();
		cube->addComponent(vwgl::scene::PrimitiveFactory::cube(1.0));
		cube->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(-2.0f, 0.0f, 0.0f)));
		_sceneRoot->addChild(cube);

		vwgl::scene::Node * cameraNode = new vwgl::scene::Node();
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform());
		cameraNode->addComponent(new CameraRotation());
		_sceneRoot->addChild(cameraNode);

		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathDeferred, _sceneRoot.getPtr());
		
		_renderer->setClearColor(vwgl::Color::blue());
		
		_renderer->init();
	}
	
	virtual void frame(float delta) {
		_renderer->frame(delta);
	}
	
	virtual void draw() {
		_renderer->update();
		_renderer->draw();
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		_renderer->resize(vwgl::Size2Di(w,h));
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			destroy();
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_renderer = nullptr;
		vwgl::scene::Light::clearShadowMap();
		vwgl::TextureManager::get()->finalize();
	}
	
protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);
	
	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("Cubemap test");
	
	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}
	
	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
