#include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

class ObjectRotation : public vwgl::scene::Component {
public:
	ObjectRotation() :_rotY(0.0f), _rotX(22.5f), _incrementY(0.0f), _incrementX(0.0f) {}

	virtual void frame(float delta) {
		vwgl::scene::Transform * trx = sceneObject()->getComponent<vwgl::scene::Transform>();

		if (trx) {
			trx->getTransform().getMatrix()
				.identity()
				.rotate(vwgl::Math::degreesToRadians(_rotY), 0.0f, 1.0f, 0.0f)
				.rotate(vwgl::Math::degreesToRadians(_rotX), -1.0f, 0.0f, 0.0f);
		}
		_rotX += _incrementX;
		_rotY += _incrementY;
	}
	
	virtual void keyDown(const vwgl::app::KeyboardEvent & event) {
		if (event.keyboard().key()==vwgl::Keyboard::kKeyLeft) {
			_incrementY = 2.0f;
		}
		if (event.keyboard().key()==vwgl::Keyboard::kKeyRight) {
			_incrementY = -2.0f;
		}
		if (event.keyboard().key()==vwgl::Keyboard::kKeyUp) {
			_incrementX = 2.0f;
		}
		if (event.keyboard().key()==vwgl::Keyboard::kKeyDown) {
			_incrementX = -2.0f;
		}
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & event) {
		if (event.keyboard().key()==vwgl::Keyboard::kKeyLeft ||
			event.keyboard().key()==vwgl::Keyboard::kKeyRight) {
			_incrementY = 0.0f;
		}
		if (event.keyboard().key()==vwgl::Keyboard::kKeyUp ||
			event.keyboard().key()==vwgl::Keyboard::kKeyDown) {
			_incrementX = 0.0f;
		}
	}

protected:
	virtual ~ObjectRotation() {}

	float _rotY;
	float _rotX;
	float _incrementY;
	float _incrementX;
};


class MyWindowController : public vwgl::app::WindowController {
public:
	
	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBModelReader());
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBPrefabReader());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBModelWritter());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBPrefabWritter());

		vwgl::Loader::get()->registerReader(new vwgl::ColladaModelReader());

		_sceneRoot = new vwgl::scene::Node();
		
		vwgl::scene::Node * bigCube = new vwgl::scene::Node();
		bigCube->addComponent(vwgl::scene::PrimitiveFactory::cube(10.0,10.0,10.0));
		bigCube->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(15.0f, 5.0f, 15.0f)
														 .rotate(vwgl::Math::degreesToRadians(45.0f), 0.0f, 1.0f, 0.0f)));
		_sceneRoot->addChild(bigCube);

		vwgl::scene::Node * testLoadNode = vwgl::Loader::get()->loadPrefab("test_shape.vwglb", true);
		_sceneRoot->addChild(testLoadNode);
		
		vwgl::scene::Node * lightNode = new vwgl::scene::Node();
		vwgl::scene::Light * light = new vwgl::scene::Light();
		light->setShadowStrength(0.8f);
		lightNode->addComponent(light);
		lightNode->addComponent(new vwgl::scene::Transform());
		lightNode->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(-20.0f), 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(-67.0f), 1.0f, 0.0f, 0.0f)
			.translate(0.0f, 0.0f, 5.0f);
		_sceneRoot->addChild(lightNode);
		
		vwgl::scene::Node * floorNode = new vwgl::scene::Node();
		floorNode->addComponent(vwgl::scene::PrimitiveFactory::plane(35.0f, 35.0f));
		floorNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, -1.0f, 0.0f)));
		_sceneRoot->addChild(floorNode);
		vwgl::GenericMaterial * mat = floorNode->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0);
		mat->setReceiveProjections(true);
		// loadTexture(path, loadFromResourcesFolder): the second parameter specify that the
		// texture file is located in the standard resource directory:
		//		Windows: resources folder, located at the same path as the .exe
		//		OS X: [Application Bundle].app/Contents/Resources folder
		//		iOS: the apk resources folder
		mat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg", true));
		mat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png", true));
		mat->setTextureScale(vwgl::Vector2(20.0f,20.0f));
		mat->setNormalMapScale(vwgl::Vector2(20.0f,20.0f));
		mat->setShininess(50.0f);
		mat->setSpecular(vwgl::Color::white());
		
		vwgl::scene::Node * sphere = new vwgl::scene::Node();
		sphere->addComponent(vwgl::scene::PrimitiveFactory::sphere(1.1f));
		_sceneRoot->addChild(sphere);
		mat = sphere->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0);
		mat->setDiffuse(vwgl::Color(0.9f, 0.9f, 0.9f, 1.0f));
		mat->setSpecular(vwgl::Color::white());
		mat->setShininess(30.0f);
		mat->setTexture(vwgl::Loader::get()->loadTexture("alpha.png", true));
		mat->setCullFace(false);
		mat->setDiffuse(vwgl::Color(1.0f, 1.0f, 1.0f, 1.0f));
		sphere->addComponent(new ObjectRotation());
		sphere->addComponent(new vwgl::scene::Transform());
		
		vwgl::scene::Node * cameraNode = new vwgl::scene::Node();
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform());
		cameraNode->addComponent(new vwgl::manipulation::TargetInputController());

		cameraNode->getComponent<vwgl::manipulation::TargetInputController>()->setForwardKey(vwgl::Keyboard::kKeyW);
		cameraNode->getComponent<vwgl::manipulation::TargetInputController>()->setBackwardKey(vwgl::Keyboard::kKeyS);
		cameraNode->getComponent<vwgl::manipulation::TargetInputController>()->setRightKey(vwgl::Keyboard::kKeyA);
		cameraNode->getComponent<vwgl::manipulation::TargetInputController>()->setLeftKey(vwgl::Keyboard::kKeyD);
		_sceneRoot->addChild(cameraNode);
		
		

		// The scene renderer will handle all the initialization, animation, drawing and reshaping process
		// for us. A scene renderer can also handle postprocess effects and advanced rendering techniques, suc
		// as deferred rendering, automatically and in a transparent way.
		// Configure renderer: set the scene root
		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathDeferred, _sceneRoot.getPtr());
		
		vwgl::scene::Light::setShadowMapSize(vwgl::Size2Di(1024));

		vwgl::ShadowRenderPassMaterial * shadows = _renderer->getFilter<vwgl::ShadowRenderPassMaterial>();
		if (shadows) {
			shadows->setShadowType(vwgl::ShadowRenderPassMaterial::kShadowTypeSoft);
			shadows->setBlurIterations(3);
			shadows->setShadowCascades(3);
		}

		vwgl::SSAOMaterial * ssao = _renderer->getFilter<vwgl::SSAOMaterial>();
		if (ssao) {
			ssao->setEnabled(true);
			ssao->setKernelSize(16);
			ssao->setSampleRadius(0.32f);
			ssao->setMaxDistance(7.0f);
		}
		
		vwgl::BloomMapMaterial * bloom = _renderer->getFilter<vwgl::BloomMapMaterial>();
		if (bloom) {
			bloom->setBloomAmount(4);
		}
		
		vwgl::DeferredLighting * lighting = _renderer->getFilter<vwgl::DeferredLighting>();
		if (lighting) {
			lighting->setExposure(1.3f);
		}
		// You can change the scene at any time using:
		//_renderer->setSceneRoot(_sceneRoot.getPtr());
		
		// Configure other renderer settings
		_renderer->setClearColor(vwgl::Color::blue());

		
		// Init scene:
		_renderer->init();
	}
	
	virtual void frame(float delta) {
		// Animate using the scene renderer
		_renderer->frame(delta);
	}
	
	virtual void draw() {
		// Update before draw. This will prepare some elements, such as dynamic cubemaps or relfectors
		_renderer->update();

		// Draw using the scene renderer
		_renderer->draw();
		
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		// The renderer will handle the viewport and camera settings for us with
		// one single call
		_renderer->resize(vwgl::Size2Di(w,h));
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			destroy();
			vwgl::app::MainLoop::get()->quit(0);
		}
		else if (evt.keyboard().key()==vwgl::Keyboard::kKeySpace) {
			_renderer->getRenderSettings().setAntialiasingEnabled(!_renderer->getRenderSettings().isAntialiasingEnabled());
		}
		else {
			_renderer->keyUp(evt);
		}
	}
	
	virtual void keyDown(const vwgl::app::KeyboardEvent & evt) {
		_renderer->keyDown(evt);
	}
	
	virtual void mouseUp(const vwgl::app::MouseEvent & evt) {
		_renderer->mouseUp(evt);
	}
	
	virtual void mouseDown(const vwgl::app::MouseEvent & evt) {
		_renderer->mouseDown(evt);
	}
	
	virtual void mouseMove(const vwgl::app::MouseEvent & evt) {
		if (!evt.mouse().anyButtonPressed()) {
			_renderer->mouseMove(evt);
		}
	}
	
	virtual void mouseDrag(const vwgl::app::MouseEvent & evt) {
		_renderer->mouseDrag(evt);
	}
	
	virtual void mouseWheel(const vwgl::app::MouseEvent & evt) {
		_renderer->mouseWheel(evt);
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_cubeNode = nullptr;
		_renderer = nullptr;
		vwgl::scene::Light::clearShadowMap();
		vwgl::TextureManager::get()->finalize();
	}
	
protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Node> _cubeNode;
	
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);
	
	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("Renderer test");
	
	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}
	
	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
