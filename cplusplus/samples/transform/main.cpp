
#include "../common/sample_window_controller.hpp"

#include <vwgl/vwgl.hpp>

/*
 *	This sample shows the usage of the TransformNode
 *	Key topics:
 *	- SSAO configuration
 *	- MultimaterialCube
 *	- Flip drawable faces and normals
 *	- GenericMaterial settings
 *	- Texture load and configuration
 *	- DrawableProxy
 *	- TransformNode hierarchy
 *	- TransformNode rotation reference: relative, absolute
 */

class MyWindowController : public sample::WindowController {
public:
	MyWindowController() { setRenderPath(sample::WindowController::kRenderPathDeferred); }

	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key()==vwgl::Keyboard::kKeySpace) {
			vwgl::Node * scene = vwgl::Loader::get()->loadNode("/Users/fernando/Desktop/ssao_test.vitscn");
			if (scene) {
				_sceneSet->setScene(scene);
				_renderer->destroy();
				_renderer = new vwgl::DeferredRenderer();
				_renderer->build(_sceneSet->getSceneRoot(), 1024, 1024);
				_renderer->setViewport(_viewport);
				_renderer->resize(vwgl::Size2Di(viewport().width(),viewport().height()));
				configureRenderer(_renderer.getPtr());
//
				_cameraManipulator->setTransformNode(_sceneSet->getMainCameraTransform());
				_cameraManipulator->setTransform();
//				_cameraManipulator = new vwgl::MouseTargetManipulator(_sceneSet->getMainCameraTransform());
//				_cameraManipulator->setTransform();
			}
		}
		sample::WindowController::keyUp(evt);
	}
protected:
	virtual ~MyWindowController() { }
	
	virtual void configureRenderer(vwgl::Renderer * renderer) {
		vwgl::SSAOMaterial * ssao = renderer->getFilter<vwgl::SSAOMaterial>();
		if (ssao) {
			ssao->setMaxDistance(50.0f);	// This will prevent to apply SSAO on SkyBox
			ssao->setKernelSize(64);
			ssao->setBlurIterations(8);
			ssao->setSampleRadius(0.5f);
		}
	}
	
	virtual void registerPlugins() {
		vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader());
		vwgl::Loader::get()->registerWritter(new vwgl::VwglbWritter());
		vwgl::Loader::get()->registerReader(new vwgl::SceneLoader());
	}

	virtual void createScene(vwgl::SceneSet * sceneSet) {
		sceneSet->getMainLightTransform()->getTransform()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(120.0f),0.0,1.0,0.0)
			.rotate(vwgl::Math::degreesToRadians(-35.0f),1.0,0.0,0.0)
			.translate(0.0f, 0.0f, 20.0f);
		
		std::string resourcePath = vwgl::System::get()->getResourcesPath();
		
		vwgl::TransformNode * set = new vwgl::TransformNode();	// Create a transform node as the main set root
		sceneSet->getSceneRoot()->addChild(set);	// Add set to the scene root
		
		// Create a cube
		vwgl::Cube * cube = new vwgl::Cube(1.0f);
		cube->getGenericMaterial()->setDiffuse(vwgl::Color::orange());			// Configure material
		vwgl::Matrix4 cubeMatrix;
		cubeMatrix.identity();	// Load identity
		cubeMatrix.translate(2.0f, 0.0f, 0.0f);	// Translate 2 units in +X axis
		vwgl::TransformNode * cubeTrx = new vwgl::TransformNode(cubeMatrix);	// Create transform node
		cubeTrx->addChild(new vwgl::DrawableNode(cube));	// Create drawable node and add it to transform node
		set->addChild(cubeTrx);	// Add the cube to the set
		
		// Create a second cube
		vwgl::Cube * secondCube = new vwgl::Cube(1.0f);
		secondCube->getGenericMaterial()->setDiffuse(vwgl::Color::pink());
		// Set the transformation in one line:
		vwgl::TransformNode * secondCubeTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(-2.0, 0.0f, 0.0f));
		secondCubeTrx->addChild(new vwgl::DrawableNode(secondCube));
		set->addChild(secondCubeTrx);	// Add the cube to the set
	
		// Create a sphere over the second cube: to do it, we'll add it to the secondCubeTrx node
		vwgl::Sphere * sphere = new vwgl::Sphere(0.5f);
		sphere->getGenericMaterial()->setDiffuse(vwgl::Color::green());
		vwgl::TransformNode * sphereTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0f, 0.8f, 0.0f));
		sphereTrx->addChild(new vwgl::DrawableNode(sphere));
		secondCubeTrx->addChild(sphereTrx);
		
		// The floor of the scene
		vwgl::Drawable * floor = new vwgl::Plane(20.0f);
		floor->getGenericMaterial()->setNormalMap(vwgl::Loader::get()->loadTexture(resourcePath + "bricks_nm.png"));
		floor->getGenericMaterial()->setShininess(120.0f);
		floor->getGenericMaterial()->setSpecular(vwgl::Color::lightGray());
		floor->getGenericMaterial()->setNormalMapScale(vwgl::Vector2(10.0f));
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode();
		// Compose various transformations:
		floorTrx->getTransform()
			.identity()
			.translate(0.0f, -0.5f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(45.0f), 0.0f, 1.0f, 0.0f);
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		set->addChild(floorTrx);

		// Sphere proxy: reference to a drawable node, to save memory
		vwgl::DrawableProxy * sphereProxy = new vwgl::DrawableProxy(sphere);
		vwgl::TransformNode * sphereProxyTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.5f, 0.5f, 0.5f));
		sphereProxyTrx->addChild(new vwgl::DrawableNode(sphereProxy));
		cubeTrx->addChild(sphereProxyTrx);
		
		// Create an skybox using the absolute rotation reference in the transform node:
		vwgl::MultimaterialCube * skybox = new vwgl::MultimaterialCube(100.0f);

		// Flip faces and normals to view the cube from inside
		skybox->flipFaces();
		skybox->flipNormals();

		skybox->leftFace()->getGenericMaterial()->setLightEmission(1.0);
		skybox->leftFace()->getGenericMaterial()->setDiffuse(vwgl::Color::white());
		skybox->leftFace()->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(resourcePath + "skybox/left.jpg"));
		skybox->leftFace()->getGenericMaterial()->getTexture()->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		skybox->leftFace()->getGenericMaterial()->getTexture()->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		
		skybox->backFace()->getGenericMaterial()->setLightEmission(1.0);
		skybox->backFace()->getGenericMaterial()->setDiffuse(vwgl::Color::white());
		skybox->backFace()->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(resourcePath + "skybox/back.jpg"));
		skybox->backFace()->getGenericMaterial()->getTexture()->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		skybox->backFace()->getGenericMaterial()->getTexture()->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		
		skybox->rightFace()->getGenericMaterial()->setLightEmission(1.0);
		skybox->rightFace()->getGenericMaterial()->setDiffuse(vwgl::Color::white());
		skybox->rightFace()->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(resourcePath + "skybox/right.jpg"));
		skybox->rightFace()->getGenericMaterial()->getTexture()->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		skybox->rightFace()->getGenericMaterial()->getTexture()->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		
		skybox->frontFace()->getGenericMaterial()->setLightEmission(1.0);
		skybox->frontFace()->getGenericMaterial()->setDiffuse(vwgl::Color::white());
		skybox->frontFace()->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(resourcePath + "skybox/front.jpg"));
		skybox->frontFace()->getGenericMaterial()->getTexture()->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		skybox->frontFace()->getGenericMaterial()->getTexture()->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		
		skybox->topFace()->getGenericMaterial()->setLightEmission(1.0);
		skybox->topFace()->getGenericMaterial()->setDiffuse(vwgl::Vector4::white());
		skybox->topFace()->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(resourcePath + "skybox/top.jpg"));
		skybox->topFace()->getGenericMaterial()->getTexture()->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		skybox->topFace()->getGenericMaterial()->getTexture()->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		
		skybox->bottomFace()->getGenericMaterial()->setLightEmission(1.0);
		skybox->bottomFace()->getGenericMaterial()->setDiffuse(vwgl::Vector4::white());
		skybox->bottomFace()->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(resourcePath + "skybox/bottom.jpg"));
		skybox->bottomFace()->getGenericMaterial()->getTexture()->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		skybox->bottomFace()->getGenericMaterial()->getTexture()->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		
		
		vwgl::TransformNode * skyboxTrx = new vwgl::TransformNode();
		
		// Sky box: a cube attached to the camera node.
		skyboxTrx->setRotationReference(vwgl::TransformNode::kAbsolute);
		
		skyboxTrx->addChild(new vwgl::DrawableNode(skybox));
		sceneSet->getMainCameraTransform()->addChild(skyboxTrx);
	}
	
	virtual void configureCameraManipulator(vwgl::MouseTargetManipulator * manipulator) {
		manipulator->setMinPitch(-vwgl::Math::kPiOver2);
		manipulator->setMaxPitch(vwgl::Math::kPiOver2);
	}
};

int main(int argc, char ** argv) {
    vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	if (!window) {
		exit(-1);
	}
    window->setWindowController(new MyWindowController());
    window->setSize(640, 480);
    window->setPosition(100, 100);
    window->setTitle("Sample Transforms");
	window->create();

    vwgl::app::MainLoop::get()->setWindow(window);
    return vwgl::app::MainLoop::get()->run();
}
