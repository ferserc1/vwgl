
add_executable (sample_transform main.cpp)

target_link_libraries (sample_transform vwgl)

set_target_properties(sample_transform PROPERTIES DEBUG_POSTFIX "d")

install (TARGETS sample_transform DESTINATION bin)
