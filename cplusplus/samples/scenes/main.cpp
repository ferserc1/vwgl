#include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

// Create custom components to add functionality to a scene node
class MyRotationComponent : public vwgl::scene::Component {
public:
	MyRotationComponent() {}

	virtual void frame(float delta) {
		vwgl::scene::Transform * trx = sceneObject()->getComponent<vwgl::scene::Transform>();
		
		if (trx) {
			// If the scene node has a Transform component, rotete X and Y axis
			trx->getTransform().getMatrix()
				.identity()
				.rotate(vwgl::Math::degreesToRadians(_rot.x()), 1.0f, 0.0f, 0.0f)
				.rotate(vwgl::Math::degreesToRadians(_rot.y()), 0.0f, 1.0f, 0.0f);
		}
		
		_rot.add(vwgl::Vector2(180.0f * delta, 90.0f * delta));
	}
	
protected:
	vwgl::Vector2 _rot;
};

class CameraRotation : public vwgl::scene::Component {
public:
	CameraRotation() :_rot(0.0f) {}

	virtual void frame(float delta) {
		vwgl::scene::Transform * trx = sceneObject()->getComponent<vwgl::scene::Transform>();

		if (trx) {
			trx->getTransform().getMatrix()
				.identity()
				.rotate(vwgl::Math::degreesToRadians(_rot), 0.0, 1.0, 0.0)
				.translate(0.0f, 0.0f, 5.0f);
		}
		--_rot;
	}

protected:
	virtual ~CameraRotation() {}

	float _rot;
};


class MyWindowController : public vwgl::app::WindowController {
public:
	
	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		
		vwgl::State::get()->setClearColor(vwgl::Color(0.2f,0.5f,1.0f,1.0f));
		vwgl::State::get()->enableDepthTest();
		
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBModelReader());
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBPrefabReader());
		vwgl::Loader::get()->registerReader(new vwgl::ObjModelReader());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBModelWritter());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBPrefabWritter());

		_sceneRoot = new vwgl::scene::Node();

		vwgl::scene::Node * testLoadNode = vwgl::Loader::get()->loadPrefab("test_shape.vwglb", true);
		_sceneRoot->addChild(testLoadNode);
		
		// A model file contains the scene::Drawable data: polyList and materials. A prefab file also contains
		// data from other components in the scene::Node, specifically, will include the physics joints and the
		// shadow projectors. All the other components, such as scene::Transform, scene::Light or scene::Camera
		// will not be included in the prefab.
//		vwgl::Loader::get()->write("path/to/model.vwglb", testLoadNode->getComponent<vwgl::scene::Drawable>());	// Write model
//		vwgl::Loader::get()->write("path/to/prefab.vwglb", testLoadNode);	// Write prefab
		
		// Light: scene node with a Light component and a Transform component to specify the light's direction
		// By default, the lights are of type Directional, so the light's position is irrelevant
		vwgl::scene::Node * lightNode = new vwgl::scene::Node();
		vwgl::scene::Light * light = new vwgl::scene::Light();
		lightNode->addComponent(light);
		lightNode->addComponent(new vwgl::scene::Transform());
		lightNode->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(45.0f), 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(45.0f), -1.0f, 0.0f, 0.0f);
		_sceneRoot->addChild(lightNode);
		
		// Cube: scene node with a Drawable component (scene::Cube is a scene::Drawable component), a Transform
		// component and a custom component to implement the cube rotation (see the implementation of the
		// MyRotationComponent class)
		_cubeNode = new vwgl::scene::Node();
		_cubeNode->addComponent(new vwgl::scene::Transform());
		_cubeNode->addComponent(vwgl::scene::PrimitiveFactory::cube(1.0f,1.0f,1.0f));
		_cubeNode->addComponent(new MyRotationComponent());
		_sceneRoot->addChild(_cubeNode.getPtr());
		
		// Floor: a scene node with a Drawable component and a Transform component, to place the floor one metter
		// down
		vwgl::scene::Node * floorNode = new vwgl::scene::Node();
		floorNode->addComponent(vwgl::scene::PrimitiveFactory::plane(5.0f, 5.0f));
		floorNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, -1.0f, 0.0f)));
		_sceneRoot->addChild(floorNode);

		// Sphere: a scene node with a drawable component and a transform component. The sphere is attached to the
		// cube node, so the transform is relative to the Cube transform.
		vwgl::scene::Node * sphereNode = new vwgl::scene::Node();
		sphereNode->addComponent(vwgl::scene::PrimitiveFactory::sphere(0.5f, 30, 30));
		sphereNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, 0.5f, 0.0f)));
		_cubeNode->addChild(sphereNode);
		

		// Add a camera to the scene
		vwgl::scene::Node * cameraNode = new vwgl::scene::Node();
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform());
		cameraNode->addComponent(new CameraRotation());
		_sceneRoot->addChild(cameraNode);

		// Initialize scene: use the scene::InitVisitor to perform the scene initialization
		_sceneRoot->accept(_initVisitor);
	}
	
	virtual void frame(float delta) {
		// Animation: use the FrameVisitor to set the current animation loop delta
		_frameVisitor.setDelta(delta);
		_sceneRoot->accept(_frameVisitor);
	}
	
	virtual void draw() {
		_sceneRoot->accept(_updateVisitor);

		// Draw: use the scene::DrawVisitor to draw the scene
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		vwgl::scene::Camera::applyTransform(vwgl::scene::Camera::getMainCamera());

		_sceneRoot->accept(_drawVisitor);
		
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		glViewport(0,0,w,h);
		_resizeVisitor.setSize(vwgl::Size2Di(w,h));
		_sceneRoot->accept(_resizeVisitor);
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Node> _cubeNode;
	vwgl::scene::InitVisitor _initVisitor;
	vwgl::scene::ResizeVisitor _resizeVisitor;
	vwgl::scene::FrameVisitor _frameVisitor;
	vwgl::scene::UpdateVisitor _updateVisitor;
	vwgl::scene::DrawVisitor _drawVisitor;

	vwgl::Viewport _viewport;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);
	
	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("Scene test");
	
	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}
	
	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
