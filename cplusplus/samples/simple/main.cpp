
#include <vwgl/vwgl.hpp>

class MyWindowController : public vwgl::app::DefaultWindowController {
public:
	MyWindowController() :vwgl::app::DefaultWindowController(vwgl::app::DefaultWindowController::kQualityHigh) {}

	virtual void initScene(vwgl::scene::Node * sceneRoot) {
		vwgl::scene::Node * cubeNode = new vwgl::scene::Node("Cube");
		cubeNode->addComponent(vwgl::scene::PrimitiveFactory::cube());
		sceneRoot->addChild(cubeNode);
		
		vwgl::scene::Node * planeNode = new vwgl::scene::Node("Floor");
		planeNode->addComponent(vwgl::scene::PrimitiveFactory::plane(15.0f, 15.0f));
		planeNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, -0.52f, 0.0f)));
		sceneRoot->addChild(planeNode);

		vwgl::scene::Node * cameraNode = new vwgl::scene::Node("Camera");
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform());
		cameraNode->addComponent(new vwgl::manipulation::TargetInputController());
		sceneRoot->addChild(cameraNode);


		vwgl::scene::Node * lightNode = new vwgl::scene::Node("Main light");
		vwgl::scene::Light * light = new vwgl::scene::Light();
		light->setShadowBias(0.002f);
		lightNode->addComponent(light);
		lightNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(75.0f), -1.0f, 0.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(22.05f),0.0f, 1.0f, 0.0f)));
		sceneRoot->addChild(lightNode);
	}
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);
	
    vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
    window->setWindowController(new MyWindowController());
    window->setSize(1280, 720);
    window->setPosition(100, 100);
    window->setTitle("Simplest Scene");
	window->create();

    vwgl::app::MainLoop::get()->setWindow(window);
    return vwgl::app::MainLoop::get()->run();
}
