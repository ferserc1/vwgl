
add_executable (sample_simple main.cpp)

target_link_libraries (sample_simple vwgl)

set_target_properties(sample_simple PROPERTIES DEBUG_POSTFIX "d")

install (TARGETS sample_simple DESTINATION bin)
