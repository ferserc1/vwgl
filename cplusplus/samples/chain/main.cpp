#include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>


class MyWindowController : public vwgl::app::WindowController {
public:
	vwgl::scene::Node * getCubeNode(const vwgl::Vector3 & size, const vwgl::Color & color) {
		vwgl::scene::Node * result = new vwgl::scene::Node();
		result->addComponent(vwgl::scene::PrimitiveFactory::cube(size.width(), size.height(), size.depth()));
		result->addComponent(new vwgl::scene::Transform());
		result->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0)->setDiffuse(color);
		return result;
	}

	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBModelReader());
		vwgl::Loader::get()->registerReader(new vwgl::VWGLBPrefabReader());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBModelWritter());
		vwgl::Loader::get()->registerWritter(new vwgl::VWGLBPrefabWritter());

		vwgl::Loader::get()->registerReader(new vwgl::ColladaModelReader());

		_sceneRoot = new vwgl::scene::Node();
		
		vwgl::scene::Light::setShadowMapSize(vwgl::Size2Di(2048));

		// a chain node must contain a vwgl::scene::Transform and at least one ChainJoint node (InputChainJoint or OutputChainJoint)
		vwgl::scene::Node * cube1 = getCubeNode(vwgl::Vector3(1.0f, 1.0f, 1.0f), vwgl::Color::yellow());
		vwgl::scene::Node * cube2 = getCubeNode(vwgl::Vector3(0.5f, 0.5f, 0.5f), vwgl::Color::blue());
		vwgl::scene::Node * cube3 = getCubeNode(vwgl::Vector3(2.0f, 1.1f, 1.1f), vwgl::Color::red());

		vwgl::scene::Node * chainNode = new vwgl::scene::Node();
		chainNode->addChild(cube1);
		chainNode->addChild(cube2);
		chainNode->addChild(cube3);
		_sceneRoot->addChild(chainNode);

		// each output joint will be connected to the next node's input joint
		// The input joint is the transformation you apply to the node before draw it
		// The output joint is the transformation you apply to the next node before draw it
		vwgl::physics::LinkJoint * joint = new vwgl::physics::LinkJoint();
		joint->setOffset(vwgl::Vector3(0.5f, 0.0f, 0.0f));
		cube1->addComponent(new vwgl::scene::OutputChainJoint(joint));

		cube1->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.translate(-0.75f, 0.0f, 0.0f);

		joint = new vwgl::physics::LinkJoint();
		joint->setOffset(vwgl::Vector3(0.25f, 0.0f, 0.0f));
		cube2->addComponent(new vwgl::scene::InputChainJoint(joint));
		joint = new vwgl::physics::LinkJoint();
		joint->setOffset(vwgl::Vector3(0.25f, 0.0f, 0.0f));
		cube2->addComponent(new vwgl::scene::OutputChainJoint(joint));

		joint = new vwgl::physics::LinkJoint();
		joint->setOffset(vwgl::Vector3(1.0f, 0.0f, 0.0f));
		cube3->addComponent(new vwgl::scene::InputChainJoint(joint));

		cube3->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.translate(2.0f, 0.0f, 0.0f);

		vwgl::scene::Chain * chain = new vwgl::scene::Chain();
		chainNode->addComponent(chain);

		vwgl::scene::Node * lightNode = new vwgl::scene::Node();
		vwgl::scene::Light * light = new vwgl::scene::Light();
		light->getProjectionMatrix().ortho(-6.0f, 6.0f, -6.0f, 6.0f, 0.5f, 90.0f);
		lightNode->addComponent(light);
		lightNode->addComponent(new vwgl::scene::Transform());
		lightNode->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(-20.0f), 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(-67.0f), 1.0f, 0.0f, 0.0f)
			.translate(0.0f, 0.0f, 5.0f);
		_sceneRoot->addChild(lightNode);
		
		vwgl::scene::Node * cameraNode = new vwgl::scene::Node();
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform(
			vwgl::Matrix4::makeRotation(vwgl::Math::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0f).
			translate(0.0f, 0.0f, 5.0f)));
		_sceneRoot->addChild(cameraNode);

		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathDeferred, _sceneRoot.getPtr());
		
		_renderer->init();
	}
	
	virtual void frame(float delta) { _renderer->frame(delta); }
	
	virtual void draw() {
		_renderer->update();
		_renderer->draw();
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) { _renderer->resize(vwgl::Size2Di(w,h)); }
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			destroy();
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_cubeNode = nullptr;
		_renderer = nullptr;
		vwgl::scene::Light::clearShadowMap();
		vwgl::TextureManager::get()->finalize();
	}
	
protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Node> _cubeNode;
	
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGL);
	
	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("Chain of nodes");
	
	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}
	
	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
