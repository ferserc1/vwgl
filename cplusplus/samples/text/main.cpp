
#include <vwgl/vwgl.hpp>

class MyWindowController : public vwgl::app::DefaultWindowController {
public:
	MyWindowController() :vwgl::app::DefaultWindowController(vwgl::app::DefaultWindowController::kQualityLow) {}
	
	virtual void initScene(vwgl::scene::Node * sceneRoot) {
		vwgl::scene::Node * cameraNode = new vwgl::scene::Node("Camera");
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform());
		cameraNode->addComponent(new vwgl::manipulation::TargetInputController());
		cameraNode->getComponent<vwgl::manipulation::TargetInputController>()->setDistance(25.0f);
		sceneRoot->addChild(cameraNode);
		
		vwgl::scene::Node * lightNode = new vwgl::scene::Node("Main light");
		lightNode->addComponent(new vwgl::scene::Light());
		lightNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, 1.0f, 0.0f)
														   .rotate(vwgl::Math::degreesToRadians(75.0f), -1.0f, 0.0f, 0.0f)
														   .rotate(vwgl::Math::degreesToRadians(22.05f),0.0f, 1.0f, 0.0f)));
		sceneRoot->addChild(lightNode);
		
		vwgl::ptr<vwgl::TextFont> font = vwgl::Loader::get()->loadFont(vwgl::TextFont::getDefaultFontPath(vwgl::TextFont::kTypeSerif | vwgl::TextFont::kTypeMonospace),70);
		
		if (font.valid()) {
			font->setColor(vwgl::Color::green());
			font->setLighEmission(1.0f);
			vwgl::scene::Node * charNode = new vwgl::scene::Node("Text Node");
			std::string msg = "Hello, World!\nThis is a new message, which\nincludes new line characters.\rand carriage return.";
			vwgl::scene::Text * text = new vwgl::scene::Text(font.getPtr(), msg);
			vwgl::Size2D textSize = text->getSize();
			charNode->addComponent(text);
			charNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(-textSize.x()/2.0f, textSize.y()/2.0f, 0.0f)));
			sceneRoot->addChild(charNode);
		}
	}
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(1280, 720);
	window->setPosition(100, 100);
	window->setTitle("Font test");
	window->create();

	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}