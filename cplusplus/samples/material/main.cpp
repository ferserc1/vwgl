 #include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

static std::string s_vshader =
"#version 150\n"
"in vec4 in_Position;\n"
"in vec4 in_Color;\n"

"uniform mat4 uMVMatrix;\n"
"uniform mat4 uPMatrix;\n"

"out vec4 ex_Color;\n"
"void main(void) {\n"
"	gl_Position = uPMatrix * uMVMatrix * in_Position;\n"
"	ex_Color = in_Color;\n"
"}\n";


static std::string s_fshader =
"#version 150\n"

"// precision highp float\n"

"in vec4 ex_Color;\n"
"out vec4 out_Color;\n"

"void main(void) {\n"
"	out_Color = ex_Color;\n"
"}\n";

class BasicMaterial : public vwgl::Material {
public:
	BasicMaterial() {
		setReuseShaderKey("sample_basic_material");
		initShader();
	}
	
	virtual void initShader() {
		// Use getShader()->loadAndAttachShader(vwgl::Shader::ShaderType shaderType, const std::string &path) to load and attach shader text files
		getShader()->attachShader(vwgl::Shader::kTypeVertex, s_vshader);
		getShader()->attachShader(vwgl::Shader::kTypeFragment, s_fshader);
		getShader()->setOutputParameterName(vwgl::Shader::kOutTypeFragmentDataLocation, "out_Color");
		getShader()->link();
		
		loadVertexAttrib("in_Position");
		loadColorAttrib("in_Color");
		
		getShader()->initUniformLocation("uMVMatrix");
		getShader()->initUniformLocation("uPMatrix");
	}

protected:
	virtual ~BasicMaterial() {}
	
	virtual void setupUniforms() {
		getShader()->setUniform("uMVMatrix", vwgl::State::get()->modelViewMatrix());
		getShader()->setUniform("uPMatrix", vwgl::State::get()->projectionMatrix());
	}
};

class MyWindowController : public vwgl::app::WindowController {
public:
	
	virtual void initGL() {
		_rot = 0;
		vwgl::Graphics::get()->initContext();
		
		glClearColor(0.2f, 0.5f, 1.0f, 1.0f);
		glEnable(GL_DEPTH_TEST);


		_polyList = new vwgl::PolyList();
		
		_polyList->addVertex(vwgl::Vector3(-1.0f, -1.0f, 0.0f));
		_polyList->addVertex(vwgl::Vector3( 1.0f, -1.0f, 0.0f));
		_polyList->addVertex(vwgl::Vector3( 1.0f,  1.0f, 0.0f));
		_polyList->addVertex(vwgl::Vector3(-1.0f,  1.0f, 0.0f));
		
		_polyList->addColor(vwgl::Color::red());
		_polyList->addColor(vwgl::Color::green());
		_polyList->addColor(vwgl::Color::blue());
		_polyList->addColor(vwgl::Color::yellow());
		
		_polyList->addTriangle(0, 1, 2);
		_polyList->addTriangle(2, 3, 0);
		
		_polyList->buildPolyList();

		// Load material
		_material = new BasicMaterial();
		_material->setCullFace(false);
	}

	virtual void draw() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		vwgl::State::get()->projectionMatrix().perspective(45.0f, _viewport.aspectRatio(), 0.1f, 100.0f);
		vwgl::State::get()->viewMatrix().identity().translate(0.0f, 0.0f, -5.0f);
		vwgl::State::get()->modelMatrix().identity().rotate(vwgl::Math::degreesToRadians(_rot), 0.0f, 1.0f, 0.0f);
		++_rot;
		
		_material->bindPolyList(_polyList.getPtr());
		_material->activate();
		_polyList->drawElements();
		_material->deactivate();
	
		window()->glContext()->swapBuffers();
	}

	virtual void reshape(int w, int h) {
		glViewport(0,0,w,h);
		_viewport = vwgl::Viewport(0,0,w,h);
	}

	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
protected:
	vwgl::ptr<vwgl::PolyList> _polyList;
	vwgl::ptr<vwgl::Material> _material;
	float _rot;
	vwgl::Viewport _viewport;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("A very basic test: materials");

	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}

	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
