
#include <vwgl/vwgl.hpp>

class MyWindowController : public vwgl::app::WindowController {
public:
    virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		glEnable(GL_DEPTH_TEST);

		registerPlugins();
		createScene();
		createCamera();
		createLights();
		createRenderer();
    }

	virtual void frame(float delta) {
		static float rotation = 0.0f;
		_rotatingLightTransform->getTransform()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(rotation), 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(-52.5f), 1.0f, 0.0f, 0.0f)
			.translate(vwgl::Vector3(0.0f, 0.0f, 3.0f));
	
		vwgl::TRSTransformStrategy * trans = _pointLightRotation->getTransformStrategy<vwgl::TRSTransformStrategy>();
		trans->rotateY(vwgl::Math::degreesToRadians(-rotation));
		++rotation;
	}

    virtual void draw() {
		_renderer->draw();
        window()->glContext()->swapBuffers();
    }

    virtual void reshape(int w, int h) {
		vwgl::Viewport vp = vwgl::Viewport(0, 0, w, h);
		_renderer->setViewport(vp);
		_renderer->resize(vwgl::Size2Di(w, h));
		_camera->getProjectionMatrix().perspective(60.0f, vp.aspectRatio(), 0.1f, 100.0f);
    }

    virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
        if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
            vwgl::app::MainLoop::get()->quit(0);
        }
		if (evt.keyboard().key() == vwgl::Keyboard::kKey1) {
			vwgl::LightManager::get()->setShadowMapSize(vwgl::Size2Di(512));
		}
		if (evt.keyboard().key() == vwgl::Keyboard::kKey2) {
			vwgl::LightManager::get()->setShadowMapSize(vwgl::Size2Di(1024));
		}
		if (evt.keyboard().key() == vwgl::Keyboard::kKey3) {
			vwgl::LightManager::get()->setShadowMapSize(vwgl::Size2Di(2048));
		}
		if (evt.keyboard().key() == vwgl::Keyboard::kKey4) {
			vwgl::LightManager::get()->setShadowMapSize(vwgl::Size2Di(4096));
		}
		if (evt.keyboard().key()==vwgl::Keyboard::kKey5) {
			vwgl::ShadowRenderPassMaterial * shadowPass = _renderer->getFilter<vwgl::ShadowRenderPassMaterial>();
			if (shadowPass) {
				shadowPass->setShadowType(vwgl::ShadowRenderPassMaterial::kShadowTypeHard);
			}
		}
		if (evt.keyboard().key()==vwgl::Keyboard::kKey6) {
			vwgl::ShadowRenderPassMaterial * shadowPass = _renderer->getFilter<vwgl::ShadowRenderPassMaterial>();
			if (shadowPass) {
				shadowPass->setShadowType(vwgl::ShadowRenderPassMaterial::kShadowTypeSoft);
			}
		}
		if (evt.keyboard().key()==vwgl::Keyboard::kKey7) {
			vwgl::ShadowRenderPassMaterial * shadowPass = _renderer->getFilter<vwgl::ShadowRenderPassMaterial>();
			if (shadowPass) {
				shadowPass->setShadowType(vwgl::ShadowRenderPassMaterial::kShadowTypeSoftStratified);
			}
		}
		if (evt.keyboard().key()==vwgl::Keyboard::kKeyAdd) {
			vwgl::DeferredLighting * lighting = _renderer->getFilter<vwgl::DeferredLighting>();
			if (lighting) {
				lighting->setExposure(lighting->getExposure() + 0.1f);
			}
		}
		if (evt.keyboard().key()==vwgl::Keyboard::kKeySub) {
			vwgl::DeferredLighting * lighting = _renderer->getFilter<vwgl::DeferredLighting>();
			if (lighting) {
				lighting->setExposure(lighting->getExposure() - 0.1f);
			}
		}
    }

    virtual void mouseDown(const vwgl::app::MouseEvent & evt) {
		if (evt.mouse().getButtonStatus(vwgl::Mouse::kLeftButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationRotate);
		}
		else if (evt.mouse().getButtonStatus(vwgl::Mouse::kRightButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationDrag);
		}
		else if (evt.mouse().getButtonStatus(vwgl::Mouse::kMiddleButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationZoom);
		}
    }

    virtual void mouseDrag(const vwgl::app::MouseEvent & evt) {
		_cameraManipulator->mouseMove(evt.pos());
    }

    virtual void mouseWheel(const vwgl::app::MouseEvent & evt) {
		_cameraManipulator->mouseWheel(evt.delta());
    }

protected:
	vwgl::ptr<vwgl::Group> _sceneRoot;
	vwgl::ptr<vwgl::Camera> _camera;
	vwgl::ptr<vwgl::TransformNode> _cameraTransform;
	vwgl::ptr<vwgl::MouseTargetManipulator> _cameraManipulator;
	vwgl::ptr<vwgl::Renderer> _renderer;
	vwgl::ptr<vwgl::TransformNode> _rotatingLightTransform;
	vwgl::ptr<vwgl::TransformNode> _pointLightRotation;

	void createRenderer() {
		vwgl::DeferredRenderer * rend = new vwgl::DeferredRenderer();
		_renderer = rend;

		_renderer->build(_sceneRoot.getPtr(), 1024, 1024);
		_renderer->setClearColor(vwgl::Color(0.45f, 0.65f, 0.89f, 1.0f));
		vwgl::SSAOMaterial * ssao = _renderer->getFilter<vwgl::SSAOMaterial>();
		if (ssao) {
			ssao->setEnabled(true);
			ssao->setSampleRadius(0.1f);
			ssao->setKernelSize(64);
			ssao->setBlurIterations(7);
		}
	}

	void registerPlugins() {
		vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader());
		vwgl::Loader::get()->registerReader(new vwgl::SceneLoader());
	}

	void createScene() {
		_sceneRoot = new vwgl::Group();

		std::string resourcePath = vwgl::System::get()->getResourcesPath();

		vwgl::Drawable * model = vwgl::Loader::get()->loadDrawable(vwgl::System::get()->addPathComponent(resourcePath, "test_shape.vwglb"));
		addObject(model,vwgl::Matrix4::makeScale(vwgl::Vector3(0.42f, 0.42f, 0.42f)));
		model->getGenericMaterial()->setShininess(150.0f);
		model->getGenericMaterial()->setSpecular(vwgl::Color::white());
		model->getGenericMaterial()->setDiffuse(vwgl::Color(0.8f, 0.2f, 0.2f, 1.0f));

		vwgl::Drawable * floor = new vwgl::Plane(10.0f, 10.0f);
		addObject(floor, vwgl::Matrix4::makeTranslation(0.0f, -0.5f, 0.0f));
		floor->getGenericMaterial()->setSpecular(vwgl::Color(1.0f, 1.0f, 1.0f, 1.0f));
		floor->getGenericMaterial()->setShininess(70.0f);
		floor->getGenericMaterial()->setNormalMap(vwgl::Loader::get()->loadTexture(vwgl::System::get()->addPathComponent(resourcePath, "bricks_nm.png")));
		floor->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(vwgl::System::get()->addPathComponent(resourcePath, "bricks.jpg")));
		floor->getGenericMaterial()->setTextureScale(vwgl::Vector2(10.0f, 10.0f));
		floor->getGenericMaterial()->setNormalMapScale(vwgl::Vector2(10.0f, 10.0f));
	}

	void addObject(vwgl::Drawable * drawable, const vwgl::Matrix4 & trans) {
		vwgl::TransformNode * trx = new vwgl::TransformNode(trans);
		vwgl::DrawableNode * drw = new vwgl::DrawableNode(drawable);
		trx->addChild(drw);
		_sceneRoot->addChild(trx);
	}

	void createCamera() {
		_camera = new vwgl::Camera();
		_cameraTransform = new vwgl::TransformNode();
		_cameraTransform->addChild(_camera.getPtr());
		_sceneRoot->addChild(_cameraTransform.getPtr());
		
		_cameraManipulator = new vwgl::MouseTargetManipulator(_cameraTransform.getPtr());
		_cameraManipulator->setDistance(2.0f);
		_cameraManipulator->setTransform();
	}

	void createLights() {
		vwgl::LightManager::get()->setShadowMapSize(vwgl::Size2Di(2048));
		
		vwgl::ShadowLight * l1 = new vwgl::ShadowLight();
		l1->getProjectionMatrix().perspective(40.0f, 1.0f, 0.5f, 50.0f);
		l1->setShadowBias(0.0001f);
		l1->setAmbient(vwgl::Color(0.1f, 0.1f, 0.1f, 1.0f));
		l1->setDiffuse(vwgl::Color(0.9f, 0.9f, 0.9f, 1.0f));
		l1->setSpecular(vwgl::Color::white());
		l1->setType(vwgl::Light::kTypeSpot);
		l1->setConstantAttenuation(0.0f);
		l1->setLinearAttenuation(0.3f);
		l1->setExpAttenuation(0.1f);
		l1->setSpotCutoff(22.0f);
		l1->setSpotExponent(22.0f);
		l1->setShadowStrength(1.0f);
		l1->setShadowBias(0.001f);
		

		vwgl::TransformNode * tl1 = new vwgl::TransformNode();
		tl1->getTransform()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(-52.5f), 1.0f, 0.0f, 0.0f)
			.translate(vwgl::Vector3(0.0f, 0.0f, 3.0f));
		tl1->addChild(l1);
		_sceneRoot->addChild(tl1);
		_rotatingLightTransform = tl1;

		_pointLightRotation = new vwgl::TransformNode();
		vwgl::TRSTransformStrategy * rotation = new vwgl::TRSTransformStrategy();
		_pointLightRotation->setTransformStrategy(rotation);
		_pointLightRotation->addChild(getPointLight(vwgl::Color(0.9f, 0.3f, 0.2f, 1.0f),
													vwgl::Matrix4::makeTranslation( 1.5f, 0.3f,-1.5f)));
		_pointLightRotation->addChild(getPointLight(vwgl::Color(0.9f, 1.0f, 0.2f, 1.0f),
													vwgl::Matrix4::makeTranslation(-1.5f, 0.3f,-1.5f)));
		_pointLightRotation->addChild(getPointLight(vwgl::Color(0.1f, 1.0f, 0.2f, 1.0f),
													vwgl::Matrix4::makeTranslation(-1.5f, 0.3f, 1.5f)));
		_pointLightRotation->addChild(getPointLight(vwgl::Color(0.2f, 0.4f, 1.0f, 1.0f),
													vwgl::Matrix4::makeTranslation( 1.5f, 0.3f, 1.5f)));

		vwgl::ShadowLight * spot = new vwgl::ShadowLight();
		spot->getProjectionMatrix().perspective(40.0f, 1.0f, 0.05f, 100.0f);
		spot->setShadowBias(0.0001f);
		vwgl::TransformNode * spotTrx = new vwgl::TransformNode();
		spotTrx->addChild(spot);
		_sceneRoot->addChild(spotTrx);

		spotTrx->getTransform().identity()
			.rotate(vwgl::Math::degreesToRadians(-45.0f), 1.0f, 0.0f, 0.0f)
			.translate(0.0f, 0.0f, 3.0f);
		spot->setType(vwgl::Light::kTypeSpot);
		spot->setAmbient(vwgl::Color::black());
		spot->setConstantAttenuation(0.0f);
		spot->setLinearAttenuation(0.3f);
		spot->setExpAttenuation(0.0f);
		spot->setShadowStrength(1.0f);
		spot->setSpotExponent(22.0f);
		spot->setSpotAngle(22.0f);
	}
	
	vwgl::Node * getPointLight(const vwgl::Color & diffuse, const vwgl::Matrix4 & transform) {
		vwgl::Light * light = new vwgl::Light();
		light->setDiffuse(diffuse);
		light->setSpecular(vwgl::Color::white());
		light->setAmbient(vwgl::Color::black());
		light->setConstantAttenuation(0.0f);
		light->setLinearAttenuation(0.25f);
		light->setExpAttenuation(0.9f);
		light->setType(vwgl::Light::kTypePoint);
		vwgl::TransformNode * trx = new vwgl::TransformNode(transform);
		trx->addChild(light);
		return trx;
	}
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);
    vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	if (!window) {
		exit(-1);
	}
    window->setWindowController(new MyWindowController());
    window->setSize(640, 480);
    window->setPosition(100, 100);
    window->setTitle("Sample Deferred Render");
	window->create();

    vwgl::app::MainLoop::get()->setWindow(window);
    return vwgl::app::MainLoop::get()->run();
}
