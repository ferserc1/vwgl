
#include <vwgl/vwgl.hpp>

#include <exception>


class TranslateCommand : public vwgl::app::ContextCommand {
public:
	typedef std::vector<vwgl::ptr<vwgl::scene::Transform> > TransformVector;

	TranslateCommand(vwgl::plain_ptr ctx, const vwgl::Vector3 & translate)
		:ContextCommand(ctx), _translate(translate) {}

	inline void addTarget(vwgl::scene::Transform * target) { _targetVector.push_back(target); }

	virtual void doCommand() {
		if (_targetVector.size() == 0) throw std::runtime_error("Empty target list");
		TransformVector::iterator it;
		for (it = _targetVector.begin(); it != _targetVector.end(); ++it) {
			vwgl::TRSTransformStrategy * trs = (*it)->getTransform().getTransformStrategy<vwgl::TRSTransformStrategy>();
			if (trs) {
				vwgl::Vector3 translateVector = trs->getTranslate() + _translate;
				trs->translate(translateVector);
			}
			else {
				(*it)->getTransform().getMatrix().translate(_translate);
			}
		}
	}

	virtual void undoCommand() {
		TransformVector::iterator it;
		for (it = _targetVector.begin(); it != _targetVector.end(); ++it) {
			vwgl::TRSTransformStrategy * trs = (*it)->getTransform().getTransformStrategy<vwgl::TRSTransformStrategy>();
			if (trs) {
				vwgl::Vector3 translateVector = trs->getTranslate() - _translate;
				trs->translate(translateVector);
			}
			else {
				(*it)->getTransform().getMatrix().translate(-_translate.x(), -_translate.y(), -_translate.z());
			}
		}
	}

protected:
	virtual ~TranslateCommand() {}

	TransformVector _targetVector;
	vwgl::Vector3 _translate;
};

class MyWindowController :	public vwgl::app::WindowController {
public:
	MyWindowController() { }
	
	vwgl::scene::Node * getCube(const std::string & name, const vwgl::Vector3 & rotationAxis, const vwgl::Color & color, const vwgl::Vector3 & position) {
		vwgl::scene::Node * cubeNode = new vwgl::scene::Node(name);
		cubeNode->addComponent(vwgl::scene::PrimitiveFactory::cube());
		cubeNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(position.x(), position.y(), position.z())));
		cubeNode->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0)->setDiffuse(color);
		cubeNode->getComponent<vwgl::scene::Drawable>()->getPolyList(0)->setName(name);
		cubeNode->addComponent(new vwgl::manipulation::Selectable());
		
		return cubeNode;
	}
	
	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
	
		_sceneRoot = new vwgl::scene::Node();
		
		_sceneRoot->addChild(getCube("Red cube", vwgl::Vector3(1.0f, 0.0f, 0.0f), vwgl::Color::red(), vwgl::Vector3(-1.6f,0.0f,0.0f)));
		_sceneRoot->addChild(getCube("Blue cube", vwgl::Vector3(0.0f, 1.0f, 0.0f), vwgl::Color::blue(), vwgl::Vector3(0.0f,0.0f,0.0f)));
		_sceneRoot->addChild(getCube("Yellow cube",vwgl::Vector3(-1.0f, 0.0f, 1.0f), vwgl::Color::yellow(), vwgl::Vector3(1.6f,0.0f,0.0f)));
		
		vwgl::scene::Node * camNode = new vwgl::scene::Node();
		camNode->addComponent(new vwgl::scene::Camera());
		camNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeRotation(vwgl::Math::kPiOver4 / 2.0f, -1.0f, 0.0f, 0.0f)
														 .translate(0.0f, 0.0f, 5.0f)));
		_sceneRoot->addChild(camNode);
		
		vwgl::scene::Node * lightNode = new vwgl::scene::Node();
		lightNode->addComponent(new vwgl::scene::Light());
		lightNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeRotation(vwgl::Math::kPiOver4, 0.0f, 1.0f, 0.0f)
														   .rotate(vwgl::Math::kPiOver4, -1.0f, 0.0f, 0.0f)
														   .translate(0.0f, 0.0f, 5.0f)));
		_sceneRoot->addChild(lightNode);

		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathForward, _sceneRoot.getPtr());

		_mousePicker.setSceneRoot(_sceneRoot.getPtr());		
		_selectionHandler.setMarkMaterials(true);

		_renderer->init();

		// This function will be called by the CommandManager before do or undo a ContextCommand
		_commandManager.setCurrentContextCallback([](vwgl::plain_ptr ctx) {
			vwgl::app::GLContext * context = vwgl::native_cast<vwgl::app::GLContext*>(ctx);
			if (context) {
				context->makeCurrent();
			}
		});
	}
	
	virtual void frame(float delta) {
		_renderer->frame(delta);
	}
	
	virtual void draw() {
		_renderer->update();
		_renderer->draw();
		
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		_renderer->resize(vwgl::Size2Di(w,h));
		_viewport = vwgl::Viewport(0,0,w,h);
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			destroy();
			vwgl::app::MainLoop::get()->quit(0);
		}

		else if (evt.keyboard().key() == vwgl::Keyboard::kKeySpace) {
			vwgl::ptr<TranslateCommand> cmd = new TranslateCommand(window()->glContext(), vwgl::Vector3(0.0f, 0.0f, -1.0f));
			_selectionHandler.eachDrawable([&](vwgl::scene::Drawable * drw) {
				if (drw->transform()) {
					cmd->addTarget(drw->transform());
				}
			});
			_commandManager.execute(cmd.getPtr());
		}
		else if (evt.keyboard().key() == vwgl::Keyboard::kKeyLeft) {
			_commandManager.undo();
		}
		else if (evt.keyboard().key() == vwgl::Keyboard::kKeyRight) {
			_commandManager.redo();
		}
	}
	
	virtual void mouseDown(const vwgl::app::MouseEvent & evt) {
		bool additive = evt.mouse().getButtonStatus(vwgl::Mouse::kRightButton);
		
		if (_mousePicker.pick(evt.pos(), _viewport)) {
			_selectionHandler.setAdditiveSelection(additive);
			_selectionHandler.select(_mousePicker.getPickedNode(),
									 _mousePicker.getPickedDrawable(),
									 _mousePicker.getPickedPolyList(),
									 _mousePicker.getPickedMaterial());
		}
		else {
			_selectionHandler.clearSelection();
		}
	}
	
	void destroy() {
		vwgl::scene::Light::clearShadowMap();
		vwgl::TextureManager::get()->finalize();
	}

protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
	vwgl::manipulation::MousePicker _mousePicker;
	vwgl::manipulation::SelectionHandler _selectionHandler;
	vwgl::Viewport _viewport;

	vwgl::app::CommandManager _commandManager;
	
	virtual ~MyWindowController() { }
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

    vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	if (!window) {
		exit(-1);
	}
    window->setWindowController(new MyWindowController());
    window->setSize(640, 480);
    window->setPosition(100, 100);
    window->setTitle("Sample Command Manager");
	window->create();

    vwgl::app::MainLoop::get()->setWindow(window);
    return vwgl::app::MainLoop::get()->run();
}
