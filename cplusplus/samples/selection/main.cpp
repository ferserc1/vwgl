
#include <vwgl/vwgl.hpp>

/*
 *	This sample shows the usage of the Mouse Picker to select items in the scene, and usage the SelectionHandler
 *	to manage the selected scene objects
 *	Key topics:
 */
class RotationComponent : public vwgl::scene::Component {
public:
	RotationComponent(const vwgl::Vector3 & rotationAxis) :_rotationAxis(rotationAxis) {}
	
	virtual void frame(float delta) {
		if (sceneObject() && sceneObject()->getComponent<vwgl::scene::Transform>()) {
			sceneObject()->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
				.rotate(delta, _rotationAxis.x(), _rotationAxis.y(), _rotationAxis.z());
		}
	}
	
protected:
	virtual ~RotationComponent() {}
	
	vwgl::Vector3 _rotationAxis;
};

class SelectionObserver : public vwgl::manipulation::ISelectionObserver {
public:
	virtual void selectionChanged(vwgl::manipulation::SelectionHandler & sender) {
		std::cout << "Selection changed" << std::endl;
		sender.eachPolyList([&](vwgl::PolyList * polyList) {
			std::cout << "  " << polyList->getName() << " selected." << std::endl;
		});
	}
};

class MyWindowController :	public vwgl::app::WindowController {
public:
	MyWindowController() { }
	
	vwgl::scene::Node * getCube(const std::string & name, const vwgl::Vector3 & rotationAxis, const vwgl::Color & color, const vwgl::Vector3 & position) {
		vwgl::scene::Node * cubeNode = new vwgl::scene::Node(name);
		cubeNode->addComponent(vwgl::scene::PrimitiveFactory::cube());
		cubeNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(position.x(), position.y(), position.z())));
		cubeNode->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0)->setDiffuse(color);
		cubeNode->getComponent<vwgl::scene::Drawable>()->getPolyList(0)->setName(name);
		cubeNode->addComponent(new RotationComponent(rotationAxis));
		
		cubeNode->addComponent(new vwgl::manipulation::Selectable());	// Make the object selectable
		
		return cubeNode;
	}
	
	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
	
		_sceneRoot = new vwgl::scene::Node();
		
		_sceneRoot->addChild(getCube("Red cube", vwgl::Vector3(1.0f, 0.0f, 0.0f), vwgl::Color::red(), vwgl::Vector3(-1.6f,0.0f,0.0f)));
		_sceneRoot->addChild(getCube("Blue cube", vwgl::Vector3(0.0f, 1.0f, 0.0f), vwgl::Color::blue(), vwgl::Vector3(0.0f,0.0f,0.0f)));
		_sceneRoot->addChild(getCube("Yellow cube",vwgl::Vector3(-1.0f, 0.0f, 1.0f), vwgl::Color::yellow(), vwgl::Vector3(1.6f,0.0f,0.0f)));
		
		vwgl::scene::Node * camNode = new vwgl::scene::Node();
		camNode->addComponent(new vwgl::scene::Camera());
		camNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeRotation(vwgl::Math::kPiOver4 / 2.0f, -1.0f, 0.0f, 0.0f)
														 .translate(0.0f, 0.0f, 5.0f)));
		_sceneRoot->addChild(camNode);
		
		vwgl::scene::Node * lightNode = new vwgl::scene::Node();
		lightNode->addComponent(new vwgl::scene::Light());
		lightNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeRotation(vwgl::Math::kPiOver4, 0.0f, 1.0f, 0.0f)
														   .rotate(vwgl::Math::kPiOver4, -1.0f, 0.0f, 0.0f)
														   .translate(0.0f, 0.0f, 5.0f)));
		_sceneRoot->addChild(lightNode);

		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathForward, _sceneRoot.getPtr());

		// MousePicker is used to pick objects from scene using the mouse position
		// SelectionHandler is used to manage selected objects
		_mousePicker.setSceneRoot(_sceneRoot.getPtr());
		
		_selectionHandler.setMarkMaterials(true);	// Mark selected materials
		
		_selectionHandler.registerObserver(&_selectionObserver);	// Notify the selection changes to the observer
		
		// init must be always the last action before start the animation/rendering loop
		_renderer->init();
	}
	
	virtual void frame(float delta) {
		_renderer->frame(delta);
	}
	
	virtual void draw() {
		_renderer->update();
		_renderer->draw();
		
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		_renderer->resize(vwgl::Size2Di(w,h));
		_viewport = vwgl::Viewport(0,0,w,h);
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			destroy();
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
	virtual void mouseDown(const vwgl::app::MouseEvent & evt) {
		// Left button: normal selection, right button: additive selection
		bool additive = evt.mouse().getButtonStatus(vwgl::Mouse::kRightButton);
		
		if (_mousePicker.pick(evt.pos(), _viewport)) {
			_selectionHandler.setAdditiveSelection(additive);
			_selectionHandler.select(_mousePicker.getPickedNode(),
									 _mousePicker.getPickedDrawable(),
									 _mousePicker.getPickedPolyList(),
									 _mousePicker.getPickedMaterial());
		}
		else {
			_selectionHandler.clearSelection();
		}
	}
	
	void destroy() {
		vwgl::scene::Light::clearShadowMap();
		vwgl::TextureManager::get()->finalize();
	}

protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
	vwgl::manipulation::MousePicker _mousePicker;
	vwgl::manipulation::SelectionHandler _selectionHandler;
	vwgl::Viewport _viewport;
	SelectionObserver _selectionObserver;
	
	virtual ~MyWindowController() { }
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

    vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	if (!window) {
		exit(-1);
	}
    window->setWindowController(new MyWindowController());
    window->setSize(640, 480);
    window->setPosition(100, 100);
    window->setTitle("Sample Mouse Selection");
	window->create();

    vwgl::app::MainLoop::get()->setWindow(window);
    return vwgl::app::MainLoop::get()->run();
}
