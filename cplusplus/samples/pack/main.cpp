#include <vwgl/vwgl.hpp>

int main(int argc, char ** argv) {
	std::string inPath = vwgl::System::get()->getResourcesPath();
	std::vector<std::string> fileList;
	fileList.push_back("alpha.png");
	fileList.push_back("bricks.jpg");
	fileList.push_back("skybox/back.jpg");
	fileList.push_back("skybox/bottom.jpg");
	std::string packageFile = "/Users/fernando/Desktop/test.pack";
	std::string unpackFolder = "/Users/fernando/Desktop/test";

	vwgl::System::get()->packFiles(inPath, fileList, packageFile);
	vwgl::System::get()->unpackFiles(packageFile, unpackFolder);

	return 0;
}