#include <vwgl/vwgl.hpp>

#include <string>
#include <iostream>

class CameraRotation : public vwgl::scene::Component {
public:
	CameraRotation() :_rot(0.0f) {}

	virtual void frame(float delta) {
		vwgl::scene::Transform * trx = sceneObject()->getComponent<vwgl::scene::Transform>();

		if (trx) {
			trx->getTransform().getMatrix()
				.identity()
				.rotate(vwgl::Math::degreesToRadians(_rot), 0.0f, 1.0f, 0.0f)
				.rotate(vwgl::Math::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0f)
				.translate(0.0f, 0.0f, 9.0f);
		}
		--_rot;
	}

protected:
	virtual ~CameraRotation() {}

	float _rot;
};


class MyWindowController : public vwgl::app::WindowController {
public:
	
	virtual void initGL() {
		vwgl::Graphics::get()->initContext();
		
		_sceneRoot = new vwgl::scene::Node();
		
		vwgl::scene::Node * bbNode1 = new vwgl::scene::Node();
		bbNode1->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(1.0f, 0.0f, 0.0F)));
		bbNode1->addComponent(new vwgl::scene::Billboard(vwgl::Loader::get()->loadTexture("bricks.jpg",true)));
		bbNode1->getComponent<vwgl::scene::Billboard>()->getMaterial()->setLightEmission(1.0f);
		_sceneRoot->addChild(bbNode1);
		
		
		vwgl::scene::Node * lightNode = new vwgl::scene::Node();
		vwgl::scene::Light * light = new vwgl::scene::Light();
		light->getProjectionMatrix().ortho(-6.0f, 6.0f, -6.0f, 6.0f, 0.5f, 90.0f);
		lightNode->addComponent(light);
		lightNode->addComponent(new vwgl::scene::Transform());
		lightNode->getComponent<vwgl::scene::Transform>()->getTransform().getMatrix()
			.identity()
			.rotate(vwgl::Math::degreesToRadians(-20.0f), 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(-67.0f), 1.0f, 0.0f, 0.0f)
			.translate(0.0f, 0.0f, 5.0f);
		_sceneRoot->addChild(lightNode);
		
		vwgl::scene::Node * floorNode = new vwgl::scene::Node();
		floorNode->addComponent(vwgl::scene::PrimitiveFactory::plane(5.0f, 5.0f));
		floorNode->addComponent(new vwgl::scene::Transform(vwgl::Matrix4::makeTranslation(0.0f, -1.0f, 0.0f)));
		_sceneRoot->addChild(floorNode);
		vwgl::GenericMaterial * mat = floorNode->getComponent<vwgl::scene::Drawable>()->getGenericMaterial(0);
		mat->setTexture(vwgl::Loader::get()->loadTexture("bricks.jpg", true));
		mat->setNormalMap(vwgl::Loader::get()->loadTexture("bricks_nm.png", true));
		mat->setShininess(50.0f);
		mat->setSpecular(vwgl::Color::white());
		
		vwgl::scene::Node * cameraNode = new vwgl::scene::Node();
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform());
		cameraNode->addComponent(new CameraRotation());
		_sceneRoot->addChild(cameraNode);

		_renderer = vwgl::scene::Renderer::create(vwgl::scene::Renderer::kRenderPathDeferred, _sceneRoot.getPtr());
		_renderer->init();
	}
	
	virtual void frame(float delta) {
		// Animate using the scene renderer
		_renderer->frame(delta);
	}
	
	virtual void draw() {
		// Update before draw. This will prepare some elements, such as dynamic cubemaps or relfectors
		_renderer->update();

		// Draw using the scene renderer
		_renderer->draw();
		
		window()->glContext()->swapBuffers();
	}
	
	virtual void reshape(int w, int h) {
		// The renderer will handle the viewport and camera settings for us with
		// one single call
		_renderer->resize(vwgl::Size2Di(w,h));
	}
	
	virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			destroy();
			vwgl::app::MainLoop::get()->quit(0);
		}
	}
	
	void destroy() {
		_sceneRoot = nullptr;
		_cubeNode = nullptr;
		_renderer = nullptr;
		vwgl::scene::Light::clearShadowMap();
		vwgl::TextureManager::get()->finalize();
	}
	
protected:
	vwgl::ptr<vwgl::scene::Node> _sceneRoot;
	vwgl::ptr<vwgl::scene::Node> _cubeNode;
	
	vwgl::ptr<vwgl::scene::Renderer> _renderer;
};

int main(int argc, char ** argv) {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);
	
	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(640, 480);
	window->setPosition(100, 100);
	window->setTitle("Billboards");
	
	if (!window || !window->create()) {
		std::cout << "Error creating window" << std::endl;
		exit(-1);
	}
	
	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}
