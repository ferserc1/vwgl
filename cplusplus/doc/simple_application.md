# Simple application sample
## Objective
This is a simple tutorial to create your first fully functional application with VitaminewGL. After reading this document, you'll be able to:

- Initialize VitaminewGL
- Load drawable objects
- Draw gizmos
- Create simple scene sets
- Use the renderers
- Configure advanced render features, such as the SSAO, or the light shadows

You can use the vwgl::app namespace to create an OpenGL-ready window. Read the app_namespace documentation to learn how to do it.

## Configuration
You must to copy the resources and shaders to a location that your application can access. Depending on the platform:

- In Windows and Linux, you must to copy the resources folder and the shaders folder in the same path as your executable. You'll probably need to copy the dynamic library dependencies:

	resources
	shaders
	MyApp.exe
	vwgl.dll
	archive.dll

- In OS X, you must to copy the content of the resources and shaders folder into the application bundle resources folder, and the libvwgl.dylib library into the executable folder:

	MyApp.app
		Contents
			MacOS
				MyApp
				libvwgl.dylib
			Resources
				bricks.jpg
				bricks_nm.png
				def_diffuse.fsh
				def_diffuse.vsh
				def_lighting.fsh
				def_lighting.vsh
				...


## Include files
The simplest way to include all the required headers is to use the general vwgl.hpp file:

#include <vwgl/vwgl.hpp>

But if you have a lot of source files, will be better to include only the files that you need, to improve the compilation time.


## Initialization
Initialize vwgl library and register the plugins that you'll use to load the drawable objects:

	virtual void initGL() {
		vwgl::OpenGL::init();
		glEnable(GL_DEPTH_TEST);

		vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader()); 	// Register the native format loader
		vwgl::Loader::get()->registerReader(new vwgl::ColladaLoader());	// Register the collada format loader
	

## Create the scene
After the initialization, we'll create the scene. To do it, we'll declare some variables to store all the scene resources. This sample is implemented using the vwgl::app namespace, so, we can use our Window Controller class to store them:

	class MyWindowController : public vwgl::app::WindowController {
	public:
		virtual void initGL() {...}
		...
	protected:
		vwgl::ptr<vwgl::SceneSet> _sceneSet;	// Will store our scene data
		vwgl::ptr<vwgl::MouseTargetManipulator> _cameraManipulator;	// To handle the camera movements
		vwgl::ptr<vwgl::Renderer> _renderer;	// Our scene renderer
		vwgl::ptr<vwgl::DrawGizmosVisitor> _gizmoDrawer;	// To draw the scene gizmos
	}

Note that we are using referenced pointers to store our objects.

Now, we can use the initGL() method to create our scene:

		...
		vwgl::Loader::get()->registerReader(new vwgl::ColladaLoader());	// Register the collada format loader

		// Create the scene:
		_sceneSet = new vwgl::SceneSet();
		// Create a sample scene set: a camera and a shadow light. The 2048 parameter
		// is the size of the shadow map. You can also create the scene set manually, adding cameras
		// and lights
		_sceneSet->buildDefault(vwgl::Size2Di(2048));

		// Customize the main light: adjust the projection matrix that will generate
		// the shadow, and the shadow strength
		vwgl::ShadowLight * sl = dynamic_cast<vwgl::ShadowLight*>(_sceneSet->getMainLight());
		if (sl) {
			sl->getProjectionMatrix().ortho(-2.0f, 2.0f, -2.0f, 2.0f, 0.5f, 50.0f);
			sl->setShadowStrength(0.9f);
		}

		// Get the resources path. This function will returns the 'resources' folder
		// path in Windows and Linux, and the 'Resources' bundle folder in OS X
		std::string resourcePath = vwgl::System::get()->getResourcesPath();

		// Load the drawable model. This model is provided in the vwgl's resource folder
		vwgl::Drawable * model = vwgl::Loader::get()->loadDrawable(resourcePath + "test_shape.vwglb");

		// Create a transformation node, attach a drawable node to it, and set the drawable object to
		// the drawable node. Add the transformation node to the scene root
		vwgl::TransformNode * trx = new vwgl::TransformNode();
		vwgl::DrawableNode * drwNode = new vwgl::DrawableNode(model);
		trx->addChild(drwNode);
		_sceneSet->getSceneRoot()->addChild(trx);

		// Create a floor:
		vwgl::Drawable * floor = new vwgl::Plane(10.0f, 10.0f);
		floor->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(resourcePath + "bricks.jpg"));
		floor->getGenericMaterial()->setNormalMap(vwgl::Loader::get()->loadTexture(resourcePath + "bricks_nm.png"));
		vwgl::TransformNode * floorTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0f,-1.3f,0.0f));
		floorTrx->addChild(new vwgl::DrawableNode(floor));
		_sceneSet->getSceneRoot()->addChild(floorTrx);


## Create the renderer
Now we'll create and configure the scene renderer

		...
		_renderer = new vwgl::DeferredRenderer();	// use vwgl::ForwardRenderer() for forward render path
		_renderer->build(_sceneSet->getSceneRoot(), 1024, 1024);
		_renderer->setClearColor(vwgl::Color::black());

		// Configure SSAO (only for deferred renderer)
		vwgl::SSAOMaterial * ssao = _renderer->getFilter<vwgl::SSAOMaterial>();
		if (ssao) {
			ssao->setEnabled(true);
			ssao->setSampleRadius(0.4f);	// radius
			ssao->setKernelSize(64);	// 64 samples per pixel
			ssao->setBlurIterations(8);	// final postprocess blur
		}

## Configure the gizmo drawer
The scene gizmos are drawn over the scene using another renderer
		
		...
		_gizmoDrawer = new vwgl::DrawGizmosVisitor();
		_gizmoDrawer->enableFeature(vwgl::DrawGizmosVisitor::kAxis);
		_gizmoDrawer->enableFeature(vwgl::DrawGizmosVisitor::kDrawableBBox);
		_gizmoDrawer->setDrawableBBoxColor(vwgl::Color::red());

## Create and configure the camera node manipulator

		...
		_cameraManipulator = new vwgl::MouseTargetManipulator(_sceneSet->getMainCameraTransform());
		_cameraManipulator->setDistance(3.0f);
		_cameraManipulator->setTransform();	// Update the camera node position
		_cameraManipulator->setSceneBounds(vwgl::Bounds(-1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f));

## Reshape and draw
The scene is now created and all its resources are initialized. Now, we'll proceed to reshape and draw the scene


	virtual void draw() {
		_renderer->draw(); 	// Draw the scene
		_gizmoDrawer->visit(_sceneSet->getSceneRoot());	// Draw gizmos
		window()->glContext()->swapBuffers(); 	// swap buffers
	}

	virtual void reshape(int w, int h) {
		vwgl::Viewport vp(0,0,w,h);
		_renderer->setViewport(vp);
		_renderer->resize(vwgl::Size2Di(w,h));
		_sceneSet->getMainCamera()->getProjectionMatrix().perspective(60.0f, vp.aspectRatio(), 0.1f, 100.0f);
		_gizmoDrawer->resize(vp);
	}

## Manipulate the camer
The last step is to handle the mouse events to manipulate the camera

	virtual void mouseDown(const vwgl::app::MouseEvent & evt) {
		if (evt.mouse().getButtonStatus(vwgl::Mouse::kLeftButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationRotate);
		}
		else if (evt.mouse().getButtonStatus(vwgl::Mouse::kRightButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationDrag);
		}
		else if (evt.mouse().getButtonStatus(vwgl::Mouse::kMiddleButton)) {
			_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationZoom);
		}
	}

	virtual void mouseDrag(const vwgl::app::MouseEvent & evt) {
		_cameraManipulator->mouseMove(evt.pos());
	}

	virtual void mouseWheel(const vwgl::app::MouseEvent & evt) {
		_cameraManipulator->mouseWheel(evt.delta());
	}


# Sample source:

	#include <vwgl/vwgl.hpp>
	#include <iostream>

	class MyWindowController : public vwgl::app::WindowController {
	public:
		virtual void initGL() {
			vwgl::OpenGL::init();
			glEnable(GL_DEPTH_TEST);

			vwgl::Loader::get()->registerReader(new vwgl::VwglbLoader()); 	// Register the native format loader
			vwgl::Loader::get()->registerReader(new vwgl::ColladaLoader());	// Register the collada format loader

			// Create the scene:
			_sceneSet = new vwgl::SceneSet();
			// Create a sample scene set: a camera and a shadow light. The 2048 parameter
			// is the size of the shadow map. You can also create the scene set manually, adding cameras
			// and lights
			_sceneSet->buildDefault(vwgl::Size2Di(2048));

			// Customize the main light: adjust the projection matrix that will generate
			// the shadow, and the shadow strength
			vwgl::ShadowLight * sl = dynamic_cast<vwgl::ShadowLight*>(_sceneSet->getMainLight());
			if (sl) {
				sl->getProjectionMatrix().ortho(-2.0f, 2.0f, -2.0f, 2.0f, 0.5f, 50.0f);
				sl->setShadowStrength(0.9f);
			}

			// Get the resources path. This function will returns the 'resources' folder
			// path in Windows and Linux, and the 'Resources' bundle folder in OS X
			std::string resourcePath = vwgl::System::get()->getResourcesPath();

			// Load the drawable model. This model is provided in the vwgl's resource folder
			vwgl::Drawable * model = vwgl::Loader::get()->loadDrawable(resourcePath + "test_shape.vwglb");

			// Create a transformation node, attach a drawable node to it, and set the drawable object to
			// the drawable node. Add the transformation node to the scene root
			vwgl::TransformNode * trx = new vwgl::TransformNode();
			vwgl::DrawableNode * drwNode = new vwgl::DrawableNode(model);
			trx->addChild(drwNode);
			_sceneSet->getSceneRoot()->addChild(trx);

			// Create a floor:
			vwgl::Drawable * floor = new vwgl::Plane(10.0f, 10.0f);
			floor->getGenericMaterial()->setTexture(vwgl::Loader::get()->loadTexture(resourcePath + "bricks.jpg"));
			floor->getGenericMaterial()->setNormalMap(vwgl::Loader::get()->loadTexture(resourcePath + "bricks_nm.png"));
			floor->getGenericMaterial()->setSpecular(vwgl::Color::white());
			floor->getGenericMaterial()->setShininess(90.0f);
			vwgl::TransformNode * floorTrx = new vwgl::TransformNode(vwgl::Matrix4::makeTranslation(0.0f, -1.3f, 0.0f));
			floorTrx->addChild(new vwgl::DrawableNode(floor));
			_sceneSet->getSceneRoot()->addChild(floorTrx);

			_renderer = new vwgl::DeferredRenderer();	// use vwgl::ForwardRenderer() for forward render path
			_renderer->build(_sceneSet->getSceneRoot(), 1024, 1024);
			_renderer->setClearColor(vwgl::Color::black());

			// Configure SSAO (only for deferred renderer)
			vwgl::SSAOMaterial * ssao = _renderer->getFilter<vwgl::SSAOMaterial>();
			if (ssao) {
				ssao->setEnabled(true);
				ssao->setSampleRadius(0.4f);	// radius
				ssao->setKernelSize(64);	// 64 samples per pixel
				ssao->setBlurIterations(8);	// final postprocess blur
			}

			// Configure the gizmo drawer
			_gizmoDrawer = new vwgl::DrawGizmosVisitor();
			_gizmoDrawer->enableFeature(vwgl::DrawGizmosVisitor::kAxis);
			_gizmoDrawer->enableFeature(vwgl::DrawGizmosVisitor::kDrawableBBox);
			_gizmoDrawer->setDrawableBBoxColor(vwgl::Color::red());

			// Configure the camera manipulator
			_cameraManipulator = new vwgl::MouseTargetManipulator(_sceneSet->getMainCameraTransform());
			_cameraManipulator->setDistance(3.0f);
			_cameraManipulator->setTransform();	// Update the camera node position
			_cameraManipulator->setCenterBounds(vwgl::Bounds(-1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f));
		}

		virtual void draw() {
			_renderer->draw(); 	// Draw the scene
			_gizmoDrawer->visit(_sceneSet->getSceneRoot());	// Draw gizmos
			window()->glContext()->swapBuffers(); 	// swap buffers
		}

		virtual void reshape(int w, int h) {
			vwgl::Viewport vp(0, 0, w, h);
			_renderer->setViewport(vp);
			_renderer->resize(vwgl::Size2Di(w, h));
			_sceneSet->getMainCamera()->getProjectionMatrix().perspective(60.0f, vp.aspectRatio(), 0.1f, 100.0f);
			_gizmoDrawer->resize(vp);
		}

		virtual void keyDown(const vwgl::app::KeyboardEvent & evt) {
			if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
				vwgl::app::MainLoop::get()->quit(0);
			}
		}

		virtual void mouseDown(const vwgl::app::MouseEvent & evt) {
			if (evt.mouse().getButtonStatus(vwgl::Mouse::kLeftButton)) {
				_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationRotate);
			}
			else if (evt.mouse().getButtonStatus(vwgl::Mouse::kRightButton)) {
				_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationDrag);
			}
			else if (evt.mouse().getButtonStatus(vwgl::Mouse::kMiddleButton)) {
				_cameraManipulator->mouseDown(evt.pos(), vwgl::MouseTargetManipulator::kManipulationZoom);
			}
		}

		virtual void mouseDrag(const vwgl::app::MouseEvent & evt) {
			_cameraManipulator->mouseMove(evt.pos());
		}

		virtual void mouseWheel(const vwgl::app::MouseEvent & evt) {
			_cameraManipulator->mouseWheel(evt.delta());
		}

	protected:
		vwgl::ptr<vwgl::SceneSet> _sceneSet;	// Will store our scene data
		vwgl::ptr<vwgl::MouseTargetManipulator> _cameraManipulator;	// To handle the camera movements
		vwgl::ptr<vwgl::Renderer> _renderer;	// Our scene renderer
		vwgl::ptr<vwgl::DrawGizmosVisitor> _gizmoDrawer;	// To draw the scene gizmos

	};

	int main(int argc, char ** argv) {
		vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
		window->setWindowController(new MyWindowController());
		window->setSize(640, 480);
		window->setPosition(100, 100);
		window->setTitle("My first vwgl window");

		if (!window->create()) {
			std::cerr << "Error creating window" << std::endl;
			exit(-1);
		}

		vwgl::app::MainLoop::get()->setWindow(window);
		return vwgl::app::MainLoop::get()->run();
	}
