# VitaminewGL Application Utilities
## Objective
The application utilities namespace 'vwgl.app' is a ultra light object oriented library to create OpenGL-ready windows.

## The window controller
The window controller is an object used to define your app's behaviour. To create a window controller, you only must to extend the vwgl::app::WindowController class:

	class MyWindowController : public vwgl::app::WindowController {
		...
	};

The implementation of the window controller is very similar to the GLUT library's lifecycle, but in an object-oriented way:

	class MyWindowController : public vwgl::app::WindowController {
		virtual void initGL() {}
		virtual void frame(float delta) {}
		virtual void draw() {}
		virtual void reshape(int w, int h) {}
		virtual void keyDown(const vwgl::app::KeyboardEvent & evt) {}
		virtual void keyUp(const vwgl::app::KeyboardEvent & evt) {}
		virtual void mouseUp(const vwgl::app::MouseEvent & evt) {}
		virtual void mouseDown(const vwgl::app::MouseEvent & evt) {}
		virtual void mouseMove(const vwgl::app::MouseEvent & evt) {}
		virtual void mouseDrag(const vwgl::app::MouseEvent & evt) {}
		virtual void mouseWheel(const vwgl::app::MouseEvent & evt) {}
	};
	

Some considerations about the Window Controller:

- The initGL() function is called inmediatelly after the render context is created.
- The frame() function is similar to the GLUT's idle function. The delta parameter is the amount of time since the previous call to the frame() function. You can use this function to animate your scene.
- The mouseDrag() function is similar to the mouseMove() function, but it's only called if at least one button of the mouse is pressed

## Create a window
Once you have defined your Window Controller class, it's time to create a window. The window must to be created using a window factory:

	int main(int argc, char ** argv) {
		vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
		window->setWindowController(new MyWindowController());
		window->setSize(640,480);
		window->setPosition(100,100);
		window->setTitle("My first vwgl window");

		if (!window->create()) {
			std::cerr << "Error creating window" << std::endl;
			return -1;
		}

		...
	

## Start the main loop
The only thing that remains to do is to start the main application loop:

		vwgl::app::MainLoop::get()->setWindow(window);
		return vwgl::app::MainLoop::get()->run();
	}

## Close application
You can use the MainLoop::quit() method to close the application.

	virtual void keyDown(const vwgl::app::KeyboardEvent & evt) {
		if (evt.keyboard().key() == vwgl::Keyboard::kKeyEsc) {
			vwgl::app::MainLoop::get()->quit(0);
		}
	}



