#version 150

in vec3 aVertexPosition;
in vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

out vec4 vPosition;
out vec2 vTexturePosition;

void main() {
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = vPosition;
	vTexturePosition = aTexturePosition;
}
