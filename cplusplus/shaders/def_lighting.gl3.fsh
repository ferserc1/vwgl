#version 150

in vec2 vTexturePosition;

out vec4 out_FragColor;

uniform sampler2D uDiffuseMap;
uniform sampler2D uNormalMap;
uniform sampler2D uSpecularMap;
uniform sampler2D uPositionMap;
uniform sampler2D uShadowMap;

uniform vec2 uShadowMapSize;

uniform int uBlurIterations;


const int kDirectionalLightType = 4;
const int kSpotLightType = 1;
const int kPointLightType = 5;
const int kDisabledLightType = 10;

// Light properties
uniform int uLightType;
uniform vec3 uLightPosition;
uniform vec3 uLightDirection;
uniform vec4 uLightAmbient;
uniform vec4 uLightDiffuse;
uniform vec4 uLightSpecular;
uniform vec3 uLightAttenuation;
uniform vec3 uSpotDirection;
uniform float uSpotExponent;
uniform float uSpotCutoff;
uniform float uCutoffDistance;
uniform float uExposure;

// Other properties
uniform float uLightEmissionMult;

vec3 getNormal() {
	return normalize(texture(uNormalMap,vTexturePosition).rgb * 2.0 - 1.0);
}

vec4 getLightingColor(	vec4 lightAmb, vec4 lightDiff, vec4 lightSpec,
					  vec3 lightDir, vec3 lightPos, vec3 vertexPos,
					  vec3 att, float spotCutoff, vec3 spotDirection, float spotExponent, int lightType,
					  vec4 matDiff, vec4 matSpec, vec3 normal, float shininess, float lightEmission,
					  vec4 shadowColor) {
	if (lightType==kDisabledLightType) return vec4(0.0,0.0,0.0,1.0);
	float attenuation;
	float spotlight = 1.0;
	if (lightType==kDirectionalLightType) {
		attenuation = 1.0;
	}
	else {
		vec3 posToLightSource = vec3(lightPos - vertexPos);
		float distance = length(posToLightSource);
		lightDir = normalize(posToLightSource);
		attenuation = 1.0 / (att.x + att.y * distance + att.z * distance * distance);
		if (lightType==kSpotLightType) {
			float clampedCos = max(0.0, dot(lightDir,spotDirection));
			if (clampedCos<cos(radians(spotCutoff))) {
				spotlight = 0.0;
			}
			else {
				spotlight = pow(clampedCos, spotExponent);
			}
		}
	}
	vec3 color = (lightEmission + lightAmb.rgb) * matDiff.rgb;
	vec3 diffuseWeight = max(0.0,dot(normal,lightDir)) * lightDiff.rgb * attenuation;
	color += diffuseWeight * matDiff.rgb;
	if (shininess>0.0) {
		vec3 eyeDirection = normalize(-vertexPos);
		vec3 reflectionDirection = normalize(reflect(-lightDir, normal));
		float specularWeight = clamp(pow(max(dot(reflectionDirection, eyeDirection),0.0), shininess), 0.0, 1.0);
		vec3 specularColor = specularWeight * pow(shadowColor.rgb,vec3(10.0));
		color += specularColor  * lightSpec.rgb * matSpec.rgb * attenuation;
	}
	color *= spotlight;
	return vec4(color,1.0);
}

vec4 blur(sampler2D textureInput, vec2 position, vec2 textureSize, int size) {
	vec2 texelSize = 1.0 / textureSize;
	vec3 result = vec3(0.0);
	vec2 hlim = vec2(float(-size) * 0.5 + 0.5);
	for (int x=0; x<size; ++x) {
		for (int y=0; y<size; ++y) {
			vec2 offset = (hlim + vec2(float(x), float(y))) * texelSize;
			result += texture(textureInput, position + offset).rgb;
		}
	}
	return vec4(result / float(size * size),1.0);
}

void main() {
	vec4 diffuseMatColor = texture(uDiffuseMap,vTexturePosition);
	vec3 vertexPosition = texture(uPositionMap,vTexturePosition).xyz;
	float lightEmission = texture(uNormalMap,vTexturePosition).w;
	vec4 emissionColor = diffuseMatColor * lightEmission * uLightEmissionMult;
	vec4 exposureVec = vec4(uExposure,uExposure,uExposure,1.0);
	
	if (diffuseMatColor.a==0.0) {
		out_FragColor = vec4(0.0);
	}
	else if (uCutoffDistance==-1.0 || uCutoffDistance>distance(vertexPosition,uLightPosition)) {
		vec3 normal = getNormal();
		vec4 ambient = uLightAmbient;
		vec4 diffuseColor = uLightDiffuse;
		vec4 specColor = uLightSpecular;
		vec4 specMatColor = texture(uSpecularMap,vTexturePosition);
		float matShininess = specMatColor[3] * 255.0;
		specMatColor.a = 1.0;
		
		vec4 shadowColor = blur(uShadowMap, vTexturePosition, uShadowMapSize, uBlurIterations);
		vec4 light = getLightingColor(	ambient,diffuseColor,specColor,
									  uLightDirection,uLightPosition,vertexPosition,
									  uLightAttenuation,uSpotCutoff,uSpotDirection,uSpotExponent,uLightType,
									  diffuseMatColor,specMatColor,normal,matShininess,lightEmission,shadowColor);
		out_FragColor = ((light * shadowColor * (1.0 - lightEmission)) + emissionColor) * exposureVec;
	}
	else {
		out_FragColor = emissionColor;
	}
}