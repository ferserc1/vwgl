#version 150

in vec2 vNormalMapPos;
in vec2 vLightNormalMapPos;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vBitangent;

out vec4 out_FragColor;

uniform sampler2D uNormalMap;
uniform sampler2D uLightNormalMap;
uniform bool uUseNormalMap;
uniform bool uUseLightNormalMap;
uniform vec2 uNormalMapScale;
uniform vec2 uNormalMapOffset;
uniform float uLightEmission;
uniform sampler2D uLightEmissionMask;
uniform int uLightEmissionMaskChannel;
uniform float uLightEmissionMaskInvert;

// use the alpha texture component to know if we discard the fragment
uniform sampler2D uTexture;
uniform vec2 uTextureOffset;
uniform vec2 uTextureScale;
uniform float uAlphaCutoff;

vec3 getNormal(vec3 normalCoord, vec3 normalMapValue, vec3 tangent, vec3 bitangent, bool useNormalMap) {
	vec3 normal = normalCoord;
	if (useNormalMap) {
		mat3 tbnMat = mat3(	tangent.x, bitangent.x, normalCoord.x,
						   tangent.y, bitangent.y, normalCoord.y,
						   tangent.z, bitangent.z, normalCoord.z);
		normal = normalize(normalMapValue * tbnMat);
	}
	normalize(normal);
	return normal;
}

void main() {
	vec4 tex = texture(uTexture,vNormalMapPos * uTextureScale + uTextureOffset);
	if (tex.a<=uAlphaCutoff) {
		discard;
	}
	else {
		vec3 nmap = normalize(texture(uNormalMap, vNormalMapPos * uNormalMapScale + uNormalMapOffset).rgb * 2.0 - 1.0);
		vec3 lnmap = normalize(texture(uLightNormalMap, vLightNormalMapPos).rgb * 2.0 - 1.0);
		vec3 normal = getNormal(vNormal,
								nmap,
								vTangent, vBitangent, uUseNormalMap);
		normal = normal * 0.5 + 0.5;
		float lightEmissionMask = (1.0 - texture(uLightEmissionMask,vNormalMapPos * uTextureScale + uTextureOffset)[uLightEmissionMaskChannel]);
		float lightEmission = uLightEmission * abs(uLightEmissionMaskInvert - lightEmissionMask);
		out_FragColor = vec4(normal,lightEmission);
	}
}
