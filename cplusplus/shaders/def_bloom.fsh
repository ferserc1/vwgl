#ifdef GL_ES
precision highp float;
precision highp int;
#endif

varying vec2 vTexturePosition;

uniform sampler2D uLightingMap;

void main() {
	gl_FragColor = vec4(clamp(texture2D(uLightingMap,vTexturePosition).rgb - vec3(1.0),0.0,1.0),1.0);
}