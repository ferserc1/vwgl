#version 150

in vec4 aVertexPos;
in vec2 aTexturePosition;

out vec2 vTexturePosition;

void main() {
	gl_Position = aVertexPos;
	vTexturePosition = aTexturePosition;
}