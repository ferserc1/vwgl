#version 150

in vec3 aVertexPosition;
in vec3 aNormalPosition;
in vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uMMatrix;
uniform mat4 uNMatrix;
uniform mat4 uVMatrixInv;

out vec2 vTexturePosition;
out vec4 vPosition;
out vec3 vNormal;
out vec3 vSurfaceToView;

void main() {
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = vPosition;
	vTexturePosition = aTexturePosition;
	
	vec4 transformedNormal = uNMatrix * vec4(aNormalPosition,1.0);
	vNormal = normalize(transformedNormal.xyz);
	vSurfaceToView = (uVMatrixInv[3] - (uMMatrix * vec4(aVertexPosition,1.0))).xyz;
}
