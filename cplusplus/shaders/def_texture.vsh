#ifdef GL_ES
precision highp float;
#endif
attribute vec3 aVertexPosition;
attribute vec3 aNormalPosition;
attribute vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uMMatrix;
uniform mat4 uNMatrix;
uniform mat4 uVMatrixInv;

varying vec2 vTexturePosition;
varying vec4 vPosition;
varying vec3 vNormal;
varying vec3 vSurfaceToView;

void main() {
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = vPosition;
	vTexturePosition = aTexturePosition;
	
	vec4 transformedNormal = uNMatrix * vec4(aNormalPosition,1.0);
	vNormal = normalize(transformedNormal.xyz);
	vSurfaceToView = (uVMatrixInv[3] - (uMMatrix * vec4(aVertexPosition,1.0))).xyz;
}
