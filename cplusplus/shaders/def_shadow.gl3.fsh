#version 150

in vec4 vVertexPosFromLight;
in vec4 vWorldVertex;
in vec3 vTransformedNormal;
in vec4 vPosition;
in vec2 vTexturePosition;

out vec4 out_FragColor;

uniform sampler2D uDepthMap;
uniform vec2 uDepthMapSize;
uniform vec3 uLightDirection;
uniform bool uReceiveShadows;
uniform vec4 uShadowColor;
uniform float uShadowStrength;

const float Near = 1.0;
const float Far = 30.0;
const float LinearDepthConstant = 1.0 / (Far - Near);

uniform sampler2D uTexture;
uniform vec2 uTextureScale;
uniform vec2 uTextureOffset;
uniform float uAlphaCutoff;
uniform float uShadowBias;

/*
 *	1: hard shadows
 *	2: soft shadows
 *	3: soft stratified shadows
 */
uniform int uShadowType;

float unpack (vec4 colour) {
	const vec4 bitShifts = vec4(1.0 / (256.0 * 256.0 * 256.0),
								1.0 / (256.0 * 256.0),
								1.0 / 256.0,
								1);
	return dot(colour , bitShifts);
}

float kShadowBorderOffset = 3.0;

float random(vec3 seed, int i){
	vec4 seed4 = vec4(seed,i);
	float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
	return fract(sin(dot_product) * 43758.5453);
}

void main(void) {
	float alpha = texture(uTexture,vTexturePosition * uTextureScale + uTextureOffset).a;
	if (alpha<=uAlphaCutoff) {
		discard;
	}
	else {
		float visibility = 1.0;
		vec3 depth = vVertexPosFromLight.xyz / vVertexPosFromLight.w;
		
		float shadowBorderOffset = kShadowBorderOffset / uDepthMapSize.x;
		float bias = uShadowBias;
		vec4 shadow = vec4(1.0);
		vec4 shadowColor = texture(uDepthMap,depth.xy);
		if (shadowColor.r==0.0 && shadowColor.g==0.0 && shadowColor.b==0.0) {
			shadow = vec4(1.0);
		}
		else if (uShadowType==0) {
			float shadowDepth = unpack(shadowColor);
			if (shadowDepth<depth.z - bias
				&& (depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0) && uReceiveShadows) {
				visibility = 1.0 - uShadowStrength;
			}
			shadow = clamp(uShadowColor + visibility,0.0,1.0);
		}
		else if (uShadowType==1) {
			const vec3 poissonDisk[4] = vec3[4](
												vec3( -0.94201624, -0.39906216, 0.0 ),
												vec3( 0.94558609, -0.76890725, 0.0 ),
												vec3( -0.094184101, -0.92938870, 0.0 ),
												vec3( 0.34495938, 0.29387760, 0.0 )
												);
			for (int i=0; i<4; ++i) {
				float shadowDepth = unpack(texture(uDepthMap, (depth + poissonDisk[i]/1000.0).xy));
				
				if (shadowDepth<depth.z - bias
					&& (depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0) && uReceiveShadows) {
					visibility -= (uShadowStrength) * 0.25;
				}
			}
			shadow = clamp(uShadowColor + visibility,0.0,1.0);
		}
		else if (uShadowType==2) {
			const vec3 poissonDisk[16] = vec3[](
										  vec3( -0.94201624, -0.39906216, 0.0 ),
										  vec3( 0.94558609, -0.76890725, 0.0 ),
										  vec3( -0.094184101, -0.92938870, 0.0 ),
										  vec3( 0.34495938, 0.29387760, 0.0 ),
										  vec3( -0.91588581, 0.45771432, 0.0 ),
										  vec3( -0.81544232, -0.87912464, 0.0 ),
										  vec3( -0.38277543, 0.27676845, 0.0 ),
										  vec3( 0.97484398, 0.75648379, 0.0 ),
										  vec3( 0.44323325, -0.97511554, 0.0 ),
										  vec3( 0.53742981, -0.47373420, 0.0 ),
										  vec3( -0.26496911, -0.41893023, 0.0 ),
										  vec3( 0.79197514, 0.19090188, 0.0 ),
										  vec3( -0.24188840, 0.99706507, 0.0 ),
										  vec3( -0.81409955, 0.91437590, 0.0 ),
										  vec3( 0.19984126, 0.78641367, 0.0 ),
										  vec3( 0.14383161, -0.14100790, 0.0 )
										  );
			for (int i=0; i<4; ++i) {
				int index = int(mod(float(16.0*random(gl_FragCoord.xyy, i)),16.0));
				float shadowDepth = unpack(texture(uDepthMap, (depth + poissonDisk[index]/1000.0).xy));
				
				if (shadowDepth<depth.z - bias
					&& (depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0) && uReceiveShadows) {
					visibility -= (uShadowStrength) * 0.25;
				}
			}
			shadow = clamp(uShadowColor + visibility,0.0,1.0);
		}
		
		out_FragColor = shadow;
	}
}
