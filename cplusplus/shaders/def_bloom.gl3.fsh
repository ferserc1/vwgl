#version 150

in vec2 vTexturePosition;

out vec4 out_FragColor;

uniform sampler2D uLightingMap;

void main() {
	out_FragColor = vec4(clamp(texture(uLightingMap,vTexturePosition).rgb - vec3(1.0),0.0,1.0),1.0);
}