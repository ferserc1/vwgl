#ifdef GL_ES
precision highp float;
#endif
attribute vec3 aVertexPosition;
attribute vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uMMatrix;

uniform mat4 uTexCoordMatrix;
//uniform mat4 uInvViewMatrix;

//varying vec2 vTexturePosition;
varying vec4 vPosition;
varying vec4 vTexturePosition;

void main() {
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = vPosition;
	
	//vec4 posEye = uMVMatrix * vec4(aVertexPosition,1.0);
	//vec4 posWorld = uInvViewMatrix * posEye;
	
	vec3 direction = vec3(0.0,0.0,-1.0);

	vec4 posWorld = uMMatrix * vec4(aVertexPosition,1.0);
	//vTexturePosition = aTexturePosition;
	vTexturePosition = uTexCoordMatrix * posWorld;
}
