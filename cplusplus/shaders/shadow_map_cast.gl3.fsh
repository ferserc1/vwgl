#version 150

uniform bool uCastShadow;

in vec4 vPosition;
in vec2 vTexturePosition;

out vec4 out_FragColor;

uniform sampler2D uTexture;
uniform vec2 uTextureOffset;
uniform vec2 uTextureScale;
uniform float uAlphaCutoff;

vec4 pack (float depth)
{
	const vec4 bitSh = vec4(256 * 256 * 256,
							256 * 256,
							256,
							1.0);
	const vec4 bitMsk = vec4(0,
							 1.0 / 256.0,
							 1.0 / 256.0,
							 1.0 / 256.0);
	vec4 comp = fract(depth * bitSh);
	comp -= comp.xxyz * bitMsk;
	return comp;
}

void main(void) {
	float alpha = texture(uTexture,vTexturePosition * uTextureScale + uTextureOffset).a;
	if (uCastShadow && alpha>uAlphaCutoff) {
		out_FragColor = pack(gl_FragCoord.z);
	}
	else {
		discard;
	}
}
