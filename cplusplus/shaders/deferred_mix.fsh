#ifdef GL_ES
precision highp float;
precision highp int;
#endif
varying vec2 vTexturePosition;
uniform sampler2D uDiffuseMap;
uniform sampler2D uTranslucentMap;
uniform sampler2D uTranslucentNormalMap;
uniform sampler2D uPositionMap;
uniform sampler2D uSelectionMap;
uniform sampler2D uSSAOMap;
uniform sampler2D uBloomMap;

uniform vec2 uSize;
uniform float uBlurKernel[9];
uniform float uSelectionKernel[9];
uniform float uTargetDistance;
uniform float uDOFAmount;
uniform vec4 uClearColor;
uniform int uSSAOBlur;

uniform float uBrightness;
uniform float uContrast;
uniform float uHue;
uniform float uSaturation;
uniform float uLightness;

uniform int uBloomAmount;

// TODO: Pass this constant through an uniform
const float MAX_RADIUS = 25.0;

vec4 applyConvolution(sampler2D texture, vec2 texCoord, vec2 texSize, float convMatrix[9], float radius) {
	vec2 onePixel = vec2(1.0, 1.0) / texSize * radius;
	vec4 colorSum =
	texture2D(texture, texCoord + onePixel * vec2(-1, -1)) * convMatrix[0] +
	texture2D(texture, texCoord + onePixel * vec2( 0, -1)) * convMatrix[1] +
	texture2D(texture, texCoord + onePixel * vec2( 1, -1)) * convMatrix[2] +
	texture2D(texture, texCoord + onePixel * vec2(-1,  0)) * convMatrix[3] +
	texture2D(texture, texCoord + onePixel * vec2( 0,  0)) * convMatrix[4] +
	texture2D(texture, texCoord + onePixel * vec2( 1,  0)) * convMatrix[5] +
	texture2D(texture, texCoord + onePixel * vec2(-1,  1)) * convMatrix[6] +
	texture2D(texture, texCoord + onePixel * vec2( 0,  1)) * convMatrix[7] +
	texture2D(texture, texCoord + onePixel * vec2( 1,  1)) * convMatrix[8] ;
	float kernelWeight =
	convMatrix[0] +
	convMatrix[1] +
	convMatrix[2] +
	convMatrix[3] +
	convMatrix[4] +
	convMatrix[5] +
	convMatrix[6] +
	convMatrix[7] +
	convMatrix[8] ;
	
	if (kernelWeight <= 0.0) {
		kernelWeight = 1.0;
	}
	
	return vec4((colorSum / kernelWeight).rgb, 1.0);
}

vec4 blur(sampler2D textureInput, int size) {
	vec2 texelSize = 1.0 / uSize;
	vec3 result = vec3(0.0);
	vec2 hlim = vec2(float(-size) * 0.5 + 0.5);
	for (int x=0; x<size; ++x) {
		for (int y=0; y<size; ++y) {
			vec2 offset = (hlim + vec2(float(x), float(y))) * texelSize;
			result += texture2D(textureInput, vTexturePosition + offset).rgb;
		}
	}
	return vec4(result / float(size * size),1.0);
}

vec4 bloom(sampler2D textureInput, int size) {
	vec2 texelSize = 1.0 / uSize;
	vec3 result = vec3(0.0);
	vec2 hlim = vec2(float(-size) * 0.5 + 0.5, 0.0);
	for (int x=0; x<size; ++x) {
		vec2 offset = (hlim + vec2(float(x), 0.0)) * texelSize;
		result += texture2D(textureInput, vTexturePosition + offset).rgb;
	}
	return vec4(result / float(size), 1.0);
}

vec4 blurDepth(sampler2D textureInput, int size, sampler2D posMap, float sourceDepth) {
	vec2 texelSize = 1.0 / uSize;
	vec3 result = vec3(0.0);
	vec2 hlim = vec2(float(-size) * 0.5 + 0.5);
	for (int x=0; x<size; ++x) {
		for (int y=0; y<size; ++y) {
			vec2 offset = (hlim + vec2(float(x), float(y))) * texelSize;
			float targetDepth = texture2D(posMap, vTexturePosition + offset).z;
			if (abs(sourceDepth-targetDepth)>2.0) {
				result += texture2D(textureInput, vTexturePosition + offset).rgb;
			}
			else {
				result += texture2D(textureInput, vTexturePosition).rgb;
			}
		}
	}
	return vec4(result / float(size * size),1.0);
}

vec3 rgb2hsv(vec3 c) {
	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
	vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
	
	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec4 applyBrightness(vec4 color, float brightness) {
	return clamp(vec4(color.rgb + brightness - 0.5,1.0),0.0,1.0);
}

vec4 applyContrast(vec4 color, float contrast) {
	return clamp(vec4((color.rgb * max(contrast + 0.5,0.0)),1.0),0.0,1.0);
}

vec4 applySaturation(vec4 color, float hue, float saturation, float lightness) {
	vec3 fragRGB = clamp(color.rgb + vec3(0.001),0.0,1.0);
	vec3 fragHSV = rgb2hsv(fragRGB);
	lightness -= 0.01;
	float h = hue;
	fragHSV.x *= h;
	fragHSV.yz *= vec2(saturation,lightness);
	fragHSV.x = mod(fragHSV.x, 1.0);
	fragHSV.y = mod(fragHSV.y, 1.0);
	fragHSV.z = mod(fragHSV.z, 1.0);
	fragRGB = hsv2rgb(fragHSV);
	return clamp(vec4(hsv2rgb(fragHSV), color.w),0.0,1.0);
}

vec4 postprocess(vec4 fragColor) {
	return applyContrast(applyBrightness(applySaturation(fragColor,uHue,uSaturation,uLightness),uBrightness),uContrast);
}

void main() {
	vec4 diffuseColor = texture2D(uDiffuseMap,vTexturePosition);
	vec4 translucentColor = texture2D(uTranslucentMap,vTexturePosition);
	if (diffuseColor.a==0.0 && translucentColor.a==0.0) {
		gl_FragColor = uClearColor;
	}
	else {
		vec4 ssao = blur(uSSAOMap,uSSAOBlur);
		if (uTargetDistance>0.0) {
			vec3 pos = texture2D(uPositionMap,vTexturePosition).xyz;
			float dist = distance(pos,vec3(0.0));
			float radius = max(min((dist - uTargetDistance) * uDOFAmount,MAX_RADIUS),1.0);
			diffuseColor = dist>=uTargetDistance ? blurDepth(uDiffuseMap,int(radius),uPositionMap,uTargetDistance):texture2D(uDiffuseMap,vTexturePosition);
		}
		else {
			diffuseColor = texture2D(uDiffuseMap,vTexturePosition);
		}
		diffuseColor *= ssao;
		diffuseColor = diffuseColor + translucentColor;
		diffuseColor.a = 1.0;
		
		vec4 bloom = bloom(uBloomMap,uBloomAmount);
		
		vec4 finalColor = postprocess(diffuseColor + bloom*2.0);
		vec4 selectionColor = applyConvolution(uSelectionMap,vTexturePosition,uSize,uSelectionKernel,2.0);
		selectionColor = (selectionColor.r!=0.0 && selectionColor.g!=0.0 && selectionColor.b!=0.0) ? vec4(1.0):vec4(0.0);
		gl_FragColor = clamp(finalColor + selectionColor,0.0,1.0);
	}
}
