#version 330

in vec3 in_Vertex;
in vec2 in_TexCoord;

out vec2 out_TexCoord;

void main() {
	gl_Position = vec4(in_Vertex,1.0);
	out_TexCoord = in_TexCoord;
}