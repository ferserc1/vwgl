#ifdef GL_ES
precision highp float;
#endif
attribute vec3 aVertexPosition;
attribute vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uMMatrix;

varying vec2 vTexturePosition;

varying vec4 vPosition;

void main() {
	vTexturePosition = aTexturePosition;
	vPosition = uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = uPMatrix * vPosition;
}
