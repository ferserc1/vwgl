#ifdef GL_ES
precision highp float;
#endif

varying vec4 vTexturePosition;
varying vec4 vPosition;

uniform sampler2D uTexture;
uniform bool uReceiveProjections;

void main(void) {
	if (uReceiveProjections) {
		vec4 texColor = vec4(1.0);
	
		if (vTexturePosition.q>0.0) {
			texColor = texture2DProj(uTexture,vTexturePosition);
		}
		gl_FragColor = texColor;
		//texColor.a = 1.0-texColor.r;
		//texColor.rgb = vec3(0.0);
		//if (texColor.a==0.0) discard;
		//else {
		//	gl_FragColor = texColor;
		//}
	}
	else {
		discard;
	}
}
