#ifdef GL_ES
precision highp float;
#endif
attribute vec3 aVertexPosition;
attribute vec3 aNormalPosition;
attribute vec2 aTexturePosition;
uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uMMatrix;
uniform mat4 uNMatrix;
uniform mat4 uLightProjectionMatrix;
uniform mat4 uLightViewMatrix;
uniform vec3 uLightDirection;
varying vec4 vVertexPosFromLight;
varying vec4 vWorldVertex;
varying vec4 vPosition;
varying vec3 vTransformedNormal;
varying vec2 vTexturePosition;

const mat4 ScaleMatrix = mat4(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.5, 1.0);

void main() {
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = vPosition;

	vWorldVertex = uMMatrix * vec4(aVertexPosition,1.0);
	vVertexPosFromLight = ScaleMatrix * uLightProjectionMatrix * uLightViewMatrix * vWorldVertex;

	vTransformedNormal = (uNMatrix * vec4(aNormalPosition,1.0)).xyz;
	vTexturePosition = aTexturePosition;
}
