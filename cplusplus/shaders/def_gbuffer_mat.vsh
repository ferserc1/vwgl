#version 330

in vec3 in_Position;
in vec3 in_Normal;
in vec2 in_TexCoord0;
in vec2 in_TexCoord1;
in vec3 in_Tangent;

uniform mat4 uPMatrix;
uniform mat4 uVMatrix;
uniform mat4 uMMatrix;
uniform mat4 uNMatrix;
uniform mat4 uVMatrixInv;

out vec2 vTexCoord0;
out vec2 vTexCoord1;
out vec3 vNormal;
out vec3 vTangent;
out vec3 vBitangent;
out vec3 vPosition;
out vec3 vSurfaceToView;

void main() {
	mat4 MVMatrix = uVMatrix * uMMatrix;
	gl_Position = uPMatrix * MVMatrix * vec4(in_Position,1.0);
	
	vTexCoord0 = in_TexCoord0;
	vTexCoord1 = in_TexCoord1;
	vNormal = normalize((uNMatrix * vec4(in_Normal,1.0)).rgb);
	vTangent = normalize((uNMatrix * vec4(in_Tangent,1.0)).rgb);
	vBitangent = cross(vNormal, vTangent);
	
	vPosition = (MVMatrix * vec4(in_Position,1.0)).xyz;
	vSurfaceToView = (uVMatrixInv[3] - (uMMatrix * vec4(in_Position,1.0))).xyz;
}
