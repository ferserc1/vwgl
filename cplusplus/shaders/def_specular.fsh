#ifdef GL_ES
precision highp float;
#endif

uniform vec4 uSpecularColor;
uniform float uShininess;
uniform sampler2D uTexture;
uniform vec2 uTextureOffset;
uniform vec2 uTextureScale;
uniform float uAlphaCutoff;
uniform sampler2D uShininessMask;
uniform int uShininessMaskComponent;
uniform float uShininessMaskInvert;

varying vec2 vTexturePosition;

void main(void) {
	vec4 tex = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset);
	if (tex.a<=uAlphaCutoff) {
		discard;
	}
	else {
		float shininessMask = texture2D(uShininessMask,vTexturePosition * uTextureScale + uTextureOffset)[uShininessMaskComponent];
		vec4 color = uSpecularColor  * abs(uShininessMaskInvert - shininessMask);
		color.a = uShininess / 255.0;
		gl_FragColor = color;
	}
}