#ifdef GL_ES
precision highp float;
precision highp int;
#endif
varying vec2 vTexturePosition;

uniform sampler2D uPositionMap;
uniform sampler2D uNormalMap;

uniform mat4 uProjectionMat;

const int MAX_KERN_OFFSETS = 64;
uniform sampler2D uSSAORandomMap;
uniform vec2 uRandomTexSize;
uniform vec2 uViewportSize;
uniform float uSSAOSampleRadius;
uniform vec3 uSSAOKernelOffsets[MAX_KERN_OFFSETS];
uniform int uSSAOKernelSize;
uniform vec4 uSSAOColor;
uniform bool uEnabled;
uniform float uMaxDistance;

void main(void) {
	if (!uEnabled) discard;
	vec3 normal = texture2D(uNormalMap,vTexturePosition).xyz * 2.0 - 1.0;
	vec4 vertexPos = texture2D(uPositionMap,vTexturePosition);
	if (distance(vertexPos.xyz,vec3(0))>uMaxDistance || vertexPos.w==1.0) {
		discard;
	}
	else {
		vec2 noiseScale = vec2(uViewportSize.x/uRandomTexSize.x,uViewportSize.y/uRandomTexSize.y);
		vec3 randomVector = texture2D(uSSAORandomMap,vTexturePosition * noiseScale).xyz * 2.0 - 1.0;
		vec3 tangent = normalize(randomVector - normal * dot(randomVector, normal));
		vec3 bitangent = cross(normal,tangent);
		mat3 tbn = mat3(tangent, bitangent, normal);
		
		float occlusion = 0.0;
		for (int i=0; i<MAX_KERN_OFFSETS; ++i) {
			if (uSSAOKernelSize==i) break;
			vec3 samplePos = tbn * uSSAOKernelOffsets[i];
			samplePos = samplePos * uSSAOSampleRadius + vertexPos.xyz;
			
			vec4 offset = uProjectionMat * vec4(samplePos,1.0); // -w, w
			offset.xyz /= offset.w;	// -1, 1
			offset.xyz = offset.xyz * 0.5 + 0.5;	// 0, 1
			
			vec4 sampleRealPos = texture2D(uPositionMap, offset.xy);
			
			if (samplePos.z<sampleRealPos.z) {
				float dist = distance(vertexPos.xyz,sampleRealPos.xyz);
				occlusion += dist<uSSAOSampleRadius ? 1.0:0.0;
			}
		}
		occlusion = 1.0 - (occlusion / float(uSSAOKernelSize));
		
		gl_FragColor = clamp(vec4(occlusion,occlusion,occlusion,1.0) + uSSAOColor,0.0,1.0);
	}
}
