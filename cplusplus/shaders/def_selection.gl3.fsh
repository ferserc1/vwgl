#version 150

out vec4 out_FragColor;

uniform bool uSelected;
uniform vec4 uColor;

void main() {
	if (uSelected) {
		out_FragColor = uColor;
	}
	else {
		discard;
	}
}