#version 150

in vec3 aVertexPosition;
in vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

out vec2 vTexturePosition;

void main() {
	vTexturePosition = aTexturePosition;
	gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
}
