#version 150

in vec2 vTexturePosition;
in vec4 vPosition;
in vec3 vNormal;
in vec3 vSurfaceToView;

out vec4 out_FragColor;

uniform sampler2D uTexture;
uniform vec4 uColorTint;
uniform bool uSelectMode;
uniform bool uIsEnabled;
uniform float uAlphaCutoff;

uniform mat4 uNMatrix;
uniform sampler2D uNormalMap;
uniform bool uUseNormalMap;
uniform vec2 uNormalMapScale;
uniform vec2 uNormalMapOffset;

uniform vec2 uTextureOffset;
uniform vec2 uTextureScale;

uniform samplerCube uCubeMap;
uniform bool uUseCubeMap;
uniform float uReflectionAmount;

vec3 getNormal() {
	vec3 normal = vNormal;
	if (uUseNormalMap) {
		vec3 tangent = normalize(uNMatrix[0].xyz);
		vec3 binormal = normalize(uNMatrix[1].xyz);
		mat3 tangentToWorld = mat3(tangent.x, binormal.x, normal.x,
								   tangent.y, binormal.y, normal.y,
								   tangent.z, binormal.z, normal.z);
		normal = (texture(uNormalMap, vTexturePosition * uNormalMapScale + uNormalMapOffset).rgb * 2.0 - 1.0) * tangentToWorld;
	}
	normalize(normal);
	return normal;
}

vec4 getCubeColor(vec3 normal) {
	vec3 surfaceToView = normalize(vSurfaceToView);
	vec3 reflected = reflect(surfaceToView,-normal);
	return texture(uCubeMap,reflected);
}

vec4 getSelectionColor() {
	vec4 selectionColor = vec4(0.0,0.0,0.0,0.0);
	float size = 0.9;
	
	if (uSelectMode) {
		float intValueX = floor(vPosition.x);
		float intValueY = floor(vPosition.y);
		float decValueX = vPosition.x - intValueX;
		float decValueY = vPosition.y - intValueY;
		if (!((decValueX>size && decValueY>size) ||
			  (decValueX<size && decValueY<size))) {
			selectionColor = vec4(0.5,0.5,0.5,0.0);
		}
	}
	return selectionColor;
}

void main(void) {
	vec4 texColor = texture(uTexture,vTexturePosition * uTextureScale + uTextureOffset);
	if (texColor.a==uAlphaCutoff) {
		discard;
	}
	else {
		if (!uIsEnabled) {
			texColor = vec4(1.0);
		}
		
		vec4 cmColor = vec4(0.0,0.0,0.0,0.0);
		if (uUseCubeMap) {
			cmColor = getCubeColor(getNormal());
		}
		vec4 color = uColorTint * vec4(1.0 - uReflectionAmount);
		out_FragColor = texColor * color + cmColor * uReflectionAmount + getSelectionColor();
	}
}
