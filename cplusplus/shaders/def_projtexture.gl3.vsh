#version 150

in vec3 aVertexPosition;
in vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uMMatrix;

uniform mat4 uTexCoordMatrix;

out vec4 vPosition;
out vec4 vTexturePosition;

void main() {
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = vPosition;
	
	vec3 direction = vec3(0.0,0.0,-1.0);

	vec4 posWorld = uMMatrix * vec4(aVertexPosition,1.0);
	vTexturePosition = uTexCoordMatrix * posWorld;
}
