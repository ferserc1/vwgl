#version 150

in vec3 aVertexPosition;
in vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uMMatrix;

out vec2 vTexturePosition;
out vec4 vPosition;

void main() {
	vTexturePosition = aTexturePosition;
	vPosition = uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = uPMatrix * vPosition;
}
