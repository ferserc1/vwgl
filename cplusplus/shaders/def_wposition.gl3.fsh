#version 150

in vec4 vPosition;
in vec2 vTexturePosition;

out vec4 out_FragColor;

uniform sampler2D uTexture;
uniform vec2 uTextureScale;
uniform vec2 uTextureOffset;
uniform float uAlphaCutoff;

void main() {
	float alpha = texture(uTexture,vTexturePosition * uTextureScale + uTextureOffset).a;
	if (alpha<=uAlphaCutoff) {
		discard;
	}
	else {
		out_FragColor = vec4(vPosition.xyz,gl_FragCoord.z);
	}
}
