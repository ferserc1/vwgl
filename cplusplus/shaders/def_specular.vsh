#ifdef GL_ES
precision highp float;
#endif
attribute vec3 aVertexPosition;
attribute vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

varying vec2 vTexturePosition;

void main() {
	vTexturePosition = aTexturePosition;
	gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
}
