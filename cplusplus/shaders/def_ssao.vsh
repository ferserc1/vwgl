#ifdef GL_ES
precision highp float;
precision highp int;
#endif
attribute vec4 aVertexPos;
attribute vec2 aTexturePosition;

varying vec2 vTexturePosition;

void main() {
	gl_Position = aVertexPos;
	vTexturePosition = aTexturePosition;
}