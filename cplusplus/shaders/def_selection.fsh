#ifdef GL_ES
precision highp float;
#endif
uniform bool uSelected;
uniform vec4 uColor;

void main() {
	if (uSelected) {
		gl_FragColor = uColor;
	}
	else {
		discard;
	}
}