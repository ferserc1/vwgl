#version 150

in vec3 aVertexPos;
in vec3 aNormalPos;
in vec2 aNormalMapPos;
in vec2 aLightNormalMapPos;
in vec3 aTangentPos;

uniform mat4 uMMatrix;
uniform mat4 uVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uNMatrix;

out vec2 vNormalMapPos;
out vec2 vLightNormalMapPos;
out vec3 vNormal;
out vec3 vTangent;
out vec3 vBitangent;

void main() {
	gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aVertexPos,1.0);
	vNormalMapPos = aNormalMapPos;
	vLightNormalMapPos = aLightNormalMapPos;
	vNormal = normalize((uNMatrix * vec4(aNormalPos,1.0)).rgb);
	vTangent = normalize((uNMatrix * vec4(aTangentPos,1.0)).rgb);
	vBitangent = cross(vNormal,vTangent);
}
