#ifdef GL_ES
precision highp float;
#endif
attribute vec3 aVertexPosition;
attribute vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

varying vec4 vPosition;
varying vec2 vTexturePosition;

void main() {
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = vPosition;
	vTexturePosition = aTexturePosition;
}
