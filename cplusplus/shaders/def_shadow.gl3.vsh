#version 150

in vec3 aVertexPosition;
in vec3 aNormalPosition;
in vec2 aTexturePosition;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uMMatrix;
uniform mat4 uNMatrix;
uniform mat4 uLightProjectionMatrix;
uniform mat4 uLightViewMatrix;
uniform vec3 uLightDirection;

out  vec4 vVertexPosFromLight;
out  vec4 vWorldVertex;
out  vec4 vPosition;
out  vec3 vTransformedNormal;
out  vec2 vTexturePosition;

const mat4 ScaleMatrix = mat4(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.5, 1.0);

void main() {
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);
	gl_Position = vPosition;

	vWorldVertex = uMMatrix * vec4(aVertexPosition,1.0);
	vVertexPosFromLight = ScaleMatrix * uLightProjectionMatrix * uLightViewMatrix * vWorldVertex;

	vTransformedNormal = (uNMatrix * vec4(aNormalPosition,1.0)).xyz;
	vTexturePosition = aTexturePosition;
}
