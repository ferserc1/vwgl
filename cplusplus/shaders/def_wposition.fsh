#ifdef GL_ES
precision highp float;
#endif

varying vec4 vPosition;
varying vec2 vTexturePosition;

uniform sampler2D uTexture;
uniform vec2 uTextureScale;
uniform vec2 uTextureOffset;
uniform float uAlphaCutoff;

void main() {
	float alpha = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset).a;
	if (alpha<=uAlphaCutoff) {
		discard;
	}
	else {
		gl_FragColor = vec4(vPosition.xyz,gl_FragCoord.z);
	}
}
