#version 330

in vec2 out_TexCoord;

out vec4 out_Color;

uniform sampler2D uDiffuse;
uniform sampler2D uNormal;
uniform sampler2D uSpecular;
uniform sampler2D uPosition;

void main() {
	if (out_TexCoord.y<0.25) {
		out_Color = texture(uDiffuse, out_TexCoord);
	}
	else if (out_TexCoord.y<0.5) {
		out_Color = texture(uNormal, out_TexCoord);
	}
	else if (out_TexCoord.y<0.75) {
		out_Color = texture(uSpecular, out_TexCoord);
	}
	else {
		out_Color = texture(uPosition, out_TexCoord);
	}
}
