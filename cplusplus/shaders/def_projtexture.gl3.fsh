#version 150

in vec4 vTexturePosition;
in vec4 vPosition;

out vec4 out_FragColor;

uniform sampler2D uTexture;
uniform bool uReceiveProjections;

void main(void) {
	if (uReceiveProjections) {
		vec4 texColor = vec4(1.0);
	
		if (vTexturePosition.q>0.0) {
			texColor = textureProj(uTexture,vTexturePosition);
		}
		out_FragColor = texColor;
	}
	else {
		discard;
	}
}
