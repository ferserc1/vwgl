#version 150

uniform vec4 uSpecularColor;
uniform float uShininess;
uniform sampler2D uTexture;
uniform vec2 uTextureOffset;
uniform vec2 uTextureScale;
uniform float uAlphaCutoff;
uniform sampler2D uShininessMask;
uniform int uShininessMaskComponent;
uniform float uShininessMaskInvert;

in vec2 vTexturePosition;

out vec4 out_FragColor;

void main(void) {
	vec4 tex = texture(uTexture,vTexturePosition * uTextureScale + uTextureOffset);
	if (tex.a<=uAlphaCutoff) {
		discard;
	}
	else {
		float shininessMask = texture(uShininessMask,vTexturePosition * uTextureScale + uTextureOffset)[uShininessMaskComponent];
		vec4 color = uSpecularColor  * abs(uShininessMaskInvert - shininessMask);
		color.a = uShininess / 255.0;
		out_FragColor = color;
	}
}