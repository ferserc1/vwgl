#ifdef GL_ES
precision highp float;
precision highp int;
#endif

varying vec2 vNormalMapPos;
varying vec2 vLightNormalMapPos;
varying vec3 vNormal;
varying vec3 vTangent;
varying vec3 vBitangent;

uniform sampler2D uNormalMap;
uniform sampler2D uLightNormalMap;
uniform bool uUseNormalMap;
uniform bool uUseLightNormalMap;
uniform vec2 uNormalMapScale;
uniform vec2 uNormalMapOffset;
uniform float uLightEmission;
uniform sampler2D uLightEmissionMask;
uniform int uLightEmissionMaskChannel;
uniform float uLightEmissionMaskInvert;

// use the alpha texture component to know if we discard the fragment
uniform sampler2D uTexture;
uniform vec2 uTextureOffset;
uniform vec2 uTextureScale;
uniform float uAlphaCutoff;

vec3 getNormal(vec3 normalCoord, vec3 normalMapValue, vec3 tangent, vec3 bitangent, bool useNormalMap) {
	vec3 normal = normalCoord;
	if (useNormalMap) {
		mat3 tbnMat = mat3(	tangent.x, bitangent.x, normalCoord.x,
						   tangent.y, bitangent.y, normalCoord.y,
						   tangent.z, bitangent.z, normalCoord.z);
		normal = normalize(normalMapValue * tbnMat);
	}
	normalize(normal);
	return normal;
}

void main() {
	vec4 tex = texture2D(uTexture,vNormalMapPos * uTextureScale + uTextureOffset);
	if (tex.a<=uAlphaCutoff) {
		discard;
	}
	else {
		vec3 nmap = normalize(texture2D(uNormalMap, vNormalMapPos * uNormalMapScale + uNormalMapOffset).rgb * 2.0 - 1.0);
		vec3 lnmap = normalize(texture2D(uLightNormalMap, vLightNormalMapPos).rgb * 2.0 - 1.0);
		vec3 normal = getNormal(vNormal,
								nmap,
								vTangent, vBitangent, uUseNormalMap);
		normal = normal * 0.5 + 0.5;
		float lightEmissionMask = (1.0 - texture2D(uLightEmissionMask,vNormalMapPos * uTextureScale + uTextureOffset)[uLightEmissionMaskChannel]);
		float lightEmission = uLightEmission * abs(uLightEmissionMaskInvert - lightEmissionMask);
		gl_FragColor = vec4(normal,lightEmission);
	}
}
