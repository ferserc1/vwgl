#version 330

layout (location = 0) out vec4 out_Diffuse;
layout (location = 1) out vec4 out_Normal;
layout (location = 2) out vec4 out_Specular;
layout (location = 3) out vec4 out_Position;
layout (location = 4) out vec4 out_Selection;

uniform sampler2D uDiffuse;
uniform vec2 uDiffuseScale;
uniform vec2 uDiffuseOffset;
uniform float uAlphaCutoff;
uniform vec4 uColorTint;
uniform float uReflectionAmount;
uniform samplerCube uCubeMap;
uniform bool uUseCubemap;

uniform sampler2D uNormalMap;
uniform vec2 uNormalMapScale;
uniform vec2 uNormalMapOffset;

uniform vec4 uSpecularColor;
uniform float uShininess;
uniform sampler2D uShininessMask;
uniform int uShininessMaskComponent;
uniform float uShininessMaskInvert;

uniform float uLightEmission;
uniform sampler2D uLightEmissionMask;
uniform int uLightEmissionMaskChannel;
uniform float uLightEmissionMaskInvert;

uniform vec4 uSelected;


in vec2 vTexCoord0;
in vec2 vTexCoord1;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vBitangent;
in vec3 vSurfaceToView;

in vec3 vPosition;

vec4 diffuse(sampler2D tex, vec2 texCoord, vec2 offset, vec2 scale) {
	return texture(tex, texCoord * scale + offset);
}

vec4 getCubeColor(vec3 normal) {
	vec3 surfaceToView = normalize(vSurfaceToView);
	vec3 reflected = reflect(surfaceToView,-normal);
	return texture(uCubeMap,reflected);
}

vec3 normal(vec3 normal, vec3 tangent, vec3 bitangent, sampler2D tex, vec2 texCoord, vec2 offset, vec2 scale) {
	vec3 result = normal;
	vec4 nmapColor = texture(tex,texCoord * scale + offset);
	vec3 nmap = nmapColor.xyz;
	if (nmapColor.a!=0.0) {
		nmap = normalize(nmap * 2.0 - 1.0);
		mat3 tbnMat = mat3( tangent.x, bitangent.x, normal.x,
						   tangent.y, bitangent.y, normal.y,
						   tangent.z, bitangent.z,	normal.z);
		result = normalize(nmap * tbnMat);
	}
	return result;
}

float lightEmission(float emission, sampler2D tex, vec2 texCoord, vec2 offset, vec2 scale, float invert, int channel) {
	float lightEmissionMask = (1.0 - texture(tex,texCoord * scale + offset)[channel]);
	return emission * abs(invert - lightEmissionMask);
}

vec4 specular(vec4 specularColor, float shininess, sampler2D shininessMask, vec2 texCoord, vec2 offset, vec2 scale, float shininessInvert, int shininessComponent) {
	float value = texture(shininessMask, texCoord * scale + offset)[shininessComponent];
	vec4 color = specularColor * abs(shininessInvert - value);
	color.a = shininess / 255.0;
	return color;
}

void main() {
	vec4 diffuse = diffuse(uDiffuse, vTexCoord0, uDiffuseOffset, uDiffuseScale);
	if (diffuse.a<uAlphaCutoff) {
		discard;
	}
	else {
		float le = lightEmission(uLightEmission, uLightEmissionMask, vTexCoord0, uNormalMapOffset, uNormalMapScale, uLightEmissionMaskInvert, uLightEmissionMaskChannel);
		
		vec3 normalVector = normal(vNormal,vTangent,vBitangent,uNormalMap,vTexCoord0,uNormalMapOffset,uNormalMapScale);
		vec4 cmColor = vec4(0.0,0.0,0.0,0.0);
		if (uUseCubemap) {
			cmColor = getCubeColor(normalVector);
			cmColor.a = 1.0f;
		}
		
		vec4 color = uColorTint * vec4(1.0 - uReflectionAmount);
		out_Diffuse = diffuse * color + cmColor * uReflectionAmount;
		out_Normal = vec4(normalVector * 0.5 + 0.5, le) ;
		out_Specular = specular(uSpecularColor, uShininess, uShininessMask, vTexCoord0, uDiffuseOffset, uDiffuseScale, uShininessMaskInvert, uShininessMaskComponent);
		out_Position = vec4(vPosition, gl_FragCoord.z);
		out_Selection = uSelected;
	}
}