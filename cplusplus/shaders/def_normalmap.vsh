#ifdef GL_ES
precision highp float;
#endif
attribute vec3 aVertexPos;
attribute vec3 aNormalPos;
attribute vec2 aNormalMapPos;
attribute vec2 aLightNormalMapPos;
attribute vec3 aTangentPos;

uniform mat4 uMMatrix;
uniform mat4 uVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uNMatrix;

varying vec2 vNormalMapPos;
varying vec2 vLightNormalMapPos;
varying vec3 vNormal;
varying vec3 vTangent;
varying vec3 vBitangent;

void main() {
	gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aVertexPos,1.0);
	vNormalMapPos = aNormalMapPos;
	vLightNormalMapPos = aLightNormalMapPos;
	vNormal = normalize((uNMatrix * vec4(aNormalPos,1.0)).rgb);
	vTangent = normalize((uNMatrix * vec4(aTangentPos,1.0)).rgb);
	vBitangent = cross(vNormal,vTangent);
}
