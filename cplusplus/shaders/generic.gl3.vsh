#version 150

in vec3 aVertexPosition;
in vec3 aVertexNormal;
in vec3 aVertexTangent;
in vec2 aTextureCoord;
in vec2 aLightmapCoord;


uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uNMatrix;
uniform mat4 uVMatrix;
uniform mat4 uMMatrix;
uniform mat4 uVMatrixInv;

out vec2 vTextureCoord;
out vec2 vLightmapCoord;
out vec3 vNormal;
out vec3 vTangent;
out vec3 vBitangent;
out vec3 vPosition;
out vec3 vSurfaceToView;


void main() {
	gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aVertexPosition,1.0);
	
	vNormal = normalize((uNMatrix * vec4(aVertexNormal,1.0)).rgb);
	vTangent = normalize((uNMatrix * vec4(aVertexTangent,1.0)).rgb);
	vBitangent = cross(vNormal,vTangent);
	
	vTextureCoord = aTextureCoord;
	vLightmapCoord = aLightmapCoord;
	vPosition = (uMVMatrix * vec4(aVertexPosition,1.0)).xyz;
	
	vSurfaceToView = (uVMatrixInv[3] - (uMMatrix * vec4(aVertexPosition,1.0))).xyz;
}
