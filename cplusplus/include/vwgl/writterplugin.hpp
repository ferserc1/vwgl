
#ifndef _vwgl_writterplugin_hpp_
#define _vwgl_writterplugin_hpp_

#include <vwgl/drawable.hpp>
#include <vwgl/readerplugin.hpp>
#include <vwgl/library/node.hpp>
#include <vwgl/dictionary.hpp>

#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/node.hpp>

namespace vwgl {

class WritterPlugin : public Plugin{
public:
	WritterPlugin() {}

	virtual bool acceptFileType(const std::string & path) = 0;

protected:
	virtual ~WritterPlugin() {}
};

class WriteDrawablePlugin : public WritterPlugin {
public:
	WriteDrawablePlugin() {}
	
	virtual bool writeDrawable(const std::string & path, Drawable * drawable) = 0;

protected:
	virtual ~WriteDrawablePlugin() {}
};

class WriteLibraryPlugin : public WritterPlugin {
public:
	WriteLibraryPlugin() {}
	
	virtual bool writeLibrary(const std::string & path, library::Node * library) = 0;
	
protected:
	virtual ~WriteLibraryPlugin() {}
};

class WriteNodePlugin : public WritterPlugin {
public:
	WriteNodePlugin() {}
	
	virtual bool writeNode(const std::string & path, Node * library) = 0;
	
protected:
	virtual ~WriteNodePlugin() {}
};
	
class WriteDictionaryPlugin : public WritterPlugin {
public:
	WriteDictionaryPlugin() {}
	
	virtual bool writeDictionary(const std::string & path, Dictionary * dict) = 0;
	
protected:
	virtual ~WriteDictionaryPlugin() {}
};

class WriteModelPlugin : public WritterPlugin {
public:
	WriteModelPlugin() {}
	
	virtual bool writeModel(const std::string & path, vwgl::scene::Drawable * drw) = 0;
	
protected:
	virtual ~WriteModelPlugin() {}
};

class WritePrefabPlugin : public WritterPlugin {
public:
	WritePrefabPlugin() {}
	
	virtual bool writePrefab(const std::string & path, vwgl::scene::Node * node) = 0;
	
protected:
	virtual ~WritePrefabPlugin() {}
};

class WriteScenePlugin : public WritterPlugin {
public:
	WriteScenePlugin() {}
	
	virtual bool writeScene(const std::string & path, vwgl::scene::Node * node) = 0;
	
protected:
	virtual ~WriteScenePlugin() {}
};

}

#endif
