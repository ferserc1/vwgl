
#ifndef _vwgl_draw_node_visitor_hpp_
#define _vwgl_draw_node_visitor_hpp_

#include <vwgl/node_visitor.hpp>
#include <vwgl/material.hpp>

namespace vwgl {

class VWGLEXPORT DrawNodeVisitor : public NodeVisitor {
public:
	DrawNodeVisitor();

	void setMaterial(Material * mat) { _material = mat; }
	Material * getMaterial() { return _material.getPtr(); }
	void setUseMaterialSettings(bool s) { _useMaterialSettings = s; }

	void setRenderOpaque(bool opaque) { _renderOpaque = opaque; }
	bool getRenderOpaque() const { return _renderOpaque; }
	
	void setRenderTransparent(bool transparent) { _renderTransparent = transparent; }
	bool getRenderTransparent() const { return _renderTransparent; }
	
	virtual void visit(Node * node);

protected:
	virtual ~DrawNodeVisitor();
	
	ptr<Material> _material;
	bool _useMaterialSettings;
	bool _renderOpaque;
	bool _renderTransparent;
};

}
#endif
