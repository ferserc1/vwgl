
#ifndef _vwgl_dynamic_cubemap_hpp_
#define _vwgl_dynamic_cubemap_hpp_

#include <vwgl/node.hpp>
#include <vwgl/cubemap.hpp>
#include <vwgl/fbo.hpp>
#include <vwgl/vector.hpp>
#include <vwgl/transform_visitor.hpp>
#include <vwgl/draw_node_visitor.hpp>
#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class VWGLEXPORT DynamicCubemapMaterial : public DeferredMaterial {
public:
	DynamicCubemapMaterial() { initShader(); }

	virtual void initShader();

protected:
	virtual void setupUniforms();
	
	static std::string s_vshader;
	static std::string s_fshader;
};

class VWGLEXPORT DynamicCubemap : public Node {
public:
	DynamicCubemap();

	void createCubemap(const vwgl::Size2Di & size);

	inline CubeMap * getCubeMap() { return _fbo->getCubeMap(); }
	
	void beginDraw();
	void drawToFace(Texture::TextureTarget target);
	void endDraw();
	
	// You can use this function directly instead the three previous functions
	void drawScene(vwgl::Node * sceneRoot);
	
	inline void setClearColor(const vwgl::Color & color) { _clearColor = color; }
	inline const vwgl::Color & getClearColor() const { return _clearColor; }
	
	inline int getWidth() const { return _fbo->getWidth(); }
	inline int getHeight() const { return _fbo->getHeight(); }
	
protected:
	virtual ~DynamicCubemap();
	
	ptr<FramebufferObject> _fbo;
	ptr<TransformVisitor> _trxVisitor;
	ptr<DrawNodeVisitor> _drawVisitor;
	Color _clearColor;
};

class VWGLEXPORT DynamicCubemapManager {
	friend class DynamicCubemap;
public:
	typedef std::vector<DynamicCubemap*> DynamicCubemapList;
	
	static DynamicCubemapManager * get();
	
	void update(Node * sceneRoot);
	
	inline void updateCubemaps() { _updateIterator = _cubeMapList.begin(); }
	
	void updateAllCubemaps(Node * sceneRoot);
	
	static const Size2Di & defaultCubeMapSize();
	static void setDefaultCubeMapSize(const Size2Di & s);
	
protected:
	DynamicCubemapManager();
	virtual ~DynamicCubemapManager();
	
	void addCubemap(DynamicCubemap * cm);
	void removeCubemap(DynamicCubemap * cm);

	static DynamicCubemapManager s_singleton;
	static Size2Di s_defaultCubeMapSize;
	
	DynamicCubemapList _cubeMapList;
	DynamicCubemapList::iterator _updateIterator;
	unsigned long long _frameCount;
};

}

#endif
