
#ifndef _vwgl_transformvisitor_hpp_
#define _vwgl_transformvisitor_hpp_

#include <vwgl/node_visitor.hpp>
#include <vwgl/math.hpp>

namespace vwgl {

class VWGLEXPORT TransformVisitor : public NodeVisitor {
public:
	TransformVisitor();

	virtual void visit(Node * node);

	const Matrix4 & getTransform() const { return _transform; }
	Matrix4 & getTransform() { return _transform; }

protected:
	virtual ~TransformVisitor();
	
	Matrix4 _transform;
};

}

#endif
