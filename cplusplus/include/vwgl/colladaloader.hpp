
#ifndef _vwgl_colladaloader_hpp_
#define _vwgl_colladaloader_hpp_

#include <vwgl/readerplugin.hpp>
#include <vwgl/collada/colladautils.hpp>

// API 1.0 collada to drawable plugin

namespace vwgl {
	
class LoadColladaException : public LoadDrawableException {
public:
	explicit LoadColladaException() :LoadDrawableException("Error loading collada file") {}
	explicit LoadColladaException(const std::string & msg) :LoadDrawableException(msg) {}
	virtual ~LoadColladaException() throw () {}
};

class VWGLEXPORT ColladaToDrawable {
public:
	typedef std::vector<ptr<collada::Geometry> > DaeGeometryList;
	ColladaToDrawable(collada::Collada * collada) { _collada = collada; }
	
	Drawable * buildDrawable();

	bool getNonTriangularWarning() { return _nonTriangularPlist; }

protected:
	ptr<collada::Collada> _collada;
	DaeGeometryList _geometryList;
	
	PolyList * getPolyList(collada::Polylist * c_plist, collada::Mesh * mesh);
	PolyList * getPolyList(collada::Triangles * c_triangles, collada::Mesh * mesh);
	
	void applyNodeSettings(collada::SceneNode * node, PolyList * plist);
	
	void fillInSources(collada::InputList & inputs, std::vector<collada::Source*> & sources, collada::Mesh * mesh);
	
	bool _nonTriangularPlist;
};

class VWGLEXPORT ColladaLoader : public ReadDrawablePlugin {
public:
	ColladaLoader();

	virtual bool acceptFileType(const std::string & path);
	
	virtual Drawable * loadDrawable(const std::string & path);

protected:
	virtual ~ColladaLoader();
};

}

#endif
