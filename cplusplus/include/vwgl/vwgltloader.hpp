
#ifndef _vwgl_vwgltreader_hpp_
#define _vwgl_vwgltreader_hpp_

#include <vwgl/readerplugin.hpp>

namespace vwgl {

class VWGLEXPORT VwgltLoader : public ReadDrawablePlugin {
public:
	VwgltLoader();

	virtual bool acceptFileType(const std::string & path);
	
	virtual Drawable * loadDrawable(const std::string & path);

protected:
	virtual ~VwgltLoader();
};

}

#endif
