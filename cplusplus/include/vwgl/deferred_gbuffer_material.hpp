#ifndef _VWGL_DEFERRED_GBUFFER_MATERIAL_HPP_
#define _VWGL_DEFERRED_GBUFFER_MATERIAL_HPP_

#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class DeferredGBufferMaterial : public DeferredMaterial {
public:
	DeferredGBufferMaterial();
	
	virtual void initShader();

	inline void setClearColor(const Color & col) { _clearColor = col;  }

protected:
	virtual ~DeferredGBufferMaterial();
	
	virtual void setupUniforms();

	Color _clearColor;
};

}

#endif