
#ifndef _vwgl_camera_hpp_
#define _vwgl_camera_hpp_

#include <vwgl/node.hpp>
#include <vwgl/transform_visitor.hpp>
#include <vwgl/math.hpp>
#include <vwgl/matrix_factory.hpp>

#include <vector>

namespace vwgl {

class VWGLEXPORT Camera : public Node {
public:
	Camera();

	Matrix4 & getProjectionMatrix() {
		if (_projectionMethod.valid()) _projectionMethod->updateMatrix();
		return _projection;
	}

	Matrix4 & getViewMatrix();

	vwgl::Matrix4 & getTransform();
	
	inline void setProjectionMethod(ProjectionFactoryMethod * method) {
		_projectionMethod = method;
		if (_projectionMethod.valid()) {
			_projectionMethod->setMatrixRef(&_projection);
		}
	}

	inline ProjectionFactoryMethod * getProjectionMethod() { return _projectionMethod.getPtr(); }

	const vwgl::Vector3 & getDirectionVector();

protected:
	virtual ~Camera();
	
	Matrix4 _projection;
	ptr<TransformVisitor> _transformVisitor;
	Matrix4 _fooTransform;
	ptr<ProjectionFactoryMethod> _projectionMethod;
	
	vwgl::Vector3 _direction;
};

class VWGLEXPORT CameraManager : Singleton {
	friend class Camera;
public:
	static CameraManager * get();

	Camera * getMainCamera() { return _mainCamera; }
	void setMainCamera(Camera * cam) { if (cam!=NULL) _mainCamera = cam; }
	
	void applyTransform(Camera * cam=NULL);

	virtual void finalize();

protected:
	typedef std::vector<Camera*> CameraList;

	CameraManager();
	virtual ~CameraManager();
	
	static CameraManager * s_singleton;
	
	void addCamera(Camera * cam);
	void removeCamera(Camera * cam);
	
	CameraList _cameraList;
	Camera * _mainCamera;
};

}

#endif