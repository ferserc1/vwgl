
#ifndef _vwgl_drawable_hpp_
#define _vwgl_drawable_hpp_

#include <vwgl/solid.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/matrix.hpp>
#include <vwgl/projector.hpp>
#include <vwgl/dynamic_cubemap.hpp>
#include <vwgl/physics/joint.hpp>
#include <vwgl/boundingbox.hpp>

namespace vwgl {

class DrawableNode;
	
class VWGLEXPORT DrawableBase : public ReferencedPointer {
	friend class DrawableNode;
public:
	DrawableBase();
	DrawableBase(const std::string & name);
	
	inline void setName(const std::string & name) { _name = name; }
	inline const std::string & getName() const { return _name; }
	
	virtual const SolidList & getSolidList() const = 0;
	virtual SolidList & getSolidList() = 0;
	
	virtual void addSolid(Solid * solid) = 0;
	
	virtual Solid * getSolid(const std::string & plistName) = 0;
	virtual PolyList * getPolyList(const std::string & name) = 0;
	
	virtual BoundingBox * boundingBox() = 0;
	
	virtual void switchUVs(PolyList::VertexBufferType uv1, PolyList::VertexBufferType uv2) = 0;
	virtual void setMaterial(Material * mat) = 0;
	
	virtual Material * getMaterial() = 0;
	GenericMaterial * getGenericMaterial() { return dynamic_cast<GenericMaterial*>(getMaterial()); }
	virtual void setCubemap(vwgl::CubeMap * cm) = 0;
	
	virtual void init() = 0;
	
	virtual void draw(Material * mat = NULL, bool useMaterialSettings = false) = 0;
	
	virtual void setRenderOpaque(bool opaque) = 0;
	virtual bool getRenderOpaque() const = 0;
	
	virtual void setRenderTransparent(bool transparent) = 0;
	virtual bool getRenderTransparent() const = 0;
	
	inline void show() { _hidden = false; }
	inline void hide() { _hidden = true; }
	inline void setHidden(bool h) { _hidden = h; }
	inline bool isHidden() const { return _hidden; }
	
	inline DrawableNode * getParent() { return _parent; }
	
	virtual void applyTransform(const vwgl::Matrix4 & transform) = 0;
	virtual void flipFaces() = 0;
	virtual void flipNormals() = 0;
	
	virtual void setShadowProjector(Projector * proj) = 0;
	virtual Projector * getShadowProjector() = 0;
	
	virtual void hideGroup(const std::string & groupName) = 0;
	virtual void showGroup(const std::string & groupName) = 0;
	virtual void showByName(const std::string & solidName) = 0;
	virtual void hideByName(const std::string & solidName) = 0;
	
	virtual void setInputJoint(physics::Joint * joint) = 0;
	virtual void setOutputJoint(physics::Joint * joint) = 0;
	virtual physics::Joint * getInputJoint() = 0;
	virtual const physics::Joint * getInputJoint() const = 0;
	virtual physics::Joint * getOutputJoint() = 0;
	virtual const physics::Joint * getOutputJoint() const = 0;
	
	virtual void initJoints() = 0;
	
protected:
	virtual ~DrawableBase() {}
	
	std::string _name;
	bool _hidden;
	DrawableNode * _parent;
	
};

class VWGLEXPORT Drawable : public DrawableBase {
	friend class DrawableNode;
public:
	Drawable();
	Drawable(const std::string & name);
	
	inline void setIdentifier(int id) { _identifier = id; }
	inline int getIdentifier() const { return _identifier; }

	const SolidList & getSolidList() const { return _solidList; }
	SolidList & getSolidList() { return _solidList; }
	
	void addSolid(Solid * solid) {
		solid->_drawable = this;
		_solidList.push_back(solid);
		updateBoundingBox();
	}

	Solid * getSolid(const std::string & plistName);
	PolyList * getPolyList(const std::string & name);
	
	inline BoundingBox * boundingBox() { return _bbox.getPtr(); }
	
	void switchUVs(PolyList::VertexBufferType uv1, PolyList::VertexBufferType uv2);

	// Apply material to all solids
	void setMaterial(Material * mat);
	
	// Material utility functions
	// Get first material in solid list
	Material * getMaterial();
	void setCubemap(vwgl::CubeMap * cm);

	virtual void init();

	void draw(Material * mat = NULL, bool useMaterialSettings = false);
	
	void destroy();
	
	void setRenderOpaque(bool opaque) { _renderOpaque = opaque; }
	bool getRenderOpaque() const { return _renderOpaque; }
	
	void setRenderTransparent(bool transparent) { _renderTransparent = transparent; }
	bool getRenderTransparent() const { return _renderTransparent; }
	
	DrawableNode * getParent() { return _parent; }
	
	void applyTransform(const vwgl::Matrix4 & transform);
	void flipFaces();
	void flipNormals();
	
	virtual void setShadowProjector(Projector * proj);
	Projector * getShadowProjector() { return _shadowProjector.getPtr(); }
	
	void hideGroup(const std::string & groupName);
	void showGroup(const std::string & groupName);
	void showByName(const std::string & solidName);
	void hideByName(const std::string & solidName);

	inline void setInputJoint(physics::Joint * joint) { _inputJoint = joint; setupJointMaterial(_inputJoint.getPtr()); }
	inline void setOutputJoint(physics::Joint * joint) { _outputJoint = joint; setupJointMaterial(_outputJoint.getPtr()); }
	inline physics::Joint * getInputJoint() { return _inputJoint.getPtr(); }
	inline const physics::Joint * getInputJoint() const { return _inputJoint.getPtr(); }
	inline physics::Joint * getOutputJoint() { return _outputJoint.getPtr(); }
	inline const physics::Joint * getOutputJoint() const { return _outputJoint.getPtr(); }

	inline void initJoints() {
		if (_inputJoint.valid() && !_inputJoint->getDrawable()) {
			_inputJoint->initDrawable();
			setupJointMaterial(_inputJoint.getPtr());
		}
		if (_outputJoint.valid() && !_outputJoint->getDrawable()) {
			_outputJoint->initDrawable();
			setupJointMaterial(_outputJoint.getPtr());
		}
	}

protected:
	virtual ~Drawable();
	
	static int s_identifier;
	static int createIdentifier();
	
	int _identifier;

	SolidList _solidList;
	bool _renderOpaque;
	bool _renderTransparent;
	
	ptr<BoundingBox> _bbox;
	ptr<DynamicCubemap> _cubeMap;
	ptr<physics::Joint> _inputJoint;
	ptr<physics::Joint> _outputJoint;
	ptr<Projector> _shadowProjector;
	
	void addProjectorToScene();
	void removeProjectorFromScene();
	bool needsCubeMap();
	void createCubeMap();
	void removeCubeMap();
	void addToScene(Node * n, Node * parent);
	void setupJointMaterial(physics::Joint * joint);
	
	inline void updateBoundingBox() {
		if (!_bbox.valid()) {
			_bbox = new BoundingBox();
		}
		_bbox->reset();
		_bbox->addVertexData(this);
	}
};
	
class DrawableProxy : public DrawableBase {
	friend class DrawableNode;
public:
	DrawableProxy(int reference) :DrawableBase(), _reference(reference) {}
	DrawableProxy(const std::string & name, int reference) :DrawableBase(name), _reference(reference) {}
	DrawableProxy(Drawable * drawable) :DrawableBase(), _drawable(drawable) {}
	DrawableProxy(const std::string & name, Drawable * drawable) :DrawableBase(name), _drawable(drawable) {}
	
	int getDrawableReference() const { return _reference; }
	virtual void setDrawable(Drawable * drw) { _drawable = drw; }
	Drawable * getDrawable() { return _drawable.getPtr(); }
	
	virtual const SolidList & getSolidList() const { return _drawable.valid() ? _drawable->getSolidList():_emptySolidList; };
	virtual SolidList & getSolidList() { return _drawable.valid() ? _drawable->getSolidList():_emptySolidList; }
	
	virtual void addSolid(Solid * solid) { if (_drawable.valid()) _drawable->addSolid(solid); }
	
	virtual Solid * getSolid(const std::string & plistName) { return _drawable.valid() ? _drawable->getSolid(plistName):nullptr; }
	PolyList * getPolyList(const std::string & name) { return _drawable.valid() ? _drawable->getPolyList(name):nullptr; }
	
	BoundingBox * boundingBox() { return _drawable.valid() ? _drawable->boundingBox():nullptr; }
	
	void switchUVs(PolyList::VertexBufferType uv1, PolyList::VertexBufferType uv2) { if (_drawable.valid()) _drawable->switchUVs(uv1, uv2); }
	void setMaterial(Material * mat) { if (_drawable.valid()) _drawable->setMaterial(mat); }
	
	Material * getMaterial() { return _drawable.valid() ? _drawable->getMaterial():nullptr; }
	void setCubemap(vwgl::CubeMap * cm) { if (_drawable.valid()) _drawable->setCubemap(cm); }
	
	void init() { if (_drawable.valid()) _drawable->init(); }
	
	void draw(Material * mat = NULL, bool useMaterialSettings = false) { if (!_hidden && _drawable.valid()) _drawable->draw(mat,useMaterialSettings); }
	
	void setRenderOpaque(bool opaque) { if (_drawable.valid()) _drawable->setRenderOpaque(opaque); }
	bool getRenderOpaque() const { return _drawable.valid() ? _drawable->getRenderOpaque():false; }
	
	void setRenderTransparent(bool transparent) { if (_drawable.valid()) _drawable->setRenderTransparent(transparent); }
	bool getRenderTransparent() const { return _drawable.valid() ? _drawable->getRenderTransparent():false; }
	
	void applyTransform(const vwgl::Matrix4 & transform) { if (_drawable.valid()) _drawable->applyTransform(transform); }
	void flipFaces() { if (_drawable.valid()) _drawable->flipFaces(); }
	void flipNormals() { if (_drawable.valid()) _drawable->flipNormals(); }
	
	void setShadowProjector(Projector * proj) { if (_drawable.valid()) _drawable->setShadowProjector(proj); }
	Projector * getShadowProjector() { return _drawable.valid() ? _drawable->getShadowProjector():nullptr; }
	
	void hideGroup(const std::string & groupName) { if (_drawable.valid()) _drawable->hideGroup(groupName); }
	void showGroup(const std::string & groupName) { if (_drawable.valid()) _drawable->showGroup(groupName); }
	void showByName(const std::string & solidName) { if (_drawable.valid()) _drawable->showByName(solidName); }
	void hideByName(const std::string & solidName) { if (_drawable.valid()) _drawable->hideByName(solidName); }
	
	void setInputJoint(physics::Joint *) {  }
	void setOutputJoint(physics::Joint *) {  }
	physics::Joint * getInputJoint() { return _drawable.valid() ? _drawable->getInputJoint():nullptr; }
	const physics::Joint * getInputJoint() const { return _drawable.valid() ? _drawable->getInputJoint():nullptr; }
	physics::Joint * getOutputJoint() { return _drawable.valid() ? _drawable->getOutputJoint():nullptr; }
	const physics::Joint * getOutputJoint() const { return _drawable.valid() ? _drawable->getOutputJoint():nullptr; }
	
	void initJoints() { }
	
protected:
	virtual ~DrawableProxy() {}
	
	int _reference;
	ptr<Drawable> _drawable;
	SolidList _emptySolidList;
};
	
class VWGLEXPORT DrawableInstance : public Drawable {
public:
	DrawableInstance(int reference) :Drawable(), _reference(reference) {}
	DrawableInstance(const std::string & name, int reference) :Drawable(name), _reference(reference) {}
	DrawableInstance(Drawable * drawable) :Drawable() { setDrawable(drawable); }
	DrawableInstance(const std::string & name, Drawable * drawable) :Drawable(name) { setDrawable(drawable); }
	
	virtual void setDrawable(Drawable * drw);

protected:
	virtual ~DrawableInstance() {}
	
	int _reference;
	ptr<Drawable> _sourceDrawable;
};

}

#endif
