
#ifndef _vwgl_rendercanvas_hpp_
#define _vwgl_rendercanvas_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/solid.hpp>

namespace vwgl {

class VWGLEXPORT RenderCanvas : public ReferencedPointer {
public:
	RenderCanvas();

	void setClearBuffer(bool clear) { _clear = clear; }
	bool getClearBuffer() { return _clear; }

	void setMaterial(Material * m) { _material = m; }
	Material * getMaterial() { return _material.getPtr(); }
	
	void draw(int w, int h);

protected:
	virtual ~RenderCanvas();
	
	void buildCanvas();
	
	ptr<Material> _material;
	ptr<PolyList> _plist;
	Matrix4 _projection;
	bool _clear;
};

	
class VWGLEXPORT HomogeneousRenderCanvas : public RenderCanvas {
public:
	HomogeneousRenderCanvas();
	
	void setMaterial(Material * m) { _material = m; }
	Material * getMaterial() { return _material.getPtr(); }
		
	void draw(int w, int h);

	void destroy();

protected:
	virtual ~HomogeneousRenderCanvas();

	ptr<Material> _material;
	ptr<PolyList> _plist;
	Matrix4 _projection;
	
	
};

}

#endif
