
#ifndef _vwgl_sceneset_hpp_
#define _vwgl_sceneset_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/group.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/shadow_light.hpp>
#include <vwgl/camera.hpp>

#include <unordered_map>

namespace vwgl {

class VWGLEXPORT SceneSet : public ReferencedPointer {
public:
	typedef std::unordered_map<std::string, ptr<Light> > LightList;
	typedef std::unordered_map<std::string, ptr<Camera> > CameraList;

	SceneSet();

	void buildDefault(Size2Di shadowMapSize = Size2Di(512));
	
	void setScene(Node * root);

	void resize(const Viewport & vp);

	bool addLight(const std::string & name, Light * light, Matrix4 transform);
	bool addCamera(const std::string & name, Camera * cam, Matrix4 transform);

	Light * getLight(const std::string & name);
	Light * getMainLight();
	Camera * getCamera(const std::string & name);
	Camera * getMainCamera() { return CameraManager::get()->getMainCamera(); }
	TransformNode * getLightTransform(const std::string & name);
	TransformNode * getMainLightTransform();
	TransformNode * getCameraTransform(const std::string & name);
	TransformNode * getMainCameraTransform();

	bool removeLight(const std::string & name);
	bool removeCamera(const std::string &name);

	Group * getSceneRoot() { return _sceneRoot.getPtr(); }

	LightList & getLightList() { return _lights; }
	CameraList & getCameraList() { return _cameras; }

protected:
	virtual ~SceneSet();

	LightList _lights;
	CameraList _cameras;
	ptr<Group> _sceneRoot;
};

}

#endif
