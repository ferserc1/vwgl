
#ifndef _VWGL_GRAPHICS_H_
#define _VWGL_GRAPHICS_H_

#include <vwgl/system.hpp>

namespace vwgl {

class VWGLEXPORT Graphics {
public:
	enum Api {
		kApiOpenGLBasic = 1,
		kApiOpenGL,
		kApiOpenGLAdvanced,
		kApiDirectX
	};

	static Graphics * get() {
		if (s_singleton == nullptr) {
			s_singleton = new Graphics();
		}
		return s_singleton;
	}

	bool useApi(Api api);
	bool useApi(System::Platform platform, Api api);

	inline Api getApi() const { return _api;  }

	void initContext();

	bool isAvailable(Api api);

protected:
	Graphics();
	virtual ~Graphics();

	static Graphics * s_singleton;

	Api _api;
};

}

#endif
