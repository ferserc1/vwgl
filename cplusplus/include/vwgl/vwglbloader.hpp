
#ifndef _vwgl_vwglbreader_hpp_
#define _vwgl_vwglbreader_hpp_

#include <vwgl/readerplugin.hpp>
#include <vwgl/vwglbutils.hpp>

namespace vwgl {

class VWGLEXPORT  VwglbLoader : public ReadDrawablePlugin {
public:
	struct FileVersion {
		unsigned char major;
		unsigned char minor;
		unsigned char revision;
	};
	VwglbLoader();

	virtual bool acceptFileType(const std::string & path);
	
	virtual Drawable * loadDrawable(const std::string & path);
	virtual void getDependencies(const std::string & path, std::vector<std::string> & deps);

protected:
	virtual ~VwglbLoader();
	
	void readHeader(Drawable * drawable, FileVersion & version, int & numberOfPlist, std::string &materials, std::string & projectorTexture);
	void readHeader(FileVersion & version, int & numberOfPlist, std::string & materials, std::string & projectorTexture);
	void readPolyLists(Drawable * drawable, int numberOfPlist);
	void readSinglePolyList(Drawable * drawable);
	void applyMaterials(Drawable * drawable, const std::string & strData);
	void applyProjectors(Drawable * drawable, const std::string & strData);
	
	VwglbUtils _fileUtils;
};

}

#endif
