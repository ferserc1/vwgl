
#ifndef _vwgl_ssao_hpp_
#define _vwgl_ssao_hpp_

#include <vwgl/deferredmaterial.hpp>
#include <vwgl/texture.hpp>

namespace vwgl {

#define VWGL_SSAO_MAX_KERNEL_SIZE 64

class VWGLEXPORT SSAOMaterial : public DeferredMaterial {
public:
	SSAOMaterial();

	virtual void initShader();
	
	inline void setPositionMap(Texture * map) { _positionMap = map; }
	inline void setNormalMap(Texture * map) { _normalMap = map; }

	inline void setKernelSize(int size) { _kernelSize = size<=s_maxKernelSize ? size:s_maxKernelSize; }
	inline void setSampleRadius(float r) { _sampleRadius = r; }
	inline void setColor(const Color & c) { _color = c; }
	inline void setViewportSize(Size2D size) { _viewportSize = size; }
	inline void setEnabled(bool e) { _enabled = e; }
	inline void setBlurIterations(int it) { _blur = it; }
	inline void setMaxDistance(float dist) { _maxDistance = dist; }
	
	inline int getKernelSize() const { return _kernelSize; }
	inline float getSampleRadius() const { return _sampleRadius; }
	inline const Color & getColor() const { return _color; }
	inline bool isEnabled() const { return _enabled; }
	inline int getBlurIterations() const { return _blur; }
	inline float getMaxDistance() const { return _maxDistance; }
	
	Texture * getRandomTexture();
	inline void setRandomTexture(Texture * tex) { _randomTexture = tex; }
	

public:
	virtual ~SSAOMaterial();
	
	virtual void setupUniforms();
	
	ptr<Texture> _positionMap;
	ptr<Texture> _normalMap;
	ptr<Texture> _randomTexture;
	
	bool _enabled;

	int _currentKernelSize;
	int _kernelSize;
	Vector2 _viewportSize;
	float _sampleRadius;
	static const int s_maxKernelSize;
	float _kernelOffsets[VWGL_SSAO_MAX_KERNEL_SIZE * 3];
	Color _color;
	float _maxDistance;
	
	// This parameter is stored here but processed in deferred mix material
	int _blur;
	
	

};

}

#endif
