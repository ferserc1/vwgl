
#ifndef _VWGL_DICTIONARY_HPP_
#define _VWGL_DICTIONARY_HPP_

#include <vwgl/referenced_pointer.hpp>
#include <string>
#include <vector>
#include <unordered_map>

namespace vwgl {

class Dictionary : public ReferencedPointer {
public:
	enum ValueType {
		kTypeNull = 0,
		kTypeString = 1,
		kTypeBool,
		kTypeNumber,
		kTypeArray,
		kTypeObject,
	};
	typedef std::vector<ptr<Dictionary> > DictionaryArray;
	typedef std::unordered_map<std::string,ptr<Dictionary> > DictionaryMap;
	
	Dictionary() :_stringValue(""), _boolValue(false), _numberValue(0.0), _valueType(kTypeNull) {}
	Dictionary(bool value) :Dictionary() { _boolValue = value; _valueType = kTypeBool; }
	Dictionary(const char * value) :Dictionary(std::string(value)) {}
	Dictionary(const std::string & value) :Dictionary() { _stringValue =value; _valueType = kTypeString; }
	Dictionary(char value) :Dictionary() { _stringValue = value; _valueType = kTypeString; }
	Dictionary(short value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(unsigned short value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(int value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(unsigned int value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(long value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(unsigned long value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(long long value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(unsigned long long value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(float value) :Dictionary() { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	Dictionary(double value) :Dictionary() { _numberValue = value; _valueType = kTypeNumber; }
	Dictionary(const DictionaryArray & value) :Dictionary() { _arrayValue = value; _valueType = kTypeArray; }
	Dictionary(const DictionaryMap & value) :Dictionary() { _objectValue = value; _valueType = kTypeObject; }
	
	inline void set(bool value) { _boolValue = value; _valueType = kTypeBool; }
	inline void set(const std::string & value) { _stringValue =value; _valueType = kTypeString; }
	inline void set(char value) { _stringValue = value; _valueType = kTypeString; }
	inline void set(short value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(unsigned short value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(int value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(unsigned int value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(long value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(unsigned long value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(long long value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(unsigned long long value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(float value) { _numberValue = static_cast<double>(value); _valueType = kTypeNumber; }
	inline void set(double value) { _numberValue = value; _valueType = kTypeNumber; }
	inline void set(const DictionaryArray & value) { _arrayValue = value; _valueType = kTypeArray; }
	inline void set(const DictionaryMap & value) { _objectValue = value; _valueType = kTypeObject; }
	inline void setNull() { _valueType = kTypeNull; }
	
	inline ValueType getType() const { return _valueType; }
	inline bool isString() const { return _valueType == kTypeString; }
	inline bool isBoolean() const { return _valueType == kTypeBool; }
	inline bool isNumber() const { return _valueType == kTypeNumber; }
	inline bool isArray() const { return _valueType == kTypeArray; }
	inline bool isObject() const { return _valueType == kTypeObject; }
	
	inline bool getBoolean() const { return _boolValue; }
	inline const std::string & getString() const { return _stringValue; }
	inline double getNumber() const { return _numberValue; }
	inline const DictionaryArray & getArray() const { return _arrayValue; }
	inline DictionaryArray & getArray() { _valueType = kTypeArray; return _arrayValue; }
	inline const DictionaryMap & getObject() const { return _objectValue; }
	inline DictionaryMap & getObject() { _valueType = kTypeObject; return _objectValue; }
	
protected:
	virtual ~Dictionary() {}
	
	std::string _stringValue;
	bool _boolValue;
	double _numberValue;
	DictionaryArray _arrayValue;
	DictionaryMap _objectValue;
	
	ValueType _valueType;
};

}

#endif
