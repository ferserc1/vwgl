#ifndef _VWGL_DIRECTX_HPP_
#define _VWGL_DIRECTX_HPP_

#include <vwgl/system.hpp>

#if VWGL_DIRECTX_AVAILABLE==1

#include <Windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>

//#pragma comment(lib,"d3d11.lib")
//#pragma comment(lib,"d3dx11.lib")
//#pragma comment(lib,"d3d10.lib")

#endif

#endif
