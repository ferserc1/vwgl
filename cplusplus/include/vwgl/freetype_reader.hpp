#ifndef _vwgl_freetype_reader_hpp_
#define _vwgl_freetype_reader_hpp_

#include <vwgl/readerplugin.hpp>

namespace vwgl {
	
class VWGLEXPORT FreetypeReader : public ReadFontPlugin {
public:
	FreetypeReader() {}
	
	bool acceptFileType(const std::string & path);
	
	vwgl::TextFont * loadFont(const std::string & path, int size, int res);
	
protected:
	virtual ~FreetypeReader() {}
	
	vwgl::TextFont * doLoadFont(const std::string & path, int size, int res);
	
	void getFontNameWithPath(const std::string & path, std::string & name);
};

}

#endif
