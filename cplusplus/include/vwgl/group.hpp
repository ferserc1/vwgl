#ifndef _vwgl_group_hpp_
#define _vwgl_group_hpp_

#include <vwgl/node.hpp>

#include <vector>

namespace vwgl {

class VWGLEXPORT Group : public Node {
public:
	Group();
	
	void addChild(Node * child);
	void removeChild(Node * child);
	
	NodeList & getNodeList() { return _children; }
	const NodeList & getNodeList() const { return _children; }

	inline bool haveChild(Node * node) { return std::find(_children.begin(), _children.end(), node)!=_children.end(); }

	Node * nextChildOf(Node * node);
	Node * prevChildOf(Node * node);
	
	bool swapChildren(Node * childA, Node * childB);
	
	bool moveChildNextTo(Node * nextToNode, Node * movingNode);

protected:
	virtual ~Group();
	
	NodeList _children;
};

}

#endif
