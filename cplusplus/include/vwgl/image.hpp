
#ifndef _vwgl_image_hpp_
#define _vwgl_image_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/vector.hpp>
#include <string>

namespace vwgl {

class VWGLEXPORT Image : public ReferencedPointer {
public:
	enum ImageFormat {
		kFormatNone,
		kFormatRGB,
		kFormatRGBA
	};
	Image();
	Image(const std::string & path);
	Image(const Image * clone);
	Image(unsigned char * buffer, const Size2Di size, int bytesPerPixel, ImageFormat fmt)
		:_buffer(buffer), _width(size.width()), _height(size.height()), _bytesPerPixel(bytesPerPixel), _format(fmt), _lastStatus(true)
	{
	}

	Image * load(const std::string & path);

	Image * saveBMP(const std::string & path);
	Image * saveTGA(const std::string & path);
	
	void setData(unsigned char * buffer, const Size2Di size, int bytesPerPixel, ImageFormat fmt);

	const unsigned char * getBuffer() const { return _buffer; }
	void setBuffer(unsigned char * buffer) { if (_buffer) destroy(); _buffer = buffer; _lastStatus = true; }
	ImageFormat getImageFormat() const { return _format; }
	void setImageFormat(ImageFormat format) { _format = format; }
	unsigned int getBytesPerPixel() const { return _bytesPerPixel; }
	void setBytesPerPixel(unsigned int bpp) { _bytesPerPixel = bpp; }
	unsigned int getWidth() const { return _width; }
	void setWidth(unsigned int w) { _width = w; }
	unsigned int getHeight() const { return _height; }
	void setHeight(unsigned int h) { _height = h; }
	
	bool valid() const { return _buffer!=NULL; }
	bool status() const { return _lastStatus; }
	
	const std::string & getFileName() const { return _fileName; }
	void setFileName(const std::string & name) { _fileName = name; }

protected:
	virtual ~Image();
	
	unsigned char * _buffer;
	ImageFormat _format;
	unsigned int _bytesPerPixel;
	unsigned int _width;
	unsigned int _height;
	std::string _fileName;
	
	bool _lastStatus;

	void destroy();
	
	void save(const std::string & path, int format);
};

}

#endif
