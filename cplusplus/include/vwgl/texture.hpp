
#ifndef _vwgl_texture_hpp_
#define _vwgl_texture_hpp_

#include <vwgl/opengl.hpp>
#include <vwgl/image.hpp>
#include <vwgl/vector.hpp>

#include <string>

namespace vwgl {

class VWGLEXPORT Texture : public ReferencedPointer {
public:
	Texture();
	
	enum TextureTarget {
	#ifdef VWGL_OPENGL_ES
		kTargetTexture1D		= 0,
	#else
		kTargetTexture1D		= GL_TEXTURE_1D,
	#endif
		
		kTargetTexture2D		= GL_TEXTURE_2D,
		
	#ifdef VWGL_OPENGL_ES
		kTargetTexture3D		= 1,
	#else
		kTargetTexture3D		= GL_TEXTURE_3D,
	#endif
	
		kTargetTextureCubeMap	= GL_TEXTURE_CUBE_MAP,
		kTexturePositiveXFace	= GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		kTextureNegativeXFace	= GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		kTexturePositiveYFace	= GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		kTextureNegativeYFace	= GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		kTexturePositiveZFace	= GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		kTextureNegativeZFace	= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};
	
	enum TextureFilter {
		kFilterNearestMipmapNearest = GL_NEAREST_MIPMAP_NEAREST,
		kFilterLinearMipmapNearest = GL_LINEAR_MIPMAP_NEAREST,
		kFilterNearestMipmapLinear = GL_NEAREST_MIPMAP_LINEAR,
		kFilterLinearMipmapLinear = GL_LINEAR_MIPMAP_LINEAR,
		kFilterNearest = GL_NEAREST,
		kFilterLinear = GL_LINEAR
	};
	
	enum TextureWrap {
		kWrapRepeat = GL_REPEAT,
	#ifdef VWGL_OPENGL_ES
		kWrapClamp = GL_CLAMP_TO_EDGE,
		kWrapClampToBorder = GL_CLAMP_TO_EDGE,
	#else
		kWrapClamp = GL_CLAMP_TO_EDGE,
		kWrapClampToBorder = GL_CLAMP_TO_BORDER,
	#endif
		kWrapClampToEdge = GL_CLAMP_TO_EDGE
	};
	
	enum TextureUnit {
		kTexture0 = GL_TEXTURE0,
		kTexture1 = GL_TEXTURE1,
		kTexture2 = GL_TEXTURE2,
		kTexture3 = GL_TEXTURE3,
		kTexture4 = GL_TEXTURE4,
		kTexture5 = GL_TEXTURE5,
		kTexture6 = GL_TEXTURE6,
		kTexture7 = GL_TEXTURE7,
		kTexture8 = GL_TEXTURE8,
		kTexture9 = GL_TEXTURE9,
		kTexture10 = GL_TEXTURE10,
		kTexture11 = GL_TEXTURE11,
		kTexture12 = GL_TEXTURE12,
		kTexture13 = GL_TEXTURE13,
		kTexture14 = GL_TEXTURE14,
		kTexture15 = GL_TEXTURE15,
		kTexture16 = GL_TEXTURE16,
		kTexture17 = GL_TEXTURE17,
		kTexture18 = GL_TEXTURE18,
		kTexture19 = GL_TEXTURE19,
		kTexture20 = GL_TEXTURE20,
		kTexture21 = GL_TEXTURE21,
		kTexture22 = GL_TEXTURE22,
		kTexture23 = GL_TEXTURE23,
		kTexture24 = GL_TEXTURE24,
		kTexture25 = GL_TEXTURE25,
		kTexture26 = GL_TEXTURE26,
		kTexture27 = GL_TEXTURE27,
		kTexture28 = GL_TEXTURE28,
		kTexture29 = GL_TEXTURE29,
		kTexture30 = GL_TEXTURE30
	};
	
	static void setActiveTexture(TextureUnit target);

	void createTexture();
	void bindTexture(TextureTarget target);
	void setMinFilter(TextureTarget target, TextureFilter filter);
	void setMagFilter(TextureTarget target, TextureFilter filter);
	void setTextureWrapS(TextureTarget target, TextureWrap wrap);
	void setTextureWrapT(TextureTarget target, TextureWrap wrap);
	void setTextureWrapR(TextureTarget target, TextureWrap wrap);
	void setImage(TextureTarget target, const Image * img);
	
	static void unbindTexture(TextureTarget target);
	
	void destroy();
	
	bool valid() const { return _textureName!=0; }
	
	GLuint getTextureName() const { return _textureName; }
	
	const std::string & getFileName() const { return _fileName; }
	void setFileName(const std::string & fileName) { _fileName = fileName; }
	
	inline int getWidth() const { return _width;  }
	inline int getHeight() const { return _height; }

	static Texture * getRandomTexture();
	static int getRandomTextureSize();

	static Texture * getColorTexture(const vwgl::Color & color, const vwgl::Size2Di & size);

protected:
	virtual ~Texture();
	
	GLuint _textureName;
	TextureFilter _minFilter;
	TextureFilter _magFilter;
	std::string _fileName;
	int _width;
	int _height;
};

}

#endif