
#ifndef _vwgl_readerplugin_hpp_
#define _vwgl_readerplugin_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/exceptions.hpp>

#include <vwgl/library/node.hpp>

#include <vwgl/dictionary.hpp>

#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/node.hpp>

#include <vwgl/text_font.hpp>

namespace vwgl {

class LoaderException : public FileException {
public:
	explicit LoaderException() :FileException("Error loading file") {}
	explicit LoaderException(const std::string & msg) :FileException(msg) {}
	virtual ~LoaderException() throw() {}
};

class LoadDrawableException : public LoaderException {
public:
	explicit LoadDrawableException() : LoaderException("Error loading drawable file") {}
	explicit LoadDrawableException(const std::string & msg) :LoaderException(msg) {}
	virtual ~LoadDrawableException() throw () {}
};

class VWGLEXPORT Plugin : public ReferencedPointer {
public:
	Plugin() {}
	
	void getFileExtension(const std::string & path, std::string & extension);
	void removeLastPathComponent(const std::string & filePath, std::string & path);
	void getFileName(const std::string & path, std::string & fileName);
	void toLower(std::string & str);

	
	std::vector<std::string> & getWarningMessages() { return _warnings; }

protected:
	virtual ~Plugin() {}
	
	void clearWarningMessages() { _warnings.clear(); }
	void addWarningMessage(const std::string & warn) { _warnings.push_back(warn); }

	std::vector<std::string> _warnings;
};

class ReaderPlugin : public Plugin {
public:
	ReaderPlugin() {}

	virtual bool acceptFileType(const std::string & path) = 0;
    virtual void getDependencies(const std::string & /* path */, std::vector<std::string> & /* deps */) {}

protected:
	virtual ~ReaderPlugin() {}
};

class ReadDrawablePlugin : public ReaderPlugin {
public:
	ReadDrawablePlugin() {}
	
	virtual Drawable * loadDrawable(const std::string & path) = 0;

protected:
	virtual ~ReadDrawablePlugin() {}

};

class ReadLibraryPlugin : public ReaderPlugin {
public:
	ReadLibraryPlugin() {}
	
	virtual library::Node * loadLibrary(const std::string & path) = 0;

protected:
	virtual ~ReadLibraryPlugin() {}
};
	
class ReadNodePlugin : public ReaderPlugin {
public:
	ReadNodePlugin() {}
	
	virtual Node * loadNode(const std::string & path) = 0;
	
protected:
	virtual ~ReadNodePlugin() {}
};
	
class ReadDictionaryPlugin : public ReaderPlugin {
public:
	ReadDictionaryPlugin() {}
	
	virtual Dictionary * loadDictionary(const std::string & path) = 0;

protected:
	virtual ~ReadDictionaryPlugin() {}
};
	
// API 2.0: model
class ReadModelPlugin : public ReaderPlugin {
public:
	ReadModelPlugin() {}
	
	virtual vwgl::scene::Drawable * loadModel(const std::string & path) = 0;
	
protected:
	virtual ~ReadModelPlugin() {}
};

// API 2.0: prefab.
class ReadPrefabPlugin : public ReaderPlugin {
public:
	ReadPrefabPlugin() {}
	
	virtual vwgl::scene::Node * loadPrefab(const std::string & path) = 0;
	
protected:
	virtual ~ReadPrefabPlugin() {}
};
	
// API 2.0: scene
class ReadScenePlugin : public ReaderPlugin {
public:
	ReadScenePlugin() {}
	
	virtual vwgl::scene::Node * loadScene(const std::string & path) = 0;
	
protected:
	virtual ~ReadScenePlugin() {}
};
	
// API 2.0: font
class ReadFontPlugin : public ReaderPlugin {
public:
	ReadFontPlugin() {}
	
	virtual vwgl::TextFont * loadFont(const std::string & path, int size = 50, int res = 100) = 0;
	
};

}

#endif
