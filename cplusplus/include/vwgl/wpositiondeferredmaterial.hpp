
#ifndef _vwgl_wpositiondeferredmaterial_hpp_
#define _vwgl_wpositiondeferredmaterial_hpp_

#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class VWGLEXPORT WPositionDeferredMaterial : public DeferredMaterial {
public:
	WPositionDeferredMaterial();

	virtual void initShader();
	
protected:
	virtual ~WPositionDeferredMaterial();
	
	virtual void setupUniforms();
};

}

#endif
