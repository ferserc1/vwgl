//
//  scene_commands.hpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 17/3/15.
//  Copyright (c) 2015 Vitaminew CB. All rights reserved.
//

#ifndef _VWGL_CMD_SCENE_COMMANDS_HPP_
#define _VWGL_CMD_SCENE_COMMANDS_HPP_

#include <vwgl/app/command.hpp>

#include <vwgl/scene/node.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/primitive_factory.hpp>

#include <vector>

namespace vwgl {
namespace cmd {

class VWGLEXPORT AddNodeCommand : public app::ContextCommand {
public:
	AddNodeCommand(plain_ptr ctx, scene::Node * child, scene::Node * newParent)
		:ContextCommand(ctx), _child(child), _newParent(newParent) {
		if (_child.valid()) {
			_prevParent = _child->parent();
		}
	}
	
	void doCommand();
	void undoCommand();
	

protected:
	ptr<scene::Node> _child;
	ptr<scene::Node> _newParent;
	ptr<scene::Node> _prevParent;
};

// If the node already have a component, this command does nothing
class AddComponentPtrCommand : public app::ContextCommand {
public:
	AddComponentPtrCommand(plain_ptr ctx, std::function<scene::Component*()> cb) :app::ContextCommand(ctx), _createComponentFunction(cb) {}
	
	inline void onComponentDidCreate(std::function<void(scene::Node*,scene::Component*)> cb) { _componentDidCreateFunction = cb; }
	
	inline void addTargetNode(scene::Node * node) {
		bool addComponent = true;
		ptr<scene::Component> newComp = _createComponentFunction();
		node->eachComponent([&](scene::Component * comp) {
			if (typeid(comp)==typeid(newComp.getPtr())) {
				addComponent = true;
			}
		});
		if (addComponent) {
			_targetNodes.push_back(node);
			_components.push_back(newComp.getPtr());
		}
	}
	
	
	
	void doCommand() {
		if (_targetNodes.size()==0) throw std::runtime_error("Could not add component: the node list is empty");
		scene::Node::NodeVector::iterator it = _targetNodes.begin();
		std::vector<ptr<scene::Component> >::iterator cit = _components.begin();
		
		for (;it!=_targetNodes.end(); ++it, ++cit) {
			scene::Node * node = (*it).getPtr();
			scene::Component * comp = (*cit).getPtr();
			node->addComponent(comp);
			if (_componentDidCreateFunction) {
				_componentDidCreateFunction(node,comp);
			}
			node->init();
		}
	}
	
	void undoCommand() {
		scene::Node::NodeVector::iterator it = _targetNodes.begin();
		std::vector<ptr<scene::Component> >::iterator cit = _components.begin();
		
		for (;it!=_targetNodes.end(); ++it, ++cit) {
			scene::Node * node = (*it).getPtr();
			scene::Component * comp = (*cit).getPtr();
			node->removeComponent(comp);
		}
	}
	
protected:
	std::function<scene::Component *()> _createComponentFunction;
	std::function<void(scene::Node*,scene::Component*)> _componentDidCreateFunction;
	scene::Node::NodeVector _targetNodes;
	std::vector<ptr<scene::Component> > _components;
};

template <class T>
class AddComponentCommand : public app::ContextCommand {
public:
	AddComponentCommand(plain_ptr ctx) :app::ContextCommand(ctx) {}
	
	inline T * addTargetNode(scene::Node * node) {
		T * comp = nullptr;
		if (!node->getComponent<T>()) {
			_targetNodes.push_back(node);
			comp = createComponent();
			_components.push_back(comp);
			configureComponent(comp);
		}
		return comp;
	}
	
	virtual T * createComponent() { return new T(); }
	virtual void configureComponent(T *) {}
	
	void doCommand() {
		if (_targetNodes.size()==0) throw std::runtime_error("Could not add component: the node list is empty");
		scene::Node::NodeVector::iterator it = _targetNodes.begin();
		std::vector<ptr<scene::Component> >::iterator cit = _components.begin();
		
		for (;it!=_targetNodes.end(); ++it, ++cit) {
			scene::Node * node = (*it).getPtr();
			scene::Component * comp = (*cit).getPtr();
			node->addComponent(comp);
			node->init();
		}
	}
	
	void undoCommand() {
		scene::Node::NodeVector::iterator it;
		for (it=_targetNodes.begin(); it!=_targetNodes.end(); ++it) {
			scene::Node * node = (*it).getPtr();
			node->removeComponent(node->getComponent<T>());
		}
	}
	
protected:
	scene::Node::NodeVector _targetNodes;
	std::vector<ptr<scene::Component> > _components;
};

template <class T>
class RemoveComponentCommand : public app::ContextCommand {
public:
	RemoveComponentCommand(plain_ptr ctx) :app::ContextCommand(ctx) {}
	
	inline void addTargetNode(scene::Node * n) {
		if (n->getComponent<T>()) {
			_targetNodes.push_back(n);
			_bkpComponents.push_back(n->getComponent<T>());
		}
	}
	
	void doCommand() {
		if (_targetNodes.size()==0) throw std::runtime_error("Could not remove component: the node list is empty");
		
		scene::Node::NodeVector::iterator it = _targetNodes.begin();
		std::vector<ptr<scene::Component> >::iterator cit = _bkpComponents.begin();
		
		for	(;it!=_targetNodes.end();++it,++cit) {
			scene::Node * n = (*it).getPtr();
			n->removeComponent((*cit).getPtr());
		}
	}
	
	void undoCommand() {
		scene::Node::NodeVector::iterator it = _targetNodes.begin();
		std::vector<ptr<scene::Component> >::iterator cit = _bkpComponents.begin();
		
		for	(;it!=_targetNodes.end();++it,++cit) {
			scene::Node * n = (*it).getPtr();
			n->addComponent((*cit).getPtr());
		}
	}
	
protected:
	scene::Node::NodeVector _targetNodes;
	std::vector<ptr<scene::Component> > _bkpComponents;
};

typedef AddComponentCommand<scene::Drawable> AddDrawableCommand;

class AddCubeComponentCommand : public AddDrawableCommand {
public:
	AddCubeComponentCommand(plain_ptr ctx) :AddDrawableCommand(ctx), _w(1.0f), _h(1.0f), _d(1.0f) {}
	AddCubeComponentCommand(plain_ptr ctx, float size) :AddDrawableCommand(ctx), _w(size), _h(size), _d(size)  {}
	AddCubeComponentCommand(plain_ptr ctx, float w, float h, float d) :AddDrawableCommand(ctx), _w(w), _h(h), _d(d)  {}
	
	scene::Drawable * createComponent() {
		return scene::PrimitiveFactory::cube(_w, _h, _d);
	}

protected:
	float _w;
	float _h;
	float _d;
};

class AddSphereComponentCommand : public AddDrawableCommand {
public:
	AddSphereComponentCommand(plain_ptr ctx) :AddDrawableCommand(ctx), _r(1.0f), _slices(20), _stacks(20) {}
	AddSphereComponentCommand(plain_ptr ctx, float r) :AddDrawableCommand(ctx), _r(r), _slices(20), _stacks(20)  {}
	AddSphereComponentCommand(plain_ptr ctx, float r, int divisions) :AddDrawableCommand(ctx), _r(r), _slices(divisions), _stacks(divisions)  {}
	AddSphereComponentCommand(plain_ptr ctx, float r, int slices, int stacks) :AddDrawableCommand(ctx), _r(r), _slices(slices), _stacks(stacks)  {}
	
	scene::Drawable * createComponent() {
		return scene::PrimitiveFactory::sphere(_r, _slices, _stacks);
	}
	
protected:
	float _r;
	int _slices;
	int _stacks;
};

class AddPlaneComponentCommand : public AddDrawableCommand {
public:
	AddPlaneComponentCommand(plain_ptr ctx) :AddDrawableCommand(ctx), _w(1.0f), _h(1.0f) {}
	AddPlaneComponentCommand(plain_ptr ctx, float size) :AddDrawableCommand(ctx), _w(size), _h(size) {}
	AddPlaneComponentCommand(plain_ptr ctx, float w, float h) :AddDrawableCommand(ctx), _w(w), _h(h) {}
	
	scene::Drawable * createComponent() {
		return scene::PrimitiveFactory::plane(_w, _h);
	}
	
protected:
	float _w;
	float _h;
};
	
class SetNodeNameCommand : public app::Command {
public:
	SetNodeNameCommand(const std::string & newName) :_newName(newName) {}
	
	inline void addTargetNode(scene::Node * node) {
		_target.push_back(node);
		_undoNames.push_back(node->getName());
	}
	
	void doCommand() {
		if (_target.size()==0) throw std::runtime_error("Could not execute command: the target is empty");
		
		scene::Node::NodeVector::iterator it;
		for (it=_target.begin(); it!=_target.end(); ++it) {
			(*it)->setName(_newName);
		}
	}
	
	void undoCommand() {
		scene::Node::NodeVector::iterator it;
		std::vector<std::string>::iterator uit;
		for (it=_target.begin(), uit=_undoNames.begin(); it!=_target.end(); ++it, ++uit) {
			(*it)->setName(*uit);
		}
	}
	
protected:
	std::string _newName;
	scene::Node::NodeVector _target;
	std::vector<std::string> _undoNames;
};

class SetNodeEnabledCommand : public app::Command {
public:
	SetNodeEnabledCommand(bool enabled) :_enabled(enabled) {}

	inline void addTargetNode(scene::Node * node) {
		_target.push_back(node);
		_nodeStatus.push_back(node->isEnabled());
	}

	void doCommand() {
		if (_target.size() == 0) { throw std::runtime_error("Could not execute command: the target is empty"); }

		scene::Node::NodeVector::iterator it;
		for (it = _target.begin(); it != _target.end(); ++it) {
			(*it)->setEnabled(_enabled);
		}
	}

	void undoCommand() {
		scene::Node::NodeVector::iterator it;
		std::vector<bool>::iterator uit;
		for (it = _target.begin(), uit = _nodeStatus.begin(); it != _target.end(); ++it, ++uit) {
			(*it)->setEnabled((*uit));
		}
	}

protected:
	bool _enabled;
	scene::Node::NodeVector _target;
	std::vector<bool> _nodeStatus;
};

}
}

#endif
