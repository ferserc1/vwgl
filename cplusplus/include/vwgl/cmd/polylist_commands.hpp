
#ifndef _VWGL_CMD_POLYLIST_COMMANDS_HPP_
#define _VWGL_CMD_POLYLIST_COMMANDS_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/scene/drawable.hpp>

#include <functional>

namespace vwgl {
namespace cmd {

class PolyListCommand : public app::ContextCommand {
public:
	typedef std::vector<ptr<PolyList> > PolyListVector;
	
	PolyListCommand(plain_ptr ctx) :ContextCommand(ctx) {}
	
	inline void addPolyList(PolyList * plist) { _plistVector.push_back(plist); }
	
protected:
	virtual ~PolyListCommand() {}
	
	PolyListVector _plistVector;
	
	inline void eachPlist(std::function<void (PolyList*)> closure) {
		PolyListVector::iterator it;
		for (it=_plistVector.begin(); it!=_plistVector.end(); ++it) {
			closure((*it).getPtr());
		}
	}
};

class VWGLEXPORT PListFlipFacesCommand : public PolyListCommand {
public:
	PListFlipFacesCommand(plain_ptr ptr) :PolyListCommand(ptr) {}
	
	void doCommand();
	void undoCommand() { doCommand(); }
};
	
class VWGLEXPORT PListFlipNormalsCommand : public PolyListCommand {
public:
	PListFlipNormalsCommand(plain_ptr ptr) :PolyListCommand(ptr) {}
	
	void doCommand();
	void undoCommand() { doCommand(); }
};
	

class VWGLEXPORT PListSetNameCommand : public PolyListCommand {
public:
	PListSetNameCommand(plain_ptr ptr, const std::string & name) :PolyListCommand(ptr), _name(name) {}
	
	void doCommand();
	void undoCommand();

protected:
	std::string _name;
	std::vector<std::string> _undoNames;
};
	
class VWGLEXPORT PListSetGroupNameCommand : public PolyListCommand {
public:
	PListSetGroupNameCommand(plain_ptr ptr, const std::string & name) :PolyListCommand(ptr), _name(name) {}
	
	void doCommand();
	void undoCommand();
	
protected:
	std::string _name;
	std::vector<std::string> _undoNames;
};

class VWGLEXPORT PListSetVisibleCommand : public PolyListCommand {
public:
	PListSetVisibleCommand(plain_ptr ptr, bool visible) :PolyListCommand(ptr), _visible(visible) {}
	
	void doCommand();
	void undoCommand();
	
protected:
	bool _visible;
	std::vector<bool> _undoVisible;
};
	
class VWGLEXPORT PListSwapUVsCommand  : public PolyListCommand {
public:
	PListSwapUVsCommand(plain_ptr ctx, PolyList::VertexBufferType uvFrom, PolyList::VertexBufferType uvTo) :PolyListCommand(ctx), _uvFrom(uvFrom), _uvTo(uvTo) {}

	void doCommand();
	void undoCommand();

protected:
	virtual ~PListSwapUVsCommand() {}

	PolyList::VertexBufferType _uvFrom;
	PolyList::VertexBufferType _uvTo;
};

class VWGLEXPORT PListDeleteCommand : public PolyListCommand {
public:
	
	PListDeleteCommand(plain_ptr ctx) :PolyListCommand(ctx), _initialized(false) {}
	
	inline void addParentDrawable(scene::Drawable * drw) {
		_drawableVector.push_back(drw);
	}
	
	void doCommand();
	void undoCommand();
	
protected:
	virtual ~PListDeleteCommand() {}
	
	class DrawablePolyList {
	public:
		DrawablePolyList(scene::Drawable * drw) :_drawable(drw) {}
	
		void tryAddPolyList(PolyList * plist) {
			int index = _drawable->getPolyListIndex(plist);
			if (index!=-1) {
				_plistVector.push_back(plist);
				_materialVector.push_back(_drawable->getMaterial(index));
				_transformVector.push_back(_drawable->getTransform(index));
			}
		}
		
		void removePlist() {
			PolyListVector::iterator it;
			for (it=_plistVector.begin(); it!=_plistVector.end(); ++it) {
				try {
					_drawable->removePolyList((*it).getPtr());
				}
				catch (scene::DrawableElementNotFoundException &) {
				}
			}
		}
		
		void undoRemovePlist() {
			eachElement([&](PolyList * plist, Material * mat, Transform & trx) {
				_drawable->addPolyList(plist, mat, trx);
			});
		}
		
		void eachElement(std::function<void(PolyList *,Material *, Transform &)> cb) {
			PolyListVector::iterator p_it = _plistVector.begin();
			MaterialVector::iterator m_it = _materialVector.begin();
			TransformVector::iterator t_it = _transformVector.begin();
			
			for (; p_it!=_plistVector.end(); ++p_it, ++m_it, ++t_it) {
				cb((*p_it).getPtr(), (*m_it).getPtr(), *t_it);
			}
		}
		
	protected:
		ptr<scene::Drawable> _drawable;
		PolyListVector _plistVector;
		MaterialVector _materialVector;
		TransformVector _transformVector;
	};
	
	std::vector<DrawablePolyList> _drawableVector;
	bool _initialized;
};
	

}
}

#endif
