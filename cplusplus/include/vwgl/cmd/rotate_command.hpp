
#ifndef _VWGL_CMD_ROTATE_COMMAND_HPP_
#define _VWGL_CMD_ROTATE_COMMAND_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/scene/transform.hpp>

namespace vwgl {
namespace cmd {

class VWGLEXPORT RotateCommand : public app::Command {
public:
	RotateCommand(scene::Transform * trx, const Vector3 & rotate) :_trx(trx), _rotate(rotate) {}
	
	virtual void doCommand();
	virtual void undoCommand();
	
protected:
	virtual ~RotateCommand() {}
	
	ptr<scene::Transform> _trx;
	Vector3 _rotate;
	Vector3 _undoRotate;
	Matrix4 _undoMatrix;
};

class VWGLEXPORT SetRotateCommand : public RotateCommand {
public:
	SetRotateCommand(scene::Transform * trx, const Vector3 & rotate) :RotateCommand(trx,rotate) {}
	
	virtual void doCommand();
	
protected:
	virtual ~SetRotateCommand() {}
};
	
}
}

#endif
