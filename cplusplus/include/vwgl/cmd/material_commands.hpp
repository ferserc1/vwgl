
#ifndef _VWGL_CMD_MATERIAL_COMMANDS_HPP_
#define _VWGL_CMD_MATERIAL_COMMANDS_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/generic_material.hpp>

#include <vector>
#include <functional>

namespace vwgl {
namespace cmd {


class MaterialCommand : public app::ContextCommand {
public:
	typedef std::vector<ptr<GenericMaterial> > MaterialVector;
	
	MaterialCommand(plain_ptr ctx) :ContextCommand(ctx) {}

	inline void addMaterial(GenericMaterial * mat) { _materials.push_back(mat); }
	
protected:
	virtual ~MaterialCommand() {}

	MaterialVector _materials;

	inline void eachMaterial(std::function<void (GenericMaterial*)> closure) {
		MaterialVector::iterator it;
		for (it=_materials.begin(); it!=_materials.end(); ++it) {
			closure((*it).getPtr());
		}
	}
};

class VWGLEXPORT ApplyMaterialModifierCommand : public MaterialCommand {
public:
	typedef std::vector<MaterialModifier> ModifierVector;
	
	ApplyMaterialModifierCommand(plain_ptr ctx, const MaterialModifier & modifier) :MaterialCommand(ctx), _modifier(modifier) {}
	
	void doCommand();
	void undoCommand();
	
protected:
	virtual ~ApplyMaterialModifierCommand() {}
	
	MaterialModifier _modifier;
	ModifierVector _materialBackup;
};

class SetCullFaceCommand : public MaterialCommand {
public:
	SetCullFaceCommand(plain_ptr ctx, bool cullFace) :MaterialCommand(ctx), _cullFace(cullFace) {}
	
	void doCommand() {
		if (_materials.size()==0) {
			throw std::runtime_error("Could not execute SetCullFaceCommand: emtpy material list found");
		}
		
		_undoCullFace.clear();
		eachMaterial([&](GenericMaterial * mat) {
			_undoCullFace.push_back(mat->getCullFace());
			mat->setCullFace(_cullFace);
		});
	}
	
	void undoCommand() {
		std::vector<bool>::iterator it = _undoCullFace.begin();
		eachMaterial([&](GenericMaterial * mat) {
			mat->setCullFace(*it);
			++it;
		});
	}
	
protected:
	std::vector<bool> _undoCullFace;
	bool _cullFace;
};

}
}

#endif