
#ifndef _VWGL_CMD_TRANSLATE_COMMAND_HPP_
#define _VWGL_CMD_TRANSLATE_COMMAND_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/scene/transform.hpp>

namespace vwgl {
namespace cmd {

class VWGLEXPORT TranslateCommand : public app::Command {
public:
	TranslateCommand(scene::Transform * trx, const Vector3 & trans) :_trx(trx), _translate(trans) {}
	
	virtual void doCommand();
	virtual void undoCommand();

protected:
	virtual ~TranslateCommand() {}
	
	ptr<scene::Transform> _trx;
	Vector3 _translate;
};
	
class VWGLEXPORT SetTranslateCommand : public TranslateCommand {
public:
	SetTranslateCommand(scene::Transform * trx, const Vector3 & trans) :TranslateCommand(trx, trans) {}
	
	virtual void doCommand();
	virtual void undoCommand();

protected:
	Vector3 _translateBkp;
};

}
}

#endif
