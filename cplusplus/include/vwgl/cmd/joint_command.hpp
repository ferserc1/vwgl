
#ifndef _VWGL_CMD_JOINT_COMMAND_HPP_
#define _VWGL_CMD_JOINT_COMMAND_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/scene/joint.hpp>
#include <vwgl/scene/transform.hpp>

#include <vector>

namespace vwgl {
namespace cmd {

class VWGLEXPORT EditJointCommand : public app::Command {
public:
	EditJointCommand(const Vector3 & offset, const Vector3 & rotation) :_offset(offset), _rotation(rotation) {}
	
	inline void addJoint(scene::ChainJoint * joint) {
		_chainJointList.push_back(joint);
		_positionList.push_back(joint->getJoint()->offset());
		_rotationList.push_back(joint->getJoint()->eulerRotation());
	}
	
	
	
	virtual void doCommand();
	virtual void undoCommand();
	
protected:
	virtual ~EditJointCommand() {}
	
	Vector3 _offset;
	Vector3 _rotation;
	std::vector<ptr<scene::ChainJoint> > _chainJointList;
	std::vector<Vector3> _positionList;
	std::vector<Vector3> _rotationList;
};

}
}

#endif
