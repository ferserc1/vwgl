//
//  delete_command.hpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 17/3/15.
//  Copyright (c) 2015 Vitaminew CB. All rights reserved.
//

#ifndef _VWGL_CMD_DELETE_COMMAND_HPP_
#define _VWGL_CMD_DELETE_COMMAND_HPP_

#include <vwgl/app/command.hpp>

#include <vwgl/scene/node.hpp>

#include <vector>

namespace vwgl {
namespace cmd {

class VWGLEXPORT DeleteCommand : public app::ContextCommand {
public:
	DeleteCommand(plain_ptr ctx);
	
	inline void addNodeToDelete(scene::Node * node) {
		if (node && node->parent()) {
			_children.push_back(node);
			_parents.push_back(node->parent());
		}
	}
	
	void doCommand();
	void undoCommand();
	

protected:
	std::vector<ptr<scene::Node> > _children;
	std::vector<ptr<scene::Node> > _parents;
};

}
}

#endif
