
#ifndef _VWGL_CMD_SCALE_COMMAND_HPP_
#define _VWGL_CMD_SCALE_COMMAND_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/scene/transform.hpp>

namespace vwgl {
namespace cmd {

class VWGLEXPORT ScaleCommand : public app::Command {
public:
	ScaleCommand(scene::Transform * trx, const Vector3 & scale) :_trx(trx), _scale(scale) {}
	
	virtual void doCommand();
	virtual void undoCommand();
	
protected:
	virtual ~ScaleCommand() {}
	
	ptr<scene::Transform> _trx;
	Vector3 _scale;
	Vector3 _undoScale;
	Matrix4 _undoMatrix;
};
	
class VWGLEXPORT SetScaleCommand : public ScaleCommand {
public:
	SetScaleCommand(scene::Transform * trx, const Vector3 & scale) :ScaleCommand(trx,scale) {}
	
	virtual void doCommand();
	
protected:
	virtual ~SetScaleCommand() {}
};

}
}

#endif
