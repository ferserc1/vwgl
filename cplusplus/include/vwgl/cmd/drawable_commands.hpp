
#ifndef _VWGL_CMD_DRAWABLE_COMMANDS_HPP_
#define _VWGL_CMD_DRAWABLE_COMMANDS_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/node.hpp>

#include <vector>
#include <functional>
#include <string>

namespace vwgl {
namespace cmd {

typedef std::vector<ptr<scene::Drawable> > DrawableVector;
	
class VWGLEXPORT DrawableCommand : public app::ContextCommand {
public:
	DrawableCommand(plain_ptr ctx, bool undoable = true) :ContextCommand(ctx, undoable) {}
	
	inline void addDrawable(scene::Drawable * drw) { _drawableVector.push_back(drw); }
	inline size_t targetSize() const { return _drawableVector.size(); }
	
	void eachDrawable(std::function<void(scene::Drawable*)> closure) {
		DrawableVector::iterator it;
		for (it=_drawableVector.begin(); it!=_drawableVector.end(); ++it) {
			closure((*it).getPtr());
		}
	}
	
protected:
	DrawableVector _drawableVector;
};

class VWGLEXPORT DrwSetNameCommand : public DrawableCommand {
public:
	DrwSetNameCommand(plain_ptr ctx, const std::string & name)
		:DrawableCommand(ctx), _name(name) {}
	
	void doCommand();
	void undoCommand();
	
protected:
	std::string _name;
	std::vector<std::string> _prevNames;
};

class VWGLEXPORT DrwSwitchUVsCommand : public DrawableCommand {
public:
	DrwSwitchUVsCommand(plain_ptr ctx, PolyList::VertexBufferType from, PolyList::VertexBufferType to)
		:DrawableCommand(ctx), _from(from), _to(to)
	{}
	
	void doCommand();
	void undoCommand();
	
protected:
	PolyList::VertexBufferType _from;
	PolyList::VertexBufferType _to;
};

class VWGLEXPORT DrwFlipNormalsCommand : public DrawableCommand {
public:
	DrwFlipNormalsCommand(plain_ptr ctx) :DrawableCommand(ctx) {}
	
	void doCommand();
	void undoCommand();
};

class DrawableTransformCommand : public DrawableCommand {
public:
	DrawableTransformCommand(plain_ptr cmd) :DrawableCommand(cmd) {}
	
protected:
	std::vector<vwgl::ptr<vwgl::TransformStrategy> > _strategyVector;
	std::vector<vwgl::Matrix4> _matrixVector;
	
	void pushTransform(scene::Drawable * drw);
	
	void restoreTransform(scene::Drawable * drw);

	void beginDoCommand();
};
	
class VWGLEXPORT DrwFlipFacesCommand : public DrawableTransformCommand {
public:
	DrwFlipFacesCommand(plain_ptr ctx) :DrawableTransformCommand(ctx) {}
	
	void doCommand();
	void undoCommand();
};
	
class VWGLEXPORT DrwFreezeTransformCommand : public DrawableTransformCommand {
public:
	DrwFreezeTransformCommand(plain_ptr ctx) :DrawableTransformCommand(ctx) {}
	
	void doCommand();
	void undoCommand();

};

class VWGLEXPORT DrwMoveToCenterCommand : public DrawableTransformCommand {
public:
	DrwMoveToCenterCommand(plain_ptr ctx) :DrawableTransformCommand(ctx) {}
	
	void doCommand();
	void undoCommand();
};
	
class VWGLEXPORT DrwPutOnFloorCommand : public DrawableTransformCommand {
public:
	DrwPutOnFloorCommand(plain_ptr ctx) :DrawableTransformCommand(ctx) {}
	
	void doCommand();
	void undoCommand();
};

class VWGLEXPORT DrwReplaceGeometryCommand : public DrawableCommand {
public:
	DrwReplaceGeometryCommand(plain_ptr ctx, scene::Drawable * newDrw) :DrawableCommand(ctx), _newDrawable(newDrw) {}
	
	
	void doCommand();
	void undoCommand();
	
protected:
	ptr<scene::Drawable> _newDrawable;
	scene::Node::NodeVector _parentNodes;
	std::vector<ptr<scene::Drawable> > _oldDrawables;
};

}
}

#endif
