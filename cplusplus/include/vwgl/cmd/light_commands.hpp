#ifndef _VWGL_CMD_LIGHT_COMMANDS_HPP_
#define _VWGL_CMD_LIGHT_COMMANDS_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/scene/light.hpp>

namespace vwgl {
namespace cmd {

class LightCommand : public app::ContextCommand {
public:
	typedef std::vector<ptr<scene::Light> > LightVector;
	
	LightCommand(plain_ptr ctx) :ContextCommand(ctx) {}
	
	inline void addLight(scene::Light * light) { _lights.push_back(light); }
	
protected:
	virtual ~LightCommand() {}
	
	LightVector _lights;
	
	inline void eachLight(std::function<void (scene::Light *)> closure) {
		LightVector::iterator it;
		for (it=_lights.begin(); it!=_lights.end(); ++it) {
			closure((*it).getPtr());
		}
	}
};

class VWGLEXPORT ApplyLightModifierCommand : public LightCommand {
public:
	typedef std::vector<scene::LightModifier> ModifierVector;
	
	ApplyLightModifierCommand(plain_ptr ctx, const scene::LightModifier & modifier) :LightCommand(ctx), _modifier(modifier) {}
	
	void doCommand();
	void undoCommand();
	
protected:
	virtual ~ApplyLightModifierCommand() {}
	
	scene::LightModifier _modifier;
	ModifierVector _backup;
};

}
}

#endif
