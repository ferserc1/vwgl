
#ifndef _VWGL_CMD_EXPORT_cOMMAND_HPP_
#define _VWGL_CMD_EXPORT_cOMMAND_HPP_

#include <vwgl/cmd/drawable_commands.hpp>

namespace vwgl {
namespace cmd {

class VWGLEXPORT ExportCommand : public DrawableCommand {
public:
	ExportCommand(plain_ptr ctx, const std::string & dstPath) :DrawableCommand(ctx, false), _destinationPath(dstPath) {}

	void doCommand();

protected:
	std::string _destinationPath;
};

}
}

#endif