
#ifndef _VWGL_CMD_IMPORT_COMMAND_HPP_
#define _VWGL_CMD_IMPORT_COMMAND_HPP_

#include <vwgl/app/command.hpp>
#include <vwgl/scene/node.hpp>

namespace vwgl {
namespace cmd {

class VWGLEXPORT ImportCommand : public app::ContextCommand {
public:
	ImportCommand(plain_ptr ctx, const std::string & path, vwgl::scene::Node * parent, const Vector3 & position, bool loadPrefab = true);

	inline scene::Node * importNode() { return _importNode.getPtr(); }

	void doCommand();
	void undoCommand();

protected:
	std::string _path;
	ptr<scene::Node> _importNode;
	ptr<scene::Node> _parent;
	Vector3 _position;
	bool _loadPrefab;
};

}
}
#endif