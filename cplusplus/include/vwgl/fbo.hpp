
#ifndef _vwgl_fbo_hpp_
#define _vwgl_fbo_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/cubemap.hpp>

#include <map>

namespace vwgl {

class VWGLEXPORT FramebufferObject : public ReferencedPointer {
public:
	enum FBOType {
		kTypeDefault,
		kTypeMultiTarget,
		kTypeDepthTexture,
		kTypeFloatTexture,
		kTypeCubeMap,
		kTypeDepthRenderbuffer	// This will create a depth buffer, not associated with a texture
	};
	
	enum Attachment {
		kAttachmentColor0 = GL_COLOR_ATTACHMENT0,
		kAttachmentColor1 = GL_COLOR_ATTACHMENT1,
		kAttachmentColor2 = GL_COLOR_ATTACHMENT2,
		kAttachmentColor3 = GL_COLOR_ATTACHMENT3,
		kAttachmentColor4 = GL_COLOR_ATTACHMENT4,
		kAttachmentColor5 = GL_COLOR_ATTACHMENT5,
		kAttachmentColor6 = GL_COLOR_ATTACHMENT6,
		kAttachmentDepth = GL_DEPTH_ATTACHMENT
	};
	
	class AttachmentData {
	public:
		AttachmentData() {}
		AttachmentData(Texture * texture, FBOType type) :_texture(texture), _type(type) {}
		AttachmentData(GLuint renderbuffer) :_type(kTypeDepthRenderbuffer), _renderBuffer(renderbuffer) {}
		
		inline void setTexture(Texture * tex) { _texture = tex; }
		inline void setRenderBuffer(GLuint rb) { _renderBuffer = rb; }
		inline void setType(FBOType type) { _type = type; }
	
		inline Texture * getTexture() { return _texture.getPtr(); }
		inline GLuint getRenderBuffer() { return _renderBuffer; }
		inline FBOType getType() { return _type; }

		inline void operator = (AttachmentData & other) {
			_texture = other._texture;
			_type = other._type;
			_renderBuffer = other._renderBuffer;
		}

	protected:
		ptr<Texture> _texture;
		FBOType _type;
		GLuint _renderBuffer;
	};

	typedef std::map<Attachment, AttachmentData> TextureMap;
	
	FramebufferObject();
	FramebufferObject(int width, int height);
	FramebufferObject(int width, int height, FBOType type);
	FramebufferObject(const Size2Di & size);
	FramebufferObject(const Size2Di & size, FBOType type);

	void setTextureColorTarget(Attachment attachment, Texture::TextureTarget target);

	// Quick create function: create a FBO with one texture attachment of the specified type
	inline void create(const Size2Di & size, FBOType type = kTypeDefault) { create(size.width(), size.height(), type);  }
	void create(int width, int height, FBOType type = kTypeDefault);
	
	// Low level create: allows to create multiple attachment FBOs
	void genFramebuffer(const Size2Di & size);	// Create the fbo and specify the size
	bool addAttachment(Attachment att, FBOType type = kTypeDefault);
	
	void drawBuffers(int size, const GLuint att[]);
	
	// Use the following functions to resize the FBO (quick or low level)
	inline void resize(const Size2Di & size) { resize(size.width(), size.height()); }
	void resize(int width, int height);

	void bind();
	void unbind();
	
	void destroy();
	
	int getWidth() const { return _width; }
	int getHeight() const { return _height; }

	inline Texture * getTexture(Attachment attachment = kAttachmentColor0) { return _textureMap.find(attachment)!=_textureMap.end() ? _textureMap[attachment].getTexture():nullptr; }
	inline CubeMap * getCubeMap(Attachment attachment = kAttachmentColor0) { return dynamic_cast<CubeMap*>(getTexture(attachment)); }
	inline Texture * getDephtTexture() { return getTexture(kAttachmentDepth); }

	void readColor(const Viewport & rectangle, Attachment readFrom, unsigned char * pixels);

protected:
	virtual ~FramebufferObject();
	
	int _width;
	int _height;
	
	GLuint _fbo;
	
	TextureMap _textureMap;
};

}

#endif
