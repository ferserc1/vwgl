
#ifndef _vwgl_font_manager_hpp_
#define _vwgl_font_manager_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/text_font.hpp>
#include <unordered_map>

namespace vwgl {

class VWGLEXPORT FontManager : public Singleton {
public:
	typedef std::unordered_map<std::string,TextFont*> FontMap;
	
	static FontManager * get();

	TextFont * getFont(const std::string & name, int size, int resolution);
	void registerFont(TextFont * font);
	void unregisterFont(TextFont * font);
	
	void finalize();
	
protected:
	
	void getHash(const std::string & fontName, int size, int res, std::string & hash);
	
	FontMap _fonts;
	
	FontManager();
	virtual ~FontManager();
	static FontManager * s_singleton;
	
	
};

}

#endif
