#ifndef _vwgl_scenewritter_hpp_
#define _vwgl_scenewritter_hpp_

#include <vwgl/writterplugin.hpp>
#include <vwgl/node_visitor.hpp>
#include <vwgl/exceptions.hpp>

#include <fstream>

#include <vwgl/json_serializer.hpp>

#include <vwgl/scene/scene.hpp>

namespace vwgl {

class SceneContainerException : public FileException {
public:
	SceneContainerException() :FileException ("Error creating scene container") {}
};

class SceneFileException : public FileException {
public:
	SceneFileException() :FileException ("Error creating scene file") {}
};

class VWGLEXPORT SceneWritter : public WriteScenePlugin {
public:
	SceneWritter();
	
	virtual bool acceptFileType(const std::string & path);
	
	virtual bool writeScene(const std::string & path, scene::Node * node);
	
protected:
	virtual ~SceneWritter();
	
	void makeContainer(const std::string & path);
	void openFile(const std::string & path);
	void closeFile();
	void writeHeader();
	
	void writeNode(scene::Node * node);
	
	void writeComponent(scene::Component * component, bool lastItem);
		
	std::string _folderPath;
	std::ofstream _file;
	JsonSerializer _serializer;
	bool _isLastNode;
	
	static const int s_majorVersion;
	static const int s_minorVersion;
	static const int s_revision;
};

}

#endif