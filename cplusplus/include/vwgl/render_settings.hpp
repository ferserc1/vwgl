//
//  render_settings.hpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 24/02/14.
//  Copyright (c) 2014 Vitaminew. All rights reserved.
//

#ifndef vwgl_render_settings_hpp
#define vwgl_render_settings_hpp

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/material.hpp>

namespace vwgl {

class VWGLEXPORT RenderSettings {
public:
	RenderSettings()
		:_brightness(0.5f),
		_contrast(0.5f),
		_hue(1.0f),
		_saturation(1.0f),
		_lightness(1.0f),
		_antialiasing(false)
	{}
	
	inline void setBrightness(float b) { _brightness = b; }
	inline void setContrast(float c) { _contrast = c; }
	inline void setHue(float h) { _hue = h; }
	inline void setSaturation(float s) { _saturation = s; }
	inline void setLightness(float l) { _lightness = l; }
	inline void setAntialiasingEnabled(bool a) { _antialiasing = a; }
	
	inline float getBrightness() const { return _brightness; }
	inline float getContrast() const { return _contrast; }
	inline float getHue() const { return _hue; }
	inline float getSaturation() { return _saturation; }
	inline float getLightness() { return _lightness; }
	inline bool isAntialiasingEnabled() { return _antialiasing; }
	
	// Current render settings used only in API 1.0
	static void setCurrentRenderSettings(RenderSettings * settings);
	static RenderSettings * getCurrentRenderSettings();
	
	inline void operator = (const RenderSettings & rs) {
		_brightness = rs._brightness;
		_contrast = rs._contrast;
		_hue = rs._hue;
		_saturation = rs._saturation;
		_lightness = rs._lightness;
		_antialiasing = rs._antialiasing;
	}
	
protected:
	float _brightness;
	float _contrast;
	float _hue;
	float _saturation;
	float _lightness;
	bool _antialiasing;
	
	// current render settings is used only in the API 1.0
	static RenderSettings * s_currentRenderSettings;
};

typedef std::vector<Material*> FilterList;

}


#endif
