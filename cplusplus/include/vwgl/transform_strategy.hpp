#ifndef _VWGL_TRANSFORM_STRATEGY_HPP_
#define _VWGL_TRANSFORM_STRATEGY_HPP_

#include <vwgl/transformable.hpp>
#include <vwgl/referenced_pointer.hpp>
#include <vwgl/json_serializer.hpp>

namespace vwgl {

class VWGLEXPORT TransformStrategy : public ReferencedPointer {
	friend class TransformNode;
public:
	TransformStrategy() :_transformable(nullptr), _target(nullptr) {}
	
	virtual void updateTransform() = 0;
	
	void setTransformable(Transformable * trx) { _transformable = trx; }
	
	// API 2.0
	void setTransformable(ITransformable * trx) { _target = trx; }
	
	virtual void serialize(JsonSerializer & serializer) = 0;
	virtual void deserialize(JsonDeserializer & deserializer) = 0;
	static TransformStrategy * factory(JsonDeserializer & deserializer);
	
	virtual TransformStrategy * clone() = 0;
	virtual void reset() = 0;
	virtual Vector3 position() = 0;
		
protected:
	virtual ~TransformStrategy() {}

	Transformable * _transformable;
	
	// API 2.0
	ITransformable * _target;
};

class VWGLEXPORT TRSTransformStrategy : public TransformStrategy {
public:
	enum RotationOrder {
		kOrderXYZ = 0,
		kOrderXZY,
		kOrderYXZ,
		kOrderYZX,
		kOrderZYX,
		kOrderZXY
	};
	
	TRSTransformStrategy() :_translate(0.0f), _rotateX(0.0f), _rotateY(0.0f) ,_rotateZ(0.0f) ,_scale(1.0f) ,_rotationOrder(kOrderXYZ) {}
	
	inline void translate(Vector3 t) { _translate = t; updateTransform(); }
	inline void rotateX(float x) { _rotateX = x; updateTransform(); }
	inline void rotateY(float y) { _rotateY = y; updateTransform(); }
	inline void rotateZ(float z) { _rotateZ = z; updateTransform(); }
	inline void scale(Vector3 s) { _scale = s; updateTransform(); }
	
	inline const Vector3 & getTranslate() const { return _translate; }
	inline float getRotateX() const { return _rotateX; }
	inline float getRotateY() const { return _rotateY; }
	inline float getRotateZ() const { return _rotateZ; }
	inline const Vector3 & getScale() const { return _scale; }
	
	inline void setRotationOrder(RotationOrder order) {
		_rotationOrder = order;
		updateTransform();
	}
	inline RotationOrder getRotationOrder() const {
		return _rotationOrder;
	}
	
	void updateTransform() {
		if (!_transformable && !_target) return;
		Matrix4 matrix;
		matrix
			.identity()
			.translate(_translate.x(), _translate.y(), _translate.z());
		
		switch (_rotationOrder) {
			case TRSTransformStrategy::kOrderXYZ:
				matrix.rotate(_rotateX, 1.0f, 0.0f, 0.0f)
					.rotate(_rotateY, 0.0f, 1.0f, 0.0f)
					.rotate(_rotateZ, 0.0f, 0.0f, 1.0f);
				break;
			case TRSTransformStrategy::kOrderXZY:
				matrix.rotate(_rotateX, 1.0f, 0.0f, 0.0f)
					.rotate(_rotateZ, 0.0f, 0.0f, 1.0f)
					.rotate(_rotateY, 0.0f, 1.0f, 0.0f);
				break;
			case TRSTransformStrategy::kOrderYXZ:
				matrix.rotate(_rotateY, 0.0f, 1.0f, 0.0f)
					.rotate(_rotateX, 1.0f, 0.0f, 0.0f)
					.rotate(_rotateZ, 0.0f, 0.0f, 1.0f);
				break;
			case TRSTransformStrategy::kOrderYZX:
				matrix.rotate(_rotateY, 0.0f, 1.0f, 0.0f)
					.rotate(_rotateZ, 0.0f, 0.0f, 1.0f)
					.rotate(_rotateX, 1.0f, 0.0f, 0.0f);
				break;
			case TRSTransformStrategy::kOrderZYX:
				matrix.rotate(_rotateZ, 0.0f, 0.0f, 1.0f)
					.rotate(_rotateY, 0.0f, 1.0f, 0.0f)
					.rotate(_rotateX, 1.0f, 0.0f, 0.0f);
				break;
			case TRSTransformStrategy::kOrderZXY:
				matrix.rotate(_rotateZ, 0.0f, 0.0f, 1.0f)
					.rotate(_rotateX, 1.0f, 0.0f, 0.0f)
					.rotate(_rotateY, 0.0f, 1.0f, 0.0f);
				break;
		}
		
		matrix.scale(_scale.x(), _scale.y(), _scale.z());
		if (_transformable) _transformable->setTransform(matrix);
		else if (_target) _target->setMatrix(matrix);
	}
	
	virtual void serialize(JsonSerializer & serializer) {
		serializer.openObject(false, true);
		serializer.writeProp<std::string>("type", "TRSTransformStrategy", true, true);
		serializer.writeProp("translate", _translate, true, true);
		serializer.writeProp("rotateX", _rotateX, true, true);
		serializer.writeProp("rotateY", _rotateY, true, true);
		serializer.writeProp("rotateZ", _rotateZ, true, true);
		switch (_rotationOrder) {
			case TRSTransformStrategy::kOrderXYZ:
				serializer.writeProp<std::string>("rotationOrder", "kOrderXYZ", true, true);
				break;
			case TRSTransformStrategy::kOrderXZY:
				serializer.writeProp<std::string>("rotationOrder", "kOrderXZY", true, true);
				break;
			case TRSTransformStrategy::kOrderYXZ:
				serializer.writeProp<std::string>("rotationOrder", "kOrderYXZ", true, true);
				break;
			case TRSTransformStrategy::kOrderYZX:
				serializer.writeProp<std::string>("rotationOrder", "kOrderYZX", true, true);
				break;
			case TRSTransformStrategy::kOrderZYX:
				serializer.writeProp<std::string>("rotationOrder", "kOrderZYX", true, true);
				break;
			case TRSTransformStrategy::kOrderZXY:
				serializer.writeProp<std::string>("rotationOrder", "kOrderZXY", true, true);
				break;
		}
		serializer.writeProp("scale", _scale, false, true);
		serializer.closeObject(true, false, true);
	}
	
	virtual void deserialize(JsonDeserializer & deserializer);
	
	virtual TransformStrategy * clone() {
		TRSTransformStrategy * strategy = new TRSTransformStrategy();
		
		strategy->_target = _target;
		strategy->_transformable = _transformable;
		strategy->_translate = _translate;
		strategy->_rotateX = _rotateX;
		strategy->_rotateY = _rotateY;
		strategy->_rotateZ = _rotateZ;
		strategy->_scale = _scale;
		strategy->_rotationOrder = _rotationOrder;
		strategy->updateTransform();
		
		return strategy;
	}
	
	void reset() {
		_translate.set(0.0f);
		_rotateX = 0.0f;
		_rotateY = 0.0f;
		_rotateZ = 0.0f;
		_scale.set(1.0f);
		updateTransform();
	}
	
	Vector3 position() {
		return _translate;
	}
	
protected:
	virtual ~TRSTransformStrategy() {}
	
	Vector3 _translate;
	float _rotateX;
	float _rotateY;
	float _rotateZ;
	Vector3 _scale;
	
	RotationOrder _rotationOrder;
};

class VWGLEXPORT PolarTransformStrategy : public TransformStrategy {
public:
	PolarTransformStrategy() :_distance(0.0f) {}
	
	inline void setOrientation(float o) { _coords.x(o); updateTransform(); }
	inline void setElevation(float e) { _coords.y(e); updateTransform(); }
	inline void setDistance(float d) { _distance = d; updateTransform(); }
	inline void setCoords(const vwgl::Vector2 & c) { _coords = c; updateTransform(); }
	inline void setOrigin(const vwgl::Vector3 & o) { _origin = o; updateTransform(); }
	inline float getOrientation() const { return _coords.x(); }
	inline float getElevation() const { return _coords.y(); }
	inline float getDistance() const { return _distance; }
	inline const vwgl::Vector2 & getCoords() const { return _coords; }
	inline const vwgl::Vector3 & getOrigin() const { return _origin; }
	
	inline void updateTransform() {
		if (!_transformable && !_target) return;
		
		Matrix4 matrix;
		matrix.identity()
			.translate(_origin.x(), _origin.y(), _origin.z())
			.rotate(_coords.x(),0.0,1.0,0.0)
			.rotate(_coords.y(),1.0,0.0,0.0)
			.translate(0.0f,0.0f,-_distance);
		if (_transformable) _transformable->setTransform(matrix);
		else if (_target) _target->setMatrix(matrix);
	}
	
	virtual void serialize(JsonSerializer & serializer) {
		serializer.openObject(false, true);
		serializer.writeProp<std::string>("type", "PolarTransformStrategy", true, true);
		serializer.writeProp("coords", _coords, true, true);
		serializer.writeProp("distance", _distance, true, true);
		serializer.writeProp("origin", _origin, false, true);
		serializer.closeObject(true, false, true);
	}
	
	virtual void deserialize(JsonDeserializer & deserializer);
	
	virtual TransformStrategy * clone() {
		PolarTransformStrategy * strategy = new PolarTransformStrategy();
		
		strategy->_target = _target;
		strategy->_transformable = _transformable;
		strategy->_coords = _coords;
		strategy->_distance = _distance;
		strategy->_origin = _origin;
		strategy->updateTransform();
		
		return strategy;
	}
	
	void reset() {
		_coords.set(0.0f, 0.0f);
		_distance = 0.0f;
		_origin.set(0.0f);
		updateTransform();
	}
	
	Vector3 position() {
		return _origin;
	}
	
protected:
	virtual ~PolarTransformStrategy() {}
	
	vwgl::Vector2 _coords;
	float _distance;
	vwgl::Vector3 _origin;
};


}

#endif