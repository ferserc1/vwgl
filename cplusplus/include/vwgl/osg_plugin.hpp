#ifndef _OSGEXPORT_OSG_PLUGIN_HPP_
#define _OSGEXPORT_OSG_PLUGIN_HPP_

#include <vwgl/writterplugin.hpp>
#include <fstream>

namespace vwgl {

class VWGLEXPORT OSGWritter : public vwgl::WriteScenePlugin {
public:
	OSGWritter();
	
	virtual bool acceptFileType(const std::string & path);
	
	virtual bool writeScene(const std::string & path, vwgl::scene::Node * node);
	
protected:
	virtual ~OSGWritter();

	void writeNode(vwgl::scene::Node * node);
	void writeDrawable(vwgl::scene::Drawable * drw);
	void writeStateSet(vwgl::GenericMaterial * mat, const std::string & id);
	void writeGeom(vwgl::PolyList * plist, const std::string & id);

	std::ofstream _file;
	std::string _path;
	int _numTabs;
	int _groupUID;
	int _geoUID;
	int _stateSetUID;
	int _texEnvId;
	std::string _tabs;

	inline const std::string & incTabs() { ++_numTabs; _tabs = ""; for (int i=0; i<_numTabs; ++i) { _tabs += "  "; } return _tabs; }
	inline const std::string & decTabs() { --_numTabs; _tabs = ""; for (int i=0; i<_numTabs; ++i) { _tabs += "  "; } return _tabs; }
	inline const std::string & tabs() { return _tabs; }

	inline void openBlock() { _file << " {\n"; incTabs(); }
	inline void closeBlock() { decTabs(); _file << "\n" << tabs() << "}\n"; }
	inline void printVector(const vwgl::Vector2 & v) { _file << v.x() << " " << v.y(); }
	inline void printVector(const vwgl::Vector3 & v) { _file << v.x() << " " << v.y() << " " << v.z(); }
	inline void printVector(const vwgl::Vector4 & v) { _file << v.x() << " " << v.y() << " " << v.z() << " " << v.w(); }
};

}

#endif
