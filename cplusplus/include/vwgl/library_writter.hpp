
#ifndef _vwgl_library_writter_hpp_
#define _vwgl_library_writter_hpp_

#include <vwgl/writterplugin.hpp>
#include <vwgl/library/library.hpp>

#include <fstream>

namespace vwgl {

class VWGLEXPORT LibraryWritter : public WriteLibraryPlugin {
public:
	LibraryWritter();
	
	virtual bool acceptFileType(const std::string & path);
	
	virtual bool writeLibrary(const std::string & path, library::Node * node);
	
protected:
	virtual ~LibraryWritter();

	std::ofstream _file;
	std::string _sourcePath;
	std::string _dstPath;
	std::string _dstFileName;
	std::string _folderName;
	std::string _dstFolder;

	void writeFile(library::Node * node);

	void writeProp(int tabs, const std::string & key);
	void writeProp(int tabs, const std::string & key, const std::string & value, const std::string & separator = ",\n");
	void writeProp(int tabs, const std::string & key, int value, const std::string & separator = ",\n");
	void writeProp(int tabs, const std::string & key, float value, const std::string & separator = ",\n");
	void writeProp(int tabs, const std::string & key, bool value, const std::string & separator = ",\n");

	void writeNodeArray(int tabs, vwgl::library::NodeList & nodeList);
	void writeNode(int tabs, library::Node * node);

	void writeDictionary(int tabs, const std::string & name, library::Dictionary & dict);
	void writeMaterialModifier(int tabs, MaterialModifier & mod);

	void writeTabs(int tabs);

	void copyResources(library::Node * node);

	std::string copyResource(const std::string & src, const std::string & folder = "");

	void extractFileName(const std::string & input, std::string & output) {
		unsigned long pos = static_cast<unsigned long>(input.find_last_of("\\/"));
		if (pos==std::string::npos) {
			output = input;
		}
		else {
			output = input.substr(pos + 1);
		}
	}
};
	
}

#endif
