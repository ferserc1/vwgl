
#ifndef _vwgl_exceptions_hpp_
#define _vwgl_exceptions_hpp_

#include <exception>
#include <string>

namespace vwgl {

class BaseException : public std::exception {
public:
	explicit BaseException(const std::string & msg) :_msg(msg) { }
	virtual ~BaseException() throw () {}

	virtual const char * what() const throw() {
		return _msg.c_str();
	}

protected:
	std::string _msg;
};

class FileException : public BaseException {
public:
	explicit FileException() :BaseException("File I/O exception") {}
	explicit FileException(const std::string & msg) :BaseException(msg) {}
	virtual ~FileException() throw() {}
};

template <class exceptionType>
void assert(bool expression, const std::string & message = "") {
	if (!expression) throw exceptionType(message);
}

}

#endif