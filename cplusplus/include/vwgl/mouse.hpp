
#ifndef _vwgl_mouse_hpp_
#define _vwgl_mouse_hpp_

#include <iostream>

namespace vwgl {

class Mouse {
public:
	enum Button {
		kLeftButton		= 1 << 0,
		kMiddleButton	= 1 << 1,
		kRightButton	= 1 << 2
	};

	Mouse() :_buttonMask(0) {}

	inline void setMouseDown(Button btn) { _buttonMask = _buttonMask | btn; }
	inline void setMouseUp(Button btn) { _buttonMask = _buttonMask & ~btn; }
	inline bool getButtonStatus(Button btn) const { return (_buttonMask & btn)!=0; }
	inline unsigned int buttonMask() const { return _buttonMask; }
	inline void setButtonMask(unsigned int mask) { _buttonMask = mask; }
	inline bool anyButtonPressed() const { return _buttonMask!=0; }

protected:
	unsigned int _buttonMask;
};

}
#endif
