
#ifndef _vwgl_glyph_hpp_
#define _vwgl_glyph_hpp_

#include <vwgl/vector.hpp>
#include <vwgl/image.hpp>

namespace vwgl {

class GlyphBounds {
public:
	inline void set(const Vector2 & min, const Vector2 & max) { _min = min; _max = max;}
	
	inline const Vector2 & max() const { return _max; }
	inline const Vector2 & min() const { return _min; }
	
	inline float width() const { return _max.x() - _min.x(); }
	inline float height() const { return _max.y() - _min.y(); }
	
	inline void operator=(const GlyphBounds & b) { _max = b._max; _min = b._min; }
	inline bool operator==(const GlyphBounds & b) { return _max==b._max && _min==b._min; }
	
protected:
	Vector2 _max;
	Vector2 _min;
};

class Glyph {
public:
	Glyph() :_advance(0), _character('\0') {}
	
	inline Image * getBitmap() { return _bitmap.getPtr(); }
	inline const Image * getBitmap() const { return _bitmap.getPtr(); }
	inline void setBitmap(Image * img) { _bitmap = img; }
	
	inline char getCharacter() const { return _character; }
	inline void setCharacter(char c) { _character = c; }
	
	inline const Vector2 & getBearing() const { return _bearing; }
	inline void setBearing(const Vector2 & b) { _bearing = b; }
	
	inline const GlyphBounds & getBBox() const { return _boundingBox; }
	inline GlyphBounds & getBBox() { return _boundingBox; }
	inline void setBBox(const GlyphBounds & b) { _boundingBox = b; }
	
	inline float getAdvance() const { return _advance; }
	inline void setAdvance(float a) { _advance = a; }
	
	inline float getWidth() const { return _boundingBox.width(); }
	inline float getHeight() const { return _boundingBox.height(); }
	
	inline void operator=(const Glyph & g) {
		_bitmap = new Image(g._bitmap.getPtr());
		_character = g._character;
		_bearing = g._bearing;
		_boundingBox = g._boundingBox;
	}
	
protected:
	ptr<Image> _bitmap;
	GlyphBounds _boundingBox;
	Vector2 _bearing;
	float _advance;
	char _character;
};

}

#endif
