
#ifndef _vwgl_transform_node_hpp_
#define _vwgl_transform_node_hpp_

#include <vwgl/math.hpp>
#include <vwgl/group.hpp>
#include <vwgl/transform_strategy.hpp>

namespace vwgl {
	
class VWGLEXPORT TransformNode : public Group, Transformable {
public:
	enum TransformReference {
		kAbsolute,
		kRelative
	};

	TransformNode() :_rotationReference(kRelative) { _transform.identity(); }
	TransformNode(const Matrix4 & t) :_rotationReference(kRelative) { _transform = t; }
	TransformNode(TransformStrategy * strategy) :_rotationReference(kRelative) { _transform.identity(); setTransformStrategy(strategy); }

	inline void setTransform(const Matrix4 & t) { _transform = t; }
	inline Matrix4 & getTransform() { return _transform; }
	inline const Matrix4 & getTransform() const { return _transform; }
	
	inline void setRotationReference(TransformReference ref) { _rotationReference = ref; }
	inline TransformReference getRotationReference() const { return _rotationReference; }
	
	inline void setTransformStrategy(TransformStrategy * strategy) {
		_transformStrategy = strategy;
		if (_transformStrategy.valid()) {
			_transformStrategy->setTransformable(this);
			_transformStrategy->updateTransform();
		}
	}
	template <class T> inline T * getTransformStrategy() { return dynamic_cast<T*>(_transformStrategy.getPtr()); }
	template <class T> inline const T * getTransformStrategy() const { return dynamic_cast<T*>(_transformStrategy.getPtr()); }

	inline void prepareMatrix(Matrix4 & modelMatrix) {
		if (_rotationReference==kAbsolute) {
			modelMatrix.set00(1.0f); modelMatrix.set01(0.0f); modelMatrix.set02(0.0f);
			modelMatrix.set10(0.0f); modelMatrix.set11(1.0f); modelMatrix.set12(0.0f);
			modelMatrix.set20(0.0f); modelMatrix.set21(0.0f); modelMatrix.set22(1.0f);
		}
	}
	
protected:
	virtual ~TransformNode() {}

	Matrix4 _transform;
	ptr<TransformStrategy> _transformStrategy;
	TransformReference _rotationReference;
};

}

#endif
