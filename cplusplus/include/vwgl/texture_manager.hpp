
#ifndef _vwgl_texture_manager_hpp_
#define _vwgl_texture_manager_hpp_

#include <vwgl/texture.hpp>
#include <vwgl/cubemap.hpp>
#include <vwgl/image.hpp>

#include <unordered_map>

namespace vwgl {

class VWGLEXPORT TextureManager : public Singleton {
public:
	static TextureManager * get();
	
	Texture * loadTexture(ptr<Image> img, bool nocache=false);
	Texture * loadTexture(Image * img, bool nocache=false);
	CubeMap * loadCubeMap(ptr<Image> posX,
								 ptr<Image> negX,
								 ptr<Image> posY,
								 ptr<Image> negY,
								 ptr<Image> posZ,
								 ptr<Image> negZ);
	CubeMap * loadCubeMap(Image * posX,
								 Image * negX,
								 Image * posY,
								 Image * negY,
								 Image * posZ,
								 Image * negZ);

	void setMinFilter(Texture::TextureFilter minFilter) { _minFilter = minFilter; }
	void setMagFilter(Texture::TextureFilter magFilter) { _magFilter = magFilter; }
	void setWrapS(Texture::TextureWrap w) { _wrapS = w; }
	void setWrapT(Texture::TextureWrap w) { _wrapT = w; }

	void clearUnused();
	
	virtual void finalize();

	inline int colorTextureSize() const { return _colorTextureSize; }
	Texture * whiteTexture();
	Texture * blackTexture();
	Texture * transparentTexture();
	Texture * yNormalTexture();	// Neutral normal map (0.0, 1.0, 0.0)

	Texture * randomTexture();
	
protected:
	Texture::TextureFilter _minFilter;
	Texture::TextureFilter _magFilter;
	Texture::TextureWrap _wrapS;
	Texture::TextureWrap _wrapT;
	
	typedef std::unordered_map<std::string, ptr<Texture> > TextureMap;
	int _colorTextureSize;
	TextureMap _textures;
	ptr<Texture> _firstTexture;
	ptr<Texture> _whiteTexture;
	ptr<Texture> _blackTexture;
	ptr<Texture> _transparentTexture;
	ptr<Texture> _yNormalTexture;

	TextureManager();
	virtual ~TextureManager();
	static TextureManager * s_singleton;
};

}
#endif
