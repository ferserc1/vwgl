
#ifndef _vwgl_draw_gizmos_visitor_hpp_
#define _vwgl_draw_gizmos_visitor_hpp_

#include <vwgl/node_visitor.hpp>
#include <vwgl/material.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/light.hpp>

namespace vwgl {
	
class VWGLEXPORT DrawGizmosVisitor : public NodeVisitor {
public:
	enum Features {
		kPhysicJoints		= 1 << 0,
		kDirectionalLights	= 1 << 1,
		kSpotLights			= 1 << 2,
		kPointLights		= 1 << 3,
		kAxis				= 1 << 4,
		kDrawableBBox		= 1 << 5,
		kTransformBBox		= 1 << 6
	};

	DrawGizmosVisitor();
	
	virtual void visit(Node * node);
	
	inline void resize(const Viewport & vp) { _viewport = vp; }

	inline void enableFeature(Features feature) {
		_featureMask = _featureMask | feature;
	}

	inline void disableFeature(Features feature) {
		_featureMask = _featureMask & ~feature;
	}
	
	inline void setFeatureMask(unsigned int mask) {
		_featureMask = mask;
	}

	inline bool isFeatureEnabled(Features feature) {
		return (_featureMask & feature)!=0;
	}

	inline void useMaterial(Material * mat) { _mat = mat; }

	inline void setDrawableBBoxColor(const Color & color) {
		if (bboxDrawable()) {
			bboxDrawable()->getGenericMaterial()->setDiffuse(color);
		}
	}

protected:
	virtual ~DrawGizmosVisitor();
	
	unsigned int _featureMask;
	
	Material * _mat;
	
	inline Drawable * getDrawable(Light * light) {
		Drawable * drw = nullptr;
		if (light &&
			((light->getType()==Light::kTypeDirectional && isFeatureEnabled(kDirectionalLights)) ||
			 (light->getType()==Light::kTypeSpot && isFeatureEnabled(kSpotLights)) ||
			 (light->getType()==Light::kTypePoint && isFeatureEnabled(kPointLights)))) {
			drw = light->getGizmoDrawable();
		}
		return drw;
	}
	
	Viewport _viewport;
	vwgl::Drawable * getAxis();
	vwgl::Drawable * bboxDrawable();
	vwgl::ptr<vwgl::Drawable> _axisDrawable;
	vwgl::ptr<vwgl::Drawable> _boundingBoxDrawable;
};

}
#endif
