
#ifndef _vwgl_gizmo_manager_hpp_
#define _vwgl_gizmo_manager_hpp_

#include <vwgl/gizmo.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/transform_visitor.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/boundingbox.hpp>
#include <vwgl/color_pick_material.hpp>

namespace vwgl {


class VWGLEXPORT GizmoManager : public Singleton {
public:
	static GizmoManager * get();

	void enableGizmo(TransformNode * trx, Gizmo * gizmo);
	void disableGizmo();
	
	bool checkItemPicked(vwgl::Solid * solid);
	void beginMouseEvents(const vwgl::Position2Di pos, Camera * camera, const Viewport & vp);
	void mouseEvent(const vwgl::Position2Di pos);
	void endMouseEvents();
	
	void drawGizmo(vwgl::Camera * cam, vwgl::ColorPickMaterial * gizmoMaterial = nullptr);
	
	inline Gizmo * getCurrentGizmo() { return _currentGizmo.getPtr(); }
	
	inline float getGizmoScale() const { return _gizmoScale; }
	inline void setGizmoScale(float s) { _gizmoScale = s; }
	
	inline vwgl::TransformNode * currentTransform() { return _currentTransform.getPtr(); }
	inline vwgl::Gizmo * currentGizmo() { return _currentGizmo.getPtr(); }
	
	virtual void finalize();

protected:
	GizmoManager();
	virtual ~GizmoManager();
	
	static GizmoManager * _singleton;
	
	ptr<TransformNode> _currentTransform;
	ptr<Gizmo> _currentGizmo;
	ptr<BoundingBox> _boundingBox;
	float _gizmoScale;

	
	ptr<TransformVisitor> _gizmoTransformVisitor;
};

}

#endif
