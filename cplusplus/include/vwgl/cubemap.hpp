
#ifndef _vwgl_cubemap_hpp_
#define _vwgl_cubemap_hpp_

#include <vwgl/opengl.hpp>
#include <vwgl/referenced_pointer.hpp>
#include <vwgl/texture.hpp>

namespace vwgl {

class VWGLEXPORT CubeMap : public Texture {
public:
	CubeMap();

	inline const std::string & getPositiveXFile() const { return _positiveXFile; }
	inline const std::string & getNegativeXFile() const { return _negativeXFile; }
	inline const std::string & getPositiveYFile() const { return _positiveYFile; }
	inline const std::string & getNegativeYFile() const { return _negativeYFile; }
	inline const std::string & getPositiveZFile() const { return _positiveZFile; }
	inline const std::string & getNegativeZFile() const { return _negativeZFile; }
	
	inline void setPositiveXFile(const std::string & fileName) { _positiveXFile = fileName; }
	inline void setNegativeXFile(const std::string & fileName) { _negativeXFile = fileName; }
	inline void setPositiveYFile(const std::string & fileName) { _positiveYFile = fileName; }
	inline void setNegativeYFile(const std::string & fileName) { _negativeYFile = fileName; }
	inline void setPositiveZFile(const std::string & fileName) { _positiveZFile = fileName; }
	inline void setNegativeZFile(const std::string & fileName) { _negativeZFile = fileName; }
	
protected:
	virtual ~CubeMap();
	
	std::string _positiveXFile;
	std::string _negativeXFile;
	std::string _positiveYFile;
	std::string _negativeYFile;
	std::string _positiveZFile;
	std::string _negativeZFile;
};

}

#endif
