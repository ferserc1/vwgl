#ifndef _vwgl_diffuse_render_pas_hpp_
#define _vwgl_diffuse_render_pas_hpp_

#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class DiffuseRenderPassMaterial : public DeferredMaterial {
public:
	DiffuseRenderPassMaterial();
				
	virtual void initShader();
		
protected:
	virtual ~DiffuseRenderPassMaterial();

	virtual void setupUniforms();

};

}

#endif
