
#ifndef _vwgl_plane_gizmo_hpp_
#define _vwgl_plane_gizmo_hpp_

#include <vwgl/gizmo.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/physics/plane.hpp>

#include <iostream>

namespace vwgl {

class VWGLEXPORT PlaneGizmo : public Gizmo {
public:
	PlaneGizmo();
	PlaneGizmo(const std::string & modelPath);

	virtual bool checkItemPicked(Solid * solid) {
		_mode = getMode(solid->getPickId());
		return _mode!=PlaneGizmo::kGizmoNone;
	}

	virtual void beginMouseEvents(const Position2Di & pos, vwgl::Camera * camera, const Viewport & vp, const Vector3 & gizmoPos, TransformNode * trx);
	virtual void mouseEvent(const vwgl::Position2Di & pos);
	virtual void endMouseEvents();

protected:
	virtual ~PlaneGizmo();

	virtual void loadPickIds();
	
	PickId _moveId;
	PickId _rotateId;
	PickId _rotateFineId;
	
	enum Mode {
		kGizmoNone 		 = 0,
		kGizmoMove 		 = 1,
		kGizmoRotate 	 = 2,
		kGizmoRotateFine = 3
	};

	Mode _mode;
	Camera * _camera;
	Viewport _viewport;
	physics::Plane _plane;
	Vector3 _lastPickPoint;
	Vector3 _translateOffset;
	TransformNode * _transformNode;
	
	Mode getMode(PickId pickedId) {
		if (pickedId==_moveId) {
			return PlaneGizmo::kGizmoMove;
		}
		else if (pickedId==_rotateId) {
			return PlaneGizmo::kGizmoRotate;
		}
		else if (pickedId==_rotateFineId) {
			return PlaneGizmo::kGizmoRotateFine;
		}
		return PlaneGizmo::kGizmoNone;
	}
};

}

#endif
