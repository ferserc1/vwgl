
#ifndef _vwgl_vwglbwritter_hpp_
#define _vwgl_vwglbwritter_hpp_

#include <vwgl/writterplugin.hpp>
#include <vwgl/vwglbutils.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/image.hpp>

namespace vwgl {

class VWGLEXPORT VwglbWritter : public WriteDrawablePlugin {
public:
	VwglbWritter();

	virtual bool acceptFileType(const std::string & path);

	virtual bool writeDrawable(const std::string & path, Drawable * drawable);

protected:
	virtual ~VwglbWritter();
	
	void writeHeader(Drawable * drawable);
	void ensurePolyListNames(Drawable * drawable);
	void writeMaterial(Drawable * drawable);
	void writeShadow(Drawable * drawable);
	void writeJoints(Drawable * drawable);
	void writePolyList(Solid * solid);
	void saveTextureFile(const std::string & imagePath);
	
	VwglbUtils _fileUtils;
	std::string _dstPath;
};

}

#endif
