
#ifndef _VWGL_DICTIONARY_WRITTER_HPP_
#define _VWGL_DICTIONARY_WRITTER_HPP_

#include <vwgl/writterplugin.hpp>
#include <iostream>

namespace vwgl {

class VWGLEXPORT DictionaryWritter : public WriteDictionaryPlugin {
public:
	DictionaryWritter() :_out(&std::cout) { }

	virtual bool acceptFileType(const std::string & path);
	virtual bool writeDictionary(const std::string & path, Dictionary * dict);

protected:
	virtual ~DictionaryWritter() {}

	void tab(int tabs);
	void printObject(Dictionary * value, int tabs = 0);
	void printArray(Dictionary * obj, int tabs = 0);
	void printObjectMap(Dictionary * obj, int tabs = 0);

	std::ostream * _out;
};

}

#endif
