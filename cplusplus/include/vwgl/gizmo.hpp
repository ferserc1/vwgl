
#ifndef _vwgl_gizmo_hpp_
#define _vwgl_gizmo_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/color_picker.hpp>

#include <string>

namespace vwgl {

class Gizmo : public ReferencedPointer {
public:
	Gizmo(const std::string & modelPath);

	Drawable * getDrawable() { return _drawable.getPtr(); }
	const Drawable * getDrawable() const { return _drawable.getPtr(); }
	
    virtual bool checkItemPicked(Solid * /* solid */) { return false; }
    virtual void beginMouseEvents(const Position2Di & /* pos */, vwgl::Camera * /* camera */, const Viewport & /* vp */, const Vector3 & /* gizmoPos */, TransformNode * /* trx */) {}
    virtual void mouseEvent(const vwgl::Position2Di & /* pos */) {}
	virtual void endMouseEvents() {}
	
	virtual bool getResetRotation() const { return _resetRotation; }

protected:
	virtual ~Gizmo();
	
	void init(const std::string & modelPath);
	
	virtual void loadPickIds() = 0;
	
	bool _resetRotation;

	ptr<Drawable> _drawable;
};

}

#endif
