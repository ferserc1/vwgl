
#ifndef _VWGL_TRANSFORMABLE_HPP_
#define _VWGL_TRANSFORMABLE_HPP_

#include <vwgl/math.hpp>

namespace vwgl {

	
class Transformable {
public:
	virtual void setTransform(const Matrix4 & transform) = 0;
	virtual const Matrix4 & getTransform() const = 0;
	virtual Matrix4 & getTransform() = 0;
};
	
// API 2.0
class ITransformable {
public:
	virtual void setMatrix(const Matrix4 & transform) = 0;
	virtual const Matrix4 & getMatrix() const = 0;
	virtual Matrix4 & getMatrix() = 0;
};

}

#endif
