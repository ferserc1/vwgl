
#ifndef _VWGL_TRANSFORM_HPP_
#define _VWGL_TRANSFORM_HPP_

#include <vwgl/transformable.hpp>
#include <vwgl/transform_strategy.hpp>
#include <vwgl/json_serializer.hpp>


namespace vwgl {

class Transform : public ITransformable {
public:
	Transform() { _matrix.identity(); }
	Transform(const Matrix4 & mat) :_matrix(mat) {}
	
	void setMatrix(const Matrix4 & matrix) { _matrix = matrix; }
	const Matrix4 & getMatrix() const { return _matrix; }
	Matrix4 & getMatrix() { return _matrix; }
	
	TransformStrategy * getTransformStrategy() { return _transformStrategy.getPtr(); }
	const TransformStrategy * getTransformStrategy() const { return _transformStrategy.getPtr(); }

	template <class T> inline T * getTransformStrategy() { return dynamic_cast<T*>(_transformStrategy.getPtr()); }
	template <class T> inline const T * getTransformStreategy() const { return dynamic_cast<T*>(_transformStrategy.getPtr()); }
	inline void setTransformStrategy(TransformStrategy * strategy) {
		_transformStrategy = strategy;
		if (_transformStrategy.valid()) {
			_transformStrategy->setTransformable(this);
			_transformStrategy->updateTransform();
		}
	}

	inline Transform & operator = (const Transform & trx) {
		_matrix = trx._matrix;
		if (trx._transformStrategy.valid()) {
			setTransformStrategy(trx._transformStrategy->clone());
		}
		return *this;
	}
	
	Vector3 position() {
		return _matrix.getPosition();
	}
		
protected:
	Matrix4 _matrix;
	ptr<TransformStrategy> _transformStrategy;
};

typedef std::vector<vwgl::Transform> TransformVector;

}

#endif
