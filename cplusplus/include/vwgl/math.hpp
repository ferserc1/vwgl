
#ifndef vwgl_math_hpp
#define vwgl_math_hpp

#include <vwgl/mathutils.hpp>
#include <vwgl/vector.hpp>
#include <vwgl/matrix.hpp>
#include <vwgl/quaternion.hpp>

#endif
