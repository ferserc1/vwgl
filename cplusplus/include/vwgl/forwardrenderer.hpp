
#ifndef _vwgl_forwardrenderer_hpp_
#define _vwgl_forwardrenderer_hpp_

#include <vwgl/renderer.hpp>
#include <vwgl/scenerenderer.hpp>
#include <vwgl/projtexturedeferredmaterial.hpp>
#include <vwgl/initvisitor.hpp>

namespace vwgl {

class VWGLEXPORT ForwardRenderer : public Renderer {
public:
	ForwardRenderer();
	
	void build(Group * sceneRoot, int width, int height);
	void build(Group * sceneRoot, const Size2Di & size) { build(sceneRoot, size.width(), size.height()); }
	void destroy();
	void draw(Camera * cam = NULL);
    virtual void resize(const Size2Di & /* size */) {}
	
	virtual void setClearColor(const Vector4 & color) { _clearColor = color; }

protected:
	virtual ~ForwardRenderer();
	
	Color _clearColor;
	ptr<SceneRenderer> _sceneRenderer;
	ptr<InitVisitor> _initVisitor;
	ptr<ProjTextureDeferredMaterial> _projectorMat;
	
	virtual void configureSceneRoot();
};

}

#endif
