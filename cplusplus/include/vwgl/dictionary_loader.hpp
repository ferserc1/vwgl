
#ifndef _VWGL_DICTIONARY_LOADER_HPP_
#define _VWGL_DICTIONARY_LOADER_HPP_

#include <vwgl/readerplugin.hpp>

namespace vwgl {

class VWGLEXPORT DictionaryLoader : public ReadDictionaryPlugin {
public:
	DictionaryLoader() {};

	virtual bool acceptFileType(const std::string & path);
	virtual Dictionary * loadDictionary(const std::string & path);

protected:
	virtual ~DictionaryLoader() {}
};

}

#endif
