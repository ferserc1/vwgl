
#ifndef _vwgl_deferredmaterial_hpp_
#define _vwgl_deferredmaterial_hpp_

#include <vwgl/material.hpp>
#include <vwgl/generic_material.hpp>

namespace vwgl {

class VWGLEXPORT DeferredMaterial : public Material {
public:
	DeferredMaterial();

	virtual void useSettingsOf(Material * mat) { _settingsMaterial = dynamic_cast<GenericMaterial*>(mat); }

	virtual void activate();
	virtual void deactivate();
	
protected:
	virtual ~DeferredMaterial();

	GenericMaterial * getSettingsMaterial() { return _settingsMaterial.getPtr(); }

	ptr<GenericMaterial> _settingsMaterial;
	Material::PolygonMode _polygonModeBkp;
	bool _cullFaceBkp;
	float _lineWidthBkp;
};

}

#endif
