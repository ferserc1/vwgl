
#ifndef _vwgl_texturedeferredmaterial_hpp_

#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class VWGLEXPORT TextureDeferredMaterial : public DeferredMaterial {
public:
	TextureDeferredMaterial();

	virtual void initShader();
	
protected:
	virtual ~TextureDeferredMaterial();
	
	virtual void setupUniforms();
};

}

#endif
