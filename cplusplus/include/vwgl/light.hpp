
#ifndef _vwgl_light_hpp_
#define _vwgl_light_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/vector.hpp>
#include <vwgl/mathutils.hpp>
#include <vwgl/node.hpp>
#include <vwgl/transform_visitor.hpp>
#include <vwgl/color_pick_material.hpp>
#include <vwgl/render_pass.hpp>
#include <vector>

#ifdef min
#undef min
#endif

namespace vwgl {

class ILight {
public:
	enum LightType {
		kTypeDirectional = 4,
		kTypeSpot = 1,
		kTypePoint = 5,
		kTypeDisabled = 10	// Internal use: to disable/enable a light, use setEnabled(), do not use this type
	};
	
	ILight() :_lightType(LightType::kTypeDirectional) {}
	
	inline LightType getType() const { return _lightType; }
	inline void setType(LightType type) { _lightType = type; }
	
	virtual Vector3 getPosition() = 0;
	virtual Vector3 getDirection() = 0;
	inline Vector3 getSpotDirection() { return getDirection(); } // TODO: remove this function
	virtual Vector4 & getAmbient() = 0;
	virtual Vector4 & getDiffuse() = 0;
	virtual Vector4 & getSpecular() = 0;
	virtual float getIntensity() const = 0;
	virtual float getConstantAttenuation() const = 0;
	virtual float getLinearAttenuation() const = 0;
	virtual float getExpAttenuation() const = 0;
	virtual float getSpotCutoff() const = 0;
	virtual float getSpotExponent() const = 0;
	inline float getSpotAngle() const { return getSpotCutoff(); }	// TODO: Remove this function
	virtual float getShadowStrength() const = 0;
	virtual float getCutoffDistance() const = 0;
	virtual bool getCastShadows() const = 0;
	virtual float getShadowBias() const = 0;
	
	virtual void setAmbient(const Vector4 & ambient) = 0;
	virtual void setDiffuse(const Vector4 & diffuse) = 0;
	virtual void setSpecular(const Vector4 & specular) = 0;
	virtual void setIntensity(float i) = 0;
	virtual void setConstantAttenuation(float att) = 0;
	virtual void setLinearAttenuation(float att) = 0;
	virtual void setExpAttenuation(float att) = 0;
	virtual void setSpotCutoff(float v) = 0;
	virtual void setSpotExponent(float v) = 0;
	inline void setSpotAngle(float v) { setSpotCutoff(v); }		// TODO: remove this function
	virtual void setShadowStrength(float v) = 0;
	virtual void setCutoffDistance(float d) = 0;
	virtual void setCastShadows(bool) = 0;
	virtual void setShadowBias(float) = 0;
	
	virtual vwgl::Matrix4 & getTransform() = 0;
	virtual vwgl::Matrix4 & getAbsoluteTransform() = 0;
	
	// Shadow projection and view matrix
	virtual vwgl::Matrix4 & getProjectionMatrix() = 0;
	virtual vwgl::Matrix4 & getViewMatrix() = 0;
	
protected:
	LightType _lightType;
};
	
	
// TODO: This file may only contain the ILight interface declaration. Remove Light and LightManager
// when the 2.0 API implementation is complete
	
class Drawable;

class VWGLEXPORT Light : public Node, public ILight {
public:
	Light();

	virtual Vector3 getPosition();
	virtual Vector3 getDirection();
	virtual Vector4 & getAmbient() { return _ambient; }
	virtual Vector4 & getDiffuse() { return _diffuse; }
	virtual Vector4 & getSpecular() { return _specular; }
	virtual float getIntensity() const { return _intensity; }
	virtual float getConstantAttenuation() const { return _constantAtt; }
	virtual float getLinearAttenuation() const { return _linearAtt; }
	virtual float getExpAttenuation() const { return _expAtt; }
	virtual float getSpotCutoff() const { return _spotCutoff; }
	virtual float getSpotExponent() const { return _spotExponent; }
	virtual float getShadowStrength() const { return _shadowStrength; }
	virtual float getCutoffDistance() const { return _cutoffDistance; }

	virtual void setAmbient(const Vector4 & ambient) { _ambient = ambient; }
	virtual void setDiffuse(const Vector4 & diffuse) { _diffuse = diffuse; }
	virtual void setSpecular(const Vector4 & specular) { _specular = specular; }
	virtual void setIntensity(float i) { _intensity = i; }
	virtual void setConstantAttenuation(float att) { _constantAtt = att; }
	virtual void setLinearAttenuation(float att) { _linearAtt = att; }
	virtual void setExpAttenuation(float att) { _expAtt = att; }
	virtual void setSpotCutoff(float v) { _spotCutoff = v; }
	virtual void setSpotExponent(float v) { _spotExponent = v; }
	virtual void setShadowStrength(float v) { _shadowStrength = v; }
	virtual void setCutoffDistance(float d) { _cutoffDistance = d; }
	
	// This light doesn't support shadows
	virtual bool getCastShadows() const { return false; }
	virtual float getShadowBias() const { return 0.0f; }
	virtual void setCastShadows(bool) {}
	virtual void setShadowBias(float) {}
	virtual vwgl::Matrix4 & getProjectionMatrix() { return _fooTransform; }
	virtual vwgl::Matrix4 & getViewMatrix() { return _fooTransform; }
	
	virtual vwgl::Matrix4 & getTransform();
	virtual vwgl::Matrix4 & getAbsoluteTransform();
	
	Drawable * getGizmoDrawable();

	inline void setPickId(const PickId & id) {
		_pickId.group = id.group;
		_pickId.section = id.section;
		_pickId.item = id.item;
		_pickId.part = id.part;
	}
	
	const PickId & getPickId() const {
		return _pickId;
	}
	
	PickId & getPickId() {
		return _pickId;
	}

protected:
	virtual ~Light();

	LightType _lightType;
	Vector4 _ambient;
	Vector4 _diffuse;
	Vector4 _specular;
	float _intensity;
	float _constantAtt;
	float _linearAtt;
	float _expAtt;
	float _spotCutoff;
	float _spotExponent;
	float _shadowStrength;
	float _cutoffDistance;
	
	ptr<TransformVisitor> _transformVisitor;
	vwgl::Matrix4 _fooTransform;
	PickId _pickId;
};

class ShadowLight;
class VWGLEXPORT LightManager : public Singleton {
	friend class Light;
public:
	typedef std::vector<Light*> LightList;

	LightManager();
	virtual ~LightManager();

	static LightManager * get();

	virtual void finalize();

	LightList & getLightList() { return _lightList; }

	void prepareFrame(bool noShadows = false);
	
	int getLightSourcesCount() { return Math::min(_numberOfLights,_maxLightSources); }
	int * getTypeUniformArray() { return _typeArray; }
	float * getPositionUniformArray() { return _positionArray; }
	float * getDirectionUniformArray() { return _directionArray; }
	float * getAmbientUniformArray() { return _ambientArray; }
	float * getDiffuseUniformArray() { return _diffuseArray; }
	float * getSpecularUniformArray() { return _specularArray; }
	float * getPropertiesUniformArray() { return _properties; }
	
	Drawable * getGizmo(Light::LightType type);

	void updateShadowMap(Group * sceneRoot, ShadowLight * light);
	inline void setShadowMapSize(const Size2Di & size) { _shadowMapSize = size; createShadowMap(); }
	inline const vwgl::Size2Di & getShadowMapSize() const { return _shadowMapSize; }
	Texture * getShadowTexture();


protected:
	static LightManager * s_lightManager;
	LightList _lightList;

	void prepareUniforms();
	
	void addLight(Light * light);
	void removeLight(Light * light);
	
	void allocUniforms();
	void destroyUniforms();
	void createShadowMap();
	void destroyShadowMap();

	static int _maxLightSources;

	int * _typeArray;
	float * _positionArray;
	float * _directionArray;
	float * _ambientArray;
	float * _diffuseArray;
	float * _specularArray;
	float * _properties;	// r: intensity, g: linearAttenuation b: expAttenuation, a: enabled
	int _numberOfLights;
	
	ptr<Drawable> _directionalGizmo;
	ptr<Drawable> _spotGizmo;
	ptr<Drawable> _pointGizmo;
	ptr<Light> _gizmoLight;

	vwgl::ptr<vwgl::RenderPass> _renderPass;
	vwgl::Size2Di _shadowMapSize;
	
	Drawable * loadGizmo(const std::string & path);
};

}

#endif
