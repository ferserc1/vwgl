
#ifndef _vwgl_deferredrenderer_hpp_
#define _vwgl_deferredrenderer_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/render_pass.hpp>
#include <vwgl/render_canvas.hpp>
#include <vwgl/group.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/deferredmixmaterial.hpp>
#include <vwgl/projtexturedeferredmaterial.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/renderer.hpp>
#include <vwgl/initvisitor.hpp>
#include <vwgl/diffuse_render_pass.hpp>
#include <vwgl/specular_render_pass.hpp>
#include <vwgl/shadow_render_pass.hpp>
#include <vwgl/deferred_lighting.hpp>
#include <vwgl/bloom_map_material.hpp>
#include <vwgl/ssao.hpp>

namespace vwgl {

class VWGLEXPORT DeferredRenderer : public Renderer {
public:
	enum RenderPhase {
		kRenderPassDiffuse		= 1 << 0,
		kRenderPassNormal		= 1 << 1,
		kRenderPassSpecular		= 1 << 2,
		kRenderPassPosition		= 1 << 3,
		kRenderPassSelection	= 1 << 4,
		kRenderPassLighting		= 1 << 5,
		kRenderPassShadows		= 1 << 6,
		kRenderPassPostprocess	= 1 << 7
	};

	virtual RenderSettings & getRenderSettings() { return _postprocessMat->getRenderSettings(); }
	
	void setClearColor(const Vector4 & color) { _clearColor = color; }
	const Vector4 & getClearColor() const { return _clearColor; }
	Vector4 & getClearColor() { return _clearColor; }
	
	DeferredRenderer();
	
	void build(Group * sceneRoot, int width, int height);
	
	void resize(const Size2Di & size);
	
	void destroy();
	
	void draw(Camera * cam);
	
	inline void setAntiAliasingSamples(int samples, bool update=false) {
		_antiAliasing = samples;
		if (update) {
			resize(_mapSize);
		}
	}

	void enableRenderPass(RenderPhase pass) { _renderPasses = _renderPasses | pass; }
	void disableRenderPass(RenderPhase pass) { _renderPasses = _renderPasses & ~pass; }
	unsigned int getRenderPassMask() const { return _renderPasses; }
	void setRenderPassMask(unsigned int mask) { _renderPasses = mask; }

protected:
	virtual ~DeferredRenderer();

	ptr<Group> _sceneRoot;
	ptr<InitVisitor> _initVisitor;
	Color _clearColor;
	int _antiAliasing;
	unsigned int _renderPasses;
	
	// Render pass
	ptr<RenderPass> _diffuseRP;
	ptr<RenderPass> _normalRP;
	ptr<RenderPass> _specularRP;
	ptr<RenderPass> _depthRP;
	ptr<RenderPass> _positionRP;
	ptr<RenderPass> _shadowRP;
	ptr<RenderPass> _selectionRP;
	
	ptr<ProjTextureDeferredMaterial> _projectorMat;
	
	ptr<DeferredLighting> _lightingMaterial;
	ptr<HomogeneousRenderCanvas> _lightingCanvas;
	ptr<FramebufferObject> _lightingFbo;
	
	ptr<SSAOMaterial> _ssaoMaterial;
	ptr<HomogeneousRenderCanvas> _ssaoCanvas;
	ptr<FramebufferObject> _ssaoFbo;
	
	ptr<BloomMapMaterial> _bloomMaterial;
	ptr<HomogeneousRenderCanvas> _bloomCanvas;
	ptr<FramebufferObject> _bloomFbo;
	
	
	ptr<HomogeneousRenderCanvas> _canvas;
	
	ptr<DeferredPostprocess> _postprocessMat;
	

	void prepareFrame(Camera * cam);
	void geometryPass();
	void lightingPass();
	void postprocessPass();
	void presentCanvas(Camera * cam);

	virtual void configureSceneRoot();
};

}

#endif
