
#ifndef _vwgl_deferredmixmaterial_hpp_
#define _vwgl_deferredmixmaterial_hpp_

#include <vwgl/material.hpp>
#include <vwgl/render_settings.hpp>

namespace vwgl {
	
class RenderSettings;
	
class DeferredPostprocess : public Material {
public:
	DeferredPostprocess();
	
	inline void setDiffuseMap(Texture * texture) { _diffuseMap = texture; }
	inline void setTranslucentMap(Texture * texture) { _translucentMap = texture; }
	inline void setPositionMap(Texture * texture) { _positionMap = texture; }
	inline void setSelectionMap(Texture * texture) { _selectionMap = texture; }
	inline void setSSAOMap(Texture * texture) { _ssaoMap = texture; }
	inline void setBloomMap(Texture * texture) { _bloomMap = texture; }
	inline void setTargetDistance(float dist) { _targetDistance = dist; }
	inline void setDOFAmount(float amount) { _dofAmount = amount; }
	
		
	inline Texture * getDiffuseMap() { return _diffuseMap.getPtr(); }
	inline Texture * getTranslucentMap() { return _translucentMap.getPtr(); }
	inline Texture * getPositionMap() { return _positionMap.getPtr(); }
	inline Texture * getSSAOMap() { return _ssaoMap.getPtr(); }
	inline Texture * getBloomMap() { return _bloomMap.getPtr(); }
	inline float getTargetDistance() const { return _targetDistance; }
	inline float getDOFAmount() const { return _dofAmount; }
	inline RenderSettings & getRenderSettings() { return _renderSettings; }
	inline RenderSettings * getRenderSettingsPtr() { return &_renderSettings; }
	
	inline void setSSAOBlur(int blurIterations) { _ssaoBlur = blurIterations; }
	inline int getSSAOBlur() const { return _ssaoBlur; }
		

	inline void setClearColor(const Color & color) { _clearColor = color; }
	inline const Color & getClearColor() const { return _clearColor; }
	inline Color & getClearColor() { return _clearColor; }
	
	inline void setBloomAmount(int amount) { _bloomAmount = amount; }
	
	virtual void initShader();

protected:
	virtual ~DeferredPostprocess();
	
	virtual void setupUniforms();
	
	ptr<Texture> _diffuseMap;
	ptr<Texture> _translucentMap;
	ptr<Texture> _positionMap;
	ptr<Texture> _selectionMap;
	ptr<Texture> _ssaoMap;
	ptr<Texture> _bloomMap;
	
	int _ssaoBlur;
	
	// Depth of field
	float _targetDistance;
	float _dofAmount;
	
	RenderSettings _renderSettings;
	Color _clearColor;
	
	int _bloomAmount;
};

}

#endif

