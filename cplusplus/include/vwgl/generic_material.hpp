
#ifndef _vwgl_generic_material_hpp_
#define _vwgl_generic_material_hpp_

#include <vwgl/material.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/cubemap.hpp>
#include <vwgl/shadow_light.hpp>

#include <vwgl/scene/drawable.hpp>

namespace vwgl {

class Drawable;

class GenericMaterial;

class MaterialModifier {
public:
	enum ModifierFlag {
		kDiffuse			= 1 << 0,
		kSpecular			= 1 << 1,
		kShininess			= 1 << 2,
		kLightEmission		= 1 << 3,
		kRefractionAmount	= 1 << 4,
		kReflectionAmount	= 1 << 5,
		kTexture			= 1 << 6,
		kLightmap			= 1 << 7,
		kNormalMap			= 1 << 8,
		kLightNormalMap		= 1 << 9,
		kTextureOffset		= 1 << 10,
		kTextureScale		= 1 << 11,
		kLightmapOffset		= 1 << 12,
		kLightmapScale		= 1 << 13,
		kNormalMapOffset	= 1 << 14,
		kNormalMapScale		= 1 << 15,
		kCubeMap			= 1 << 16,
		kCastShadows		= 1 << 17,
		kReceiveShadows		= 1 << 18,
		kReceiveProjections	= 1 << 19,
		kAlphaCutoff		= 1 << 20,
		kShininessMask		= 1 << 21,
		kLightEmissionMask	= 1 << 22,
		
		kModifierAll		= ~0
	};
	

	inline void setFlags(unsigned int flags) { _modifierFlags = flags; }
	inline unsigned int getFlags() const { return _modifierFlags; }
	inline bool isEnabled(ModifierFlag flag) const { return (_modifierFlags & flag)!=0; }
	inline void setEnabled(ModifierFlag flag) { _modifierFlags = _modifierFlags | flag; }
	inline void setDisabled(ModifierFlag flag) { _modifierFlags = _modifierFlags & ~flag; }
	
	inline void setDiffuse(const Vector4 & d) { setEnabled(kDiffuse); _diffuse = d; }
	inline Vector4 & getDiffuse() { return _diffuse; }
	inline const Vector4 & getDiffuse() const { return _diffuse; }
	
	inline void setSpecular(const Vector4 & d) { setEnabled(kSpecular); _specular = d; }
	inline Vector4 & getSpecular() { return _specular; }
	inline const Vector4 & getSpecular() const { return _specular; }
	
	inline void setShininess(float s) { setEnabled(kShininess); _shininess = s; }
	inline float getShininess() const { return _shininess; }
	
	inline void setLightEmission(float e) { setEnabled(kLightEmission); _lightEmission = e; }
	inline float getLightEmission() const { return _lightEmission; }
	
	inline void setRefractionAmount(float r) { setEnabled(kRefractionAmount); _refractionAmount = r; }
	inline float getRefractionAmount() const { return _refractionAmount; }
	
	inline void setReflectionAmount(float r) { setEnabled(kReflectionAmount); _reflectionAmount = r; }
	inline float getReflectionAmount() const { return _reflectionAmount; }
	
	inline void setTexture(const std::string & t) { setEnabled(kTexture); _texture = t; }
	inline const std::string & getTexture() const { return _texture; }
	inline std::string & getTexture() { return _texture; }
	
	inline void setLightMap(const std::string & t) { setEnabled(kLightmap); _lightmap = t; }
	inline const std::string & getLightMap() const { return _lightmap; }
	inline std::string & getLightMap() { return _lightmap; }
	
	inline void setNormalMap(const std::string & t) { setEnabled(kNormalMap); _normalMap = t; }
	inline const std::string & getNormalMap() const { return _normalMap; }
	inline std::string & getNormalMap() { return _normalMap; }
	
	inline void setLightNormalMap(const std::string & t) { setEnabled(kLightNormalMap); _lightNormalMap = t; }
	inline const std::string & getLightNormalMap() const { return _lightNormalMap; }
	inline std::string & getLightNormalMap() { return _lightNormalMap; }
	
	inline void setTextureOffset(const Vector2 & o) { setEnabled(kTextureOffset); _textureOffset = o; }
	inline Vector2 & getTextureOffset() { return _textureOffset; }
	inline const Vector2 & getTextureOffset() const { return _textureOffset; }
	
	inline void setTextureScale(const Vector2 & o) { setEnabled(kTextureScale); _textureScale = o; }
	inline Vector2 & getTextureScale() { return _textureScale; }
	inline const Vector2 & getTextureScale() const { return _textureScale; }
	
	inline void setLightmapOffset(const Vector2 & o) { setEnabled(kLightmapOffset); _lightmapOffset = o; }
	inline Vector2 & getLightmapOffset() { return _lightmapOffset; }
	inline const Vector2 & getLightmapOffset() const { return _lightmapOffset; }
	
	inline void setLightmapScale(const Vector2 & o) { setEnabled(kLightmapScale); _lightmapScale = o; }
	inline Vector2 & getLightmapScale() { return _lightmapScale; }
	inline const Vector2 & getLightmapScale() const { return _lightmapScale; }
	
	inline void setNormalMapScale(const Vector2 & o) { setEnabled(kNormalMapScale); _normalMapScale = o; }
	inline Vector2 & getNormalMapScale() { return _normalMapScale; }
	inline const Vector2 & getNormalMapScale() const { return _normalMapScale; }
	
	inline void setNormalMapOffset(const Vector2 & o) { setEnabled(kNormalMapOffset); _normalMapOffset = o; }
	inline Vector2 & getNormalMapOffset() { return _normalMapOffset; }
	inline const Vector2 & getNormalMapOffset() const { return _normalMapOffset; }
	
	inline void setCubeMapPosX(const std::string & cubeMap) { setEnabled(kCubeMap); _cubeMapPosX = cubeMap; }
	inline const std::string & getCubeMapPosX() const { return _cubeMapPosX; }
	inline std::string & getCubeMapPosX() { return _cubeMapPosX; }
	
	inline void setCubeMapNegX(const std::string & cubeMap) { setEnabled(kCubeMap); _cubeMapNegX = cubeMap; }
	inline const std::string & getCubeMapNegX() const { return _cubeMapNegX; }
	inline std::string & getCubeMapNegX() { return _cubeMapNegX; }
	
	inline void setCubeMapPosY(const std::string & cubeMap) { setEnabled(kCubeMap); _cubeMapPosY = cubeMap; }
	inline const std::string & getCubeMapPosY() const { return _cubeMapPosY; }
	inline std::string & getCubeMapPosY() { return _cubeMapPosY; }
	
	inline void setCubeMapNegY(const std::string & cubeMap) { setEnabled(kCubeMap); _cubeMapNegY = cubeMap; }
	inline const std::string & getCubeMapNegY() const { return _cubeMapNegY; }
	inline std::string & getCubeMapNegY() { return _cubeMapNegY; }
	
	inline void setCubeMapPosZ(const std::string & cubeMap) { setEnabled(kCubeMap); _cubeMapPosZ = cubeMap; }
	inline const std::string & getCubeMapPosZ() const { return _cubeMapPosZ; }
	inline std::string & getCubeMapPosZ() { return _cubeMapPosZ; }
	
	inline void setCubeMapNegZ(const std::string & cubeMap) { setEnabled(kCubeMap); _cubeMapNegZ = cubeMap; }
	inline const std::string & getCubeMapNegZ() const { return _cubeMapNegZ; }
	inline std::string & getCubeMapNegZ() { return _cubeMapNegZ; }
	
	inline void setCastShadows(bool s) { setEnabled(kCastShadows); _castShadows = s; }
	inline bool getCastShadows() const { return _castShadows; }
	
	inline void setReceiveShadows(bool s) { setEnabled(kReceiveShadows); _receiveShadows = s; }
	inline bool getReceiveShadows() const { return _receiveShadows; }
	
	inline void setReceiveProjections(bool p) { setEnabled(kReceiveProjections); _receiveProjections = p; }
	inline bool getReceiveProjections() const { return _receiveProjections; }

	inline void setAlphaCutoff(float c) { setEnabled(kAlphaCutoff); _alphaCutoff = c; }
	inline float getAlphaCutoff() const { return _alphaCutoff; }
	
	inline void setShininessMask(const std::string & shininessMask, int channel = 0, bool invert = false) {
		setEnabled(kShininessMask);
		_shininessMask = shininessMask;
		_shininessMaskChannel = channel;
		_shininessMaskInvert = invert;
	}
	inline const std::string & getShininessMask() const { return _shininessMask; }
	inline std::string & getShininessMask() { return _shininessMask; }
	inline int getShininessMaskChannel() const { return _shininessMaskChannel; }
	inline bool getInvertShininessMask() const { return _shininessMaskInvert; }
	inline void setInvertShininessMask(bool invert) { _shininessMaskInvert = invert; }
	
	inline void setLightEmissionMask(const std::string & leMask, int channel = 0, bool invert = false) {
		setEnabled(kLightEmissionMask);
		_lightEmissionMask = leMask;
		_lightEmissionMaskChannel = channel;
		_lightEmissionMaskInvert = invert;
	}
	inline const std::string & getLightEmissionMask() const { return _lightEmissionMask; }
	inline std::string & getLightEmissionMask() { return _lightEmissionMask; }
	inline int getLightEmissionMaskChannel() const { return _lightEmissionMaskChannel; }
	inline bool getInvertLightEmissionMask() const { return _lightEmissionMaskInvert; }
	inline void setInvertLightEmissionMask(bool invert) { _lightEmissionMaskInvert = invert; }
	
	
	void operator = (const MaterialModifier & mod) {
		_modifierFlags = mod._modifierFlags;
		_diffuse = mod._diffuse;
		_specular = mod._specular;
		_shininess = mod._shininess;
		_lightEmission = mod._lightEmission;
		_refractionAmount = mod._refractionAmount;
		_reflectionAmount = mod._reflectionAmount;
		_texture = mod._texture;
		_lightmap = mod._lightmap;
		_normalMap = mod._normalMap;
		_lightNormalMap = mod._lightNormalMap;
		_textureOffset = mod._textureOffset;
		_textureScale = mod._textureScale;
		_lightmapOffset = mod._lightmapOffset;
		_lightmapScale = mod._lightmapScale;
		_normalMapOffset = mod._normalMapOffset;
		_normalMapScale = mod._normalMapScale;
		_cubeMapPosX = mod._cubeMapPosX;
		_cubeMapNegX = mod._cubeMapNegX;
		_cubeMapPosY = mod._cubeMapPosY;
		_cubeMapNegY = mod._cubeMapNegY;
		_cubeMapPosZ = mod._cubeMapPosZ;
		_cubeMapNegZ = mod._cubeMapNegZ;
		_castShadows = mod._castShadows;
		_receiveShadows = mod._receiveShadows;
		_receiveProjections = mod._receiveProjections;
		_alphaCutoff = mod._alphaCutoff;
		_shininessMask = mod._shininessMask;
		_shininessMaskChannel = mod._shininessMaskChannel;
		_shininessMaskInvert = mod._shininessMaskInvert;
		_lightEmissionMask = mod._lightEmissionMask;
		_lightEmissionMaskChannel = mod._lightEmissionMaskChannel;
		_lightEmissionMaskInvert = mod._lightEmissionMaskInvert;
	}
	
	MaterialModifier()
		:_modifierFlags(0),
		_diffuse(Color(1.0f,1.0f,1.0f,1.0)),
		_specular(Color(1.0f,1.0f,1.0f,1.0)),
		_shininess(0.0f),
		_lightEmission(0.0f),
		_refractionAmount(0.0f),
		_reflectionAmount(0.0f),
		_texture(""),
		_lightmap(""),
		_normalMap(""),
		_lightNormalMap(""),
		_textureOffset(Vector2(0.0f,0.0f)),
		_textureScale(Vector2(1.0f,1.0f)),
		_lightmapOffset(Vector2(0.0f,0.0f)),
		_lightmapScale(Vector2(1.0f,1.0f)),
		_normalMapOffset(Vector2(0.0f,0.0f)),
		_normalMapScale(Vector2(1.0f,1.0f)),
		_cubeMapPosX(""),
		_cubeMapNegX(""),
		_cubeMapPosY(""),
		_cubeMapNegY(""),
		_cubeMapPosZ(""),
		_cubeMapNegZ(""),
		_castShadows(true),
		_receiveShadows(true),
		_receiveProjections(false),
		_alphaCutoff(0.5f),
		_shininessMask(""),
		_shininessMaskChannel(0),
		_shininessMaskInvert(false),
		_lightEmissionMaskChannel(0),
		_lightEmissionMaskInvert(false)
	{
	}

protected:
	unsigned int _modifierFlags;
	
	Color _diffuse;
	Color _specular;
	float _shininess;
	float _lightEmission;
	float _refractionAmount;
	float _reflectionAmount;
	std::string _texture;
	std::string _lightmap;
	std::string _normalMap;
	std::string _lightNormalMap;
	Vector2 _textureOffset;
	Vector2 _textureScale;
	Vector2 _lightmapOffset;
	Vector2 _lightmapScale;
	Vector2 _normalMapOffset;
	Vector2 _normalMapScale;
	std::string _cubeMapPosX;
	std::string _cubeMapNegX;
	std::string _cubeMapPosY;
	std::string _cubeMapNegY;
	std::string _cubeMapPosZ;
	std::string _cubeMapNegZ;
	bool _castShadows;
	bool _receiveShadows;
	bool _receiveProjections;
	float _alphaCutoff;
	std::string _shininessMask;
	int _shininessMaskChannel;
	bool _shininessMaskInvert;
	std::string _lightEmissionMask;
	int _lightEmissionMaskChannel;
	bool _lightEmissionMaskInvert;
};

class VWGLEXPORT GenericMaterial : public Material {
public:
	GenericMaterial();

	virtual Material * clone();
	
	virtual void initShader();
	
	virtual bool isTransparent() { return _diffuse.a()!=1.0f; }

	inline void setDiffuse(const Vector4 & d) { _diffuse = d; }
	inline Vector4 & getDiffuse() { return _diffuse; }
	inline const Vector4 & getDiffuse() const { return _diffuse; }
	
	inline void setSpecular(const Vector4 & d) { _specular = d; }
	inline Vector4 & getSpecular() { return _specular; }
	inline const Vector4 & getSpecular() const { return _specular; }
	
	inline void setShininess(float s) { _shininess = s; }
	inline float getShininess() const { return _shininess; }
	
	inline void setLightEmission(float e) { _lightEmission = e; }
	inline float getLightEmission() const { return _lightEmission; }
	
	inline void setRefractionAmount(float r) { _refractionAmount = r; }
	inline float getRefractionAmount() const { return _refractionAmount; }
	
	inline void setReflectionAmount(float r) { _reflectionAmount = r; }
	inline float getReflectionAmount() const { return _reflectionAmount; }

	inline void setTexture(Texture * t) { _texture = t; }
	inline Texture * getTexture() { return _texture.getPtr(); }
	const std::string & getTexturePath() const;
	
	inline void setLightMap(Texture * t) { _lightMap = t; }
	inline Texture * getLightMap() { return _lightMap.getPtr(); }
	const std::string & getLightMapPath() const;
	
	inline void setNormalMap(Texture * t) { _normalMap = t; }
	inline Texture * getNormalMap() { return _normalMap.getPtr(); }
	const std::string & getNormalMapPath() const;
	
	inline void setLightNormalMap(Texture * t) { _lightNormalMap = t; }
	inline Texture * getLightNormalMap() { return _lightNormalMap.getPtr(); }
	const std::string & getLightNormalMapPath() const;
	
	inline void setTextureOffset(const Vector2 & o) { _textureOffset = o; }
	inline Vector2 & getTextureOffset() { return _textureOffset; }
	inline const Vector2 & getTextureOffset() const { return _textureOffset; }
	
	inline void setTextureScale(const Vector2 & o) { _textureScale = o; }
	inline Vector2 & getTextureScale() { return _textureScale; }
	inline const Vector2 & getTextureScale() const { return _textureScale; }
	
	inline void setLightmapOffset(const Vector2 & o) { _lightmapOffset = o; }
	inline Vector2 & getLightmapOffset() { return _lightmapOffset; }
	inline const Vector2 & getLightmapOffset() const { return _lightmapOffset; }
	
	inline void setLightmapScale(const Vector2 & o) { _lightmapScale = o; }
	inline Vector2 & getLightmapScale() { return _lightmapScale; }
	inline const Vector2 & getLightmapScale() const { return _lightmapScale; }
	
	inline void setNormalMapScale(const Vector2 & o) { _normalMapScale = o; }
	inline Vector2 & getNormalMapScale() { return _normalMapScale; }
	inline const Vector2 & getNormalMapScale() const { return _normalMapScale; }
	
	inline void setNormalMapOffset(const Vector2 & o) { _normalMapOffset = o; }
	inline Vector2 & getNormalMapOffset() { return _normalMapOffset; }
	inline const Vector2 & getNormalMapOffset() const { return _normalMapOffset; }
	
	inline void setCubeMap(CubeMap * cubeMap) { _cubeMap = cubeMap; }
	inline CubeMap * getCubeMap() { return _cubeMap.getPtr(); }
	
	const std::string & getCubeMapPosXPath() const;
	const std::string & getCubeMapNegXPath() const;
	const std::string & getCubeMapPosYPath() const;
	const std::string & getCubeMapNegYPath() const;
	const std::string & getCubeMapPosZPath() const;
	const std::string & getCubeMapNegZPath() const;
	
	inline void setCastShadows(bool s) { _castShadows = s; }
	inline bool getCastShadows() const { return _castShadows; }
	
	inline void setReceiveShadows(bool s) { _receiveShadows = s; }
	inline bool getReceiveShadows() const { return _receiveShadows; }
	
	inline void setSelectedMode(bool selected) { _selectedMode = selected; }
	inline bool getSelectedMode() const { return _selectedMode; }

	inline void setReceiveProjections(bool p) { _receiveProjections = p; }
	inline bool getReceiveProjections() const { return _receiveProjections; }
	
	inline void setAlphaCutoff(float c) { _alphaCutoff = c; }
	inline float getAlphaCutoff() const { return _alphaCutoff; }
	
	// The shininessMask uses the diffuse texture offset and scale
	inline void setShininessMask(Texture * t, int channel = 0, bool invert = false) {
		_shininessMask = t;
		_shininessMaskChannel = channel;
		_shininessMaskInvert = invert;
	}
	inline Texture * getShininessMask() { return _shininessMask.getPtr(); }
	const std::string & getShininessMaskPath() const;
	inline int getShininessMaskChannel() const { return _shininessMaskChannel; }
	inline bool getInvertShininessMask() const { return _shininessMaskInvert; }
	inline void setInvertShininessMask(bool invert) { _shininessMaskInvert = invert; }
	
	// The lightEmissionMask uses the diffuse texture offset and scale
	inline void setLightEmissionMask(Texture * tex, int channel = 0, bool invert = false) {
		_lightEmissionMask = tex;
		_lightEmissionMaskChannel = channel;
		_lightEmissionMaskInvert = invert;
	}
	inline Texture * getLightEmissionMask() { return _lightEmissionMask.getPtr(); }
	const std::string & getLightEmissionMaskPath() const;
	inline int getLightEmissionMaskChannel() const { return _lightEmissionMaskChannel; }
	inline bool getInvertLightEmissionMask() const { return _lightEmissionMaskInvert; }
	inline void setInvertLightEmissionMask(bool invert) { _lightEmissionMaskInvert = invert; }

	void destroy();
	
	void useSettingsOf(Material * tex) { _settingsMaterial = dynamic_cast<GenericMaterial*>(tex); }
	
	void applySettings(Drawable * drawable, unsigned int settings);
	
	void applyModifier(const MaterialModifier & mod, const std::string & resourcePath, bool forceResourcePath = true);
	vwgl::MaterialModifier getModifierWithMask(unsigned int modifierMask);
	
	// This option only works in forward render mode
	inline void useLight(Light * light) { _light = light; }
	inline Light * getLight() { return _light.getPtr(); }
	
	static void setMaterialsWithJson(Drawable * drawable, const std::string & jsonString);
	static void setMaterialsWithJson(scene::Drawable * drawable, const std::string & jsonString);
	static void getMaterialResourcesWithJson(std::vector<std::string > & resources, const std::string & jsonString);
	
	enum MaterialSettings {
		kDiffuseColor			= 1 << 0,
		kShininess				= 1 << 1,
		kRefractionAmount		= 1 << 2,
		kLightEmission			= 1 << 3,
		kTexture				= 1 << 4,
		kLightMap				= 1 << 5,
		kNormalMap				= 1 << 6,
		kTextureOffset			= 1 << 7,
		kTextureScale			= 1 << 8,
		kLightmapOffset			= 1 << 9,
		kLightmapScacle			= 1 << 10,
		kCastShadows			= 1 << 11,
		kReceiveShadows			= 1 << 12,
		kSelectedMode			= 1 << 13,
		kAlphaCutoff			= 1 << 14,
		kShininessMask			= 1 << 15,
		kLightEmissionMask		= 1 << 16,
		kCubeMap				= 1 << 17
	};
	

protected:
	virtual ~GenericMaterial();
	
	virtual void cloneContent(Material * newMat);
	
	virtual void setupUniforms();
	
	Vector4 _diffuse;
	Vector4 _specular;
	float _shininess;
	float _refractionAmount;
	float _reflectionAmount;
	float _lightEmission;
	ptr<Texture> _texture;
	ptr<Texture> _lightMap;
	ptr<Texture> _normalMap;
	ptr<Texture> _lightNormalMap;
	Vector2 _textureOffset;
	Vector2 _textureScale;
	Vector2 _lightmapOffset;
	Vector2 _lightmapScale;
	Vector2 _normalMapOffset;
	Vector2 _normalMapScale;
	ptr<CubeMap> _cubeMap;
	bool _castShadows;
	bool _receiveShadows;
	bool _selectedMode;
	bool _receiveProjections;
	float _alphaCutoff;
	ptr<Texture> _shininessMask;
	int _shininessMaskChannel;
	bool _shininessMaskInvert;
	ptr<Texture> _lightEmissionMask;
	int _lightEmissionMaskChannel;
	bool _lightEmissionMaskInvert;
	
	ptr<GenericMaterial> _settingsMaterial;
	
	ptr<Light> _light;
	
	void applyMaterialSettings(GenericMaterial * mat, unsigned int settings);
	
	void getTextureAbsolutePath(const std::string & input, std::string & output, const std::string & resourcePath, bool forceResourcePath);
	
};

}

#endif
