
#ifndef _vwgl_colladaplugin_hpp_
#define _vwgl_colladaplugin_hpp_

#include <vwgl/readerplugin.hpp>
#include <vwgl/collada/colladautils.hpp>
#include <vwgl/scene/drawable.hpp>

namespace vwgl {
	
class ColladaPluginException : public LoadDrawableException {
public:
	explicit ColladaPluginException() :LoadDrawableException("Error loading collada file") {}
	explicit ColladaPluginException(const std::string & msg) :LoadDrawableException(msg) {}
	virtual ~ColladaPluginException() throw () {}
};

class VWGLEXPORT ColladaToDrawableUtils {
public:
	typedef std::vector<ptr<collada::Geometry> > DaeGeometryList;
	ColladaToDrawableUtils(collada::Collada * collada) { _collada = collada; }
	
	scene::Drawable * buildDrawable();

	bool getNonTriangularWarning() { return _nonTriangularPlist; }

protected:
	ptr<collada::Collada> _collada;
	DaeGeometryList _geometryList;
	
	PolyList * getPolyList(collada::Polylist * c_plist, collada::Mesh * mesh);
	PolyList * getPolyList(collada::Triangles * c_triangles, collada::Mesh * mesh);
	
	void applyNodeSettings(collada::SceneNode * node, PolyList * plist);
	
	void fillInSources(collada::InputList & inputs, std::vector<collada::Source*> & sources, collada::Mesh * mesh);
	
	bool _nonTriangularPlist;
};

class VWGLEXPORT ColladaModelReader : public ReadModelPlugin {
public:
	ColladaModelReader();

	virtual bool acceptFileType(const std::string & path);
	
	virtual scene::Drawable * loadModel(const std::string & path);

protected:
	virtual ~ColladaModelReader();
};

}

#endif
