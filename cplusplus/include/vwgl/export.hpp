#ifndef _vwgl_export_hpp_
#define _vwgl_export_hpp_

#include <vwgl/platform.hpp>

#ifndef WIN32
#define VWGLEXPORT

#else

#pragma warning( disable: 4251 )
#pragma warning( disable: 4275 )

#if defined( VWGL_STATIC_LIB )
#define VWGLEXPORT

#elif defined( VWGL_DYNAMIC_LIB )
#define VWGLEXPORT __declspec(dllexport)

#else
#define VWGLEXPORT __declspec(dllimport)

#ifdef _MSC_VER
#ifdef _DEBUG
#pragma comment(lib ,"vwgld")

#else
#pragma comment(lib, "vwgl")

#endif

#endif

#endif

#endif

#endif	// _vwgl_export_hpp_


