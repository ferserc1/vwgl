
#ifndef _vwgl_cube_hpp_
#define _vwgl_cube_hpp_

#include <vwgl/drawable.hpp>
#include <vwgl/boundingbox.hpp>

namespace vwgl {

class VWGLEXPORT Cube: public Drawable {
public:
	Cube();
	Cube(float side);
	Cube(float w, float h, float d);
	Cube(BoundingBox * bbox);

protected:
	virtual ~Cube();
	
	void buildCube(float w, float h, float d);
};
}

#endif
