
#ifndef _vwgl_state_hpp_
#define _vwgl_state_hpp_

#include <vwgl/math.hpp>
#include <vwgl/referenced_pointer.hpp>

#include <vector>
#include <iostream>

namespace vwgl {

class VWGLEXPORT State : public Singleton {
public:
	enum Buffer {
		kBufferColor = 1 << 0,
		kBufferDepth = 1 << 1
		// TODO: add other buffers
	};
	
	// High level render functions
	enum BlendFunction {
		kBlendFunctionCurrent,			// enable blend using the current blend function
		kBlendFunctionAlpha,			// use to render translucent objects
		kBlendFunctionAddShadow,		// use to render shadows over the current color buffer
		kBlendFunctionAddLight			// use to render lights in more than one pass
	};

	static State * get();
	static State * set(State * state);
	
	virtual void finalize();

	Matrix4 & viewMatrix() { _mvMatrixDirty = true; _nMatrixDirty = true; return _viewMatrix; }
	Matrix4 & modelMatrix() { _mvMatrixDirty = true; _nMatrixDirty = true; return _modelMatrix; }
	Matrix4 & projectionMatrix() { return _projectionMatrix; }
	Matrix4 & modelViewMatrix() {
		if (_mvMatrixDirty) {
			_modelViewMatrix = Matrix4(_viewMatrix);
			_modelViewMatrix.mult(modelMatrix());
			_mvMatrixDirty = false;
		}
		return _modelViewMatrix;
	}
	
	Matrix4 & normalMatrix() {
		if (_nMatrixDirty) {
			_normalMatrix = Matrix4(modelViewMatrix());
			_normalMatrix.invert();
			_normalMatrix.traspose();
			_nMatrixDirty = false;
		}
		return _normalMatrix;
	}

	void pushViewMatrix();
	void popViewMatrix();
	
	void pushModelMatrix();
	void popModelMatrix();
	
	void pushProjectionMatrix();
	void popProjectionMatrix();
	
	void setOpenGLState();
	
	void setViewport(const Viewport & vp);
	const Viewport & getViewport() const { return _viewport; }
	Viewport & getViewport() { return _viewport; }
	
	void enableDepthMask();
	void disableDepthMask();
	
	void enableFillOffset();	// Enable polygon offset, keep current factor and units values
	void enableFillOffset(float factor, float units);
	void disableFillOffset();
	void enableLineOffset();	// Enable polygon offset, keep current factor and units values
	void enableLineOffset(float factor, float units);
	void disableLineOffset();
	void enablePointOffset();	// Enable polygon offset, keep current factor and units values
	void enablePointOffset(float factor, float units);
	void disablePointOffset();
	
	void enableBlend(BlendFunction function = kBlendFunctionCurrent);
	void disableBlend();
	void enableDepthTest();
	void disableDepthTest();
	void setClearColor(const Color & color);
	void clearBuffers(int buffers);

protected:
	Matrix4 _modelMatrix;
	Matrix4 _viewMatrix;
	Matrix4 _projectionMatrix;
	
	bool _mvMatrixDirty;
	bool _nMatrixDirty;
	Matrix4 _modelViewMatrix;
	Matrix4 _normalMatrix;

	std::vector<Matrix4> _modelMatrixStack;
	std::vector<Matrix4> _viewMatrixStack;
	std::vector<Matrix4> _projectionMatrixStack;
	
	vwgl::Viewport _viewport;
	
	static State * s_state;
	
	State();
	virtual ~State();
};

}

#endif
