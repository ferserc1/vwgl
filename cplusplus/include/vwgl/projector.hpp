#ifndef _vwgl_projector_hpp_
#define _vwgl_projector_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/transform_visitor.hpp>
#include <vwgl/node.hpp>
#include <vwgl/matrix.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/projectable.hpp>

namespace vwgl {

class VWGLEXPORT Projector : public Node, public IProjectable {
public:
	Projector();

	virtual void setProjection(const Matrix4 & proj) { _projection = proj; }
	virtual Matrix4 & getProjection() { return _projection; }
	virtual const Matrix4 & getProjection() const { return _projection; }
	
	virtual void setTransform(const Matrix4 & trans) { _transform = trans; }
	virtual Matrix4 & getRawTransform() { return _transform; }
	virtual const Matrix4 & getRawTransform() const { return _transform; }
	
	virtual void setAttenuation(float att) { _attenuation = att; }
	virtual float getAttenuation() const { return _attenuation; }
	
	virtual Vector3 getPosition();
	virtual Vector3 getDirection();
	
	virtual Matrix4 getTransform();
	
	virtual void setTexture(Texture * tex) { _texture = tex; }
	virtual Texture * getTexture() { return _texture.getPtr(); }
	virtual const Texture * getTexture() const { return _texture.getPtr(); }
	
	inline void setVisible(bool v) { _visible = v; }
	inline bool isVisible() { return _visible && getParent(); }
	inline void show() { _visible = true; }
	inline void hide() { _visible = false; }

protected:
	virtual ~Projector();
	
	Matrix4 _projection;
	Matrix4 _transform;
	float _attenuation;
	ptr<TransformVisitor> _transformVisitor;
	Matrix4 _fooTransform;
	ptr<Texture> _texture;
	bool _visible;
};

class VWGLEXPORT ProjectorManager {
	friend class Projector;
public:
	typedef std::vector<Projector*> ProjectorList;
	
	ProjectorManager();
	virtual ~ProjectorManager();
	
	static ProjectorManager * get();
	
	ProjectorList & getProjectorList() { return _projectorList; }
	
protected:
	
	inline void addProjector(Projector * proj);
	inline void removeProjector(Projector * proj);
	
	ProjectorList _projectorList;
};

}
#endif
