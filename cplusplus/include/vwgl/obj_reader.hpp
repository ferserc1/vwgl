
#ifndef _vwgl_obj_plugin_hpp_
#define _vwgl_obj_plugin_hpp_

#include <vwgl/readerplugin.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/vector.hpp>

#include <vector>
#include <fstream>

namespace vwgl {

class ObjPluginException : public LoadDrawableException {
public:
	explicit ObjPluginException() :LoadDrawableException("Error loading obj file") {}
	explicit ObjPluginException(const std::string & msg) :LoadDrawableException(msg) {}
	virtual ~ObjPluginException() throw () {}
};
	
class ObjInvalidVertexException : public ObjPluginException {
public:
	explicit ObjInvalidVertexException() :ObjPluginException("Invalid number of vertex found.") {}
	explicit ObjInvalidVertexException(const std::string & msg) :ObjPluginException(msg) {}
	virtual ~ObjInvalidVertexException() throw () {}
};
	
class ObjInvalidFacesException : public ObjPluginException {
public:
	explicit ObjInvalidFacesException() :ObjPluginException("Invalid faces found. The object must contain vertex, texture coords and normal coords.") {}
	explicit ObjInvalidFacesException(const std::string & msg) :ObjPluginException(msg) {}
	virtual ~ObjInvalidFacesException() throw () {}
};

class VWGLEXPORT ObjModelReader : public ReadModelPlugin {
public:
	ObjModelReader();
	
	virtual bool acceptFileType(const std::string & path);
	
	virtual scene::Drawable * loadModel(const std::string & path);
	
protected:
	virtual ~ObjModelReader();
	
	std::fstream _file;
};
	
}

#endif
