
#ifndef _vwgl_loader_hpp_
#define _vwgl_loader_hpp_

#include <vwgl/drawable.hpp>
#include <vwgl/image.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/writterplugin.hpp>
#include <vwgl/readerplugin.hpp>
#include <vwgl/cubemap.hpp>
#include <vwgl/dictionary.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/text_font.hpp>

#include <string>
#include <vector>
#include <unordered_map>

namespace vwgl {

class VWGLEXPORT Loader : Singleton {
public:
	typedef	std::vector<ptr<WritterPlugin> > WritterList;
	typedef	std::vector<ptr<ReaderPlugin> > ReaderList;
	
	static Loader * get();

	vwgl::library::Node * loadLibrary(const std::string & path, bool loadFromResources = false);
	
	// API 1.0
	vwgl::Node * loadNode(const std::string & path, bool loadFromResources = false);
	vwgl::Drawable * loadDrawable(const std::string & path, bool loadFromResources = false);
	
	// API 2.0
	vwgl::scene::Node * loadScene(const std::string & path, bool loadFromResources = false);
	vwgl::scene::Node * loadPrefab(const std::string & path, bool loadFromResources = false);
	vwgl::scene::Drawable * loadModel(const std::string & path, bool loadFromResources = false);
	
	vwgl::TextFont * loadFont(const std::string & path, int size, int res = 100, bool loadFromResources = false);
	
	
	// path: path to the drawable file
	// deps: (out) drawable's dependencies
	// loadFromResources: path is a relative path from the resource path
	void getDrawableDependencies(const std::string & path, std::vector<std::string> & deps, bool loadFromResources = false);
	vwgl::Image * loadImage(const std::string & path, bool loadFromResources = false);
	vwgl::Texture * loadTexture(const std::string & path, bool loadFromResources = false);
	vwgl::CubeMap * loadCubemap(const std::string & posX,
								const std::string & negX,
								const std::string & posY,
								const std::string & negY,
								const std::string & posZ,
								const std::string & negZ, bool loadFromResources = false);
	vwgl::Dictionary * loadDictionary(const std::string &path, bool loadFromResources = false);
	
	bool canReadFile(const std::string & file);
	bool canWriteFile(const std::string & file);
	bool registerWritter(WritterPlugin * plugin);
	bool registerReader(ReaderPlugin * plugin);
	
	bool writeNode(const std::string & path, Node * node);
	bool writeLibrary(const std::string & path, library::Node * lib);
	bool writeDrawable(const std::string & path, Drawable * drawable);
	bool writeDictionary(const std::string & path, Dictionary * dict);
	
	// API 2.0
	bool writeModel(const std::string & path, scene::Drawable * model);
	// This function will write a prefab or a model, depending on the file extension (vwglb: prefab, vitscn: scene)
	bool writeNode(const std::string &path, scene::Node * node);

	inline bool write(const std::string & path, Node * node) { return writeNode(path, node); }
	inline bool write(const std::string & path, library::Node * node) { return writeLibrary(path, node); }
	inline bool write(const std::string & path, Drawable * drawable) { return writeDrawable(path, drawable); }
	inline bool write(const std::string & path, Dictionary * dict) { return writeDictionary(path, dict); }
	inline bool write(const std::string & path, vwgl::scene::Drawable * model) { return writeModel(path, model); }
	inline bool write(const std::string & path, vwgl::scene::Node * node) { return writeNode(path, node); }
	
	void setCurrentDir(const std::string & dir) { _currentDir = dir; }
	const std::string & getCurrentDir() const { return _currentDir; }

	
	std::vector<std::string> & getWarningMessages() { return _warningMessages; }
	void clearWarnings() { _warningMessages.clear(); }

	virtual void finalize();

protected:
	Loader();
	virtual ~Loader();
	
	static Loader * s_loader;
	
	WritterList _writterPluginList;
	ReaderList _readerPluginList;
	std::string _currentDir;
	
	std::vector<std::string> _warningMessages;
	typedef std::unordered_map<std::string, ptr<Image> > ImageCache;
	ImageCache _imageCache;
};

}

#endif
