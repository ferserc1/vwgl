//
//  json_serializer.hpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 28/04/14.
//  Copyright (c) 2014 Vitaminew. All rights reserved.
//

#ifndef vwgl_json_serializer_hpp
#define vwgl_json_serializer_hpp

#include <vwgl/export.hpp>
#include <vwgl/math.hpp>

#include <ostream>
namespace vwgl {

class JsonSerializer {
public:
	JsonSerializer(std::ostream & output) :_out (&output) { }
	JsonSerializer() :_out (nullptr) {}
	
	inline void setOutputStream(std::ostream & output) { _out = &output; }
	
	JsonSerializer & operator << (const std::string & str) { (*_out) << str; return *this; }

	inline void openObject(bool insertTabs, bool endLine) {
		if (insertTabs) {
			tabulate();
		}
		(*_out) << "{";
		if (endLine) {
			(*_out) << std::endl;
			++_currentTabs;
		}
	}
	
	inline void closeObject(bool insertTabs, bool endSeparator, bool endLine) {
		if (insertTabs) {
			--_currentTabs;
			tabulate();
		}
		(*_out) << "}";
		if (endSeparator) (*_out) << ",";
		if (endLine) (*_out) << std::endl;
	}
	
	inline void openArray(bool insertTabs, bool endLine) {
		if (insertTabs) {
			tabulate();
		}
		(*_out) << "[";
		if (endLine) {
			(*_out) << std::endl;
			++_currentTabs;
		}
	}
	
	inline void closeArray(bool insertTabs, bool endSeparator, bool endLine) {
		if (insertTabs) {
			--_currentTabs;
			tabulate();
		}
		(*_out) << "]";
		if (endSeparator) (*_out) << ",";
		if (endLine) (*_out) << std::endl;
	}
	
	inline void writeKey(const std::string & name, int tabs) {
		tabulate(tabs);
		writeValue(name,false,false);
		(*_out) << ":";
	}
	
	inline void writeKey(const std::string &name) {
		writeKey(name, _currentTabs);
	}
	
	inline void writeValue(const std::string & val, bool endSeparator, bool endLine) {
		(*_out) << "\"" << val << "\"";
		endValue(endSeparator,endLine);
	}
	
	inline void writeValue(const Vector2 & val, bool endSeparator, bool endLine) {
		(*_out) << "[" << val.x() << "," << val.y() << "]";
		endValue(endSeparator, endLine);
	}
	
	inline void writeValue(const Vector3 & val, bool endSeparator, bool endLine) {
		(*_out) << "[" << val.x() << "," << val.y() << "," << val.z() << "]";
		endValue(endSeparator, endLine);
	}
	
	inline void writeValue(const Vector4 & val, bool endSeparator, bool endLine) {
		(*_out) << "[" << val.x() << "," << val.y() << "," << val.z() << "," << val.w() << "]";
		endValue(endSeparator, endLine);
	}
	
	inline void writeValue(const Viewport & val, bool endSeparator, bool endLine) {
		(*_out) << "[" << val.x() << "," << val.y() << "," << val.width() << "," << val.height() << "]";
		endValue(endSeparator, endLine);
	}
	
	inline void writeValue(const Matrix4 & val, bool endSeparator, bool endLine) {
		(*_out) << "[" << val.get00() << "," << val.get01() << "," << val.get02() << "," << val.get03() << ",";
		(*_out) << val.get10() << "," << val.get11() << "," << val.get12() << "," << val.get13() << ",";
		(*_out) << val.get20() << "," << val.get21() << "," << val.get22() << "," << val.get23() << ",";
		(*_out) << val.get30() << "," << val.get31() << "," << val.get32() << "," << val.get33() << "]";
		endValue(endSeparator, endLine);
	}
	
	inline void tabulate(int tabs) {
		for (int i = 0; i<tabs; ++i) {
			(*_out) << "\t";
		}
	}
	
	inline void tabulate() {
		tabulate(_currentTabs);
	}
	
	inline void endValue(bool endSeparator, bool endLine) {
		if (endSeparator) (*_out) << ",";
		if (endLine) (*_out) << std::endl;
	}
	
	template <class T> void writeValue(T val, bool endSeparator = true, bool endLine = true) {
		(*_out) << val;
		endValue(endSeparator,endLine);
	}
	
	template <class T> void writeProp(const std::string &key, T val, int tabs, bool endSeparator, bool endLine) {
		writeKey(key,tabs);
		writeValue(val,endSeparator,endLine);
	}
	
	template <class T> void writeProp(const std::string &key, T val, bool endSeparator, bool endLine) {
		writeKey(key);
		writeValue(val,endSeparator,endLine);
	}

protected:
	std::ostream * _out;
	int _currentTabs;
};

typedef void JsonLibPtr;

class VWGLEXPORT JsonDeserializer {
public:
	JsonDeserializer() :_jsonLib(nullptr) {}
	JsonDeserializer(JsonLibPtr * libPtr) :_jsonLib(libPtr) {}

	inline void setJsonLibPtr(JsonLibPtr * libPtr) { _jsonLib = libPtr; }
	inline JsonLibPtr * getJsonLibPtr() { return _jsonLib; }
	
	float getFloat(const std::string & key, float defaultValue);
	int getInt(const std::string & key, int defaultValue);
	bool getBool(const std::string & key, bool defaultValue);
	std::string getString(const std::string & key, const std::string & defaultValue);
	Vector2 getVector2(const std::string & key, const Vector2 & defaultValue);
	Vector3 getVector3(const std::string & key, const Vector3 & defaultValue);
	Vector4 getVector4(const std::string & key, const Vector4 & defaultValue);
	Viewport getViewport(const std::string & key, const Viewport & defaultValue);
	Matrix3 getMatrix3(const std::string & key, const Matrix3 & defaultValue);
	Matrix4 getMatrix4(const std::string & key, const Matrix4 & defaultValue);
	
	bool keyExists(const std::string & key);
	
protected:
	JsonLibPtr * _jsonLib;
};

}

#endif
