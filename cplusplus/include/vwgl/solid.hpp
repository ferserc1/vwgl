
#ifndef _vwgl_solid_hpp_
#define _vwgl_solid_hpp_

#include <vwgl/opengl.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/color_pick_material.hpp>
#include <vwgl/projector.hpp>
#include <vwgl/transform_strategy.hpp>

#include <vector>

namespace vwgl {

class Drawable;
class VWGLEXPORT Solid : public ReferencedPointer, Transformable {
	friend class ColorPicker;
	friend class PickIdFindVisitor;
	friend class Drawable;
public:
	enum CloneMode {
		kCloneModeInstance = 1,
		kCloneModeCopy
	};

	Solid();
	Solid(PolyList * plist);
	Solid(Material * mat);
	Solid(PolyList * plist, Material * mat);
	
	void setTransform(const Matrix4 & transform) { _transform = transform; }
	const Matrix4 & getTransform() const { return _transform; }
	Matrix4 & getTransform() { return _transform; }
	
	inline void setTransformStrategy(TransformStrategy * strategy) {
		_transformStrategy = strategy;
		if (_transformStrategy.valid()) {
			_transformStrategy->setTransformable((this));
			_transformStrategy->updateTransform();
		}
	}
	inline TransformStrategy * getTransformStrategy() { return _transformStrategy.getPtr(); }
	inline const TransformStrategy * getTransformStrategy() const { return _transformStrategy.getPtr(); }
	
	Solid * clone(CloneMode polyListClone = kCloneModeCopy, CloneMode materialClone = kCloneModeCopy);

	void setPolyList(PolyList * plist);
	void setMaterial(Material * mat);
	
	PolyList * getPolyList();
	Material * getMaterial();
	inline GenericMaterial * getGenericMaterial() { return dynamic_cast<GenericMaterial*>(getMaterial()); }
	
	void draw(Material * mat = NULL, bool useMaterialSettings = false);

	void destroy();
	
	const PickId & getPickId() const { return _pickId; }
	PickId & getPickId() { return _pickId; }
	
	void setRenderOpaque(bool opaque) { _renderOpaque = opaque; }
	bool getRenderOpaque() const { return _renderOpaque; }
	
	void setRenderTransparent(bool transparent) { _renderTransparent = transparent; }
	bool getRenderTransparent() const { return _renderTransparent; }
	
	void setShadowProjector(Projector * proj);
	inline Projector * getShadowProjector() { return _shadowProjector.getPtr(); }
	inline const Projector * getShadowProjector() const { return _shadowProjector.getPtr(); }

	inline void setVisible(bool visible) { _visible = visible; if (_shadowProjector.valid()) _shadowProjector->setVisible(_visible); }
	inline bool isVisible() const { return _visible; }
	inline void show() { setVisible(true); }
	inline void hide() { setVisible(false); }
	
	inline void setGroupName(const std::string & name) { _groupName = name; }
	inline const std::string & getGroupName() const { return _groupName; }

	inline Drawable * getDrawable() { return _drawable; }

protected:
	virtual ~Solid();
	
	ptr<PolyList> _polyList;
	ptr<Material> _material;
	ptr<Projector> _shadowProjector;
	Drawable * _drawable;
	PickId _pickId;
	std::string _groupName;
	
	bool _renderOpaque;
	bool _renderTransparent;
	bool _visible;
	
	Matrix4 _transform;
	ptr<TransformStrategy> _transformStrategy;
	
	void removeProjectorFromScene();
};
	
typedef std::vector<ptr<Solid> > SolidList;

}

#endif
