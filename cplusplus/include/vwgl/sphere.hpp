
#ifndef _vwgl_sphere_hpp_
#define _vwgl_sphere_hpp_

#include <vwgl/drawable.hpp>

namespace vwgl {

class VWGLEXPORT Sphere : public Drawable {
public:
	Sphere();
	Sphere(float radius);
	Sphere(float radius, int divisions);
	Sphere(float radius, int slices, int stacks);

protected:
	virtual ~Sphere();
	
	void buildSphere(float radius, int slices, int stacks);
};
	
}

#endif
