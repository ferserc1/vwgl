
#ifndef _vwgl_selectiondeferredmaterial_hpp_
#define _vwgl_selectiondeferredmaterial_hpp_

#include <vwgl/vector.hpp>
#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class VWGLEXPORT SelectionDeferredMaterial : public DeferredMaterial {
public:
	SelectionDeferredMaterial();

	virtual void initShader();

protected:
	virtual ~SelectionDeferredMaterial();
	
	virtual void setupUniforms();
	
	void updateColor();

	vwgl::Vector3i _color;
};

}

#endif
