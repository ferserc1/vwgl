
#ifndef _vwgl_referenced_pointer_hpp_
#define _vwgl_referenced_pointer_hpp_

#include <vwgl/export.hpp>
#include <vector>
#include <algorithm>

namespace vwgl {

template <class T> T native_cast(void * nativePtr) { return static_cast<T>(nativePtr); }
typedef void * plain_ptr;

class VWGLEXPORT ReferencedPointer;

template <class T>
class ptr {
public:
	ptr() :_ptr(0L) {}
	ptr(T * p) :_ptr(p) { if (_ptr) _ptr->inc_ref(); }
	ptr(const ptr & p)  :_ptr(p._ptr) { if (_ptr) _ptr->inc_ref(); }
	~ptr() { if (_ptr) _ptr->dec_ref(); _ptr = 0L; }

	ptr& operator=(const ptr& p) {
		assign(p);
		return *this;
	}
	inline ptr & operator=(T * p) {
		if (_ptr==p) {
			return *this;
		}
		T * tmp_ptr = _ptr;
		_ptr = p;
		if (_ptr) _ptr->inc_ref();
		if (tmp_ptr) tmp_ptr->dec_ref();
		return *this;
	}
	bool operator==(const ptr & p) const { return _ptr==p._ptr; }
	bool operator==(const T * p) const { return _ptr==p; }
	friend bool operator==(const T* p, const ptr & rp) { return p==rp._ptr; }

	bool operator < (const ptr& p) const { return _ptr<p._ptr; }

	T& operator*() const { return *_ptr; }
	T* operator->() const { return _ptr; }

	bool operator!() const { return _ptr==0; }
	bool valid() const { return _ptr!=0; }
	bool isNull() const { return _ptr==0; }
	T * release() { T * tmp=_ptr; if (_ptr) _ptr->dec_ref_nodelete(); _ptr=0; return tmp; }
	void swap(ptr & p) { T * tmp = _ptr; _ptr=p._ptr; p._ptr = tmp; }

	bool operator==(T * p) const { return _ptr==p; }

	T * getPtr() { return _ptr; }
	const T * getPtr() const { return _ptr; }

	T * operator->() { return _ptr; }

protected:
	void assign(const ptr<T>& p) {
		if (_ptr==p._ptr) return;
		T * tmp = _ptr;
		_ptr = p._ptr;
		if (_ptr) _ptr->inc_ref();
		if (tmp) tmp->dec_ref();
	}

	T * _ptr;
};

class VWGLEXPORT ReferencedPointer {
public:
	ReferencedPointer();

	void inc_ref();
	void dec_ref();
	void dec_ref_nodelete();

	inline int getReferences() const { return _ref; }

protected:
	virtual ~ReferencedPointer();

	int _ref;
};

class Singleton {
public:
	virtual void finalize() = 0;
};

class VWGLEXPORT SingletonController {
public:
	static SingletonController * get();

	inline void registerSingleton(Singleton * singleton) {
		if (!isRegistered(singleton)) {
			_singletonList.push_back(singleton);
		}
	}

	inline void unregisterSingleton(Singleton * singleton) {
		std::vector<Singleton*>::iterator it = std::find(_singletonList.begin(), _singletonList.end(), singleton);
		if (it!=_singletonList.end()) {
			_singletonList.erase(it);
		}
	}

	inline bool isRegistered(Singleton * singleton) {
		return std::find(_singletonList.begin(), _singletonList.end(), singleton)!=_singletonList.end();
	}

	inline void postFinalize() {
		std::vector<Singleton*>::iterator it;
		for (it=_singletonList.begin(); it!=_singletonList.end(); ++it) {
			(*it)->finalize();
		}
	}


protected:
	std::vector<Singleton*> _singletonList;

	SingletonController() {}
	virtual ~SingletonController() {}

	static SingletonController * s_singletonController;
};

}

#endif
