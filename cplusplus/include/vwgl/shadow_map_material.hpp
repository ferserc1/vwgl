
#ifndef _vwgl_shadow_map_material_hpp_
#define _vwgl_shadow_map_material_hpp_

#include <vwgl/material.hpp>
#include <vwgl/generic_material.hpp>

namespace vwgl {

class VWGLEXPORT ShadowMapMaterial : public Material {
public:
	ShadowMapMaterial();

	virtual void initShader();

	virtual void useSettingsOf(Material * mat) { _settingsMaterial = dynamic_cast<GenericMaterial*>(mat); }

protected:
	virtual ~ShadowMapMaterial();

	virtual void setupUniforms();
	
	ptr<GenericMaterial> _settingsMaterial;
};

}

#endif
