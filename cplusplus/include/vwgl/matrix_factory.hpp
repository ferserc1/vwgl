
#ifndef _VWGL_MATRIX_FACTORY_HPP_
#define _VWGL_MATRIX_FACTORY_HPP_

#include <vwgl/math.hpp>
#include <vwgl/referenced_pointer.hpp>
#include <vwgl/json_serializer.hpp>

namespace vwgl {

class MatrixFactory : public ReferencedPointer {
public:
	static MatrixFactory * factory(JsonDeserializer & deserializer);

	MatrixFactory() :_matrix(nullptr) {}

	virtual void updateMatrix() = 0;

	inline bool valid() const { return _matrix!=nullptr; }
	inline void setMatrixRef(vwgl::Matrix4 * matrixRef) { _matrix = matrixRef; }
	inline vwgl::Matrix4 & getMatrix() { return *_matrix; }
	inline const vwgl::Matrix4 & getMatrix() const { return *_matrix; }

	virtual void serialize(JsonSerializer & serializer) = 0;
	virtual void deserialize(JsonDeserializer & deserializer) = 0;

protected:
	virtual ~MatrixFactory() {}

	vwgl::Matrix4 * _matrix;
};

class ProjectionFactoryMethod : public MatrixFactory {
public:
	ProjectionFactoryMethod() :MatrixFactory(), _near(0.1f), _far(1000.0f) {}

	inline void setViewport(const vwgl::Viewport & vp) { _viewport = vp; }
	inline const vwgl::Viewport & getViewport() const { return _viewport; }
	inline vwgl::Viewport & getViewport() { return _viewport; }
	inline void setNear(float n) { _near = n; }
	inline void setFar(float f) { _far = f; }
	inline float getNear() const { return  _near; }
	inline float getFar() const { return _far; }
	
protected:
	vwgl::Viewport _viewport;
	float _near;
	float _far;
};

class VWGLEXPORT PerspectiveProjectionMethod : public ProjectionFactoryMethod {
public:
	PerspectiveProjectionMethod() :ProjectionFactoryMethod(), _fov(45.0f) {}
	PerspectiveProjectionMethod(float fov) :ProjectionFactoryMethod(), _fov(fov) {}
	
	inline void setFov(float fov) { _fov = fov; }
	inline float getFov() const { return _fov; }
	
	virtual void updateMatrix() { if (valid()) getMatrix().perspective(_fov, getViewport().aspectRatio(), getNear(), getFar()); }

	virtual void serialize(JsonSerializer & serializer) {
		serializer.openObject(false, true);
		serializer.writeProp<std::string>("type", "PerspectiveProjectionMethod", true, true);
		serializer.writeProp("near", _near, true, true);
		serializer.writeProp("far", _far, true, true);
		serializer.writeProp("fov", _fov, false, true);
		serializer.closeObject(true, false, true);
	}
	
	virtual void deserialize(JsonDeserializer & deserializer);
	
protected:
	virtual ~PerspectiveProjectionMethod() {}
	
	float _fov;
};

class VWGLEXPORT OpticalProjectionMethod : public ProjectionFactoryMethod {
public:
	OpticalProjectionMethod(float focalLength = 50.0f, float frameSize = 35.0f)
		:ProjectionFactoryMethod(), _focalLength(focalLength), _frameSize(frameSize) {}
	
	inline void setFocalLengt(float fl) { _focalLength = fl; }
	inline void setFilmSize(float fs) { _frameSize = fs; }
	inline void setFrameSize(float fs) { _frameSize = fs; }
	inline float getFocalLength() const { return _focalLength; }
	inline float getFilmSize() const { return _frameSize; }
	inline float getFrameSize() const { return _frameSize; }
	
	virtual void updateMatrix() {
		if (valid()) {
			float fov = 2.0f * Math::atan(_frameSize / (_focalLength * 2.0f));
			getMatrix().perspective(vwgl::Math::radiansToDegrees(fov), getViewport().aspectRatio(), getNear(), getFar());
		}
	}
	
	virtual void serialize(JsonSerializer & serializer) {
		serializer.openObject(false, true);
		serializer.writeProp<std::string>("type", "OpticalProjectionMethod", true, true);
		serializer.writeProp("near", _near, true, true);
		serializer.writeProp("far", _far, true, true);
		serializer.writeProp("focalLength", _focalLength, true, true);
		serializer.writeProp("frameSize", _frameSize, false, true);
		serializer.closeObject(true, false, true);
	}
	
	virtual void deserialize(JsonDeserializer & deserializer);

protected:
	virtual ~OpticalProjectionMethod() {}

	float _focalLength;
	float _frameSize;
};

}

#endif
