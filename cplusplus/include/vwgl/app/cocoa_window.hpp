
#ifndef _VWGL_APP_COCOA_WINDOW_HPP_
#define _VWGL_APP_COCOA_WINDOW_HPP_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/app/window.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT CocoaWindow : public Window {
public:
	CocoaWindow() :Window(), _windowPtr(nullptr), _createWindow(false) {}
	
	virtual bool create();
	virtual void destroy();
	
	inline plain_ptr windowPtr() { return _windowPtr; }
	
	bool createCocoaWindow();
	
protected:
	virtual ~CocoaWindow() {}
	
	plain_ptr _windowPtr;
	bool _createWindow;
};

}
}
#endif
