#ifndef _VWGL_APP_WINDOW_CONTROLLER_HPP_
#define _VWGL_APP_WINDOW_CONTROLLER_HPP_

#include <vwgl/app/keyboard_event.hpp>
#include <vwgl/app/mouse_event.hpp>
#include <vwgl/app/window.hpp>

#include <ctime>

namespace vwgl {
namespace app {

class WindowController : public vwgl::ReferencedPointer {
public:

	virtual void initGL() {}
	virtual void reshape(int, int) {}
	virtual void draw() {}
	virtual void frame(float) {}
	virtual void destroy() {}
	virtual void keyUp(const KeyboardEvent &) {}
	virtual void keyDown(const KeyboardEvent &) {}
	virtual void mouseDown(const MouseEvent &) {}
	virtual void mouseDrag(const MouseEvent &) {}
	virtual void mouseMove(const MouseEvent &) {}
	virtual void mouseUp(const MouseEvent &) {}
	virtual void mouseWheel(const MouseEvent &) {}

	inline std::clock_t getLastClock() { return _lastClock; }
	inline void setLastClock(std::clock_t clock) { _lastClock = clock; }

	inline void setWindow(Window * win) { _window = win; }
	inline Window * window() { return _window; }

protected:
	virtual ~WindowController() {}
	std::clock_t _lastClock;

	Window * _window;
};

}
}

#endif
