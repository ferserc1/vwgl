
#ifndef _VWGL_APP_GL_CONTEXT_HPP_
#define _VWGL_APP_GL_CONTEXT_HPP_

#include <vwgl/app/context.hpp>

namespace vwgl {
namespace app {

class Window;

class GLContext : public Context {
public:
	GLContext(Window * window) :Context(window) {}

	virtual void makeCurrent() = 0;
};

}
}
#endif // _VWGL_APP_GL_CONTEXT_HPP_
