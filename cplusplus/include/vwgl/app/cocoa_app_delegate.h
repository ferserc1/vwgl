
#ifndef _VWGL_APP_COCOA_APP_DELEGATE_H_
#define _VWGL_APP_COCOA_APP_DELEGATE_H_

#include <vwgl/system.hpp>

#if VWGL_MAC==1

#include <Cocoa/Cocoa.h>

@interface CocoaAppDelegate : NSObject<NSApplicationDelegate>

@end

#endif

#endif
