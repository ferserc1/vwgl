#ifndef _VWGL_APP_KEYBOARD_HPP_
#define _VWGL_APP_KEYBOARD_HPP_

#include <vwgl/keyboard.hpp>

namespace vwgl {
namespace app {

class KeyboardEvent {
public:
	
	inline Keyboard & keyboard() { return _keyboard; }
	inline const Keyboard & keyboard() const { return _keyboard; }

protected:
	Keyboard _keyboard;
};

}
}

#endif
