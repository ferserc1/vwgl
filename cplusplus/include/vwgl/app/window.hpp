
#ifndef _VWGL_APP_WINDOW_HPP_
#define _VWGL_APP_WINDOW_HPP_

#include <string>

#include <vwgl/referenced_pointer.hpp>

#include <vwgl/app/gl_context.hpp>

#include <vwgl/vector.hpp>

namespace vwgl {
namespace app {

class WindowController;

class VWGLEXPORT Window : public ReferencedPointer {
public:
	static Window * newWindowInstance();

	Window();
	virtual ~Window();

	void setWindowController(WindowController * controller);

	inline void setTitle(const std::string & title) { _title = title; }
	inline void setRect(const Rect & rect) { _rect = rect; }
	inline void setPosition(int x, int y) { _rect.x(x); _rect.y(y); }
	inline void setSize(int w, int h) { _rect.width(w); _rect.height(h); }
	inline void setFullScreen(bool fs) { _fullscreen = fs; }
	inline Rect & getRect() { return _rect; }
	inline const Rect & getRect() const { return _rect; }

	inline WindowController * getWindowController() { return _controller.getPtr(); }

	virtual bool create() = 0;
	virtual void destroy() = 0;

	inline Context * context() { return _context.getPtr(); }
	inline GLContext * glContext() { return dynamic_cast<GLContext*>(_context.getPtr()); }

protected:
	std::string _title;
	Rect _rect;
	bool _fullscreen;

	ptr<Context> _context;
	ptr<WindowController> _controller;
};

}
}
#endif // _VWGL_APP_WINDOW_HPP_
