
#ifndef _VWGL_APP_GDL_WINDOW_HPP_
#define _VWGL_APP_GDL_WINDOW_HPP_

#include <vwgl/app/window.hpp>
#include <vwgl/app/gdl_context.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT GdlWindow : public Window {
	friend class GdlContext;
public:
  GdlWindow() :Window() {}

  virtual bool create();
  virtual void destroy();

  inline plain_ptr windowDataPtr() { return _windowData; }
  
protected:
  virtual ~GdlWindow() {}

  plain_ptr _windowData;
};

}
}

#endif
