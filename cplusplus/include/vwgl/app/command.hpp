
#ifndef _VWGL_APP_COMMAND_HPP_
#define _VWGL_APP_COMMAND_HPP_

#include <vwgl/referenced_pointer.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT Command : public ReferencedPointer {
public:
	Command(bool undoable = true);

	inline bool isUndoable() const { return _undoable; }

	virtual void doCommand() = 0;
	virtual void undoCommand() {}

protected:
	virtual ~Command();

	bool _undoable;
};

class VWGLEXPORT ContextCommand : public Command {
public:
	ContextCommand(plain_ptr ctx, bool undoable = true);

	inline plain_ptr getContextPtr() { return _context; }

protected:
	virtual ~ContextCommand();

	plain_ptr _context;
};

}
}

#endif
