
#ifndef _VWGL_APP_COCOAGLCONTEXT_HPP
#define _VWGL_APP_COCOAGLCONTEXT_HPP

#include <vwgl/app/gl_context.hpp>
#include <vwgl/app/window.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT CocoaGLContext : public GLContext {
public:
	CocoaGLContext(Window * window) :GLContext(window), _nativeContext(nullptr) {}
	
	virtual bool createContext();
	virtual void makeCurrent();
	virtual void swapBuffers();
	
	virtual void destroy();
	
	void setCocoaGLContext(plain_ptr ctx) { _nativeContext = ctx; }
	void * cocoaGLContext() { return _nativeContext; }
	
protected:
	plain_ptr _nativeContext;
};

}
}

#endif
