
#ifndef _VWGL_APP_INPUT_MEDIATOR_HPP_
#define _VWGL_APP_INPUT_MEDIATOR_HPP_

#include <vwgl/manipulation/gizmo_manager.hpp>
#include <vwgl/manipulation/mouse_picker.hpp>
#include <vwgl/manipulation/selection_handler.hpp>
#include <vwgl/app/command_manager.hpp>
#include <vwgl/scene/renderer.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/manipulation/rotate_gizmo.hpp>
#include <vwgl/manipulation/scale_gizmo.hpp>
#include <vwgl/manipulation/translate_gizmo.hpp>
#include <vwgl/app/mouse_event.hpp>
#include <vwgl/app/keyboard_event.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT InputMediator {
public:
	enum Gizmo {
		kGizmoNone,
		kGizmoTranslate,
		kGizmoRotate,
		kGizmoScale
	};

	InputMediator();

	
	inline void setRenderer(scene::Renderer * rend) {
		if (rend) {
			setSceneRoot(rend->getSceneRoot());
		}
		_renderer = rend;
	}
	inline scene::Renderer * getRenderer() { return _renderer.getPtr(); }

	inline manipulation::GizmoManager & gizmoManager() { return _gizmoManager; }
	inline manipulation::SelectionHandler & selectionHandler() { return _selectionHandler; }
	inline manipulation::MousePicker & mousePicker() { return _mousePicker; }
	inline app::CommandManager & commandManager() { return _commandManager; }

	void setGizmo(Gizmo g);
	Gizmo currentGizmo();

	void init();
	inline void resize(const Size2Di & size) { _viewport.set(0, 0, size.width(), size.height()); }
	void drawGizmos();

	void keyDown(const app::KeyboardEvent & evt);
	void keyUp(const app::KeyboardEvent & evt);
	void mouseDown(const app::MouseEvent & evt);
	void mouseUp(const app::MouseEvent & evt);
	void mouseMove(const app::MouseEvent & evt);
	void mouseDrag(const app::MouseEvent & evt);
	void mouseWheel(const app::MouseEvent & evt);

protected:
	manipulation::GizmoManager _gizmoManager;
	manipulation::SelectionHandler _selectionHandler;
	manipulation::MousePicker _mousePicker;	
	CommandManager _commandManager;
	ptr<scene::Renderer> _renderer;
	ptr<scene::Node> _sceneRoot;

	ptr<manipulation::RotateGizmo> _rotateGizmo;
	ptr<manipulation::ScaleGizmo> _scaleGizmo;
	ptr<manipulation::TranslateGizmo> _translateGizmo;

	Viewport _viewport;
	Position2Di _mouseDownCoords;

	inline void setSceneRoot(scene::Node * sr) {
		_sceneRoot = sr;
		_gizmoManager.setSceneRoot(sr);
		_mousePicker.setSceneRoot(sr);
	}
	inline scene::Node * getSceneRoot() { return _sceneRoot.getPtr(); }
};

}
}
#endif
