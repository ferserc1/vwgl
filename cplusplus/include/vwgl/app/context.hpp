#ifndef _VWGL_APP_CONTEXT_HPP_
#define _VWGL_APP_CONTEXT_HPP_

#include <vwgl/referenced_pointer.hpp>

namespace vwgl {
namespace app {

class Window;

class Context : public vwgl::ReferencedPointer {
public:
	Context(Window * window) :_window(window) {}

	virtual bool createContext() = 0;
	virtual void swapBuffers() = 0;

	virtual void destroy() = 0;

protected:
	Window * _window;
};

}
}

#endif