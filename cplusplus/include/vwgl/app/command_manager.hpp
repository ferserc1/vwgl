
#ifndef _VWGL_APP_COMMAND_MANAGER_HPP_
#define _VWGL_APP_COMMAND_MANAGER_HPP_

#include <vwgl/app/command.hpp>

#include <functional>
#include <list>
#include <vector>

namespace vwgl {
namespace app {

class ICommandManagerObserver {
public:
	/*
	 *	Called when a command is going to be executed
	 */
	virtual void willExecuteCommand(Command * ) {}
	/*
	 *	Called when a command is executed successfully
	 */
	virtual void didExecuteCommand(Command * ) {}
	/*
	 *	Called when a command execution is failed. Can be called from execute(), undo() and redo() functions
	 */
	virtual void commandDidFail(Command * ) {}
	/*
	 *	Called before undo a command
	 */
	virtual void willUndo(Command * ) {}
	/*
	 *	Called after undo a command
	 */
	virtual void didUndo(Command * ) {}
	/*
	 *	Called before redo a command
	 */
	virtual void willRedo(Command * ) {}
	/*
	 *	Called after redo a command
	 */
	virtual void didRedo(Command * ) {}
};

typedef std::vector<ICommandManagerObserver*> CommandManagerObserverVector;

class VWGLEXPORT CommandManager {
public:
	CommandManager();
	
	void registerObserver(ICommandManagerObserver * obs);
	void unregisterObserver(ICommandManagerObserver * obs);

	inline void setCurrentContextCallback(std::function<void(plain_ptr)> ctx) { _setCurrentContext = ctx; }

	void setCurrentContext(ContextCommand * command);

	void clearCommandStack();
	bool execute(Command * cmd);
	void undo();
	void redo();

	inline bool canUndo() const { return !_undoStack.empty(); }
	inline bool canRedo() const { return !_redoStack.empty(); }
	
	inline void setMaxUndos(int max) { _maxUndos = max; }
	inline int getMaxUndos() const { return _maxUndos; }
	
	inline void eachObserver(std::function<void (ICommandManagerObserver*)> closure) {
		CommandManagerObserverVector::iterator it;
		for (it=_observers.begin(); it!=_observers.end(); ++it) {
			closure(*it);
		}
	}

protected:
	std::function<void(plain_ptr)> _setCurrentContext;
	int _maxUndos;

	std::list<ptr<Command> > _undoStack;
	std::list<ptr<Command> > _redoStack;

	bool executeCommand(Command * cmd, bool doCommand);
	
	CommandManagerObserverVector _observers;
};

}
}

#endif
