#ifndef _VWGL_APP_DEFAULT_WINDOW_CONTROLLER_HPP_
#define _VWGL_APP_DEFAULT_WINDOW_CONTROLLER_HPP_

#include <vwgl/app/window_controller.hpp>

#include <vwgl/scene/renderer.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/app/input_mediator.hpp>


namespace vwgl {
namespace app {

class VWGLEXPORT DefaultWindowController : public WindowController {
public:
	enum Quality {
		kQualityLow = 1,
		kQualityMedium,
		kQualityHigh,
		kQualityExtreme
	};

	DefaultWindowController(Quality q = Quality::kQualityHigh, scene::Renderer::RenderPath renderPath = scene::Renderer::kRenderPathDeferred);

	virtual void initGL();
	virtual void reshape(int w, int h);
	virtual void draw();
	virtual void frame(float);
	virtual void destroy();
	virtual void keyUp(const KeyboardEvent & evt);
	virtual void keyDown(const KeyboardEvent & evt);
	virtual void mouseDown(const MouseEvent & evt);
	virtual void mouseDrag(const MouseEvent & evt);
	virtual void mouseMove(const MouseEvent & evt);
	virtual void mouseUp(const MouseEvent & evt);
	virtual void mouseWheel(const MouseEvent & evt);
	
	inline scene::Renderer * renderer() { return _renderer.getPtr(); }
	inline scene::Node * sceneRoot() { return _sceneRoot.getPtr(); }

	inline void setQuality(Quality q) { _quality = q; configureQuality(); }
	inline Quality getQuality() const { return _quality;  }

protected:
	virtual ~DefaultWindowController();

	ptr<scene::Renderer> _renderer;
	ptr<scene::Node> _sceneRoot;
	app::InputMediator _inputMediator;
	scene::Renderer::RenderPath _renderPath;
	Quality _quality;

	virtual void loadReaders();
	virtual void loadWritters();
	virtual scene::Node * createScene();
	virtual void initScene(scene::Node * sceneRoot);
	virtual void createRenderer(scene::Node * sceneRoot);
	virtual void configureRenderer(scene::Renderer * renderer);

	virtual void configureQuality();

};

}
}

#endif