
#ifndef _VWGL_APP_GLVIEW_HPP_
#define _VWGL_APP_GLVIEW_HPP_

#include <vwgl/system.hpp>
#include <vwgl/app/cocoa_window.hpp>

#if VWGL_MAC==1

#import <OpenGL/OpenGL.h>
#import <Cocoa/Cocoa.h>

@interface CocoaView : NSOpenGLView

@property (readwrite) vwgl::app::CocoaWindow * cocoaWindow;

@end

#endif

#endif
