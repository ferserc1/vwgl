
#ifndef _VWGL_APP_MAIN_LOOP_HPP_
#define _VWGL_APP_MAIN_LOOP_HPP_

#include <vwgl/app/window.hpp>
#include <vwgl/mouse.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT MainLoop : Singleton {
public:
	static MainLoop * s_singleton;
	static MainLoop * get() {
		if (s_singleton == nullptr) {
			s_singleton = new MainLoop();
		}
		return s_singleton;
	}
	static void destroy() {
		if (s_singleton) {
			delete s_singleton;
		}
	}
	
	virtual void finalize();

	inline void setWindow(Window * wnd) { _window = wnd; }
	inline Window * getWindow() { return _window.getPtr(); }

	int run();

	void quit(int code);

	inline const Mouse & getMouse() const { return _mouseStatus; }
	inline Mouse & getMouse() { return _mouseStatus; }

protected:
	MainLoop() :_window(nullptr) {}
	virtual ~MainLoop() {}

	ptr<Window> _window;
	Mouse _mouseStatus;
};

}
}

#endif
