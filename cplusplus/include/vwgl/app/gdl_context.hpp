
#ifndef _VWGL_APP_GDLGLCONTEXT_HPP
#define _VWGL_APP_GDLGLCONTEXT_HPP

#include <vwgl/app/gl_context.hpp>
#include <vwgl/app/window.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT GdlGLContext : public GLContext {
public:
  GdlGLContext(Window * window) :GLContext(window), _nativeContext(nullptr) {}

  virtual bool createContext();
  virtual void makeCurrent();
  virtual void swapBuffers();

  virtual void destroy();

protected:
  plain_ptr _nativeContext;
};

}
}

#endif
