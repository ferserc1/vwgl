
#ifndef _VWGL_APP_WIN32GLCONTEXT_HPP
#define _VWGL_APP_WIN32GLCONTEXT_HPP

#include <vwgl/app/gl_context.hpp>
#include <vwgl/app/window.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT Win32GLContext : public GLContext {
public:
	Win32GLContext(Window * window) :GLContext(window), _nativeContext(nullptr) {}

	virtual bool createContext();
	virtual void makeCurrent();
	virtual void swapBuffers();

	virtual void destroy();

	void * win_GLContext() { return _nativeContext; }

protected:
	plain_ptr _nativeContext;
};

}
}

#endif
