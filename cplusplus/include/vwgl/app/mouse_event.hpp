#ifndef _VWGL_APP_MOUSE_HPP_
#define _VWGL_APP_MOUSE_HPP_

#include <vwgl/mouse.hpp>
#include <vwgl/keyboard.hpp>
#include <vwgl/vector.hpp>

namespace vwgl {
namespace app {

class MouseEvent {
public:
    inline Mouse & mouse() { return _mouse; }
    inline const Mouse & mouse() const { return _mouse; }
    inline Keyboard & keyboard() { return _keyboard; }
    inline const Keyboard & keyboard() const { return _keyboard; }
    inline const Vector2i & pos() const { return _pos; }
    inline void setPos(const Vector2i & pos) { _pos = pos; }
    inline const Vector2 & delta() const { return _delta; }
    inline void setDelta(const Vector2 & delta) { _delta = delta; }

protected:
    Mouse _mouse;
    Keyboard _keyboard;
    Vector2i _pos;
    Vector2 _delta;
};

}
}

#endif
