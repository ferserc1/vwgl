
#ifndef _VWGL_APP_DIRECTX_CONTEXT_HPP_
#define _VWGL_APP_DIRECTX_CONTEXT_HPP_

#include <vwgl/app/context.hpp>

namespace vwgl {
namespace app {

class Window;

class VWGLEXPORT DirectXContext : public Context {
public:
	DirectXContext(Window * window) :Context(window) {}

	virtual bool createContext();
	virtual void swapBuffers();

	virtual void destroy();

	plain_ptr getSwapChain() { return _swapChain; }
	plain_ptr getDevice() { return _device; }
	plain_ptr getDeviceContext() { return _deviceContext; }
	plain_ptr getRenderTargetView() { return _renderTargetView; }
	plain_ptr getDepthStencilView() { return _depthStencilView; }
	plain_ptr getDepthStencilBuffer() { return _depthStencilBuffer; }

protected:
	plain_ptr _swapChain;
	plain_ptr _device;
	plain_ptr _deviceContext;
	plain_ptr _renderTargetView;
	plain_ptr _depthStencilView;
	plain_ptr _depthStencilBuffer;
};

}
}
#endif // _VWGL_APP_GL_CONTEXT_HPP_
