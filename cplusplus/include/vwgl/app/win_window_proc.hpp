
#ifndef _VWGL_APP_WIN_WINDOW_PROC_HPP_

#include <vwgl/system.hpp>

#if VWGL_WINDOWS==1
#include <windows.h>

extern LRESULT CALLBACK vwgl_app_WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

#endif  // VWGL_WINDOWS==1

#endif // _VWGL_APP_WIN_WINDOW_PROC_HPP_
