
#ifndef _VWGL_APP_WIN32_WINDOW_HPP_
#define _VWGL_APP_WIN32_WINDOW_HPP_

#include <vwgl/app/window.hpp>

namespace vwgl {
namespace app {

class VWGLEXPORT Win32Window : public Window {
public:
	Win32Window() :Window(), _hDC(0), _hWnd(0) {}

	virtual bool create();
	virtual void destroy();

	inline void * win_DeviceContext() { return _hDC; }
	inline void * win_Wnd() { return _hWnd; }
	inline bool mouseLeaveSent() const { return _mouseLeaveSent; }
	inline void setMouseLeaveSent(bool mouseLeaveSent) { _mouseLeaveSent = mouseLeaveSent; }

protected:
	virtual ~Win32Window() {}

	plain_ptr _hDC;
	plain_ptr _hWnd;
	plain_ptr _hInstance;
	bool _mouseLeaveSent;
};

}
}

#endif
