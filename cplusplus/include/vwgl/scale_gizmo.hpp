//
//  scale_gizmo.hpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 28/03/14.
//  Copyright (c) 2014 Vitaminew. All rights reserved.
//

#ifndef vwgl_scale_gizmo_hpp
#define vwgl_scale_gizmo_hpp

#include <vwgl/gizmo.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/physics/plane.hpp>

#include <iostream>

namespace vwgl {
	
class VWGLEXPORT ScaleGizmo : public Gizmo {
public:
	ScaleGizmo();
	ScaleGizmo(const std::string & modelPath);
	
	virtual bool checkItemPicked(Solid * solid) {
		_mode = getMode(solid->getPickId());
		return _mode!=ScaleGizmo::kGizmoNone;
	}
	
	virtual void beginMouseEvents(const Position2Di & pos, vwgl::Camera * camera, const Viewport & vp, const Vector3 & gizmoPos, TransformNode * trx);
	virtual void mouseEvent(const vwgl::Position2Di & pos);
	virtual void endMouseEvents();
	
protected:
	virtual ~ScaleGizmo();
	
	virtual void loadPickIds();
	
	PickId _xAxisId;
	PickId _yAxisId;
	PickId _zAxisId;
	PickId _allAxisId;
	
	enum Mode {
		kGizmoNone		= 0,
		kGizmoAxisX 	= 1,
		kGizmoAxisY 	= 2,
		kGizmoAxisZ		= 3,
		kGizmoAllAxis	= 4
	};
	
	Mode _mode;
	TransformNode * _transformNode;
	Camera * _camera;
	
	vwgl::Position2Di _previousPosition;
	
	Mode getMode(PickId pickedId) {
		if (pickedId==_xAxisId) {
			return ScaleGizmo::kGizmoAxisX;
		}
		else if (pickedId==_yAxisId) {
			return ScaleGizmo::kGizmoAxisY;
		}
		else if (pickedId==_zAxisId) {
			return ScaleGizmo::kGizmoAxisZ;
		}
		else if (pickedId==_allAxisId) {
			return ScaleGizmo::kGizmoAllAxis;
		}
		return ScaleGizmo::kGizmoNone;
	}
};
	
}


#endif
