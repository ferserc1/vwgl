
#ifndef _vwgl_node_manipulator_hpp_
#define _vwgl_node_manipulator_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/transform_node.hpp>

#include <float.h>

namespace vwgl {

class NodeManipulator: public ReferencedPointer {
public:
	NodeManipulator(TransformNode * node) :_transformNode(node) {}
	
	virtual void setTransform() = 0;
	
	inline void setTransformNode(vwgl::TransformNode * trx) { if (trx) _transformNode = trx; }
	inline TransformNode * getTransformNode() { return _transformNode.getPtr(); }
	inline const TransformNode * getTransformNode() const { return _transformNode.getPtr(); }
	
protected:
	virtual ~NodeManipulator() {}
	
	ptr<TransformNode> _transformNode;
};


class TargetNodeManipulator :public vwgl::NodeManipulator {
public:
	TargetNodeManipulator(vwgl::TransformNode * node)
		:NodeManipulator(node),
		_yaw(0.0f),
		_pitch(0.0f),
		_distance(10.0f),
		_center(0.0f),
		_minDistance(0.1f),
		_maxDistance(50.0f),
		_minPitch(-vwgl::Math::kPi/2.0f),
		_maxPitch(0.0f)
	{
		
	}
	
	void setTransform() {
		if (_yaw>vwgl::Math::kPi*2.0f) _yaw = _yaw - vwgl::Math::kPi*2.0f;
		if (_yaw<0.0f) _yaw = vwgl::Math::kPi * 2.0f - _yaw;
		
		if (_pitch<_minPitch) _pitch = _minPitch;
		if (_pitch>_maxPitch) _pitch = _maxPitch;
		if (_distance<_minDistance) _distance = _minDistance;
		if (_distance>_maxDistance) _distance = _maxDistance;
		TransformNode * node = getTransformNode();
		node->getTransform().identity();
		node->getTransform().translate(_center);
		node->getTransform().rotate(_yaw, 0.0f, 1.0f, 0.0f);
		node->getTransform().rotate(_pitch, 1.0f, 0.0f, 0.0f);
		node->getTransform().translate(0.0f, 0.0f, _distance);
	}
	
	float & yaw() { return _yaw; }
	const float & yaw() const { return _yaw; }
	float & pitch() { return _pitch; }
	const float & pitch() const { return _pitch; }
	float & distance() { return _distance; }
	const float & distance() const { return _distance; }
	vwgl::Vector3 & center() { return _center; }
	const vwgl::Vector3 & center() const { return _center; }
	vwgl::Bounds & centerBounds() { return _centerBounds; }
	const vwgl::Bounds & centerBounds() const { return _centerBounds; }
	float & maxDistance() { return _maxDistance; }
	const float & maxDistance() const { return _maxDistance; }
	float & minDistance() { return _minDistance; }
	const float & minDistance() const { return _minDistance; }
	
	void setYaw(float yaw) { _yaw = yaw; }
	void setPitch(float pitch) { _pitch = pitch; }
	void setDistance(float distance) { _distance = distance; }
	void setCenter(const vwgl::Vector3 & center) { _center = center; }
	void setCenterBounds(const vwgl::Bounds & bounds) { _centerBounds = bounds; }
	void setMaxDistance(float maxDistance) { _maxDistance = maxDistance; }
	void setMinDistance(float minDistance) { _minDistance = minDistance; }
	void setMaxPitch(float max) { _maxPitch = max; }
	void setMinPitch(float min) { _minPitch = min; }

	
	void moveForward(float increment) {
		vwgl::Vector3 fw = getTransformNode()->getTransform().forwardVector().scale(increment);
		vwgl::Vector3 newCenter = _center + fw;
		if (_centerBounds.isInBounds(newCenter)) {
			_center = newCenter;
		}
		setTransform();
	}
	
	void moveBackward(float increment) {
		vwgl::Vector3 bw = getTransformNode()->getTransform().backwardVector().scale(increment);
		vwgl::Vector3 newCenter = _center + bw;
		if (_centerBounds.isInBounds(newCenter)) {
			_center = newCenter;
		}
		setTransform();
	}
	
	void moveLeft(float increment) {
		vwgl::Vector3 left = getTransformNode()->getTransform().leftVector().scale(increment);
		vwgl::Vector3 newCenter = _center + left;
		if (_centerBounds.isInBounds(newCenter)) {
			_center = newCenter;
		}
		setTransform();
	}
	
	void moveRight(float increment) {
		vwgl::Vector3 right = getTransformNode()->getTransform().rightVector().scale(increment);
		vwgl::Vector3 newCenter = _center + right;
		if (_centerBounds.isInBounds(newCenter)) {
			_center = newCenter;
		}
		setTransform();
	}
	
protected:
	virtual ~TargetNodeManipulator() {
		
	}
	
	float _yaw;
	float _pitch;
	float _distance;
	vwgl::Vector3 _center;
	vwgl::Bounds _centerBounds;
	float _minDistance;
	float _maxDistance;
	float _minPitch;
	float _maxPitch;
};

class MouseTargetManipulator : public vwgl::TargetNodeManipulator {
public:
	enum ManipulationType {
		kManipulationRotate,
		kManipulationDrag,
		kManipulationZoom
	};
	MouseTargetManipulator(vwgl::TransformNode * node) :vwgl::TargetNodeManipulator(node), _scrollVelocity(0.1f) {}
	
	void mouseDown(const vwgl::Position2Di & mousePos, ManipulationType type) {
		_lastCoords = mousePos;
		_type = type;
	}
	
	void mouseMove(const vwgl::Position2Di & mousePos) {
		if (_type==kManipulationRotate) {
			yaw() += (_lastCoords.x() - mousePos.x()) * 0.01f;
			pitch() += (_lastCoords.y() - mousePos.y()) * 0.01f;
		}
		else if (_type==kManipulationDrag) {
			vwgl::Vector3 center = this->center();
			if (pitch()<-vwgl::Math::kPi/4.0) {
				center.x(center.x() + (_lastCoords.x() - mousePos.x()) * 0.01f * vwgl::Math::cos(yaw()) +
						 (_lastCoords.y() - mousePos.y()) * 0.01f * vwgl::Math::sin(yaw()));
				center.z(center.z() - (_lastCoords.x() - mousePos.x()) * 0.01f * vwgl::Math::sin(yaw()) +
						 (_lastCoords.y() - mousePos.y()) * 0.01f * vwgl::Math::cos(yaw()));
			}
			else {
				center.x(center.x() + (_lastCoords.x() - mousePos.x()) * 0.01f * vwgl::Math::cos(yaw()));
				center.z(center.z() - (_lastCoords.x() - mousePos.x()) * 0.01f * vwgl::Math::sin(yaw()));
				center.y(center.y() - (_lastCoords.y() - mousePos.y()) * 0.01f);
			}
			if (_centerBounds.isInBounds(center)) {
				_center = center;
			}
		}
		else if (_type==kManipulationZoom) {
			distance() += (_lastCoords.y() - mousePos.y()) * 0.1f;
		}
		setTransform();
		_lastCoords = mousePos;
	}
	
	void mouseWheel(const vwgl::Vector2 delta) {
		distance() += delta.y() * _scrollVelocity;
		setTransform();
	}
	
	void beginZoom(float scale) {
		_initZoomDistance = distance();
		distance() /= scale;
	}

	void zoom(float scale) {
		distance() = _initZoomDistance / scale;
		setTransform();
	}
	
protected:
	virtual ~MouseTargetManipulator() {}
	
	vwgl::Position2Di _lastCoords;
	ManipulationType _type;
	float _scrollVelocity;
	float _initZoomDistance;
};

}
#endif
