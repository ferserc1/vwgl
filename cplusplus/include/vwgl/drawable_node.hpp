#ifndef _vwgl_drawable_node_hpp_
#define _vwgl_drawable_node_hpp_

#include <vwgl/node.hpp>
#include <vwgl/group.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/drawable_node.hpp>

namespace vwgl {

class DrawableNode : public Node {
public:
	DrawableNode() {}
	DrawableNode(DrawableBase * drawable) {
		setDrawable(drawable);
	}
	
	void setDrawable(DrawableBase * drawable) {
		if (drawable && drawable->_parent==this) return;
		if (drawable && drawable->_parent) drawable->_parent->setDrawable(NULL);
		if (_drawable.valid()) _drawable->_parent = NULL;
		_drawable = drawable;
		if (_drawable.valid()) _drawable->_parent = this;
	}
	DrawableBase * getDrawable() { return _drawable.getPtr(); }
	const DrawableBase * getDrawable() const { return _drawable.getPtr(); }

	virtual void init() { if (_drawable.valid()) _drawable->init(); }

protected:
	virtual ~DrawableNode() {}
	
	ptr<DrawableBase> _drawable;
};

class FindDrawableVisitor : public NodeVisitor {
public:
	FindDrawableVisitor(int identifier) :_identifier(identifier), _result(nullptr) {}
	
	Drawable * getResult() { return _result; }

	virtual void visit(Node * node) {
		DrawableNode * drwNode = dynamic_cast<DrawableNode*>(node);
		Group * grp = dynamic_cast<Group*>(node);
		
		if (drwNode) {
			Drawable * drw = dynamic_cast<Drawable*>(drwNode->getDrawable());
			if (drw && drw->getIdentifier()==_identifier) {
				_result = drw;
			}
		}
		if (!_result && grp) {
			NodeList::iterator it;
			for (it=grp->getNodeList().begin(); it!=grp->getNodeList().end() && !_result; ++it) {
				visit((*it).getPtr());
			}
		}
	}

protected:
	int _identifier;
	vwgl::Drawable * _result;
};
	
class FindDrawableProxyVisitor : public NodeVisitor {
public:
	
	inline std::vector<DrawableProxy*> & getResult() { return _result; }
	
	virtual void visit(Node * node) {
		DrawableNode * drwNode = dynamic_cast<DrawableNode*>(node);
		Group * grp = dynamic_cast<Group*>(node);
		
		if (drwNode) {
			DrawableProxy * proxy = dynamic_cast<DrawableProxy*>(drwNode->getDrawable());
			if (proxy) {
				_result.push_back(proxy);
			}
		}
		if (grp) {
			NodeList::iterator it;
			for (it=grp->getNodeList().begin(); it!=grp->getNodeList().end(); ++it) {
				visit((*it).getPtr());
			}
		}
	}

protected:
	std::vector<DrawableProxy*> _result;
};

}

#endif
