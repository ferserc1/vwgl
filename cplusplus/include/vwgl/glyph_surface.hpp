
#ifndef _vwgl_glyph_surface_hpp_
#define _vwgl_glyph_surface_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/glyph.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/texture_manager.hpp>

#include <unordered_map>

namespace vwgl {

class VWGLEXPORT GlyphSurface : public ReferencedPointer {
public:
	GlyphSurface(const Glyph & g);
	
	inline const Glyph & getGlyph() const { return _gliph; }
	inline const GenericMaterial * getMaterial() const { return _mat.getPtr(); }
	inline GenericMaterial * getMaterial() { return _mat.getPtr(); }
	inline const PolyList * getPolyList() const { return _plist.getPtr(); }
	inline PolyList * getPolyList() { return _plist.getPtr(); }
	
	static float scale() { return s_scale; }
	
protected:
	virtual ~GlyphSurface();
	
	Glyph _gliph;
	ptr<PolyList> _plist;
	ptr<GenericMaterial> _mat;
	static float s_scale;
	
	void build();
};

typedef std::unordered_map<char, ptr<GlyphSurface> > GlyphSurfaceMap;
	
}

#endif
