
#ifndef _vwgl_render_pass_hpp_
#define _vwgl_render_pass_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/render_pass.hpp>
#include <vwgl/draw_node_visitor.hpp>
#include <vwgl/material.hpp>
#include <vwgl/fbo.hpp>
#include <vwgl/group.hpp>
#include <vwgl/scenerenderer.hpp>

namespace vwgl {

class VWGLEXPORT RenderPass : public ReferencedPointer {
public:
	RenderPass();
	RenderPass(Group * sceneRoot, Material * mat, FramebufferObject * fbo);

	void setSceneRoot(Group * sceneRoot);
	
	void setClearColor(const Vector4 & color) { _clearColor = color; }
	Vector4 & getClearColor() { return _clearColor; }
	const Vector4 & getClearColor() const { return _clearColor; }
	
	// CAUTION: sceneRoot is a weak reference, do not destroy before this RenderPass
	void create(Group * sceneRoot, Material * mat, FramebufferObject * fbo);
	void create(Material * mat, FramebufferObject * fbo);

	void clear();
	
	void updateTexture(bool alphaRender = false);

	void destroy();

	FramebufferObject * getFbo() { return _fbo.getPtr(); }
	void setFbo(FramebufferObject * fbo) { _fbo = fbo; }

	void setMaterial(vwgl::Material * mat);
	vwgl::Material * getMaterial() { return _renderer->getNodeVisitor()->getMaterial(); }
	
	inline void setClearBuffers(bool clear) { _clearBuffers = clear; }
	inline bool getClearBuffers() const{ return _clearBuffers; }
	
	inline void setRenderTransparentObjects(bool r) { _renderTransparentObjects = r; }
	inline bool getRenderTransparentObjects() const { return _renderTransparentObjects; }
	
	inline void setRenderOpaqueObjects(bool r) { _renderer->setRenderOpaque(r); }
	inline bool getRenderOpaqueObjects() const { return _renderer->getRenderOpaque(); }

protected:
	virtual ~RenderPass();
	
	Group * _sceneRoot;
	ptr<FramebufferObject> _fbo;
	ptr<SceneRenderer> _renderer;
	Vector4 _clearColor;
	bool _clearBuffers;
	bool _renderTransparentObjects;
};

}

#endif
