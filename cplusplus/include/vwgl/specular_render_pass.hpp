#ifndef _vwgl_specular_render_pass_hpp_
#define _vwgl_specular_render_pass_hpp_

#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class SpecularRenderPassMaterial : public DeferredMaterial {
public:
	SpecularRenderPassMaterial();
				
	virtual void initShader();

protected:
	virtual ~SpecularRenderPassMaterial();
		
	virtual void setupUniforms();
};

}

#endif
