
#ifndef _vwgl_basicphongmat_hpp_
#define _vwgl_basicphongmat_hpp_

#include <vwgl/export.hpp>

#include <vwgl/material.hpp>
#include <vwgl/texture.hpp>

namespace vwgl {


	
class VWGLEXPORT BasicPhongMaterial : public Material {
public:
	BasicPhongMaterial() :Material(), _ambient(0.2f), _diffuse(1.0), _lightDirection(0.0f,0.0f,-1.0f) { initShader(); }
	
	virtual void initShader();
	
	vwgl::Vector4 & ambient() { return _ambient; }
	vwgl::Vector4 & diffuse() { return _diffuse; }
	Texture * texture() { return _texture.getPtr(); }
	void setAmbient(const vwgl::Vector4 & ambient) { _ambient = ambient; }
	void setDiffuse(const vwgl::Vector4 & diffuse) { _diffuse = diffuse; }
	void setTexture(Texture * tex) { _texture = tex; }
	
	void setLightDirection(const vwgl::Vector3 & dir) {
		_lightDirection = dir;
		_lightDirection.normalize();
	}
	
protected:
	virtual void setupUniforms();
	
	virtual ~BasicPhongMaterial() {}
	
	vwgl::Vector4 _ambient;
	vwgl::Vector4 _diffuse;
	vwgl::Vector3 _lightDirection;
	ptr<vwgl::Texture> _texture;
	
	static std::string _vshader;
	static std::string _fshader;
};


}

#endif
