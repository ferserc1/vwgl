
#ifndef _vwgl_initvisitor_hpp_
#define _vwgl_initvisitor_hpp_

#include <vwgl/node_visitor.hpp>

namespace vwgl {

class InitVisitor : public NodeVisitor {
public:
	InitVisitor();

	virtual void visit(Node * node);

protected:
	virtual ~InitVisitor();
	
	std::vector<Node *> _initNodes;
	
	void doVisit(Node * node);
	
	void init();
};

}

#endif
