//
//  mathconstants.hpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 08/11/13.
//  Copyright (c) 2013 Vitaminew. All rights reserved.
//

#ifndef vwgl_mathutils_hpp
#define vwgl_mathutils_hpp

#include <vwgl/export.hpp>

#define _USE_MATH_DEFINES
#ifdef _WIN32
#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif
#else
#include <math.h>
#endif

namespace vwgl {

class VWGLEXPORT Math {
public:
	static float kPi;
	static float kPiOver2;
	static float kPiOver4;
	static float k2Pi;
	static float kPiOver180;
	static float k180OverPi;
	
	static float degreesToRadians(float d);
	
	static float radiansToDegrees(float r);
	
	static float sin(float f);
	static float cos(float f);
	static float tan(float f);
	
	static float asin(float f);
	static float acos(float f);
	
	static float cotan(float i);
	
	static float atan(float i);
	
	static float atan2(float i, float j);
	
	static float sqrt(float v);
	
	static short max(short a,short b) { return a>b ? a:b; }
	static unsigned short max(unsigned short a,unsigned short b) { return a>b ? a:b; }
	static int max(int a,int b) { return a>b ? a:b; }
	static unsigned int max(unsigned int a,unsigned int b) { return a>b ? a:b; }
	static long max(long a,long b) { return a>b ? a:b; }
	static unsigned long max(unsigned long a,unsigned long b) { return a>b ? a:b; }
	static long long max(long long a,long long b) { return a>b ? a:b; }
	static unsigned long long max(unsigned long long a,unsigned long long b) { return a>b ? a:b; }
	static float max(float a,float b) { return a>b ? a:b; }
	static double max(double a,double b) { return a>b ? a:b; }
	
	static short min(short a,short b) { return a>b ? b:a; }
	static unsigned short min(unsigned short a,unsigned short b) { return a>b ? b:a; }
	static int min(int a,int b) { return a>b ? b:a; }
	static unsigned int min(unsigned int a,unsigned int b) { return a>b ? b:a; }
	static long min(long a,long b) { return a>b ? b:a; }
	static unsigned long min(unsigned long a,unsigned long b) { return a>b ? b:a; }
	static long long min(long long a,long long b) { return a>b ? b:a; }
	static unsigned long long min(unsigned long long a,unsigned long long b) { return a>b ? b:a; }
	static float min(float a,float b) { return a>b ? b:a; }
	static double min(double a,double b) { return a>b ? b:a; }
	
	static short abs(short a) { return a<0 ? -a:a; }
	static int abs(int a) { return a<0 ? -a:a; }
	static long abs(long a) { return a<0L ? -a:a; }
	static long long abs(long long a) { return a<0 ? -a:a; }
	static float abs(float a) { return a<0.0f ? -a:a; }
	static double abs(double a) { return a<0.0 ? -a:a; }
	
	static float random();
	
	static float lerp(float from, float to, float t);
	
	static short minValue(short val);
	static short maxValue(short val);
	static int minValue(int val);
	static int maxValue(int val);
	static long minValue(long val);
	static long maxValue(long val);
	static long long minValue(long long val);
	static long long maxValue(long long val);
	static unsigned short minValue(unsigned short val);
	static unsigned short maxValue(unsigned short val);
	static unsigned int minValue(unsigned int val);
	static unsigned int maxValue(unsigned int val);
	static unsigned long minValue(unsigned long val);
	static unsigned long maxValue(unsigned long val);
	static unsigned long long minValue(unsigned long long val);
	static unsigned long long maxValue(unsigned long long val);
	static float minValue(float val);
	static float maxValue(float val);
	static double minValue(double val);
	static double maxValue(double val);
	
	static bool isPowerOfTwo(int x) { return (x!=0 && (x & (~x + 1))==x); }
	
	static int nextPowerOfTwo(int x);
};

}

#endif
