
#ifndef _vwgl_text_font_hpp_
#define _vwgl_text_font_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/glyph_surface.hpp>
#include <vwgl/system.hpp>

#include <string>
#include <functional>

namespace vwgl {

class VWGLEXPORT TextFont : public ReferencedPointer {
public:
	enum FontType {
		kTypeSerif			= 0x1 << 0,
		kTypeSansSerif		= 0x1 << 1,
		kTypeMonospace		= 0x1 << 2
	};
	
	TextFont(const std::string & fontName, int size, int res, const std::string & path = "");
	
	inline const std::string & getFontName() const { return _fontName; }
	inline int getSize() const { return _size; }
	inline int getResolution() const { return _res; }
	inline const std::string & getPath() const { return _path; }
	
	inline GlyphSurface * get(char c) { return _glyphMap[c].getPtr(); }
	inline void add(char c, GlyphSurface * g) { _glyphMap[c] = g; }
	
	inline void setSpaceSize(float size) { _spaceSize = size; }
	inline void setLineHeight(float lh) { _lineHeight = lh; }
	
	inline float getSpaceSize() const { return _spaceSize; }
	inline float getTabSize() const { return _spaceSize * 4.0f; }
	inline float getLineHeight() const { return _lineHeight; }
	
	static std::string getDefaultFontPath(unsigned int type = kTypeSansSerif);
	
	GenericMaterial * getMaterial() {
		if (_glyphMap.find('a')!=_glyphMap.end()) {
			return _glyphMap['a']->getMaterial();
		}
		return nullptr;
	}

	void eachGlyph(std::function<void(GlyphSurface*)> cb) {
		GlyphSurfaceMap::iterator it;
		for (it=_glyphMap.begin(); it!=_glyphMap.end(); ++it) {
			cb(it->second.getPtr());
		}
	}
	
	void setColor(const Color & c) {
		eachGlyph([&](GlyphSurface * glyph) {
			glyph->getMaterial()->setDiffuse(c);
		});
	}
	
	void setSpecular(const Color & c) {
		eachGlyph([&](GlyphSurface * glyph) {
			glyph->getMaterial()->setSpecular(c);
		});
	}
	
	void setShininess(float s) {
		eachGlyph([&](GlyphSurface * glyph) {
			glyph->getMaterial()->setShininess(s);
		});
	}
	
	void setLighEmission(float le) {
		eachGlyph([&](GlyphSurface * glyph) {
			glyph->getMaterial()->setLightEmission(le);
		});
	}
	
	void setCastShadows(bool s) {
		eachGlyph([&](GlyphSurface * glyph) {
			glyph->getMaterial()->setCastShadows(s);
		});
	}
	
	void setReceiveShadows(bool s) {
		eachGlyph([&](GlyphSurface * glyph) {
			glyph->getMaterial()->setReceiveShadows(s);
		});
	}
	
	
protected:
	virtual ~TextFont();
	
	std::string _fontName;
	int _size;
	int _res;
	
	GlyphSurfaceMap _glyphMap;
	float _spaceSize;
	float _lineHeight;
	std::string _path;
};

}

#endif
