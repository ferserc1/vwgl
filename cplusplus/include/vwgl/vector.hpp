
#ifndef _vwgl_vector_hpp_
#define _vwgl_vector_hpp_

#include <vwgl/export.hpp>

#include <vwgl/mathutils.hpp>

#include <string>
#include <sstream>

namespace vwgl {

template <class T>
class Vector2Generic {
public:
	Vector2Generic<T>() { _v[0] = static_cast<T>(0); _v[1] = static_cast<T>(0); }
	Vector2Generic<T>(T s) { _v[0] = s; _v[1] = s; }
	Vector2Generic<T>(T x, T y) { _v[0] = x; _v[1] = y; }
	Vector2Generic<T>(const T * v) { _v[0] = v[0]; _v[1] = v[1]; }
	Vector2Generic<T>(const Vector2Generic<T> & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; }

	static Vector2Generic lerp(const Vector2Generic<T> & from, const Vector2Generic<T> & to, float delta) {
		return Vector2Generic<T>(vwgl::Math::lerp(from._v[0],to._v[0],delta),
								 vwgl::Math::lerp(from._v[1],to._v[1],delta));
	}
	
	inline T * raw() { return _v; }
	inline const T * raw() const { return _v; }
	inline int length() const { return 2; }

	inline float distance(const Vector2Generic<T> & v) {
		Vector2Generic<T> v3(_v[0] - v._v[0],
							 _v[1] - v._v[1]);
		return v3.magnitude();
	}

	inline float magnitude() { return sqrtf(static_cast<float>(_v[0]*_v[0] + _v[1]*_v[1])); }
	inline Vector2Generic<T> & normalize() {
		T m = magnitude();
		_v[0]=_v[0]/m; _v[1]=_v[1]/m;
		return *this;
	}
	inline Vector2Generic<T> & add(const Vector2Generic<T> & v2) {
		_v[0] += v2._v[0];
		_v[1] += v2._v[1];
		return *this;
	}
	inline Vector2Generic<T> & sub(const Vector2Generic<T> & v2) {
		_v[0] -= v2._v[0];
		_v[1] -= v2._v[1];
		return *this;
	}
	inline T dot(const Vector2Generic<T> & v2) {
		return _v[0] * v2._v[0] + _v[1] * v2._v[1];
	}
	inline Vector2Generic<T> & scale(T scale) {
		_v[0] *= scale; _v[1] *= scale;
		return *this;
	}

	inline T operator[](int i) const { return _v[i]; }
	inline bool operator==(const Vector2Generic<T> & v) const { return _v[0]==v._v[0] && _v[1]==v._v[1]; }
	inline bool operator!=(const Vector2Generic<T> & v) const { return _v[0]!=v._v[0] || _v[1]!=v._v[1]; }
	inline void operator=(const Vector2Generic & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; }
	inline void operator=(const T * v) { _v[0] = v[0]; _v[1] = v[1]; }

	inline std::string toString() const {
		std::stringstream ss;
		ss	<< '[' << _v[0] << "," << _v[1] << ']';
		return ss.str();
	}

	inline void set(T v) { _v[0] = v; _v[1] = v; }
	inline void set(T x, T y) { _v[0] = x; _v[1] = y; }

	inline T x() const { return _v[0]; }
	inline T y() const { return _v[1]; }

	inline void x(T v) { _v[0] = v; }
	inline void y(T v) { _v[1] = v; }

	inline T width() const { return _v[0]; }
	inline T height() const { return _v[1]; }

	inline void width(T v) { _v[0] = v; }
	inline void height(T v) { _v[1] = v; }

	inline T aspectRatio() const { return static_cast<T>(_v[0])/static_cast<T>(_v[1]); }

	inline bool isNan() {
		return isnan(_v[0]) || isnan(_v[1]);
	}

protected:
	T _v[3];
};

typedef Vector2Generic<short> Vector2s;
typedef Vector2Generic<int> Vector2i;
typedef Vector2Generic<long long> Vector2l;
typedef Vector2Generic<unsigned short> Vector2us;
typedef Vector2Generic<unsigned int> Vector2ui;
typedef Vector2Generic<unsigned long long> Vector2ul;
typedef Vector2Generic<float> Vector2;
typedef Vector2Generic<double> Vector2d;

template <class T>
class Vector3Generic {
public:
	Vector3Generic() { _v[0] = static_cast<T>(0); _v[1] = static_cast<T>(0); _v[2] = static_cast<T>(0); }
	Vector3Generic(T s) { _v[0] = s; _v[1] = s; _v[2] = s; }
	Vector3Generic(T x, T y, T z) { _v[0] = x; _v[1] = y; _v[2] = z; }
	Vector3Generic(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; }
	Vector3Generic(const Vector3Generic<T> & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; }
	Vector3Generic(const Vector2Generic<T> & v, T z) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = z; }

	static Vector3Generic lerp(const Vector3Generic<T> & from, const Vector3Generic<T> & to, float delta) {
		return Vector3Generic<T>(vwgl::Math::lerp(from._v[0],to._v[0],delta),
								vwgl::Math::lerp(from._v[1],to._v[1],delta),
								vwgl::Math::lerp(from._v[2],to._v[2],delta));
	}
	
	
	inline T * raw() { return _v; }
	inline const T * raw() const { return _v; }
	inline int length() const { return 3; }

	inline float distance(const Vector3Generic<T> & v) {
		Vector3Generic<T> v3(_v[0] - v._v[0],
							 _v[1] - v._v[1],
							 _v[2] - v._v[2]);
		return v3.magnitude();
	}

	inline float magnitude() { return sqrtf(static_cast<float>(_v[0]*_v[0] + _v[1]*_v[1] + _v[2]*_v[2])); }
	inline Vector3Generic<T> & normalize() {
		T m = magnitude();
		_v[0]=_v[0]/m; _v[1]=_v[1]/m; _v[2]=_v[2]/m;
		return *this;
	}
	inline Vector3Generic<T> & add(const Vector3Generic<T> & v2) {
		_v[0] += v2._v[0];
		_v[1] += v2._v[1];
		_v[2] += v2._v[2];
		return *this;
	}
	inline Vector3Generic<T> & sub(const Vector3Generic<T> & v2) {
		_v[0] -= v2._v[0];
		_v[1] -= v2._v[1];
		_v[2] -= v2._v[2];
		return *this;
	}
	inline T dot(const Vector3Generic<T> & v2) {
		return _v[0] * v2._v[0] + _v[1] * v2._v[1] + _v[2] * v2._v[2];
	}
	inline Vector3Generic<T> & scale(T scale) {
		_v[0] *= scale; _v[1] *= scale; _v[2] *= scale;
		return *this;
	}
	inline Vector3Generic<T> & cross(const Vector3Generic<T> & v2) {
		T x = _v[1] * v2._v[2] - _v[2] * v2._v[1];
		T y = _v[2] * v2._v[0] - _v[0] * v2._v[2];
		T z = _v[0] * v2._v[1] - _v[1] * v2._v[0];
		_v[0]=x; _v[1]=y; _v[2]=z;
		return *this;
	}

	inline T operator[](int i) const { return _v[i]; }
	inline bool operator==(const Vector3Generic<T> & v) const { return _v[0]==v._v[0] && _v[1]==v._v[1] && _v[2]==v._v[2]; }
	inline bool operator!=(const Vector3Generic<T> & v) const { return _v[0]!=v._v[0] || _v[1]!=v._v[1] || _v[2]!=v._v[2]; }
	inline void operator=(const Vector3Generic<T> & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; }
	inline void operator=(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; }

	inline Vector3Generic operator+(const Vector3Generic &v) const { return Vector3Generic(_v[0]+v._v[0],_v[1]+v._v[1],_v[2]+v._v[2]); }
	inline Vector3Generic operator-(const Vector3Generic &v) const { return Vector3Generic(_v[0]-v._v[0],_v[1]-v._v[1],_v[2]-v._v[2]); }
	// Cross product operator
	inline Vector3Generic operator^(const Vector3Generic &v) const {
		return Vector3Generic(_v[1] * v._v[2] - _v[2] * v._v[1],
							  _v[2] * v._v[0] - _v[0] * v._v[2],
							  _v[0] * v._v[1] - _v[1] * v._v[0]);
	}
	// Dot product operator (the same operation as dot() function)
	inline T operator*(const Vector3Generic &v) const { return _v[0] * v._v[0] + _v[1] * v._v[1] + _v[2] * v._v[2]; }
	inline Vector3Generic operator/(T scalar) const { return Vector3Generic(_v[0]/scalar,_v[1]/scalar,_v[2]/scalar); }


	inline std::string toString() const {
		std::stringstream ss;
		ss	<< '[' << _v[0] << "," << _v[1] << "," << _v[2] << ']';
		return ss.str();
	}

	inline void set(T v) { _v[0] = v; _v[1] = v; _v[2] = v; }
	inline void set(T x, T y, T z) { _v[0] = x; _v[1] = y; _v[2] = z; }

	inline T x() const { return _v[0]; }
	inline T y() const { return _v[1]; }
	inline T z() const { return _v[2]; }

	inline void x(T v) { _v[0] = v; }
	inline void y(T v) { _v[1] = v; }
	inline void z(T v) { _v[2] = v; }

	inline T r() const { return _v[0]; }
	inline T g() const { return _v[1]; }
	inline T b() const { return _v[2]; }

	inline void r(T v) { _v[0] = v; }
	inline void g(T v) { _v[1] = v; }
	inline void b(T v) { _v[2] = v; }

	inline T width() const { return _v[0]; }
	inline T height() const { return _v[1]; }
	inline T depth() const { return _v[2]; }

	inline void width(T v) { _v[0] = v; }
	inline void height(T v) { _v[1] = v; }
	inline void depth(T v) { _v[2] = v; }

	inline Vector2Generic<T> xy() const { return Vector2Generic<T>(x(),y()); }
	inline Vector2Generic<T> yz() const { return Vector2Generic<T>(y(),z()); }
	inline Vector2Generic<T> xz() const { return Vector2Generic<T>(x(),z()); }

	inline T aspectRatio() const { return static_cast<T>(_v[0])/static_cast<T>(_v[1]); }

	inline bool isNan() {
#ifdef _WIN32
		return _isnan(_v[0]) || _isnan(_v[1]) || _isnan(_v[2]);
#else
		return isnan(_v[0]) || isnan(_v[1]) || isnan(_v[2]);
#endif
	}

protected:
	T _v[3];
};

typedef Vector3Generic<short> Vector3s;
typedef Vector3Generic<int> Vector3i;
typedef Vector3Generic<long> Vector3l;
typedef Vector3Generic<unsigned short> Vector3us;
typedef Vector3Generic<unsigned int> Vector3ui;
typedef Vector3Generic<unsigned long> Vector3ul;
typedef Vector3Generic<float> Vector3;
typedef Vector3Generic<double> Vector3d;

template <class T>
class Vector4Generic {
public:
	static Vector4Generic yellow() { return Vector4Generic(1.0f,1.0f,0.0f,1.0f); }
	static Vector4Generic orange() { return Vector4Generic(1.0f,0.5f,0.0f,1.0f); }
	static Vector4Generic red() { return Vector4Generic(1.0f,0.0f,0.0f,1.0f); }
	static Vector4Generic pink() { return Vector4Generic(1.0f,0.5f,0.5f,1.0f); }
	static Vector4Generic violet() { return Vector4Generic(0.5f,0.0f,1.0f,1.0f); }
	static Vector4Generic blue() { return Vector4Generic(0.0f,0.0f,1.0f,1.0f); }
	static Vector4Generic green() { return Vector4Generic(0.0f,1.0f,0.0f,1.0f); }
	static Vector4Generic white() { return Vector4Generic(1.0f,1.0f,1.0f,1.0f); }
	static Vector4Generic lightGray() { return Vector4Generic(0.8f,0.8f,0.8f,1.0f); }
	static Vector4Generic gray() { return Vector4Generic(0.5f,0.5f,0.5f,1.0f); }
	static Vector4Generic darkGray() { return Vector4Generic(0.2f,0.2f,0.2f,1.0f); }
	static Vector4Generic black() { return Vector4Generic(0.0f,0.0f,0.0f,1.0f); }
	static Vector4Generic brown() { return Vector4Generic(0.4f,0.2f,0.0f,1.0f); }
	static Vector4Generic transparent() { return Vector4Generic(0.0f); }
	
	static Vector4Generic<T> lerp(const Vector4Generic<T> & from, const Vector4Generic<T> & to, float delta) {
		return Vector4Generic<T>(vwgl::Math::lerp(from._v[0],to._v[0],delta),
								  vwgl::Math::lerp(from._v[1],to._v[1],delta),
								  vwgl::Math::lerp(from._v[2],to._v[2],delta),
								  vwgl::Math::lerp(from._v[3],to._v[3],delta));
	}


	Vector4Generic() { _v[0] = static_cast<T>(0); _v[1] = static_cast<T>(0); _v[2] = static_cast<T>(0); _v[3] = static_cast<T>(0); }
	Vector4Generic(T s) { _v[0] = s; _v[1] = s; _v[2] = s; _v[3] = s; }
	Vector4Generic(T x, T y, T z, T w) { _v[0]=x; _v[1]=y; _v[2]=z; _v[3]=w; }
	Vector4Generic(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; _v[3] = v[3]; }
	Vector4Generic(const Vector4Generic<T> & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; _v[3] = v._v[3]; }
	Vector4Generic(const Vector3Generic<T> & v, T w) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; _v[3] = w; }
	Vector4Generic(const Vector2 & v, T z, T w) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = z; _v[3] = w; }

	inline T * raw() { return _v; }
	inline const T * raw() const { return _v; }
	inline int length() const { return 4; }

	inline float distance(const Vector4Generic<T> & v) {
		Vector4Generic<T> v3(_v[0] - v._v[0],
							 _v[1] - v._v[1],
							 _v[2] - v._v[2],
							 _v[3] - v._v[3]);
		return v3.magnitude();
	}

	inline float magnitude() { return sqrtf(static_cast<float>(_v[0]*_v[0] + _v[1]*_v[1] + _v[2]*_v[2] + _v[3]*_v[3])); }
	inline Vector4Generic & normalize() {
		T m = magnitude();
		_v[0]=_v[0]/m; _v[1]=_v[1]/m; _v[2]=_v[2]/m; _v[3]=_v[3]/m;
		return *this;
	}

	inline T operator[](int i) const { return _v[i]; }
	inline bool operator==(const Vector4Generic & v) const { return _v[0]==v._v[0] && _v[1]==v._v[1] && _v[2]==v._v[2] && _v[3]==v._v[3]; }
	inline bool operator!=(const Vector4Generic & v) const { return _v[0]!=v._v[0] || _v[1]!=v._v[1] || _v[2]!=v._v[2] || _v[3]!=v._v[3]; }
	inline void operator=(const Vector4Generic & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; _v[3] = v._v[3]; }
	inline void operator=(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; _v[3] = v[3]; }

	inline Vector4Generic operator+(const Vector4Generic &v) const { return Vector4Generic(_v[0]+v._v[0],_v[1]+v._v[1],_v[2]+v._v[2],_v[3]+v._v[3]); }
	inline Vector4Generic operator-(const Vector4Generic &v) const { return Vector4Generic(_v[0]-v._v[0],_v[1]-v._v[1],_v[2]-v._v[2],_v[3]-v._v[3]); }
	inline T operator*(const Vector4Generic &v) const { return _v[0] * v._v[0] + _v[1] * v._v[1] + _v[2] * v._v[2] + + _v[3] * v._v[3]; }
	inline Vector4Generic operator/(T scalar) const { return Vector4Generic(_v[0]/scalar,_v[1]/scalar,_v[2]/scalar,_v[3]/scalar); }

	inline std::string toString() const {
		std::stringstream ss;
		ss	<< '[' << _v[0] << "," << _v[1] << "," << _v[2] << "," << _v[3] << ']';
		return ss.str();
	}

	inline void set(T v) { _v[0] = v; _v[1] = v; _v[2] = v; _v[3] = v; }
	inline void set(T x, T y, T z, T w) { _v[0] = x; _v[1] = y; _v[2] = z; _v[3] = w; }

	inline T x() const { return _v[0]; }
	inline T y() const { return _v[1]; }
	inline T z() const { return _v[2]; }
	inline T w() const { return _v[3]; }

	inline void x(T v) { _v[0] = v; }
	inline void y(T v) { _v[1] = v; }
	inline void z(T v) { _v[2] = v; }
	inline void w(T v) { _v[3] = v; }

	inline T r() const { return _v[0]; }
	inline T g() const { return _v[1]; }
	inline T b() const { return _v[2]; }
	inline T a() const { return _v[3]; }

	inline void r(T v) { _v[0] = v; }
	inline void g(T v) { _v[1] = v; }
	inline void b(T v) { _v[2] = v; }
	inline void a(T v) { _v[3] = v; }
	

	inline Vector2Generic<T> xy() const { return Vector2Generic<T>(x(),y()); }
	inline Vector2Generic<T> xz() const { return Vector2Generic<T>(x(),z()); }
	inline Vector2Generic<T> xw() const { return Vector2Generic<T>(x(),w()); }
	inline Vector3Generic<T> xyz() const { return Vector3Generic<T>(x(),y(),z()); }

	inline T width() const { return _v[2]; }
	inline T height() const { return _v[3]; }

	inline void width(T v) { _v[2] = v; }
	inline void height(T v) { _v[3] = v; }

	inline float aspectRatio() const { return _v[3]!=static_cast<T>(0) ? static_cast<float>(_v[2])/static_cast<float>(_v[3]):1.0f; }

	inline bool isNan() {
		return isnan(_v[0]) || isnan(_v[1]) || isnan(_v[2]) || isnan(_v[3]);
	}

protected:
	T _v[4];
};

typedef Vector4Generic<short> Vector4s;
typedef Vector4Generic<int> Vector4i;
typedef Vector4Generic<long> Vector4l;
typedef Vector4Generic<unsigned short> Vector4us;
typedef Vector4Generic<unsigned int> Vector4ui;
typedef Vector4Generic<unsigned long> Vector4ul;
typedef Vector4Generic<float> Vector4;
typedef Vector4Generic<double> Vector4d;

typedef Vector4 Color;
typedef Vector3 Size3D;
typedef Vector2 Size2D;
typedef Vector3i Size3Di;
typedef Vector2i Size2Di;
typedef Vector4i Viewport;
typedef Vector4i Rect;
typedef Vector2 Position2D;
typedef Vector2i Position2Di;

template <class T>
class BoundsGeneric {
public:
	BoundsGeneric() {
		T min = vwgl::Math::minValue(static_cast<T>(0));
		T max = vwgl::Math::maxValue(static_cast<T>(0));
		_v[0] = min;
		_v[1] = max;
		_v[2] = min;
		_v[3] = max;
		_v[4] = min;
		_v[5] = max;
	}
	BoundsGeneric(T s) { _v[0] = s; _v[1] = s; _v[2] = s; _v[3] = s; _v[4] = s; _v[5] = s; }
	BoundsGeneric(T left, T right, T bottom, T top, T back, T front) { _v[0]=left; _v[1]=right; _v[2]=bottom; _v[3]=top; _v[4]=back; _v[5]=front; }
	BoundsGeneric(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; _v[3] = v[3]; _v[4] = v[4]; _v[5] = v[5]; }

	
	inline T * raw() { return _v; }
	inline const T * raw() const { return _v; }
	inline int length() const { return 6; }
	
	inline bool valid() { return !isNan() && left()<right() && bottom()<top() && back()<front(); }
	
	inline T operator[](int i) const { return _v[i]; }
	inline bool operator==(const BoundsGeneric & v) const { return _v[0]==v._v[0] && _v[1]==v._v[1] && _v[2]==v._v[2] && _v[3]==v._v[3] && _v[4]==v._v[4] && _v[5]==v._v[5]; }
	inline bool operator!=(const BoundsGeneric & v) const { return _v[0]!=v._v[0] || _v[1]!=v._v[1] || _v[2]!=v._v[2] || _v[3]!=v._v[3] || _v[4]!=v._v[4] || _v[5]!=v._v[5]; }
	inline void operator=(const BoundsGeneric & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; _v[3] = v._v[3]; _v[4]=v._v[4];  _v[5]=v._v[5]; }
	inline void operator=(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; _v[3] = v[3]; _v[4] = v[4]; _v[5] = v[5]; }
	
	inline std::string toString() const {
		std::stringstream ss;
		ss	<< '[' << _v[0] << "," << _v[1] << "," << _v[2] << "," << _v[3] << "," << _v[4] << "," << _v[5] << ']';
		return ss.str();
	}
	
	inline void set(T v) { _v[0] = v; _v[1] = v; _v[2] = v; _v[3] = v; _v[4] = v; _v[5] = v; }
	inline void set(T left, T right, T bottom, T top, T back, T front) { _v[0] = left; _v[1] = right; _v[2] = bottom; _v[3] = top;  _v[4] = back; _v[5] = front; }
	
	inline T left() const { return _v[0]; }
	inline T right() const { return _v[1]; }
	inline T bottom() const { return _v[2]; }
	inline T top() const { return _v[3]; }
	inline T back() const { return _v[4]; }
	inline T front() const { return _v[5]; }
	
	inline void left(T v) { _v[0] = v; }
	inline void right(T v) { _v[1] = v; }
	inline void bottom(T v) { _v[2] = v; }
	inline void top(T v) { _v[3] = v; }
	inline void back(T v) { _v[4] = v; }
	inline void front(T v) { _v[5] = v; }
	
	inline T width() const { return vwgl::Math::abs(right() - left()); }
	inline T height() const { return vwgl::Math::abs(top() - bottom()); }
	inline T depth() const { return vwgl::Math::abs(front() - back()); }
	
	inline bool isNan() {
		return isnan(_v[0]) || isnan(_v[1]) || isnan(_v[2]) || isnan(_v[3]) || isnan(_v[4]) || isnan(_v[5]);
	}
	
	inline bool isInBounds(const Vector3Generic<T> & v) {
		return v.x()>=_v[0] && v.x()<=_v[1] && v.y()>=_v[2] && v.y()<=_v[3] && v.z()>=_v[4] && v.z()<=_v[5];
	}
	
protected:
	T _v[6];
};
	
	
typedef BoundsGeneric<short> Boundss;
typedef BoundsGeneric<int> Boundsi;
typedef BoundsGeneric<long> Boundsl;
typedef BoundsGeneric<unsigned short> Boundsus;
typedef BoundsGeneric<unsigned int> Boundsui;
typedef BoundsGeneric<unsigned long> Boundsul;
typedef BoundsGeneric<float> Bounds;
typedef BoundsGeneric<double> Boundsd;

}

#endif
