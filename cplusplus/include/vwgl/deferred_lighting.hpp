#ifndef _vwgl_deferred_lighting_hpp_
#define _vwgl_deferred_lighting_hpp_

#include <vwgl/deferredmaterial.hpp>
#include <vwgl/light.hpp>
#include <vwgl/scene/light.hpp>

namespace vwgl {

namespace scene {
class DeferredRenderer;
}

class DeferredRenderer;
class DeferredLighting : public Material {
	friend class vwgl::DeferredRenderer;
	friend class scene::DeferredRenderer;
public:
	
	DeferredLighting();

	virtual void initShader();
	
	inline void setExposure(float e) { _exposure = e; }
	inline float getExposure() const { return _exposure; }

	void setDiffuseMap(Texture * texture) { _diffuseMap = texture; }
	void setNormalMap(Texture * texture) { _normalMap = texture; }
	void setSpecularMap(Texture * texture) { _specularMap = texture; }
	void setDepthMap(Texture * texture) { _depthMap = texture; }
	void setPositionMap(Texture * texture) { _positionMap = texture; }
	void setShadowMap(Texture * texture) { _shadowMap = texture; }
	inline void setEnabledLights(int lights) { _enabledLights = static_cast<float>(lights); }
	
	void setLight(Light * light) { _legacyLight = light; }
	void setLight(scene::Light * light) { _light = light; }
	
protected:
	virtual ~DeferredLighting();
	
	void setupUniforms();

	
	ptr<Texture> _diffuseMap;
	ptr<Texture> _normalMap;
	ptr<Texture> _specularMap;
	ptr<Texture> _depthMap;
	ptr<Texture> _positionMap;
	ptr<Texture> _shadowMap;
	float _enabledLights;
	int _shadowBlurIterations;
	
	Light * _legacyLight;	// API 1.0
	scene::Light * _light;	// API 2.0
	
	float _exposure;
};

}

#endif
