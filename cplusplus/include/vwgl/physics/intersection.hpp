
#ifndef _vwgl_physics_intersection_hpp_
#define _vwgl_physics_intersection_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/physics/ray.hpp>
#include <vwgl/physics/plane.hpp>

namespace vwgl {
namespace physics {

class RayToPlaneIntersection;

class VWGLEXPORT Intersection {
public:
	enum Type {
		kTypeNone = 0,
		kTypePoint = 1,
		kTypeLine = 2
	};
	static RayToPlaneIntersection rayToPlane(const Ray & ray, const Plane & plane);

	Intersection() :_type(kTypeNone) {}

	Type getType() const { return _type; }
	const Vector3 & getPoint() const { return _p0; }
	const Vector3 & getEndPoint() const { return _p1; }
	
	bool intersects() const { return false; }

protected:
	Type _type;
	Vector3 _p0;
	Vector3 _p1;
	
};

class VWGLEXPORT RayToPlaneIntersection : public Intersection {
public:
	RayToPlaneIntersection(const Ray & ray, const Plane & plane);
	
	const Ray & getRay() const { return _ray; }
	bool intersects() const { return _intersects; }

	void operator=(const RayToPlaneIntersection & i) {
		_ray = i._ray;
		_intersects = i._intersects;
	}

protected:
	Ray _ray;
	bool _intersects;
};

}
}

#endif
