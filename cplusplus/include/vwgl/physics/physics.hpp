#ifndef _vwgl_physics_physics_hpp_
#define _vwgl_physics_physics_hpp_

#include <vwgl/physics/plane.hpp>
#include <vwgl/physics/ray.hpp>
#include <vwgl/physics/intersection.hpp>

#endif