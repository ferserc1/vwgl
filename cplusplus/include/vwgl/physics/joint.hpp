
#ifndef vwgl_joint_hpp
#define vwgl_joint_hpp

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/matrix.hpp>
#include <vwgl/json_serializer.hpp>

#include <vector>

namespace vwgl {
	
class Drawable;

namespace physics {

class VWGLEXPORT Joint : public vwgl::ReferencedPointer {
public:
	Joint();

	virtual void applyTransform(Matrix4 & modelview) = 0;

	virtual void initDrawable() {}
	inline Drawable * getDrawable() { return _drawable.getPtr(); }

	virtual void serialize(JsonSerializer & serializer, bool endSeparator, bool endLine) = 0;
	virtual void deserialize(JsonDeserializer & deserializer) = 0;

protected:
	virtual ~Joint();
	
	ptr<Drawable> _drawable;
};

typedef std::vector<vwgl::ptr<Joint> > JointList;

class VWGLEXPORT LinkJoint : public Joint {
public:
	LinkJoint();

	virtual void applyTransform(Matrix4 & modelview);
	
	virtual void initDrawable();

	inline const Vector3 & offset() const { return _offset; }
	inline void setOffset(const vwgl::Vector3 & offset) { _offset = offset; calculateTransform(); }
	
	inline void setEulerRotation(const Vector3 & e) { _eulerRotation = e; calculateTransform(); }
	inline const Vector3 & eulerRotation() const { return _eulerRotation; }
	
	inline float getYaw() const { return _eulerRotation.x(); }
	inline float getPitch() const { return _eulerRotation.y(); }
	inline float getRoll() const { return _eulerRotation.z(); }
	
	inline void setYaw(float x) { _eulerRotation.x(x); calculateTransform(); }
	inline void setPitch(float y) { _eulerRotation.y(y); calculateTransform();  }
	inline void setRoll(float z) { _eulerRotation.z(z); calculateTransform();  }

	inline const Matrix4 & transform() const { return _transform; }
	inline Matrix4 & transform() { return _transform; }
	
	virtual void serialize(JsonSerializer & serializer, bool endSeparator = false, bool endLine = true) {
		serializer.openObject(false, true);
		serializer.writeProp<std::string>("type", "LinkJoint", true, true);
		serializer.writeProp("offset", _offset, true, true);
		serializer.writeProp("yaw", _eulerRotation.x(), true, true);
		serializer.writeProp("pitch", _eulerRotation.y(), true, true);
		serializer.writeProp("roll", _eulerRotation.z(), false, true);
		serializer.closeObject(true, endSeparator, endLine);
	}
	
	virtual void deserialize(JsonDeserializer & deserializer) {
		_offset = deserializer.getVector3("offset", _offset);
		float yaw = deserializer.getFloat("yaw", _eulerRotation.x());
		float pitch = deserializer.getFloat("pitch", _eulerRotation.y());
		float roll = deserializer.getFloat("roll", _eulerRotation.z());
		_eulerRotation.set(yaw, pitch, roll);
		calculateTransform();
	}

protected:
	virtual ~LinkJoint();
	
	void calculateTransform();

	Vector3 _offset;
	Vector3 _eulerRotation;
	Matrix4 _transform;
};

}
}

#endif
