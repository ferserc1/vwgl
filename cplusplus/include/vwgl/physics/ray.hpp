
#ifndef _vwgl_physics_ray_hpp_
#define _vwgl_physics_ray_hpp_

#include <vwgl/math.hpp>

namespace vwgl {
namespace physics {

class Ray {
public:
	Ray() {}
	
	static Ray rayWithPoints(const Vector3 & p0, const Vector3 & p1) {
		Ray ray;
		ray.setWithPoints(p0, p1);
		return ray;
	}
	
	static Ray rayWithVector(const Vector3 & vec, const Vector3 & origin, float maxDepth) {
		Ray ray;
		ray.setWithVector(vec, origin, maxDepth);
		return ray;
	}
	
	static Ray rayWithScreenPoint(const Position2Di & screenPoint, const Matrix4 & projMatrix, const Matrix4 & viewMatrix, const Viewport & viewport) {
		Ray ray;
		ray.setWithScreenPoint(screenPoint, projMatrix, viewMatrix, viewport);
		return ray;
	}
	
	
	Ray & setWithPoints(const Vector3 & p0, const Vector3 & p1) {
		_start = p0;
		_end = p1;
		calculateVector();
		return (*this);
	}
	
	Ray & setWithVector(const Vector3 & vec, const Vector3 & origin, float maxDepth) {
		_start = origin;
		_end = origin;
		Vector3 vector = vec;
		vector.normalize().scale(maxDepth);
		_end.add(vector);
		calculateVector();
		return (*this);
	}
	
	Ray & setWithScreenPoint(const Position2Di & screenPoint, const Matrix4 & projMatrix, const Matrix4 & viewMatrix, const Viewport & viewport) {
		Vector4 start = Matrix4::unproject(static_cast<float>(screenPoint.x()), static_cast<float>(screenPoint.y()), 0.0f, viewMatrix, projMatrix, viewport);
		Vector4 end = Matrix4::unproject(static_cast<float>(screenPoint.x()), static_cast<float>(screenPoint.y()), 1.0f, viewMatrix, projMatrix, viewport);
		_start = start.xyz();
		_end = end.xyz();
		calculateVector();
		return (*this);
	}
	
	void operator=(const Ray & ray) {
		_start = ray._start;
		_end = ray._end;
		_vector = ray._vector;
		_magnitude = ray._magnitude;
	}
	
	bool operator==(const Ray & ray) const {
		return _start==ray._start && _end==ray._end;
	}

	inline void setStart(const Vector3 & s) { _start = s; calculateVector(); }
	const Vector3 & getStart() const { return _start; }
	
	inline void setEnd(const Vector3 &e) { _end = e; calculateVector(); }
	const Vector3 & getEnd() const { return _end; }
	
	const Vector3 & getVector() const { return _vector; }
	float getMagnitude() const { return _magnitude; }
	
	
protected:
	Vector3 _start;
	Vector3 _end;
	Vector3 _vector;
	float _magnitude;
	
	void calculateVector() {
		_vector = _end;
		_vector.sub(_start);
		_magnitude = _vector.magnitude();
		_vector.normalize();
	}
};

}
}

#endif
