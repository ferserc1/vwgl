
#ifndef _vwgl_physics_plane_hpp_
#define _vwgl_physics_plane_hpp_

#include <vwgl/math.hpp>

#include <sstream>

namespace vwgl {
namespace physics {

class Plane {
public:
	Plane() :_origin(0.0f), _normal(0.0f,1.0f,0.0f) {}
	Plane(const Vector3 & normal) :_origin(0.0f), _normal(normal) {}
	Plane(const Vector3 & normal, const Vector3 & origin) :_origin(origin), _normal(normal) {}
	Plane(const Vector3 & p1, const Vector3 & p2, const Vector3 & p3) {
		Vector3 vec1(p1); vec1.sub(p2);
		Vector3 vec2(p3); vec2.sub(p1);
		_origin = p1;
		_normal = vec1;
		_normal.cross(vec2).normalize();
	}
	virtual ~Plane() {}

	inline const Vector3 & getNormal() const { return _normal; }
	inline Vector3 & getNormal() { return _normal; }
	inline void setNormal(const Vector3 & n) { _normal = n; }
	
	inline const Vector3 & getOrigin() const { return _origin; }
	inline Vector3 & getOrigin() { return _origin; }
	inline void setOrigin(const Vector3 & o) { _origin = o; }
	
	std::string toString() {
		std::stringstream str;
		str << "P0:" << _origin.toString() << ", normal: " << _normal.toString();
		return str.str();
	}
	
	inline bool valid() { return !_origin.isNan() && !_normal.isNan(); }

	void operator=(const Plane & p) { _origin = p._origin; _normal = p._normal; }
	bool operator==(const Plane & p) const { return _origin==p._origin && _normal==p._normal; }
	
protected:
	
	Vector3 _origin;
	Vector3 _normal;
};

}
}

#endif
