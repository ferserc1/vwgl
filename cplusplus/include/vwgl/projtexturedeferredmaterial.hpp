
#ifndef _vwgl_projtexturedeferredmaterial_hpp_
#define _vwgl_projtexturedeferredmaterial_hpp_

#include <vwgl/deferredmaterial.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/projectable.hpp>

namespace vwgl {

class ProjTextureDeferredMaterial : public DeferredMaterial {
public:
	ProjTextureDeferredMaterial();
	virtual void initShader();

	inline void setProjector(IProjectable * proj) { _projector = proj; }
	inline const IProjectable * getProjector() const { return _projector; }
	inline IProjectable * getProjector() { return _projector; }
	

protected:
	virtual ~ProjTextureDeferredMaterial();
	
	virtual void setupUniforms();
	
	IProjectable * _projector;
	static Matrix4 s_texCoordGenMatrix;
};

}

#endif
