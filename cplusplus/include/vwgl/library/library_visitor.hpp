#ifndef _vwgl_library_library_visitor_hpp_
#define _vwgl_library_library_visitor_hpp_

#include <vwgl/library/node.hpp>
#include <vwgl/library/model.hpp>
#include <vwgl/library/group.hpp>
#include <vwgl/library/material.hpp>

#include <vwgl/system.hpp>

#include <unordered_map>
#include <vector>

namespace vwgl {
namespace library {

class LibraryVisitor {
public:
	virtual void visit(vwgl::library::Node * node) = 0;
};

class AssertLibraryIdentifiersVisitor : public LibraryVisitor {
public:
	virtual void visit(vwgl::library::Node * node) {
		if (!node) return;
		std::map<std::string, library::Node*> identifiers;
		vwgl::library::Group * grp = dynamic_cast<vwgl::library::Group*>(node);

		if (grp) {
			library::NodeList::iterator it;
			for (it = grp->getNodeList().begin(); it != grp->getNodeList().end(); ++it) {
				if ((*it)->getId() == "") {
					(*it)->setId(getUniqueId());
				}
				library::NodeList::iterator other = it + 1;
				for (other = it + 1; other != grp->getNodeList().end(); ++other) {
					if ((*it)->getId() == (*other)->getId()) {
						(*other)->setId(getUniqueId());
					}
				}
				visit((*it).getPtr());
			}
		}
	}

protected:
	std::string getUniqueId() {
		return System::get()->getUuid();
	}
};

template <class T>
class FindNodeVisitor : public LibraryVisitor {
public:
	FindNodeVisitor() {}
	FindNodeVisitor(const std::string & identifier) :_identifier(identifier) {}
	
	inline void setIdentifier(const std::string & identifier) { _identifier = identifier; }
	inline const std::string & getIdentifier() const { return _identifier; }

	void clear() {
		_result = nullptr;
		_resultList.clear();
	}

	virtual void visit(vwgl::library::Node * node) {
		bool isEnd = false;
		if (!_result.valid() && !_identifier.empty()) {
			T * targetNode = dynamic_cast<T*>(node);
			if (targetNode && targetNode->getId()==_identifier) {
				_result = targetNode;
				isEnd = true;
			}
		}
		else if (_identifier.empty()) {
			// Find node list
			T * targetNode = dynamic_cast<T*>(node);
			if (targetNode) {
				_resultList.push_back(targetNode);
			}
		}
		else {
			isEnd = true;
		}
		
		if (!isEnd) {
			vwgl::library::Group * grp = dynamic_cast<vwgl::library::Group*>(node);
			if (grp) {
				library::NodeList::iterator it;
				for (it = grp->getNodeList().begin(); it!=grp->getNodeList().end(); ++it) {
					visit((*it).getPtr());
				}
			}
		}
	}
	
	T * getResult() { return _result.getPtr(); }
	std::vector<ptr<T> > & getResultList() { return _resultList; }
	
protected:
	std::string _identifier;
	ptr<T> _result;
	std::vector<ptr<T> > _resultList;
};

}
}

#endif
