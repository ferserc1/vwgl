#ifndef _vwgl_library_model_hpp_
#define _vwgl_library_model_hpp_

#include <vwgl/library/model.hpp>

namespace vwgl {
namespace library {

class Model : public vwgl::library::Node {
public:
	Model() {}

	inline const std::string & getFile() const { return _file; }
	inline std::string & getFile() { return _file; }
	inline void setFile(const std::string & file) { _file = file; }
	
	virtual Node * clone() {
		ptr<Model> newNode = new Model();
		
		cloneData(newNode.getPtr());
		
		return newNode.release();
	}
	
	virtual void cloneData(Node * newNode) {
		Node::cloneData(newNode);
		Model * modelNode = dynamic_cast<vwgl::library::Model*>(newNode);
		if (modelNode) {
			modelNode->_file = _file;
		}
	}
	
protected:
	virtual ~Model() {}
	
	std::string _file;
};

}
}

#endif
