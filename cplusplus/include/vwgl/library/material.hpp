#ifndef _vwgl_library_material_hpp_
#define _vwgl_library_material_hpp_

#include <vwgl/library/node.hpp>
#include <vwgl/generic_material.hpp>

namespace vwgl {
namespace library {

class Material : public vwgl::library::Node {
public:
	Material() {}

	inline const std::string & getMaterialType() const { return _materialType; }
	inline void setMaterialType(const std::string & type) { _materialType = type; }
	inline MaterialModifier & getMaterialModifier() { return _modifier; }
	inline const MaterialModifier & getMaterialModifier() const { return _modifier; }
	inline void setMaterialModifier(const MaterialModifier & mod) { _modifier = mod; }

	virtual Node * clone() {
		ptr<Material> newNode = new Material();
		
		cloneData(newNode.getPtr());
		
		return newNode.release();
	}
	
	virtual void cloneData(Node * newNode) {
		Node::cloneData(newNode);
		Material * materialNode = dynamic_cast<vwgl::library::Material*>(newNode);
		if (materialNode) {
			materialNode->_materialType = _materialType;
			materialNode->_modifier = _modifier;
		}
	}

protected:
	virtual ~Material() {}
	
	std::string _materialType;
	MaterialModifier _modifier;
};

}
}

#endif
