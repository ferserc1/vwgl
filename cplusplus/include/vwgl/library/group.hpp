#ifndef _vwgl_library_group_hpp_
#define _vwgl_library_group_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/library/node.hpp>

#include <vector>

namespace vwgl {
namespace library {

typedef std::vector<ptr<library::Node> > NodeList;
	
class Group : public library::Node {
public:
	Group() {}

	const library::NodeList & getNodeList() const { return _children; }
	library::NodeList & getNodeList() { return _children; }
	
	void addChild(library::Node * node) {
		if (node->_parent) {
			node->_parent->removeChild(node);
		}
		_children.push_back(node);
		node->_parent = this;
	}
	
	int indexOf(const library::Node * node) {
		int i;
		for (i=0;i<_children.size();++i) {
			if (_children[i].getPtr()==node) {
				return i;
			}
		}
		return -1;
	}
	
	void removeChild(library::Node * node) {
		int index = indexOf(node);
		if (index!=-1) {
			node->_parent = NULL;
			_children.erase(_children.begin() + index);
		}
	}
	
	virtual Node * clone() {
		ptr<Group> newNode = new Group();
		
		cloneData(newNode.getPtr());
		
		return newNode.release();
	}
	
	virtual void cloneData(Node * newNode) {
		Node::cloneData(newNode);
		Group * groupNode = dynamic_cast<vwgl::library::Group*>(newNode);
		if (groupNode) {
			NodeList::iterator it;
			for (it=_children.begin(); it!=_children.end(); ++it) {
				vwgl::library::Node * child = (*it).getPtr();
				vwgl::library::Node * newChild = child->clone();
				groupNode->addChild(newChild);
			}
		}
	}
	
protected:
	virtual ~Group() {}
	
	library::NodeList _children;
};

}
}

#endif
