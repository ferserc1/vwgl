#ifndef _vwgl_library_node_hpp_
#define _vwgl_library_node_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/system.hpp>

#include <string>
#include <map>

namespace vwgl {
namespace library {

typedef std::map<std::string, std::string> Dictionary;

class Group;

class Node : public vwgl::ReferencedPointer {
	friend class Group;
public:
	Node() :_parent(nullptr) {}

	inline const std::string & getPath() const { return _path; }
	inline std::string & getPath() { return _path; }
	inline void setPath(const std::string & path) { _path = path; }

	inline const std::string & getId() const { return _id; }
	inline std::string & getId() { return _id; }
	inline void setId(const std::string & id) { _id = id; }
	
	inline const std::string & getName() const { return _name; }
	inline std::string & getName() { return _name; }
	inline void setName(const std::string & name) { _name = name; }
	
	inline const std::string & getIcon() const { return _icon; }
	inline std::string & getIcon() { return _icon; }
	inline void setIcon(const std::string & icon) { _icon = icon; }
	
	inline const Dictionary & getMetadata() const { return _metadata; }
	inline Dictionary & getMetadata() { return _metadata; }
	inline const std::string & getMetadata(const std::string & key) const { return _metadata.find(key)!=_metadata.end() ? _metadata.find(key)->second:_empty_metadata; }
	inline std::string & getMetadata(const std::string & key) { return _metadata[key]; }
	inline void clearMetadata() { _metadata.clear(); }
	inline void addMetadata(const std::string & key, const std::string & value) { _metadata[key] = value; }
	inline void deleteMetadata(const std::string & key) { _metadata.erase(key); }
	
	inline Group * getParent() { return _parent; }
	
	virtual Node * clone() {
		ptr<Node> newNode = new Node();
		
		cloneData(newNode.getPtr());
		
		return newNode.release();
	}
	
	virtual void cloneData(Node * newNode) {
		if (newNode) {
			newNode->_id = vwgl::System::get()->getUuid();
			newNode->_name = _name;
			newNode->_icon = _icon;
			
			Dictionary::iterator it;
			for (it=_metadata.begin(); it!=_metadata.end(); ++it) {
				newNode->addMetadata(it->first, it->second);
			}
		}
	}
	
protected:
	virtual ~Node() {}
	
	std::string _id;
	std::string _name;
	std::string _icon;
	Dictionary _metadata;
	std::string _empty_metadata;
	Group * _parent;

	std::string _path;
};

}
}

#endif
