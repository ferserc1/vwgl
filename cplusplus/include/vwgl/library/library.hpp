#ifndef _vwgl_library_library_hpp_
#define _vwgl_library_library_hpp_

#include <vwgl/library/node.hpp>
#include <vwgl/library/group.hpp>
#include <vwgl/library/model.hpp>
#include <vwgl/library/material.hpp>

#include <vwgl/library/library_visitor.hpp>

#endif
