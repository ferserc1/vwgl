
#ifndef _vwgl_renderer_hpp_
#define _vwgl_renderer_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/vector.hpp>
#include <vwgl/group.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/render_settings.hpp>
#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class VWGLEXPORT Renderer : public ReferencedPointer {
public:
	Renderer() {}
	
	virtual RenderSettings & getRenderSettings() { return _renderSettings; }

	inline void setViewport(int x, int y, int w, int h) { _viewport = Viewport(x,y,w,h); }
	inline void setViewport(const Viewport & vp) { _viewport = vp; }

	virtual void build(Group * sceneRoot, int width, int height) = 0;
	inline void setSceneRoot(Group * sceneRoot) { if (sceneRoot) { _sceneRoot = sceneRoot;  configureSceneRoot(); } }
	virtual void destroy() = 0;
	virtual void draw(Camera * cam = NULL) = 0;
	virtual void resize(const Size2Di & size) = 0;
	
	virtual void setClearColor(const Vector4 & color) = 0;
	
	inline FilterList & getFilterList() { return _filters; }
	inline const FilterList & getFilterList() const { return _filters; }
	
	template <class T>
	T * getFilter() {
		FilterList::iterator it;
		for (it=_filters.begin(); it!=_filters.end(); ++it) {
			T * mat = dynamic_cast<T*>(*it);
			if (mat) return mat;
		}
		return nullptr;
	}

protected:
	virtual ~Renderer() {}
	
	Viewport _viewport;
	Size2Di _mapSize;
	ptr<Group> _sceneRoot;
	RenderSettings _renderSettings;
	FilterList _filters;
	
	virtual void configureSceneRoot() {}
};

}

#endif
