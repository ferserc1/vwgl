
#ifndef _vwgl_node_visitor_hpp_
#define _vwgl_node_visitor_hpp_

#include <vwgl/node.hpp>
#include <vwgl/group.hpp>

#include <vector>

namespace vwgl {

class VWGLEXPORT NodeVisitor : public ReferencedPointer {
public:
	NodeVisitor() {}

	virtual void visit(Node * node) = 0;

protected:
	virtual ~NodeVisitor() {}
};

template <class T>
class FindNodeVisitor : public NodeVisitor {
public:
	virtual void visit(Node * node) {
		Group * grp = dynamic_cast<Group*>(node);
		T * targetNode = dynamic_cast<T*>(node);
		
		if (targetNode) {
			_result.push_back(targetNode);
		}
		if (grp) {
			NodeList::iterator it;
			for (it=grp->getNodeList().begin(); it!=grp->getNodeList().end(); ++it) {
				visit((*it).getPtr());
			}
		}
	}
	
	std::vector<T*> & getResult() { return _result; }
	const std::vector<T*> & getResult() const { return _result; }

protected:
	std::vector<T*> _result;
};

}
#endif
