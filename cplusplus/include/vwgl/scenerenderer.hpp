
#ifndef _vwgl_scenerenderer_hpp_
#define _vwgl_scenerenderer_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/group.hpp>
#include <vwgl/draw_node_visitor.hpp>

namespace vwgl {

class VWGLEXPORT SceneRenderer : public ReferencedPointer {
public:
	SceneRenderer();

	void draw(Group * sceneRoot, bool renderAlpha = false);
	
	DrawNodeVisitor * getNodeVisitor() { return _drawVisitor.getPtr(); }
	
	inline void setRenderTransparency(bool r) { _renderTransparency = r; }
	inline bool getRenderTransparency() const { return _renderTransparency; }
	
	inline void setRenderOpaque(bool r) { _renderOpaque = r; }
	inline bool getRenderOpaque() const { return _renderOpaque; }
	
protected:
	virtual ~SceneRenderer();
	
	ptr<DrawNodeVisitor> _drawVisitor;
	bool _renderTransparency;
	bool _renderOpaque;
};

}

#endif
