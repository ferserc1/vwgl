#ifndef _vwgl_node_hpp_
#define _vwgl_node_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vector>
#include <string>

namespace vwgl {

class Group;
class TransformNode;

class VWGLEXPORT Node : public ReferencedPointer {
	friend class Group;
public:
	Node();

	virtual void init() {}
	inline bool isInitialized() const { return _initialized; }
	inline void setInitialized(bool i) { _initialized = i; }
	inline void setName(const std::string & name) { _name = name; }
	inline const std::string & getName() const { return _name; }
	inline void setEnabled(bool enabled) { _enabled = enabled; }
	inline bool isEnabled() const { return _enabled; }
	
	bool branchEnabled();
	
	Group * getParent() { return _parent; }
	
	virtual void update() {}
	
protected:
	virtual ~Node();
	
	Group * _parent;
	bool _initialized;
	std::string _name;
	bool _enabled;
};

typedef std::vector<ptr<Node> > NodeList;

}

#endif
