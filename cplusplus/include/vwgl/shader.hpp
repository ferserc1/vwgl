
#ifndef _vwgl_shader_hpp_
#define _vwgl_shader_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/opengl.hpp>
#include <string>
#include <unordered_map>
#include <vwgl/vector.hpp>
#include <vwgl/matrix.hpp>
#include <vwgl/quaternion.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/cubemap.hpp>

#include <unordered_map>

namespace vwgl {

class VWGLEXPORT Shader : public ReferencedPointer {
public:
	Shader();
	
	static void initDefaultPath();

	static const std::string & getShaderPath() { return s_shaderPath; }
	static void getShaderPath(const std::string & path) {
		s_shaderPath = path;
		if (path.back()!='/')
			s_shaderPath = s_shaderPath + '/';
	}
	
	enum ShaderType {
		kTypeVertex = GL_VERTEX_SHADER,
		kTypeFragment = GL_FRAGMENT_SHADER,
		kTypeTessControl = GL_TESS_CONTROL_SHADER,
		kTypeTessEvaluation = GL_TESS_EVALUATION_SHADER,
		kTypeGeometry = GL_GEOMETRY_SHADER
	};
	
	enum ParameterType {
		kOutTypeFragmentDataLocation = 1,
		kInTypeAttribute,
		kInTypeUniform
	};

	enum BindBufferTarget {
		kBindArrayBuffer = GL_ARRAY_BUFFER,
		kBindCopyReadBuffer = GL_COPY_READ_BUFFER,
		kBindCopyWriteBuffer = GL_COPY_WRITE_BUFFER,
		kBindDrawIndirectBuffer = GL_DRAW_INDIRECT_BUFFER,
		kBindElementArrayBuffer = GL_ELEMENT_ARRAY_BUFFER,
		kBindPixelPackBuffer = GL_PIXEL_PACK_BUFFER,
		kBindPixelUnpackBuffer = GL_PIXEL_UNPACK_BUFFER,
		kBindTextureBuffer = GL_TEXTURE_BUFFER,
		kBindTransformFeedbackBuffer = GL_TRANSFORM_FEEDBACK_BUFFER,
		kBindUniformBuffer = GL_UNIFORM_BUFFER
	};

	enum PointerType {
		kPointerTypeByte = GL_BYTE,
		kPointerTypeUnsignedByte = GL_UNSIGNED_BYTE,
		kPointerTypeShort = GL_SHORT,
		kPointerTypeUnsignedShort = GL_UNSIGNED_SHORT,
		kPointerTypeInt = GL_INT,
		kPointerTypeUnsignedInt = GL_UNSIGNED_INT,
		kPointerTypeHalfFloat = GL_HALF_FLOAT,
		kPointerTypeFloat = GL_FLOAT,
		kPointerTypeDouble = GL_DOUBLE,
		kPointerTypeFixed = GL_FIXED,
		kPointerTypeInt_2_10_10_10 = GL_INT_2_10_10_10_REV,
		kPointerTypeUnsignedInt_2_10_10_10 = GL_UNSIGNED_INT_2_10_10_10_REV,
		kPointerTypeUnsignedInt_10F_11F_11F_REV = GL_UNSIGNED_INT_10F_11F_11F_REV
	};
 

	bool loadAndAttachShader(ShaderType shaderType, const std::string & path, bool isAbsolutePath = false);
	bool attachShader(ShaderType shaderType, const std::string & shaderSource, const std::string & debugContext = "");
	bool link(const std::string & debugContext = "");
	inline bool isLinked() { return _linked; }
	void setOutputParameterName(ParameterType type, const std::string & name, int dataLocation = 0);

	inline const std::string & getCompileError() const { return _compileError; }
	inline const std::string & getLinkError() const { return _linkError; }
	
	inline bool isTessellationShader() const { return _isTessellationShader; }
	
	void bind();
	void unbind();
	
	inline GLint getShaderProgram() const { return _shaderProgram; }

	void bindBuffer(BindBufferTarget target, GLint vertexBuffer);
	void enableVertexAttribArray(GLint attribLocation);
	void vertexAttribPointer(GLint attribPointer, int size, PointerType type, bool normalize, int stride, const void * pointer);
	
	GLint getAttribLocation(const std::string & name) const;
	void initUniformLocation(const std::string &name);
	inline GLint getUniformLocation(const std::string & name) { return _uniformLocations[name];	}

	void setUniform(const std::string & name, float v0);
	void setUniform(const std::string & name, float v0, float v1);
	void setUniform(const std::string & name, float v0, float v1, float v2);
	void setUniform(const std::string & name, float v0, float v1, float v2, float v3);
	void setUniform(const std::string & name, int v0);
	void setUniform(const std::string & name, int v0, int v1);
	void setUniform(const std::string & name, int v0, int v1, int v2);
	void setUniform(const std::string & name, int v0, int v1, int v2, int v3);
	void setUniform(const std::string & name, unsigned int v0);
	void setUniform(const std::string & name, unsigned int v0, unsigned int v1);
	void setUniform(const std::string & name, unsigned int v0, unsigned int v1, unsigned int v2);
	void setUniform(const std::string & name, unsigned int v0, unsigned int v1, unsigned int v2, unsigned int v3);
	void setUniform(const std::string & name, bool v);
	void setUniform1v(const std::string & name, int count, const float *value);
	void setUniform2v(const std::string & name, int count, const float *value);
	void setUniform3v(const std::string & name, int count, const float *value);
	void setUniform4v(const std::string & name, int count, const float *value);
	void setUniform1v(const std::string & name, int count, const int *value);
	void setUniform2v(const std::string & name, int count, const int *value);
	void setUniform3v(const std::string & name, int count, const int *value);
	void setUniform4v(const std::string & name, int count, const int *value);
	void setUniform1v(const std::string & name, int count, const unsigned int *value);
	void setUniform2v(const std::string & name, int count, const unsigned int *value);
	void setUniform3v(const std::string & name, int count, const unsigned int *value);
	void setUniform4v(const std::string & name, int count, const unsigned int *value);
	void setUniformMatrix2(const std::string & name,int count, bool transpose, const float *value);
	void setUniformMatrix3(const std::string & name,int count, bool transpose, const float *value);
	void setUniformMatrix4(const std::string & name,int count, bool transpose, const float *value);
	
	void setUniform(const std::string & name, Vector2 & v);
	void setUniform(const std::string & name, Vector3 & v);
	void setUniform(const std::string & name, Vector4 & v);
	void setUniform(const std::string & name, Quaternion & q);
	void setUniform(const std::string & name, Matrix3 & m, bool transpose = false);
	void setUniform(const std::string & name, Matrix4 & m, bool transpose = false);
	void setUniform(const std::string & name, const Vector2 & v);
	void setUniform(const std::string & name, const Vector3 & v);
	void setUniform(const std::string & name, const Vector4 & v);
	void setUniform(const std::string & name, const Quaternion & q);
	void setUniform(const std::string & name, const Matrix3 & m, bool transpose = false);
	void setUniform(const std::string & name, const Matrix4 & m, bool transpose = false);
	
	void setUniform(const std::string & name, Texture * tex, Texture::TextureUnit unit);
	inline void setUniform(const std::string & name, Texture * tex, Texture * alt, Texture::TextureUnit unit) {
		setUniform(name, tex != nullptr ? tex : alt, unit);
	}
	void setUniform(const std::string & name, CubeMap * tex, Texture::TextureUnit unit);

	void destroy();
	
protected:
	virtual ~Shader();

	GLint _shaderProgram;
	
	std::unordered_map<std::string, GLint> _uniformLocations;
	bool _linked;
	std::string _compileError;
	std::string _linkError;
	bool _isTessellationShader;
	
	inline int getTextureUnit(Texture::TextureUnit unit);
	
	static std::string s_shaderPath;
};

class VWGLEXPORT ShaderLibrary : public Singleton {
public:
	static ShaderLibrary * get();

	void addShader(const std::string & name, Shader * shader);
	Shader * getShader(const std::string & name);

	virtual void finalize();
	
protected:
	ShaderLibrary();
	virtual ~ShaderLibrary();

	static ShaderLibrary * s_shaderLibrary;
	
	std::unordered_map<std::string, ptr<Shader> > _shaderMap;
};

}

#endif
