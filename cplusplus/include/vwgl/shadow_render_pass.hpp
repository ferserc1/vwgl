#ifndef _vwgl_shadow_render_pass_material_hpp_
#define _vwgl_shadow_render_pass_material_hpp_

#include <vwgl/deferredmaterial.hpp>
#include <vwgl/shadow_light.hpp>
#include <vwgl/scene/light.hpp>

namespace vwgl {

class ShadowRenderPassMaterial : public DeferredMaterial {
public:
	enum ShadowType {
		kShadowTypeHard = 0,
		kShadowTypeSoft = 1,
		kShadowTypeSoftStratified = 2
	};
	ShadowRenderPassMaterial();

	void setLight(ShadowLight * light) { _legacyLight = light; }
	void setLight(scene::Light * light) { _light = light; }
	ShadowLight * getLegacyLight() { return _legacyLight; }
	scene::Light * getLight() { return _light; }
	inline void setShadowTexture(Texture * tex) { _shadowTexture = tex;  }
	inline Texture * getShadowTexture() { return _shadowTexture; }
	inline const Texture * getShadowTexture() const { return _shadowTexture; }
	inline void setShadowType(ShadowType type) { _shadowType = type; }
	inline ShadowType getShadowType() const { return _shadowType; }
	inline void setBlurIterations(int it) { _blurIterations = it; }
	inline int getBlurIterations() const { return _blurIterations; }
	inline void setShadowCascades(int cascades) { if (cascades>0 && cascades<4) _shadowCascades = cascades; }
	inline int getShadowCascades() const { return _shadowCascades; }
	
	virtual void initShader();
	
protected:
	virtual ~ShadowRenderPassMaterial();
	
	virtual void setupUniforms();
	
	ShadowLight * _legacyLight;
	scene::Light * _light;
	Texture * _shadowTexture;
	ShadowType _shadowType;
	int _blurIterations;
	int _shadowCascades;
};

}

#endif
