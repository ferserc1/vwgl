
#ifndef _vwgl_color_picker_hpp_
#define _vwgl_color_picker_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/node_visitor.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/color_pick_material.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/solid.hpp>
#include <vwgl/light.hpp>
#include <vwgl/draw_gizmos_visitor.hpp>

#include <string>
#include <sstream>

namespace vwgl {

class VWGLEXPORT PickIdFindVisitor;
class VWGLEXPORT PickDrawVisitor;

class VWGLEXPORT ColorPicker : public ReferencedPointer {
public:
	ColorPicker();
	
	void assignPickId(Solid * solid);
	void assignPickId(Drawable * drawable);
	void assignPickId(Light * light);
	
	// TODO: Aquí se le pasará la posición del ratón
	// pero para hacer la prueba pasamos directamente el pickid
	void pick(Node * sceneRoot, PickId id);
	void pick(Node * sceneRoot, int mouseX, int mouseY, int viewportW, int viewportH, Camera * cam = NULL);
	
	const Drawable * getDrawable() const;
	const Solid * getSolid() const;
	const Light * getLight() const;
	Drawable * getDrawable();
	Solid * getSolid();
	Light* getLight();
	const PickId & getResultId() const { return _resultId; }
	
	void clearResult();

	void destroy();
	
	inline void setPickGizmos(bool pick) { _pickGizmos = pick; }
	inline bool getPickGizmos() const { return _pickGizmos; }

protected:
	virtual ~ColorPicker();
	
	static PickId _newPickId;
	ptr<PickIdFindVisitor> _findNodeVisitor;
	ptr<PickDrawVisitor> _drawPickIdsVisitor;
	ptr<DrawGizmosVisitor> _drawGizmosVisitor;
	PickId _resultId;
	bool _pickGizmos;
};

class VWGLEXPORT PickIdFindVisitor : public NodeVisitor {
public:
	PickIdFindVisitor();
	
	void visit(Node * node);
	
	void setTarget(const PickId & target) {
		_target = target;
	}
	
	const Drawable * getDrawable() const { return _drawable.getPtr(); }
	const Solid * getSolid() const { return _solid.getPtr(); }
	const Light * getLight() const { return _light.getPtr(); }
	Drawable * getDrawable() { return _drawable.getPtr(); }
	Solid * getSolid() { return _solid.getPtr(); }
	Light * getLight() { return _light.getPtr(); }

	void clear() {
		_drawable = nullptr;
		_solid = nullptr;
		_light = nullptr;
		_target.group = 0;
		_target.section = 0;
		_target.item = 0;
		_target.part = 0;
	}

protected:
	virtual ~PickIdFindVisitor();
	
	PickId _target;
	ptr<Drawable> _drawable;
	ptr<Solid> _solid;
	ptr<Light> _light;
};

class VWGLEXPORT PickDrawVisitor : public NodeVisitor {
public:
	PickDrawVisitor();
	
	virtual void visit(Node * node);
	
	inline void setCamera(Camera * cam) { _camera = cam; }
	
	inline ColorPickMaterial * getMaterial() { return _material.getPtr(); }
	
protected:
	virtual ~PickDrawVisitor();
	
	void doVisit(Node * node);
	
	ptr<ColorPickMaterial> _material;
	ptr<Camera> _camera;
};

}

#endif
