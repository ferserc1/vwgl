#ifndef _VWGL_DYNAMIC_LIBRARY_HPP_
#define _VWGL_DYNAMIC_LIBRARY_HPP_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/system.hpp>

#include <string>
#include <vector>
#include <functional>

#if VWGL_WINDOWS==1
#define VWGL_LIBRARY __declspec (dllexport)
#else
#define VWGL_LIBRARY
#endif

namespace vwgl {

class VWGLEXPORT DynamicLibrary : public ReferencedPointer {
public:
	static void addSearchPath(const std::string & path);
	static void removeSearchPath(const std::string & path);

	static DynamicLibrary * load(const std::string & path);

	plain_ptr getProcAddress(const std::string & symbolName);

	void destroy();

	static void searchPaths(std::function<void (const std::string&)> callback) {
		if (s_searchPathVector.size()==0) {
			initDefaultSearchPath();
		}

		std::vector<std::string>::iterator it;
		for (it=s_searchPathVector.begin(); it!=s_searchPathVector.end(); ++it) {
			callback(*it);
		}
	}
	
	static std::vector<std::string> & getSearchPathVector() {
		if (s_searchPathVector.size()==0) {
			initDefaultSearchPath();
		}

		return s_searchPathVector;
	}

protected:
	DynamicLibrary();
	virtual ~DynamicLibrary();

	static void initDefaultSearchPath();
	static std::vector<std::string> s_searchPathVector;

	plain_ptr _libraryHandle;
};

}

#endif
