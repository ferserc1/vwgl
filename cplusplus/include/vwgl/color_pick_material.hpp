
#ifndef _vwgl_color_pick_material_hpp_
#define _vwgl_color_pick_material_hpp_

#include <vwgl/material.hpp>

namespace vwgl {

// The "part" component has been removed from the equal operator due to incompatibilities
// with some graphic cards in Windows platform showing the alpha channel
struct PickId {
	unsigned char group;
	unsigned char section;
	unsigned char item;
	unsigned char part;
	
	PickId() {
		group = 0;
		section = 0;
		item = 0;
		part = 0;
	}

	void operator=(const PickId & other) {
		group = other.group;
		section = other.section;
		item = other.item;
		part = other.part;
	}
		
	bool operator==(const PickId & other) {
		return group==other.group && section==other.section && item==other.item;// && part==other.part;
	}
	
	bool isZero() const {
		return group==0 && section==0 && item==0 && part==0;
	}
		
	std::string toString() {
		std::stringstream str;
		str << (int)group << ":" << (int)section << ":" << (int)item << ":" << (int)part;
		return str.str();
	}
};

class VWGLEXPORT ColorPickMaterial : public Material {
public:
	ColorPickMaterial() :Material() { initShader(); }
	
	virtual void initShader();
	
	void setColorPickId(const PickId & id) { _pickId = id; }

protected:
	virtual void setupUniforms();
	
	virtual ~ColorPickMaterial() {}
	
	PickId _pickId;
		
	static std::string _vshader;
	static std::string _fshader;
	static std::string _vshader_gl3;
	static std::string _fshader_gl3;
};

}

#endif
