
#include <vwgl/export.hpp>

#include <vwgl/vwglbutils.hpp>

#include <string>


namespace vwgl {

class VWGLEXPORT ArchiveFile {
public:
	enum PackageStatus {
		kStatusClosed = 0,
		kStatusPack = 1,
		kStatusUnpack = 2
	};

	enum OpenMode {
		kModePack = PackageStatus::kStatusPack,
		kModeUnpack = PackageStatus::kStatusUnpack
	};

	ArchiveFile();

	/*
	*	Open a package file and prepare it for unpack or unpack files
	*	Returns false if packagePath does not exist
	*/
	bool open(const std::string & packagePath, OpenMode mode);

	/*
	 *	Add the file filePath to the package.
	 *	packagePath is the internal path of this file inside the package (only the path, without the file name)
	 *	This function returns false if the file does not exists, if the file could not be opened or if the package
	 *	is not prepared to pack files
	 */
	bool addFile(const std::string & filePath, const std::string & packagePath = "");

	/*
	 *	Unpack the package to path
	 *	Returns false if the package format is not correct
	 */
	bool unpackTo(const std::string & path);

	/*
	 *	Close the package.
	 *	Returns false if the package is not prepared for pack or is not opened
	 */
	bool close();

	/*
	 *	Rerturns the status of the package, that could be:
	 *		kStatusClosed
	 *		kStatusPack: the package is ready to pack files
	 *		kStatusUnpack: the package is ready to unpack files
	 */
	PackageStatus getStatus() const { return _status;  }

protected:
	PackageStatus _status;
	std::string _packagePath;
	std::string _packageFileName;

	VwglbUtils _packageFile;

	bool prepareToPack(const std::string & path);
	bool prepareToUnpack(const std::string & path);
};

}
