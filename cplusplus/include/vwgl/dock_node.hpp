
#ifndef _vwgl_dock_node_hpp_
#define _vwgl_dock_node_hpp_

#include <vwgl/group.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/drawable.hpp>

namespace vwgl {

class VWGLEXPORT DockNode : public Group {
public:
	DockNode();

	virtual void update();

protected:
	virtual ~DockNode();
	
	DrawableBase * getDrawable(TransformNode * trx);
};

}

#endif
