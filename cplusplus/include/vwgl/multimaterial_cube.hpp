#ifndef _VWGL_MULTIMATERIAL_CUBE_HPP_
#define _VWGL_MULTIMATERIAL_CUBE_HPP_

#include <vwgl/drawable.hpp>


namespace vwgl {

class VWGLEXPORT MultimaterialCube : public vwgl::Drawable {
public:
	MultimaterialCube();
	MultimaterialCube(float side);
	MultimaterialCube(float w, float h, float d);
	MultimaterialCube(vwgl::BoundingBox * bbox);
	
	vwgl::Solid * getFace(int index) { return _faces[index].getPtr(); }
	
	vwgl::Solid * leftFace() { return _faces[0].getPtr(); }
	vwgl::Solid * backFace() { return _faces[1].getPtr(); }
	vwgl::Solid * rightFace() { return _faces[2].getPtr(); }
	vwgl::Solid * frontFace() { return _faces[3].getPtr(); }
	vwgl::Solid * topFace() { return _faces[4].getPtr(); }
	vwgl::Solid * bottomFace() { return _faces[5].getPtr(); }
	
protected:
	virtual ~MultimaterialCube();
	
	void buildCube(float w, float h, float d);
	
	vwgl::Solid * getFace(float w, float h, float d, int face);
	
	std::vector<vwgl::ptr<vwgl::Solid> > _faces;
};

}

#endif