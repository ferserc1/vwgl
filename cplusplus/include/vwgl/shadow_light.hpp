
#ifndef _vwgl_shadow_light_hpp_
#define _vwgl_shadow_light_hpp_

#include <vwgl/light.hpp>
#include <vwgl/render_pass.hpp>
#include <vwgl/group.hpp>
#include <vwgl/transform_visitor.hpp>

namespace vwgl {

class VWGLEXPORT ShadowLight : public Light {
	friend class LightManager;
public:
	ShadowLight();

	void updateShadowMap();

	void destroy();
	
	virtual Matrix4 & getProjectionMatrix() { return _projection; }
	virtual Matrix4 & getViewMatrix();
	
	virtual void setCastShadows(bool cs) { _castShadows = cs; }
	virtual bool getCastShadows() const { return _castShadows; }
	
	int getShadowMapWidth() const { return LightManager::get()->getShadowMapSize().width(); }
	int getShadowMapHeight() const { return LightManager::get()->getShadowMapSize().height(); }

	virtual void setShadowBias(float bias) { _shadowBias = bias; }
	virtual float getShadowBias() const { return _shadowBias; }
	
protected:
	virtual ~ShadowLight();
	
	Matrix4 _projection;
	ptr<TransformVisitor> _transformVisitor;
	
	bool _castShadows;
	float _shadowBias;
};


}

#endif
