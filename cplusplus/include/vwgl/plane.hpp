
#ifndef _vwgl_plane_hpp_
#define _vwgl_plane_hpp_

#include <vwgl/drawable.hpp>

namespace vwgl {

class VWGLEXPORT Plane : public Drawable {
public:
	Plane();
	Plane(float side, int rows = 1, int columns = 1);
	Plane(float width, float depth, int rows = 1, int columns = 1);

	
protected:
	virtual ~Plane();
	
	void buildPlane(float width, float height);
	
	int _rows;
	int _columns;
};

}

#endif
