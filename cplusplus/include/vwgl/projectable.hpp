
#ifndef _VWGL_PROJECTABLE_HPP_
#define _VWGL_PROJECTABLE_HPP_

namespace vwgl {

class IProjectable {
public:
	virtual void setProjection(const Matrix4 & proj) = 0;
	virtual Matrix4 & getProjection() = 0;
	virtual const Matrix4 & getProjection() const = 0;
	
	virtual void setTransform(const Matrix4 & trans) = 0;
	virtual Matrix4 & getRawTransform() = 0;
	virtual const Matrix4 & getRawTransform() const = 0;
	
	virtual void setAttenuation(float att) = 0;
	virtual float getAttenuation() const = 0;
	
	virtual Vector3 getPosition() = 0;
	virtual Vector3 getDirection() = 0;
	
	virtual Matrix4 getTransform() = 0;
	
	virtual void setTexture(Texture * tex) = 0;
	virtual Texture * getTexture() = 0;
	virtual const Texture * getTexture() const = 0;
};

}
#endif
