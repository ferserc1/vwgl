
#ifndef _VWGL_VWGLB_FILE_READER_HPP_
#define _VWGL_VWGLB_FILE_READER_HPP_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/vwglbutils.hpp>
#include <vwgl/math.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/polylist.hpp>

#include <vwgl/exceptions.hpp>

#include <map>

namespace vwgl {

class VWGLBFileReaderDelegate {
public:
	struct FileVersion {
		unsigned char major;
		unsigned char minor;
		unsigned char revision;
	};

	enum FileMetadata {
		kNumberOfPlist = 1
	};

	enum JointType {
		kJointTypeInput = 0,
		kJointTypeOutput
	};
	// TODO: Define the delegate methods

	virtual void onError(const std::string & error) = 0;
	virtual void onWarning(const std::string & warning) = 0;

	virtual void fileVersion(const FileVersion & version) = 0;
	virtual void metadata(FileMetadata metadata, int value) = 0;
	virtual void metadata(FileMetadata metadata, const std::string & value) = 0;
	virtual void metadata(FileMetadata metadata, bool value) = 0;
	virtual void metadata(FileMetadata metadata, float value) = 0;

	virtual void polyList(PolyList * plist) = 0;

	virtual void materials(const std::string & materialsString) = 0;

	virtual void projector(Texture * projectorTexture, const Matrix4 & projection, const Matrix4 & transform, float attenuation) = 0;
	virtual void projector(PolyList * plist, Texture * projectorTexture, const Matrix4 & projection, const Matrix4 & transform, float attenuation) = 0;

	virtual void joint(JointType type, const Vector3 & offset, float pitch, float roll, float yaw) = 0;

protected:
	virtual ~VWGLBFileReaderDelegate() {}

	int _expectedPlist;
};
	
class VWGLEXPORT VWGLBFileReader {
public:

	bool load(const std::string & path);
	bool loadHeader(const std::string & path);

	inline void setDelegate(VWGLBFileReaderDelegate * delegate) { _delegate = delegate; }
	inline VWGLBFileReaderDelegate * getDelegate() { return _delegate; }

protected:
	VWGLBFileReaderDelegate * _delegate;

	void readHeader(std::string & materialsString, int & numberOfPlist);
	void readPolyList(int numberOfPlist);
	void readSinglePolyList();
	void loadProjectors(const std::string & materialsString);

	VwglbUtils _fileUtils;
	std::map<std::string, ptr<PolyList> > _plistMap;
};
	
}
#endif