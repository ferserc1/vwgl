
#ifndef _vwgl_system_hpp_
#define _vwgl_system_hpp_

#include <vwgl/platform.hpp>

#include <vwgl/export.hpp>
#include <vwgl/referenced_pointer.hpp>
#include <string>
#include <vector>

#if PLATFORM_WINDOWS==1

#define VWGL_WINDOWS 1
#define VWGL_MAC 0
#define VWGL_IOS 0
#define VWGL_IOS_SIMULATOR 0
#define VWGL_LINUX 0

#elif __APPLE__
#include "TargetConditionals.h"
#if TARGET_IPHONE_SIMULATOR

#define VWGL_WINDOWS 0
#define VWGL_MAC 1
#define VWGL_LINUX 0
#define VWGL_IOS 1
#define VWGL_IOS_SIMULATOR 1

#elif TARGET_OS_IPHONE

#define VWGL_WINDOWS 0
#define VWGL_MAC 1
#define VWGL_IOS 1
#define VWGL_IOS_SIMULATOR 0
#define VWGL_LINUX 0

#elif TARGET_OS_MAC

#define VWGL_WINDOWS 0
#define VWGL_MAC 1
#define VWGL_IOS 0
#define VWGL_IOS_SIMULATOR 0
#define VWGL_LINUX 0

#else

#define VWGL_WINDOWS 0
#define VWGL_MAC 0
#define VWGL_IOS 0
#define VWGL_IOS_SIMULATOR 0
#define VWGL_LINUX 0

#endif

#elif PLATFORM_LINUX==1

#define VWGL_WINDOWS 0
#define VWGL_MAC 0
#define VWGL_IOS 0
#define VWGL_IOS_SIMULATOR 0
#define VWGL_LINUX 1

#endif

namespace vwgl {

class VWGLEXPORT System : public Singleton {
public:
	static System * get() {
		if (s_singleton==NULL) s_singleton = new System();
		return s_singleton;
	}

	enum PackFormat {
		kPackFormatDefault = 0,
		kPackFormatTar = 1
	};

	enum Platform {
		kPlatformWindows = 1,
		kPlatformMac,
		kPlatformIOS,
		kPlatformLinux
	};

	bool isMac();
	bool isWindows();
	bool isLinux();
	bool isIOS();
	bool isIOSSimulator();
	inline bool isPlatform(Platform platform) {
		return
			(platform == kPlatformWindows && isWindows()) ||
			(platform == kPlatformMac && isMac()) ||
			(platform == kPlatformIOS && isIOS()) ||
			(platform == kPlatformLinux && isLinux());
	}

	void standarizePath(const std::string & inputPath, std::string & outputPath);
	bool isAbsolutePath(const std::string & path);

	void setDefaultShaderPath(const std::string & path) { _defaultShaderPath = path; }
	void setResourcesPath(const std::string & path) { _resourcesPath = path; }
	std::string getResourcesPath(const std::string & path) { return addPathComponent(getResourcesPath(), path); }

	const std::string & getExecutablePath();
	const std::string & getDefaultShaderPath();
	const std::string & getResourcesPath();
	const std::string & getTempPath();
	void cleanTempDir();

	inline bool isBigEndian() const { unsigned int v = 0x01020304; return reinterpret_cast<unsigned char*>(&v)[0]==1; }
	inline bool isLittleEndian() const { unsigned int v = 0x01020304; return reinterpret_cast<unsigned char*>(&v)[0]==4; }

	// This is used only in iOS version
	inline void setGLKView(void * view) { _glkView = view; }
	inline void * getGLKView() { return _glkView; }
	void bindGLKViewDrawable();

	bool directoryExists(const std::string & path);
	bool fileExists(const std::string &path);
	bool mkdir(const std::string & dir);
	bool cp(const std::string & src, const std::string & dst, bool overwrite);
	bool removeDirectory(const std::string & path);
	bool removeFile(const std::string & path);
	void listDirectory(const std::string & path, std::vector<std::string> & files, std::vector<std::string> & directories, bool clearVectors = true);
	void listFilesRecursive(const std::string & path, std::vector<std::string> & allFiles, bool clearVector = true, const std::string & basePath = "");
	std::string getUniqueFileName(const std::string & path, int baseIndex = 0);

	bool packFiles(const std::string & inPath, const std::vector<std::string> & files, const std::string & outFile, PackFormat fmt = vwgl::System::kPackFormatDefault);
	bool unpackFiles(const std::string & packedFile, const std::string & outPath);

	std::string getExtension(const std::string & path);
	std::string removeExtension(const std::string & path);
	std::string getFileName(const std::string & path);
	std::string getLastPathComponent(const std::string & path);
	std::string getPath(const std::string & filePath);
	std::string addPathComponent(const std::string & base, const std::string & append);
	std::string addExtension(const std::string & path, const std::string & extension);
	std::string getSubpath(const std::string & fullPath, const std::string & subpath);

	std::string getUuid();

	virtual void finalize() {
		cleanTempDir();
	}

protected:
	std::string _tempPath;
	std::string _executablePath;
	std::string _defaultShaderPath;
	std::string _resourcesPath;

	static System * s_singleton;

	void * _glkView;

	System() :_glkView(nullptr) {
		SingletonController::get()->registerSingleton(this);
	}

	virtual ~System() {
		SingletonController::get()->unregisterSingleton(this);
	}

	void _createTempDir();
};

}

#endif
