
#ifndef _vwgl_boundingbox_hpp_
#define _vwgl_boundingbox_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/node_visitor.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/scene/drawable.hpp>

#ifdef _MSC_VER
#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif
#endif
namespace vwgl {

class DrawableBase;
class Group;
class BoundingBox;
	
// API 1.0
class VWGLEXPORT BuildBoundingBoxVisitor : public NodeVisitor {
public:
	BuildBoundingBoxVisitor(BoundingBox * bbox) :_boundingBox(bbox) {}

	virtual void visit(Node * node);

protected:
	BoundingBox * _boundingBox;
};
	

class VWGLEXPORT BoundingBox : public ReferencedPointer {
public:
	BoundingBox();
	BoundingBox(const float * vertexData, unsigned int length);
	BoundingBox(const PolyList * plist);
	BoundingBox(const DrawableBase * plist);	// API 1.0
	BoundingBox(const scene::Drawable * drawable);	// API 2.0
	BoundingBox(const PolyListVector & plistVector);
	BoundingBox(Group * grp);	// API 1.0

	void setVertexData(const float * vertexData, unsigned int length);
	void setVertexData(const PolyList * plist) { if (plist) setVertexData(plist->getVertexPointer(), plist->getVertexCount()); }
	void setVertexData(const DrawableBase * drawable);	// API 1.0
	void setVertexData(const scene::Drawable * drawable);	// API 2.0
	void setVertexData(const PolyListVector & plistVector);
	void setVertexData(Group * group);	// API 1.0

	void addVertexData(const float * vertexData, unsigned int length);
	void addVertexData(const PolyList * plist) { if (plist) addVertexData(plist->getVertexPointer(), plist->getVertexCount()); }
	void addVertexData(const DrawableBase * drawable);	// API 1.0
	void addVertexData(const scene::Drawable * drawable);	// API 2.0
	void addVertexData(const PolyListVector & plistVector);
	void addVertexData(Group * group);	// API 1.0
	
	void addVertex(const vwgl::Vector3 & v);
	
	void reset();
	
	const Vector3 & max() const { return _max; }
	const Vector3 & min() const { return _min; }
	const Vector3 & size() const { return _size; }
	const Vector3 & halfSize() const { return _halfSize; }
	const Vector3 & center() const { return _center; }
	
protected:
	virtual ~BoundingBox();
	
	void calculateBounds();

	float _left;
	float _right;
	float _top;
	float _bottom;
	float _back;
	float _front;
	
	Vector3 _max;
	Vector3 _min;
	Vector3 _size;
	Vector3 _halfSize;
	Vector3 _center;
};

}

#endif
