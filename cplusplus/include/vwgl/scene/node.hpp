
#ifndef _VWGL_SCENE_NODE_HPP_
#define	_VWGL_SCENE_NODE_HPP_

#include <vwgl/scene/scene_object.hpp>
#include <vwgl/scene/node_visitor.hpp>

#include <vector>
#include <algorithm>
#include <functional>

namespace vwgl {
namespace scene {

class VWGLEXPORT Node : public SceneObject {
public:
	typedef std::vector<ptr<scene::Node> > NodeVector;
	
	Node();
	Node(const std::string & name);

	bool addChild(scene::Node * child);
	bool removeChild(scene::Node * child);
	
	NodeVector & children() { return _children; }
	const NodeVector & children() const { return _children; }
	
	inline Node * parent() { return _parent; }
	
	inline bool haveChild(scene::Node * node) { return std::find(_children.begin(), _children.end(), node)!=_children.end(); }
	inline bool isAncientOf(scene::Node * ancient) {
		return isAncient(this, ancient);
	}
	static bool isAncient(scene::Node * node, scene::Node * potentialAncient) {
		if (!node || !potentialAncient) {
			return false;
		}
		else if (node->_parent == potentialAncient) {
			return true;
		}
		else {
			return isAncient(node->_parent, potentialAncient);
		}
	}
	
	scene::Node * nextChildOf(scene::Node * node);
	scene::Node * prevChildOf(scene::Node * node);
	
	bool swapChildren(scene::Node * childA, scene::Node * childB);
	
	bool moveChildNextTo(scene::Node * nextToNode, scene::Node * movingNode);
	
	void iterateChildren(std::function<void(scene::Node*)> callback) {
		NodeVector::iterator it;
		for (it=_children.begin(); it!=_children.end(); ++it) {
			callback((*it).getPtr());
		}
	}
	
	virtual void accept(scene::NodeVisitor & visitor) {
		visitor.visit(this);
		iterateChildren([&](scene::Node * node) {
			node->accept(visitor);
		});
		visitor.didVisit(this);
	}
	
	virtual void acceptReverse(scene::NodeVisitor & visitor) {
		if (_parent ) {
			_parent->acceptReverse(visitor);
		}
		visitor.visit(this);
	}

	Node * sceneRoot();
	
protected:
	virtual ~Node();
	
	NodeVector _children;
	scene::Node * _parent;
};

}
}

#endif
