
#ifndef _VWGL_SCENE_RENDER_PASS_HPP_
#define _VWGL_SCENE_RENDER_PASS_HPP_

#include <vwgl/material.hpp>
#include <vwgl/fbo.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/draw_visitor.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT RenderPass {
public:
	RenderPass();
	RenderPass(vwgl::Material * mat, vwgl::FramebufferObject * fbo);
	
	inline void setup(vwgl::Material * mat, vwgl::FramebufferObject * fbo) { _material = mat, _fbo = fbo; }
	
	void clear();
	void updateTexture(scene::Node * sceneRoot, bool alphaRender = false, bool renderTransparent = true, bool renderOpaque = true);


	inline void setClearColor(const Color & c) { _clearColor = c; }
	inline Color & getClearColor() { return _clearColor; }
	inline const Color & getClearColor() const { return _clearColor; }
	
	inline void setMaterial(vwgl::Material * mat) { _material = mat; }
	inline vwgl::Material * getMaterial() { return _material.getPtr(); }
	inline const vwgl::Material * getMaterial() const { return _material.getPtr(); }
	
	inline FramebufferObject * getFbo() { return _fbo.getPtr(); }
	inline const FramebufferObject * getFbo() const { return _fbo.getPtr(); }
	inline void setFbo(FramebufferObject * fbo) { _fbo = fbo; }

	inline void setClearBuffers(bool c) { _clearColorBuffer = c; _clearDepthBuffer = c; }
	inline bool getClearBuffers() { return _clearColorBuffer && _clearDepthBuffer; }
	inline void setClearColorBuffer(bool c) { _clearColorBuffer = c; }
	inline void setClearDepthBuffer(bool c) { _clearDepthBuffer = c; }
	inline bool getClearColorBuffer() { return _clearColorBuffer; }
	inline bool getClearDepghtBuffer() { return _clearDepthBuffer; }

protected:
	ptr<Material> _material;
	ptr<FramebufferObject> _fbo;
	scene::DrawVisitor _drawVisitor;
	Color _clearColor;
	bool _clearColorBuffer;
	bool _clearDepthBuffer;
	
	void draw(scene::Node * root, bool alphaRender, bool renderTransparent, bool renderOpaque);
	void drawOpaque(scene::Node * root);
	void drawTranslucent(scene::Node * root);
};

}
}

#endif
