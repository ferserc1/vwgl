
#ifndef _VWGL_SCENE_COMPONENT_PLUGIN_HPP_
#define _VWGL_SCENE_COMPONENT_PLUGIN_HPP_

#include <vwgl/scene/component.hpp>
#include <vwgl/dynamic_library.hpp>

#include <string>
#include <functional>

namespace vwgl {
namespace scene {
	
class VWGLEXPORT ComponentPlugin : public ReferencedPointer {
public:
	ComponentPlugin();

	virtual void initComponents() = 0;

	const std::vector<std::string> & getComponentNames() {
		if (_componentNames.size()==0) initComponents();
		return _componentNames;
	}
	
	inline void eachComponent(std::function<void (const std::string &)> callback) {
		if (_componentNames.size()==0) initComponents();
		std::vector<std::string>::iterator it;
		for (it=_componentNames.begin(); it!=_componentNames.end(); ++it) {
			callback(*it);
		}
	}

	static ComponentPlugin * loadFromLibrary(DynamicLibrary * library);

protected:
	virtual ~ComponentPlugin();
	
	std::vector<std::string> _componentNames;
	
	template <class T>
	void registerComponent(const std::string & componentName) {
		_componentNames.push_back(componentName);
		ComponentFactory<T> pluginComponent(componentName);
	}
};

}
}

extern "C" {
	
typedef vwgl::scene::ComponentPlugin * (*VWGL_SCENE_GET_COMPONENT_PLUGIN_T)();
	
}

#endif