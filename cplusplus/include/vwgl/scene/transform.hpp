
#ifndef _VWGL_SCENE_TRANSFORM_HPP_
#define	_VWGL_SCENE_TRANSFORM_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/transform.hpp>

#include <vector>

namespace vwgl {
namespace scene {


class VWGLEXPORT Transform : public SceneComponent {
public:
	Transform();
	Transform(const Matrix4 & t);
	Transform(TransformStrategy * strategy);
	
	Transform * clone() {
		Transform * trx = new Transform();
		trx->_transform = _transform;
		return trx;
	}

	virtual void willDraw();
	
	virtual void didDraw();
	
	void identity();

	inline vwgl::Transform & getTransform() { return _transform; }
	inline const vwgl::Transform & getTransform() const { return _transform; }

	virtual bool serialize(JsonSerializer & serializer, bool lastItem);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string &resourcePath);

protected:
	virtual ~Transform();
	
	vwgl::Transform _transform;
};


}
}

#endif
