
#ifndef _VWGL_SCENE_FORWARD_RENDERER_HPP_
#define _VWGL_SCENE_FORWARD_RENDERER_HPP_

#include <vwgl/scene/renderer.hpp>
#include <vwgl/projtexturedeferredmaterial.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT ForwardRenderer : public scene::Renderer {
public:
	ForwardRenderer();
	ForwardRenderer(vwgl::scene::Node * sceneRoot);
	
	virtual void draw();
	
protected:
	virtual ~ForwardRenderer();
	
	void renderTranslucent();
	void renderOpaque();
	
	ptr<ProjTextureDeferredMaterial> _projectorMat;
};
	
}
}

#endif
