
#ifndef _VWGL_SCENE_LOCATION_HELPER_HPP_
#define _VWGL_SCENE_LOCATION_HELPER_HPP_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/scene/transform_visitor.hpp>
#include <vwgl/scene/component.hpp>
#include <vwgl/math.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT LocationHelper {
public:
	inline void setComponent(scene::Component * component) { _component = component; }
	
	Vector3 & getPosition();
	Vector3 & getCameraDirection();
	Matrix4 & getTransformMatrix();
	Matrix4 & getAbsoluteTransformMatrix();

protected:
	Component * _component;
	Vector3 _position;
	Vector3 _direction;
	Matrix4 _transformMatrix;
	
	scene::TransformVisitor _transformVisitor;
	
	scene::Node * node();
};

}
}
#endif
