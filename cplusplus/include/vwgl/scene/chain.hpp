#ifndef _VWGL_SCENE_CHAIN_HPP_
#define _VWGL_SCENE_CHAIN_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/math.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT Chain :public SceneComponent {
public:
	Chain();

	virtual void willDraw();

	virtual bool serialize(JsonSerializer & serializer, bool lastItem);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string & resourcePath);
	
protected:
	virtual ~Chain();
};

}
}

#endif