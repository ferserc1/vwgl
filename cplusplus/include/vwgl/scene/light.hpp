
#ifndef _VWGL_SCENE_LIGHT_HPP_
#define _VWGL_SCENE_LIGHT_HPP_

#include <vwgl/light.hpp>
#include <vwgl/scene/scene_component.hpp>
#include <vwgl/scene/transform_visitor.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/scene/render_pass.hpp>
#include <vwgl/scene/location_helper.hpp>

#include <list>

namespace vwgl {
namespace scene {

class LightModifier {
public:
	enum ModifierFlag {
		kAmbient			= 0x1 <<  0,
		kDiffuse			= 0x1 <<  1,
		kSpecular			= 0x1 <<  2,
		kConstantAtt		= 0x1 <<  3,
		kLinearAtt			= 0x1 <<  4,
		kExpAtt				= 0x1 <<  5,
		kSpotCutoff			= 0x1 <<  6,
		kSpotExponent		= 0x1 <<  7,
		kShadowStrength		= 0x1 <<  8,
		kCutoffDistance		= 0x1 <<  9,
		kCastShadows		= 0x1 << 10,
		kShadowBias			= 0x1 << 11,
		kType				= 0x1 << 12,
		
		kModifierAll = ~0
	};
	
	LightModifier()
		:_type(ILight::kTypeDirectional),
		_constantAtt(1.0f),
		_linearAtt(0.5f),
		_expAtt(0.1f),
		_spotExponent(30.0f),
		_spotCutoff(20.0f),
		_shadowStrength(1.0f),
		_cutoffDistance(-1.0f),
		_castShadows(true),
		_shadowBias(0.002f),
		_modifierMask(0) {}
	
	inline const ILight::LightType & getType() const { return _type; }
	inline const Vector4 & getAmbient() const { return _ambient; }
	inline const Vector4 & getDiffuse() const { return _diffuse; }
	inline const Vector4 & getSpecular() const { return _specular; }
	inline float getConstantAttenuation() const { return _constantAtt; }
	inline float getLinearAttenuation() const { return _linearAtt; }
	inline float getExpAttenuation() const { return _expAtt; }
	inline float getSpotCutoff() const { return _spotCutoff; }
	inline float getSpotExponent() const { return _spotExponent; }
	inline float getShadowStrength() const { return _shadowStrength; }
	inline float getCutoffDistance() const { return _cutoffDistance; }
	inline bool getCastShadows() const { return _castShadows; }
	inline float getShadowBias() const { return _shadowBias; }
	
	inline void setType(const ILight::LightType & type) { setEnabled(kType); _type = type; }
	inline void setAmbient(const Vector4 & ambient) { setEnabled(kAmbient); _ambient = ambient; }
	inline void setDiffuse(const Vector4 & diffuse) { setEnabled(kDiffuse); _diffuse = diffuse; }
	inline void setSpecular(const Vector4 & specular) { setEnabled(kSpecular); _specular = specular; }
	inline void setConstantAttenuation(float att) { setEnabled(kConstantAtt); _constantAtt = att; }
	inline void setLinearAttenuation(float att) { setEnabled(kLinearAtt); _linearAtt = att; }
	inline void setExpAttenuation(float att) { setEnabled(kExpAtt); _expAtt = att; }
	inline void setSpotCutoff(float v) { setEnabled(kSpotCutoff); _spotCutoff = v; }
	inline void setSpotExponent(float v) { setEnabled(kSpotExponent); _spotExponent = v; }
	inline void setShadowStrength(float v) { setEnabled(kShadowStrength); _shadowStrength = v; }
	inline void setCutoffDistance(float d) { setEnabled(kCutoffDistance); _cutoffDistance = d; }
	inline void setCastShadows(bool cs) { setEnabled(kCastShadows); _castShadows = cs; }
	inline void setShadowBias(float bias) { setEnabled(kShadowBias); _shadowBias = bias; }

	inline unsigned int getFlags() const { return _modifierMask; }
	inline void setFlags(unsigned int mask) { _modifierMask = mask; }
	inline bool isEnabled(ModifierFlag m) const { return (_modifierMask & m)!=0; }
	inline void setEnabled(ModifierFlag m) { _modifierMask = _modifierMask | m; }
	inline void setDisabled(ModifierFlag m) { _modifierMask = _modifierMask & ~m;}
	
protected:
	ILight::LightType _type;
	vwgl::Color _ambient;
	vwgl::Color _diffuse;
	vwgl::Color _specular;
	float _constantAtt;
	float _linearAtt;
	float _expAtt;
	float _spotCutoff;
	float _spotExponent;
	float _shadowStrength;
	float _cutoffDistance;
	bool _castShadows;
	float _shadowBias;
	
	unsigned int _modifierMask;
};

class VWGLEXPORT Light : public SceneComponent, public ILight {
public:
	typedef std::list<Light*> LightList;

	Light();
	
	static LightList & getLightSources() {
		return s_lightVector;
	}
	
	static void updateShadowMap(scene::Node * sceneRoot, scene::Light * light);
	static void setShadowMapSize(const Size2Di & size) { s_shadowMapSize = size; }
	static const vwgl::Size2Di & getShadowMapSize() { return s_shadowMapSize; }
	static Texture * getShadowTexture() {
		return s_renderPass.getFbo() ? s_renderPass.getFbo()->getTexture():nullptr;
	}
	static void clearShadowMap() {
		s_renderPass.setup(nullptr, nullptr);
	}
	
	virtual Vector3 getPosition();
	virtual Vector3 getDirection();
	virtual Vector4 & getAmbient() { return _ambient; }
	virtual Vector4 & getDiffuse() { return _diffuse; }
	virtual Vector4 & getSpecular() { return _specular; }
	virtual float getIntensity() const { return _intensity; }
	virtual float getConstantAttenuation() const { return _constantAtt; }
	virtual float getLinearAttenuation() const { return _linearAtt; }
	virtual float getExpAttenuation() const { return _expAtt; }
	virtual float getSpotCutoff() const { return _spotCutoff; }
	virtual float getSpotExponent() const { return _spotExponent; }
	virtual float getShadowStrength() const { return _shadowStrength; }
	virtual float getCutoffDistance() const { return _cutoffDistance; }
	virtual void setCastShadows(bool cs) { _castShadows = cs; }
	virtual void setShadowBias(float bias) { _shadowBias = bias; }
	
	virtual void setAmbient(const Vector4 & ambient) { _ambient = ambient; }
	virtual void setDiffuse(const Vector4 & diffuse) { _diffuse = diffuse; }
	virtual void setSpecular(const Vector4 & specular) { _specular = specular; }
	virtual void setIntensity(float i) { _intensity = i; }
	virtual void setConstantAttenuation(float att) { _constantAtt = att; }
	virtual void setLinearAttenuation(float att) { _linearAtt = att; }
	virtual void setExpAttenuation(float att) { _expAtt = att; }
	virtual void setSpotCutoff(float v) { _spotCutoff = v; }
	virtual void setSpotExponent(float v) { _spotExponent = v; }
	virtual void setShadowStrength(float v) { _shadowStrength = v; }
	virtual void setCutoffDistance(float d) { _cutoffDistance = d; }
	virtual bool getCastShadows() const { return _castShadows; }
	virtual float getShadowBias() const { return _shadowBias; }
	
	virtual vwgl::Matrix4 & getTransform();
	virtual vwgl::Matrix4 & getAbsoluteTransform();
	virtual vwgl::Matrix4 & getProjectionMatrix() { return _projection; }
	virtual vwgl::Matrix4 & getViewMatrix();
	
	virtual bool serialize(JsonSerializer & serializer, bool lastItem);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string &resourcePath);
	
	LightModifier getModifierWithMask(unsigned int mask);
	void applyModifier(const LightModifier & mod);
	
protected:
	virtual ~Light();
	
	vwgl::Color _ambient;
	vwgl::Color _diffuse;
	vwgl::Color _specular;
	float _intensity;
	float _constantAtt;
	float _linearAtt;
	float _expAtt;
	float _spotCutoff;
	float _spotExponent;
	float _shadowStrength;
	float _cutoffDistance;
	
	TransformVisitor _transformVisitor;
	LocationHelper _locationHelper;
	
	Matrix4 _projection;
	Matrix4 _directionalViewMatrix;
	bool _castShadows;
	float _shadowBias;
	Matrix4 _transformMatrix;
	
	static void createShadowMap();

	static void registerLight(Light * l);
	static void unregisterLight(Light * l);
	
	
	static LightList s_lightVector;
	static scene::RenderPass s_renderPass;
	static Size2Di s_shadowMapSize;
};


}
}

#endif
