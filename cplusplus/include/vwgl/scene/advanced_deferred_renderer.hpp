
#ifndef _VWGL_SCENE_ADVANCED_DEFERRED_RENDERER_HPP_
#define _VWGL_SCENE_ADVANCED_DEFERRED_RENDERER_HPP_


#include <vwgl/scene/renderer.hpp>
#include <vwgl/scene/node.hpp>

#include <vwgl/render_canvas.hpp>
#include <vwgl/deferred_lighting.hpp>
#include <vwgl/bloom_map_material.hpp>
#include <vwgl/ssao.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/scene/render_pass.hpp>
#include <vwgl/advanced_def_renderer_material.hpp>
#include <vwgl/deferredmixmaterial.hpp>
#include <vwgl/shadow_render_pass.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT AdvancedDeferredRenderer : public Renderer {
public:
	enum RenderPhase {
		kRenderPassGBuffers			= 1 << 0,
		kRenderPassTranslucent		= 1 << 1,
		kRenderPassLighting			= 1 << 2,
		kRenderPassShadows			= 1 << 3,
		kRenderPassPostprocess		= 1 << 4
	};

	AdvancedDeferredRenderer();
	AdvancedDeferredRenderer(vwgl::scene::Node * sceneRoot);
	
	virtual void resize(const Size2Di & size);
	virtual void draw();
	
	void enableRenderPass(RenderPhase pass) { _renderPasses = _renderPasses | pass; }
	void disableRenderPass(RenderPhase pass) { _renderPasses = _renderPasses & ~pass; }
	unsigned int getRenderPassMask() const { return _renderPasses; }
	void setRenderPassMask(unsigned int mask) { _renderPasses = mask; }
	
protected:
	virtual ~AdvancedDeferredRenderer();
	
	void build();
	
	void destroy();
	
	void prepareFrame(scene::Camera * cam);
	void geometryPass();
	void lightingPass();
	Texture * getShadowMap(scene::Camera * cam, scene::Light * light);
	void renderShadowCascade(scene::Camera * cam, scene::Light * light, ShadowRenderPassMaterial * shadowMat, float near, float far, float boxWidth, float boxNear, float boxFar);
	void postprocessPass(scene::Camera * cam);
	void presentCanvas(scene::Camera * cam);
	
	unsigned int _renderPasses;
	
	scene::RenderPass _gbufferPass;
	scene::RenderPass _shadowRP;
	scene::RenderPass _translucentRP;
	
	ptr<DeferredLighting> _lightingMaterial;
	ptr<HomogeneousRenderCanvas> _lightingCanvas;
	ptr<FramebufferObject> _lightingFbo;
	
	ptr<SSAOMaterial> _ssaoMaterial;
	ptr<HomogeneousRenderCanvas> _ssaoCanvas;
	ptr<FramebufferObject> _ssaoFbo;
	
	ptr<BloomMapMaterial> _bloomMaterial;
	ptr<HomogeneousRenderCanvas> _bloomCanvas;
	ptr<FramebufferObject> _bloomFbo;
	
	ptr<HomogeneousRenderCanvas> _canvas;
	ptr<DeferredPostprocess> _postprocessMat;
	
	Viewport _viewport;
};

}
}

#endif