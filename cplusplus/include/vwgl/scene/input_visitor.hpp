
#ifndef _VWGL_SCENE_INPUT_VISITOR_HPP_
#define	_VWGL_SCENE_INPUT_VISITOR_HPP_

#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/app/mouse_event.hpp>
#include <vwgl/app/keyboard_event.hpp>
#include <vwgl/scene/node.hpp>

namespace vwgl {
namespace scene {

template <class T>
class EventVisitor : public NodeVisitor {
public:
	inline const T & getEvent() const { return _event; }
	inline T & getEvent() { return _event; }
	inline void setEvent(const T & evt) { _event = evt; }

protected:
	T _event;
};

typedef EventVisitor<app::MouseEvent> MouseEventVisitor;
typedef EventVisitor<app::KeyboardEvent> KeyboardEventVisitor;

class VWGLEXPORT MouseUpInputVisitor : public MouseEventVisitor {
public:
	virtual void visit(scene::Node * node);
};
	
class VWGLEXPORT MouseDownInputVisitor : public MouseEventVisitor {
public:
	virtual void visit(scene::Node * node);
};

class VWGLEXPORT MouseMoveInputVisitor : public MouseEventVisitor {
public:
	virtual void visit(scene::Node * node);
};

class VWGLEXPORT MouseDragInputVisitor : public MouseEventVisitor {
public:
	virtual void visit(scene::Node * node);
};
	
class VWGLEXPORT MouseWheelInputVisitor : public MouseEventVisitor {
public:
	virtual void visit(scene::Node * node);
};

class VWGLEXPORT KeyDownInputVisitor : public KeyboardEventVisitor {
public:
	virtual void visit(scene::Node * node);
	
};

class VWGLEXPORT KeyUpInputVisitor : public KeyboardEventVisitor {
public:
	virtual void visit(scene::Node * node);
	
};

}
}

#endif
