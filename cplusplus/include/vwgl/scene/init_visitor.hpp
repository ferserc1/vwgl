
#ifndef _VWGL_SCENE_INIT_VISITOR_HPP_
#define	_VWGL_SCENE_INIT_VISITOR_HPP_

#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/scene/node.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT InitVisitor : public NodeVisitor {
public:
	InitVisitor();
	
	virtual void visit(scene::Node * node);

};

}
}

#endif
