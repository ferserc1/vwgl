
#ifndef _VWGL_SCENE_SCENEOBJECT_HPP_
#define	_VWGL_SCENE_SCENEOBJECT_HPP_

#include <vwgl/system.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/scene/lifecycle.hpp>
#include <vwgl/scene/component.hpp>

#include <functional>
#include <string>

namespace vwgl {
namespace scene {

class VWGLEXPORT SceneObject : public ReferencedPointer, public ILifeCycle {
public:
	SceneObject();
	SceneObject(const std::string & name);
	
	inline void setName(const std::string & name) { _name = name; }
	inline const std::string & getName() const { return _name; }
	inline std::string & getName() { return _name; }
	
	inline void setEnabled() { _enabled = true; }
	inline void setEnabled(bool enabled) { _enabled = enabled; }
	inline void setDisabled() { _enabled = false; }
	inline bool isEnabled() const { return _enabled; }
	
	virtual Texture * getGizmoIcon() { return _gizmoIcon.getPtr(); }
	virtual void setGizmoIcon(Texture * tex) { _gizmoIcon = tex; }
	inline PolyList * getGizmoPolyList() { return _gizmoPolyList.getPtr(); }
	inline void setGizmoPolyList(PolyList * plist) { _gizmoPolyList = plist; }
	
	void addComponent(Component * component) {
		if (component) {
			ptr<Component> preventDelete = component;
			
			if (component->_sceneObject!=nullptr) {
				component->_sceneObject->removeComponent(component);
			}
			
			_componentMap[getHash(component)] = component;
			component->_sceneObject = this;
		}
	}
	
	void removeComponent(Component * component) {
		ComponentMap::iterator iterator = _componentMap.find(getHash(component));
		if (iterator!=_componentMap.end()) {
			component->_sceneObject = nullptr;
			_componentMap.erase(iterator);
		}
	}
	
	template <class T>
	T * getComponent() {
		return dynamic_cast<T*>(_componentMap[typeid(T).hash_code()].getPtr());
	}
	
	ComponentMap & componentMap() { return _componentMap; }
	const ComponentMap & componentMap() const { return _componentMap; }

	void eachComponent(std::function<void(Component*)> callback) {
		ComponentMap::iterator it;
		for (it = _componentMap.begin(); it!=_componentMap.end(); ++it) {
			if (it->second.valid()) callback(it->second.getPtr());
		}
	}
	
	virtual void init();
	virtual void resize(const Size2Di & size);
	virtual void update();
	virtual void frame(float delta);
	virtual void willDraw();
	virtual void draw();
	virtual void didDraw();
	virtual void mouseDown(const app::MouseEvent & evt);
	virtual void mouseUp(const app::MouseEvent & evt);
	virtual void mouseMove(const app::MouseEvent & evt);
	virtual void mouseDrag(const app::MouseEvent & evt);
	virtual void keyDown(const app::KeyboardEvent & evt);
	virtual void keyUp(const app::KeyboardEvent & evt);
	virtual void mouseWheel(const app::MouseEvent & evt);
	virtual void willDrawIcon();
	virtual void didDrawIcon();

protected:
	virtual ~SceneObject();
	
	ComponentMap _componentMap;
	
	std::string _name;
	bool _enabled;
	
	vwgl::ptr<vwgl::Texture> _gizmoIcon;
	vwgl::ptr<vwgl::PolyList> _gizmoPolyList;
	
	size_t getHash(Component * component) { return typeid(*component).hash_code(); }
};

}
}

#endif
