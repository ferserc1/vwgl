
#ifndef _VWGL_SCENE_CUBEMAP_HPP_
#define _VWGL_SCENE_CUBEMAP_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/scene/draw_visitor.hpp>
#include <vwgl/scene/transform_visitor.hpp>
#include <vwgl/cubemap.hpp>
#include <vwgl/deferredmaterial.hpp>
#include <vwgl/fbo.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT Cubemap : public SceneComponent {
public:
	static void setCubemapSize(const Size2Di & size) { s_cubemapSize = size; s_cubemapSizeChanged = true; }
	static const Size2Di & getCubemapSize() { return s_cubemapSize; }
	static void finalize() { s_cubemapMaterial = nullptr; }

	Cubemap();

	inline void setClearColor(const Color & c) { _clearColor = c; }
	inline const Color & getClearColor() const { return _clearColor; }

	virtual void init();
	virtual void update();

	inline vwgl::CubeMap * getCubeMap() { return getFbo()->getCubeMap(); }

	virtual bool serialize(JsonSerializer & serializer, bool lastItem);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string & resourcePath);
	
protected:
	virtual ~Cubemap();

	FramebufferObject * getFbo();

	static Size2Di s_cubemapSize;
	static bool s_cubemapSizeChanged;

	// This is a simple shared material to render the scene to the cubemap. This
	// material only uses the diffuse color and texture, without ilumination nor
	// other effects
	static ptr<DeferredMaterial> s_cubemapMaterial;
	static DeferredMaterial * cubemapMaterial();

	void beginDraw();
	void drawToFace(Texture::TextureTarget target);
	void endDraw();

	DrawVisitor _drawVisitor;
	Color _clearColor;
	ptr<FramebufferObject> _fbo;
	scene::TransformVisitor _transformVisitor;

};

}
}

#endif
