
#ifndef _VWGL_SCENE_SCENE_COMPONENT_HPP_
#define _VWGL_SCENE_SCENE_COMPONENT_HPP_

#include <vwgl/scene/scene_object.hpp>
#include <vwgl/scene/component.hpp>


namespace vwgl {
namespace scene {

class Billboard;
class BoundingBox;
class Camera;
class Chain;
class Cubemap;
class Drawable;
class InputChainJoint;
class OutputChainJoint;
class Light;
class Projector;
class Transform;
class VWGLEXPORT SceneComponent : public Component {
public:
	template <class T>
	inline T * component() {
		return sceneObject() ? sceneObject()->getComponent<T>():nullptr;
	}
	
	Billboard * billboard();
	BoundingBox * boundingBox();
	Camera * camera();
	Chain * chain();
	Cubemap * cubemap();
	Drawable * drawable();
	InputChainJoint * inputJoint();
	OutputChainJoint * outputJoint();
	Light * light();
	Projector * projector();
	Transform * transform();

protected:
	virtual ~SceneComponent() {}
};

}
}
#endif
