
#ifndef _VWGL_SCENE_PLANE_HPP_
#define _VWGL_SCENE_PLANE_HPP_

#include <vwgl/scene/drawable.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT Plane : public DrawableFactory {
public:
	Plane() :_w(1.0f), _d(1.0f) {}
	Plane(float side) :_w(side), _d(side) {}
	Plane(float w, float d) :_w(w), _d(d) {}

	virtual Drawable * getDrawable();
	
	inline void setSize(float w, float d) { _w = w; _d = d; }

protected:

	float _w;
	float _d;
};

}
}

#endif
