
#ifndef _VWGL_SCENE_NODEVISITOR_HPP_
#define _VWGL_SCENE_NODEVISITOR_HPP_

#include <vwgl/system.hpp>


namespace vwgl {
namespace scene {

class Node;
class VWGLEXPORT NodeVisitor {
public:
	virtual void visit(scene::Node * node);
	virtual void didVisit(scene::Node * node);
};
	
}
}

#endif
