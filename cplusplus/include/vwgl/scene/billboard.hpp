
#ifndef _VWGL_SCENE_BILLBOARD_HPP_
#define _VWGL_SCENE_BILLBOARD_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/scene/drawable.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT Billboard : public SceneComponent {
public:
	Billboard();
	Billboard(Texture * texture);
	Billboard(Texture * texture, const Size2D & size);

	// Billboard drawing is affected by Drawable::disableDraw()
	virtual void draw();
	
	virtual bool serialize(JsonSerializer & serializer, bool lastItem);
	virtual bool saveResourcesToPath(const std::string & dstPath);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string & resourcePath);
	
	inline PolyList * getPolyList() { return _polyList.getPtr(); }
	inline GenericMaterial * getMaterial() { return _material.getPtr(); }
	
	inline Texture * getTexture() { return _material->getTexture(); }
	inline void setTexture(Texture * texture) { _material->setTexture(texture); }

protected:
	virtual ~Billboard();
	
	Size2D _size;
	ptr<PolyList> _polyList;
	ptr<GenericMaterial> _material;
	
	void create();
	void clear();
};

}
}
#endif
