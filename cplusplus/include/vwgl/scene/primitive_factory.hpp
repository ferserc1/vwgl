
#ifndef _VWGL_SCENE_PRIMITIVE_FACTORY_HPP_
#define _VWGL_SCENE_PRIMITIVE_FACTORY_HPP_

#include <vwgl/scene/drawable.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT PrimitiveFactory {
public:
	static Drawable * cube();
	static Drawable * cube(float side);
	static Drawable * cube(float w, float h, float d);
	
	static Drawable * plane();
	static Drawable * plane(float side);
	static Drawable * plane(float w, float d);
	
	static Drawable * sphere();
	static Drawable * sphere(float radius);
	static Drawable * sphere(float radius, int divisions);
	static Drawable * sphere(float radius, int slices, int stacks);
};
	
}
}

#endif
