
#ifndef _VWGL_SCENE_DRAWABLE_HPP_
#define _VWGL_SCENE_DRAWABLE_HPP_

#include <vwgl/scene/scene_component.hpp>

#include <vwgl/polylist.hpp>
#include <vwgl/material.hpp>
#include <vwgl/transform.hpp>

#include <unordered_map>
#include <functional>
#include <vector>

#include <stdexcept>

namespace vwgl {
	
class GenericMaterial;

namespace scene {

class DrawableElementException : public std::runtime_error {
public:
	DrawableElementException() :std::runtime_error("Drawable element exception") {}
	DrawableElementException(const std::string & message) :std::runtime_error(message) {}
};

class DrawableIndexOutOfBoundsException : public DrawableElementException {
public:
	DrawableIndexOutOfBoundsException() :DrawableElementException("Drawable element index out of bounds") {}
	DrawableIndexOutOfBoundsException(const std::string & message) :DrawableElementException(message) {}
};

class DrawableElementNotFoundException : public DrawableElementException {
public:
	DrawableElementNotFoundException() :DrawableElementException("Drawable element not found") {}
	DrawableElementNotFoundException(const std::string & message) :DrawableElementException(message) {}
};

class DrawableInvalidParameterException : public DrawableElementException {
public:
	DrawableInvalidParameterException() :DrawableElementException("Invalid parameter supplied to drawable") {}
	DrawableInvalidParameterException(const std::string & message) :DrawableElementException(message) {}
};

class Drawable;
class IDrawableObserver {
public:
	virtual void drawableChanged(Drawable *) {}		// Triggered when the Drawable geometry has chaged
	virtual void applyMatrix(const Matrix4 &) {}	// Triggered when the method Drawable::applyTransform() is called
};

class VWGLEXPORT Drawable : public SceneComponent {
public:
	typedef std::vector<IDrawableObserver*> DrawableObserverVector;

	enum RenderFlag {
		kRenderTransparent	= 1 << 0,
		kRenderOpaque		= 1 << 1
	};

	Drawable();

	/*
	 *	Add a new PolyList to the drawable. A polyList can only be added once. If the material is not specified or is nullptr,
	 *	a new vwgl::GenericMaterial will be added as material. If no Transform is specified, the Transform object will be
	 *	initialized with an identity matrix.
	 *	Return value:
	 *		the new polyList index if succes
	 *		-1 if plist == nullptr
	 *		the existing polyList index if the polyList is already in the list
	 *
	 */
	int addPolyList(PolyList * plist, Material * mat, const vwgl::Transform & trx);
	inline int addPolyList(PolyList * plist, Material * mat) { return addPolyList(plist, mat, vwgl::Transform(Matrix4::makeIdentity())); }
	inline int addPolyList(PolyList * plist) { return addPolyList(plist, nullptr, vwgl::Transform(Matrix4::makeIdentity())); }
	
	inline PolyListVector & getPolyListVector() { return _polyList; }
	inline const PolyListVector & getPolyListVector() const { return _polyList; }
	
	inline std::string & getName() { return _name; }
	inline const std::string & getName() const { return _name; }
	inline void setName(const std::string & name) { _name = name; }

	/*
	*	The following methods throws DrawableIndexOutOfBoundsException or DrawableElementNotFoundException
	*/

	/*
	 *	Remove the specified polyList.
	 *		Throws a DrawableElementNotFound exception if the plist element is not found.
	 *		Throws a DrawableIndexOutOfBounds exception if the specified index is out of bounds
	 */
	void removePolyList(PolyList * plist);
	void removePolyList(int index);

	/*
	 *	Returns the plist index or -1 if the item is not found in the PolyListVector	
	 */
	int getPolyListIndex(PolyList * plist);

	void setPolyList(int index, vwgl::PolyList * plist);
	void setMaterial(int index, vwgl::Material * mat);
	void setTransform(int index, vwgl::Transform & trx);

	
	PolyList * getPolyList(int index);
	Material * getMaterial(int index);
	GenericMaterial * getGenericMaterial(int index);
	vwgl::Transform & getTransform(int index);

	Material * getMaterial(PolyList * plist);
	GenericMaterial * getGenericMaterial(PolyList * plist);
	vwgl::Transform & getTransform(PolyList * plist);

	void registerObserver(IDrawableObserver * observer);
	void unregisterObserver(IDrawableObserver * observer);

	inline void eachElement(std::function<void(vwgl::PolyList * plist, vwgl::Material * mat, vwgl::Transform & trx)> callback) {
		for (size_t i = 0; i < _polyList.size(); ++i) {
			callback(_polyList[i].getPtr(), _material[i].getPtr(), _transform[i]);
		}
	}

	inline void eachObserver(std::function<void(IDrawableObserver*)> callback) {
		DrawableObserverVector::iterator it;
		for (it = _observerVector.begin(); it != _observerVector.end(); ++it) {
			callback(*it);
		}
	}

	virtual void draw();

	void switchUVs(PolyList::VertexBufferType uv1, PolyList::VertexBufferType uv2);
	void setCubemap(vwgl::CubeMap * cm);
	void applyTransform(const vwgl::Matrix4 & transform);
	void flipFaces();
	void flipNormals();

	void hideGroup(const std::string & groupName);
	void showGroup(const std::string & groupName);
	void showByName(const std::string & name);
	void hideByName(const std::string & name);

	inline void setDirty() {
		eachObserver([&](IDrawableObserver * observer) {
			observer->drawableChanged(this);
		});
	}

	static inline void enableRenderFlag(RenderFlag flag) { s_renderFlags = s_renderFlags | flag; }
	static inline void disableRenderFlag(RenderFlag flag) { s_renderFlags = s_renderFlags & ~flag; }
	static inline bool isRenderFlagEnabled(RenderFlag flag) { return (s_renderFlags & flag) != 0; }
	static inline unsigned int getRenderFlags() { return s_renderFlags; }
	static inline void setRenderFlags(unsigned int flags) { s_renderFlags = flags; }
	
	// Use mat as material, ignore the material vector
	static inline void setForcedMaterial(Material * mat) { s_fordedMaterial = mat; }
	static inline Material * getForcedMaterial() { return s_fordedMaterial; }

	// Use the material vector's settings with the forced material
	static inline void setUseMaterialSettings(bool use) { s_useMaterialSettings = use; }
	static inline bool getUseMaterialSettings() { return s_useMaterialSettings; }

	static inline void disableDraw() { s_preventDraw = true; }
	static inline void enableDraw() { s_preventDraw = false; }
	static inline bool isDrawingEnabled() { return !s_preventDraw; }

	virtual bool serialize(JsonSerializer & serializer, bool lastItem);
	virtual bool saveResourcesToPath(const std::string & dstPath);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string & resourcePath);

protected:
	virtual ~Drawable();

	vwgl::PolyListVector _polyList;
	vwgl::MaterialVector _material;
	vwgl::TransformVector _transform;
	std::string _name;
	
	DrawableObserverVector _observerVector;
	
	static unsigned int s_renderFlags;
	static Material * s_fordedMaterial;
	static bool s_preventDraw;
	static bool s_useMaterialSettings;
};
	
class DrawableFactory {
public:
	virtual Drawable * getDrawable() = 0;
};

}
}

#endif
