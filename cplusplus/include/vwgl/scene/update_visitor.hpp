
#ifndef _VWGL_SCENE_UPDATE_VISITOR_HPP_
#define _VWGL_SCENE_UPDATE_VISITOR_HPP_

#include <vwgl/scene/node_visitor.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT UpdateVisitor : public scene::NodeVisitor {
public:
	virtual void visit(scene::Node * node);
};

}
}

#endif
