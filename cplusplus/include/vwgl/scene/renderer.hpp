
#ifndef _VWGL_SCENE_RENDERER_HPP_
#define _VWGL_SCENE_RENDERER_HPP_

#include <vwgl/scene/init_visitor.hpp>
#include <vwgl/scene/resize_visitor.hpp>
#include <vwgl/scene/frame_visitor.hpp>
#include <vwgl/scene/draw_visitor.hpp>
#include <vwgl/scene/update_visitor.hpp>
#include <vwgl/scene/input_visitor.hpp>
#include <vwgl/scene/node.hpp>

#include <vwgl/render_settings.hpp>


namespace vwgl {
namespace scene {

class VWGLEXPORT Renderer : public vwgl::ReferencedPointer {
public:
	enum RenderPath {
		kRenderPathForward = 1,
		kRenderPathDeferred
	};
	
	static Renderer * create(RenderPath rp);
	static Renderer * create(RenderPath rp, scene::Node * sceneRoot);
	
	Renderer();
	Renderer(scene::Node * root);
	
	inline void setSceneRoot(scene::Node * root) { _sceneRoot = root; }
	inline scene::Node * getSceneRoot() { return _sceneRoot.getPtr(); }
	
	inline void setRenderSettings(RenderSettings & settings) { _renderSettings = settings; }
	inline RenderSettings & getRenderSettings() { return _renderSettings; }
	inline const RenderSettings & getRenderSettings() const { return _renderSettings; }
	
	inline void setClearColor(const Color & c) { _clearColor = c; }
	inline const Color & getClearColor() const { return _clearColor; }

	virtual void init();
	virtual void resize(const Size2Di & size);
	virtual void frame(float delta);
	virtual void update();

	virtual void draw() = 0;
	
	virtual void mouseDown(const app::MouseEvent & evt);
	virtual void mouseUp(const app::MouseEvent & evt);
	virtual void mouseMove(const app::MouseEvent & evt);
	virtual void mouseDrag(const app::MouseEvent & evt);
	virtual void keyDown(const app::KeyboardEvent & evt);
	virtual void keyUp(const app::KeyboardEvent & evt);
	virtual void mouseWheel(const app::MouseEvent & evt);

	inline scene::InitVisitor & initVisitor() { return _initVisitor; }
	inline scene::ResizeVisitor & resizeVisitor() { return _resizeVisitor; }
	inline scene::FrameVisitor & frameVisitor() { return _frameVisitor; }
	inline scene::UpdateVisitor & updateVisitor() { return _updateVisitor; }
	inline scene::DrawVisitor & drawVisitor() { return _drawVisitor; }
	inline scene::MouseUpInputVisitor & mouseUpVisitor() { return _mouseUpVisitor; }
	inline scene::MouseDownInputVisitor & mouseDownVisitor() { return _mouseDownVisitor; }
	inline scene::MouseMoveInputVisitor & mouseMoveVisitor() { return _mouseMoveVisitor; }
	inline scene::MouseDragInputVisitor & mouseDragVisitor() { return _mouseDragVisitor; }
	inline scene::KeyDownInputVisitor & keyDownVisitor() { return _keyDownVisitor; }
	inline scene::KeyUpInputVisitor & keyUpVisitor() { return _keyUpVisitor; }
	inline scene::MouseWheelInputVisitor & mouseWheelVisitor() { return _mouseWheelVisitor; }

	inline FilterList & getFilterList() { return _filters; }
	inline const FilterList & getFilterList() const { return _filters; }
	
	template <class T>
	T * getFilter() {
		FilterList::iterator it;
		for (it=_filters.begin(); it!=_filters.end(); ++it) {
			T * mat = dynamic_cast<T*>(*it);
			if (mat) return mat;
		}
		return nullptr;
	}
	
protected:
	virtual ~Renderer();
	
	ptr<scene::Node> _sceneRoot;
	
	RenderSettings _renderSettings;
	
	scene::InitVisitor _initVisitor;
	scene::ResizeVisitor _resizeVisitor;
	scene::FrameVisitor _frameVisitor;
	scene::UpdateVisitor _updateVisitor;
	scene::DrawVisitor _drawVisitor;
	scene::MouseUpInputVisitor _mouseUpVisitor;
	scene::MouseDownInputVisitor _mouseDownVisitor;
	scene::MouseMoveInputVisitor _mouseMoveVisitor;
	scene::MouseDragInputVisitor _mouseDragVisitor;
	scene::KeyUpInputVisitor _keyUpVisitor;
	scene::KeyDownInputVisitor _keyDownVisitor;
	scene::MouseWheelInputVisitor _mouseWheelVisitor;
	
	FilterList _filters;
	
	Color _clearColor;
};

}
}

#endif