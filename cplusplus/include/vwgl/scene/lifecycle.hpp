
#ifndef _VWGL_SCENE_LIFECYCLE_HPP_
#define	_VWGL_SCENE_LIFECYCLE_HPP_

#include <vwgl/material.hpp>

#include <vwgl/app/mouse_event.hpp>
#include <vwgl/app/keyboard_event.hpp>

namespace vwgl {
namespace scene {

class ILifeCycle {
public:
	virtual void init() = 0;
	virtual void update() = 0;
	virtual void resize(const vwgl::Size2Di & size) = 0;
	virtual void frame(float delta) = 0;
	virtual void willDraw() = 0;
	virtual void draw() = 0;
	virtual void didDraw() = 0;
	
	virtual void mouseDown(const app::MouseEvent &) = 0;
	virtual void mouseUp(const app::MouseEvent &) = 0;
	virtual void mouseMove(const app::MouseEvent &) = 0;
	virtual void mouseDrag(const app::MouseEvent &) = 0;
	virtual void keyDown(const app::KeyboardEvent &) = 0;
	virtual void keyUp(const app::KeyboardEvent &) = 0;
	virtual void mouseWheel(const app::MouseEvent &) = 0;
	
	virtual void willDrawIcon() = 0;
	virtual void didDrawIcon() = 0;
};

}
}

#endif
