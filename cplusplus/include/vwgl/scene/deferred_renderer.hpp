
#ifndef _VWGL_SCENE_DEFERRED_RENDERER_HPP_
#define _VWGL_SCENE_DEFERRED_RENDERER_HPP_

#include <vwgl/scene/renderer.hpp>
#include <vwgl/scene/render_pass.hpp>

#include <vwgl/deferredmixmaterial.hpp>
#include <vwgl/projtexturedeferredmaterial.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/render_canvas.hpp>
#include <vwgl/diffuse_render_pass.hpp>
#include <vwgl/specular_render_pass.hpp>
#include <vwgl/shadow_render_pass.hpp>
#include <vwgl/deferred_lighting.hpp>
#include <vwgl/bloom_map_material.hpp>
#include <vwgl/ssao.hpp>
#include <vwgl/scene/camera.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT DeferredRenderer : public scene::Renderer {
public:
	enum RenderPhase {
		kRenderPassDiffuse		= 1 << 0,
		kRenderPassNormal		= 1 << 1,
		kRenderPassSpecular		= 1 << 2,
		kRenderPassPosition		= 1 << 3,
		kRenderPassSelection	= 1 << 4,
		kRenderPassLighting		= 1 << 5,
		kRenderPassShadows		= 1 << 6,
		kRenderPassPostprocess	= 1 << 7
	};

	DeferredRenderer();
	DeferredRenderer(vwgl::scene::Node * sceneRoot);
	
	virtual void resize(const Size2Di & size);
	virtual void draw();
	
	
	inline void setAntiAliasingSamples(int samples, bool update=false) {
		_antiAliasing = samples;
		if (_sceneRoot.valid() && update) {
			_sceneRoot->accept(resizeVisitor());
		}
	}
	void enableRenderPass(RenderPhase pass) { _renderPasses = _renderPasses | pass; }
	void disableRenderPass(RenderPhase pass) { _renderPasses = _renderPasses & ~pass; }
	unsigned int getRenderPassMask() const { return _renderPasses; }
	void setRenderPassMask(unsigned int mask) { _renderPasses = mask; }
	
protected:
	virtual ~DeferredRenderer();
	
	int _antiAliasing;
	unsigned int _renderPasses;
	
	// Render pass
	scene::RenderPass _diffuseRP;
	scene::RenderPass _translucentRP;
	scene::RenderPass _normalRP;
	scene::RenderPass _specularRP;
	scene::RenderPass _depthRP;
	scene::RenderPass _positionRP;
	scene::RenderPass _shadowRP;
	scene::RenderPass _selectionRP;
	
	ptr<ProjTextureDeferredMaterial> _projectorMat;
	
	ptr<DeferredLighting> _lightingMaterial;
	ptr<HomogeneousRenderCanvas> _lightingCanvas;
	ptr<FramebufferObject> _lightingFbo;
	
	ptr<SSAOMaterial> _ssaoMaterial;
	ptr<HomogeneousRenderCanvas> _ssaoCanvas;
	ptr<FramebufferObject> _ssaoFbo;
	
	ptr<BloomMapMaterial> _bloomMaterial;
	ptr<HomogeneousRenderCanvas> _bloomCanvas;
	ptr<FramebufferObject> _bloomFbo;
	
	
	ptr<HomogeneousRenderCanvas> _canvas;
	
	ptr<DeferredPostprocess> _postprocessMat;
	
	Viewport _viewport;
	
	
	void build();
	
	void destroy();
	
	void prepareFrame(scene::Camera * cam);
	void geometryPass();
	void lightingPass();
	void postprocessPass(scene::Camera * cam);
	void presentCanvas(scene::Camera * cam);
};
	
}
}

#endif
