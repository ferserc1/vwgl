
#ifndef _VWGL_SCENE_FRAME_VISITOR_HPP_
#define	_VWGL_SCENE_FRAME_VISITOR_HPP_

#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/scene/node.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT FrameVisitor : public NodeVisitor {
public:
	FrameVisitor();

	inline void setDelta(float d) { _delta = d; }
	inline float getDelta() const { return _delta; }

	virtual void visit(scene::Node * node);
	
protected:
	
	float _delta;
};

}
}

#endif
