
#ifndef _VWGL_SCENE_TRANSFORM_VISITOR_HPP_
#define	_VWGL_SCENE_TRANSFORM_VISITOR_HPP_

#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/math.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT TransformVisitor : public NodeVisitor {
public:
	TransformVisitor();

	inline Matrix4 & getMatrix() { return _matrix; }
	inline const Matrix4 & getMatrix() const { return _matrix; }

	virtual void visit(scene::Node * node);
	
protected:
	Matrix4 _matrix;
};

}
}

#endif
