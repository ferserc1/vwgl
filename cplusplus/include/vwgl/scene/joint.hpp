
#ifndef _VWGL_SCENE_JOINT_HPP_
#define	_VWGL_SCENE_JOINT_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/physics/joint.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/boundingbox.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT ChainJoint : public SceneComponent {
public:
	ChainJoint() { _joint = new physics::LinkJoint(); }
	ChainJoint(physics::LinkJoint * joint) :_joint(joint) {}

	inline void setJoint(physics::LinkJoint * joint) { _joint = joint; }
	inline physics::LinkJoint * getJoint() { return _joint.getPtr(); }
	inline const physics::LinkJoint * getJoint() const { return _joint.getPtr(); }

	virtual void deserialize(JsonDeserializer & deserializer, const std::string & resourcePath);
	
protected:
	virtual ~ChainJoint() {}

	ptr<physics::LinkJoint> _joint;
};

class VWGLEXPORT InputChainJoint : public ChainJoint {
public:
	InputChainJoint() :ChainJoint() {}
	InputChainJoint(physics::LinkJoint * joint) :ChainJoint(joint) {}

	virtual bool serialize(JsonSerializer & serializer, bool lastItem) {
		serializer.openObject(true, true);
		serializer.writeProp<std::string>("type", "InputChainJoint", true, true);
		serializer.writeKey("joint");
		_joint->serialize(serializer);
		serializer.closeObject(true, !lastItem, true);
		return true;
	}
	
	virtual void willDrawIcon();
	virtual void didDrawIcon();
	
	virtual void init();
	
protected:
	virtual ~InputChainJoint() {}
};

class VWGLEXPORT OutputChainJoint : public ChainJoint {
public:
	OutputChainJoint() :ChainJoint() {}
	OutputChainJoint(physics::LinkJoint * joint) :ChainJoint(joint) {}

	virtual bool serialize(JsonSerializer & serializer, bool lastItem) {
		serializer.openObject(true, true);
		serializer.writeProp<std::string>("type", "OutputChainJoint", true, true);
		serializer.writeKey("joint");
		_joint->serialize(serializer);
		serializer.closeObject(true, !lastItem, true);
		return true;
	}
	
	virtual void willDrawIcon();
	virtual void didDrawIcon();
	
	virtual void init();
	
protected:
	virtual ~OutputChainJoint() {}
};

}
}

#endif
