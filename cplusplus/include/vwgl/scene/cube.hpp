
#ifndef _VWGL_SCENE_CUBE_HPP_
#define _VWGL_SCENE_CUBE_HPP_

#include <vwgl/scene/drawable.hpp>
#include <vwgl/boundingbox.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT Cube : public scene::DrawableFactory {
public:
	Cube() :_w(1.0f), _h(1.0f), _d(1.0f) {}
	Cube(float side) :_w(side), _h(side), _d(side) {}
	Cube(float w, float h, float d) :_w(w), _h(h), _d(d) {}

	virtual Drawable * getDrawable();
	
	inline void setSize(float side) { _w = side; _h = side; _d = side; }
	inline void setSize(float w, float h, float d) { _w = w; _h = h; _d = d; }
	
protected:
	float _w;
	float _h;
	float _d;
};

}
}

#endif
