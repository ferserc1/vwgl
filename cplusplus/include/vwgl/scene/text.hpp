
#ifndef _vwgl_scene_text_hpp_
#define _vwgl_scene_text_hpp_

#include <vwgl/text_font.hpp>
#include <vwgl/scene/component.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT Text : public Component {
public:
	Text() :_dirtySize(true) {}
	Text(TextFont * font) :_font(font), _dirtySize(true)  {}
	Text(TextFont * font, const std::string & message) :_font(font), _text(message), _dirtySize(true) {}
	
	inline void setFont(TextFont * font) { _dirtySize = true; _font = font;}
	inline const TextFont * getFont() const { return _font.getPtr(); }
	inline TextFont * getFont() { _dirtySize = true; return _font.getPtr(); }
	
	inline void setText(const std::string & text) { _dirtySize = true; _text = text; }
	inline const std::string & getText() const { return _text; }
	
	const Size2D & getSize();
	
	virtual void draw();

	virtual bool serialize(JsonSerializer & serializer, bool lastItem);
	virtual bool saveResourcesToPath(const std::string & dstPath);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string & resourcePath);
	
protected:
	virtual ~Text() {}
	
	ptr<TextFont> _font;
	std::string _text;
	bool _dirtySize;
	Size2D _size;
};

}
}

#endif
