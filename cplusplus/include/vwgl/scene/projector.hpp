
#ifndef _VWGL_SCENE_PROJECTOR_HPP_
#define _VWGL_SCENE_PROJECTOR_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/scene/location_helper.hpp>
#include <vwgl/math.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/projectable.hpp>

#include <list>

namespace vwgl {
namespace scene {

class VWGLEXPORT Projector : public SceneComponent, public IProjectable {
public:
	typedef std::list<Projector*> ProjectorList;
	
	static ProjectorList & getProjectorList() { return s_projectorList; }

	Projector();
	
	virtual void setProjection(const Matrix4 & proj) { _projection = proj; }
	virtual Matrix4 & getProjection() { return _projection; }
	virtual const Matrix4 & getProjection() const { return _projection; }
	
	virtual void setTransform(const Matrix4 & trans) { _transform = trans; }
	virtual Matrix4 & getRawTransform() { return _transform; }
	virtual const Matrix4 & getRawTransform() const { return _transform; }
	
	virtual void setAttenuation(float att) { _attenuation = att; }
	virtual float getAttenuation() const { return _attenuation; }
	
	virtual Vector3 getPosition() { return _locationHelper.getPosition(); }
	virtual Vector3 getDirection() { return _locationHelper.getCameraDirection(); }
	virtual Matrix4 getTransform() { Matrix4 result = _locationHelper.getTransformMatrix(); return result.mult(_transform); }
	
	virtual void setTexture(Texture * tex);
	virtual Texture * getTexture() { return _texture.getPtr(); }
	virtual const Texture * getTexture() const { return _texture.getPtr(); }
	
	inline void setVisible(bool v) { _visible = v; }
	inline bool isVisible() { return _visible && sceneObject()!=nullptr && sceneObject()->isEnabled(); }
	inline void show() { _visible = true; }
	inline void hide() { _visible = false; }
	
	virtual bool serialize(JsonSerializer & serializer, bool lastItem);
	virtual bool saveResourcesToPath(const std::string & dstPath);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string &resourcePath);
	
protected:
	virtual ~Projector();
	
	static ProjectorList s_projectorList;
	
	static void registerProjector(Projector * proj) { s_projectorList.push_back(proj); }
	static void unregisterProjector(Projector * proj) { s_projectorList.remove(proj); }
	
	ptr<Texture> _texture;
	Matrix4 _projection;
	Matrix4 _transform;
	float _attenuation;
	LocationHelper _locationHelper;
	bool _visible;
};

}
}

#endif