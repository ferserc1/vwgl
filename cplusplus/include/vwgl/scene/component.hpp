
#ifndef _VWGL_SCENE_COMPONENT_HPP_
#define	_VWGL_SCENE_COMPONENT_HPP_

#include <vwgl/system.hpp>
#include <vwgl/scene/lifecycle.hpp>
#include <vwgl/material.hpp>
#include <vwgl/json_serializer.hpp>

#include <unordered_map>
#include <functional>

namespace vwgl {
namespace scene {

class SceneObject;
class Node;
class VWGLEXPORT Component : public ReferencedPointer, public ILifeCycle {
	friend class SceneObject;
public:
	static Component * factory(const std::string & jsonString, const std::string & resourcePath);
	
	Component();
	
	virtual void init();
	virtual void resize(const Size2Di & size);
	virtual void update();
	virtual void frame(float delta);
	virtual void willDraw();
	virtual void draw();
	virtual void didDraw();
	virtual void mouseDown(const app::MouseEvent & evt);
	virtual void mouseUp(const app::MouseEvent & evt);
	virtual void mouseMove(const app::MouseEvent & evt);
	virtual void mouseDrag(const app::MouseEvent & evt);
	virtual void keyDown(const app::KeyboardEvent & evt);
	virtual void keyUp(const app::KeyboardEvent & evt);
	virtual void mouseWheel(const app::MouseEvent & evt);
	virtual void willDrawIcon();
	virtual void didDrawIcon();

	inline SceneObject * sceneObject() { return _sceneObject; }
	Node * node();
	
	virtual bool serialize(vwgl::JsonSerializer &, bool) { return false; };
	virtual bool saveResourcesToPath(const std::string &) { return true; }
	virtual void deserialize(JsonDeserializer &, const std::string &) { };
	
	inline void setIgnoreSerialize(bool ignore) { _ignoreSerialize = ignore; }
	inline bool getIgnoreSerialize() const { return _ignoreSerialize; }
	
	inline void setGizmoIcon(Texture * tex) { _gizmoIcon = tex; }
	inline Texture * getGizmoIcon() { return _gizmoIcon.getPtr(); }
	
protected:
	virtual ~Component();
	
	SceneObject * _sceneObject;
	ptr<Texture> _gizmoIcon;
	
	bool _ignoreSerialize;
};
	
class VWGLEXPORT ComponentRegistry {
public:
	static ComponentRegistry * get() {
		if (s_singleton==nullptr) {
			s_singleton = new ComponentRegistry();
		}
		return s_singleton;
	}
	
	void registerFactory(const std::string & name, std::function<Component *()> factory) {
		_factoryMap[name] = factory;
	}
	
	Component * instantiate(const std::string & name) {
		if (_factoryMap.find(name)!=_factoryMap.end()) {
			return _factoryMap[name]();
		}
		return nullptr;
	}

protected:
	ComponentRegistry();
	~ComponentRegistry();

	static ComponentRegistry * s_singleton;
	std::unordered_map<std::string, std::function<Component*()> > _factoryMap;
};

template <class T>
class ComponentFactory {
public:
	ComponentFactory(const std::string & name) {
		ComponentRegistry::get()->registerFactory(name, []() { return new T(); });
	}
};

typedef std::unordered_map<size_t, ptr<Component> > ComponentMap;
	
}
}

#endif
