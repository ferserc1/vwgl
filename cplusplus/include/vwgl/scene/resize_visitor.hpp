
#ifndef _VWGL_SCENE_RESIZE_VISITOR_HPP_
#define	_VWGL_SCENE_RESIZE_VISITOR_HPP_

#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/math.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT ResizeVisitor : public NodeVisitor {
public:
	ResizeVisitor();
	
	inline void setSize(const Size2Di & size) { _size = size; }
	inline const Size2Di & getSize() const { return _size; }
	
	virtual void visit(scene::Node * node);

protected:
	Size2Di _size;
};

}
}

#endif
