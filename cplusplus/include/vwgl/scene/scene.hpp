
#ifndef _VWGL_SCENE_SCENE_HPP_
#define _VWGL_SCENE_SCENE_HPP_

#include <vwgl/scene/billboard.hpp>
#include <vwgl/scene/bounding_box.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/scene/chain.hpp>
#include <vwgl/scene/component.hpp>
#include <vwgl/scene/component_plugin.hpp>
#include <vwgl/scene/cube.hpp>
#include <vwgl/scene/cubemap.hpp>
#include <vwgl/scene/forward_renderer.hpp>
#include <vwgl/scene/draw_visitor.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/deferred_renderer.hpp>
#include <vwgl/scene/frame_visitor.hpp>
#include <vwgl/scene/init_visitor.hpp>
#include <vwgl/scene/joint.hpp>
#include <vwgl/scene/lifecycle.hpp>
#include <vwgl/scene/light.hpp>
#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/plane.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/primitive_factory.hpp>
#include <vwgl/scene/projector.hpp>
#include <vwgl/scene/sphere.hpp>
#include <vwgl/scene/text.hpp>
#include <vwgl/scene/transform_visitor.hpp>
#include <vwgl/scene/update_visitor.hpp>

#endif
