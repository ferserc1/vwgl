
#ifndef _VWGL_SCENE_SPHERE_HPP_
#define _VWGL_SCENE_SPHERE_HPP_

#include <vwgl/scene/drawable.hpp>

namespace vwgl {
namespace scene {


class VWGLEXPORT Sphere : public scene::DrawableFactory {
public:
	Sphere() :_radius(1.0f), _slices(20), _stacks(20) {}
	Sphere(float radius) :_radius(radius), _slices(20), _stacks(20) {}
	Sphere(float radius, int divisions) :_radius(radius), _slices(divisions), _stacks(divisions) {}
	Sphere(float radius, int slices, int stacks) :_radius(radius), _slices(slices), _stacks(stacks) {}

	virtual Drawable * getDrawable();
	
	inline void setRadius(float r) { _radius = r; }
	inline void setDivisions(int d) { _slices = d; _stacks = d; }
	inline void setDivisions(int slices, int stacks) { _slices = slices; _stacks = stacks; }
	inline void setSphere(float radius, int divisions) { _radius = radius; _slices = divisions; _stacks = divisions; }
	inline void setSphere(float radius, int slices, int stacks) { _radius = radius; _slices = slices; _stacks = stacks; }
	
protected:
	float _radius;
	int _slices;
	int _stacks;
};

}
}

#endif
