
#ifndef _VWGL_SCENE_CAMERA_HPP_
#define _VWGL_SCENE_CAMERA_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/scene/transform_visitor.hpp>
#include <vwgl/matrix_factory.hpp>

#include <list>

namespace vwgl {
namespace scene {

class VWGLEXPORT Camera : public SceneComponent {
public:
	typedef std::list<Camera*> CameraList;

	static CameraList & getCameraList();
	static Camera * getMainCamera();
	static void setMainCamera(Camera * cam);
	/*
	 *	Apply the cam's projection and view matrixes to the current vwgl::State
	 */
	static void applyTransform(Camera * cam);

	Camera();

	Matrix4 & getProjectionMatrix() {
		if (_projectionMethod.valid()) _projectionMethod->updateMatrix();
		return _projection;
	}

	Matrix4 & getViewMatrix();

	Matrix4 & getTransform();

	inline void setProjectionMethod(ProjectionFactoryMethod * method) {
		_projectionMethod = method;
		if (_projectionMethod.valid()) {
			_projectionMethod->setMatrixRef(&_projection);
		}
	}

	template <class T> T * getProjectionMethod() { return dynamic_cast<T*>(_projectionMethod.getPtr()); }
	ProjectionFactoryMethod * getProjectionMethod() { return _projectionMethod.getPtr(); }

	const vwgl::Vector3 & getDirectionVector();
	
	virtual void resize(const Size2Di & size);
	
	virtual bool serialize(vwgl::JsonSerializer & serializer, bool lastItem);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string & resourcePath);

protected:
	virtual ~Camera();

	Matrix4 _projection;
	TransformVisitor _transformVisitor;
	ptr<ProjectionFactoryMethod> _projectionMethod;
	vwgl::Vector3 _direction;
	Matrix4 _identityMatrix;

	static void registerCamera(Camera * cam);
	static void unregisterCamera(Camera * cam);

	static CameraList s_cameraList;
	static Camera * s_mainCamera;
};

}
}
#endif
