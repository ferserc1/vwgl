
#ifndef _VWGL_SCENE_BOUNDINGBOX_HPP_
#define _VWGL_SCENE_BOUNDINGBOX_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/boundingbox.hpp>

namespace vwgl {
namespace scene {

class BoundingBox : public SceneComponent, public IDrawableObserver {
public:
	BoundingBox();
	
	virtual void init();
	
	virtual void drawableChanged(vwgl::scene::Drawable * drawable);
	
	inline const Vector3 & min() const { return  _boundingBox->min(); }
	inline const Vector3 & max() const { return _boundingBox->max(); }
	inline const Vector3 & size() const { return _boundingBox->size(); }
	inline const Vector3 & halfSize() const { return _boundingBox->halfSize(); }
	inline const Vector3 & center() const { return _boundingBox->center(); }

	virtual bool serialize(vwgl::JsonSerializer & serializer, bool lastItem);
	virtual void deserialize(JsonDeserializer & deserializer, const std::string & resourcePath);

protected:
	virtual ~BoundingBox();
	
	ptr<vwgl::BoundingBox> _boundingBox;
};
	
}
}
#endif
