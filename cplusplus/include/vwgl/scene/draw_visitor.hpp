
#ifndef _VWGL_SCENE_DRAW_VISITOR_HPP_
#define _VWGL_SCENE_DRAW_VISITOR_HPP_


#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/drawable.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT DrawVisitor : public NodeVisitor {
public:
	DrawVisitor();
	
	virtual void visit(scene::Node * node);
	
	virtual void didVisit(scene::Node * node);

};

}
}


#endif
