#ifndef _VWGL_BLOOM_MAP_MATERIAL_HPP_
#define	_VWGL_BLOOM_MAP_MATERIAL_HPP_

#include <vwgl/deferredmaterial.hpp>
#include <vwgl/texture.hpp>

namespace vwgl {

class BloomMapMaterial : public Material {
public:
	
	BloomMapMaterial();
	
	inline void setLightingMap(Texture * tex) { _lightingMap = tex; }
	inline const Texture * getLightingMap() const { return _lightingMap.getPtr(); }
	inline Texture * getLightingMap() { return _lightingMap.getPtr(); }
	
	inline void setBloomAmount(int amount) { _bloomAmount = amount; }
	inline int getBloomAmount() const { return _bloomAmount; }
	
	virtual void initShader();
	
protected:
	virtual ~BloomMapMaterial();
	
	void setupUniforms();
	
	ptr<Texture> _lightingMap;
	int _bloomAmount;
};

}

#endif
