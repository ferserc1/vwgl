
#ifndef _vwgl_material_hpp_
#define _vwgl_material_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/shader.hpp>
#include <vwgl/state.hpp>

#include <string>
#include <vector>

namespace vwgl {

class PolyList;

class VWGLEXPORT Material : public ReferencedPointer {
public:
	enum PolygonMode {
	#ifdef VWGL_OPENGL_ES
		kFill = 0,
		kLine = 1,
	#else
		kFill = GL_FILL,
		kLine = GL_LINE,
	#endif
		kPoints = GL_POINTS
	};
	
	Material();
	Material(const std::string & name);

	inline void setShader(Shader * shader) { _shader = shader; }
	inline Shader * getShader() {
		if (_shader.isNull()) {
			if (_reuseShader) {
				_shader = ShaderLibrary::get()->getShader(_reuseShaderKey);
				if (!_shader.valid()) {
					_shader = new vwgl::Shader();
					ShaderLibrary::get()->addShader(_reuseShaderKey, _shader.getPtr());
				}
			}
			else {
				_shader = new vwgl::Shader();
			}
		}
		return _shader.getPtr();
	}

	void bindPolyList(PolyList * polyList);
	
	virtual Material * clone() {
		ptr<Material> newMat = new Material(_name);
		
		cloneContent(newMat.getPtr());
		
		return newMat.release();
	}
	
	// Override this function to initialize all the shader parameters
	virtual void initShader() {}
	
	virtual bool isTransparent() { return false; }
	inline bool isTessellationMaterial() { return getShader()->isTessellationShader(); }
	
	void loadVertexAttrib(const std::string & vertexAttribName);
	void loadNormalAttrib(const std::string & normalAttribName);
	void loadColorAttrib(const std::string & colorAttribName);
	void loadTexCoord0Attrib(const std::string & texCoordAttribName);
	void loadTexCoord1Attrib(const std::string & texCoordAttribName);
	void loadTexCoord2Attrib(const std::string & texCoordAttribName);
	void loadTangentArray(const std::string & tangentAttribName);

	virtual void activate();
	virtual void deactivate();
	
	void destroy();
	
	vwgl::Matrix4 & modelViewMatrix() { return vwgl::State::get()->modelViewMatrix(); }
	vwgl::Matrix4 & modelMatrix() { return vwgl::State::get()->modelMatrix(); }
	vwgl::Matrix4 & viewMatrix() { return vwgl::State::get()->viewMatrix(); }
	vwgl::Matrix4 & projectionMatrix() { return vwgl::State::get()->projectionMatrix(); }
	vwgl::Matrix4 & normalMatrix() { return vwgl::State::get()->normalMatrix(); }

	// Override this function to use the settings of the supplied material
    virtual void useSettingsOf(Material * /* tex */) {}
	
	// Generic OpenGL material properties
	inline void setPolygonMode(PolygonMode p) { _polygonMode = p; }
	inline PolygonMode getPolygonMode() const { return _polygonMode; }
	inline void setCullFace(bool h) { _cullFace = h; }
	inline bool getCullFace() const { return _cullFace; }
	inline void setLineWidth(float w) { _lineWidth = w; }
	inline float getLineWidth() const { return _lineWidth; }

protected:
	
	virtual void cloneContent(Material * newMat) {
		newMat->_polygonMode = _polygonMode;
		newMat->_cullFace = _cullFace;
		newMat->_lineWidth = _lineWidth;
		newMat->_reuseShader = _reuseShader;
		newMat->_reuseShaderKey = _reuseShaderKey;
	}
	
	// Override this function to setup the shader uniforms by calling getShader()->setUniform...()
	virtual void setupUniforms() {}
	
	virtual ~Material();

	std::string _name;
	ptr<Shader> _shader;
	PolygonMode _polygonMode;
	bool _cullFace;
	float _lineWidth;
	
	void enableAttribs();
	void disableAttribs();
	
	void enableVertexAttrib();
	void enableNormalAttrib();
	void enableColorAttrib();
	void enableTexCoord0Attrib();
	void enableTexCoord1Attrib();
	void enableTexCoord2Attrib();
	void enableTangentAttrib();
	
	void disableVertexAttrib();
	void disableNormalAttrib();
	void disableColorAttrib();
	void disableTexCoord0Attrib();
	void disableTexCoord1Attrib();
	void disableTexCoord2Attrib();
	void disableTangentAttrib();
	
	inline void setReuseShaderKey(const std::string & key) {
		_reuseShader = true;
		_reuseShaderKey = key;
	}
	
	GLint _vao;

	GLint _vertexVBO;
	GLint _normalVBO;
	GLint _colorVBO;
	GLint _texCoord0VBO;
	GLint _texCoord1VBO;
	GLint _texCoord2VBO;
	GLint _tangentVBO;
	
	GLint _vertexAttrib;
	GLint _normalAttrib;
	GLint _colorAttrib;
	GLint _texCoord0Attrib;
	GLint _texCoord1Attrib;
	GLint _texCoord2Attrib;
	GLint _tangentAttrib;
	
	bool _initialized;
	
	std::string _reuseShaderKey;
	bool _reuseShader;
	
};

typedef std::vector<ptr<Material> > MaterialVector;

}

#endif
