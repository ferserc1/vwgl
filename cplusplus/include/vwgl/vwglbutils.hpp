
#ifndef _vwgl_vwglbutils_hpp_
#define _vwgl_vwglbutils_hpp_

#include <vwgl/export.hpp>

#include <string>
#include <vector>
#include <fstream>

namespace vwgl {

class VWGLEXPORT VwglbUtils {
public:
	enum OpenMode {
		kModeClosed,
		kModeRead,
		kModeWrite
	};
	enum BlockType {
		kHeader = 'hedr',			// cabecera, seguido del número de polyLists que hay en el fichero y a continuación los materiales (bloque 'mtrl')
		kPolyList = 'plst',			// polyList
		kVertexArray = 'varr',		// Vector de vértices
		kNormalArray = 'narr',		// Vector de normales
		kTexCoord0Array = 't0ar',	// Vector de coordenadas de textura
		kTexCoord1Array = 't1ar',	// Vector de coordenadas de textura
		kTexCoord2Array = 't2ar',	// Vector de coordenadas de textura
		kTexCoord3Array = 't3ar',	// Vector de coordenadas de textura
		kTexCoord4Array = 't4ar',	// Vector de coordenadas de textura
		kIndexArray = 'indx',		// Vector de índices
		kMaterials = 'mtrl',		// Materiales
		kPlistName = 'pnam',		// Nombre del polyList
		kMatName = 'mnam',			// Nombre del material para un plist
		kShadowProjector = 'proj',	// Sombra proyectada
		kJoint = 'join',			// Junta física
		kEnd = 'endf'				// Final del fichero
	};
	VwglbUtils();
	virtual ~VwglbUtils();

	bool open(const std::string & path,OpenMode mode);
	
	void close();
	
	bool writeByte(unsigned char byte);
	bool writeBlock(BlockType b);
	bool writeInteger(int value);
	bool writeFloat(float value);
	bool writeString(const std::string & str);
	bool writeArray(const std::vector<float> array);
	bool writeArray(const std::vector<int> array);
	bool writeArray(const std::vector<unsigned int> array);
	
	bool readByte(unsigned char & byte);
	bool readBlock(BlockType & b);
	bool readInteger(int & value);
	bool readFloat(float & value);
	bool readString(std::string & str);
	bool readArray(std::vector<float> & array);
	bool readArray(std::vector<int> & array);
	bool readArray(std::vector<unsigned int> & array);
	
	void setBigEndian();
	void setLittleEndian();
	
	void seekForward(int bytes);

	std::fstream & stream() { return _stream; }
	
protected:
	std::fstream _stream;
	OpenMode _mode;
	bool _swapBytes;
};

}

#endif
