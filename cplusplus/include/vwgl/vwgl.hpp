
#ifndef _vwgl_hpp_
#define _vwgl_hpp_

#include <vwgl/opengl.hpp>
#include <vwgl/boundingbox.hpp>
#include <vwgl/cube.hpp>
#include <vwgl/cubemap.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/plane.hpp>
#include <vwgl/sphere.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/dynamic_cubemap.hpp>
#include <vwgl/forwardrenderer.hpp>
#include <vwgl/glerror.hpp>
#include <vwgl/graphics.hpp>
#include <vwgl/light.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/math.hpp>
#include <vwgl/image.hpp>
#include <vwgl/keyboard.hpp>
#include <vwgl/material.hpp>
#include <vwgl/basic_phong_mat.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/referenced_pointer.hpp>
#include <vwgl/state.hpp>
#include <vwgl/shader.hpp>
#include <vwgl/renderer.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/solid.hpp>
#include <vwgl/texture.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/fbo.hpp>
#include <vwgl/node.hpp>
#include <vwgl/group.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/node_visitor.hpp>
#include <vwgl/draw_gizmos_visitor.hpp>
#include <vwgl/dock_node.hpp>
#include <vwgl/update_visitor.hpp>
#include <vwgl/draw_node_visitor.hpp>
#include <vwgl/render_pass.hpp>
#include <vwgl/render_canvas.hpp>
#include <vwgl/node_manipulator.hpp>
#include <vwgl/multimaterial_cube.hpp>
#include <vwgl/color_picker.hpp>
#include <vwgl/scene_loader.hpp>
#include <vwgl/scene_writter.hpp>
#include <vwgl/system.hpp>
#include <vwgl/shadow_map_material.hpp>
#include <vwgl/shadow_light.hpp>
#include <vwgl/texturedeferredmaterial.hpp>
#include <vwgl/normalmapdeferredmaterial.hpp>
#include <vwgl/selectiondeferredmaterial.hpp>
#include <vwgl/wpositiondeferredmaterial.hpp>
#include <vwgl/projector.hpp>
#include <vwgl/projtexturedeferredmaterial.hpp>
#include <vwgl/deferredmixmaterial.hpp>
#include <vwgl/scenerenderer.hpp>
#include <vwgl/deferredrenderer.hpp>
#include <vwgl/dictionary_loader.hpp>
#include <vwgl/dictionary_writter.hpp>
#include <vwgl/dictionary.hpp>
#include <vwgl/sceneset.hpp>
#include <vwgl/writterplugin.hpp>
#include <vwgl/readerplugin.hpp>
#include <vwgl/mouse.hpp>
#include <vwgl/gizmo.hpp>
#include <vwgl/gizmo_manager.hpp>
#include <vwgl/plane_gizmo.hpp>
#include <vwgl/translate_gizmo.hpp>
#include <vwgl/rotate_gizmo.hpp>
#include <vwgl/scale_gizmo.hpp>
#include <vwgl/transform.hpp>
#include <vwgl/dynamic_library.hpp>
#include <vwgl/osg_plugin.hpp>
#include <vwgl/glyph.hpp>
#include <vwgl/glyph_surface.hpp>
#include <vwgl/text_font.hpp>

// API 1.0
#include <vwgl/vwgltloader.hpp>
#include <vwgl/vwglbloader.hpp>
#include <vwgl/vwglbwritter.hpp>

// API 2.0
#include <vwgl/vwglb_plugin.hpp>
#include <vwgl/collada_plugin.hpp>
#include <vwgl/obj_reader.hpp>
#include <vwgl/freetype_reader.hpp>

#include <vwgl/colladaloader.hpp>


#include <vwgl/physics/physics.hpp>

#include <vwgl/library/library.hpp>
#include <vwgl/library_reader.hpp>
#include <vwgl/library_writter.hpp>

#include <vwgl/app/app.hpp>

#include <vwgl/scene/scene.hpp>

#include <vwgl/manipulation/manipulation.hpp>

#endif
