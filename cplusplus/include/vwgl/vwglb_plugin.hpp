
#ifndef _VWGL_VWGLB_PLUGIN_HPP_
#define _VWGL_VWGLB_PLUGIN_HPP_

#include <vwgl/readerplugin.hpp>
#include <vwgl/writterplugin.hpp>
#include <vwgl/vwglb_file_reader.hpp>

namespace vwgl {

class VWGLEXPORT VWGLBPluginDelegate : public VWGLBFileReaderDelegate {
public:
	VWGLBPluginDelegate() { _fileReader.setDelegate(this); }

	// VWGLBFileReaderDelegate methods
	virtual void onError(const std::string & error);
	virtual void onWarning(const std::string & warning);

	virtual void fileVersion(const FileVersion & version);
	virtual void metadata(FileMetadata metadata, int value) { if (metadata == kNumberOfPlist) { _expectedPlist = value; } }
	virtual void metadata(FileMetadata, const std::string &) {}
	virtual void metadata(FileMetadata, bool) {}
	virtual void metadata(FileMetadata, float) {}

	virtual void polyList(PolyList * plist);

	virtual void materials(const std::string & materialsString);

protected:
	VWGLBFileReader _fileReader;

	ptr<scene::Drawable> _drawable;
};

class VWGLEXPORT VWGLBModelReader : public ReadModelPlugin, public VWGLBPluginDelegate {
public:
	VWGLBModelReader() { _fileReader.setDelegate(this); }
	
	virtual bool acceptFileType(const std::string & path);
	
	virtual vwgl::scene::Drawable * loadModel(const std::string & path);

	// Projectors and joints are only applicable to prefabs
	// Parameters: Texture * projectorTexture, const Matrix4 & projection, const Matrix4 & transform, float attenuation
	virtual void projector(Texture *, const Matrix4 &, const Matrix4 &, float) {}

	// Parameters: PolyList * plist, Texture * projectorTexture, const Matrix4 & projection, const Matrix4 & transform, float attenuation
	virtual void projector(PolyList *, Texture *, const Matrix4 &, const Matrix4 &, float) {}

	// Parameters: JointType type, const Vector3 & offset, float pitch, float roll, float yaw
	virtual void joint(JointType, const Vector3 &, float, float, float) {}

protected:
	virtual ~VWGLBModelReader() {}
};

class VWGLEXPORT VWGLBPrefabReader : public ReadPrefabPlugin, public VWGLBPluginDelegate {
public:
	VWGLBPrefabReader() { _fileReader.setDelegate(this); }
	
	virtual bool acceptFileType(const std::string & path);
	
	virtual vwgl::scene::Node * loadPrefab(const std::string & path);
	
	// VWGLBFileReaderDelegate methods
	virtual void projector(Texture * projectorTexture, const Matrix4 & projection, const Matrix4 & transform, float attenuation);
	virtual void projector(PolyList * plist, Texture * projectorTexture, const Matrix4 & projection, const Matrix4 & transform, float attenuation);

	virtual void joint(JointType type, const Vector3 & offset, float pitch, float roll, float yaw);
	
protected:
	virtual ~VWGLBPrefabReader() {}
	
	ptr<scene::Node> _prefab;
};


class VWGLEXPORT VWGLBWritter {
public:
	VWGLBWritter() {}

protected:

	bool writeData(const std::string & path, scene::Drawable * model);
	bool writeData(const std::string & path, scene::Node * node);

	void writeHeader(scene::Drawable * drawable);
	void writeHeader(scene::Node * node);
	void ensurePolyListNames(scene::Drawable * drawable);
	void writeMaterial(scene::Drawable * drawable, scene::Node * node = nullptr);
	void writeShadow(scene::Node * node);
	void writeJoints(scene::Node * node);
	void writePolyList(PolyList * plist);
	void saveTextureFile(const std::string & imagePath);

	VwglbUtils _fileUtils;
	std::string _dstPath;
};

class VWGLEXPORT VWGLBModelWritter : public WriteModelPlugin, public VWGLBWritter {
public:
	virtual bool acceptFileType(const std::string & path);

	virtual bool writeModel(const std::string & path, vwgl::scene::Drawable * drawable);
};

class VWGLEXPORT VWGLBPrefabWritter : public WritePrefabPlugin, public VWGLBWritter {
public:
	virtual bool acceptFileType(const std::string & path);

	virtual bool writePrefab(const std::string & path, vwgl::scene::Node * node);
};

}


#endif
