
#ifndef _VWGL_MANIPULATION_DRAW_GIZMO_VISITOR_HPP_
#define _VWGL_MANIPULATION_DRAW_GIZMO_VISITOR_HPP_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/material.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/manipulation/picker_identifier.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/manipulation/gizmo.hpp>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT GizmoMaterial : public Material {
public:
	GizmoMaterial();
	
	inline void setTexture(Texture * tex) { _texture = tex; }
	inline void setPickerIdentifier(const PickerIdentifier &id) { _pickerId = id; }

	virtual void initShader();
	
	inline void setScale(float s) { _scale = s; }
	inline float getScale() const { return _scale; }

protected:
	virtual ~GizmoMaterial();
	
	virtual void setupUniforms();
	
	Texture * _texture;
	PickerIdentifier _pickerId;
	float _scale;
	
	static std::string s_vshader;
	static std::string s_fshader;
	static std::string s_vshader_gl3;
	static std::string s_fshader_gl3;
};
	
class VWGLEXPORT GizmoPlistMaterial : public Material {
public:
	GizmoPlistMaterial();
	
	inline void setColor(const Color & c) { _color = c; }
	
	virtual void initShader();
	
	inline void setScale(float s) { _scale = s; }
	inline float getScale() const { return _scale; }
	
protected:
	virtual ~GizmoPlistMaterial();
	
	virtual void setupUniforms();
	
	Color _color;
	float _scale;
	
	static std::string s_vshader;
	static std::string s_fshader;
	static std::string s_vshader_gl3;
	static std::string s_fshader_gl3;
};

class VWGLEXPORT DrawGizmosVisitor : public scene::NodeVisitor {
public:
	DrawGizmosVisitor();

	virtual void visit(scene::Node * node);

	virtual void didVisit(scene::Node * node);
	
	static void clean() { s_iconRect = nullptr; s_material = nullptr; }
	
	static vwgl::PolyList * getIconRect();
	static GizmoMaterial * getMaterial();
	static GizmoPlistMaterial * getPlistMaterial();
	
	inline void setIconPass() { _manipulatorPass = false; }
	inline void setManipulatorPass() { _manipulatorPass = true; }
	
	inline void setManipulateObject(scene::SceneObject * n) {
		// The gizmos can only manipulate objects with a Transform component
		if (n && n->getComponent<scene::Transform>()) {
			_manipulateNode = n;
		}
		else {
			_manipulateNode = nullptr;
		}
	}
	inline void setManipulateGizmo(manipulation::Gizmo * g) { _manipulateGizmo = g; }
	inline scene::SceneObject * currentObject() { return _manipulateNode.valid() ? _manipulateNode.getPtr():nullptr; }
	inline scene::Transform * currentTransform() { return currentObject() ? currentObject()->getComponent<scene::Transform>():nullptr; }
	inline manipulation::Gizmo * currentGizmo() { return _manipulateGizmo.getPtr(); }
	
protected:
	ptr<scene::SceneObject> _manipulateNode;
	ptr<manipulation::Gizmo> _manipulateGizmo;
	bool _manipulatorPass;

	static vwgl::ptr<vwgl::PolyList> s_iconRect;
	static vwgl::ptr<GizmoMaterial> s_material;
	static vwgl::ptr<GizmoPlistMaterial> s_plistMaterial;
};

}
}

#endif
