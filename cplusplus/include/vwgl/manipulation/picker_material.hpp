#ifndef _VWGL_MANIPULATION_PICKER_MATERIAL_HPP_
#define _VWGL_MANIPULATION_PICKER_MATERIAL_HPP_

#include <vwgl/material.hpp>
#include <vwgl/manipulation/picker_identifier.hpp>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT PickerMaterial : public Material {
public:
	PickerMaterial() :Material(), _pickerIdentifier(nullptr) { _reuseShaderKey = "manipulation::PickerMaterial"; initShader(); }
	
	virtual void initShader();
	
	void setPickerIdentifier(PickerIdentifier * id) { _pickerIdentifier = id; }

protected:
	virtual void setupUniforms();
	
	virtual ~PickerMaterial() {}
	
	PickerIdentifier * _pickerIdentifier;
		
	static std::string _vshader;
	static std::string _fshader;
	static std::string _vshader_gl3;
	static std::string _fshader_gl3;
};

}
}
#endif
