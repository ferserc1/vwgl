
#ifndef _VWGL_MANIPULATION_SELECTION_HANDLER_HPP_
#define _VWGL_MANIPULATION_SELECTION_HANDLER_HPP_

#include <vwgl/export.hpp>
#include <vwgl/manipulation/selectable.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/generic_material.hpp>

#include <list>
#include <vector>
#include <functional>

namespace vwgl {
namespace manipulation {

class SelectionHandler;
class ISelectionObserver {
public:
	virtual void selectionChanged(SelectionHandler & sender) = 0;
};

class VWGLEXPORT SelectionHandler {
public:
	typedef std::list<ptr<scene::Node> > NodeList;
	typedef std::list<ptr<scene::Drawable> > DrawableList;
	typedef std::list<ptr<PolyList> > PolyListList;
	typedef std::list<ptr<Material> > MaterialList;
	typedef std::list<ISelectionObserver*> ObserverList;
	
	SelectionHandler();
	
	inline void setMarkMaterials(bool mark) { _markMaterial = mark; }

	inline void clearNode() { _nodeList.clear(); notifyObservers(); }
	inline void clearDrawable() { _drawableList.clear(); notifyObservers(); }
	inline void clearPolyList() { _polyListList.clear(); notifyObservers(); }
	inline void clearMaterial() { unmarkMaterials(); _materialList.clear(); notifyObservers(); }
	
	inline void clearSelection() {
		unmarkMaterials();
		_nodeList.clear();
		_drawableList.clear();
		_polyListList.clear();
		_materialList.clear();
		notifyObservers();
	}

	inline void setAdditiveSelection(bool a) { _additiveSelection = a; }
	inline bool isAdditiveSelection() const { return _additiveSelection; }
	void selectNode(scene::Node * node, bool forceSelect = false);
	void selectDrawable(scene::Drawable * drw, bool forceSelect = false);
	void selectPolyList(PolyList * plist);
	void selectMaterial(Material * mat);
	void select(scene::Node * node, scene::Drawable * drw, PolyList * plist, Material * mat);
	
	NodeList & getNodeList() { return _nodeList; }
	DrawableList & getDrawableList() { return _drawableList; }
	const DrawableList & getDrawableList() const { return _drawableList; }
	PolyListList & getPolyListList() { return _polyListList; }
	const PolyListList & getPolyListList() const { return _polyListList; }
	MaterialList & getMaterialList() { return _materialList; }
	const MaterialList & getMaterialList() const { return _materialList; }
	
	inline int numberOfSelectedNodes() const { return static_cast<int>(_nodeList.size()); }
	inline void eachNode(std::function<void(scene::Node*)> callback) {
		NodeList::iterator it;
		for (it=_nodeList.begin(); it!=_nodeList.end(); ++it) {
			callback((*it).getPtr());
		}
	}
	
	inline int numberOfSelectedDrawables() const { return static_cast<int>(_drawableList.size()); }
	inline void eachDrawable(std::function<void(scene::Drawable*)> callback) {
		DrawableList::iterator it;
		for (it=_drawableList.begin(); it!=_drawableList.end(); ++it) {
			callback((*it).getPtr());
		}
	}
	
	inline int numberOfSelectedPolyList() const { return static_cast<int>(_polyListList.size()); }
	inline void eachPolyList(std::function<void(PolyList*)> callback) {
		PolyListList::iterator it;
		for (it=_polyListList.begin(); it!=_polyListList.end(); ++it) {
			callback((*it).getPtr());
		}
	}
	
	inline int numberOfSelectedMaterials() const { return static_cast<int>(_materialList.size()); }
	inline void eachMaterial(std::function<void(Material*)> callback) {
		MaterialList::iterator it;
		for (it=_materialList.begin(); it!=_materialList.end(); ++it) {
			callback((*it).getPtr());
		}
	}
	
	void notifyObservers();
	
	void registerObserver(ISelectionObserver * observer);
	void unregisterObserver(ISelectionObserver * observer);
	inline void lockNotifications() { _lockNotifications = true; }
	inline void unlockNotifications() { _lockNotifications = false; }
	inline bool notificationsLocked() const { return _lockNotifications; }
	
protected:
	NodeList _nodeList;
	DrawableList _drawableList;
	PolyListList _polyListList;
	MaterialList _materialList;
	ObserverList _observers;
	bool _additiveSelection;
	
	bool _markMaterial;
	bool _lockNotifications;
	
	void markMaterials();
	void unmarkMaterials();
};

}
}

#endif
