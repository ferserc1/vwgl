
#ifndef _VWGL_MANIPULATION_PICK_VISITOR_HPP_
#define _VWGL_MANIPULATION_PICK_VISITOR_HPP_

#include <vwgl/scene/node_visitor.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/manipulation/selectable.hpp>
#include <vwgl/polylist.hpp>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT PickVisitor : public scene::NodeVisitor {
public:
	virtual void visit(scene::Node * node);
	
	inline void clear() { _selectable = nullptr; _polyList = nullptr; }
	inline Selectable * getSelectable() { return _selectable.getPtr(); }
	inline PolyList * getPolyList() { return _polyList.getPtr(); }
	
	inline void setTarget(const PickerIdentifier & id) { _targetIdentifier = id; }

protected:
	ptr<Selectable> _selectable;
	ptr<PolyList> _polyList;
	PickerIdentifier _targetIdentifier;
};

}
}
#endif
