//
//  transform_controller.hpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 18/2/15.
//  Copyright (c) 2015 Vitaminew CB. All rights reserved.
//

#ifndef _VWGL_MANIPULATION_TRANSFORM_CONTROLLER_HPP_
#define _VWGL_MANIPULATION_TRANSFORM_CONTROLLER_HPP_

#include <vwgl/scene/scene_component.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/scene/transform.hpp>

namespace vwgl {
namespace manipulation {

	
class VWGLEXPORT TargetInputController : public scene::SceneComponent {
public:
	TargetInputController()
		:_distance(5.0f), _rotationSpeed(0.2f), _flyMode(true),
		_forward(0.0f), _left(0.0f), _movementVelocity(0.1f),
		_forwardKey(Keyboard::kKeyUp), _backwardKey(Keyboard::kKeyDown),
		_leftKey(Keyboard::kKeyLeft), _rightKey(Keyboard::kKeyRight)
	{}
	
	inline void setRotationSpeed(float s) { _rotationSpeed = s; }
	inline float getRotationSpeed() const { return _rotationSpeed; }

	inline void setFlyMode(bool m) { _flyMode = m; }
	inline bool getFlyMode() const { return _flyMode; }
	
	
	inline void setForwardKey(Keyboard::KeyCode code) { _forwardKey = code; }
	inline void setBackwardKey(Keyboard::KeyCode code) { _backwardKey = code; }
	inline void setLeftKey(Keyboard::KeyCode code) { _leftKey = code; }
	inline void setRightKey(Keyboard::KeyCode code) { _rightKey = code; }
	inline Keyboard::KeyCode getForwardKey() const { return _forwardKey; }
	inline Keyboard::KeyCode getBackwardKey() const { return _backwardKey; }
	inline Keyboard::KeyCode getLeftKey() const { return _leftKey; }
	inline Keyboard::KeyCode getRightKey() const { return _rightKey; }

	virtual void update();
	virtual void mouseDown(const app::MouseEvent & evt);
	virtual void mouseDrag(const app::MouseEvent & evt);
	virtual void mouseWheel(const app::MouseEvent & evt);
	virtual void keyDown(const app::KeyboardEvent & evt);
	virtual void keyUp(const app::KeyboardEvent & evt);
	
	inline const Vector3 & center() const { return _center; }
	inline void setCenter(const Vector3 & c) { _center = c; }
	inline const Vector2 & rotation() const { return _rotation; }
	inline void setRotation(const Vector2 & r) { _rotation = r; }
	inline float distance() const { return _distance; }
	inline void setDistance(float d) { _distance = d; }
	

protected:
	Position2Di _lastPosition;
	Vector2 _rotation;
	float _distance;
	Vector3 _center;
	float _rotationSpeed;
	
	bool _flyMode;
	float _forward;
	float _left;
	float _movementVelocity;
	Keyboard::KeyCode _forwardKey;
	Keyboard::KeyCode _backwardKey;
	Keyboard::KeyCode _leftKey;
	Keyboard::KeyCode _rightKey;
};

}
}

#endif
