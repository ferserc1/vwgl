
#ifndef _VWGL_MANIPULATION_TRANSLATE_GIZMO_HPP_
#define _VWGL_MANIPULATION_TRANSLATE_GIZMO_HPP_

#include <vwgl/manipulation/gizmo.hpp>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT TranslateGizmo : public Gizmo {
public:
	TranslateGizmo();

	virtual void draw();
	
	virtual void beginAction();
	virtual void offsetChanged(unsigned int axis, const Vector2i & offset);
	virtual void commit();
	virtual void discard();

protected:
	virtual ~TranslateGizmo();
	
	Vector3 _restorePosition;
	Matrix4 _restoreMatrix;
	Vector3 _translate;
};

}
}

#endif
