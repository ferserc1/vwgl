
#ifndef _VWGL_MANIPULATION_ROTATE_GIZMO_HPP_
#define _VWGL_MANIPULATION_ROTATE_GIZMO_HPP_

#include <vwgl/manipulation/gizmo.hpp>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT RotateGizmo : public Gizmo {
public:
	RotateGizmo();

	virtual void draw();

	virtual void beginAction();
	virtual void offsetChanged(unsigned int axis, const Vector2i & offset);
	virtual void commit();
	virtual void discard();
	
protected:
	virtual ~RotateGizmo();
	
	Matrix4 _previewRotation;
	Vector3 _rotate;
	Vector3 _restoreRotate;
	Matrix4 _restoreMatrix;
};

}
}

#endif
