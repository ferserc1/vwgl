#ifndef _VWGL_MANIPULATION_PICKER_IDENTIFIER_HPP_
#define _VWGL_MANIPULATION_PICKER_IDENTIFIER_HPP_

#include <vwgl/math.hpp>
#include <vwgl/polylist.hpp>

#include <vector>

namespace vwgl {
namespace manipulation {

class PickerIdentifier {
public:
	PickerIdentifier() :_r(0), _g(0), _b(0), _a(0), _polyList(nullptr) { }
	PickerIdentifier(unsigned char r, unsigned char g, unsigned char b, unsigned char a) :_r(r), _g(g), _b(b), _a(a), _polyList(nullptr) { }
	PickerIdentifier(PolyList * plist) :_r(0), _g(0), _b(0), _a(0),  _polyList(plist) { }
	PickerIdentifier(PolyList * plist, unsigned char r, unsigned char g, unsigned char b, unsigned char a) :_r(r), _g(g), _b(b), _a(a), _polyList(plist) { }
	
	// A note about the PickerIdentifier hash: keep in mind that, depending on the platform endianess, the hash code may be different. The hash code is not platform
	// independent, to share the PickerIdentifier value between platforms, use the r(), g() and b() components
	
	inline unsigned char r() const { return _r; }
	inline unsigned char g() const { return _g; }
	inline unsigned char b() const { return _b; }
	inline unsigned char a() const { return _a; }
	inline void set(unsigned char r, unsigned char g, unsigned char b, unsigned char a) { _r = r; _g = g; _b = b; _a = a; }
	inline void setPolyList(PolyList * plist) { _polyList = plist; }
	inline PolyList * getPolyList() { return _polyList.getPtr(); }
	inline const PolyList * getPolyList() const { return _polyList.getPtr(); }
	
	void operator=(const PickerIdentifier & other) {
		_r = other._r;
		_g = other._g;
		_b = other._b;
		_a = other._a;
	}
	
	bool operator==(const PickerIdentifier & other) {
		return _r==other._r && _g==other._g && _b==other._b && _a==other._a;
	}
	
	bool isZero() const {
		return _r==0 && _g==0 && _b==0 && _a==0;
	}
	
	std::string toString() {
		std::stringstream str;
		str << (int)_r << ":" << (int)_g << ":" << (int)_b << ":" << (int)_a;
		return str.str();
	}

protected:
	unsigned char _r;
	unsigned char _g;
	unsigned char _b;
	unsigned char _a;
	
	ptr<PolyList> _polyList;
};

typedef std::vector<PickerIdentifier> PickerIdentifierVector;


}
}

#endif
