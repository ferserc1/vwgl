
#ifndef _VWGL_MANIPULATION_SCALE_GIZMO_HPP_
#define _VWGL_MANIPULATION_SCALE_GIZMO_HPP_

#include <vwgl/manipulation/gizmo.hpp>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT ScaleGizmo : public Gizmo {
public:
	ScaleGizmo();
	
	virtual void draw();

	virtual void beginAction();
	virtual void offsetChanged(unsigned int axis, const Vector2i & offset);
	virtual void commit();
	virtual void discard();
	
protected:
	virtual ~ScaleGizmo();
	
	Vector3 _scaleVector;
	Vector3 _restoreScale;
	Matrix4 _restoreMatrix;
};

}
}

#endif
