
#ifndef _VWGL_MANIPULATION_MOUSE_PICKER_HPP_
#define _VWGL_MANIPULATION_MOUSE_PICKER_HPP_

#include <vwgl/math.hpp>
#include <vwgl/fbo.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/draw_visitor.hpp>
#include <vwgl/manipulation/selectable.hpp>
#include <vwgl/manipulation/pick_visitor.hpp>
#include <vwgl/material.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/manipulation/gizmo_manager.hpp>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT MousePicker {
public:
	MousePicker();
	MousePicker(scene::Node * sceneRoot);

	inline void setSceneRoot(scene::Node * sr) { _sceneRoot = sr; }
	inline scene::Node * getSceneRoot() { return _sceneRoot.getPtr(); }
	inline const scene::Node * getSceneRoot() const { return _sceneRoot.getPtr(); }

	// Set the gizmo manager to allow manipulation gizmo selection. The manipulation gizmo 
	inline void setGizmoManager(GizmoManager & mgr) { _gizmoManager = &mgr; }

	inline void setInvertedY(bool inverted) { _invertedY = inverted; }
	inline bool isInvertedY() const { return _invertedY; }

	bool gizmo(const Position2Di & mousePosition, const Viewport & viewport);
	bool pick(const Position2Di & mousePosition, const Viewport & viewport);
	
	// Query functions
	inline Selectable * getSelectable() { return _selectableResult.getPtr(); }
	inline scene::Node * getPickedNode() { return _selectableResult.valid() ? _selectableResult->node():nullptr; }
	inline scene::Drawable * getPickedDrawable() {
		if (_selectableResult.valid() && _selectableResult->sceneObject()) {
			return _selectableResult->sceneObject()->getComponent<scene::Drawable>();
		}
		return nullptr;
	}
	inline Material * getPickedMaterial() {
		scene::Drawable * drw = nullptr;
		if ((drw=getPickedDrawable()) && _polyListResult.valid()) {
			try {
				return drw->getMaterial(_polyListResult.getPtr());
			}
			catch (const scene::DrawableElementNotFoundException &) {}
		}
		return nullptr;
	}
	inline GenericMaterial * getPickedGenericMaterial() { return dynamic_cast<GenericMaterial*>(getPickedMaterial()); }
	inline PolyList * getPickedPolyList() { return _polyListResult.getPtr(); }
	inline manipulation::Gizmo * getPickedGizmo() { return _pickedGizmo.getPtr(); }
	inline PolyList * getPickedGizmoPolyList() { return _pickedGizmoPlist.getPtr(); }

protected:
	ptr<scene::Node> _sceneRoot;

	void createCanvas(const Viewport & vp);
	void resizeCanvas(const Viewport & vp);

	void beginPick(const Position2Di & mousePosition, const Viewport & viewport);
	void endPick();
	void getColor(const Position2Di & mousePosition, const Viewport & viewport, unsigned char rgba[4]);

	GizmoManager * _gizmoManager;
	ptr<FramebufferObject> _fbo;
	scene::DrawVisitor _drawVisitor;
	PickVisitor _pickVisitor;
	ptr<Selectable> _selectableResult;
	ptr<PolyList> _polyListResult;
	bool _invertedY;
	Viewport _restoreViewpor;
	ptr<manipulation::Gizmo> _pickedGizmo;
	ptr<PolyList> _pickedGizmoPlist;
};

}
}

#endif

