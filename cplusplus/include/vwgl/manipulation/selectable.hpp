
#ifndef _VWGL_MANIPULATION_SELECTABLE_HPP_
#define _VWGL_MANIPULATION_SELECTABLE_HPP_

#include <vwgl/scene/component.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/manipulation/picker_identifier.hpp>
#include <vwgl/manipulation/picker_material.hpp>

#include <vector>
#include <functional>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT Selectable : public scene::Component, public scene::IDrawableObserver {
public:
	
	virtual void init();
	virtual void draw();

	virtual void drawableChanged(scene::Drawable * drawable);

	static void enablePickMode() { s_doPick = true; }
	static void disablePickMode() { s_doPick = false; }
	
	void eachIdentifier(std::function<bool(PickerIdentifier&)> callback) {
		PickerIdentifierVector::iterator it;
		callback(_pickerIdentifier);
		for (it=_pickerIdVector.begin(); it!=_pickerIdVector.end(); ++it) {
			if (callback(*it)) break;
		}
	}

protected:
	virtual ~Selectable();

	void updatePolyListRefs();
	void clearPolyListRefs();
	void assignId(PickerIdentifier & id);
	void addPolyList(PolyList * plist);
	
	PickerIdentifierVector _pickerIdVector;
	ptr<PickerMaterial> _pickerMaterial;
	// This is the identifier assigned to this component. This identifier is used to allow selection of
	// nodes that aren't drawables (a light, for instance) through the gizmo icon
	PickerIdentifier _pickerIdentifier;

	static bool s_doPick;
	
	static unsigned char s_r;
	static unsigned char s_g;
	static unsigned char s_b;
	static unsigned char s_a;
};

}
}

#endif
