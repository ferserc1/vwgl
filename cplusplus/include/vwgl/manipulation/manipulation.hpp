
#ifndef _VWGL_MANIPULATION_HPP_
#define _VWGL_MANIPULATION_HPP_

#include <vwgl/manipulation/draw_gizmo_visitor.hpp>
#include <vwgl/manipulation/gizmo_manager.hpp>
#include <vwgl/manipulation/mouse_picker.hpp>
#include <vwgl/manipulation/selectable.hpp>
#include <vwgl/manipulation/selection_handler.hpp>
#include <vwgl/manipulation/transform_controller.hpp>

#include <vwgl/manipulation/translate_gizmo.hpp>
#include <vwgl/manipulation/rotate_gizmo.hpp>
#include <vwgl/manipulation/scale_gizmo.hpp>

#endif
