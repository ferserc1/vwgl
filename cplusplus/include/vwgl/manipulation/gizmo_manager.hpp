#ifndef _VWGL_MANIPULATION_GIZMO_MANAGER_HPP_
#define _VWGL_MANIPULATION_GIZMO_MANAGER_HPP_

#include <vwgl/manipulation/draw_gizmo_visitor.hpp>
#include <vwgl/manipulation/gizmo.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/texture.hpp>

#include <unordered_map>
#include <algorithm>

namespace vwgl {
namespace manipulation {

class GizmoManager;
class IGizmoObserver {
public:
	virtual void gizmoPicked(GizmoManager &) {}
	virtual void gizmoMoved(GizmoManager &) {}
	virtual void gizmoReleased(GizmoManager &) {}
	virtual void currentGizmoChanged(GizmoManager &) {}
};

typedef std::vector<IGizmoObserver*> GizmoObserverVector;

class VWGLEXPORT GizmoManager : public scene::NodeVisitor {
public:
	enum GizmoIcon {
		kIconLightSpot			= 1 << 0,
		kIconLightPoint			= 1 << 1,
		kIconLightDirectional	= 1 << 2,
		kIconLightDisabled		= 1 << 3,
		kIconCamera				= 1 << 4,
		kIconTransform			= 1 << 5,
		kIconDrawable			= 1 << 6,
		kIconJoint				= 1 << 7
	};

	GizmoManager();
	GizmoManager(scene::Node * root);

	inline void registerObserver(IGizmoObserver * observer) {
		GizmoObserverVector::iterator it = std::find(_observerVector.begin(), _observerVector.end(), observer);
		if (it == _observerVector.end()) {
			_observerVector.push_back(observer);
		}
	}

	inline void unregisterObserver(IGizmoObserver * observer) {
		GizmoObserverVector::iterator it = std::find(_observerVector.begin(), _observerVector.end(), observer);
		if (it != _observerVector.end()) {
			_observerVector.erase(it);
		}
	}

	inline void setSceneRoot(scene::Node * root) { _sceneRoot = root; }
	
	void draw();

	void destroy();
	
	virtual void visit(scene::Node * node);
	
	inline void setIcon(GizmoIcon icon, Texture * tex) {
		if (tex) {
			_icons[icon] = tex;
		}
	}
	
	inline Texture * getIcon(GizmoIcon icon) {
		return _visibleGizmos & icon ? _icons[icon].getPtr():nullptr;
	}
	
	inline void setPolyList(GizmoIcon icon, PolyList * plist) {
		if (plist) {
			_plist[icon] = plist;
		}
	}
	
	inline PolyList * getPolyList(GizmoIcon icon) {
		return _visibleGizmos & icon ? _plist[icon].getPtr():nullptr;
	}
	
	inline void setVisibleGizmos(unsigned int iconMask) { _visibleGizmos = iconMask; }
	inline void showGizmo(GizmoIcon icon) { _visibleGizmos = _visibleGizmos | icon; }
	inline void hideGizmo(GizmoIcon icon) { _visibleGizmos = _visibleGizmos & ~icon; }
	inline unsigned int getGizmoVisibilityMask() const { return _visibleGizmos; }
	
	inline void setManipulateObject(scene::SceneObject * n) { _drawGizmoVisitor.setManipulateObject(n); }
	inline void setManipulateGizmo(manipulation::Gizmo * gizmo) {
		_drawGizmoVisitor.setManipulateGizmo(gizmo);
		eachObserver([&](IGizmoObserver * observer) {
			observer->currentGizmoChanged(*this);
		});
	}
	inline manipulation::Gizmo * currentGizmo() { return _drawGizmoVisitor.currentGizmo(); }


	void initDefaultIcons();
	void initDefaultPlist();

	inline DrawGizmosVisitor & drawGizmosVisitor() { return _drawGizmoVisitor; }
	
	// Manipulation gizmo control functions
	void mouseDown(PolyList * gizmoPlist, const app::MouseEvent & evt);
	bool mouseDrag(const app::MouseEvent & evt);
	bool mouseUp(const app::MouseEvent & evt);

protected:
	GizmoObserverVector _observerVector;

	DrawGizmosVisitor _drawGizmoVisitor;
	ptr<scene::Node> _sceneRoot;
	
	unsigned int _visibleGizmos;
	std::unordered_map<int,ptr<Texture> > _icons;
	std::unordered_map<int,ptr<PolyList> > _plist;
	ptr<PolyList> _gizmoPlist;
	Vector2i _lastMousePosition;
	unsigned int _currentAxis;

	void eachObserver(std::function<void(IGizmoObserver*)> closure) {
		GizmoObserverVector::iterator it;
		for (it = _observerVector.begin(); it!=_observerVector.end(); ++it) {
			closure(*it);
		}
	}
};

}
}

#endif
