
#ifndef _VWGL_MANIPULATION_GIZMO_HPP_
#define _VWGL_MANIPULATION_GIZMO_HPP_

#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/manipulation/selectable.hpp>
#include <vwgl/app/command_manager.hpp>

#include <unordered_map>

namespace vwgl {
namespace manipulation {

class VWGLEXPORT Gizmo : public scene::SceneObject {
public:
	static void setCommandManager(app::CommandManager & mgr) {
		s_commandManager = &mgr;
	}

	enum Axis {
		kAxisX = 1 << 0,
		kAxisY = 1 << 1,
		kAxisZ = 1 << 2
	};

	Gizmo();

	void loadDrawable(scene::Drawable * drw);
	scene::Drawable * drawable() { return getComponent<scene::Drawable>(); }
	manipulation::Selectable * selectable() { return getComponent<manipulation::Selectable>(); }

	inline void setScale(float scale) { _scale = scale; }
	inline float getScale() const { return _scale; }

	inline void setTarget(scene::Transform * trx) { _target = trx; }
	inline scene::Transform * getTarget() { return _target.getPtr(); }

	app::CommandManager * getCommandManager() { return s_commandManager; }

	virtual void beginAction() = 0;
	virtual void offsetChanged(unsigned int axis, const Vector2i & offset) = 0;
	virtual void commit() = 0;
	virtual void discard() = 0;

protected:
	virtual ~Gizmo();
	
	float _scale;
	bool _initialized;
	ptr<scene::Transform> _target;
	static app::CommandManager * s_commandManager;
};

}
}
#endif
