
#ifndef _vwgl_normalmapdeferredmaterial_hpp_
#define _vwgl_normalmapdeferredmaterial_hpp_

#include <vwgl/deferredmaterial.hpp>

namespace vwgl {

class VWGLEXPORT NormalMapDeferredMaterial : public DeferredMaterial {
public:
	NormalMapDeferredMaterial();

	virtual void initShader();

protected:
	virtual ~NormalMapDeferredMaterial();
	
	virtual void setupUniforms();
};

}

#endif
