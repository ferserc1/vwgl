
#ifndef _vwgl_colladautils_hpp_
#define _vwgl_colladautils_hpp_

#include <vwgl/export.hpp>

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/vector.hpp>
#include <vwgl/matrix.hpp>

#include <stdlib.h>

#include <string>
#include <vector>
#include <map>



namespace vwgl {
namespace collada {


class Node : public ReferencedPointer {
public:
	Node(const std::string & tag) :_tag(tag) {}

	const std::string & getTag() const { return _tag; }
	
	inline void setId(const std::string & id) { _id=id; }
	inline void setName(const std::string & name) { _name=name; }
	inline const std::string & getId() const { return _id; }
	inline const std::string & getName() const { return _name; }
	inline void setType(const std::string & type) { _type = type; }
	inline const std::string & getType() const { return _type; }
	inline void setSource(const std::string & s) { _source = s; }
	inline const std::string & getSource() const { return _source; }
	inline void setCount(int c) { _count = c; }
	inline void setCount(const std::string & c) { _count = atoi(c.c_str()); }
	inline int getCount() const { return _count; }
	inline void setStride(int s) { _stride = s; }
	inline void setStride(const std::string & s) { _stride = atoi(s.c_str()); }
	inline int getStride() const { return _stride; }
	inline void setOffset(int o) { _offset = o; }
	inline void setOffset(const std::string & o) { _offset = atoi(o.c_str()); }
	inline int getOffset() const { return _offset; }
	inline void setSet(int s) { _set = s; }
	inline void setSet(const std::string & s) { _set = atoi(s.c_str()); }
	inline int getSet() const { return _set; }

protected:
	virtual ~Node() {}

	std::string _tag;
	std::string _id;
	std::string _name;
	std::string _type;
	std::string _source;
	int _count;
	int _stride;
	int _offset;
	int _set;
};

class VWGLEXPORT LibraryGeometries;
class VWGLEXPORT LibraryVisualScenes;
class Asset;
class Collada : public Node {
public:
	Collada();
	
	inline void setAsset(Asset * asset) { _asset = asset; }
	inline Asset * getAsset() { return _asset.getPtr(); }

	inline void setLibraryGeometries(LibraryGeometries * libGeo) { _libGeometries = libGeo; }
	inline LibraryGeometries * getLibraryGeometries() { return _libGeometries.getPtr(); }
	
	inline void setLibraryVisualScenes(LibraryVisualScenes * libVisualScenes) { _libVisualScenes = libVisualScenes; }
	inline LibraryVisualScenes * getLibraryVisualScenes() { return _libVisualScenes.getPtr(); }

protected:
	virtual ~Collada();
	
	ptr<Asset> _asset;
	ptr<LibraryGeometries> _libGeometries;
	ptr<LibraryVisualScenes> _libVisualScenes;
};

class Library : public Node {
public:
	Library(const std::string & tag) :Node(tag) {}

protected:
	virtual ~Library() {}
};

class UpAxis : public Node {
public:
	UpAxis() : Node("up_axis") {}

	inline void setAxis(const std::string & axis) { _axis = axis; }
	inline std::string & getAxis() { return _axis; }
	inline const std::string & getAxis() const { return _axis; }
	
protected:
	virtual ~UpAxis() {}
	
	std::string _axis;
};

class Unit : public Node {
public:
	Unit() :Node("unit") {}
	
	inline void setMeter(float m) { _meter = m; }
	inline void setMeter(const std::string & m) { _meter = static_cast<float>(atof(m.c_str())); }
	inline float getMeter() const { return _meter; }

protected:
	virtual ~Unit() {}
	
	float _meter;
};

class Asset : public Node {
public:
	Asset() :Node("asset") {}

	void setUpAxis(UpAxis * ua) { _upAxis = ua; }
	UpAxis * getUpAxis() { return _upAxis.getPtr(); }

	void setUnit(Unit * u) { _unit = u; }
	Unit * getUnit() { return _unit.getPtr(); }

protected:
	virtual ~Asset() {}
	
	ptr<UpAxis> _upAxis;
	ptr<Unit> _unit;
};

class VWGLEXPORT FloatArray : public Node {
public:
	FloatArray() :Node("float_array") {}
	
	void setArray(const std::string & array);
	const std::vector<float> & getArray() { return _array; }

protected:
	virtual ~FloatArray() {}
	std::vector<float> _array;
};

class Param : public Node {
public:
	Param() :Node("param") {}
	
protected:
	virtual ~Param() {}
};
typedef std::vector<ptr<Param> > ParamList;

class Accessor : public Node {
public:
	Accessor() :Node("accessor") {}
	
	void addParam(Param * p) { _paramList.push_back(p); }
	ParamList & getParams() { return _paramList; }
	
protected:
	virtual ~Accessor() {}
	
	ParamList _paramList;
};

class TechniqueCommon : public Node {
public:
	TechniqueCommon() :Node("technique_common") {}
	
	void setAccessor(Accessor * acc) { _accessor = acc; }
	Accessor * getAccessor() { return _accessor.getPtr(); }

protected:
	virtual ~TechniqueCommon() {}
	
	ptr<Accessor> _accessor;
};

class VWGLEXPORT Source : public Node {
public:
	enum Role {
		kVertex = 0,
		kNormal = 1,
		kTexCoord0 = 2,
		kTexCoord1 = 3,
		kTexCoord2 = 4,
		kTexCoord3 = 5,
		kUnknown = 100
	};
	Source() :Node("source") {}

	void setFloatArray(FloatArray * fa) { _floatArray = fa; }
	FloatArray * getFloatArray() { return _floatArray.getPtr(); }
	
	void setTechniqueCommon(TechniqueCommon * t) { _tc = t; }
	TechniqueCommon * getTechniqueCommon() { return _tc.getPtr(); }

	// Source utilities
	inline void setRole(Role r) { _role = r; }
	inline Role getRole() const { return _role; }
	
	int getStride();
	int getCount();
	Vector2 getVector2(int index);
	Vector3 getVector3(int index);
	Vector4 getVector4(int index);

protected:
	virtual ~Source() {}
	ptr<FloatArray> _floatArray;
	ptr<TechniqueCommon> _tc;
	Role _role;
};
typedef std::map<std::string,ptr<Source> > SourceList;


class VWGLEXPORT Input : public Node {
public:
	Input() :Node("input") {}
	
	void setSemantic(const std::string & s);
	inline const std::string & getSemantic() const { return _semantic; }
	
	Source::Role getRole();

protected:
	virtual ~Input() {}
	
	std::string _semantic;
};
typedef std::vector<ptr<Input> > InputList;

class VWGLEXPORT Vertices : public Node {
public:
	Vertices() :Node("vertices") {}
	
	void setInput(Input * i) { _input = i; }
	Input * getInput() { return _input.getPtr(); }

	const std::string & getSourceId();

protected:
	virtual ~Vertices() {}
	
	ptr<Input> _input;
};
typedef std::map<std::string,ptr<Vertices> > VerticesList;
	
class VWGLEXPORT Vcount : public Node {
public:
	Vcount() :Node("vcount") {}
	
	void setArray(const std::string & v);
	std::vector<int> & getArray() { return _array; }

protected:
	virtual ~Vcount() {}
	
	std::vector<int> _array;
};

class VWGLEXPORT P : public Node {
public:
	P() :Node("p") { }
	
	void setArray(const std::string & v);
	std::vector<int> & getArray() { return _array; }

protected:
	virtual ~P() {}
	
	std::vector<int> _array;
};

class Polylist : public Node {
public:
	Polylist() :Node("polylist") {}
	
	inline void setMaterial(const std::string & mat) { _material = mat; }
	inline const std::string & getMaterial() const { return _material; }

	void addInput(Input * i) { _inputs.push_back(i); }
	InputList & getInputs() { return _inputs; }
	
	void setVcount(Vcount * v) { _vcount = v; }
	Vcount * getVcount() { return _vcount.getPtr(); }

	void setP(P * p) { _p = p; }
	P * getP() { return _p.getPtr(); }

protected:
	virtual ~Polylist() {}

	std::string _material;
	InputList _inputs;
	ptr<Vcount> _vcount;
	ptr<P> _p;
};
typedef std::vector<ptr<Polylist> > PolylistList;
	
class Triangles : public Node {
public:
	Triangles() :Node("triangles") {}

	inline void setMaterial(const std::string & mat) { _material = mat; }
	inline const std::string & getMaterial() const { return _material; }
	
	void addInput(Input * i) { _inputs.push_back(i); }
	InputList & getInputs() { return _inputs; }
	
	void setP(P * p) { _p = p; }
	P * getP() { return _p.getPtr(); }
	
protected:
	virtual ~Triangles() {}
	
	std::string _material;
	InputList _inputs;
	ptr<Vcount> _vcount;
	ptr<P> _p;
};
typedef std::vector<ptr<Triangles> > TrianglesList;

class Mesh : public Node {
public:
	Mesh() :Node("mesh") {}

	void addSource(Source * s) { _sources[s->getId()] = s; }
	SourceList & getSources() { return _sources; }
	
	void addVertices(Vertices * v) { _vertices[v->getId()] = v; }
	VerticesList & getVertices() { return _vertices; }
	
	void addPolylist(Polylist * p) { _polyLists.push_back(p); }
	PolylistList & getPolylists() { return _polyLists; }
	
	void addTriangles(Triangles * t) { _trianglesList.push_back(t); }
	TrianglesList & getTriangles() { return _trianglesList; }

protected:
	virtual ~Mesh() {}
	
	SourceList _sources;
	VerticesList _vertices;
	PolylistList _polyLists;
	TrianglesList _trianglesList;
};

class Geometry : public Node {
public:
	Geometry() :Node("geometry") {}
	
	void setMesh(Mesh * m) { _mesh = m; }
	Mesh * getMesh() { return _mesh.getPtr(); }

protected:
	virtual ~Geometry() {}
	
	ptr<Mesh> _mesh;
};
typedef std::vector<ptr<Geometry> > GeometryList;
	
class LibraryGeometries : public Library {
public:
	LibraryGeometries() :Library("library_geometries") {}

	void addGeometry(Geometry * geo) { _geometryList.push_back(geo); }
	GeometryList & getGeometries() { return _geometryList; }
	const GeometryList & getGeometries() const { return _geometryList; }

protected:
	virtual ~LibraryGeometries() { _geometryList.clear(); }
	
	GeometryList _geometryList;
};

class VWGLEXPORT Matrix: public Node {
public:
	Matrix() :Node("matrix") {}
	
	void setMatrix(const std::string & str_matrix);
	inline void setMatrix(const vwgl::Matrix4 & mat) { _matrix = mat; }
	inline const Matrix4 & getMatrix() const { return _matrix; }
	inline Matrix4 getMatrix() { return _matrix; }
	
protected:
	virtual ~Matrix() {}
	
	Matrix4 _matrix;
};

class InstanceGeometry : public Node {
public:
	InstanceGeometry() :Node("instance_geometry") {}

	inline void setUrl(const std::string & id) { _url = id; }
	const std::string & getUrl() const { return _url; }
	std::string & getUrl() { return _url; }
	
protected:
	virtual ~InstanceGeometry() {}
	
	std::string _url;
};
	
class SceneNode : public Node {
public:
	SceneNode() :Node("node") {}
	
	inline void setMatrix(Matrix * mat) { _matrix = mat; }
	inline Matrix * getMatrix() { return _matrix.getPtr(); }

	inline void setInstanceGeometry(InstanceGeometry * ig) { _instanceGeometry = ig; }
	inline InstanceGeometry * getInstanceGeometry() { return _instanceGeometry.getPtr(); }

protected:
	virtual ~SceneNode() {}
	
	ptr<Matrix> _matrix;
	ptr<InstanceGeometry> _instanceGeometry;
};
typedef std::vector<ptr<SceneNode> > SceneNodeList;

class VisualScene : public Node {
public:
	VisualScene() :Node("visual_scene") {}

	void addSceneNode(SceneNode * n) { _nodeList.push_back(n); }
	SceneNodeList & getSceneNodeList() { return _nodeList; }

protected:
	virtual ~VisualScene() {}
	
	SceneNodeList _nodeList;
};
typedef std::vector<ptr<VisualScene> > VisualSceneList;

class LibraryVisualScenes : public Library {
public:
	LibraryVisualScenes() :Library("librar_visual_scenes") {}

	void addVisualScene(VisualScene * vs) { _visualSceneList.push_back(vs); }
	VisualSceneList & getVisualSceneList() { return _visualSceneList; }

protected:
	virtual ~LibraryVisualScenes() {}
	
	VisualSceneList _visualSceneList;
};

class Factory {
public:
	static Node * getNode(const std::string & name);
};

class VWGLEXPORT Context {
public:
	Context();

	Node * pushContextNode(Node * node) { if (node) _contextNode.push_back(node); return node; }
	void popContextNode() { if (_contextNode.size()>0) _contextNode.pop_back(); }

	bool valid() { return _contextNode.size()>0; }

	inline Collada * collada() { return dynamic_cast<Collada*>(_contextNode.back().getPtr()); }
	inline Asset * asset() { return dynamic_cast<Asset*>(_contextNode.back().getPtr()); }
	inline UpAxis * upAxis() { return dynamic_cast<UpAxis*>(_contextNode.back().getPtr()); }
	inline Unit * unit() { return dynamic_cast<Unit*>(_contextNode.back().getPtr()); }
	inline Geometry * geometry() { return dynamic_cast<Geometry*>(_contextNode.back().getPtr()); }
	inline LibraryGeometries * libraryGeometries() { return dynamic_cast<LibraryGeometries*>(_contextNode.back().getPtr()); }
	inline Mesh * mesh() { return dynamic_cast<Mesh*>(_contextNode.back().getPtr()); }
	inline Source * source() { return dynamic_cast<Source*>(_contextNode.back().getPtr()); }
	inline Vertices * vertices() { return dynamic_cast<Vertices*>(_contextNode.back().getPtr()); }
	inline Polylist * polylist() { return dynamic_cast<Polylist*>(_contextNode.back().getPtr()); }
	inline FloatArray * floatArray() { return dynamic_cast<FloatArray*>(_contextNode.back().getPtr()); }
	inline TechniqueCommon * techniqueCommon() { return dynamic_cast<TechniqueCommon*>(_contextNode.back().getPtr()); }
	inline Accessor * accessor() { return dynamic_cast<Accessor*>(_contextNode.back().getPtr()); }
	inline Param * param() { return dynamic_cast<Param*>(_contextNode.back().getPtr()); }
	inline Input * input() { return dynamic_cast<Input*>(_contextNode.back().getPtr()); }
	inline Vcount * vcount() { return dynamic_cast<Vcount*>(_contextNode.back().getPtr()); }
	inline Triangles * triangles() { return dynamic_cast<Triangles*>(_contextNode.back().getPtr()); }
	inline P * p() { return dynamic_cast<P*>(_contextNode.back().getPtr()); }
	inline LibraryVisualScenes * libraryVisualScenes() { return dynamic_cast<LibraryVisualScenes*>(_contextNode.back().getPtr()); }
	inline VisualScene * visualScene() { return dynamic_cast<VisualScene*>(_contextNode.back().getPtr()); }
	inline SceneNode * node() { return dynamic_cast<SceneNode*>(_contextNode.back().getPtr()); }
	inline Matrix * matrix() { return dynamic_cast<Matrix*>(_contextNode.back().getPtr()); }
	inline InstanceGeometry * instanceGeometry() { return dynamic_cast<InstanceGeometry*>(_contextNode.back().getPtr()); }
	
	inline Node * parent() { if (_contextNode.size()>2) return (*(_contextNode.rbegin() + 1)).getPtr(); else return NULL; }
	
	inline Geometry * parentGeometry() { return dynamic_cast<Geometry*>(parent()); }
	inline Mesh * parentMesh() { return dynamic_cast<Mesh*>(parent()); }
	inline Source * parentSource() { return dynamic_cast<Source*>(parent()); }
	inline Vertices * parentVertices() { return dynamic_cast<Vertices*>(parent()); }
	inline Polylist * parentPolylist() { return dynamic_cast<Polylist*>(parent()); }
	inline Triangles * parentTriangles() { return dynamic_cast<Triangles*>(parent()); }
	inline TechniqueCommon * parentTechniqueCommon() { return dynamic_cast<TechniqueCommon*>(parent()); }
	inline Accessor * parentAccessor() { return dynamic_cast<Accessor*>(parent()); }
	inline VisualScene * parentVisualScene() { return dynamic_cast<VisualScene*>(parent()); }
	inline SceneNode * parentNode() { return dynamic_cast<SceneNode*>(parent()); }
	inline Asset * parentAsset() { return dynamic_cast<Asset*>(parent()); }
protected:
	std::vector<ptr<Node> > _contextNode;
};

class VWGLEXPORT ColladaReader {
public:
	typedef void * XmlReader;

	std::string getAttribute(XmlReader * p_xml, const std::string & key);
		
	collada::Collada * parse(XmlReader * xml);
};

}
}

#endif
