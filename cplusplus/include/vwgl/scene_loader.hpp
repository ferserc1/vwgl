#ifndef _vwgl_sceneloader_hpp_
#define _vwgl_sceneloader_hpp_

#include <vwgl/readerplugin.hpp>

namespace vwgl {

// API 1.0
class VWGLEXPORT  SceneLoader : public ReadNodePlugin {
public:
	SceneLoader();
	
	virtual bool acceptFileType(const std::string & path);
	
	virtual Node * loadNode(const std::string & path);
	
protected:
	virtual ~SceneLoader();
};
	
// API 2.0
class VWGLEXPORT SceneReader : public ReadScenePlugin {
public:
	virtual bool acceptFileType(const std::string & path);
	
	virtual scene::Node * loadScene(const std::string & path);
};

}

#endif