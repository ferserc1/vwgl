//
//  quaternion.hpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 08/11/13.
//  Copyright (c) 2013 Vitaminew. All rights reserved.
//

#ifndef vwgl_quaternion_hpp
#define vwgl_quaternion_hpp

#include <vwgl/vector.hpp>
#include <vwgl/matrix.hpp>

namespace vwgl {

class Quaternion : public Vector4 {
public:
	Quaternion() :Vector4() {}
	Quaternion(float alpha, float x, float y, float z) {
		_v[0] = x * sinf(alpha/2.0f);
		_v[1] = y * sinf(alpha/2.0f);
		_v[2] = z * sinf(alpha/2.0f);
		_v[3] = cosf(alpha/2.0f);
	}
	Quaternion(const Quaternion & q) {
		_v[0] = q._v[0];
		_v[1] = q._v[1];
		_v[2] = q._v[2];
		_v[3] = q._v[3];
	}
	Quaternion(const Matrix3 & m) {
		float w = sqrtf(1.0f + m[0] + m[4] + m[8]) / 2.0f;
		float w4 = 4.0f * w;
		
		_v[0] =	(m[7] - m[5]) / w;
		_v[1] =	(m[2] - m[6]) / w4;
		_v[2] = (m[3] - m[1]) / w4;
		_v[3] = w;
	}
	Quaternion(const Matrix4 & m) {
		float w = sqrtf(1.0f + m[0] + m[5] + m[10]) / 2.0f;
		float w4 = 4.0f * w;
		
		_v[0] =	(m[9] - m[6]) / w;
		_v[1] =	(m[2] - m[8]) / w4;
		_v[2] = (m[4] - m[1]) / w4;
		_v[3] = w;
	}
	
	static Quaternion makeWithMatrix(const Matrix3 & m) {
		return Quaternion(m);
	}
	
	static Quaternion makeWithMatrix(const Matrix4 & m) {
		return Quaternion(m);
	}
	
	inline Matrix4 getMatrix4() const {
		Matrix4 m = Matrix4::makeIdentity();
		m.setRow(0, Vector4(1.0f - 2.0f*_v[1]*_v[1] - 2.0f*_v[2]*_v[2], 2.0f*_v[0]*_v[1] - 2.0f*_v[2]*_v[3], 2.0f*_v[0]*_v[2] + 2.0f*_v[1]*_v[3], 0.0f));
		m.setRow(1, Vector4(2.0f*_v[0]*_v[1] + 2.0f*_v[2]*_v[3], 1.0f - 2.0f*_v[0]*_v[0] - 2.0f*_v[2]*_v[2], 2.0f*_v[1]*_v[2] - 2.0f*_v[0]*_v[3], 0.0f));
		m.setRow(2, Vector4(2.0f*_v[0]*_v[2] - 2.0f*_v[1]*_v[3], 2.0f*_v[1]*_v[2] + 2.0f*_v[0]*_v[3], 1.0f - 2.0f*_v[0]*_v[0] - 2.0f*_v[1]*_v[1], 0.0f));
		return m;
	}
	
	inline Matrix3 getMatrix3() const {
		Matrix3 m = Matrix3::makeIdentity();
		m.setRow(0, Vector3(1.0f - 2.0f*_v[1]*_v[1] - 2.0f*_v[2]*_v[2], 2.0f*_v[0]*_v[1] - 2.0f*_v[2]*_v[3], 2.0f*_v[0]*_v[2] + 2.0f*_v[1]*_v[3]));
		m.setRow(1, Vector3(2.0f*_v[0]*_v[1] + 2.0f*_v[2]*_v[3], 1.0f - 2.0f*_v[0]*_v[0] - 2.0f*_v[2]*_v[2], 2.0f*_v[1]*_v[2] - 2.0f*_v[0]*_v[3]));
		m.setRow(2, Vector3(2.0f*_v[0]*_v[2] - 2.0f*_v[1]*_v[3], 2.0f*_v[1]*_v[2] + 2.0f*_v[0]*_v[3], 1.0f - 2.0f*_v[0]*_v[0] - 2.0f*_v[1]*_v[1]));
		return m;
	}
};


}

#endif
