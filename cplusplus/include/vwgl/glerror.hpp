
#ifndef _vwgl_glerror_hpp_
#define _vwgl_glerror_hpp_

#include <string>
#include <vwgl/export.hpp>
#include <vwgl/opengl.hpp>

namespace vwgl {

class VWGLEXPORT GlError {
public:
	static GLint getError();
	static std::string getErrorString();
	static bool getCompileShaderStatus(GLint shader);
	static std::string getCompileErrorString(GLint shader);
	static bool getLinkProgramStatus(GLint program);
	static std::string getLinkErrorString(GLint program);
	static void clearError();

protected:
	static int s_lastError;
};

}

#endif
