
#ifndef _vwgl_keyboard_hpp_
#define _vwgl_keyboard_hpp_

#include <iostream>

namespace vwgl {

class Keyboard {
public:
	enum KeyModifier {
		kCommandOrControlKey = 1 << 0,
		kAltKey = 1 << 1,
		kCtrlKey = 1 << 2,
		kShiftKey = 1 << 3,
		kCommandKey = 1 << 4
	};

	enum KeyCode {
		kKeyNull = '\0',
		kKeyA = 'a', kKeyB, kKeyC, kKeyD, kKeyE, kKeyF, kKeyG, kKeyH, kKeyI, kKeyJ, kKeyK, kKeyL, kKeyM, kKeyN, kKeyO, kKeyP, kKeyQ, kKeyR, kKeyS, kKeyT, kKeyU, kKeyV, kKeyW, kKeyX, kKeyY, kKeyZ,
		kKey0 = '0', kKey1, kKey2, kKey3, kKey4, kKey5, kKey6, kKey7, kKey8, kKey9,
		kKeyAdd = '+',
		kKeySub = '-',
		kKeyEsc = 27,
		kKeySpace = ' ',
		kKeyDel = 127,
		kKeyAvPag = 200,
		kKeyRePag,
		kKeyStart,
		kKeyEnd,
		kKeyLeft,
		kKeyUp,
		kKeyRight,
		kKeyDown,
		kKeyBack,
		kKeyTab = '\t',
		kKeyReturn = 13
	};
	Keyboard() :_keyMask(0) {}

	inline void clearModifierMask() { _keyMask = 0; }
	inline void setModifierMask(unsigned int mask) { _keyMask = mask; }
	inline void setModifier(KeyModifier mod) { _keyMask = _keyMask | mod; }
	inline void unsetModifier(KeyModifier mod) { _keyMask = _keyMask & ~mod; }
	inline unsigned int getKeyMask() const { return _keyMask; }
	inline bool getModifierStatus(KeyModifier mod) const { return (_keyMask & mod)!=0; }

	inline void setCharacter(char character) { _char = character; }
	inline void setKey(KeyCode key) { _key = key; }
	inline char character() const { return _char;  }
	inline KeyCode key() const { return _key; }

protected:
	char _char;
	KeyCode _key;
	unsigned int _keyMask;

};

}
#endif
