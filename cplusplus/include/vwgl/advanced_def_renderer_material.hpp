#ifndef _VWGL_ADVANCED_DEF_RENDERER_MATERIAL_HPP_
#define _VWGL_ADVANCED_DEF_RENDERER_MATERIAL_HPP_

#include <vwgl/material.hpp>
#include <vwgl/texture.hpp>

namespace vwgl {

class VWGLEXPORT AdvancedDefRendererMaterial : public Material {
public:
	AdvancedDefRendererMaterial();

	inline void setDiffuseMap(Texture * t) { _diffuseMap = t; }
	inline void setNormalMap(Texture * t) { _normalMap = t; }
	inline void setSpecularMap(Texture * t) { _specularMap = t; }
	inline void setPositionMap(Texture * t) { _positionMap = t; }
	
	virtual void initShader();
	
protected:
	virtual ~AdvancedDefRendererMaterial();
	
	virtual void setupUniforms();
	
	ptr<Texture> _diffuseMap;
	ptr<Texture> _normalMap;
	ptr<Texture> _specularMap;
	ptr<Texture> _positionMap;
};

}

#endif
