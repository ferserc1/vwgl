
#ifndef _vwgl_library_reader_hpp_
#define _vwgl_library_reader_hpp_

#include <vwgl/readerplugin.hpp>

namespace vwgl {

class VWGLEXPORT  LibraryLoader : public ReadLibraryPlugin {
public:
	LibraryLoader();
	
	virtual bool acceptFileType(const std::string & path);
	
	virtual library::Node * loadLibrary(const std::string & path);
	
protected:
	virtual ~LibraryLoader();
};
	
}

#endif