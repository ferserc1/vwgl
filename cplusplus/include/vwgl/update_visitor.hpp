
#ifndef _vwgl_update_visitor_hpp_
#define _vwgl_update_visitor_hpp_

#include <vwgl/node_visitor.hpp>

namespace vwgl {

class VWGLEXPORT UpdateVisitor : public NodeVisitor {
public:
	UpdateVisitor();

	virtual void visit(Node * node);

protected:
	virtual ~UpdateVisitor();
};

}
#endif
