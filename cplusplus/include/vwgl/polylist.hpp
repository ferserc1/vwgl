
#ifndef _vwgl_polylist_hpp_
#define _vwgl_polylist_hpp_

#include <vwgl/referenced_pointer.hpp>
#include <vwgl/vector.hpp>
#include <vwgl/opengl.hpp>
#include <vwgl/matrix.hpp>

#include <vwgl/exceptions.hpp>

#include <vector>

namespace vwgl {

class PolygonException : public BaseException {
public:
	PolygonException() :BaseException("Polygon data exception") {}
	PolygonException(const std::string & msg) :BaseException(msg) {}
	virtual ~PolygonException() throw () {}
};

class MalformedPolyListException : public PolygonException {
public:
	MalformedPolyListException() :PolygonException("Malformed polygon: the number of elements in polygon list does not match or is corrupted") {}
	MalformedPolyListException(const std::string & msg) :PolygonException(msg) {}
	virtual ~MalformedPolyListException() throw () {}
};


class VWGLEXPORT PolyList : public ReferencedPointer {
public:
	enum VertexBufferType {
		kVertex = 0,
		kNormal = 1,
		kTexCoord0 = 2,
		kTexCoord1 = 3,
		kTexCoord2 = 4,
		kTangent = 10,
		kColor = 11,
		kIndex = 100
	};

	enum DrawMode {
		kTriangles = GL_TRIANGLES,
		kTriangleFan = GL_TRIANGLE_FAN,
		kTriangleStrip = GL_TRIANGLE_STRIP,
		kLines = GL_LINES,
	#ifdef VWGL_OPENGL_ES
		kQuads = GL_TRIANGLE_FAN,
		kQuadStrip = GL_TRIANGLE_FAN,
	#else
		kQuads = GL_QUADS,
	#endif
		kDefault = 0
	};
	typedef std::vector<float> FloatVector;
	typedef std::vector<unsigned int> UIntVector;
	
	PolyList();
	PolyList(const PolyList * clone);
	
	void setName(const std::string & name) { _name = name; }
	const std::string & getName() const { return _name; }
	std::string & getName() { return _name; }
	void setGroupName(const std::string & name) { _groupName = name; }
	const std::string & getGroupName() const { return _groupName; }
	std::string & getGroupName() { return _groupName; }
	void setMaterialName(const std::string & name) { _materialName = name; }
	const std::string & getMaterialName() const { return _materialName; }
	std::string & getMaterialName() { return _materialName; }

	inline void useVertexArrayObjects(bool use) { _useVAO = use; }
	inline bool getUseVertexArrayObjects() const { return _useVAO; }
	
	void addVertex(const Vector3 & v);
	void addNormal(const Vector3 & n);
	void addTexCoord0(const Vector2 & tc);
	void addTexCoord1(const Vector2 & tc);
	void addTexCoord2(const Vector2 & tc);
	void addColor(const Color & c);
	void addIndex(unsigned int i);
	void addTriangle(unsigned int v0, unsigned int v1, unsigned int v2);
	unsigned long numberOfIndices() const { return (unsigned long)_index.size(); }
	
	void addVertexVector(const float * v, int size);
	void addNormalVector(const float * v, int size);
	void addTexCoord0Vector(const float * v, int size);
	void addTexCoord1Vector(const float * v, int size);
	void addTexCoord2Vector(const float * v, int size);
	void addColorVector(const float * v, int size);
	void addIndexVector(const unsigned int * v, int size);
	
	void switchUVs(VertexBufferType uv1, VertexBufferType uv2);

	void buildPolyList();
	void destroyGlBuffers();
	void destroy();
	
	inline float * getVertexPointer() { return &_vertex[0]; }
	inline const float * getVertexPointer() const { return &_vertex[0]; }
	inline float * getNormalPointer() { return &_vertex[0]; }
	inline const float * getNormalPointer() const { return &_vertex[0]; }
	inline float * getTexCoord0Pointer() { return &_texCoord0[0]; }
	inline const float * getTexCoord0Pointer() const { return &_texCoord0[0]; }
	inline float * getTexCoord1Pointer() { return &_texCoord1[0]; }
	inline const float * getTexCoord1Pointer() const { return &_texCoord1[0]; }
	inline float * getTexCoord2Pointer() { return &_texCoord2[0]; }
	inline const float * getTexCoord2Pointer() const { return &_texCoord2[0]; }
	inline float * getColorPointer() { return &_color[0]; }
	inline const  float * getColorPointer() const { return &_color[0]; }
	inline unsigned int * getIndexPointer() { return &_index[0]; }
	inline const unsigned int * getIndexPointer() const { return &_index[0]; }
	inline float * getTangentPointer() { return &_tangent[0]; }
	inline const float * getTangentPointer() const { return &_tangent[0]; }
	
	inline GLint getVertexBuffer() const { return _vertexBuffer; }
	inline GLint getNormalBuffer() const { return _normalBuffer; }
	inline GLint getTexCoord0Buffer() const { return _texCoord0Buffer; }
	inline GLint getTexCoord1Buffer() const { return _texCoord1Buffer; }
	inline GLint getTexCoord2Buffer() const { return _texCoord2Buffer; }
	inline GLint getColorBuffer() const { return _colorBuffer; }
	inline GLint getIndexBuffer() const { return _indexBuffer; }
	inline GLint getTangentBuffer() const { return _tangentBuffer; }
	
	inline FloatVector & getVertexVector() { return _vertex; }
	inline const FloatVector & getVertexVector() const { return _vertex; }
	inline FloatVector & getNormalVector() { return _normal; }
	inline const FloatVector & getNormalVector() const { return _normal; }
	inline FloatVector & getTexCoord0Vector() { return _texCoord0; }
	inline const FloatVector & getTexCoord0Vector() const { return _texCoord0; }
	inline FloatVector & getTexCoord1Vector() { return _texCoord1; }
	inline const FloatVector & getTexCoord1Vector() const { return _texCoord1; }
	inline FloatVector & getTexCoord2Vector() { return _texCoord2; }
	inline const FloatVector & getTexCoord2Vector() const { return _texCoord2; }
	inline FloatVector & getColorVector() { return _color; }
	inline UIntVector & getIndexVector() { return _index; }
	inline const UIntVector & getIndexVector() const { return _index; }
	inline FloatVector & getTangentVector() { return _tangent; }
	inline const FloatVector & getTangentVector() const { return _tangent; }
	
	inline unsigned int getVertexCount() const { return static_cast<unsigned int>(_vertex.size()); }
	inline unsigned int getNormalCount() const { return static_cast<unsigned int>(_normal.size()); }
	inline unsigned int getTexCoord0Count() const { return static_cast<unsigned int>(_texCoord0.size()); }
	inline unsigned int getTexCoord1Count() const { return static_cast<unsigned int>(_texCoord1.size()); }
	inline unsigned int getTexCoord2Count() const { return static_cast<unsigned int>(_texCoord2.size()); }
	inline unsigned int getColorCount() const { return static_cast<unsigned int>(_color.size()); }
	inline unsigned int getIndexCount() const { return static_cast<unsigned int>(_index.size()); }
	inline unsigned int getVertexArrayObject() const { return _vertexArrayObject; }
	
	inline void setDrawMode(DrawMode mode) { _drawMode = mode; }
	inline DrawMode getDrawMode() const { return _drawMode; }
	
	void drawElements();

	// TODO: Implement this
	//PolyList * clone();

	void applyTransform(const vwgl::Matrix4 & trx);
	void flipFaces();
	void flipNormals();
	
	inline void setPatchVertices(int patchVertices) { _patchVertices = patchVertices; }
	inline int getPatchVertices() const { return _patchVertices; }
	inline void enablePatchMode() { _patchMode = true; }
	inline void disablePatchMode() { _patchMode = false; }
	inline void setPatchMode(bool mode) { _patchMode = mode; }
	inline bool getPatchMode() const { return _patchMode; }

	inline void setVisible(bool visible = true) { _visible = visible; }
	inline bool isVisible() const { return _visible; }
	inline void show() { _visible = true; }
	inline void hide() { _visible = false; }
	
protected:
	virtual ~PolyList();
	
	std::string _name;
	std::string _groupName;
	std::string _materialName;
	FloatVector _vertex;
	FloatVector _normal;
	FloatVector _texCoord0;
	FloatVector _texCoord1;
	FloatVector _texCoord2;
	FloatVector _color;
	UIntVector _index;
	FloatVector _tangent;
	
	GLuint _vertexArrayObject;
	GLint _vertexBuffer;
	GLint _normalBuffer;
	GLint _texCoord0Buffer;
	GLint _texCoord1Buffer;
	GLint _texCoord2Buffer;
	GLint _colorBuffer;
	GLint _indexBuffer;
	GLint _tangentBuffer;
	
	DrawMode _drawMode;
	bool _useVAO;

	bool _visible;
	
	int _patchVertices;
	bool _patchMode;
	
	void calculateTangent();
};

typedef std::vector<ptr<PolyList> > PolyListVector;

}

#endif
