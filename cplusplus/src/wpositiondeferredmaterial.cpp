#include <vwgl/drawable.hpp>
#include <vwgl/wpositiondeferredmaterial.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

WPositionDeferredMaterial::WPositionDeferredMaterial() :DeferredMaterial() {
	initShader();
}

WPositionDeferredMaterial::~WPositionDeferredMaterial() {
}

void WPositionDeferredMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_wposition.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_wposition.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_wposition.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_wposition.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: position gbuffer shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("def_wposition");
	
	loadVertexAttrib("aVertexPosition");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uMMatrix");

	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
	getShader()->initUniformLocation("uAlphaCutoff");
}

void WPositionDeferredMaterial::setupUniforms() {
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uMVMatrix", modelViewMatrix());
		getShader()->setUniform("uPMatrix", projectionMatrix());
		getShader()->setUniform("uMMatrix", modelMatrix());

		Texture * tex = mat->getTexture() ? mat->getTexture() : TextureManager::get()->whiteTexture();
		getShader()->setUniform("uTexture", tex, Texture::kTexture0);
		getShader()->setUniform("uTextureOffset", mat->getTextureOffset());
		getShader()->setUniform("uTextureScale", mat->getTextureScale());
		getShader()->setUniform("uAlphaCutoff", mat->getAlphaCutoff());
	}
}


}
