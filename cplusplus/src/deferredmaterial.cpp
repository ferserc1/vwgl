
#include <vwgl/deferredmaterial.hpp>
#include <vwgl/drawable.hpp>

namespace vwgl {

DeferredMaterial::DeferredMaterial() :Material() {
}

DeferredMaterial::~DeferredMaterial() {
}
	
void DeferredMaterial::activate() {
	_polygonModeBkp = _polygonMode;
	_cullFaceBkp = _cullFace;
	_lineWidthBkp = _lineWidth;
	if (_settingsMaterial.valid()) {
		setPolygonMode(_settingsMaterial->getPolygonMode());
		setCullFace(_settingsMaterial->getCullFace());
		setLineWidth(_settingsMaterial->getLineWidth());
	}
	Material::activate();
}

void DeferredMaterial::deactivate() {
	Material::deactivate();
	_polygonMode = _polygonModeBkp;
	_cullFace = _cullFaceBkp;
	_lineWidth = _lineWidthBkp;
}

}
