
#include <vwgl/app/directx_context.hpp>

#include <vwgl/system.hpp>
#include <vwgl/directx.hpp>
#include <vwgl/app/win32_window.hpp>
#include <vwgl/app/window_controller.hpp>

namespace vwgl {
namespace app {

#if VWGL_DIRECTX_AVAILABLE==1

bool DirectXContext::createContext()
{
	Win32Window * win = dynamic_cast<Win32Window*>(_window);
	if (!win) return false;

	vwgl::Rect winRect = _window->getRect();
	HWND hwnd = native_cast<HWND>(win->win_Wnd());

	DXGI_MODE_DESC bufferDescriptor;
	ZeroMemory(&bufferDescriptor, sizeof(DXGI_MODE_DESC));

	bufferDescriptor.Width = winRect.width();
	bufferDescriptor.Height = winRect.height();
	bufferDescriptor.RefreshRate.Numerator = 60;
	bufferDescriptor.RefreshRate.Denominator = 1;
	bufferDescriptor.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	bufferDescriptor.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	bufferDescriptor.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	DXGI_SWAP_CHAIN_DESC swapChainDescriptor;
	ZeroMemory(&swapChainDescriptor, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDescriptor.BufferDesc = bufferDescriptor;
	swapChainDescriptor.SampleDesc.Count = 1;
	swapChainDescriptor.SampleDesc.Quality = 0;
	swapChainDescriptor.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDescriptor.BufferCount = 1;
	swapChainDescriptor.OutputWindow = hwnd;
	swapChainDescriptor.Windowed = true;
	swapChainDescriptor.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	IDXGISwapChain * swapChain;
	ID3D11Device * device;
	ID3D11DeviceContext * deviceContext;
	D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, NULL, NULL,
		D3D11_SDK_VERSION, &swapChainDescriptor, &swapChain, &device, NULL, &deviceContext);
	_swapChain = swapChain;
	_device = device;
	_deviceContext = deviceContext;

	ID3D11Texture2D * backBuffer;
	swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBuffer);

	ID3D11RenderTargetView * renderTargetView;
	device->CreateRenderTargetView(backBuffer, nullptr, &renderTargetView);
	_renderTargetView = renderTargetView;
	backBuffer->Release();

	D3D11_TEXTURE2D_DESC depthStencilDescriptor;

	depthStencilDescriptor.Width = winRect.width();
	depthStencilDescriptor.Height = winRect.height();
	depthStencilDescriptor.MipLevels = 1;
	depthStencilDescriptor.ArraySize = 1;
	depthStencilDescriptor.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDescriptor.SampleDesc.Count = 1;
	depthStencilDescriptor.SampleDesc.Quality = 0;
	depthStencilDescriptor.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDescriptor.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDescriptor.CPUAccessFlags = 0;
	depthStencilDescriptor.MiscFlags = 0;

	ID3D11DepthStencilView * depthStencilView;
	ID3D11Texture2D * depthStencilBuffer;
	device->CreateTexture2D(&depthStencilDescriptor, nullptr, &depthStencilBuffer);
	device->CreateDepthStencilView(depthStencilBuffer, nullptr, &depthStencilView);
	_depthStencilView = depthStencilView;
	_depthStencilBuffer = depthStencilBuffer;


	deviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	return true;
}

void DirectXContext::swapBuffers()
{
	IDXGISwapChain * swapChain = native_cast<IDXGISwapChain*>(_swapChain);
	swapChain->Present(0, 0);
}

void DirectXContext::destroy()
{
	native_cast<IDXGISwapChain*>(_swapChain)->Release();
	native_cast<ID3D11Device*>(_device)->Release();
	native_cast<ID3D11DeviceContext*>(_deviceContext)->Release();
	native_cast<ID3D11RenderTargetView*>(_renderTargetView)->Release();
	native_cast<ID3D11DepthStencilView*>(_depthStencilView)->Release();
	native_cast<ID3D11Texture2D*>(_depthStencilBuffer)->Release();
	_swapChain = nullptr;
	_device = nullptr;
	_deviceContext = nullptr;
	_renderTargetView = nullptr;
	_depthStencilView = nullptr;
	_depthStencilBuffer = nullptr;
}

#else

bool DirectXContext::createContext()
{
	return false;
}

void DirectXContext::swapBuffers()
{

}

void DirectXContext::destroy()
{

}

#endif

}
}