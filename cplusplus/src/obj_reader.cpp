
#include <vwgl/obj_reader.hpp>
#include <vwgl/system.hpp>

#include <vwgl/scene/primitive_factory.hpp>

#include <regex>

namespace vwgl {
	
class ObjParser {
public:
	enum LineType {
		v,
		vn,
		vt,
		g,
		f,
		usemtl,
		other
	};
	
	ObjParser(std::fstream & file) :_file(file) {}

	scene::Drawable * getDrawable() {
		_drawable = new scene::Drawable();
		_clearVectors = false;
		
		std::string line;
		while (std::getline(_file, line)) {
			switch (type(line)) {
				case v:
					if (_clearVectors) { clearVectors(); }
					parse_v(line);
					break;
				case vn:
					if (_clearVectors) { clearVectors(); }
					parse_vn(line);
					break;
				case vt:
					if (_clearVectors) { clearVectors(); }
					parse_vt(line);
					break;
				case g:
					parse_g(line);
					break;
				case usemtl:
					parse_usemtl(line);
					break;
				case f:
					parse_f(line);
					_clearVectors = _relative;
					break;
				default:
					break;
			}
		}
		endPolyList();
		
		return _drawable.getPtr();
	}
	
	inline LineType type(const std::string & line) {
		if (line[0]=='v' && line[1]==' ') {
			return v;
		}
		else if (line[0]=='v' && line[1]=='n' && line[2]==' ') {
			return vn;
		}
		else if (line[0]=='v' && line[1]=='t' && line[2]==' ') {
			return vt;
		}
		else if (line[0]=='g' && line[1]==' ') {
			return g;
		}
		else if (line[0]=='f' && line[1]==' ') {
			return f;
		}
		else {
			std::stringstream lineStr(line);
			std::string cmd;
			lineStr >> cmd;
			if (cmd=="usemtl") {
				return usemtl;
			}
		}
		return other;
	}
	
	inline void parse_v(const std::string & line) {
		std::stringstream ssream(line);
		std::string cmd;
		float x, y, z;
		ssream >> cmd >> x >> y >> z;
		_v.push_back(Vector3(x,y,z));
	}
	
	inline void parse_vn(const std::string & line) {
		std::stringstream ssream(line);
		std::string cmd;
		float x, y, z;
		ssream >> cmd >> x >> y >> z;
		_vn.push_back(Vector3(x,y,z).normalize());
	}
	
	inline void parse_vt(const std::string & line) {
		std::stringstream ssream(line);
		std::string cmd;
		float v, w;
		ssream >> cmd >> v >> w;
		_vt.push_back(Vector2(v,w));
	}
	
	inline void parse_g(const std::string & line) {
	}
	
	inline void parse_usemtl(const std::string & line) {
		endPolyList();
		_currentPolyList = new vwgl::PolyList();
		std::stringstream sstream(line);
		std::string cmd, name;
		sstream >> cmd >> name;
		_currentPolyList->setName(name);
	}
	
	inline void parse_f(const std::string & line) {
		if (!_currentPolyList.valid()) {
			// Unexpected face
		}
		std::stringstream sstream(line);
		std::string cmd;
		std::vector<std::string> indexes;
		sstream >> cmd;
		while (!sstream.eof()) {
			std::string index;
			sstream >> index;
			if (index.size()>4) indexes.push_back(index);
		}
		
		if (indexes.size()==3) {
			std::vector<std::string>::iterator it;
			int baseIndex = _currentPolyList->getIndexCount();
			for (it=indexes.begin(); it!=indexes.end(); ++it) {
				int i_v, i_n, i_t;
				parseFace(*it, i_v, i_n, i_t);
				_currentPolyList->addVertex(getItem(_v,i_v));
				_currentPolyList->addNormal(getItem(_vn,i_n));
				_currentPolyList->addTexCoord0(getItem(_vt,i_t));
			}
			_currentPolyList->addTriangle(baseIndex, baseIndex + 1, baseIndex + 2);
		}
		else if (indexes.size()==4) {
			throw ObjInvalidVertexException();
			// Invalid number of faces
			/*std::vector<std::string>::iterator it;
			int baseIndex = _currentPolyList->getIndexCount();
			for (it=indexes.begin(); it!=indexes.end(); ++it) {
				int i_v, i_n, i_t;
				parseFace(*it, i_v, i_n, i_t);
				_currentPolyList->addVertex(getItem(_v,i_v));
				_currentPolyList->addNormal(getItem(_vn,i_n));
				_currentPolyList->addTexCoord0(getItem(_vt,i_t));
			}
			_currentPolyList->addTriangle(baseIndex, baseIndex + 1, baseIndex + 2);
			_currentPolyList->addTriangle(baseIndex + 2, baseIndex + 3, baseIndex);
			 */
		}
		else {
			throw ObjInvalidVertexException();
			// Invalid number of faces
		}
	}
	
	inline const Vector3 & getItem(const std::vector<Vector3> & vec, int item) {
		return _relative ? vec[vec.size() - item]:vec[item - 1];
	}
	
	inline const Vector2 & getItem(const std::vector<Vector2> & vec, int item) {
		return _relative ? vec[vec.size() - item]:vec[item - 1];
	}
	
	inline void parseFace(const std::string & face, int & vertex, int & normal, int & tex) {
		std::regex re1("(\\d+)\\/(\\d+)\\/(\\d+)");
		std::regex re2("-([[:digit:]]+)\\/-([[:digit:]]+)\\/-([[:digit:]]+)");
//		std::regex re2("(\\d+)\\/(\\d+)");
//		std::regex re3("(\\d+)");
		std::smatch match;
		if (std::regex_match(face, match,re1) && match.size()>=4) {
			_relative = false;
			vertex = atoi(match[1].str().c_str());
			tex = atoi(match[2].str().c_str());
			normal = atoi(match[3].str().c_str());
		}
		else if (std::regex_match(face, match,re2) && match.size()>=4) {
			_relative = true;
			vertex = atoi(match[1].str().c_str());
			tex = atoi(match[2].str().c_str());
			normal = atoi(match[3].str().c_str());
		}
		else {
			// invalid face
			throw ObjInvalidFacesException();
		}
	}
	
	inline void endPolyList() {
		if (_currentPolyList.valid()) {
			_currentPolyList->buildPolyList();
			_drawable->addPolyList(_currentPolyList.getPtr());
		}
	}
	
	inline void clearVectors() {
		_v.clear();
		_vn.clear();
		_vt.clear();
		_clearVectors = false;
	}

protected:
	std::fstream & _file;
	ptr<scene::Drawable> _drawable;
	ptr<PolyList> _currentPolyList;
	bool _relative;
	bool _clearVectors;
	
	std::vector<Vector3> _v;
	std::vector<Vector3> _vn;
	std::vector<Vector2> _vt;
};

ObjModelReader::ObjModelReader()
{
	
}

ObjModelReader::~ObjModelReader() {
	
}
	
bool ObjModelReader::acceptFileType(const std::string &path) {
	return System::get()->getExtension(path)=="obj";
}
	
scene::Drawable * ObjModelReader::loadModel(const std::string &path) {
	ptr<scene::Drawable> drawable = nullptr;
	
	_file.open(path);
	if (_file.is_open()) {
		ObjParser parser(_file);
		try {
			drawable = parser.getDrawable();
			if (drawable.getPtr()) {
				drawable->setName(System::get()->getFileName(path));
			}
		}
		catch (ObjInvalidFacesException & e) {
			addWarningMessage(e.what());
			drawable = nullptr;
		}
		catch (ObjInvalidVertexException & e) {
			addWarningMessage(e.what());
			drawable = nullptr;
		}
		
		_file.close();
	}
	else {
		addWarningMessage("No such file or directory");
	}
	
	return drawable.release();
}

}