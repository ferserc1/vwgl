//
//  render_settings.cpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 24/02/14.
//  Copyright (c) 2014 Vitaminew. All rights reserved.
//

#include <vwgl/render_settings.hpp>

namespace vwgl {

RenderSettings * RenderSettings::s_currentRenderSettings = nullptr;
RenderSettings * RenderSettings::getCurrentRenderSettings() {
	return s_currentRenderSettings;
}
void RenderSettings::setCurrentRenderSettings(vwgl::RenderSettings *settings) {
	s_currentRenderSettings = settings;
}

}
