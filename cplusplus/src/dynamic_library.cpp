
#include <vwgl/dynamic_library.hpp>
#include <vwgl/system.hpp>
#include <iostream>

#if VWGL_WINDOWS==1

#include <Windows.h>

#elif VWGL_MAC==1

#include <dlfcn.h>

#endif


namespace vwgl {

std::vector<std::string> DynamicLibrary::s_searchPathVector;
	
#if VWGL_WINDOWS==0

plain_ptr DynamicLibrary_Load(const std::string & libraryPath) {
	System * s = System::get();
	plain_ptr result = nullptr;
	
	std::string fileName = s->getFileName(libraryPath);
	std::string path = s->getPath(libraryPath);
	
	result = dlopen(s->addExtension(libraryPath, "dylib").c_str(), RTLD_LAZY);
	result = result ? result:dlopen(s->addExtension(libraryPath, "so").c_str(), RTLD_LAZY);
	result = result ? result:dlopen(s->addExtension(libraryPath, "plugin").c_str(), RTLD_LAZY);
	result = result ? result:dlopen(s->addExtension(s->addPathComponent(path, "lib" + fileName), "dylib").c_str(), RTLD_LAZY);
	result = result ? result:dlopen(s->addExtension(s->addPathComponent(path, "lib" + fileName), "so").c_str(), RTLD_LAZY);
	result = result ? result:dlopen(s->addExtension(s->addPathComponent(path, "lib" + fileName), "plugin").c_str(), RTLD_LAZY);
	
	return result;
}

#else

plain_ptr DynamicLibrary_Load(const std::string & libraryPath) {
	System * s = System::get();
	plain_ptr result = nullptr;

#ifdef _DEBUG
	std::string filename = s->getFileName(libraryPath);
	std::string extension = s->getExtension(libraryPath);

	std::string path = s->addPathComponent(s->getPath(libraryPath), filename + "d");
	path = s->addExtension(path, extension);
#else
	std::string path = libraryPath;
#endif

	result = LoadLibrary(path.c_str());

	return result;
}

#endif

DynamicLibrary::DynamicLibrary()
	:_libraryHandle(nullptr)
{

}

DynamicLibrary::~DynamicLibrary() {
	destroy();
}

void DynamicLibrary::addSearchPath(const std::string &path) {
	std::vector<std::string>::iterator it = std::find(s_searchPathVector.begin(), s_searchPathVector.end(), path);
	if (it==s_searchPathVector.end()) {
		s_searchPathVector.push_back(path);
	}
}

void DynamicLibrary::removeSearchPath(const std::string & path) {
	if (s_searchPathVector.size()==0) {
		initDefaultSearchPath();
	}
	std::vector<std::string>::iterator it = std::find(s_searchPathVector.begin(), s_searchPathVector.end(), path);
	if (it!=s_searchPathVector.end()) {
		s_searchPathVector.erase(it);
	}
}

DynamicLibrary * DynamicLibrary::load(const std::string & path) {
	if (s_searchPathVector.size()==0) {
		initDefaultSearchPath();
	}
	ptr<DynamicLibrary> lib;
	System * s = System::get();
	plain_ptr handle;

#if VWGL_WINDOWS==1

	handle = DynamicLibrary_Load(path);

	std::vector<std::string>::iterator it;
	for (it=s_searchPathVector.begin(); handle==nullptr && it!=s_searchPathVector.end(); ++it) {
		handle = DynamicLibrary_Load(s->addPathComponent(*it, path));
	}

#else

	handle = DynamicLibrary_Load(path);
	std::vector<std::string>::iterator it;
	for (it=s_searchPathVector.begin(); handle==nullptr && it!=s_searchPathVector.end(); ++it) {
		handle = DynamicLibrary_Load(s->addPathComponent(*it, path));
	}

#endif

	if (handle) {
		lib = new DynamicLibrary();
		lib->_libraryHandle = handle;
	}
	
	return lib.release();
}

plain_ptr DynamicLibrary::getProcAddress(const std::string & symbolName) {
	plain_ptr result = nullptr;

	if (_libraryHandle) {

#if VWGL_WINDOWS==1
		result = GetProcAddress(native_cast<HINSTANCE>(_libraryHandle), symbolName.c_str());
		
		if (!result) {
			std::cerr << "vwgl::DynamicLibrary::getProcAddress(): No such symbol with name " << symbolName << std::endl;
		}
#else
		result = dlsym(_libraryHandle, symbolName.c_str());
		const char * dlsym_error = dlerror();
		if (dlsym_error) {
			std::cerr << "vwgl::DynamicLibrary::getProcAddress(): No such symbol with name " << symbolName << std::endl;
			std::cerr << " Error: " << dlsym_error << std::endl;
		}
#endif

	}

	return result;
}

void DynamicLibrary::destroy() {
	if (_libraryHandle) {

#if VWGL_WINDOWS==1
		FreeLibrary(native_cast<HINSTANCE>(_libraryHandle));
		
#elif VWGL_MAC==1
		dlclose(_libraryHandle);
#endif
		_libraryHandle = nullptr;
	}
}

void DynamicLibrary::initDefaultSearchPath() {
	System * s = System::get();
	std::string path = s->getExecutablePath();

	s_searchPathVector.push_back(path);
	s_searchPathVector.push_back(s->addPathComponent(path, "plugins"));
	s_searchPathVector.push_back(s->addPathComponent(path, "modules"));

	if (s->isMac()) {
		s_searchPathVector.push_back(s->addPathComponent(path, "../PlugIns"));
		s_searchPathVector.push_back(s->addPathComponent(path, "../../../"));
		s_searchPathVector.push_back(s->addPathComponent(path, "../../../plugins"));
		s_searchPathVector.push_back(s->addPathComponent(path, "../../../modules"));
	}
}
	

}