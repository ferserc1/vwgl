#include <vwgl/drawable.hpp>
#include <vwgl/projtexturedeferredmaterial.hpp>
#include <vwgl/projector.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

Matrix4 ProjTextureDeferredMaterial::s_texCoordGenMatrix = Matrix4(0.5f,0.0f,0.0f,0.0f,
																   0.0f,0.5f,0.0f,0.0f,
																   0.0f,0.0f,0.5f,0.0f,
																   0.5f,0.5f,0.5f,1.0f);

ProjTextureDeferredMaterial::ProjTextureDeferredMaterial() :_projector(nullptr) {
	initShader();
}

ProjTextureDeferredMaterial::~ProjTextureDeferredMaterial() {
}

void ProjTextureDeferredMaterial::initShader() {
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "def_projtexture.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "def_projtexture.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "def_projtexture.gl3.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "def_projtexture.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: projected texture shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link();
	
	loadVertexAttrib("aVertexPosition");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uMMatrix");
	
	getShader()->initUniformLocation("uTexCoordMatrix");
	
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uReceiveProjections");
}

void ProjTextureDeferredMaterial::setupUniforms() {
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uMVMatrix", modelViewMatrix());
		getShader()->setUniform("uPMatrix", projectionMatrix());
		getShader()->setUniform("uMMatrix", modelMatrix());
		
		vwgl::Matrix4 texCoordMatrix = s_texCoordGenMatrix;
		
		vwgl::Matrix4 projMatrix;
		vwgl::Matrix4 modelMatrix;
		Texture * texture = nullptr;
		if (_projector) {
			projMatrix = _projector->getProjection();
			modelMatrix = _projector->getTransform();
			texture = _projector->getTexture();
		}
		modelMatrix.invert();
		
		texCoordMatrix.mult(projMatrix).mult(modelMatrix);
		getShader()->setUniform("uTexCoordMatrix", texCoordMatrix);
		getShader()->setUniform("uTexture", texture, Texture::kTexture0);
		getShader()->setUniform("uReceiveProjections", mat->getReceiveProjections());
	}
}

}
