
#include <vwgl/app/main_loop.hpp>
#include <vwgl/app/window_controller.hpp>

#include <vwgl/app/win_window_proc.hpp>
#include <vwgl/app/win32_window.hpp>

namespace vwgl {
namespace app {

bool mouseOutDetection(Win32Window * wnd) {
	if (wnd && !wnd->mouseLeaveSent()) {
		POINT point;
		GetCursorPos(&point);
		vwgl::Rect rect = wnd->getRect();
		bool inWindow = point.x >= rect.x() && point.x <= rect.x() + rect.width() &&
						point.y >= rect.y() && point.y <= rect.y() + rect.height();
		return !inWindow;
	}
	else {
		return false;
	}
}

int MainLoop::run() {
	MSG msg;
	BOOL done = FALSE;

	if (!_window.valid()) return 0;

	WindowController * controller = _window->getWindowController();
	Win32Window * win32Window = dynamic_cast<Win32Window*>(_window.getPtr());

	controller->setLastClock(std::clock());

	float delta = 0.0f;
	while (!done) {
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				done = TRUE;
			}
			if ((msg.message == WM_MOUSEMOVE ||
				msg.message == WM_LBUTTONDOWN ||
				msg.message == WM_RBUTTONDOWN ||
				msg.message == WM_MBUTTONDOWN ||
				msg.message == WM_MOUSEHOVER ||
				msg.message == WM_MOUSEACTIVATE ||
				msg.message == WM_MOUSEWHEEL) && win32Window) {
				win32Window->setMouseLeaveSent(false);
			}

			if (mouseOutDetection(win32Window)) {
				win32Window->setMouseLeaveSent(true);
				MSG newMsg;
				msg.message = WM_MOUSELEAVE;
				msg.hwnd = native_cast<HWND>(win32Window->win_Wnd());
				msg.lParam = 0xFFFFFFFF;
				DispatchMessage(&newMsg);
			}
			
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			std::clock_t lastClock = controller->getLastClock();
			std::clock_t clock = std::clock();
			delta = static_cast<float>(clock - lastClock) / CLOCKS_PER_SEC;
			controller->setLastClock(std::clock());
			controller->frame(delta);
			controller->draw();
		}
	}

	_window->destroy();
	_window = nullptr;
	return 0;
}

void MainLoop::quit(int code) {
	PostQuitMessage(code);
}

}
}
