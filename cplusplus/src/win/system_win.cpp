
#include <vwgl/system.hpp>
#include <Windows.h>

#include <msclr\marshal_cppstd.h>

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

extern "C"
{
#include <Rpc.h>
#pragma comment(lib, "Rpcrt4.lib")
}

typedef std::basic_string<TCHAR> tstring;

bool vwgl_sysutils_DirectoryExists(const std::string & path) {
	return System::IO::Directory::Exists(gcnew System::String(path.c_str()));
}

bool vwgl_sysutils_FileExists(const std::string & path) {
	return System::IO::File::Exists(gcnew System::String(path.c_str()));
}

bool vwgl_sysutils_CreateDir(System::String ^ path) {
	try {
		System::IO::Directory::CreateDirectory(path);
		return true;
	}
	catch (System::IO::IOException ^) {
		std::cerr << "Error creating directory: input/output exception." << std::endl;
	}
	catch (System::UnauthorizedAccessException ^) {
		std::cerr << "Error creating directory: unauthorized access" << std::endl;
	}
	catch (System::Exception ^) {
		std::cerr << "Error creating directory" << std::endl;
	}
	return false; 
}

void vwgl_sysutils_ToStdString(System::String ^ str, std::string & outStr) {
	msclr::interop::marshal_context context;
	outStr = context.marshal_as<std::string>(str);
}

void vwgl_sysutils_ListDirectory(const std::string & path, std::vector<std::string> & files, std::vector<std::string> & directories, bool clearVectors) {
	try {
		array<System::String^> ^ dirs = System::IO::Directory::GetDirectories(gcnew System::String(path.c_str()));
		array<System::String^> ^ fls = System::IO::Directory::GetFiles(gcnew System::String(path.c_str()));
		if (clearVectors) {
			files.clear();
			directories.clear();
		}
		for (int i = 0; i < dirs->Length; ++i) {
			System::String ^ dirName = System::IO::Path::GetFileName(dirs[i]);
			std::string stdDirName;
			vwgl_sysutils_ToStdString(dirName, stdDirName);
			directories.push_back(stdDirName);
		}
		for (int i = 0; i < fls->Length; ++i) {
			System::String ^ fileName = System::IO::Path::GetFileName(fls[i]);
			std::string stdFileName;
			vwgl_sysutils_ToStdString(fileName, stdFileName);
			files.push_back(stdFileName);
		}
	}
	catch (System::Exception ^) {
	}
}

bool vwgl_sysutils_Copy(System::String ^ source, System::String ^ dst, bool overwrite) {
	try {
		if (System::IO::Directory::Exists(source)) {
			if (System::IO::Directory::Exists(dst)) {
				if (!overwrite) return false;
				System::IO::Directory::Delete(dst);
			}
			vwgl_sysutils_CreateDir(dst);
			array<System::String^> ^ directories = System::IO::Directory::GetDirectories(source);
			array<System::String^> ^ files = System::IO::Directory::GetFiles(source);
			for (int i = 0; i < files->Length; ++i) {
				System::String ^ fileName = System::IO::Path::GetFileName(files[i]);
				System::String ^ destination = System::IO::Path::Combine(dst, fileName);
				vwgl_sysutils_Copy(files[i], destination, overwrite);
			}
			for (int i = 0; i < directories->Length; ++i) {
				System::String ^ dirName = System::IO::Path::GetFileName(directories[i]);
				System::String ^ destination = System::IO::Path::Combine(dst, dirName);
				vwgl_sysutils_Copy(directories[i], destination, overwrite);
			}
		}
		else if (System::IO::File::Exists(source)) {
			System::IO::File::Copy(source, dst, overwrite);
		}
		else {
			return false;
		}
		
		return true;
	}
	catch (System::IO::FileNotFoundException ^) {
		std::cerr << "Error copying file: file not found" << std::endl;
	}
	catch (System::IO::DirectoryNotFoundException ^) {
		std::cerr << "Error copying file: directory not found" << std::endl;
	}
	catch (System::IO::IOException ^) {
		std::cerr << "Error copying file: input/output exception." << std::endl;
	}
	catch (System::UnauthorizedAccessException ^) {
		std::cerr << "Error copying file: unauthorized access" << std::endl;
	}
	catch (System::Exception ^) {
		std::cerr << "Error copying file" << std::endl;
	}
	return false;
}

bool vwgl_sysutils_RemoveDirectory(const std::string & path) {
	try {
		System::IO::Directory::Delete(gcnew System::String(path.c_str()),true);
		return true;
	}
	catch (System::IO::FileNotFoundException ^) {
		std::cerr << "Error removing directory: file not found" << std::endl;
	}
	catch (System::IO::DirectoryNotFoundException ^) {
		std::cerr << "Error removing directory: directory not found" << std::endl;
	}
	catch (System::IO::IOException ^) {
		std::cerr << "Error copying file: input/output exception." << std::endl;
	}
	catch (System::UnauthorizedAccessException ^) {
		std::cerr << "Error copying file: unauthorized access" << std::endl;
	}
	catch (System::Exception ^) {
		std::cerr << "Error copying file" << std::endl;
	}
	return false;
}

bool vwgl_sysutils_RemoveFile(const std::string & path) {
	try {
		System::IO::File::Delete(gcnew System::String(path.c_str()));
		return true;
	}
	catch (System::IO::FileNotFoundException ^) {
		std::cerr << "Error removing directory: file not found" << std::endl;
	}
	catch (System::IO::DirectoryNotFoundException ^) {
		std::cerr << "Error removing directory: directory not found" << std::endl;
	}
	catch (System::IO::IOException ^) {
		std::cerr << "Error copying file: input/output exception." << std::endl;
	}
	catch (System::UnauthorizedAccessException ^) {
		std::cerr << "Error copying file: unauthorized access" << std::endl;
	}
	catch (System::Exception ^) {
		std::cerr << "Error copying file" << std::endl;
	}
	return false;
}
namespace vwgl {

const std::string & System::getExecutablePath() {
	if (_executablePath=="") {
		std::vector <char> executablePath(MAX_PATH);
		DWORD result = ::GetModuleFileNameA(nullptr,&executablePath[0],static_cast<DWORD>(executablePath.size()));

		while (result==executablePath.size()) {
			executablePath.resize(executablePath.size() * 2);
			result = ::GetModuleFileNameA(nullptr,&executablePath[0], static_cast<DWORD>(executablePath.size()));
		}

		if (result==0) {
			throw std::runtime_error("GetModuleFileName() failed");
		}

		_executablePath = std::string(executablePath.begin(),executablePath.begin() + result);
		//_executablePath = _executablePath.replace(_executablePath.begin(),_executablePath.end(),"\\","/");
		std::replace(_executablePath.begin(),_executablePath.end(),'\\','/');
		_executablePath = _executablePath.substr(0,_executablePath.find_last_of("\\/"));
		_executablePath += "/";
	}
	return _executablePath;
}

const std::string & System::getDefaultShaderPath() {
	if (_defaultShaderPath=="") {
		_defaultShaderPath = getExecutablePath() + "shaders/";
	}
	return _defaultShaderPath;
}

const std::string & System::getResourcesPath() {
	if (_resourcesPath=="") {
		_resourcesPath = getExecutablePath() + "resources/";
	}
	return _resourcesPath;
}

const std::string & System::getTempPath() {
	if (_tempPath == "") {
		TCHAR temp_folder[MAX_PATH + 1];
		::GetTempPath(MAX_PATH + 1, temp_folder);
		tstring tmpPath(temp_folder);
		_tempPath = std::string(tmpPath.begin(), tmpPath.end());
	}
	return _tempPath;
}

bool System::directoryExists(const std::string & path) {
	return vwgl_sysutils_DirectoryExists(path);
}

bool System::fileExists(const std::string &path) {
	return vwgl_sysutils_FileExists(path);
}

bool System::mkdir(const std::string & dir) {
	return vwgl_sysutils_CreateDir(gcnew ::System::String(dir.c_str()));
}

bool System::cp(const std::string & src, const std::string & dst, bool overwrite) {
	::System::String ^ source = gcnew ::System::String(src.c_str());
	::System::String ^ destination = gcnew ::System::String(dst.c_str());
	return vwgl_sysutils_Copy(source,destination,overwrite);
}

bool System::removeDirectory(const std::string & path) {
	return vwgl_sysutils_RemoveDirectory(path);
}

bool System::removeFile(const std::string & path) {
	return vwgl_sysutils_RemoveFile(path);
}

void System::listDirectory(const std::string & path, std::vector<std::string> & files, std::vector<std::string> & directories, bool clearVectors) {
	vwgl_sysutils_ListDirectory(path, files, directories, clearVectors);
}

std::string System::getUuid() {
	UUID uuid;
	UuidCreate(&uuid);

	unsigned char * str;
	UuidToStringA(&uuid, &str);

	std::string s((char*)str);

	RpcStringFreeA(&str);
	return s;
}

}