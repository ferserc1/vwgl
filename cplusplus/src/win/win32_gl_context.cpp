#include <vwgl/app/win32_gl_context.hpp>

#include <vwgl/app/win_window_proc.hpp>
#include <vwgl/app/win32_window.hpp>
#include <vwgl/app/window_controller.hpp>

#include <vwgl/opengl.hpp>

namespace vwgl {
namespace app {

bool Win32GLContext::createContext() {
	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_TYPE_RGBA,
		32,		// Color buffer depth
		0, 0, 0, 0, 0, 0,	// Color bits
		0,		// Alpha buffer
		0,		// Shift bits
		0,		// Accumulation buffer
		0, 0, 0, 0,	// Accumulation bits
		32,		// Depth buffer
		0,		// Stencil buffer
		0,		// Auxiliar buffer
		PFD_MAIN_PLANE,
		0, 0, 0, 0
	};

	Win32Window * win = dynamic_cast<Win32Window*>(_window);
	HDC hdc = native_cast<HDC>(win->win_DeviceContext());
	GLuint pf = ChoosePixelFormat(hdc, &pfd);
	if (!pf || !SetPixelFormat(hdc, pf, &pfd)) {
		return false;
	}

	HGLRC hRC;
	if (!(hRC = wglCreateContext(hdc))) {
		destroy();
		return false;
	}
	_nativeContext = hRC;

	makeCurrent();
	return true;
}

void Win32GLContext::makeCurrent() {
	Win32Window * win = dynamic_cast<Win32Window*>(_window);
	HDC hdc = native_cast<HDC>(win->win_DeviceContext());
	wglMakeCurrent(hdc, native_cast<HGLRC>(_nativeContext));
}

void Win32GLContext::swapBuffers() {
	Win32Window * win = dynamic_cast<Win32Window*>(_window);
	HDC hdc = native_cast<HDC>(win->win_DeviceContext());
	SwapBuffers(hdc);
}

void Win32GLContext::destroy() {
	if (_nativeContext) {
		wglMakeCurrent(0, 0);
		wglDeleteContext(native_cast<HGLRC>(_nativeContext));
		_nativeContext = nullptr;
	}
}

}
}
