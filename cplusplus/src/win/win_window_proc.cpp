
#include <vwgl/app/win_window_proc.hpp>

#include <vwgl/app/main_loop.hpp>
#include <vwgl/app/window_controller.hpp>

#include <iostream>
#include <iomanip>

vwgl::Keyboard::KeyCode code(unsigned char character) {
	vwgl::Keyboard::KeyCode keyCode = vwgl::Keyboard::kKeyNull;

	if (character >= 'a' && character <= 'z') {
		keyCode = static_cast<vwgl::Keyboard::KeyCode>(vwgl::Keyboard::kKeyA + (character - 'a'));
	}
	else if (character >= 'A' && character <= 'Z') {
		keyCode = static_cast<vwgl::Keyboard::KeyCode>(vwgl::Keyboard::kKeyA + (character - 'A'));
	}
	else if (character >= '0' && character <= '9') {
		keyCode = static_cast<vwgl::Keyboard::KeyCode>(vwgl::Keyboard::kKey0 + (character - '0'));
	}
	else if (character == VK_ESCAPE) {
		keyCode = vwgl::Keyboard::kKeyEsc;
	}
	else if (character == VK_DELETE) {
		keyCode = vwgl::Keyboard::kKeyDel;
	}
	else if (character == VK_TAB) {
		keyCode = vwgl::Keyboard::kKeyTab;
	}
	else if (character == VK_SPACE) {
		keyCode = vwgl::Keyboard::kKeySpace;
	}
	else if (character == VK_BACK) {
		keyCode = vwgl::Keyboard::kKeyBack;
	}
	else if (character == VK_LEFT) {
		keyCode = vwgl::Keyboard::kKeyLeft;
	}
	else if (character == VK_RIGHT) {
		keyCode = vwgl::Keyboard::kKeyRight;
	}
	else if (character == VK_UP) {
		keyCode = vwgl::Keyboard::kKeyUp;
	}
	else if (character == VK_DOWN) {
		keyCode = vwgl::Keyboard::kKeyDown;
	}
	else if (character == VK_RETURN) {
		keyCode = vwgl::Keyboard::kKeyReturn;
	}
	else if (character == 187) {
		keyCode = vwgl::Keyboard::kKeyAdd;
	}
	else if (character == 189) {
		keyCode = vwgl::Keyboard::kKeySub;
	}

	return keyCode;
}

unsigned int getModifiers(bool altPressed) {
	unsigned int modifiers = 0;
	if (GetKeyState(VK_CONTROL) & 0x8000) {
		modifiers = modifiers | vwgl::Keyboard::kCtrlKey | vwgl::Keyboard::kCommandOrControlKey;
	}
	if (GetKeyState(VK_SHIFT) & 0x8000) {
		modifiers = modifiers | vwgl::Keyboard::kShiftKey;
	}
	if (altPressed) {
		modifiers = modifiers | vwgl::Keyboard::kAltKey;
	}

	return modifiers;
}

void fillKeyboard(vwgl::Keyboard & kb, unsigned char character, bool altPressed) {
	kb.setCharacter(character);
	kb.setKey(code(character));
	kb.setModifierMask(getModifiers(altPressed));
}

void fillMouseEvent(vwgl::app::MouseEvent & mouseEvent, vwgl::app::MainLoop * mainLoop) {
	POINT cursorPos;
	GetCursorPos(&cursorPos);
	vwgl::Vector2i pos;
	vwgl::Rect winRect = mainLoop->getWindow()->getRect();
	pos.x(cursorPos.x - winRect.x());
	pos.y(cursorPos.y - winRect.y());
	mouseEvent.setPos(pos);
	mouseEvent.mouse().setButtonMask(mainLoop->getMouse().buttonMask());
	mouseEvent.setDelta(vwgl::Vector2());
}

LRESULT CALLBACK vwgl_app_WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	vwgl::app::Window * window = mainLoop->getWindow();
	vwgl::app::WindowController * controller = nullptr;
	vwgl::app::KeyboardEvent kbEvent;
	vwgl::app::MouseEvent mouseEvent;

	static bool altPressed = false;
	if (window && (controller = window->getWindowController())) {
		switch (uMsg) {
		case WM_ACTIVATE:
			break;
		case WM_SYSCOMMAND:
			if (wParam == SC_SCREENSAVE || wParam == SC_MONITORPOWER) {
				return 0;
			}
			break;
		case WM_CLOSE:
			PostQuitMessage(0);
			break;
		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
			if (wParam == VK_MENU) {
				altPressed = true;
			}
			fillKeyboard(kbEvent.keyboard(), static_cast<unsigned char>(wParam), altPressed);
			controller->keyDown(kbEvent);
			break;
		case WM_KEYUP:
		case WM_SYSKEYUP:
			if (wParam == VK_MENU) {
				altPressed = false;
			}
			fillKeyboard(kbEvent.keyboard(), static_cast<unsigned char>(wParam), altPressed);
			controller->keyUp(kbEvent);
			break;
		case WM_LBUTTONDOWN:
			mainLoop->getMouse().setMouseDown(vwgl::Mouse::kLeftButton);
			fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
			fillMouseEvent(mouseEvent, mainLoop);
			controller->mouseDown(mouseEvent);
			break;
		case WM_LBUTTONUP:
			mainLoop->getMouse().setMouseUp(vwgl::Mouse::kLeftButton);
			fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
			fillMouseEvent(mouseEvent, mainLoop);
			controller->mouseUp(mouseEvent);
			break;
		case WM_RBUTTONDOWN:
			mainLoop->getMouse().setMouseDown(vwgl::Mouse::kRightButton);
			fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
			fillMouseEvent(mouseEvent, mainLoop);
			controller->mouseDown(mouseEvent);
			break;
		case WM_RBUTTONUP:
			mainLoop->getMouse().setMouseUp(vwgl::Mouse::kRightButton);
			fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
			fillMouseEvent(mouseEvent, mainLoop);
			controller->mouseUp(mouseEvent);
			break;
		case WM_MBUTTONDOWN:
			mainLoop->getMouse().setMouseDown(vwgl::Mouse::kMiddleButton);
			fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
			fillMouseEvent(mouseEvent, mainLoop);
			controller->mouseDown(mouseEvent);
			break;
		case WM_MBUTTONUP:
			mainLoop->getMouse().setMouseUp(vwgl::Mouse::kMiddleButton);
			fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
			fillMouseEvent(mouseEvent, mainLoop);
			controller->mouseUp(mouseEvent);
			break;
		case WM_MOUSEMOVE:
			fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
			fillMouseEvent(mouseEvent, mainLoop);
			if (mouseEvent.mouse().anyButtonPressed()) {
				controller->mouseDrag(mouseEvent);
			}			
			controller->mouseMove(mouseEvent);
			break;
		case WM_MOUSEWHEEL:
			fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
			fillMouseEvent(mouseEvent, mainLoop);
			if ((short)HIWORD(wParam) > 0) {
				mouseEvent.setDelta(vwgl::Vector2(0.0f, 1.0f));
			}
			else {
				mouseEvent.setDelta(vwgl::Vector2(0.0f, -1.0f));
			}
			controller->mouseWheel(mouseEvent);
			break;
		case WM_MOUSELEAVE:
			if (mainLoop->getMouse().getButtonStatus(vwgl::Mouse::kLeftButton)) {
				mainLoop->getMouse().setMouseUp(vwgl::Mouse::kLeftButton);
				fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
				fillMouseEvent(mouseEvent, mainLoop);
				controller->mouseUp(mouseEvent);
			}

			if (mainLoop->getMouse().getButtonStatus(vwgl::Mouse::kRightButton)) {
				mainLoop->getMouse().setMouseUp(vwgl::Mouse::kRightButton);
				fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
				fillMouseEvent(mouseEvent, mainLoop);
				controller->mouseUp(mouseEvent);
			}

			if (mainLoop->getMouse().getButtonStatus(vwgl::Mouse::kMiddleButton)) {
				mainLoop->getMouse().setMouseUp(vwgl::Mouse::kMiddleButton);
				fillKeyboard(mouseEvent.keyboard(), '\0', altPressed);
				fillMouseEvent(mouseEvent, mainLoop);
				controller->mouseUp(mouseEvent);
			}
			break;
		case WM_SIZE:
			{
				int w = static_cast<int>(LOWORD(lParam));
				int h = static_cast<int>(HIWORD(lParam));
				controller->reshape(w, h);
				window->setSize(w, h);
			}
			break;
		case WM_MOVE:
			{
				int x = static_cast<int>(LOWORD(lParam));
				int y = static_cast<int>(HIWORD(lParam));
				window->setPosition(x, y);
			}
			break;
		}
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}
