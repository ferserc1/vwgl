
#include <vwgl/app/win32_window.hpp>
#include <vwgl/app/window_controller.hpp>
#include <vwgl/app/win_window_proc.hpp>

#include <vwgl/opengl.hpp>
#include <vwgl/graphics.hpp>
#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include <vwgl/app/win32_gl_context.hpp>
#include <vwgl/app/directx_context.hpp>

static LPCSTR WindowClass = "OpenGL";

namespace vwgl {
namespace app {

bool Win32Window::create() {
	WNDCLASS wc;
	DWORD dwExStyle;
	DWORD dwStyle;
	RECT rect;
	rect.left = _rect.x();
	rect.right = static_cast<long>(_rect.width() + _rect.x());
	rect.top = _rect.y();
	rect.bottom = static_cast<long>(_rect.height() + _rect.y());

	_hInstance = GetModuleHandle(0);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)vwgl_app_WindowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = native_cast<HINSTANCE>(_hInstance);
	wc.hIcon = LoadIcon(0, IDI_WINLOGO);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = 0;
	wc.lpszMenuName = 0;
	wc.lpszClassName = WindowClass;

	if (!RegisterClass(&wc)) {
		return false;
	}

	if (_fullscreen) {
		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = _rect.width();
		dmScreenSettings.dmPelsHeight = _rect.height();
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL) {
			return false;
		}
		dwExStyle = WS_EX_APPWINDOW;
		dwExStyle = WS_POPUP;
	}
	else {
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle = WS_OVERLAPPEDWINDOW;
	}

	AdjustWindowRectEx(&rect, dwStyle, FALSE, dwExStyle);
	int newPosX = _rect.x() + _rect.x() - rect.left;
	int newPosY = _rect.y() + _rect.y() - rect.top;

	LPCSTR title = _title.c_str();
	if (!(_hWnd = CreateWindowEx(dwExStyle, WindowClass, title, dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		_rect.x(), _rect.y(), rect.right - rect.left, rect.bottom - rect.top,
		0, 0, native_cast<HINSTANCE>(_hInstance), 0)))
	{
		destroy();
		return false;
	}

	if (!(_hDC = GetDC(native_cast<HWND>(_hWnd)))) {
		destroy();
		return false;
	}

	if (Graphics::get()->getApi() == Graphics::kApiOpenGLBasic ||
		Graphics::get()->getApi() == Graphics::kApiOpenGL ||
		Graphics::get()->getApi() == Graphics::kApiOpenGLAdvanced) {
		GLContext * glContext = new Win32GLContext(this);
		_context = glContext;
		if (!glContext->createContext()) {
			destroy();
			return false;
		}

		glContext->makeCurrent();
		
	}
	
	ShowWindow(native_cast<HWND>(_hWnd), SW_SHOW);
	SetForegroundWindow(native_cast<HWND>(_hWnd));
	SetFocus(native_cast<HWND>(_hWnd));

	if (Graphics::get()->getApi()==Graphics::kApiDirectX) {
		DirectXContext * dxContext = new DirectXContext(this);
		_context = dxContext;
		if (!dxContext->createContext()) {
			destroy();
			return false;
		}
	}

	_controller->initGL();
	_rect.x(newPosX);
	_rect.y(newPosY);

	_controller->reshape(_rect.width(), _rect.height());

	return true;
}

void Win32Window::destroy() {
	if (_fullscreen) {
		ChangeDisplaySettings(0, 0);
	}

	if (_context.valid()) {
		_context->destroy();
		_context = nullptr;
	}

	if (_hDC) {
		ReleaseDC(native_cast<HWND>(_hWnd), native_cast<HDC>(_hDC));
		_hDC = 0;
	}

	if (_hWnd) {
		DestroyWindow(native_cast<HWND>(_hWnd));
		_hWnd = 0;
	}

	UnregisterClass(WindowClass, native_cast<HINSTANCE>(_hInstance));
}

}
}
