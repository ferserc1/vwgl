//
//  mathutils.cpp
//  vwgl
//
//  Created by Fernando Serrano Carpena on 08/11/13.
//  Copyright (c) 2013 Vitaminew. All rights reserved.
//

#include <vwgl/mathutils.hpp>
#ifdef _WIN32
#include <cmath>
#else
#include <math.h>
#endif
#include <cstdlib>

#include <cfloat>
#include <climits>

namespace vwgl {

float Math::kPi = (float)M_PI;
float Math::kPiOver180 = 0.01745329251994f;
float Math::k180OverPi = 57.29577951308233f;
float Math::kPiOver2 = 1.570796326794897f;
float Math::kPiOver4 = 0.785398163397448f;
float Math::k2Pi = 6.283185307179586f;
	
float Math::degreesToRadians(float d) { return d * kPiOver180; }
float Math::radiansToDegrees(float r) { return r * k180OverPi; }

float Math::sin(float f) {
	return sinf(f);
}

float Math::cos(float f) {
	return cosf(f);
}

float Math::tan(float f) {
	return tanf(f);
}
	
float Math::asin(float f) {
	return asinf(f);
}
	
float Math::acos(float f) {
	return acosf(f);
}

float Math::cotan(float i) {
	// TODO: Esto no va bien
	return 1.0f / tanf(i);
}

float Math::atan(float i) {
	return atanf(i);
}
	
float Math::atan2(float i, float j) {
	return atan2f(i, j);
}
	
float Math::sqrt(float v) {
	return ::sqrt(v);
}

float Math::random() {
	return ((float) rand() / (float)(RAND_MAX));
}

float Math::lerp(float from, float to, float t) {
	return (1.0f - t) * from + t * to;
}
	
short Math::minValue(short val) {
	return SHRT_MIN;
}

short Math::maxValue(short val) {
	return SHRT_MAX;
}

int Math::minValue(int val) {
	return INT_MIN;
}

int Math::maxValue(int val) {
	return INT_MAX;
}

long Math::minValue(long val) {
	return LONG_MIN;
}

long Math::maxValue(long val) {
	return LONG_MAX;
}

long long Math::minValue(long long val) {
	return LLONG_MIN;
}

long long Math::maxValue(long long val) {
	return LLONG_MAX;
}

unsigned short Math::minValue(unsigned short val) {
	return 0;
}

unsigned short Math::maxValue(unsigned short val) {
	return USHRT_MAX;
}
	
unsigned int Math::minValue(unsigned int val) {
	return 0;
}

unsigned int Math::maxValue(unsigned int val) {
	return UINT_MAX;
}
	
unsigned long Math::minValue(unsigned long val) {
	return 0;
}

unsigned long Math::maxValue(unsigned long val) {
	return ULONG_MAX;
}

unsigned long long Math::minValue(unsigned long long val) {
	return 0;
}

unsigned long long Math::maxValue(unsigned long long val) {
	return ULLONG_MAX;
}

float Math::minValue(float val) {
	return -FLT_MAX;
}

float Math::maxValue(float val) {
	return FLT_MAX;
}

double Math::minValue(double val) {
	return -DBL_MAX;
}

double Math::maxValue(double val) {
	return DBL_MAX;
}

int Math::nextPowerOfTwo(int x) {
	return static_cast<int>(pow(2, ceil(log2(static_cast<double>(x)))));
}

}