#include <vwgl/deferred_lighting.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/drawable.hpp>

#include <vwgl/deferredrenderer.hpp>
#include <vwgl/graphics.hpp>
#include <vwgl/scene/camera.hpp>

namespace vwgl {

DeferredLighting::DeferredLighting()
	:_shadowBlurIterations(4)
	,_exposure(1.0f)
	,_legacyLight(nullptr)
	,_light(nullptr)
{
	initShader();
}
DeferredLighting::~DeferredLighting() {
	
}

void DeferredLighting::initShader() {
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "def_lighting.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "def_lighting.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "def_lighting.gl3.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "def_lighting.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: deferred lighting shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("deferred_lighting");
	
	loadVertexAttrib("aVertexPos");
	loadTexCoord0Attrib("aTexturePosition");

	getShader()->initUniformLocation("uDiffuseMap");
	getShader()->initUniformLocation("uNormalMap");
	getShader()->initUniformLocation("uSpecularMap");
	getShader()->initUniformLocation("uPositionMap");
	getShader()->initUniformLocation("uShadowMap");
	getShader()->initUniformLocation("uShadowMapSize");
	
	getShader()->initUniformLocation("uLightType");
	getShader()->initUniformLocation("uLightDirection");
	getShader()->initUniformLocation("uLightPosition");
	getShader()->initUniformLocation("uLightAmbient");
	getShader()->initUniformLocation("uLightDiffuse");
	getShader()->initUniformLocation("uLightSpecular");
	getShader()->initUniformLocation("uLightAttenuation");
	getShader()->initUniformLocation("uSpotDirection");
	getShader()->initUniformLocation("uSpotExponent");
	getShader()->initUniformLocation("uSpotCutoff");
	getShader()->initUniformLocation("uCutoffDistance");
	getShader()->initUniformLocation("uLightEmissionMult");
	
	getShader()->initUniformLocation("uBlurIterations");
	
	getShader()->initUniformLocation("uExposure");
}
		
void DeferredLighting::setupUniforms() {

	getShader()->setUniform("uDiffuseMap",_diffuseMap.getPtr(),vwgl::Texture::kTexture0);
	getShader()->setUniform("uNormalMap",_normalMap.getPtr(),vwgl::Texture::kTexture1);
	getShader()->setUniform("uSpecularMap",_specularMap.getPtr(),vwgl::Texture::kTexture2);
	getShader()->setUniform("uPositionMap",_positionMap.getPtr(),vwgl::Texture::kTexture3);
	getShader()->setUniform("uShadowMap",_shadowMap.getPtr(),vwgl::Texture::kTexture4);
	Size2Di size = LightManager::get()->getShadowMapSize();
	Size2Di smapSize(State::get()->getViewport().width(), State::get()->getViewport().height());
	getShader()->setUniform("uShadowMapSize", vwgl::Vector2(static_cast<float>(smapSize.x()),
															static_cast<float>(smapSize.y())));
	getShader()->setUniform("uBlurIterations", _shadowBlurIterations);
	getShader()->setUniform("uExposure",_exposure);

	float lightEmissionMult = 1.0f / static_cast<float>(_enabledLights);
	if (_light && scene::Camera::getMainCamera()) {
		Matrix4 viewMatrix = scene::Camera::getMainCamera()->getViewMatrix();
		Vector4 lightPos(_light->getPosition(),1.0);
		lightPos = viewMatrix.multVector(lightPos);
		
		getShader()->setUniform("uLightType",_light->getType());
		getShader()->setUniform("uLightDirection",_light->getDirection());
		getShader()->setUniform("uLightPosition",lightPos.xyz());
		getShader()->setUniform("uLightAmbient",_light->getAmbient());
		getShader()->setUniform("uLightDiffuse",_light->getDiffuse());
		getShader()->setUniform("uLightSpecular",_light->getSpecular());
		Vector3 attenuation(_light->getConstantAttenuation(),
							_light->getLinearAttenuation(),
							_light->getExpAttenuation());
		getShader()->setUniform("uLightAttenuation",attenuation);
		getShader()->setUniform("uSpotDirection",_light->getSpotDirection());
		getShader()->setUniform("uSpotExponent",_light->getSpotExponent());
		getShader()->setUniform("uSpotCutoff",_light->getSpotCutoff());
		getShader()->setUniform("uCutoffDistance", _light->getCutoffDistance());
		getShader()->setUniform("uLightEmissionMult", lightEmissionMult);
	}
	else if (_legacyLight) {
		Matrix4 viewMatrix = CameraManager::get()->getMainCamera()->getViewMatrix();
		Vector4 lightPos(_legacyLight->getPosition(),1.0);
		lightPos = viewMatrix.multVector(lightPos);
		
		getShader()->setUniform("uLightType",_legacyLight->getType());
		getShader()->setUniform("uLightDirection",_legacyLight->getDirection());
		getShader()->setUniform("uLightPosition",lightPos.xyz());
		getShader()->setUniform("uLightAmbient",_legacyLight->getAmbient());
		getShader()->setUniform("uLightDiffuse",_legacyLight->getDiffuse());
		getShader()->setUniform("uLightSpecular",_legacyLight->getSpecular());
		Vector3 attenuation(_legacyLight->getConstantAttenuation(),
							_legacyLight->getLinearAttenuation(),
							_legacyLight->getExpAttenuation());
		getShader()->setUniform("uLightAttenuation",attenuation);
		getShader()->setUniform("uSpotDirection",_legacyLight->getSpotDirection());
		getShader()->setUniform("uSpotExponent",_legacyLight->getSpotExponent());
		getShader()->setUniform("uSpotCutoff",_legacyLight->getSpotCutoff());
		getShader()->setUniform("uCutoffDistance", _legacyLight->getCutoffDistance());
		getShader()->setUniform("uLightEmissionMult", lightEmissionMult);
	}
	else {
		getShader()->setUniform("uLightType",vwgl::Light::kTypeDisabled);
		getShader()->setUniform("uLightDirection",Vector3(0.0,1.0,0.0));
		getShader()->setUniform("uLightPosition",Vector3(0.0,0.0,0.0));
		getShader()->setUniform("uLightAmbient",Color::black());
		getShader()->setUniform("uLightDiffuse",Color::black());
		getShader()->setUniform("uLightSpecular",Color::black());
		getShader()->setUniform("uLightAttenuation",Vector3(0.0));
		getShader()->setUniform("uSpotDirection",Vector3(0.0,1.0,0.0));
		getShader()->setUniform("uSpotExponent",0.0f);
		getShader()->setUniform("uSpotCutoff",0.0f);
		getShader()->setUniform("uCutoffDistance", 0.0f);
		getShader()->setUniform("uLightEmissionMult", lightEmissionMult);
	}
}
	
}
