
#include <vwgl/group.hpp>
#include <algorithm>

namespace vwgl {

Group::Group() {
	
}

Group::~Group() {
	
}

void Group::addChild(vwgl::Node *child) {
	if (child) {
		ptr<Node> node = child;
		if (node->_parent) {
			node->_parent->removeChild(node.getPtr());
		}
		_children.push_back(node.getPtr());
		node->_parent = this;
	}
}

void Group::removeChild(vwgl::Node * child) {
	if (child) {
		NodeList::iterator it = std::find(_children.begin(), _children.end(), child);
		if (it!=_children.end()) {
			(*it)->_parent = NULL;
			_children.erase(it);
		}
	}
}
	
Node * Group::nextChildOf(Node * node) {
	NodeList::iterator it = std::find(_children.begin(), _children.end(), node);
	NodeList::iterator nextIt = it + 1;
	if (it!=_children.end() && nextIt!=_children.end()) {
		return (*nextIt).getPtr();
	}
	return nullptr;
}

Node * Group::prevChildOf(Node * node) {
	NodeList::iterator it = std::find(_children.begin(), _children.end(), node);
	NodeList::iterator prevIt = it - 1;
	if (it!=_children.begin()) {
		return (*prevIt).getPtr();
	}
	return nullptr;
}

bool Group::swapChildren(Node * childA, Node * childB) {
	NodeList::iterator iterA = std::find(_children.begin(), _children.end(), childA);
	NodeList::iterator iterB = std::find(_children.begin(), _children.end(), childB);
	
	if (iterA!=_children.end() && iterB!=_children.end() && iterA!=iterB) {
		std::iter_swap(iterA, iterB);
		return true;
	}
	
	return false;
}

bool Group::moveChildNextTo(Node * nextToNode, Node * movingNode) {
	NodeList::iterator nextToIterator = std::find(_children.begin(), _children.end(), nextToNode);
	NodeList::iterator movingIterator = std::find(_children.begin(), _children.end(), movingNode);
	nextToIterator++;
	
	if (nextToIterator!=_children.end() && movingIterator!=_children.end() && nextToIterator!=movingIterator) {
		ptr<Node> movingNodePtr = movingNode;
		_children.erase(movingIterator);
		
		_children.insert(nextToIterator, movingNodePtr.getPtr());
		
		return true;
	}
	return false;
}

}
