
#include <vwgl/plane_gizmo.hpp>
#include <vwgl/system.hpp>

#include <vwgl/physics/ray.hpp>
#include <vwgl/physics/plane.hpp>
#include <vwgl/physics/intersection.hpp>

namespace vwgl {

Quaternion rotationBetweenPoints(Vector3 p1, Vector3 p2, Vector3 origin, float inc = 0.0f) {
	Vector3 axis(0.0f, 1.0f, 0.0);
	Vector3 v1(p2);
	v1.sub(origin).normalize();
	Vector3 v2(p1);
	v2.sub(origin).normalize();
	float dot = v1.dot(v2);
	
	float alpha = Math::acos(dot);
	if (alpha>=inc || inc==0) {
		if (inc!=0) {
			alpha = (alpha>=2*inc) ? 2*inc:inc;
		}
		float sign = axis.dot(v1.cross(v2));
		if (sign<0.0) alpha *= -1.0;
		Quaternion q(alpha,axis.x(),axis.y(),axis.z());
		q.normalize();
		
		if (!isnan(q.x())) {
			return q;
		}
	}
	return Quaternion(0.0,0.0,1.0,0.0);
}

PlaneGizmo::PlaneGizmo() :Gizmo(System::get()->getResourcesPath() + "manipulator.vwglb") {
	loadPickIds();
}

PlaneGizmo::PlaneGizmo(const std::string & model) :Gizmo(model) {
	loadPickIds();
}
	
PlaneGizmo::~PlaneGizmo() {
		
}
	
void PlaneGizmo::beginMouseEvents(const Position2Di & pos, vwgl::Camera * camera, const Viewport & vp, const Vector3 & gizmoPos, TransformNode * trx) {
	_camera = camera;
	_viewport = vp;
	_transformNode = trx;
	
	physics::Ray ray = vwgl::physics::Ray::rayWithScreenPoint(pos, _camera->getProjectionMatrix(), _camera->getViewMatrix(), _viewport);
	vwgl::physics::Plane plane;
	plane.setOrigin(gizmoPos);
	plane.setNormal(vwgl::Vector3(0.0, 1.0, 0.0));
	_plane = plane;
	physics::RayToPlaneIntersection intersection = vwgl::physics::Intersection::rayToPlane(ray,plane);
	
	if (intersection.intersects()) {
		_lastPickPoint = intersection.getRay().getEnd();
		Matrix4 transform = _transformNode->getTransform();
		_translateOffset = Vector3(transform.getPosition());
		_translateOffset.sub(intersection.getRay().getEnd());
	}
}

void PlaneGizmo::mouseEvent(const vwgl::Position2Di & pos) {
	physics::Ray ray = physics::Ray::rayWithScreenPoint(pos, _camera->getProjectionMatrix(), _camera->getViewMatrix(), _viewport);
	physics::RayToPlaneIntersection intersection = physics::Intersection::rayToPlane(ray, _plane);
	
	if (intersection.intersects()) {
		Matrix4 transform(_transformNode->getTransform());
		Matrix4 rotation = transform.getRotation();
		Vector3 origin = transform.getPosition();
		Vector3 intersectPoint = intersection.getPoint();
		
		if (_mode==Mode::kGizmoMove) {
			float y = origin.y();
			transform = Matrix4::makeTranslation(intersectPoint.x() + _translateOffset.x(),
												 y,
												 intersectPoint.z() + _translateOffset.z());
			transform.mult(rotation);
			_lastPickPoint = intersectPoint;
		}
		else if (_mode==Mode::kGizmoRotate) {
			Quaternion rot = rotationBetweenPoints(_lastPickPoint, intersectPoint, origin, Math::degreesToRadians(22.5));
			if (rot.x()!=0 || rot.y()!=0 || rot.z()!=0 || rot.w()!=1) {
				transform.mult(rot.getMatrix4());
				_lastPickPoint = intersectPoint;
			}
		}
		else if (_mode==Mode::kGizmoRotateFine) {
			Quaternion rot = rotationBetweenPoints(_lastPickPoint, intersectPoint, origin);
			if (rot.x()!=0 || rot.y()!=0 || rot.z()!=0 || rot.w()!=1) {
				transform.mult(rot.getMatrix4());
				_lastPickPoint = intersectPoint;
			}
		}
		
		_transformNode->setTransform(transform);
	}
}

void PlaneGizmo::endMouseEvents() {

}

void PlaneGizmo::loadPickIds() {
	Solid * move = _drawable->getSolid("Material-translate");
	Solid * rotate = _drawable->getSolid("Material-rotate");
	Solid * rotateFine = _drawable->getSolid("Material-rotate_fine");
	
	_moveId = move->getPickId();
	_rotateId = rotate->getPickId();
	_rotateFineId = rotateFine->getPickId();
}

}
