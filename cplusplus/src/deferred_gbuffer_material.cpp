
#include <vwgl/deferred_gbuffer_material.hpp>
#include <vwgl/graphics.hpp>
#include <vwgl/texture_manager.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {

DeferredGBufferMaterial::DeferredGBufferMaterial()
{
	initShader();
}
	
DeferredGBufferMaterial::~DeferredGBufferMaterial() {
	
}
	
void DeferredGBufferMaterial::initShader() {
	getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_gbuffer_mat.vsh");
	getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_gbuffer_mat.fsh");
	getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_Diffuse",0);
	getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_Normal", 1);
	getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_Specular", 2);
	getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_Position", 3);
	getShader()->link();
	
	loadVertexAttrib("in_Position");
	loadNormalAttrib("in_Normal");
	loadTexCoord0Attrib("in_TexCoord0");
	loadTexCoord1Attrib("in_TexCoord1");
	loadTangentArray("in_Tangent");
	
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uVMatrix");
	getShader()->initUniformLocation("uMMatrix");
	getShader()->initUniformLocation("uNMatrix");
	getShader()->initUniformLocation("uVMatrixInv");
	
	getShader()->initUniformLocation("uDiffuse");
	getShader()->initUniformLocation("uDiffuseScale");
	getShader()->initUniformLocation("uDiffuseOffset");
	getShader()->initUniformLocation("uAlphaCutoff");
	getShader()->initUniformLocation("uColorTint");
	getShader()->initUniformLocation("uReflectionAmount");
	getShader()->initUniformLocation("uCubeMap");
	getShader()->initUniformLocation("uUseCubemap");
	
	getShader()->initUniformLocation("uNormalMap");
	getShader()->initUniformLocation("uNormalMapScale");
	getShader()->initUniformLocation("uNormalMapOffset");
	
	getShader()->initUniformLocation("uSpecularColor");
	getShader()->initUniformLocation("uShininess");
	getShader()->initUniformLocation("uShininessMask");
	getShader()->initUniformLocation("uShininessMaskComponent");
	getShader()->initUniformLocation("uShininessMaskInvert");
	
	
	getShader()->initUniformLocation("uLightEmission");
	getShader()->initUniformLocation("uLightEmissionMask");
	getShader()->initUniformLocation("uLightEmissionMaskChannel");
	getShader()->initUniformLocation("uLightEmissionMaskInvert");
	
	getShader()->initUniformLocation("uSelected");
}

void DeferredGBufferMaterial::setupUniforms() {
	getShader()->setUniform("uPMatrix", State::get()->projectionMatrix());
	getShader()->setUniform("uVMatrix", State::get()->viewMatrix());
	getShader()->setUniform("uMMatrix", State::get()->modelMatrix());
	getShader()->setUniform("uNMatrix", State::get()->normalMatrix());
	Matrix4 vMatrix(viewMatrix());
	vMatrix.invert();
	getShader()->setUniform("uVMatrixInv", vMatrix);
	
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		Texture * tex = mat->getTexture() ? mat->getTexture():TextureManager::get()->whiteTexture();
		getShader()->setUniform("uDiffuse", tex, Texture::kTexture0);
		getShader()->setUniform("uDiffuseScale", mat->getTextureScale());
		getShader()->setUniform("uDiffuseOffset", mat->getTextureOffset());
		getShader()->setUniform("uAlphaCutoff", mat->getAlphaCutoff());
		
		getShader()->setUniform("uColorTint", mat->getDiffuse());
		getShader()->setUniform("uReflectionAmount", mat->getReflectionAmount());
		if (mat->getCubeMap()) {
			getShader()->setUniform("uUseCubemap", true);
			getShader()->setUniform("uCubeMap", mat->getCubeMap(),vwgl::Texture::kTexture4);
			getShader()->setUniform("uReflectionAmount", mat->getReflectionAmount());
		}
		else {
			getShader()->setUniform("uUseCubemap", false);
			Texture::setActiveTexture(vwgl::Texture::kTexture4);
			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
			getShader()->setUniform("uCubeMap", 4);
			getShader()->setUniform("uReflectionAmount", 0.0f);
		}
		
		
		tex = mat->getNormalMap() ? mat->getNormalMap():TextureManager::get()->transparentTexture();
		getShader()->setUniform("uNormalMap", tex, Texture::kTexture1);
		getShader()->setUniform("uNormalMapScale", mat->getNormalMapScale());
		getShader()->setUniform("uNormalMapOffset", mat->getNormalMapOffset());
		
		tex = mat->getShininessMask() ? mat->getShininessMask():TextureManager::get()->blackTexture();
		getShader()->setUniform("uSpecularColor", mat->getSpecular());
		getShader()->setUniform("uShininess", mat->getShininess());
		getShader()->setUniform("uShininessMask", tex, Texture::kTexture2);
		getShader()->setUniform("uShininessMaskComponent", mat->getShininessMaskChannel());
		getShader()->setUniform("uShininessMaskInvert", mat->getInvertShininessMask() ? 0.0f:1.0f);
		
		tex = mat->getLightEmissionMask() ? mat->getLightEmissionMask():TextureManager::get()->blackTexture();
		getShader()->setUniform("uLightEmission", mat->getLightEmission());
		getShader()->setUniform("uLightEmissionMask", tex, Texture::kTexture3);
		getShader()->setUniform("uLightEmissionMaskChannel", mat->getLightEmissionMaskChannel());
		getShader()->setUniform("uLightEmissionMaskInvert", mat->getInvertLightEmissionMask() ? 1.0f:0.0f);

		Color invClear = Color::white() - _clearColor;
		getShader()->setUniform("uSelected", mat->getSelectedMode() ? invClear:_clearColor);
	}
}
	
}