
#include <vwgl/generic_material.hpp>
#include <vwgl/light.hpp>
#include <vwgl/shadow_light.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/solid.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/system.hpp>
#include <vwgl/scene/light.hpp>

#include <JsonBox.h>
#include <vwgl/camera.hpp>
#include <vwgl/render_settings.hpp>
#include <vwgl/graphics.hpp>

#include <algorithm>

namespace vwgl {

static std::string s_generic_material_empty_string = "";

template <typename T>
void remove_duplicates(std::vector<T>& vec) {
	std::sort(vec.begin(), vec.end());
	vec.erase(std::unique(vec.begin(), vec.end()), vec.end());
}
	
//	Vector4 _diffuse;
//	float _shininess;
//	float _refractionAmount;
//	ptr<Texture> _texture;
//	ptr<Texture> _lightMap;
//	ptr<Texture> _normalMap;

class MaterialJsonParser {
public:
	static float getColorComponent(JsonBox::Value & val, float defaultValue = 1.0f) {
		if (val.isDouble()) return (float)val.getDouble();
		else if (val.isInteger()) return (float)val.getInt();
		else return defaultValue;
	}
	
	static int getIntValue(JsonBox::Value & val, int defaultValue = 0) {
		if (val.isDouble()) return static_cast<int>(val.getDouble());
		else if (val.isInteger()) return val.getInt();
		else if (val.isBoolean()) return val.getBoolean() ? 1:0;
		else return defaultValue;
	}
	
	static bool getBooleanValue(JsonBox::Value & val, bool defaultValue = true) {
		if (val.isBoolean()) return val.getBoolean();
		else if (val.isInteger()) return val.isInteger()!=0;
		else return defaultValue;
	}

	static Vector4 getColor(JsonBox::Value & r, float defR, JsonBox::Value & g, float defG, JsonBox::Value & b, float defB, JsonBox::Value & a, float defA) {
		return Vector4(getColorComponent(r,1.0f),
					   getColorComponent(g,1.0f),
					   getColorComponent(b,1.0f),
					   getColorComponent(a,1.0f));
	}

	static Texture * getTexture(JsonBox::Value & js_tex, const std::string & path) {
		ptr<Texture> tex = NULL;
		
		if (js_tex.isString()) {
			std::string fileName = js_tex.getString();
			if (!vwgl::System::get()->isAbsolutePath(fileName)) {
				fileName = path + fileName;
			}
			tex = TextureManager::get()->loadTexture(Loader::get()->loadImage(fileName));
		}
		
		return tex.release();
	}

	static void configureMaterial(JsonBox::Value & jsMat, GenericMaterial * mat, const std::string & path) {
		if (jsMat.isObject()) {
			Vector4 diffuseColor = getColor(jsMat["colorTintR"], 1.0f,
											jsMat["colorTintG"], 1.0f,
											jsMat["colorTintB"], 1.0f,
											jsMat["colorTintA"], 1.0f);
			diffuseColor =	getColor(jsMat["diffuseR"], diffuseColor.r(),
									 jsMat["diffuseG"], diffuseColor.g(),
									 jsMat["diffuseB"], diffuseColor.b(),
									 jsMat["diffuseA"], diffuseColor.a());
			
			Vector4 specularcolor = getColor(jsMat["specularR"], 1.0f,
											jsMat["specularG"], 1.0f,
											jsMat["specularB"], 1.0f,
											jsMat["specularA"], 1.0f);
			
			mat->setDiffuse(diffuseColor);
			mat->setSpecular(specularcolor);
			mat->setShininess(getColorComponent(jsMat["shininess"],0.0f));
			mat->setTexture(getTexture(jsMat["texture"],path));
			mat->setLightMap(getTexture(jsMat["lightmap"],path));
			mat->setNormalMap(getTexture(jsMat["normalMap"],path));
			mat->setLightNormalMap(getTexture(jsMat["lightNormalMap"],path));
			
			// castShadows
			mat->setCastShadows(getBooleanValue(jsMat["castShadows"],true));
			mat->setReceiveShadows(getBooleanValue(jsMat["receiveShadows"],true));
			mat->setReceiveProjections(getBooleanValue(jsMat["receiveProjections"],false));
			mat->setCullFace(getBooleanValue(jsMat["cullFace"],true));

			// receiveShadows
			mat->setRefractionAmount(getColorComponent(jsMat["refractionAmount"],0.0f));
			mat->setLightEmission(getColorComponent(jsMat["lightEmission"],0.0f));
			
			float offx = getColorComponent(jsMat["textureOffsetX"],0.0f);
			float offy = getColorComponent(jsMat["textureOffsetY"],0.0f);
			float scalex = getColorComponent(jsMat["textureScaleX"],1.0f);
			float scaley = getColorComponent(jsMat["textureScaleY"],1.0f);
			mat->setTextureOffset(Vector2(offx,offy));
			mat->setTextureScale(Vector2(scalex,scaley));
			
			offx = getColorComponent(jsMat["lightmapOffsetX"],0.0f);
			offy = getColorComponent(jsMat["lightmapOffsetY"],0.0f);
			scalex = getColorComponent(jsMat["lightmapScaleX"],1.0f);
			scaley = getColorComponent(jsMat["lightmapScaleY"],1.0f);
			mat->setLightmapOffset(Vector2(offx,offy));
			mat->setLightmapScale(Vector2(scalex,scaley));
			
			offx = getColorComponent(jsMat["normalMapOffsetX"],0.0f);
			offy = getColorComponent(jsMat["normalMapOffsetY"],0.0f);
			scalex = getColorComponent(jsMat["normalMapScaleX"],1.0f);
			scaley = getColorComponent(jsMat["normalMapScaleY"],1.0f);
			mat->setNormalMapOffset(Vector2(offx,offy));
			mat->setNormalMapScale(Vector2(scalex,scaley));
			
			mat->setReflectionAmount(getColorComponent(jsMat["reflectionAmount"],0.0f));

			mat->setAlphaCutoff(getColorComponent(jsMat["alphaCutoff"], 0.5f));
			
			int shininessMaskChannel = getIntValue(jsMat["shininessMaskChannel"],0);
			bool invertShininessMask = getBooleanValue(jsMat["invertShininessMask"],false);
			mat->setShininessMask(getTexture(jsMat["shininessMask"], path), shininessMaskChannel, invertShininessMask);
			int lightEmissionMaskChannel = getIntValue(jsMat["lightEmissionMaskChannel"],0);
			bool invertLightEmissionMask = getBooleanValue(jsMat["invertLightEmissionMask"],false);
			mat->setLightEmissionMask(getTexture(jsMat["lightEmissionMask"], path), lightEmissionMaskChannel, invertLightEmissionMask);

		}
	}
	
	static void applyJson(Solid * solid, JsonBox::Value & material, const std::string & absPath) {
		ptr<GenericMaterial> mat = new GenericMaterial();
		solid->setMaterial(mat.getPtr());
		configureMaterial(material, mat.getPtr(), absPath);
	}

	static void applyJson(GenericMaterial * material, JsonBox::Value & jsonMaterial, const std::string & absPath) {
		if (material) {
			configureMaterial(jsonMaterial, material, absPath);
		}
	}
};


GenericMaterial::GenericMaterial()
	:Material(),
	_diffuse(Color::white()),
	_specular(Color::white()),
	_shininess(0.0f),
	_textureScale(1.0f),
	_lightmapScale(1.0f),
	_normalMapScale(1.0f),
	_refractionAmount(0.0f),
	_lightEmission(0.0f),
	_castShadows(true),
	_receiveShadows(true),
	_selectedMode(false),
	_receiveProjections(false),
	_reflectionAmount(0.0f),
	_alphaCutoff(0.5f),
	_shininessMaskChannel(0),
	_shininessMaskInvert(false),
	_lightEmissionMaskChannel(0),
	_lightEmissionMaskInvert(false)
{
	setReuseShaderKey("generic_material_shader");
	initShader();
}

GenericMaterial::~GenericMaterial() {
	destroy();
}
	
Material * GenericMaterial::clone() {
	ptr<GenericMaterial> newMat = new GenericMaterial();
	
	cloneContent(newMat.getPtr());
	
	return newMat.release();
}
	
void GenericMaterial::cloneContent(vwgl::Material *newMat) {
	Material::cloneContent(newMat);
	
	GenericMaterial * newGenMat = dynamic_cast<GenericMaterial*>(newMat);
	if (newGenMat) {
		applyMaterialSettings(newGenMat, ~0);
	}
}

void GenericMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL || Graphics::get()->getApi()==vwgl::Graphics::kApiOpenGLBasic) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "generic.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "generic.fsh");
	}
	else if (Graphics::get()->getApi()==vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "generic.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "generic.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: generic material shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("generic_material");
	
	loadVertexAttrib("aVertexPosition");
	loadNormalAttrib("aVertexNormal");
	loadTangentArray("aVertexTangent");
	loadTexCoord0Attrib("aTextureCoord");
	loadTexCoord1Attrib("aLightmapCoord");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uNMatrix");
	getShader()->initUniformLocation("uVMatrix");
	getShader()->initUniformLocation("uMMatrix");
	getShader()->initUniformLocation("uVMatrixInv");

	getShader()->initUniformLocation("uDiffuseColor");
	getShader()->initUniformLocation("uSpecularColor");
	getShader()->initUniformLocation("uShininess");
	getShader()->initUniformLocation("uLightEmission");
	getShader()->initUniformLocation("uAlphaCutoff");
	getShader()->initUniformLocation("uUseTexture");
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uLightMap");
	getShader()->initUniformLocation("uUseNormalMap");
	getShader()->initUniformLocation("uNormalMap");
	getShader()->initUniformLocation("uNormalMapOffset");
	getShader()->initUniformLocation("uNormalMapScale");

	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
	getShader()->initUniformLocation("uLightmapOffset");
	getShader()->initUniformLocation("uLightmapScale");
	
	getShader()->initUniformLocation("uCubeMap");
	getShader()->initUniformLocation("uUseCubeMap");
	getShader()->initUniformLocation("uReflectionAmount");
	getShader()->initUniformLocation("uAlphaCutoff");
	
	getShader()->initUniformLocation("uAmbientLight");
	getShader()->initUniformLocation("uDiffuseLight");
	getShader()->initUniformLocation("uSpecularLight");
	getShader()->initUniformLocation("uLightType");
	getShader()->initUniformLocation("uLightPosition");
	getShader()->initUniformLocation("uLightDirection");
	
	getShader()->initUniformLocation("uAttenuation");
	getShader()->initUniformLocation("uSpotCutoff");
	getShader()->initUniformLocation("uSpotExponent");
	getShader()->initUniformLocation("uSpotDirection");
	
	getShader()->initUniformLocation("uSelectMode");
	
	getShader()->initUniformLocation("uHue");
	getShader()->initUniformLocation("uSaturation");
	getShader()->initUniformLocation("uLightness");
	getShader()->initUniformLocation("uBrightness");
	getShader()->initUniformLocation("uContrast");
}

void GenericMaterial::setupUniforms() {
	GenericMaterial * settings = (_settingsMaterial.valid()) ? _settingsMaterial.getPtr():this;

	getShader()->setUniform("uMVMatrix", modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());
	getShader()->setUniform("uNMatrix", normalMatrix());
	getShader()->setUniform("uVMatrix", viewMatrix());
	getShader()->setUniform("uMMatrix", modelMatrix());
	vwgl::Matrix4 vMat = viewMatrix();
	vMat.invert();
	getShader()->setUniform("uVMatrixInv", vMat);
	
	getShader()->setUniform("uDiffuseColor", settings->_diffuse);
	getShader()->setUniform("uSpecularColor", settings->_specular);
	
	getShader()->setUniform("uShininess", settings->_shininess);
	getShader()->setUniform("uLightEmission", settings->_lightEmission);

	Texture * whiteTexture = TextureManager::get()->whiteTexture();
	
	getShader()->setUniform("uTexture", settings->_texture.getPtr(), whiteTexture, Texture::kTexture0);

	getShader()->setUniform("uLightMap", settings->_lightMap.getPtr(), whiteTexture, Texture::kTexture1);

	if (settings->_normalMap.valid()) {
		getShader()->setUniform("uUseNormalMap", settings->_normalMap->valid());
		getShader()->setUniform("uNormalMap", settings->_normalMap.getPtr(), Texture::kTexture2);
	}
	else {
		getShader()->setUniform("uUseNormalMap", false);
	}
	
	if (settings->_cubeMap.valid()) {
		getShader()->setUniform("uUseCubeMap", true);
		getShader()->setUniform("uCubeMap", settings->_cubeMap.getPtr(), Texture::kTexture3);
	}
	else {
		getShader()->setUniform("uUseCubeMap", false);
		Texture::setActiveTexture(Texture::kTexture3);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		getShader()->setUniform("uCubeMap", 3);
	}
	getShader()->setUniform("uReflectionAmount", settings->_reflectionAmount);
	getShader()->setUniform("uAlphaCutoff", settings->_alphaCutoff);
	
	vwgl::ILight * l = nullptr;
	if (scene::Light::getLightSources().size()>0) {
		l = scene::Light::getLightSources().front();
	}
	else if (LightManager::get()->getLightSourcesCount()>0) {
		l = LightManager::get()->getLightList()[0];
	}

	if (l) {
		if (_light.valid()) {
			l = _light.getPtr();
		}
		Matrix4 viewMatrix = Matrix4::makeIdentity();
		if (CameraManager::get()->getMainCamera()) {
			viewMatrix = CameraManager::get()->getMainCamera()->getViewMatrix();
		}

		Vector4 lightPos(l->getPosition(),1.0);
		lightPos = viewMatrix.multVector(lightPos);
		
		getShader()->setUniform("uAmbientLight", l->getAmbient());
		getShader()->setUniform("uDiffuseLight", l->getDiffuse());
		getShader()->setUniform("uSpecularLight", l->getSpecular());
		getShader()->setUniform("uLightType", l->getType());
		getShader()->setUniform("uLightPosition", lightPos);
		getShader()->setUniform("uLightDirection", l->getDirection());
		
		
		Vector3 attenuation(l->getConstantAttenuation(),
							l->getLinearAttenuation(),
							l->getExpAttenuation());
		getShader()->setUniform("uAttenuation",attenuation);
		getShader()->setUniform("uSpotDirection",l->getSpotDirection());
		getShader()->setUniform("uSpotExponent",l->getSpotExponent());
		getShader()->setUniform("uSpotCutoff",l->getSpotCutoff());
		
	}

	getShader()->setUniform("uTextureOffset",settings->_textureOffset);
	getShader()->setUniform("uTextureScale",settings->_textureScale);
	getShader()->setUniform("uLightmapOffset",settings->_lightmapOffset);
	getShader()->setUniform("uLightmapScale",settings->_lightmapScale);
	getShader()->setUniform("uNormalMapOffset", settings->_normalMapOffset);
	getShader()->setUniform("uNormalMapScale", settings->_normalMapScale);
	getShader()->setUniform("uAlphaCutoff", settings->_alphaCutoff);
	
	getShader()->setUniform("uSelectMode", _selectedMode);
	
	RenderSettings * renderSettings = RenderSettings::getCurrentRenderSettings();
	if (renderSettings) {
		getShader()->setUniform("uHue",renderSettings->getHue());
		getShader()->setUniform("uSaturation",renderSettings->getSaturation());
		getShader()->setUniform("uLightness",renderSettings->getLightness());
		getShader()->setUniform("uBrightness",renderSettings->getBrightness());
		getShader()->setUniform("uContrast",renderSettings->getContrast());
	}
	else {
		getShader()->setUniform("uHue",1.0f);
		getShader()->setUniform("uSaturation",1.0f);
		getShader()->setUniform("uLightness",1.0f);
		getShader()->setUniform("uBrightness",0.5f);
		getShader()->setUniform("uContrast",0.5f);
	}
}
	
const std::string & GenericMaterial::getTexturePath() const {
	if (_texture.valid()) {
		return _texture->getFileName();
	}
	return s_generic_material_empty_string;
}
	
const std::string & GenericMaterial::getLightMapPath() const {
	if (_lightMap.valid()) {
		return _lightMap->getFileName();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getNormalMapPath() const {
	if (_normalMap.valid()) {
		return _normalMap->getFileName();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getLightNormalMapPath() const {
	if (_lightNormalMap.valid()) {
		return _lightNormalMap->getFileName();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getCubeMapPosXPath() const {
	if (_cubeMap.valid()) {
		return _cubeMap->getPositiveXFile();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getCubeMapNegXPath() const {
	if (_cubeMap.valid()) {
		return _cubeMap->getNegativeXFile();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getCubeMapPosYPath() const {
	if (_cubeMap.valid()) {
		return _cubeMap->getPositiveYFile();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getCubeMapNegYPath() const {
	if (_cubeMap.valid()) {
		return _cubeMap->getNegativeYFile();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getCubeMapPosZPath() const {
	if (_cubeMap.valid()) {
		return _cubeMap->getPositiveZFile();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getCubeMapNegZPath() const {
	if (_cubeMap.valid()) {
		return _cubeMap->getNegativeZFile();
	}
	return s_generic_material_empty_string;
}

const std::string & GenericMaterial::getShininessMaskPath() const {
	if (_shininessMask.valid()) {
		return _shininessMask->getFileName();
	}
	return s_generic_material_empty_string;
}
	
const std::string & GenericMaterial::getLightEmissionMaskPath() const {
	if (_lightEmissionMask.valid()) {
		return _lightEmissionMask->getFileName();
	}
	return s_generic_material_empty_string;
}

void GenericMaterial::destroy() {
	_texture = NULL;
	_lightMap = NULL;
	_normalMap = NULL;
}
	
void GenericMaterial::applySettings(Drawable * drawable, unsigned int settings) {
	SolidList slist = drawable->getSolidList();
	SolidList::iterator it;
	for (it=slist.begin(); it!=slist.end(); ++it) {
		Solid * s = (*it).getPtr();
		applyMaterialSettings(dynamic_cast<GenericMaterial*>(s->getMaterial()),settings);
	}
}
	
void GenericMaterial::applyModifier(const MaterialModifier & mod, const std::string & resourcePath, bool forceResourcePath) {
	if (mod.isEnabled(MaterialModifier::kDiffuse)) setDiffuse(mod.getDiffuse());
	if (mod.isEnabled(MaterialModifier::kSpecular)) setSpecular(mod.getSpecular());
	if (mod.isEnabled(MaterialModifier::kShininess)) setShininess(mod.getShininess());
	if (mod.isEnabled(MaterialModifier::kLightEmission)) setLightEmission(mod.getLightEmission());
	if (mod.isEnabled(MaterialModifier::kRefractionAmount)) setRefractionAmount(mod.getRefractionAmount());
	if (mod.isEnabled(MaterialModifier::kReflectionAmount)) setReflectionAmount(mod.getReflectionAmount());
	if (mod.isEnabled(MaterialModifier::kTexture)) {
		std::string path = mod.getTexture();
		std::string absolutePath;
		getTextureAbsolutePath(path, absolutePath, resourcePath, forceResourcePath);
		setTexture(vwgl::Loader::get()->loadTexture(absolutePath));
	}
	if (mod.isEnabled(MaterialModifier::kLightmap)) {
		std::string path = mod.getLightMap();
		std::string absolutePath;
		getTextureAbsolutePath(path, absolutePath, resourcePath, forceResourcePath);
		setLightMap(vwgl::Loader::get()->loadTexture(absolutePath));
	}
	if (mod.isEnabled(MaterialModifier::kNormalMap)) {
		std::string path = mod.getNormalMap();
		std::string absolutePath;
		getTextureAbsolutePath(path, absolutePath, resourcePath, forceResourcePath);
		setNormalMap(vwgl::Loader::get()->loadTexture(absolutePath));
	}
	if (mod.isEnabled(MaterialModifier::kLightNormalMap)) {
		std::string path = mod.getLightNormalMap();
		std::string absolutePath;
		getTextureAbsolutePath(path, absolutePath, resourcePath, forceResourcePath);
		setLightNormalMap(vwgl::Loader::get()->loadTexture(absolutePath));
	}
	if (mod.isEnabled(MaterialModifier::kTextureScale)) setTextureScale(mod.getTextureScale());
	if (mod.isEnabled(MaterialModifier::kTextureOffset)) setTextureOffset(mod.getTextureOffset());
	if (mod.isEnabled(MaterialModifier::kLightmapScale)) setLightmapScale(mod.getLightmapScale());
	if (mod.isEnabled(MaterialModifier::kLightmapOffset)) setLightmapOffset(mod.getLightmapOffset());
	if (mod.isEnabled(MaterialModifier::kNormalMapScale)) setNormalMapScale(mod.getNormalMapScale());
	if (mod.isEnabled(MaterialModifier::kNormalMapOffset)) setNormalMapOffset(mod.getNormalMapOffset());
	if (mod.isEnabled(MaterialModifier::kCubeMap)) {
		std::string posx, negx, posy, negy, posz, negz;
		getTextureAbsolutePath(mod.getCubeMapPosX(), posx, resourcePath, forceResourcePath);
		getTextureAbsolutePath(mod.getCubeMapNegX(), negx, resourcePath, forceResourcePath);
		getTextureAbsolutePath(mod.getCubeMapPosY(), posy, resourcePath, forceResourcePath);
		getTextureAbsolutePath(mod.getCubeMapNegY(), negy, resourcePath, forceResourcePath);
		getTextureAbsolutePath(mod.getCubeMapPosZ(), posz, resourcePath, forceResourcePath);
		getTextureAbsolutePath(mod.getCubeMapNegZ(), negz, resourcePath, forceResourcePath);
		setCubeMap(vwgl::Loader::get()->loadCubemap(posx,negx,posy,negy,posz,negz));
	}
	if (mod.isEnabled(MaterialModifier::kCastShadows)) setCastShadows(mod.getCastShadows());
	if (mod.isEnabled(MaterialModifier::kReceiveShadows)) setReceiveShadows(mod.getReceiveShadows());
	if (mod.isEnabled(MaterialModifier::kReceiveProjections)) setReceiveProjections(mod.getReceiveProjections());
	if (mod.isEnabled(MaterialModifier::kAlphaCutoff)) setAlphaCutoff(mod.getAlphaCutoff());
	if (mod.isEnabled(MaterialModifier::kShininessMask)) {
		std::string path = mod.getShininessMask();
		std::string absolutePath;
		getTextureAbsolutePath(path, absolutePath, resourcePath, forceResourcePath);
		setShininessMask(vwgl::Loader::get()->loadTexture(absolutePath),
						 mod.getShininessMaskChannel(),
						 mod.getInvertShininessMask());
	}
	if (mod.isEnabled(MaterialModifier::kLightEmissionMask)) {
		std::string path = mod.getLightEmissionMask();
		std::string absolutePath;
		getTextureAbsolutePath(path, absolutePath, resourcePath, forceResourcePath);
		setLightEmissionMask(vwgl::Loader::get()->loadTexture(absolutePath),
						mod.getLightEmissionMaskChannel(),
						mod.getInvertLightEmissionMask());
	}
}

vwgl::MaterialModifier GenericMaterial::getModifierWithMask(unsigned int modifierMask) {
	vwgl::MaterialModifier mod;
	
	mod.setFlags(modifierMask);
	if (mod.isEnabled(MaterialModifier::kDiffuse)) mod.setDiffuse(getDiffuse());
	if (mod.isEnabled(MaterialModifier::kSpecular)) mod.setSpecular(getSpecular());
	if (mod.isEnabled(MaterialModifier::kShininess)) mod.setShininess(getShininess());
	if (mod.isEnabled(MaterialModifier::kLightEmission)) mod.setLightEmission(getLightEmission());
	if (mod.isEnabled(MaterialModifier::kRefractionAmount)) mod.setRefractionAmount(getRefractionAmount());
	if (mod.isEnabled(MaterialModifier::kReflectionAmount)) mod.setReflectionAmount(getReflectionAmount());
	if (mod.isEnabled(MaterialModifier::kTexture)) mod.setTexture(getTexturePath());
	if (mod.isEnabled(MaterialModifier::kLightmap)) mod.setLightMap(getLightMapPath());
	if (mod.isEnabled(MaterialModifier::kNormalMap)) mod.setNormalMap(getNormalMapPath());
	if (mod.isEnabled(MaterialModifier::kLightNormalMap)) mod.setLightNormalMap(getLightNormalMapPath());
	if (mod.isEnabled(MaterialModifier::kTextureScale)) mod.setTextureScale(getTextureScale());
	if (mod.isEnabled(MaterialModifier::kTextureOffset)) mod.setTextureOffset(getTextureOffset());
	if (mod.isEnabled(MaterialModifier::kLightmapScale)) mod.setLightmapScale(getLightmapScale());
	if (mod.isEnabled(MaterialModifier::kLightmapOffset)) mod.setLightmapOffset(getLightmapOffset());
	if (mod.isEnabled(MaterialModifier::kNormalMapScale)) mod.setNormalMapScale(getNormalMapScale());
	if (mod.isEnabled(MaterialModifier::kNormalMapOffset)) mod.setNormalMapOffset(getNormalMapOffset());
	if (mod.isEnabled(MaterialModifier::kCubeMap)) {
		mod.setCubeMapPosX(getCubeMapPosXPath());
		mod.setCubeMapNegX(getCubeMapNegXPath());
		mod.setCubeMapPosY(getCubeMapPosYPath());
		mod.setCubeMapNegY(getCubeMapNegYPath());
		mod.setCubeMapPosZ(getCubeMapPosZPath());
		mod.setCubeMapNegZ(getCubeMapNegZPath());
	}
	if (mod.isEnabled(MaterialModifier::kCastShadows)) mod.setCastShadows(getCastShadows());
	if (mod.isEnabled(MaterialModifier::kReceiveShadows)) mod.setReceiveShadows(getReceiveShadows());
	if (mod.isEnabled(MaterialModifier::kReceiveProjections)) mod.setReceiveProjections(getReceiveProjections());
	if (mod.isEnabled(MaterialModifier::kAlphaCutoff)) mod.setAlphaCutoff(getAlphaCutoff());
	if (mod.isEnabled(MaterialModifier::kShininessMask))
		mod.setShininessMask(getShininessMaskPath(),getShininessMaskChannel(),getInvertShininessMask());
	if (mod.isEnabled(MaterialModifier::kLightEmissionMask))
		mod.setLightEmissionMask(getLightEmissionMaskPath(),getLightEmissionMaskChannel(),getInvertLightEmissionMask());
	
	return mod;
}
	
#if VWGL_IOS || VWGL_IOS_SIMULATOR
	
// Defined in generic_material.mm


#else
void GenericMaterial::setMaterialsWithJson(Drawable * drawable, const std::string & jsonString) {
	std::string currentDir = Loader::get()->getCurrentDir();
	
	JsonBox::Value js_materials;
	js_materials.loadFromString(jsonString);
	
	std::map<std::string,JsonBox::Value> materials;
	
	if (js_materials.isArray()) {
		JsonBox::Array matArray = js_materials.getArray();
		JsonBox::Array::iterator it;
		for (it=matArray.begin(); it!=matArray.end(); ++it) {
			JsonBox::Value name = (*it)["name"];
			if (name.isString()) {
				materials[name.getString()] = *it;
			}
		}
	}
	
	SolidList solidList = drawable->getSolidList();
	SolidList::iterator it;
	for (it=solidList.begin(); it!=solidList.end(); ++it) {
		Solid * s = (*it).getPtr();
		PolyList * p = s->getPolyList();
		JsonBox::Value matJson = materials[p->getMaterialName()];
		if (matJson.isObject()) {
			MaterialJsonParser::applyJson(s, matJson, currentDir);
		}
	}
}

void GenericMaterial::setMaterialsWithJson(scene::Drawable * drawable, const std::string & jsonString) {
	std::string currentDir = Loader::get()->getCurrentDir();
	
	JsonBox::Value js_materials;
	js_materials.loadFromString(jsonString);
	
	std::map<std::string,JsonBox::Value> materials;
	
	if (js_materials.isArray()) {
		JsonBox::Array matArray = js_materials.getArray();
		JsonBox::Array::iterator it;
		for (it=matArray.begin(); it!=matArray.end(); ++it) {
			JsonBox::Value name = (*it)["name"];
			if (name.isString()) {
				materials[name.getString()] = *it;
			}
		}
	}
	
	drawable->eachElement([&](PolyList * plist, Material * mat, Transform & trx) {
		JsonBox::Value matJson = materials[plist->getMaterialName()];
		if (matJson.isObject()) {
			MaterialJsonParser::applyJson(dynamic_cast<GenericMaterial*>(mat), matJson, currentDir);
		}
	});
}
#endif

void GenericMaterial::getMaterialResourcesWithJson(std::vector<std::string > & resources, const std::string & jsonString) {
	std::string currentDir = Loader::get()->getCurrentDir();
	
	JsonBox::Value js_materials;
	js_materials.loadFromString(jsonString);
		
	if (js_materials.isArray()) {
		JsonBox::Array matArray = js_materials.getArray();
		JsonBox::Array::iterator it;
		for (it=matArray.begin(); it!=matArray.end(); ++it) {
			JsonBox::Value tex = (*it)["texture"];
			JsonBox::Value lm = (*it)["lightmap"];
			JsonBox::Value nm = (*it)["normalMap"];
			JsonBox::Value lnm = (*it)["lightNormalMap"];
			JsonBox::Value sm = (*it)["shininessMask"];
			JsonBox::Value lem = (*it)["lightEmissionMask"];
			if (tex.isString() && tex.getString()!="") {
				resources.push_back(tex.getString());
			}
			if (lm.isString() && lm.getString()!="") {
				resources.push_back(lm.getString());
			}
			if (nm.isString() && nm.getString()!="") {
				resources.push_back(nm.getString());
			}
			if (lnm.isString() && lnm.getString()!="") {
				resources.push_back(lnm.getString());
			}
			if (sm.isString() && sm.getString()!="") {
				resources.push_back(sm.getString());
			}
			if (lem.isString() && lem.getString()!="") {
				resources.push_back(lem.getString());
			}
		}
	}
	
	remove_duplicates(resources);
}

void GenericMaterial::applyMaterialSettings(GenericMaterial * mat, unsigned int settings) {
	if (mat) {
		if (settings | kDiffuseColor) {
			mat->setDiffuse(_diffuse);
		}
		if (settings | kShininess) {
			mat->setShininess(_shininess);
		}
		if (settings | kRefractionAmount) {
			mat->setRefractionAmount(_refractionAmount);
		}
		if (settings | kLightEmission) {
			mat->setLightEmission(_lightEmission);
		}
		if (settings | kTexture) {
			mat->setTexture(_texture.getPtr());
		}
		if (settings | kLightMap) {
			mat->setLightMap(_lightMap.getPtr());
		}
		if (settings | kNormalMap) {
			mat->setNormalMap(_normalMap.getPtr());
		}
		if (settings | kTextureOffset) {
			mat->setTextureOffset(_textureOffset);
		}
		if (settings | kTextureScale) {
			mat->setTextureScale(_textureScale);
		}
		if (settings | kLightmapOffset) {
			mat->setLightmapOffset(_lightmapOffset);
		}
		if (settings | kLightmapScacle) {
			mat->setLightmapScale(_lightmapScale);
		}
		if (settings | kCastShadows) {
			mat->setCastShadows(_castShadows);
		}
		if (settings | kReceiveShadows) {
			mat->setReceiveShadows(_receiveShadows);
		}
		if (settings | kSelectedMode) {
			mat->setSelectedMode(_selectedMode);
		}
		if (settings | kAlphaCutoff) {
			mat->setAlphaCutoff(_alphaCutoff);
		}
		if (settings | kShininessMask) {
			mat->setShininessMask(_shininessMask.getPtr(),_shininessMaskChannel);
		}
		if (settings | kLightEmissionMask) {
			mat->setLightEmissionMask(_lightEmissionMask.getPtr(),_lightEmissionMaskChannel);
		}
		if (settings | kCubeMap) {
			mat->setCubeMap(_cubeMap.getPtr());
		}
	}
}

void GenericMaterial::getTextureAbsolutePath(const std::string & input, std::string & output, const std::string & resourcePath, bool forceResourcePath) {
	if (forceResourcePath) {
		std::string::size_type p = input.find_last_of("/\\");
		if (p!=std::string::npos) {
			output = input.substr(p+1);
		}
		if (resourcePath.back()!='/') {
			output = resourcePath + "/" + input;
		}
		else {
			output = resourcePath + input;
		}
	}
	else if (!System::get()->isAbsolutePath(input)) output = resourcePath + input;
	else output = input;
}

}

