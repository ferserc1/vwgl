
#include <vwgl/multimaterial_cube.hpp>

namespace vwgl {


MultimaterialCube::MultimaterialCube() :Drawable() {
	buildCube(1.0f, 1.0f, 1.0f);
}

MultimaterialCube::MultimaterialCube(float side) :Drawable() {
	buildCube(side, side, side);
}

MultimaterialCube::MultimaterialCube(float w, float h, float d) :Drawable() {
	buildCube(w, h, d);
}

MultimaterialCube::MultimaterialCube(vwgl::BoundingBox * bbox) {
	buildCube(bbox->size().x(), bbox->size().y(), bbox->size().z());
	vwgl::Matrix4 translate = vwgl::Matrix4::makeTranslation(bbox->center().x(), bbox->center().y(), bbox->center().z());
	applyTransform(translate);
}

MultimaterialCube::~MultimaterialCube() {
	
}

void MultimaterialCube::buildCube(float w, float h, float d) {
	_faces.push_back(getFace(w, h, d, 0));
	_faces.push_back(getFace(w, h, d, 1));
	_faces.push_back(getFace(w, h, d, 2));
	_faces.push_back(getFace(w, h, d, 3));
	_faces.push_back(getFace(w, h, d, 4));
	_faces.push_back(getFace(w, h, d, 5));
	
	addSolid(_faces[0].getPtr());
	addSolid(_faces[1].getPtr());
	addSolid(_faces[2].getPtr());
	addSolid(_faces[3].getPtr());
	addSolid(_faces[4].getPtr());
	addSolid(_faces[5].getPtr());
}

vwgl::Solid * MultimaterialCube::getFace(float w, float h, float d, int face) {
	float x = w / 2.0f;
	float y = h / 2.0f;
	float z = d / 2.0f;
	
	float v[] = {
		-x, y, z,
		-x, y,-z,
		-x,-y, z,
		
		-x,-y,-z,
		-x,-y, z,
		-x, y,-z,
		
		-x, y,-z,
		x, y,-z,
		-x,-y,-z,
		
		x,-y,-z,
		-x,-y,-z,
		x, y,-z,
		
		x, y,-z,
		x, y, z,
		x,-y,-z,
		
		x,-y, z,
		x,-y,-z,
		x, y, z,
		
		x, y, z,
		-x, y, z,
		x,-y, z,
		
		-x,-y, z,
		x,-y, z,
		-x, y, z,
		
		-x, y,-z,
		-x, y, z,
		x, y,-z,
		
		x, y, z,
		x, y,-z,
		-x, y, z,
		
		-x,-y,-z,
		x,-y,-z,
		-x,-y, z,
		
		x,-y, z,
		-x,-y, z,
		x,-y,-z
	};
	
	float n[] = {
		-1.000000,0.000000,0.000000,
		-1.000000,0.000000,0.000000,
		-1.000000,0.000000,0.000000,
		-1.000000,0.000000,0.000000,
		-1.000000,0.000000,0.000000,
		-1.000000,0.000000,0.000000,
		
		0.000000,0.000000,-1.000000,
		0.000000,0.000000,-1.000000,
		0.000000,0.000000,-1.000000,
		0.000000,0.000000,-1.000000,
		0.000000,0.000000,-1.000000,
		0.000000,0.000000,-1.000000,
		
		1.000000,0.000000,0.000000,
		1.000000,0.000000,0.000000,
		1.000000,0.000000,0.000000,
		1.000000,0.000000,0.000000,
		1.000000,0.000000,0.000000,
		1.000000,0.000000,0.000000,
		
		0.000000,0.000000,1.000000,
		0.000000,0.000000,1.000000,
		0.000000,0.000000,1.000000,
		0.000000,0.000000,1.000000,
		0.000000,0.000000,1.000000,
		0.000000,0.000000,1.000000,
		
		0.000000,1.000000,0.000000,
		0.000000,1.000000,0.000000,
		0.000000,1.000000,0.000000,
		0.000000,1.000000,0.000000,
		0.000000,1.000000,0.000000,
		0.000000,1.000000,0.000000,
		
		0.000000,-1.000000,0.000000,
		0.000000,-1.000000,0.000000,
		0.000000,-1.000000,0.000000,
		0.000000,-1.000000,0.000000,
		0.000000,-1.000000,0.000000,
		0.000000,-1.000000,0.000000
	};
	
	float t0[] = {
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000,
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000,
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000,
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000,
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		
		0.000000,1.000000,
		0.000000,0.000000,
		1.000000,1.000000,
		
		1.000000,0.000000,
		1.000000,1.000000,
		0.000000,0.000000,
		
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000
	};
	
	float t1[] = {
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000,
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000,
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000,
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000,
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		
		1.000000,0.000000,
		1.000000,1.000000,
		0.000000,0.000000,
		0.000000,1.000000,
		0.000000,0.000000,
		1.000000,1.000000,
		
		0.000000,0.000000,
		1.000000,0.000000,
		0.000000,1.000000,
		1.000000,1.000000,
		0.000000,1.000000,
		1.000000,0.000000
	};
	
	unsigned int i[] = {
		0,1,2, 3,4,5,
		6,7,8, 9,10,11,
		12,13,14, 15,16,17,
		18,19,20, 21,22,23,
		24,25,26, 27,28,29,
		30,31,32, 33,34,35
	};
	
	int faceISize = 3 * 2;	// 2 triangles per face, 3 vertex per triangle
	int iSize = 6 * faceISize;	// 6 faces
	int vSize = iSize * 3;	// 3 elements (x.y.z) for each index
	int tSize = iSize * 2;	// 2 elements (u, v) for each index
	
	vwgl::PolyList * facePlist = new vwgl::PolyList();
	
	unsigned int * faceIndexes = i;
	faceIndexes += faceISize * face;
	
	facePlist->addVertexVector(v, vSize);
	facePlist->addNormalVector(n, vSize);
	facePlist->addTexCoord0Vector(t0, tSize);
	facePlist->addTexCoord1Vector(t1, tSize);
	
	facePlist->addIndexVector(faceIndexes, faceISize);
	
	facePlist->buildPolyList();
	facePlist->setDrawMode(vwgl::PolyList::kTriangles);
	
	return new vwgl::Solid(facePlist,new vwgl::GenericMaterial());
}

}

