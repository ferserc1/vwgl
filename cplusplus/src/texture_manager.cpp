
#include <vwgl/texture_manager.hpp>
#include <iostream>
#include <vector>
#include <vwgl/glerror.hpp>
#include <vwgl/vector.hpp>

namespace vwgl {

TextureManager * TextureManager::s_singleton = NULL;
	
TextureManager::TextureManager()
	:_minFilter(Texture::TextureFilter::kFilterLinearMipmapLinear)
	,_magFilter(Texture::TextureFilter::kFilterLinear)
	,_wrapS(Texture::TextureWrap::kWrapRepeat)
	,_wrapT(Texture::TextureWrap::kWrapRepeat)
	,_colorTextureSize(32)
{
	SingletonController::get()->registerSingleton(this);
}

TextureManager::~TextureManager() {
	SingletonController::get()->unregisterSingleton(this);
}

TextureManager * TextureManager::get() {
	if (s_singleton==NULL) s_singleton = new TextureManager();
	return s_singleton;
}

void TextureManager::clearUnused() {
	TextureMap::iterator it;
	std::vector<std::string> keysToDelete;
	for (it=_textures.begin(); it!=_textures.end(); ++it) {
		if (it->second->getReferences()==1) {
			keysToDelete.push_back(it->first);
		}
	}
	
	std::vector<std::string>::iterator s_it;
	for (s_it=keysToDelete.begin(); s_it!=keysToDelete.end(); ++s_it) {
		std::string key = *s_it;
		_textures.erase(key);
	}
}

void TextureManager::finalize() {
	clearUnused();
	_textures.clear();
	_firstTexture = nullptr;
}

Texture * TextureManager::randomTexture() {
	return _firstTexture.getPtr();
}

Texture * TextureManager::whiteTexture()
{
	if (!_whiteTexture.valid()) {
		int size = colorTextureSize();
		_whiteTexture = Texture::getColorTexture(Color::white(), Size2Di(size));
	}
	return _whiteTexture.getPtr();
}

Texture * TextureManager::blackTexture()
{
	if (!_blackTexture.valid()) {
		int size = colorTextureSize();
		_blackTexture = Texture::getColorTexture(Color::black(), Size2Di(size));
	}
	return _blackTexture.getPtr();
}

Texture * TextureManager::transparentTexture()
{
	if (!_transparentTexture.valid()) {
		int size = colorTextureSize();
		_transparentTexture = Texture::getColorTexture(Color::transparent(), Size2Di(size));
	}
	return _transparentTexture.getPtr();
}

Texture * TextureManager::yNormalTexture()
{
	if (!_yNormalTexture.valid()) {
		int size = colorTextureSize();
		_yNormalTexture = Texture::getColorTexture(Color(0.0f, 1.0f, 0.0f, 1.0f), Size2Di(size));
	}
	return _yNormalTexture.getPtr();
}

Texture * TextureManager::loadTexture(ptr<vwgl::Image> img, bool nocache) {
	if (!img.valid()) return nullptr;
	if (img->getBuffer()==NULL) return NULL;
	TextureMap & textures = _textures;
	ptr<Texture> txt;
	if (textures.find(img->getFileName())==textures.end() || nocache) {
		txt = new Texture();
		
		txt->createTexture();
		txt->bindTexture(Texture::kTargetTexture2D);
		txt->setMinFilter(Texture::kTargetTexture2D, _minFilter);
		txt->setMagFilter(Texture::kTargetTexture2D, _magFilter);
		txt->setTextureWrapS(Texture::kTargetTexture2D, _wrapS);
		txt->setTextureWrapT(Texture::kTargetTexture2D, _wrapT);
		txt->setImage(Texture::kTargetTexture2D, img.getPtr());
		txt->setFileName(img->getFileName());
		if (!nocache) textures[img->getFileName()] = txt.getPtr();
		if (_textures.size()==0) _firstTexture = txt.getPtr();
	}
	else {
		txt = textures[img->getFileName()];
	}
	return txt.release();
}

Texture * TextureManager::loadTexture(Image * img, bool nocache) {
	ptr<Image> image = img;
	return loadTexture(image,nocache);
}

CubeMap * TextureManager::loadCubeMap(ptr<Image> posX,
									  ptr<Image> negX,
									  ptr<Image> posY,
									  ptr<Image> negY,
									  ptr<Image> posZ,
									  ptr<Image> negZ) {
	if (!posX.valid() || !posY.valid() || !posZ.valid() || !negX.valid() || !negY.valid() || !negZ.valid()) return nullptr;
	if (posX->getBuffer()==NULL || negX->getBuffer()==NULL ||
		posY->getBuffer()==NULL || negY->getBuffer()==NULL ||
		posZ->getBuffer()==NULL || negZ->getBuffer()==NULL) return NULL;
	ptr<CubeMap> cubeMap = new CubeMap();
	
	cubeMap->createTexture();
	cubeMap->bindTexture(Texture::kTargetTextureCubeMap);
	cubeMap->setMinFilter(Texture::kTargetTextureCubeMap, Texture::kFilterLinear);
	cubeMap->setMagFilter(Texture::kTargetTextureCubeMap, Texture::kFilterLinear);
	cubeMap->setTextureWrapS(Texture::kTargetTextureCubeMap, Texture::kWrapClampToEdge);
	cubeMap->setTextureWrapT(Texture::kTargetTextureCubeMap, Texture::kWrapClampToEdge);
	cubeMap->setTextureWrapR(Texture::kTargetTextureCubeMap, Texture::kWrapClampToEdge);
	cubeMap->setImage(Texture::kTexturePositiveXFace, posX.getPtr());
	cubeMap->setImage(Texture::kTextureNegativeXFace, negX.getPtr());
	cubeMap->setImage(Texture::kTexturePositiveYFace, posY.getPtr());
	cubeMap->setImage(Texture::kTextureNegativeYFace, negY.getPtr());
	cubeMap->setImage(Texture::kTexturePositiveZFace, posZ.getPtr());
	cubeMap->setImage(Texture::kTextureNegativeZFace, negZ.getPtr());
	
	cubeMap->setPositiveXFile(posX->getFileName());
	cubeMap->setNegativeXFile(negX->getFileName());
	cubeMap->setPositiveYFile(posY->getFileName());
	cubeMap->setNegativeYFile(negY->getFileName());
	cubeMap->setPositiveZFile(posZ->getFileName());
	cubeMap->setNegativeZFile(posZ->getFileName());
	
	return cubeMap.release();
}

CubeMap * TextureManager::loadCubeMap(Image * posX,
									  Image * negX,
									  Image * posY,
									  Image * negY,
									  Image * posZ,
									  Image * negZ) {
	ptr<Image> posXptr = posX;
	ptr<Image> negXptr = negX;
	ptr<Image> posYptr = posY;
	ptr<Image> negYptr = negY;
	ptr<Image> posZptr = posZ;
	ptr<Image> negZptr = negZ;
	return loadCubeMap(posXptr,negXptr,posYptr,negYptr,posZptr,negZptr);
}
	
}