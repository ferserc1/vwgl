
#include <vwgl/shadow_render_pass.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

ShadowRenderPassMaterial::ShadowRenderPassMaterial() :_legacyLight(nullptr), _light(nullptr), _shadowTexture(nullptr), _shadowType(kShadowTypeHard), _blurIterations(1), _shadowCascades(3)  {
	initShader();
}

ShadowRenderPassMaterial::~ShadowRenderPassMaterial() {
	
}

void ShadowRenderPassMaterial::initShader() {
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_shadow.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_shadow.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_shadow.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_shadow.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: deferred shadow shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("def_shadow");
	
	loadVertexAttrib("aVertexPosition");
	loadNormalAttrib("aNormalPosition");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uMMatrix");
	getShader()->initUniformLocation("uNMatrix");
	
	getShader()->initUniformLocation("uDepthMap");
	getShader()->initUniformLocation("uDepthMapSize");
	getShader()->initUniformLocation("uLightProjectionMatrix");
	getShader()->initUniformLocation("uLightViewMatrix");
	getShader()->initUniformLocation("uShadowColor");
	getShader()->initUniformLocation("uShadowStrength");
	getShader()->initUniformLocation("uShadowBias");

	getShader()->initUniformLocation("uLightDirection");
	
	getShader()->initUniformLocation("uReceiveShadows");

	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uTextureScale");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uAlphaCutoff");
	getShader()->initUniformLocation("uShadowType");
}

void ShadowRenderPassMaterial::setupUniforms() {
	getShader()->setUniform("uMVMatrix", modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());
	getShader()->setUniform("uMMatrix", modelMatrix());
	getShader()->setUniform("uNMatrix", normalMatrix());
	
	getShader()->setUniform("uShadowType", static_cast<int>(_shadowType));
	
	if (_light && _shadowTexture) {
		getShader()->setUniform("uDepthMap", _shadowTexture,vwgl::Texture::kTexture0);
		getShader()->setUniform("uLightProjectionMatrix", _light->getProjectionMatrix());
		getShader()->setUniform("uLightViewMatrix", _light->getViewMatrix());
		getShader()->setUniform("uShadowColor",_light->getAmbient());
		getShader()->setUniform("uShadowStrength",_light->getShadowStrength());
		getShader()->setUniform("uLightDirection", _light->getDirection());
		getShader()->setUniform("uShadowBias", _light->getShadowBias());
	}
	else if (_legacyLight && _shadowTexture) {
		getShader()->setUniform("uDepthMap", _shadowTexture,vwgl::Texture::kTexture0);
		getShader()->setUniform("uLightProjectionMatrix", _legacyLight->getProjectionMatrix());
		getShader()->setUniform("uLightViewMatrix", _legacyLight->getViewMatrix());
		getShader()->setUniform("uShadowColor",_legacyLight->getAmbient());
		getShader()->setUniform("uShadowStrength",_legacyLight->getShadowStrength());
		getShader()->setUniform("uLightDirection", _legacyLight->getDirection());
		getShader()->setUniform("uShadowBias", _legacyLight->getShadowBias());
	}

	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uReceiveShadows", mat->getReceiveShadows());
		
		Texture * tex = mat->getTexture() ? mat->getTexture() : TextureManager::get()->whiteTexture();
		getShader()->setUniform("uTexture", tex, Texture::kTexture1);
		getShader()->setUniform("uTextureScale", mat->getTextureScale());
		getShader()->setUniform("uTextureOffset", mat->getTextureOffset());
		getShader()->setUniform("uAlphaCutoff", mat->getAlphaCutoff());
	}
}

}
