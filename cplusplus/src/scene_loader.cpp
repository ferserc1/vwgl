#include <vwgl/scene_loader.hpp>

#include <fstream>
#include <JsonBox.h>

#include <vwgl/loader.hpp>
#include <vwgl/system.hpp>
#include <vwgl/group.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/dynamic_cubemap.hpp>
#include <vwgl/light.hpp>
#include <vwgl/shadow_light.hpp>
#include <vwgl/projector.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/dock_node.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/node_visitor.hpp>

#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/chain.hpp>
#include <vwgl/scene/joint.hpp>
#include <vwgl/scene/bounding_box.hpp>
#include <vwgl/scene/cubemap.hpp>
#include <vwgl/scene/scene.hpp>
#include <vwgl/scene/init_visitor.hpp>


namespace vwgl {

class CompactSceneVisitor : public scene::NodeVisitor {
public:
	virtual void visit(scene::Node * node) {
		if ((node->getComponent<scene::Drawable>() || node->getComponent<scene::Camera>()) &&
			node->parent()) {
			_compactVector.push_back(node);
		}
	}

	void compact() {
		std::vector<ptr<scene::Node> >::iterator it;
		
		for (it = _compactVector.begin(); it != _compactVector.end(); ++it) {
			scene::Node * parent = (*it)->parent();
			if (parent) {
				std::vector<scene::Component*> componentVector;
				std::vector<scene::Component*>::iterator c_it;
				(*it)->eachComponent([&](scene::Component * comp) {
					componentVector.push_back(comp);
				});
				for (c_it=componentVector.begin(); c_it!=componentVector.end(); ++c_it) {
					parent->addComponent(*c_it);
				}
				parent->setName((*it)->getName());
				parent->removeChild((*it).getPtr());
			}
		}
	}

protected:
	std::vector<ptr<scene::Node> > _compactVector;
};

// Scene parser only create V2.0 scenes
class SceneJsonParser {
public:
	scene::Node * parseScene(const std::string & sceneFile, const std::string scenePath) {
		_indexes.resetIndexes();

		_scenePath = scenePath;
		ptr<scene::Node> root = nullptr;
		_sceneRoot = nullptr;
		
		JsonBox::Value js_lib;
		js_lib.loadFromString(sceneFile);
		if (!js_lib.isObject()) {
			throw LoaderException("Unexpected node found: expecting objects");
		}
		
		if (!js_lib["fileType"].isString() || js_lib["fileType"].getString()!="vwgl::scene") {
			throw LoaderException("Unexpected file type: expecting scene file");
		}
		
		if (!js_lib["version"].isObject()) {
			throw LoaderException("File version not found");
		}
		JsonBox::Object version = js_lib["version"].getObject();
		// TODO: version["major"], version["minor"], version["rev"]
		int majorVersion = 0;
		if (version["major"].isInteger()) {
			majorVersion = version["major"].getInt();
		}
		
		JsonBox::Array scene;
		if (!js_lib["scene"].isArray()) {
			throw LoaderException("Scene root node not found");
		}
		scene = js_lib["scene"].getArray();
		if (scene.size()!=1) {
			throw LoaderException("Wrong scene root size. Expecting 1");
		}
		
		if (majorVersion==1) {
			root = parseNodeLegacy(scene[0]);
			if (root.valid()) {
				CompactSceneVisitor compactVisitor;
				root->accept(compactVisitor);
				compactVisitor.compact();
			}
		}
		else if (majorVersion==2) {
			root = parseNode(scene[0]);
			scene::InitVisitor initVisitor;
			if (root.valid()) {
				root->accept(initVisitor);
			}
		}
		
		return root.release();
	}

protected:
	scene::Node * _sceneRoot;
	std::string _scenePath;

	scene::Node * parseNode(JsonBox::Value & js_node) {
		ptr<scene::Node> node;
		if (js_node.isObject()) {
			node = new vwgl::scene::Node();
			std::string type;
			std::string name;
			JsonBox::Array children;
			JsonBox::Array components;
			bool enabled;
			JsonBox::Array::iterator it;
			
			getString(js_node["type"], type, "");
			getString(js_node["name"], name, "");
			enabled = getBool(js_node["enabled"], true);
			getArray(js_node["children"], children);
			getArray(js_node["components"], components);
			if (type=="Node") {
				node->setName(name);
				node->setEnabled(enabled);
				for (it=children.begin(); it!=children.end(); ++it) {
					scene::Node * child = parseNode(*it);
					node->addChild(child);
				}
				for (it=components.begin(); it!=components.end(); ++it) {
					std::stringstream componentStream;
					(*it).writeToStream(componentStream);
					std::string componentString = componentStream.str();
					scene::Component * comp = scene::Component::factory(componentString, _scenePath);
					if (!comp) {
						throw LoaderException("Error loading component");
					}
					node->addComponent(comp);
				}
				
			}
			else {
				throw LoaderException("Unexpected node type found");
			}
		}
		return node.release();
	}

	// Legacy V1 API
	void parseChildren(scene::Node * root, JsonBox::Array & children) {
		JsonBox::Array::iterator it;
		for (it=children.begin(); it!=children.end(); ++it) {
			ptr<scene::Node> node = parseNodeLegacy(*it);
			if (node.valid()) {
				root->addChild(node.getPtr());
			}
		}
	}

	struct UntitledIndexes {
		int camera;
		int light;
		int drawable;
		int cubemap;
		int transform;
		int node;

		UntitledIndexes() :camera(0), light(0), drawable(0), cubemap(0), transform(0), node(0) {}
		void resetIndexes() { camera = 0; light = 0; drawable = 0; cubemap = 0; transform = 0; node = 0; }
	};

	UntitledIndexes _indexes;

	void assignName(scene::Node * node, const std::string & nodeName, const std::string & type) {
		if (nodeName == "") {
			std::stringstream name;
			if (type != "DrawableNode") {
				name << type << "_";
			}
			
			if (type == "Camera") {
				name << _indexes.camera++;
			}
			else if (type == "ShadowLight") {
				name << _indexes.light++;
			}
			else if (type == "Light") {
				name << _indexes.light++;
			}
			else if (type == "DrawableNode") {
				scene::Drawable * drw = node->getComponent<scene::Drawable>();
				if (drw && drw->getName()!="") {
					name.clear();
					name << drw->getName();
				}
				else {
					name << type << _indexes.drawable++;
				}
			}
			else if (type == "DynamicCubemap") {
				name << _indexes.cubemap++;
			}
			else if (type == "TRSTransformNode") {
				name << _indexes.transform++;
			}
			else if (type == "PolarTransformNode") {
				name << _indexes.transform++;
			}
			else if (type == "TransformNode") {
				name << _indexes.transform++;
			}
			else  {
				name << _indexes.node++;
			}

			node->setName(name.str());
		}
		else {
			node->setName(nodeName);
		}
	}

	scene::Node * parseNodeLegacy(const JsonBox::Value & js_node) {
		if (!js_node.isObject()) {
			throw LoaderException("Unexpected type found: expecting object");
		}
		std::string type;
		std::string name;
		JsonBox::Value n = js_node;
		getString(n["type"], type, "node");
		getString(n["name"], name, "");
		
		scene::Node * node = nullptr;
		
		// V1.0 nodes: will return a node containing a component
		if (type=="Camera") {
			node = parseCamera(n);
		}
		else if (type=="ShadowLight") {
			node = parseShadowLight(n);
		}
		else if (type=="Light") {
			node = parseLight(n);
		}
		else if (type=="DrawableNode") {
			node = parseDrawable(n);
		}
		else if (type=="DynamicCubemap") {
			node = parseCubemap(n);
		}
		else if (type=="TRSTransformNode") {
			node = parseTRSTransform(n);
		}
		else if (type=="PolarTransformNode") {
			node = parsePolarTransform(n);
		}
		else if (type=="TransformNode") {
			node = parseTransform(n);
		}
		else if (type=="Group") {
			node = parseGroup(n);
		}
		else if (type=="DockNode") {
			node = new vwgl::scene::Node();
			node->addComponent(new vwgl::scene::Chain());
		}
		else {
			node = new vwgl::scene::Node();
		}
		
		initNode(n,node);
		
		assignName(node, name, type);
		
		node->setEnabled(getBool(n["enabled"], true));

		return node;
	}
	
	void initNode(JsonBox::Value & js_node, scene::Node * node) {
		JsonBox::Array components;
		bool haveComponents = getArray(js_node["components"], components);
		if (_sceneRoot==nullptr) _sceneRoot = node;
		if (js_node["children"].isArray()) {
			JsonBox::Array arr = js_node["children"].getArray();
			parseChildren(node, arr);
		}
		
		if (haveComponents) {
			// TODO: parse components
		}
	}
	
	scene::Node * parseCamera(JsonBox::Value node) {
		scene::Camera * cam = new scene::Camera();
		if (!node["fov"].isNull()) {
			float fov = getNumber(node["fov"], 35.0f);
			float near = getNumber(node["near"], 0.1f);
			float far = getNumber(node["far"],100.0f);
			cam->setProjectionMethod(new PerspectiveProjectionMethod(fov));
			cam->getProjectionMethod<PerspectiveProjectionMethod>()->setNear(near);
			cam->getProjectionMethod<PerspectiveProjectionMethod>()->setFar(far);
		}
		else if (!node["focalLength"].isNull() || !node["frameSize"].isNull()) {
			float focalLength = getNumber("focalLength", 50.0f);
			float frameSize = getNumber("frameSize", 35.0f);
			float near = getNumber(node["near"], 0.1f);
			float far = getNumber(node["far"],100.0f);
			cam->setProjectionMethod(new OpticalProjectionMethod(focalLength,frameSize));
			cam->getProjectionMethod<OpticalProjectionMethod>()->setNear(near);
			cam->getProjectionMethod<OpticalProjectionMethod>()->setFar(far);
		}
		else if (!node["projection"].isNull()) {
			cam->getProjectionMatrix() = getMatrix(node["projection"], Matrix4::makePerspective(45.0f, 1.0f, 0.1f, 100.0f));
		}

		scene::Node * camNode = new scene::Node();
		camNode->addComponent(cam);
		return camNode;
	}
	
	scene::Node * parseShadowLight(JsonBox::Value node) {
		scene::Node * lightNode = new scene::Node();
		scene::Light * light = new scene::Light();
		lightNode->addComponent(light);
		bool cs = getBool(node["castShadows"], true);
		light->setShadowStrength(getNumber(node["shadowStrength"], 1.0f));
		light->setShadowBias(getNumber(node["shadowBias"], 0.0008f));
		light->setCastShadows(cs);
		light->getProjectionMatrix() = getMatrix(node["projection"], vwgl::Matrix4::makeOrtho(-4.0f, 4.0f, -4.0f, 4.0f, 0.1f, 70.0f));
		parseCommonLightParams(node, light, lightNode);
		return lightNode;
	}
	
	scene::Node * parseLight(JsonBox::Value node) {
		scene::Node * lightNode = new scene::Node();
		scene::Light * light = new scene::Light();
		lightNode->addComponent(light);
		light->setCastShadows(false);
		parseCommonLightParams(node, light, lightNode);
		return lightNode;
	}
	
	void parseCommonLightParams(JsonBox::Value node, scene::Light * light, scene::Node * lightNode) {
		light->setAmbient(getVector(node["ambient"], Vector4(0.2f,0.2f,0.2f,1.0f)));
		light->setDiffuse(getVector(node["diffuse"], Vector4(0.9f,0.9f,0.9f,1.0f)));
		light->setSpecular(getVector(node["specular"], Vector4(1.0f,1.0f,1.0f,1.0f)));
		light->setConstantAttenuation(getNumber(node["constantAttenuation"], 1.0f));
		light->setLinearAttenuation(getNumber(node["linearAttenuation"], 0.5f));
		light->setExpAttenuation(getNumber(node["expAttenuation"], 0.1f));
		light->setSpotCutoff(getNumber(node["spotCutoff"], 45.0f));
		light->setSpotExponent(getNumber(node["spotExponent"], 30.0f));
		light->setCutoffDistance(getNumber(node["cutoffDistance"], -1.0f));
		lightNode->setEnabled(getBool(node["enabled"], true));
		std::string type;
		getString(node["lightType"], type, "directional");
		if (type=="directional") {
			light->setType(Light::kTypeDirectional);
		}
		else if (type=="spot") {
			light->setType(Light::kTypeSpot);
		}
		else if (type=="point") {
			light->setType(Light::kTypePoint);
		}
		else if (type=="disabled") {
			light->setType(Light::kTypeDisabled);
		}
	}
	
	scene::Node * parseDrawable(JsonBox::Value node) {
		std::string filePath;
		int identifier = -1;
		int reference = -1;
		getString(node["file"], filePath, "");
		identifier = getInt(node["identifier"], -1);
		reference = getInt(node["reference"], -1);
		if (filePath!="") {
			filePath = System::get()->addPathComponent(_scenePath, filePath);
			ptr<scene::Node> drw = vwgl::Loader::get()->loadPrefab(filePath);
			if (!drw.valid()) {
				throw LoaderException("Could not load scene: could not load some scene resources: " + filePath);
			}
			ptr<scene::Drawable> drwComp = drw->getComponent<scene::Drawable>();
			if (drwComp.valid() && drwComp->getName() == "") {
				drwComp->setName(System::get()->getFileName(filePath));
			}
			return drw.release();
		}
		else {
			throw LoaderException("Could not load scene: no such drawable path");
		}
		return nullptr;
	}
	
	scene::Node * parseCubemap(JsonBox::Value node) {
		scene::Node * cmNode = new vwgl::scene::Node();
		scene::Cubemap * cm = new scene::Cubemap();
		cmNode->addComponent(cm);
		Vector4 color = getVector(node["color"], Color::black());
		cm->setClearColor(color);
		return cmNode;
	}
	
	scene::Node * parseTRSTransform(JsonBox::Value node) {
		TRSTransformStrategy * trans = new TRSTransformStrategy();
		scene::Node * transNode = new scene::Node();
		scene::Transform * transform = new scene::Transform();
		trans->translate(getVector(node["translate"], Vector3(0.0f)));
		trans->rotateX(getNumber(node["rotateX"], 0.0f));
		trans->rotateY(getNumber(node["rotateY"], 0.0f));
		trans->rotateZ(getNumber(node["rotateZ"], 0.0f));
		trans->scale(getVector(node["scale"], Vector3(1.0f)));
		std::string rotOrder;
		getString(node["rotationOrder"], rotOrder, "kOrderXYZ");
		if (rotOrder=="kOrderXYZ") {
			trans->setRotationOrder(vwgl::TRSTransformStrategy::kOrderXYZ);
		}
		else if (rotOrder=="kOrderXZY") {
			trans->setRotationOrder(vwgl::TRSTransformStrategy::kOrderXZY);
		}
		else if (rotOrder=="kOrderYXZ") {
			trans->setRotationOrder(vwgl::TRSTransformStrategy::kOrderYXZ);
		}
		else if (rotOrder=="kOrderYZX") {
			trans->setRotationOrder(vwgl::TRSTransformStrategy::kOrderYZX);
		}
		else if (rotOrder=="kOrderZYX") {
			trans->setRotationOrder(vwgl::TRSTransformStrategy::kOrderZYX);
		}
		else if (rotOrder=="kOrderZXY") {
			trans->setRotationOrder(vwgl::TRSTransformStrategy::kOrderZXY);
		}
		std::string rotationReference;
		getString(node["rotationReference"], rotationReference, "kRelative");
		transNode->addComponent(transform);
		transform->getTransform().setTransformStrategy(trans);
		// TODO: rotation reference
//		if (rotationReference=="kAbsolute") {
//			transNode->setRotationReference(TransformNode::kAbsolute);
//		}
//		else {
//			transNode->setRotationReference(TransformNode::kRelative);
//		}
		return transNode;
	}
	
	scene::Node * parsePolarTransform(JsonBox::Value node) {
		scene::Node * transNode = new scene::Node();
		scene::Transform * transform = new scene::Transform();
		transNode->addComponent(transform);
		PolarTransformStrategy * trans = new PolarTransformStrategy();
		trans->setOrientation(getNumber(node["orientation"], 0.0f));
		trans->setElevation(getNumber(node["elevation"], 0.0f));
		trans->setDistance(getNumber(node["distance"], 5.0f));
		trans->setOrigin(getVector(node["origin"], Vector3(0.0f)));
		transform->getTransform().setTransformStrategy(trans);
		std::string rotationReference;
		getString(node["rotationReference"], rotationReference, "kRelative");
//		if (rotationReference=="kAbsolute") {
//			transNode->setRotationReference(TransformNode::kAbsolute);
//		}
//		else {
//			transNode->setRotationReference(TransformNode::kRelative);
//		}
		return transNode;
	}
	
	scene::Node * parseTransform(JsonBox::Value node) {
		scene::Node * transNode = new scene::Node();
		scene::Transform * trx = new scene::Transform();
		trx->getTransform().setMatrix(getMatrix(node["transform"], vwgl::Matrix4::makeIdentity()));
		transNode->addComponent(trx);
//		std::string rotationReference;
//		getString(node["rotationReference"], rotationReference, "kRelative");
//		if (rotationReference=="kAbsolute") {
//			trx->setRotationReference(TransformNode::kAbsolute);
//		}
//		else {
//			trx->setRotationReference(TransformNode::kRelative);
//		}
//		return trx;
		return transNode;
	}
	
	scene::Node * parseGroup(JsonBox::Value group) {
		return new scene::Node();
	}
	
	bool getArray(const JsonBox::Value & val, JsonBox::Array & array) {
		if (val.isArray()) {
			array = val.getArray();
			return true;
		}
		else {
			return false;
		}
	}
	
	int getInt(const JsonBox::Value & val, int defaultVal) {
		if (val.isInteger()) return val.getInt();
		else return defaultVal;
	}
	
	float getFloat(const JsonBox::Value & val, float defaultVal) {
		if (val.isDouble()) return static_cast<float>(val.getDouble());
		else return defaultVal;
	}
	
	bool getBool(const JsonBox::Value & val, bool defaultVal) {
		if (val.isBoolean()) return val.getBoolean();
		else if (val.isInteger()) return val.getInt()!=0;
		else return defaultVal;
	}
	
	float getNumber(const JsonBox::Value & val, float defaultVal = 0.0f) {
		if (val.isInteger()) return static_cast<float>(val.getInt());
		else if (val.isDouble()) return static_cast<float>(val.getDouble());
		else if (val.isBoolean()) return val.getBoolean() ? 1.0f:0.0f;
		else return defaultVal;
	}
	
	void getString(const JsonBox::Value & val, std::string & string, const std::string & defaultVal) {
		std::stringstream conv;
		if (val.isString()) {
			conv << val.getString();
		}
		else if (val.isInteger()) {
			conv << val.getInt();
		}
		else if (val.isDouble()) {
			conv << val.getDouble();
		}
		else if (val.isBoolean()) {
			conv << val.getBoolean();
		}
		else {
			conv << defaultVal;
		}
		string = conv.str();
	}
	
	Vector2 getVector(const JsonBox::Value & val, const Vector2 & defaultVal) {
		if (!val.isArray()) {
			return defaultVal;
		}
		JsonBox::Array arr = val.getArray();
		if (arr.size()!=2) {
			return defaultVal;
		}
		return Vector2(getNumber(arr[0]),getNumber(arr[1]));
	}
	
	Vector3 getVector(const JsonBox::Value & val, const Vector3 & defaultVal) {
		if (!val.isArray()) {
			return defaultVal;
		}
		JsonBox::Array arr = val.getArray();
		if (arr.size()!=3) {
			return defaultVal;
		}
		return Vector3(getNumber(arr[0]),getNumber(arr[1]),getNumber(arr[2]));
	}
	
	Vector4 getVector(const JsonBox::Value & val, const Vector4 & defaultVal) {
		if (!val.isArray()) {
			return defaultVal;
		}
		JsonBox::Array arr = val.getArray();
		if (arr.size()!=4) {
			return defaultVal;
		}
		return Vector4(getNumber(arr[0]),getNumber(arr[1]),getNumber(arr[2]),getNumber(arr[3]));
	}
	
	Matrix4 getMatrix(const JsonBox::Value & val, const Matrix4 & defaultVal) {
		if (!val.isArray()) {
			return defaultVal;
		}
		JsonBox::Array arr = val.getArray();
		if (arr.size()!=16) {
			return defaultVal;
		}
		return Matrix4(getNumber(arr[ 0]),getNumber(arr[ 1]),getNumber(arr[ 2]),getNumber(arr[ 3]),
					   getNumber(arr[ 4]),getNumber(arr[ 5]),getNumber(arr[ 6]),getNumber(arr[ 7]),
					   getNumber(arr[ 8]),getNumber(arr[ 9]),getNumber(arr[10]),getNumber(arr[11]),
					   getNumber(arr[12]),getNumber(arr[13]),getNumber(arr[14]),getNumber(arr[15]));
	}
};

SceneLoader::SceneLoader() {
	
}

SceneLoader::~SceneLoader() {
	
}

bool SceneLoader::acceptFileType(const std::string & path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
//	#ifdef VWGL_IOS
//	return ext=="vitbundle" || ext=="vitscnj";
//	#else
	return ext=="vitscn" || ext=="vitbundle" || ext=="vitscnj";
//	#endif
}

Node * SceneLoader::loadNode(const std::string & path) {
	// API 1.0 scenes are deprecated. Use loadScene instead, to load a V2.0 scene
	return nullptr;
}
	
// API 2.0
bool SceneReader::acceptFileType(const std::string & path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	//	#ifdef VWGL_IOS
	//	return ext=="vitbundle" || ext=="vitscnj";
	//	#else
	return ext=="vitscn" || ext=="vitbundle" || ext=="vitscnj";
	//	#endif
}

scene::Node * SceneReader::loadScene(const std::string & path) {
	if (!acceptFileType(path)) return nullptr;
	std::string fullPath;
	std::string ext = System::get()->getExtension(path);
	bool deleteFullpath = false;
	if (System::get()->getExtension(path)=="vitbundle") {
		fullPath = System::get()->addPathComponent(path, "scene.vitscnj");
		std::cout << "Opening scene file: " << fullPath << std::endl;
		if (!System::get()->fileExists(fullPath)) {
			std::cout << "file " << fullPath << " not found." << std::endl;
			std::cout << "Trying scene file: " << fullPath << std::endl;
			fullPath = System::get()->addPathComponent(path, "scene.json");
		}
	}
	else if (System::get()->getExtension(path)=="vitscnj") {
		fullPath = path;
	}
	else if (System::get()->getExtension(path)=="vitscn") {
		std::cout << "Scene pack. Extracting package" << std::endl;
		std::string fileName = System::get()->getFileName(path);
		std::string tmpFile = System::get()->getTempPath();
		//std::string tmpFile = "/put here a path to debug";
		tmpFile = System::get()->addPathComponent(tmpFile, fileName);
		tmpFile = System::get()->addExtension(tmpFile, "vitbundle");
		System::get()->unpackFiles(path, tmpFile);
		fullPath = System::get()->addPathComponent(tmpFile, "scene.vitscnj");
		deleteFullpath = true;
	}
	std::ifstream infile;
	
	infile.open(fullPath);
	if (infile.is_open()) {
		std::stringstream strStream;
		strStream << infile.rdbuf();
		std::string fileContent = strStream.str();
		SceneJsonParser parser;
		std::string bundlePath = System::get()->getPath(fullPath);
		std::cout << "Parsing scene file" << std::endl;
		ptr<scene::Node> node = parser.parseScene(fileContent,bundlePath);
		std::cout << "Done" << std::endl;
		if (deleteFullpath) {
			//System::get()->removeDirectory(System::get()->getPath(fullPath));
		}
		return node.release();
	}
	return nullptr;
}

}
