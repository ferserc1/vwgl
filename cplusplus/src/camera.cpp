
#include <vwgl/camera.hpp>

#include <vwgl/state.hpp>

#include <vwgl/transform_node.hpp>

#include <algorithm>

#pragma warning ( disable : 4835 )

namespace vwgl {

Camera::Camera() {
	_projection.identity();
	CameraManager::get()->addCamera(this);
	_transformVisitor = new TransformVisitor();
}

Camera::~Camera() {
	CameraManager::get()->removeCamera(this);
}

Matrix4 & Camera::getViewMatrix() {
	_transformVisitor->visit(this);
	_transformVisitor->getTransform().invert();
	return _transformVisitor->getTransform();
}

vwgl::Matrix4 & Camera::getTransform() {
	vwgl::TransformNode * trNode = dynamic_cast<vwgl::TransformNode*>(getParent());
	if (trNode) {
		return trNode->getTransform();
	}
	return _fooTransform;
}
	
const vwgl::Vector3 & Camera::getDirectionVector() {
	_direction.set(0.0f, 0.0f, -1.0f);
	vwgl::TransformNode * trx = dynamic_cast<vwgl::TransformNode*>(getParent());
	if (trx) {
		vwgl::Matrix4 transform = trx->getTransform();
		transform.setRow(3, vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f));
		_direction = transform.multVector(_direction).xyz();
		_direction.normalize();
	}
	return _direction;
}

CameraManager * CameraManager::s_singleton = nullptr;

CameraManager * CameraManager::get() {
	if (s_singleton==nullptr) {
		s_singleton = new CameraManager();
	}
	return s_singleton;
}

void CameraManager::finalize() {
	_cameraList.clear();
	_mainCamera = nullptr;
}

CameraManager::CameraManager() :_mainCamera(nullptr) {
	SingletonController::get()->registerSingleton(this);
}

CameraManager::~CameraManager() {
	SingletonController::get()->unregisterSingleton(this);
}

void CameraManager::applyTransform(Camera * cam) {
	if (cam==NULL) cam = _mainCamera;
	if (cam) {
		vwgl::State::get()->projectionMatrix() = cam->getProjectionMatrix();
		vwgl::State::get()->viewMatrix() = cam->getViewMatrix();
	}
}

void CameraManager::addCamera(Camera * cam) {
	CameraList::iterator it = std::find(_cameraList.begin(), _cameraList.end(), cam);
	if (it==_cameraList.end()) {
		_cameraList.push_back(cam);
	}
	if (_mainCamera==nullptr) {
		_mainCamera = cam;
 	}
}

void CameraManager::removeCamera(Camera * cam) {
	CameraList::iterator it = std::find(_cameraList.begin(), _cameraList.end(), cam);
	if (it!=_cameraList.end()) {
		_cameraList.erase(it);
	}
	if (_mainCamera==cam && _cameraList.size()>0) {
		_mainCamera = _cameraList.front();
	}
	else if (_mainCamera==cam) {
		_mainCamera = nullptr;
	}
}

}