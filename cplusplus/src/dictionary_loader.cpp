
#include <vwgl/dictionary_loader.hpp>
#include <vwgl/system.hpp>

#include <JsonBox.h>

namespace vwgl {
	
class JsonToDictionary {
public:
	JsonToDictionary(const JsonBox::Value & root) :_root(root) {}

	Dictionary * getDictionary() {
		if (_root.isNull()) {
			return nullptr;
		}

		ptr<Dictionary> dict = new Dictionary();
		parseNode(_root,dict.getPtr());
	
		return dict.release();
	}

protected:
	JsonBox::Value _root;
	
	void parseNode(const JsonBox::Value & node, Dictionary * dict) {
		if (node.isArray()) {
			dict->set(Dictionary::DictionaryArray());
			JsonBox::Array::const_iterator it;
			for (it=node.getArray().begin(); it!=node.getArray().end(); ++it) {
				JsonBox::Value val = (*it);
				ptr<Dictionary> childDict = new Dictionary();
				parseNode(val, childDict.getPtr());
				dict->getArray().push_back(childDict.getPtr());
			}
		}
		else if (node.isObject()) {
			dict->set(Dictionary::DictionaryMap());
			JsonBox::Object::const_iterator it;
			for (it=node.getObject().begin(); it!=node.getObject().end(); ++it) {
				std::string key = it->first;
				JsonBox::Value val = it->second;
				ptr<Dictionary> childDict = new Dictionary();
				parseNode(val, childDict.getPtr());
				dict->getObject()[key] = childDict.getPtr();
			}
		}
		else if (node.isBoolean()) {
			dict->set(node.getBoolean());
		}
		else if (node.isDouble()) {
			dict->set(node.getDouble());
		}
		else if (node.isInteger()) {
			dict->set(node.getInt());
		}
		else if (node.isString()) {
			dict->set(node.getString());
		}
		else if (node.isNull()) {
			dict->setNull();
		}
	}
};

bool DictionaryLoader::acceptFileType(const std::string & path) {
	return System::get()->getExtension(path)=="json";
}

Dictionary * DictionaryLoader::loadDictionary(const std::string & path) {
	JsonBox::Value file;
	file.loadFromFile(path);
	
	return JsonToDictionary(file).getDictionary();
}

}