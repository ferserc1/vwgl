#include <vwgl/drawable.hpp>
#include <vwgl/sceneset.hpp>

namespace vwgl {

static std::string s_mainCameraName = "mainCamera";
static std::string s_mainLightName = "mainLight";

SceneSet::SceneSet() {
	_sceneRoot = new vwgl::Group();
}

SceneSet::~SceneSet() {
	_cameras.clear();
	_lights.clear();
}

void SceneSet::buildDefault(Size2Di shadowMapSize) {
	Camera * cam = new Camera();
	Matrix4 trx = Matrix4::makeRotation(Math::degreesToRadians(20.0f), 0.0f, 1.0f, 0.0f);
	trx.rotate(Math::degreesToRadians(20.0f), 1.0f, 0.0f, 0.0f);
	trx.translate(0.0f, 0.0f, 5.0f);
	addCamera(s_mainCameraName, cam, trx);
	
	ShadowLight * light = new ShadowLight();
	light->getProjectionMatrix().ortho(-5.0f, 5.0f, -5.0, 5.0f, 0.1f, 100.0f);
	Matrix4 ltrx = Matrix4::makeRotation(Math::degreesToRadians(-20.0f), 0.0f, 1.0f, 0.0f);
	ltrx.rotate(Math::degreesToRadians(-20.0f), 1.0f, 0.0f, 0.0f);
	ltrx.translate(0.0f, 0.0f, 5.0f);
	addLight(s_mainLightName, light, ltrx);
}
	
void SceneSet::setScene(Node * root) {
	Group * sceneRoot = dynamic_cast<Group*>(root);
	
	if (sceneRoot) {
		_sceneRoot = sceneRoot;
		_cameras.clear();
		_lights.clear();
		
		ptr<FindNodeVisitor<Camera> > findCameraVisitor = new FindNodeVisitor<Camera>();
		ptr<FindNodeVisitor<Light> > findLightVisitor = new FindNodeVisitor<Light>();
		
		findCameraVisitor->visit(sceneRoot);
		findLightVisitor->visit(sceneRoot);
		
		std::vector<Camera*>::iterator c_it;
		std::vector<Light*>::iterator l_it;
		int nameIndex = 0;
		for (c_it=findCameraVisitor->getResult().begin(); c_it!=findCameraVisitor->getResult().end(); ++c_it) {
			Camera * cam = *c_it;
			if (nameIndex==0) {
				_cameras[s_mainCameraName] = cam;
			}
			else {
				std::stringstream camName;
				camName << "camera_" << nameIndex;
				_cameras[camName.str()] = cam;
			}
			++nameIndex;
		}
		nameIndex = 0;
		for (l_it=findLightVisitor->getResult().begin(); l_it!=findLightVisitor->getResult().end(); ++l_it) {
			Light * light = *l_it;
			if (nameIndex==0) {
				_lights[s_mainLightName] = light;
			}
			else {
				std::stringstream lightName;
				lightName << "light_" << nameIndex;
				_lights[lightName.str()] = light;
			}
			++nameIndex;
		}
	}
}


bool SceneSet::addLight(const std::string & name, Light * light, Matrix4 transform) {
	if (_lights.find(name)==_lights.end()) {
		TransformNode * trx = new TransformNode(transform);
		_sceneRoot->addChild(trx);
		trx->addChild(light);
		_lights[name] = light;
		return true;
	}
	return false;
}

bool SceneSet::addCamera(const std::string & name, Camera * cam, Matrix4 transform) {
	if (_cameras.find(name)==_cameras.end()) {
		TransformNode * trx = new TransformNode(transform);
		_sceneRoot->addChild(trx);
		trx->addChild(cam);
		_cameras[name] = cam;
		return true;
	}
	return false;
}

Light * SceneSet::getLight(const std::string & name) {
	return _lights[name].getPtr();
}

Light * SceneSet::getMainLight() {
	return  getLight(s_mainLightName);
}

Camera * SceneSet::getCamera(const std::string & name) {
	return _cameras[name].getPtr();
}
	
TransformNode * SceneSet::getLightTransform(const std::string & name) {
	if (_lights.find(name)!=_lights.end()) {
		return dynamic_cast<TransformNode*>(_lights[name]->getParent());
	}
	return NULL;
}

TransformNode * SceneSet::getMainLightTransform() {
	return getLightTransform(s_mainLightName);
}

TransformNode * SceneSet::getCameraTransform(const std::string & name) {
	if (_cameras.find(name)!=_cameras.end()) {
		return dynamic_cast<TransformNode*>(_cameras[name]->getParent());
	}
	return NULL;
}

TransformNode * SceneSet::getMainCameraTransform() {
	return getCameraTransform(s_mainCameraName);
}

bool SceneSet::removeLight(const std::string & name) {
	if (_lights.find(name)!=_lights.end()) {
		ptr<Light> light = _lights[name];
		ptr<TransformNode> trx = dynamic_cast<TransformNode*>(light->getParent());
		if (trx.valid()) {
			_sceneRoot->removeChild(trx.getPtr());
		}
		_lights.erase(name);
		return true;
	}
	return false;
}

bool SceneSet::removeCamera(const std::string &name) {
	if (_cameras.find(name)!=_cameras.end()) {
		ptr<Camera> cam = _cameras[name];
		ptr<TransformNode> trx = dynamic_cast<TransformNode*>(cam->getParent());
		if (trx.valid()) {
			_sceneRoot->removeChild(trx.getPtr());
		}
		_cameras.erase(name);
		return true;
	}
	return false;
}

}
