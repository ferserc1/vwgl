
#include <vwgl/freetype_reader.hpp>

#include <vwgl/font_manager.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H


namespace vwgl {

bool FreetypeReader::acceptFileType(const std::string &path) {
	std::string extension = System::get()->getExtension(path);
	toLower(extension);
	return extension=="ttf" || extension=="cff" || extension=="woff" || extension=="otf" || extension=="otc"
			|| extension=="pfa" || extension=="pfb" || extension=="pfc" || extension=="fnt" || extension=="bdf" || extension=="pfr";
}
	
vwgl::TextFont * FreetypeReader::loadFont(const std::string & path, int size, int res) {
	std::string name;
	getFontNameWithPath(path, name);
	vwgl::ptr<vwgl::TextFont> font = FontManager::get()->getFont(name, size, res);
	
	if (!font.valid()) {
		font = doLoadFont(path, size, res);
	}
	
	return font.release();
}
	
vwgl::TextFont * FreetypeReader::doLoadFont(const std::string & path, int size, int res) {
	ptr<vwgl::TextFont> font = nullptr;
	try {
		font = new vwgl::TextFont(System::get()->getFileName(path),size,res,path);
		
		FT_Library	library;
		FT_Face     face;
		FT_BBox		bbox;
		FT_Glyph ftGlyph;
		
		font->setLineHeight(size * 1.5f);
		font->setSpaceSize(size * 0.5f);
		
		vwgl::assert<std::runtime_error>(FT_Init_FreeType(&library)==0,"Error initializing font library");
		vwgl::assert<std::runtime_error>(FT_New_Face(library, path.c_str(), 0, &face)==0,"Error loading font");
		
		FT_Select_Charmap(face , FT_ENCODING_UNICODE);
		
		std::string characters = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ	";
		std::string::iterator it;
		for (it=characters.begin(); it!=characters.end(); ++it) {
			vwgl::assert<std::runtime_error>(FT_Set_Char_Size(face, size * 64, 0, res, 0)==0,"Could not set the character size");
			vwgl::assert<std::runtime_error>(FT_Load_Char(face,*it,FT_LOAD_RENDER)==0,"Error loading character");
			
			FT_Get_Glyph(face->glyph, &ftGlyph);
			FT_Glyph_Get_CBox(ftGlyph, FT_GLYPH_BBOX_GRIDFIT, &bbox);
			
			FT_Bitmap & bitmap = face->glyph->bitmap;
			
			unsigned char * expanded_data = new unsigned char[ 4 * bitmap.width * bitmap.rows];
			for(int j=0; j < bitmap.rows;j++) {
				for(int i=0; i < bitmap.width; i++){
					expanded_data[4*(i+j*bitmap.width)] =
					expanded_data[4*(i+j*bitmap.width)+1] =
					expanded_data[4*(i+j*bitmap.width)+2] =
					expanded_data[4*(i+j*bitmap.width)+3] = bitmap.buffer[i + bitmap.width*j];
				}
			}
			
			vwgl::Glyph glyph;
			glyph.setCharacter(*it);
			glyph.setBitmap(new vwgl::Image(expanded_data,vwgl::Size2Di(bitmap.width,bitmap.rows),4,vwgl::Image::kFormatRGBA));
			glyph.setAdvance(static_cast<float>(face->glyph->metrics.horiAdvance) / 64.0);
			glyph.setBearing(vwgl::Vector2(static_cast<float>(face->glyph->metrics.horiBearingX) / 64.0f, static_cast<float>(face->glyph->metrics.horiBearingY)  / 64.0f));
			glyph.getBBox().set(vwgl::Vector2(static_cast<float>(bbox.xMin) / 64.0f, static_cast<float>(bbox.yMin) / 64.0f),
								vwgl::Vector2(static_cast<float>(bbox.xMax) / 64.0f, static_cast<float>(bbox.yMax) / 64.0f));
			
			font->add(*it, new vwgl::GlyphSurface(glyph));
			
		}
		
		FT_Done_Face(face);
		FT_Done_FreeType(library);
	}
	catch (std::runtime_error & e) {
		std::cerr << e.what() << std::endl;
	}
	
	return font.release();
}

void FreetypeReader::getFontNameWithPath(const std::string & path, std::string & name) {
	name = System::get()->getFileName(path);
}
	
}