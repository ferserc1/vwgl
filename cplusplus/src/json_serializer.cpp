
#include <vwgl/json_serializer.hpp>
#include <JsonBox.h>

namespace vwgl {

JsonBox::Value & value(JsonLibPtr * libPtr) {
	return *static_cast<JsonBox::Value*>(libPtr);
}

namespace deserializerUtils {

bool getArray(const JsonBox::Value & val, JsonBox::Array & array) {
	if (val.isArray()) {
		array = val.getArray();
		return true;
	}
	else {
		return false;
	}
}

int getInt(const JsonBox::Value & val, int defaultVal) {
	if (val.isInteger()) return val.getInt();
	else if (val.isDouble()) return static_cast<int>(val.getDouble());
	else if (val.isBoolean()) return val.getBoolean() ? 1:0;
	else return defaultVal;
}

float getFloat(const JsonBox::Value & val, float defaultVal) {
	if (val.isDouble()) return static_cast<float>(val.getDouble());
	else if (val.isInteger()) return static_cast<float>(val.getInt());
	else if (val.isBoolean()) return val.getBoolean() ? 1.0f:0.0f;
	else return defaultVal;
}

bool getBool(const JsonBox::Value & val, bool defaultVal) {
	if (val.isBoolean()) return val.getBoolean();
	else if (val.isInteger()) return val.getInt()!=0;
	else return defaultVal;
}

float getNumber(const JsonBox::Value & val, float defaultVal = 0.0f) {
	if (val.isInteger()) return static_cast<float>(val.getInt());
	else if (val.isDouble()) return static_cast<float>(val.getDouble());
	else if (val.isBoolean()) return val.getBoolean() ? 1.0f:0.0f;
	else return defaultVal;
}

void getString(const JsonBox::Value & val, std::string & string, const std::string & defaultVal) {
	std::stringstream conv;
	if (val.isString()) {
		conv << val.getString();
	}
	else if (val.isInteger()) {
		conv << val.getInt();
	}
	else if (val.isDouble()) {
		conv << val.getDouble();
	}
	else if (val.isBoolean()) {
		conv << val.getBoolean();
	}
	else {
		conv << defaultVal;
	}
	string = conv.str();
}

Vector2 getVector(const JsonBox::Value & val, const Vector2 & defaultVal) {
	if (!val.isArray()) {
		return defaultVal;
	}
	JsonBox::Array arr = val.getArray();
	if (arr.size()!=2) {
		return defaultVal;
	}
	return Vector2(getNumber(arr[0]),getNumber(arr[1]));
}

Vector3 getVector(const JsonBox::Value & val, const Vector3 & defaultVal) {
	if (!val.isArray()) {
		return defaultVal;
	}
	JsonBox::Array arr = val.getArray();
	if (arr.size()!=3) {
		return defaultVal;
	}
	return Vector3(getNumber(arr[0]),getNumber(arr[1]),getNumber(arr[2]));
}

Vector4 getVector(const JsonBox::Value & val, const Vector4 & defaultVal) {
	if (!val.isArray()) {
		return defaultVal;
	}
	JsonBox::Array arr = val.getArray();
	if (arr.size()!=4) {
		return defaultVal;
	}
	return Vector4(getNumber(arr[0]),getNumber(arr[1]),getNumber(arr[2]),getNumber(arr[3]));
}
	
Viewport getViewport(const JsonBox::Value & val, const Viewport & defaultVal) {
	if (!val.isArray()) {
		return defaultVal;
	}
	JsonBox::Array arr = val.getArray();
	if (arr.size()!=4) {
		return defaultVal;
	}
	return Viewport(getNumber(arr[0]),getNumber(arr[1]),getNumber(arr[2]),getNumber(arr[3]));
}

Matrix3 getMatrix(const JsonBox::Value & val, const Matrix3 & defaultVal) {
	if (!val.isArray()) {
		return defaultVal;
	}
	JsonBox::Array arr = val.getArray();
	if (arr.size()!=9) {
		return defaultVal;
	}
	return Matrix3(getNumber(arr[ 0]),getNumber(arr[ 1]),getNumber(arr[ 2]),
				   getNumber(arr[ 3]),getNumber(arr[ 4]),getNumber(arr[ 5]),
				   getNumber(arr[ 6]),getNumber(arr[ 7]),getNumber(arr[ 8]));
}

Matrix4 getMatrix(const JsonBox::Value & val, const Matrix4 & defaultVal) {
	if (!val.isArray()) {
		return defaultVal;
	}
	JsonBox::Array arr = val.getArray();
	if (arr.size()!=16) {
		return defaultVal;
	}
	return Matrix4(getNumber(arr[ 0]),getNumber(arr[ 1]),getNumber(arr[ 2]),getNumber(arr[ 3]),
				   getNumber(arr[ 4]),getNumber(arr[ 5]),getNumber(arr[ 6]),getNumber(arr[ 7]),
				   getNumber(arr[ 8]),getNumber(arr[ 9]),getNumber(arr[10]),getNumber(arr[11]),
				   getNumber(arr[12]),getNumber(arr[13]),getNumber(arr[14]),getNumber(arr[15]));
}

}
	
float JsonDeserializer::getFloat(const std::string & key, float defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getFloat(val[key], defaultValue);
}
	
int JsonDeserializer::getInt(const std::string & key, int defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getInt(val[key], defaultValue);
}

bool JsonDeserializer::getBool(const std::string & key, bool defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getBool(val[key], defaultValue);
}

std::string JsonDeserializer::getString(const std::string & key, const std::string & defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	std::string result;
	deserializerUtils::getString(val[key], result, defaultValue);
	return result;
}

Vector2 JsonDeserializer::getVector2(const std::string & key, const Vector2 & defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getVector(val[key], defaultValue);
}

Vector3 JsonDeserializer::getVector3(const std::string & key, const Vector3 & defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getVector(val[key], defaultValue);
}

Vector4 JsonDeserializer::getVector4(const std::string & key, const Vector4 & defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getVector(val[key], defaultValue);
}

Viewport JsonDeserializer::getViewport(const std::string & key, const Viewport & defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getViewport(val[key], defaultValue);
}

Matrix3 JsonDeserializer::getMatrix3(const std::string & key, const Matrix3 & defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getMatrix(val[key], defaultValue);
}

Matrix4 JsonDeserializer::getMatrix4(const std::string & key, const Matrix4 & defaultValue) {
	if (!_jsonLib) return defaultValue;
	JsonBox::Value val = value(_jsonLib);
	return deserializerUtils::getMatrix(val[key], defaultValue);
}

bool JsonDeserializer::keyExists(const std::string & key) {
	if (!_jsonLib) return false;
	JsonBox::Value val = value(_jsonLib);
	return !val[key].isNull();
}


	
}