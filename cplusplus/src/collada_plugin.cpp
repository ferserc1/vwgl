
#include <vwgl/collada_plugin.hpp>
#include <vwgl/collada/colladautils.hpp>
#include <vwgl/generic_material.hpp>
#include <irrXML.h>

#include <unordered_map>

namespace vwgl {

scene::Drawable * ColladaToDrawableUtils::buildDrawable() {
	_nonTriangularPlist = false;
	ptr<scene::Drawable> drawable = new scene::Drawable();
	DaeGeometryList::iterator geoIt;
	std::unordered_map<std::string, PolyList*> polyListMap;
	std::unordered_map<std::string, std::vector<PolyList*> > polyListGeometry;
	
	if (_collada.valid()) {
		_geometryList.clear();
		_geometryList = _collada->getLibraryGeometries()->getGeometries();
	}
	else {
		return nullptr;
	}
	
	for (geoIt=_geometryList.begin(); geoIt!=_geometryList.end(); ++geoIt) {
		collada::Geometry * geometry = (*geoIt).getPtr();
		
		geometry->setName(geometry->getName());
		collada::Mesh * mesh = geometry->getMesh();
		
		collada::PolylistList plistList = mesh->getPolylists();
		collada::PolylistList::iterator p_it;
		collada::TrianglesList triangleList = mesh->getTriangles();
		collada::TrianglesList::iterator t_it;
		
		if (plistList.size()>0) {
			for (p_it=plistList.begin(); p_it!=plistList.end(); ++p_it) {
				GenericMaterial * mat = new GenericMaterial();
				PolyList * plist = getPolyList((*p_it).getPtr(), mesh);
				mat->initShader();
				drawable->addPolyList(plist, mat);

				polyListMap[geometry->getId()] = plist;
				polyListGeometry[geometry->getId()].push_back(plist);
			}
		}
		if (triangleList.size()>0) {
			for (t_it=triangleList.begin(); t_it!=triangleList.end(); ++t_it) {
				if ((*t_it)->getCount()>0) {
					PolyList * plist = getPolyList((*t_it).getPtr(), mesh);
					GenericMaterial * mat = new GenericMaterial();
					mat->initShader();
					drawable->addPolyList(plist, mat);
					polyListMap[geometry->getId()] = plist;
					polyListGeometry[geometry->getId()].push_back(plist);
				}
			}
		}
	}
	
	collada::LibraryVisualScenes * visualScenes = _collada->getLibraryVisualScenes();
	collada::VisualSceneList::iterator it;
	for (it=visualScenes->getVisualSceneList().begin(); it!=visualScenes->getVisualSceneList().end(); ++it) {
		collada::SceneNodeList nodeList = (*it)->getSceneNodeList();
		collada::SceneNodeList::iterator nit;
		for (nit=nodeList.begin(); nit!=nodeList.end(); ++nit) {
			if ((*nit)->getInstanceGeometry()) {
				std::string url = (*nit)->getInstanceGeometry()->getUrl();
				if (url[0]=='#') {
					url = url.substr(1);
					if (polyListGeometry.find(url)!=polyListGeometry.end()) {
						std::vector<PolyList*>::iterator vecIt;
						std::vector<PolyList*> & vec = polyListGeometry[url];
						for (vecIt=vec.begin(); vecIt!=vec.end(); ++vecIt) {
							PolyList * drawable = *vecIt;
							if (drawable) applyNodeSettings((*nit).getPtr(), drawable);
						}
					}
//					PolyList * drawable = polyListMap[url];
//					if (drawable) applyNodeSettings((*nit).getPtr(), drawable);
				}
			}
		}
	}
	
	collada::Asset * asset = _collada->getAsset();
	if (asset && asset->getUpAxis()) {
		std::string axis = asset->getUpAxis()->getAxis();
		if (axis=="Z_UP") {
			Matrix4 axisTransform = Matrix4::makeRotation(Math::degreesToRadians(-90.0f), 1.0f, 0.0f, 0.0f);
			drawable->applyTransform(axisTransform);
		}
		else if (axis=="X_UP") {
			// TODO: This is not tested
		}
	}
	
	return drawable.release();
}
	
void ColladaToDrawableUtils::applyNodeSettings(collada::SceneNode * node, PolyList * plist) {
	// Transform matrix
	if (node->getMatrix()) {
		Matrix4 trx = node->getMatrix()->getMatrix();
		// Traspose matrix: collada matrix format is row-major, so we need to traspose it to
		// create an OpenGL column-major matrix
		trx.traspose();
		plist->applyTransform(trx);
	}
}


PolyList * ColladaToDrawableUtils::getPolyList(collada::Polylist * c_plist, collada::Mesh * mesh) {
	ptr<PolyList> plist = new PolyList();
	plist->setName(c_plist->getMaterial());
	plist->setMaterialName(c_plist->getMaterial());
	std::vector<collada::Source * > sources;
	collada::Vcount * vcount = c_plist->getVcount();
	collada::P * p = c_plist->getP();

	fillInSources(c_plist->getInputs(), sources, mesh);
	if (vcount) {
		std::vector<int>::iterator v_it;
		std::vector<int> pArray = p->getArray();
		std::vector<collada::Source*>::iterator s_it;
		
		int p_index = 0;
		int indexCoord = 0;
		int p_indexInc = 0;
		for (v_it=vcount->getArray().begin(); v_it!=vcount->getArray().end(); ++v_it) {
			for (int i=0;i<(*v_it);++i) {
				for (int indexSources = 0; indexSources<sources.size(); ++indexSources) {
					collada::Source * src = sources[indexSources];
					collada::Input * input = c_plist->getInputs()[indexSources].getPtr();
					
					int offset = input->getOffset();
					int nextPIndex = p_index + offset;
					p_indexInc = offset>p_indexInc ? offset:p_indexInc;
					
					if (nextPIndex>=p->getArray().size()) {
						throw ColladaPluginException("Corrupt collada file: unexpected index found in polyList");
					}
					
					int currentIndex = p->getArray()[nextPIndex];
					if (i<3) {
						switch (src->getRole()) {
							case collada::Source::kVertex:
								plist->addVertex(src->getVector3(currentIndex));
								break;
							case collada::Source::kNormal:
								plist->addNormal(src->getVector3(currentIndex));
								break;
							case collada::Source::kTexCoord0:
								plist->addTexCoord0(src->getVector2(currentIndex));
								break;
							case collada::Source::kTexCoord1:
								plist->addTexCoord1(src->getVector2(currentIndex));
								break;
							case collada::Source::kTexCoord2:
								plist->addTexCoord2(src->getVector2(currentIndex));
								break;
							default:
								break;
						}
					}
					else {
						_nonTriangularPlist = true;
					}
				}
				p_index += p_indexInc + 1;
				plist->addIndex(indexCoord);
				++indexCoord;
			}
		}
	}
	
	plist->buildPolyList();

	return plist.release();
}
	
PolyList * ColladaToDrawableUtils::getPolyList(collada::Triangles * c_triangles, collada::Mesh * mesh) {
	ptr<PolyList> plist = new PolyList();
	plist->setName(c_triangles->getMaterial());
	plist->setMaterialName(c_triangles->getMaterial());
	std::vector<collada::Source*> sources;
	collada::P * p = c_triangles->getP();
	
	fillInSources(c_triangles->getInputs(), sources, mesh);
	
	int indexCoord = 0;
	std::vector<collada::Source*>::iterator s_it = sources.begin();
	for (int i=0; i<static_cast<int>(p->getArray().size()); ++i) {
		if (s_it==sources.end()) {
			plist->addIndex(indexCoord);
			++indexCoord;
			s_it = sources.begin();
		}
		collada::Source * src = (*s_it);
		int currentIndex = p->getArray()[i];
		switch (src->getRole()) {
			case collada::Source::kVertex:
				plist->addVertex(src->getVector3(currentIndex));
				break;
			case collada::Source::kNormal:
				plist->addNormal(src->getVector3(currentIndex));
				break;
			case collada::Source::kTexCoord0:
				plist->addTexCoord0(src->getVector2(currentIndex));
				break;
			case collada::Source::kTexCoord1:
				plist->addTexCoord1(src->getVector2(currentIndex));
				break;
			case collada::Source::kTexCoord2:
				plist->addTexCoord2(src->getVector2(currentIndex));
				break;
			default:
				break;
		}
		++s_it;
	}
	plist->addIndex(indexCoord);
	
	plist->buildPolyList();
	return plist.release();
}

void ColladaToDrawableUtils::fillInSources(collada::InputList & inputs, std::vector<collada::Source*> & sources, collada::Mesh * mesh) {
	collada::InputList::iterator it;
	for (it=inputs.begin();it!=inputs.end();++it) {
		collada::Input * input = (*it).getPtr();
		std::string srcId = input->getSource();
		if (srcId.front()=='#') srcId.erase(srcId.begin());	// TODO: search by id
		collada::Source * src = mesh->getSources()[srcId].getPtr();
		if (!src) {
			collada::Vertices * vert = mesh->getVertices()[srcId].getPtr();
			if (vert) {
				srcId = vert->getSourceId();
				if (srcId.front()=='#') srcId.erase(srcId.begin());
				src = mesh->getSources()[srcId].getPtr();
			}
		}
		if (!src) {
			throw ColladaPluginException("Malformed collada file. No such source in mesh ");
		}
		src->setRole(input->getRole());
		sources.push_back(src);
	}
}

ColladaModelReader::ColladaModelReader() {
}

ColladaModelReader::~ColladaModelReader() {
}

bool ColladaModelReader::acceptFileType(const std::string & path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext=="dae";
}

scene::Drawable * ColladaModelReader::loadModel(const std::string & path) {
	irr::io::IrrXMLReader * xml = irr::io::createIrrXMLReader(path.c_str());
	collada::ColladaReader daeReader;
	ptr<collada::Collada> dae;
	ptr<scene::Drawable> drawable;
	
	try {
		dae = daeReader.parse(reinterpret_cast<void**>(xml));
		
		ColladaToDrawableUtils daeToDrawable(dae.getPtr());
		drawable = daeToDrawable.buildDrawable();
		if (daeToDrawable.getNonTriangularWarning()) {
			addWarningMessage("loadDrawable: non triangular faces was found in the model: " + path);
		}

	} catch (std::exception e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
		addWarningMessage("loadDrawable: could not load collada model from file " + path);
	}
	
	delete xml;
	
	return drawable.release();
}

}
