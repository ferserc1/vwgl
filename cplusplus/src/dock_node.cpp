
#include <vwgl/dock_node.hpp>

namespace vwgl {
	
DockNode::DockNode() {
	
}

DockNode::~DockNode() {
	
}

void DockNode::update() {
	NodeList::iterator it;
	Matrix4 transform = Matrix4::makeIdentity();
	for (it=getNodeList().begin(); it!=getNodeList().end(); ++it) {
		TransformNode * trx = dynamic_cast<TransformNode*>((*it).getPtr());
		DrawableBase * drw = getDrawable(trx);
		if (drw) {
			if (drw->getInputJoint()) {
				drw->getInputJoint()->applyTransform(transform);
				trx->setTransform(transform);
			}
			if (drw->getOutputJoint()) {
				drw->getOutputJoint()->applyTransform(transform);
			}
		}
	}
}

DrawableBase * DockNode::getDrawable(TransformNode * trx) {
	DrawableBase * drawable = nullptr;
	if (trx) {
		NodeList::iterator it;
		for (it=trx->getNodeList().begin(); drawable==nullptr && it!=trx->getNodeList().end(); ++it) {
			DrawableNode * drw = dynamic_cast<DrawableNode*>((*it).getPtr());
			if (drw) {
				drawable = drw->getDrawable();
			}
		}
	}
	return drawable;
}

}