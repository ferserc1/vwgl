
#include <vwgl/vwgltloader.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/generic_material.hpp>


#include <JsonBox.h>

namespace vwgl {
	
	
float getColorComponent(JsonBox::Value & val, float defaultValue = 1.0f) {
	if (val.isDouble()) return (float)val.getDouble();
	else if (val.isInteger()) return (float)val.getInt();
	else return defaultValue;
}

Vector4 getColor(JsonBox::Value & r, float defR, JsonBox::Value & g, float defG, JsonBox::Value & b, float defB, JsonBox::Value & a, float defA) {
	return Vector4(getColorComponent(r,1.0f),
				   getColorComponent(g,1.0f),
				   getColorComponent(b,1.0f),
				   getColorComponent(a,1.0f));
}

Texture * getTexture(JsonBox::Value & js_tex, const std::string & path) {
	ptr<Texture> tex = NULL;
	
	if (js_tex.isString()) {
		std::string fileName = path + js_tex.getString();
		tex = TextureManager::get()->loadTexture(Loader::get()->loadImage(fileName));
	}
	
	return tex.release();
}

void configureMaterial(JsonBox::Value & jsMat, GenericMaterial * mat, const std::string & path) {
	if (jsMat.isObject()) {
		Vector4 diffuseColor = getColor(jsMat["colorTintR"], 1.0f,
										jsMat["colorTintG"], 1.0f,
										jsMat["colorTintB"], 1.0f,
										jsMat["colorTintA"], 1.0f);
		diffuseColor =	getColor(jsMat["diffuseR"], diffuseColor.r(),
								 jsMat["diffuseG"], diffuseColor.g(),
								 jsMat["diffuseB"], diffuseColor.b(),
								 jsMat["diffuseA"], diffuseColor.a());
		
		mat->setDiffuse(diffuseColor);
		mat->setShininess(getColorComponent(jsMat["shininess"],0.0f));
		mat->setTexture(getTexture(jsMat["texture"],path));
		mat->setLightMap(getTexture(jsMat["lightmap"],path));
		mat->setNormalMap(getTexture(jsMat["normalMap"],path));
		
		// castShadows
		// receiveShadows
		mat->setRefractionAmount(getColorComponent(jsMat["refractionAmount"],0.0f));
		mat->setLightEmission(getColorComponent(jsMat["lightEmission"],0.0f));
	}
	mat->setDiffuse(Vector4(1.0f, 1.0f, 1.0f, 1.0f));
}

float * getFloatArray(JsonBox::Value & jsArray, int & size) {
	if (!jsArray.isArray()) return NULL;
	JsonBox::Array vArray = jsArray.getArray();
	JsonBox::Array::iterator it;
	size = (int)vArray.size() ;
	float * array = new float[size];
	int i=0;
	
	for (it=vArray.begin(); it!=vArray.end(); ++it) {
		if (it->isDouble()) {
			array[i] = (float)it->getDouble();
		}
		else if (it->isInteger()) {
			array[i] = (float)it->getInt();
		}
		++i;
	}
	
	return array;
}

int * getIntArray(JsonBox::Value & jsArray, int & size) {
	if (!jsArray.isArray()) return NULL;
	JsonBox::Array vArray = jsArray.getArray();
	JsonBox::Array::iterator it;
	size = (int)vArray.size();
	int * array = new int[size];
	int i=0;
	
	for (it=vArray.begin(); it!=vArray.end(); ++it) {
		if (it->isDouble()) {
			array[i] = (int)it->getDouble();
		}
		else if (it->isInteger()) {
			array[i] = it->getInt();
		}
		++i;
	}
	
	return array;
}

PolyList * getPolyList(JsonBox::Value & jsPlist) {
	JsonBox::Value vertex = jsPlist["vertices"];
	JsonBox::Value normal = jsPlist["normal"];
	JsonBox::Value texcoord0 = jsPlist["texcoord0"];
	JsonBox::Value texcoord1 = jsPlist["texcoord1"];
	JsonBox::Value texcoord2 = jsPlist["texcoord2"];
	JsonBox::Value indices = jsPlist["indices"];
	JsonBox::Value name = jsPlist["name"];
	JsonBox::Value matName = jsPlist["matName"];
	int size;
	
	ptr<PolyList> plist = new PolyList();
	if (name.isNull() || !name.isString()) return NULL;
	else plist->setName(name.getString());
	
	if (matName.isNull() || !matName.isString()) plist->setMaterialName(plist->getName());
	else plist->setMaterialName(matName.getString());
	
	float * array = getFloatArray(vertex,size);
	if (array) {
		for (int i=0;i<size+3;i+=3) {
			plist->addVertex(vwgl::Vector3(array[i],array[i+1],array[i+2]));
		}
		delete array;
	}
	else {
		return NULL;
	}
	array = getFloatArray(normal, size);
	if (array) {
		for (int i=0;i<size+3;i+=3) {
			plist->addNormal(vwgl::Vector3(array[i],array[i+1],array[i+2]));
		}
		delete array;
	}
	array = getFloatArray(texcoord0, size);
	if (array) {
		for (int i=0;i<size+2;i+=2) {
			plist->addTexCoord0(vwgl::Vector2(array[i],array[i+1]));
		}
		delete array;
	}
	array = getFloatArray(texcoord1, size);
	if (array) {
		for (int i=0;i<size+2;i+=2) {
			plist->addTexCoord1(vwgl::Vector2(array[i],array[i+1]));
		}
		delete array;
	}
	array = getFloatArray(texcoord2, size);
	if (array) {
		for (int i=0;i<size+2;i+=2) {
			plist->addTexCoord2(vwgl::Vector2(array[i],array[i+1]));
		}
		delete array;
	}
	int * iArray = getIntArray(indices, size);
	if (iArray) {
		for (int i=0; i<size; ++i) {
			plist->addIndex(iArray[i]);
		}
	}
	else {
		return NULL;
	}
	
	plist->buildPolyList();
	
	return plist.release();
}

Material * getMaterial(JsonBox::Value js_mat, const std::string & path) {
	ptr<GenericMaterial> mat = new GenericMaterial();
	
	if (js_mat.isNull()) {
		mat->setDiffuse(Vector4(1.0f, 0.0f, 0.0f, 1.0f));
	}
	else {
		configureMaterial(js_mat, mat.getPtr(), path);
	}
	
	return mat.release();
}


VwgltLoader::VwgltLoader() {
}

VwgltLoader::~VwgltLoader() {
}
	
bool VwgltLoader::acceptFileType(const std::string & path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext=="vwglt";
}
	
Drawable * VwgltLoader::loadDrawable(const std::string & path) {
	if (acceptFileType(path)) {
		std::string filePath;
		removeLastPathComponent(path,filePath);
		JsonBox::Value file;
		file.loadFromFile(path);
		ptr<Drawable> drawable = new Drawable();
		
		JsonBox::Value js_polyList = file["polyList"];
		JsonBox::Value js_materials = file["materials"];
		
		std::map<std::string,JsonBox::Value> materials;
		
		if (js_materials.isArray()) {
			JsonBox::Array matArray = js_materials.getArray();
			JsonBox::Array::iterator it;
			for (it=matArray.begin(); it!=matArray.end(); ++it) {
				JsonBox::Value name = (*it)["name"];
				if (name.isString()) {
					materials[name.getString()] = *it;
				}
			}
		}
		
		if (js_polyList.isArray()) {
			JsonBox::Array plistArray = js_polyList.getArray();
			JsonBox::Array::iterator it;
			for (it=plistArray.begin(); it!=plistArray.end(); ++it) {
				ptr<PolyList> polyList = getPolyList(*it);
				if (polyList.valid()) {
					ptr<Material> mat = getMaterial(materials[polyList->getMaterialName()], filePath);
					ptr<Solid> solid = new Solid(polyList.getPtr(),mat.getPtr());
					drawable->addSolid(solid.getPtr());
				}
			}
		}
		
		return drawable.release();
	}
	return NULL;
}

}
