
#include <vwgl/texture.hpp>
#include <vwgl/opengl.hpp>
#include <vwgl/glerror.hpp>
#include <iostream>
#include <vwgl/mathutils.hpp>
#include <vwgl/vector.hpp>

namespace vwgl {
	
static int s_randomTextureSize = 128;

Texture::Texture()
	:_textureName(0)
	,_width(0)
	,_height(0)
{
	
}

Texture::~Texture() {
	destroy();
}

void Texture::setActiveTexture(TextureUnit tex) {
	glActiveTexture(tex);
}

void Texture::createTexture() {
	if (_textureName!=0) destroy();
	glGenTextures(1, &_textureName);
}

void Texture::bindTexture(vwgl::Texture::TextureTarget target) {
	glBindTexture(target, _textureName);
}

void Texture::setMinFilter(vwgl::Texture::TextureTarget target, vwgl::Texture::TextureFilter filter) {
	_minFilter = filter;
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter);
}

void Texture::setMagFilter(vwgl::Texture::TextureTarget target, vwgl::Texture::TextureFilter filter) {
	_magFilter = filter;
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter);
}

void Texture::setTextureWrapS(vwgl::Texture::TextureTarget target, vwgl::Texture::TextureWrap wrap) {
	glTexParameteri(target, GL_TEXTURE_WRAP_S, wrap);
}

void Texture::setTextureWrapT(vwgl::Texture::TextureTarget target, vwgl::Texture::TextureWrap wrap) {
	glTexParameteri(target, GL_TEXTURE_WRAP_T, wrap);
}

void Texture::setTextureWrapR(vwgl::Texture::TextureTarget target, vwgl::Texture::TextureWrap wrap) {
	#ifndef VWGL_OPENGL_ES
	glTexParameteri(target, GL_TEXTURE_WRAP_R, wrap);
	#endif
}

void Texture::setImage(vwgl::Texture::TextureTarget target, const vwgl::Image *img) {
	if (img!=nullptr && img->valid()) {
		GLint internalFormat = GL_RGBA;
		GLint format = img->getBytesPerPixel()==3 ? GL_RGB:GL_RGBA;
		GlError::clearError();
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
		GlError::clearError();
		glTexImage2D(target, 0, internalFormat, img->getWidth(), img->getHeight(), 0, format, GL_UNSIGNED_BYTE, img->getBuffer());
		_width = img->getWidth();
		_height = img->getHeight();
		if (GlError::getError()!=GL_NO_ERROR){
			std::cout << GlError::getErrorString() << std::endl;
			GlError::clearError();
		}
		if (_minFilter==Texture::kFilterLinearMipmapLinear ||
			_minFilter==Texture::kFilterLinearMipmapNearest ||
			_minFilter==Texture::kFilterNearestMipmapLinear ||
			_minFilter==Texture::kFilterNearestMipmapNearest ||
			_magFilter==Texture::kFilterLinearMipmapLinear ||
			_magFilter==Texture::kFilterLinearMipmapNearest ||
			_magFilter==Texture::kFilterNearestMipmapLinear ||
			_magFilter==Texture::kFilterNearestMipmapNearest) {
			glGenerateMipmap(target);
			if (GlError::getError()) {
				GlError::clearError();
			}
		}
	}
}
	
void Texture::unbindTexture(TextureTarget target) {
	glBindTexture(target, 0);
}

void Texture::destroy() {
	if (_textureName!=0) {
		glDeleteTextures(1, &_textureName);
		_textureName = 0;
		_width = 0;
		_height = 0;
	}
}

int Texture::getRandomTextureSize() {
	return s_randomTextureSize;
}

Texture * Texture::getRandomTexture() {
	Texture * randomTexture = new vwgl::Texture();
	randomTexture->createTexture();
	randomTexture->bindTexture(Texture::kTargetTexture2D);
	
	int textureSize = s_randomTextureSize;
	randomTexture->_width = textureSize;
	randomTexture->_height = textureSize;
	int dataSize = textureSize * textureSize * 3;
	unsigned char * textureData = new unsigned char[dataSize];
	for (int i=0; i<dataSize; i+=3) {
		float r = Math::random() * 2.0f - 1.0f;
		float g = Math::random() * 2.0f - 1.0f;
		float b = 0.0f;
		
		Vector3 randVector(r,g,b);
		randVector.normalize();
		
		textureData[i] = static_cast<int>(randVector.x() * 255.0f);
		textureData[i+1] = static_cast<int>(randVector.y() * 255.0f);
		textureData[i+2] = static_cast<int>(randVector.z() * 255.0f);
	}
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureSize, textureSize, 0, GL_RGB, GL_UNSIGNED_BYTE, textureData);
	
	randomTexture->setMagFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
	randomTexture->setMinFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
	
	delete [] textureData;
	
	Texture::unbindTexture(Texture::kTargetTexture2D);
	return randomTexture;
}

Texture * Texture::getColorTexture(const vwgl::Color & color, const vwgl::Size2Di & size) {
	Texture * colorTexture = new vwgl::Texture();
	colorTexture->createTexture();
	colorTexture->bindTexture(Texture::kTargetTexture2D);

	int dataSize = size.x() * size.y() * 4;
	colorTexture->_width = size.width();
	colorTexture->_height = size.height();
	unsigned char * textureData = new unsigned char[dataSize];
	for (int i = 0; i < dataSize; i+=4) {
		textureData[i] = static_cast<int>(color.r() * 255);
		textureData[i+1] = static_cast<int>(color.g() * 255);
		textureData[i+2] = static_cast<int>(color.b() * 255);
		textureData[i+3] = static_cast<int>(color.a() * 255);
	}

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x(), size.y(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);

	colorTexture->setMagFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
	colorTexture->setMinFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);

	delete [] textureData;

	Texture::unbindTexture(Texture::kTargetTexture2D);
	return colorTexture;
}

}