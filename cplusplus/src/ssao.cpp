
#include <vwgl/ssao.hpp>
#include <vwgl/drawable.hpp>

#include <vwgl/camera.hpp>

#include <vwgl/graphics.hpp>
#include <vwgl/scene/camera.hpp>

namespace vwgl {
	

	
const int SSAOMaterial::s_maxKernelSize = VWGL_SSAO_MAX_KERNEL_SIZE;

SSAOMaterial::SSAOMaterial()
	:_enabled(true),
	_currentKernelSize(0),
	_kernelSize(16),
	_sampleRadius(2.0),
	_color(Color::black()),
	_blur(4),
	_maxDistance(100000.0f)
{
	initShader();
}

SSAOMaterial::~SSAOMaterial() {
	
}

void SSAOMaterial::initShader() {
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_ssao.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_ssao.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_ssao.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_ssao.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: SSAO shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("def_ssao");
	
	loadVertexAttrib("aVertexPos");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uPositionMap");
	getShader()->initUniformLocation("uNormalMap");
	
	getShader()->initUniformLocation("uProjectionMat");
	
	getShader()->initUniformLocation("uSSAORandomMap");
	getShader()->initUniformLocation("uRandomTexSize");
	getShader()->initUniformLocation("uViewportSize");
	getShader()->initUniformLocation("uSSAOSampleRadius");
	getShader()->initUniformLocation("uSSAOKernelOffsets");
	getShader()->initUniformLocation("uSSAOKernelSize");
	getShader()->initUniformLocation("uSSAOColor");
	getShader()->initUniformLocation("uEnabled");
	getShader()->initUniformLocation("uMaxDistance");
}
		
void SSAOMaterial::setupUniforms() {
	getShader()->setUniform("uPositionMap",_positionMap.getPtr(),vwgl::Texture::kTexture0);
	getShader()->setUniform("uNormalMap",_normalMap.getPtr(),vwgl::Texture::kTexture1);
	
	Matrix4 proj = projectionMatrix();
	
	scene::Camera * cam = scene::Camera::getMainCamera();
	Camera * legacyCam = CameraManager::get()->getMainCamera();
	if (cam) {
		proj = cam->getProjectionMatrix();
	}
	else if (legacyCam) {
		proj = legacyCam->getProjectionMatrix();
	}
	
	getShader()->setUniform("uProjectionMat", projectionMatrix());

	if (_currentKernelSize!=_kernelSize) {
		for (int i=0;i<_kernelSize*3;i+=3) {
			Vector3 kernel(Math::random() * 2.0f - 1.0f,
						   Math::random() * 2.0f - 1.0f,
						   Math::random());
			kernel.normalize();
			
			float scale = static_cast<float>(i/3) / _kernelSize;
			scale = Math::lerp(0.1f, 1.0f, scale * scale);
			
			kernel.scale(scale);
			
			_kernelOffsets[i] = kernel.x();
			_kernelOffsets[i+1] = kernel.y();
			_kernelOffsets[i+2] = kernel.z();
		}
		_currentKernelSize = _kernelSize;
	}
	
	getShader()->setUniform("uSSAORandomMap",getRandomTexture(),vwgl::Texture::kTexture2);
	getShader()->setUniform("uRandomTexSize", Vector2(static_cast<float>(Texture::getRandomTextureSize())));
	getShader()->setUniform("uViewportSize", _viewportSize);
	getShader()->setUniform("uSSAOSampleRadius",_sampleRadius);
	getShader()->setUniform3v("uSSAOKernelOffsets",_kernelSize,_kernelOffsets);
	getShader()->setUniform("uSSAOKernelSize",_kernelSize);
	getShader()->setUniform("uSSAOColor", _color);
	getShader()->setUniform("uEnabled", _enabled);
	getShader()->setUniform("uMaxDistance", _maxDistance);
}
	
Texture * SSAOMaterial::getRandomTexture() {
	if (!_randomTexture.valid()) {
		_randomTexture = Texture::getRandomTexture();
	}
	return _randomTexture.getPtr();
}
	
}
