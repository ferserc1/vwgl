
#include <vwgl/forwardrenderer.hpp>
#include <vwgl/state.hpp>
#include <vwgl/opengl.hpp>
#include <vwgl/light.hpp>
#include <vwgl/gizmo_manager.hpp>

namespace vwgl {

ForwardRenderer::ForwardRenderer() :_clearColor(0.3f,0.5f,0.9f,1.0f) {
	_initVisitor = new InitVisitor();
}

ForwardRenderer::~ForwardRenderer() {
	destroy();
}

void ForwardRenderer::build(Group * sceneRoot, int width, int height) {
	_sceneRoot = sceneRoot;
	_mapSize = Size2Di(width,height);
	_projectorMat = new ProjTextureDeferredMaterial();
	_sceneRenderer = new SceneRenderer();
}

void ForwardRenderer::destroy() {
}

void ForwardRenderer::draw(Camera * cam) {
	if (!cam) cam = vwgl::CameraManager::get()->getMainCamera();
	RenderSettings::setCurrentRenderSettings(&_renderSettings);
	_initVisitor->visit(_sceneRoot.getPtr());
	
	LightManager::get()->prepareFrame(true);
	
	State::get()->setViewport(_viewport);
	glClearColor(_clearColor.r(), _clearColor.g(), _clearColor.b(), _clearColor.a());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	CameraManager::get()->applyTransform(cam);
	
	_sceneRenderer->getNodeVisitor()->setMaterial(NULL);
	_sceneRenderer->getNodeVisitor()->setUseMaterialSettings(false);

	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0, 1.0);
	_sceneRenderer->draw(_sceneRoot.getPtr(),true);
	glDisable(GL_POLYGON_OFFSET_FILL);
	
	// Render projectors
	_sceneRenderer->getNodeVisitor()->setMaterial(_projectorMat.getPtr());
	_sceneRenderer->getNodeVisitor()->setUseMaterialSettings(true);
	
	glDepthMask(GL_FALSE);
	ProjectorManager::ProjectorList & proj = ProjectorManager::get()->getProjectorList();
	ProjectorManager::ProjectorList::iterator it;

	
	for (it=proj.begin(); it!=proj.end(); ++it) {
		if ((*it)->isVisible()) {
			glEnable(GL_BLEND);
			glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
			_projectorMat->setProjector(*it);
			_sceneRenderer->draw(_sceneRoot.getPtr(),true);
		}
	}
	

	
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
	glClear(GL_DEPTH_BUFFER_BIT);
	vwgl::GizmoManager::get()->drawGizmo(cam);
}
	
void ForwardRenderer::configureSceneRoot() {
	
}


}
