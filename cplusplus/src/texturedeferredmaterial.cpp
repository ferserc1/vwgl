#include <vwgl/drawable.hpp>
#include <vwgl/texturedeferredmaterial.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

TextureDeferredMaterial::TextureDeferredMaterial() :DeferredMaterial() {
	initShader();
}

TextureDeferredMaterial::~TextureDeferredMaterial() {
}

void TextureDeferredMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_texture.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_texture.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_texture.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_texture.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: texture deferred shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("def_texture");
	
	loadVertexAttrib("aVertexPosition");
	loadNormalAttrib("aNormalPosition");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uMMatrix");
	getShader()->initUniformLocation("uNMatrix");
	getShader()->initUniformLocation("uVMatrixInv");
	
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uSelectMode");
	getShader()->initUniformLocation("uColorTint");
	getShader()->initUniformLocation("uIsEnabled");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
	
	getShader()->initUniformLocation("uNormalMap");
	getShader()->initUniformLocation("uNormalMapOffset");
	getShader()->initUniformLocation("uNormalMapScale");
	
	getShader()->initUniformLocation("uCubeMap");
	getShader()->initUniformLocation("uUseCubeMap");
	getShader()->initUniformLocation("uReflectionAmount");
	getShader()->initUniformLocation("uAlphaCutoff");
}

void TextureDeferredMaterial::setupUniforms() {
	getShader()->setUniform("uMVMatrix", modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());
	getShader()->setUniform("uMMatrix", modelMatrix());
	getShader()->setUniform("uNMatrix", normalMatrix());
	vwgl::Matrix4 vMatrix = viewMatrix();
	vMatrix.invert();
	getShader()->setUniform("uVMatrixInv", vMatrix);
	
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uTexture", mat->getTexture(),Texture::kTexture0);
		getShader()->setUniform("uIsEnabled", mat->getTexture()!=NULL);
		getShader()->setUniform("uColorTint", mat->getDiffuse());
		getShader()->setUniform("uTextureOffset", mat->getTextureOffset());
		getShader()->setUniform("uTextureScale", mat->getTextureScale());
		
		if (mat->getNormalMap()) {
			getShader()->setUniform("uUseNormalMap", true);
			getShader()->setUniform("uNormalMap", mat->getNormalMap(),Texture::kTexture1);
			getShader()->setUniform("uNormalMapScale", mat->getNormalMapScale());
			getShader()->setUniform("uNormalMapOffset", mat->getNormalMapOffset());
		}
		else {
			getShader()->setUniform("uUseNormalMap", false);
		}
	
		if (mat->getCubeMap()) {
			getShader()->setUniform("uUseCubeMap", true);
			getShader()->setUniform("uCubeMap", mat->getCubeMap(),Texture::kTexture2);
		}
		else {
			getShader()->setUniform("uUseCubeMap", false);
			Texture::setActiveTexture(Texture::kTexture2);
			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
			getShader()->setUniform("uCubeMap", 2);
		}
		getShader()->setUniform("uReflectionAmount", mat->getReflectionAmount());
		getShader()->setUniform("uAlphaCutoff", mat->getAlphaCutoff());

	}
}

}
