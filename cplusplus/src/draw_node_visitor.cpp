
#include <vwgl/draw_node_visitor.hpp>

#include <vwgl/state.hpp>
#include <vwgl/group.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/drawable_node.hpp>

namespace vwgl {

DrawNodeVisitor::DrawNodeVisitor() :_useMaterialSettings(false), _renderTransparent(true), _renderOpaque(true) {
	
}

DrawNodeVisitor::~DrawNodeVisitor() {
	
}

void DrawNodeVisitor::visit(vwgl::Node *node) {
	if (node && node->isEnabled()) {
		Group * gn = dynamic_cast<Group*>(node);
		TransformNode * tn = dynamic_cast<TransformNode*>(node);
		DrawableNode * dn = dynamic_cast<DrawableNode*>(node);
		
		State::get()->pushModelMatrix();
		if (tn) {
			tn->prepareMatrix(State::get()->modelMatrix());
			State::get()->modelMatrix().mult(tn->getTransform());
		}
		if (gn) {
			NodeList::iterator it;
			for (it=gn->getNodeList().begin(); it!=gn->getNodeList().end(); ++it) {
				visit((*it).getPtr());
			}
		}
		if (dn && dn->getDrawable()) {
			dn->getDrawable()->setRenderTransparent(_renderTransparent);
			dn->getDrawable()->setRenderOpaque(_renderOpaque);
			dn->getDrawable()->draw(_material.getPtr(),_useMaterialSettings);
		}
		State::get()->popModelMatrix();
	}
}

}
