
#include <vwgl/glerror.hpp>

#include <vwgl/opengl.hpp>


namespace vwgl {

int GlError::s_lastError = GL_NO_ERROR;

GLint GlError::getError() {
	s_lastError = glGetError();
	return s_lastError;
}

void GlError::clearError() {
	s_lastError = GL_NO_ERROR;
}

std::string GlError::getErrorString() {
	if (s_lastError!=GL_NO_ERROR) {
		switch (s_lastError) {
			case GL_NO_ERROR:
				return "";
			case GL_INVALID_ENUM:
				return "GL_INVALID_ENUM: An unacceptable value is specified for an enumerated argument.";
			case GL_INVALID_VALUE:
				return "GL_INVALID_VALUE: A numeric argument is out of range.";
			case GL_INVALID_OPERATION:
				return "GL_INVALID_OPERATION: The specified operation is not allowed in the current state.";
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				return "GL_INVALID_FRAMEBUFFER_OPERATION: The framebuffer object is not complete.";
			case GL_OUT_OF_MEMORY:
				return "GL_OUT_OF_MEMORY: There is not enough memory left to execute the command.";
			default:
				break;
		}
	}
	return "";
}
	
bool GlError::getCompileShaderStatus(GLint shader) {
	GLint success = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (success==GL_FALSE) {
		return false;
	}
	return true;
}
	
std::string GlError::getCompileErrorString(GLint shader) {
	std::string result = "";
	GLint success = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (success==GL_FALSE) {
		GLint logSize = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
		char * errorLog = new char[logSize+1];
		glGetShaderInfoLog(shader, logSize, &logSize, errorLog);
		errorLog[logSize] = '\0';
		result = errorLog;
		delete [] errorLog;
	}
	return result;
}

bool GlError::getLinkProgramStatus(GLint program) {
	int result;
	glGetProgramiv(program, GL_LINK_STATUS, &result);
	return result!=0;
}

std::string GlError::getLinkErrorString(GLint program) {
	GLint length;
	GLint result;
	char *log;

	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
	log = new char[length + 1];
	glGetProgramInfoLog(program, length, &result, log);

	std::string resultString(log);
    delete [] log;

	return resultString;
}
	
}
