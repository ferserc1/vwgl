
#include <vwgl/render_canvas.hpp>
#include <vwgl/opengl.hpp>
#include <vwgl/state.hpp>
#include <vwgl/drawable.hpp>


namespace vwgl {

RenderCanvas::RenderCanvas() :_clear(true) {
	buildCanvas();
}

RenderCanvas::~RenderCanvas() {
	
}

void RenderCanvas::buildCanvas() {
	_plist = new PolyList();
	
	_plist->addVertex(Vector3( 0.5f, 0.5f, 0.0f));
	_plist->addVertex(Vector3(-0.5f, 0.5f, 0.0f));
	_plist->addVertex(Vector3(-0.5f,-0.5f, 0.0f));
	_plist->addVertex(Vector3( 0.5f,-0.5f, 0.0f));
	
	_plist->addTexCoord0(Vector2(1.0f,1.0f));
	_plist->addTexCoord0(Vector2(0.0f,1.0f));
	_plist->addTexCoord0(Vector2(0.0f,0.0f));
	_plist->addTexCoord0(Vector2(1.0f,0.0f));
	
	_plist->addIndex(0);
	_plist->addIndex(1);
	_plist->addIndex(2);
	_plist->addIndex(2);
	_plist->addIndex(3);
	_plist->addIndex(0);
	
	_plist->buildPolyList();
	
	_plist->setDrawMode(PolyList::kTriangles);
	
	//_projection.makeOrtho(-1, 1, -1, 1, 0.1, 100.0);
}

void RenderCanvas::draw(int w, int h) {
	if (_material.valid()) {
		vwgl::State * s = vwgl::State::get();
		glViewport(0,0,w,h);
		if (_clear) glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		s->projectionMatrix() = Matrix4::makeOrtho(-0.5f, 0.5f, -0.5f, 0.5f, 0.1f, 100.0f);
		s->viewMatrix() = Matrix4::makeTranslation(0.0f, 0.0f, -1.0f);
		s->modelMatrix() = Matrix4::makeIdentity();
		
		
		_material->bindPolyList(_plist.getPtr());
		_material->activate();
		_plist->drawElements();
		_material->deactivate();
	}
}

HomogeneousRenderCanvas::HomogeneousRenderCanvas() {
	_plist = new PolyList();
	
	_plist->addVertex(Vector3( 1.0, 1.0, 0.0));
	_plist->addVertex(Vector3(-1.0, 1.0, 0.0));
	_plist->addVertex(Vector3(-1.0,-1.0, 0.0));
	_plist->addVertex(Vector3( 1.0,-1.0, 0.0));
	
	_plist->addTexCoord0(Vector2(1.0,1.0));
	_plist->addTexCoord0(Vector2(0.0,1.0));
	_plist->addTexCoord0(Vector2(0.0,0.0));
	_plist->addTexCoord0(Vector2(1.0,0.0));
	
	_plist->addTriangle(0,1,2);
	_plist->addTriangle(2,3,0);
	
	_plist->buildPolyList();
	
	_plist->setDrawMode(vwgl::PolyList::kTriangles);
}

HomogeneousRenderCanvas::~HomogeneousRenderCanvas() {
	destroy();
}

void HomogeneousRenderCanvas::draw(int w, int h) {
	if (_material.valid()) {
		glViewport(0,0,w,h);
		if (_clear)
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		_material->bindPolyList(_plist.getPtr());
		_material->activate();
		_plist->drawElements();
		_material->deactivate();
	}
}

void HomogeneousRenderCanvas::destroy() {
	_plist->destroy();
}
	
}
