
#include <vwgl/graphics.hpp>

#include <vwgl/system.hpp>

#include <vwgl/opengl.hpp>

#include <iostream>

namespace vwgl {

Graphics * Graphics::s_singleton = nullptr;

Graphics::Graphics()
	:_api(Api::kApiOpenGL)
{

}

Graphics::~Graphics() {

}

bool Graphics::useApi(Api api)
{
	if (api == kApiDirectX && !VWGL_DIRECTX_AVAILABLE) {
		return false;
	}
	_api = api;
	return true;
}

bool Graphics::useApi(System::Platform platform, Api api)
{
	if (System::get()->isPlatform(platform)) {
		return useApi(api);
	}
	return false;
}

void Graphics::initContext()
{
	int version = OpenGL::kVersion2;
	switch (_api) {
	case kApiOpenGLBasic:
		version = OpenGL::kVersion2;
		break;
	case kApiOpenGL:
		version = OpenGL::kVersion3;
		break;
	case kApiOpenGLAdvanced:
		version = OpenGL::kVersion4;
		break;
	case kApiDirectX:
		break;
	}

	if (_api != kApiDirectX) {
		if (!OpenGL::init(version)) {
			std::cerr << "Warning: Your platform does not support the specified OpenGL version" << std::endl;
			switch (OpenGL::version()) {
			case OpenGL::kVersion1:
			case OpenGL::kVersion15:
			case OpenGL::kVersion2:
				_api = kApiOpenGLBasic;
				break;
			case OpenGL::kVersion3:
			case OpenGL::kVersion32:
			case OpenGL::kVersion33:
				_api = kApiOpenGL;
				break;
			case OpenGL::kVersion4:
				_api = kApiOpenGLAdvanced;
				break;
			}
		}
	}
}

bool Graphics::isAvailable(Api api)
{
	if (api == kApiDirectX && VWGL_DIRECTX_AVAILABLE) {
		return true;
	}
	else if (api == kApiOpenGL) {
		return true;
	}
	else return false;
}

}
