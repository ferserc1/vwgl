
#include <vwgl/physics/intersection.hpp>

namespace vwgl {
namespace physics {

RayToPlaneIntersection Intersection::rayToPlane(const vwgl::physics::Ray &ray, const vwgl::physics::Plane &plane) {
	return RayToPlaneIntersection(ray, plane);
}

RayToPlaneIntersection::RayToPlaneIntersection(const vwgl::physics::Ray &ray, const vwgl::physics::Plane &plane) {
	_type = Intersection::kTypePoint;
	Vector3 p0(plane.getOrigin());
	Vector3 n(plane.getNormal());
	Vector3 l0(ray.getStart());
	Vector3 l(ray.getVector());
	float num = p0.sub(l0).dot(n);
	float den = l.dot(n);
	float distance = 0;

	if (den==0) {
		_intersects = false;
	}
	else if ((distance=num/den)>ray.getMagnitude()) {
		_intersects = false;
	}
	else {
		_ray = Ray::rayWithVector(ray.getVector(), ray.getStart(), distance);
		_p0 = _ray.getEnd();
		_intersects = true;
	}
}

}
}
