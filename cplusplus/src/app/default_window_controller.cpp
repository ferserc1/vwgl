
#include <vwgl/app/default_window_controller.hpp>


#include <vwgl/vwglb_plugin.hpp>
#include <vwgl/collada_plugin.hpp>
#include <vwgl/obj_reader.hpp>
#include <vwgl/scene_loader.hpp>
#include <vwgl/freetype_reader.hpp>
#include <vwgl/scene_writter.hpp>
#include <vwgl/library_reader.hpp>
#include <vwgl/library_writter.hpp>
#include <vwgl/dictionary_loader.hpp>
#include <vwgl/dictionary_writter.hpp>

#include <vwgl/app/main_loop.hpp>

#include <vwgl/loader.hpp>

#include <vwgl/graphics.hpp>
#include <vwgl/state.hpp>

#include <vwgl/scene/light.hpp>

#include <vwgl/ssao.hpp>
#include <vwgl/shadow_render_pass.hpp>

namespace vwgl {
namespace app {
	
DefaultWindowController::DefaultWindowController(Quality q, scene::Renderer::RenderPath renderPath) :_quality(q), _renderPath(renderPath) {

}

DefaultWindowController::~DefaultWindowController() {

}

void DefaultWindowController::initGL() {
	vwgl::Graphics::get()->initContext();
	vwgl::State::get()->enableDepthTest();

	loadReaders();
	loadWritters();

	_inputMediator.init();
	
	_sceneRoot = createScene();

	initScene(_sceneRoot.getPtr());
	createRenderer(_sceneRoot.getPtr());

	if (_renderer.valid()) {
		configureRenderer(_renderer.getPtr());
		_renderer->init();
		_inputMediator.setRenderer(_renderer.getPtr());
		configureQuality();
	}
}

void DefaultWindowController::reshape(int w, int h) {
	Size2Di size(w, h);
	_renderer->resize(size);
	_inputMediator.resize(size);
}

void DefaultWindowController::draw() {
	_renderer->update();
	_renderer->draw();
	
	window()->glContext()->swapBuffers();
}

void DefaultWindowController::frame(float delta) {
	_renderer->frame(delta);
}

void DefaultWindowController::destroy() {

}

void DefaultWindowController::keyUp(const KeyboardEvent & evt) {
	if (evt.keyboard().key() == Keyboard::kKeyEsc) {
		app::MainLoop::get()->quit(0);
	}
	else {
		_inputMediator.keyUp(evt);
	}
}

void DefaultWindowController::keyDown(const KeyboardEvent & evt) {
	_inputMediator.keyDown(evt);
}

void DefaultWindowController::mouseDown(const MouseEvent & evt) {
	_inputMediator.mouseDown(evt);
}

void DefaultWindowController::mouseDrag(const MouseEvent & evt) {
	_inputMediator.mouseDrag(evt);
}

void DefaultWindowController::mouseMove(const MouseEvent & evt) {
	_inputMediator.mouseMove(evt);
}

void DefaultWindowController::mouseUp(const MouseEvent & evt) {
	_inputMediator.mouseUp(evt);
}

void DefaultWindowController::mouseWheel(const MouseEvent & evt) {
	_inputMediator.mouseWheel(evt);
}

void DefaultWindowController::loadReaders() {
	Loader::get()->registerReader(new vwgl::VWGLBModelReader());
	Loader::get()->registerReader(new vwgl::VWGLBPrefabReader());
	Loader::get()->registerReader(new vwgl::ColladaModelReader());
	Loader::get()->registerReader(new vwgl::SceneReader());
	Loader::get()->registerReader(new vwgl::LibraryLoader());
	Loader::get()->registerReader(new vwgl::DictionaryLoader());
	Loader::get()->registerReader(new vwgl::FreetypeReader());
}

void DefaultWindowController::loadWritters() {
	Loader::get()->registerWritter(new vwgl::VWGLBModelWritter());
	Loader::get()->registerWritter(new vwgl::VWGLBPrefabWritter());
	Loader::get()->registerWritter(new vwgl::SceneWritter());
	Loader::get()->registerWritter(new vwgl::LibraryWritter());
	Loader::get()->registerWritter(new vwgl::DictionaryWritter());
}

scene::Node * DefaultWindowController::createScene() {
	return new scene::Node("Scene root");
}

void DefaultWindowController::initScene(scene::Node * sceneRoot) {
	
}

void DefaultWindowController::createRenderer(scene::Node * sceneRoot) {
	_renderer = scene::Renderer::create(scene::Renderer::kRenderPathDeferred, sceneRoot);
	configureRenderer(_renderer.getPtr());
}

void DefaultWindowController::configureRenderer(scene::Renderer * renderer) {
	_renderer->setClearColor(Color(0.26f, 0.27f, 0.59f));
}

void DefaultWindowController::configureQuality() {
	vwgl::SSAOMaterial * ssao = _renderer->getFilter<vwgl::SSAOMaterial>();
	vwgl::ShadowRenderPassMaterial * shadows = _renderer->getFilter<vwgl::ShadowRenderPassMaterial>();

	switch (_quality) {
	case kQualityLow:
		if (ssao) {
			ssao->setEnabled(false);
		}
		if (shadows) {
			shadows->setShadowType(vwgl::ShadowRenderPassMaterial::kShadowTypeHard);
			shadows->setShadowCascades(1);
		}
		scene::Light::setShadowMapSize(vwgl::Size2Di(512));
		break;
	case kQualityMedium:
		if (ssao) {
			ssao->setEnabled(true);
			ssao->setKernelSize(16);
			ssao->setSampleRadius(0.35f);
			ssao->setBlurIterations(1);
		}
		if (shadows) {
			shadows->setShadowType(vwgl::ShadowRenderPassMaterial::kShadowTypeHard);
			shadows->setShadowCascades(3);
		}
		scene::Light::setShadowMapSize(vwgl::Size2Di(1024));
		break;
	case kQualityHigh:
		if (ssao) {
			ssao->setEnabled(true);
			ssao->setKernelSize(32);
			ssao->setSampleRadius(0.35f);
			ssao->setBlurIterations(4);
		}
		if (shadows) {
			shadows->setShadowType(vwgl::ShadowRenderPassMaterial::kShadowTypeSoft);
			shadows->setShadowCascades(3);
		}
		scene::Light::setShadowMapSize(vwgl::Size2Di(2048));
		break;
	case kQualityExtreme:
		if (ssao) {
			ssao->setEnabled(true);
			ssao->setKernelSize(64);
			ssao->setSampleRadius(0.35f);
			ssao->setBlurIterations(8);
		}
		if (shadows) {
			shadows->setShadowType(vwgl::ShadowRenderPassMaterial::kShadowTypeSoftStratified);
			shadows->setShadowCascades(3);
		}
		scene::Light::setShadowMapSize(vwgl::Size2Di(4096));
		break;
	}
}

}
}