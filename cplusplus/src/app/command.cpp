
#include <vwgl/app/command.hpp>

namespace vwgl {
namespace app {

Command::Command(bool undoable) :_undoable(undoable)
{

}

Command::~Command() {

}

ContextCommand::ContextCommand(plain_ptr ctx, bool undoable) :Command(undoable), _context(ctx)
{

}

ContextCommand::~ContextCommand() {

}

}
}