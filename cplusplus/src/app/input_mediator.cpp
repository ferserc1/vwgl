
#include <vwgl/app/input_mediator.hpp>
#include <vwgl/drawable.hpp>

namespace vwgl {
namespace app {

InputMediator::InputMediator()
{
}

void InputMediator::setGizmo(Gizmo g) {
	switch (g) {
	case kGizmoNone:
		_gizmoManager.setManipulateGizmo(nullptr);
		break;
	case kGizmoTranslate:
		_gizmoManager.setManipulateGizmo(_translateGizmo.getPtr());
		break;
	case kGizmoRotate:
		_gizmoManager.setManipulateGizmo(_rotateGizmo.getPtr());
		break;
	case kGizmoScale:
		_gizmoManager.setManipulateGizmo(_scaleGizmo.getPtr());
		break;
	}
}

InputMediator::Gizmo InputMediator::currentGizmo() {
	if (_gizmoManager.currentGizmo() == _translateGizmo.getPtr()) {
		return kGizmoTranslate;
	}
	else if (_gizmoManager.currentGizmo() == _rotateGizmo.getPtr()) {
		return kGizmoRotate;
	}
	else if (_gizmoManager.currentGizmo() == _scaleGizmo.getPtr()) {
		return kGizmoScale;
	}
	else {
		return kGizmoNone;
	}
}

void InputMediator::init() {
	_rotateGizmo = new manipulation::RotateGizmo();
	_scaleGizmo = new manipulation::ScaleGizmo();
	_translateGizmo = new manipulation::TranslateGizmo();

	_gizmoManager.initDefaultIcons();
	_gizmoManager.initDefaultPlist();
	_mousePicker.setGizmoManager(_gizmoManager);
	manipulation::Gizmo::setCommandManager(_commandManager);
	_selectionHandler.setMarkMaterials(true);
}

void InputMediator::drawGizmos() {
	if (_renderer.valid() && _sceneRoot.getPtr() != _renderer->getSceneRoot()) {
		setSceneRoot(_renderer->getSceneRoot());
	}
	_gizmoManager.draw();
}

void InputMediator::keyDown(const app::KeyboardEvent & evt) {
	if (_renderer.valid()) {
		_renderer->keyDown(evt);
	}
}

void InputMediator::keyUp(const app::KeyboardEvent & evt) {
	if (_renderer.valid()) {
		_renderer->keyUp(evt);
	}
}

void InputMediator::mouseDown(const app::MouseEvent & evt) {
	_selectionHandler.setAdditiveSelection(evt.mouse().getButtonStatus(Mouse::kRightButton));
	_mouseDownCoords = evt.pos();
	if (_mousePicker.gizmo(evt.pos(), _viewport)) {
		_gizmoManager.mouseDown(_mousePicker.getPickedGizmoPolyList(), evt);
	}
	else if (_renderer.valid()) {
		_renderer->mouseDown(evt);
	}
}

void InputMediator::mouseUp(const app::MouseEvent & evt) {
	if (!_gizmoManager.mouseUp(evt)) {
		if (_mouseDownCoords.distance(evt.pos()) < 2.0f) {
			if (_mousePicker.pick(evt.pos(), _viewport)) {
				_selectionHandler.select(_mousePicker.getPickedNode(),
										 _mousePicker.getPickedDrawable(),
										 _mousePicker.getPickedPolyList(),
										 _mousePicker.getPickedMaterial());
				manipulation::Selectable * selectable = _mousePicker.getSelectable();
				if (selectable) {
					_gizmoManager.setManipulateObject(selectable->sceneObject());
				}
				
			}
			else if (!_selectionHandler.isAdditiveSelection()) {
				_selectionHandler.clearSelection();
				_gizmoManager.setManipulateObject(nullptr);
			}
		}
		if (_renderer.valid()) {
			_renderer->mouseUp(evt);
		}
	}
}

void InputMediator::mouseMove(const app::MouseEvent & evt) {
	if (_renderer.valid()) {
		_renderer->mouseMove(evt);
	}
}

void InputMediator::mouseDrag(const app::MouseEvent & evt) {
	if (!_gizmoManager.mouseDrag(evt) && _renderer.valid()) {
		_renderer->mouseDrag(evt);
	}
}

void InputMediator::mouseWheel(const app::MouseEvent & evt) {
	if (_renderer.valid()) {
		_renderer->mouseWheel(evt);
	}
}


}
}
