
#include <vwgl/app/command_manager.hpp>

#include <iostream>
#include <exception>
#include <algorithm>

namespace vwgl {
namespace app {

CommandManager::CommandManager()
	:_setCurrentContext(nullptr)
	,_maxUndos(300)
{

}
	
void CommandManager::registerObserver(ICommandManagerObserver * obs) {
	CommandManagerObserverVector::iterator it = std::find(_observers.begin(), _observers.end(), obs);
	if (it==_observers.end()) {
		_observers.push_back(obs);
	}
}
	
void CommandManager::unregisterObserver(ICommandManagerObserver * obs) {
	CommandManagerObserverVector::iterator it = std::find(_observers.begin(), _observers.end(), obs);
	if (it!=_observers.end()) {
		_observers.erase(it);
	}
}

void CommandManager::setCurrentContext(ContextCommand * command) {
	if (_setCurrentContext && command && command->getContextPtr()) {
		_setCurrentContext(command->getContextPtr());
	}
}

void CommandManager::clearCommandStack() {
	_undoStack.clear();
	_redoStack.clear();
}

bool CommandManager::execute(Command * cmd) {
	ptr<Command> command = cmd;
	eachObserver([&](ICommandManagerObserver * observer) {
		observer->willExecuteCommand(cmd);
	});
	if (executeCommand(cmd, true)) {
		if (cmd->isUndoable()) {
			_undoStack.push_back(cmd);
			_redoStack.clear();
			if (_undoStack.size() > _maxUndos) {
				_undoStack.pop_front();
			}
		}
		eachObserver([&](ICommandManagerObserver * observer) {
			observer->didExecuteCommand(cmd);
		});
		return true;
	}
	else {
		eachObserver([&](ICommandManagerObserver * observer) {
			observer->commandDidFail(cmd);
		});
		return false;
	}
}

void CommandManager::undo() {
	if (canUndo()) {
		ptr<Command> cmd = _undoStack.back();
		eachObserver([&](ICommandManagerObserver * observer) {
			observer->willUndo(cmd.getPtr());
		});
		if (executeCommand(cmd.getPtr(), false)) {
			_undoStack.pop_back();
			_redoStack.push_back(cmd.getPtr());
			eachObserver([&](ICommandManagerObserver * observer) {
				observer->didUndo(cmd.getPtr());
			});
		}
		else {
			eachObserver([&](ICommandManagerObserver * observer) {
				observer->commandDidFail(cmd.getPtr());
			});
		}
	}
}

void CommandManager::redo() {
	if (canRedo()) {
		ptr<Command> cmd = _redoStack.back();
		eachObserver([&](ICommandManagerObserver * observer) {
			observer->willRedo(cmd.getPtr());
		});
		if (executeCommand(cmd.getPtr(), true)) {
			_redoStack.pop_back();
			_undoStack.push_back(cmd.getPtr());
			eachObserver([&](ICommandManagerObserver * observer) {
				observer->didRedo(cmd.getPtr());
			});
		}
		else {
			eachObserver([&](ICommandManagerObserver * observer) {
				observer->commandDidFail(cmd.getPtr());
			});
		}
	}
}

bool CommandManager::executeCommand(Command * cmd, bool doCommand) {
	if (cmd) {
		try {
			setCurrentContext(dynamic_cast<ContextCommand*>(cmd));
			doCommand ? cmd->doCommand() : cmd->undoCommand();
			return true;
		}
		catch (std::runtime_error & e) {
			std::cerr << "Error executing command: " << e.what() << std::endl;
		}
		catch (std::exception & e) {
			std::cerr << "Error executing command: " << e.what() << std::endl;
		}
	}
	return false;
}

}
}
