
#include <vwgl/vwglb_file_reader.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/texture_manager.hpp>

#include <JsonBox.h>

namespace vwgl {

class VWGLBFileReaderUtils {
public:
	static float getJsonNumber(const JsonBox::Value & val) {
		if (val.isInteger()) return static_cast<float>(val.getInt());
		else if (val.isDouble()) return static_cast<float>(val.getDouble());
		else return 0.0f;
	}

	static Vector3 getJsonVector(const JsonBox::Value & val) {
		if (val.isArray() && val.getArray().size() == 3) {
			return Vector3(getJsonNumber(val.getArray()[0]),
				getJsonNumber(val.getArray()[1]),
				getJsonNumber(val.getArray()[2]));
		}
		return Vector3();
	}

	static float getFloat(JsonBox::Value & data, float defaultValue) {
		if (data.isDouble()) return static_cast<float>(data.getDouble());
		else if (data.isString()) return static_cast<float>(atof(data.getString().c_str()));
		else if (data.isBoolean()) return data.getBoolean() ? 1.0f : 0.0f;
		else if (data.isInteger()) return static_cast<float>(data.getInt());
		else return defaultValue;
	}

	static Matrix4 getMatrix(JsonBox::Value & data) {
		Matrix4 result;

		if (data.isString()) {
			std::string matrixData = data.getString();
			std::stringstream stream(matrixData);
			float value[16];

			for (int i = 0; i<16; ++i) {
				stream >> value[i];
			}

			return Matrix4(value);
		}

		return result;
	}

	static Texture * getTexture(JsonBox::Value & js_tex, const std::string & path) {
		ptr<Texture> tex = NULL;

		if (js_tex.isString()) {
			std::string fileName = js_tex.getString();
			if (!vwgl::System::get()->isAbsolutePath(fileName)) {
				fileName = path + fileName;
			}
			tex = TextureManager::get()->loadTexture(Loader::get()->loadImage(fileName));
		}

		return tex.release();
	}

	static bool getBool(JsonBox::Value & js_val, bool defaultValue) {
		if (js_val.isBoolean()) {
			return js_val.getBoolean();
		}
		else if (js_val.isInteger()) {
			return js_val.getInt() == 1;
		}
		return defaultValue;
	}

	static void getString(JsonBox::Value & js_val, std::string & inputString, const std::string & defaultValue) {
		if (js_val.isString()) {
			inputString = js_val.getString();
		}
		else {
			inputString = defaultValue;
		}
	}

	static void getPlistSettings(JsonBox::Value & js_val, const std::string & absPath,
		ptr<Texture> & shadowTexture, Matrix4 & projection, Matrix4 & transform, float & projAttenuation,
		std::string & groupName, bool & visible)
	{
		Texture * tex;
		if (js_val.isObject()) {
			if ((tex = getTexture(js_val["shadowTexture"], absPath)) != nullptr) {
				tex->bindTexture(vwgl::Texture::kTargetTexture2D);
				tex->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
				tex->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
				shadowTexture = tex;
				projAttenuation = getFloat(js_val["shadowAttenuation"], 1.0f);
				projection = getMatrix(js_val["shadowProjection"]);
				transform = getMatrix(js_val["shadowTransform"]);
			}
			else {
				shadowTexture = nullptr;
			}

			visible = getBool(js_val["visible"], true);
			getString(js_val["groupName"], groupName, "");
		}
	}
};

bool VWGLBFileReader::load(const std::string &path) {
	if (_delegate == nullptr) return false;
	_plistMap.clear();

	if (_fileUtils.open(path, VwglbUtils::kModeRead)) {
		try {
			std::string materialsString;
			int numberOfPlist;
			readHeader(materialsString,numberOfPlist);

			readPolyList(numberOfPlist);

			_delegate->materials(materialsString);

			loadProjectors(materialsString);
			
			_plistMap.clear();
			_fileUtils.close();

			return true;
		}
		catch (std::exception & e) {
			_delegate->onError(e.what());
			_plistMap.clear();
		}
	}
	return false;
}

bool VWGLBFileReader::loadHeader(const std::string &path) {
	if(_delegate == nullptr) return false;
	return false;
}
	
void VWGLBFileReader::readHeader(std::string & materialsString, int & numberOfPlist) {
	//// File header
	// File endian 0=big endian, 1=little endian
	unsigned char endian;
	_fileUtils.readByte(endian);
	if (endian == 0) _fileUtils.setBigEndian();
	else _fileUtils.setLittleEndian();

	// Version (major, minor, rev)
	VWGLBFileReaderDelegate::FileVersion version;
	_fileUtils.readByte(version.major);
	_fileUtils.readByte(version.minor);
	_fileUtils.readByte(version.revision);

	_delegate->fileVersion(version);

	// Header type
	VwglbUtils::BlockType btype;
	_fileUtils.readBlock(btype);
	if (btype != VwglbUtils::kHeader) {
		throw BaseException("VWGLB: File format exception. Expecting begin of file header");
	}

	// Number of poly list
	_fileUtils.readInteger(numberOfPlist);
	_delegate->metadata(VWGLBFileReaderDelegate::kNumberOfPlist, numberOfPlist);

	// Materials
	VwglbUtils::BlockType block;
	_fileUtils.readBlock(block);
	if (block != VwglbUtils::kMaterials) {
		throw BaseException("VWGLB: File format exception. Expecting material list.");
	}

	_fileUtils.readString(materialsString);

	// Projector
	_fileUtils.readBlock(block);
	if (block == VwglbUtils::kShadowProjector) {
		std::string currentDir = Loader::get()->getCurrentDir();
		std::string fileName;
		float proj[16];
		float trans[16];
		float attenuation;
		_fileUtils.readString(fileName);
		_fileUtils.readFloat(attenuation);
		for (int i = 0; i<16; ++i) {
			_fileUtils.readFloat(proj[i]);
		}
		for (int i = 0; i<16; ++i) {
			_fileUtils.readFloat(trans[i]);
		}
		ptr<Projector> shadowProj = new Projector();

		if (!vwgl::System::get()->isAbsolutePath(fileName)) {
			fileName = currentDir + fileName;
		}
		ptr<Texture> projectorTexture = vwgl::Loader::get()->loadTexture(fileName);
		projectorTexture->bindTexture(vwgl::Texture::kTargetTexture2D);
		projectorTexture->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		projectorTexture->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		_delegate->projector(projectorTexture.getPtr(), vwgl::Matrix4(proj), vwgl::Matrix4(trans), attenuation);
	}
	else {
		_fileUtils.seekForward(-4);
	}

	// Joint list
	// TODO: Support for multiple types of joints, and multiple output joints
	_fileUtils.readBlock(block);
	if (block == VwglbUtils::kJoint) {
		std::string jointData;
		_fileUtils.readString(jointData);

		JsonBox::Value joints;
		joints.loadFromString(jointData);

		if (joints["input"].isObject()) {
			JsonBox::Object js_inJoint = joints["input"].getObject();
			// TODO: Support for multiple types of joints
			_delegate->joint(VWGLBFileReaderDelegate::kJointTypeInput,
				VWGLBFileReaderUtils::getJsonVector(js_inJoint["offset"]),
				VWGLBFileReaderUtils::getJsonNumber(js_inJoint["pitch"]),
				VWGLBFileReaderUtils::getJsonNumber(js_inJoint["roll"]),
				VWGLBFileReaderUtils::getJsonNumber(js_inJoint["yaw"]));
		}

		if (joints["output"].isArray() &&
			joints["output"].getArray().size()>0 &&
			joints["output"].getArray()[0].isObject()) {
			JsonBox::Object js_outJoint = joints["output"].getArray()[0].getObject();
			// TODO: Support for multiple types of joints

			_delegate->joint(VWGLBFileReaderDelegate::kJointTypeOutput,
				VWGLBFileReaderUtils::getJsonVector(js_outJoint["offset"]),
				VWGLBFileReaderUtils::getJsonNumber(js_outJoint["pitch"]),
				VWGLBFileReaderUtils::getJsonNumber(js_outJoint["roll"]),
				VWGLBFileReaderUtils::getJsonNumber(js_outJoint["yaw"]));
		}
	}
	else {
		_fileUtils.seekForward(-4);
	}
}

void VWGLBFileReader::readPolyList(int numberOfPlist) {
	VwglbUtils::BlockType block;

	_fileUtils.readBlock(block);
	if (block != VwglbUtils::kPolyList) throw BaseException("VWGLB: File format exception. Expecting poly list");
	for (int i = 0; i < numberOfPlist; ++i) {
		readSinglePolyList();
	}
}

void VWGLBFileReader::readSinglePolyList() {
	VwglbUtils::BlockType block;
	bool done = false;
	bool vertexFound = false;
	bool normalFound = false;
	bool tex0Found = false;
	bool tex1Found = false;
	bool tex2Found = false;
	bool indexFound = false;
	std::vector<float> vector;
	std::vector<unsigned int> ivector;
	std::string name;

	ptr<PolyList> plist = new PolyList();
	while (!done) {
		vector.clear();
		_fileUtils.readBlock(block);
		switch (block) {
		case VwglbUtils::kPlistName:
			_fileUtils.readString(name);
			plist->setName(name);
			break;
		case VwglbUtils::kMatName:
			_fileUtils.readString(name);
			plist->setMaterialName(name);
			break;
		case VwglbUtils::kVertexArray:
			if (vertexFound) throw BaseException("VWGLB: File format exception. duplicate vertex array");
			vertexFound = true;
			_fileUtils.readArray(vector);
			plist->addVertexVector(&vector[0], static_cast<int>(vector.size()));
			break;
		case VwglbUtils::kNormalArray:
			if (normalFound) throw BaseException("VWGLB: File format exception. duplicate normal array");
			normalFound = true;
			_fileUtils.readArray(vector);
			plist->addNormalVector(&vector[0], static_cast<int>(vector.size()));
			break;
		case VwglbUtils::kTexCoord0Array:
			if (tex0Found) throw BaseException("VWGLB: File format exception. duplicate texcoord0 array");
			tex0Found = true;
			_fileUtils.readArray(vector);
			plist->addTexCoord0Vector(&vector[0], static_cast<int>(vector.size()));
			break;
		case VwglbUtils::kTexCoord1Array:
			if (tex1Found) throw BaseException("VWGLB: File format exception. duplicate texcoord1 array");
			tex1Found = true;
			_fileUtils.readArray(vector);
			plist->addTexCoord1Vector(&vector[0], static_cast<int>(vector.size()));
			break;
		case VwglbUtils::kTexCoord2Array:
			if (tex2Found) throw BaseException("VWGLB: File format exception. duplicate texcoord2 array");
			tex2Found = true;
			_fileUtils.readArray(vector);
			plist->addTexCoord0Vector(&vector[1], static_cast<int>(vector.size()));
			break;
		case VwglbUtils::kIndexArray:
			if (indexFound) throw BaseException("VWGLB: File format exception. duplicate index array");
			indexFound = true;
			_fileUtils.readArray(ivector);
			plist->addIndexVector(&ivector[0], static_cast<int>(ivector.size()));
			break;
		case VwglbUtils::kPolyList:
		case VwglbUtils::kEnd:
			done = true;
			break;
		default:
			throw BaseException("VWGLB: File format exception. Unexpected poly list member found");
		}
	}

	plist->buildPolyList();
	_plistMap[plist->getMaterialName()] = plist.getPtr();
	_delegate->polyList(plist.getPtr());
}

void VWGLBFileReader::loadProjectors(const std::string & materialsString) {
	std::string currentDir = Loader::get()->getCurrentDir();

	JsonBox::Value js_materials;
	js_materials.loadFromString(materialsString);

	if (js_materials.isArray()) {
		JsonBox::Array matArray = js_materials.getArray();
		JsonBox::Array::iterator it;
		for (it = matArray.begin(); it != matArray.end(); ++it) {
			JsonBox::Value name = (*it)["name"];
			if (name.isString()) {
				JsonBox::Value material = *it;
				ptr<Texture> shadowTexture;
				Matrix4 shadowProjection;
				Matrix4 shadowTransform;
				float shadowAttenuation;
				bool visible;
				std::string groupName;
				PolyList * plist = _plistMap[name.getString()].getPtr();

				VWGLBFileReaderUtils::getPlistSettings(material, currentDir,
					shadowTexture, shadowProjection, shadowTransform,
					shadowAttenuation, groupName, visible);

				if (shadowTexture.valid()) {
					_delegate->projector(plist, shadowTexture.getPtr(), shadowProjection, shadowTransform, shadowAttenuation);
				}
				plist->setGroupName(groupName);
				plist->setVisible(visible);
			}
		}
	}
}

}
