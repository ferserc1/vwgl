
#include <vwgl/normalmapdeferredmaterial.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

NormalMapDeferredMaterial::NormalMapDeferredMaterial() :DeferredMaterial() {
	initShader();
}

NormalMapDeferredMaterial::~NormalMapDeferredMaterial() {
}

void NormalMapDeferredMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_normalmap.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_normalmap.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_normalmap.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_normalmap.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: normal map shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("def_normalmap");
	
	loadVertexAttrib("aVertexPos");
	loadNormalAttrib("aNormalPos");
	loadTexCoord0Attrib("aNormalMapPos");
	loadTexCoord1Attrib("aLightNormalMapPos");
	loadTangentArray("aTangentPos");
	
	getShader()->initUniformLocation("uMMatrix");
	getShader()->initUniformLocation("uVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uNMatrix");
	getShader()->initUniformLocation("uNormalMap");
	getShader()->initUniformLocation("uLightNormalMap");
	getShader()->initUniformLocation("uUseNormalMap");
	getShader()->initUniformLocation("uUseLightNormalMap");
	getShader()->initUniformLocation("uNormalMapOffset");
	getShader()->initUniformLocation("uNormalMapScale");

	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
	getShader()->initUniformLocation("uAlphaCutoff");
	
	
	getShader()->initUniformLocation("uLightEmission");
	getShader()->initUniformLocation("uLightEmissionMask");
	getShader()->initUniformLocation("uLightEmissionMaskChannel");
	getShader()->initUniformLocation("uLightEmissionMaskInvert");
}

void NormalMapDeferredMaterial::setupUniforms() {
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uMMatrix",modelMatrix());
		getShader()->setUniform("uVMatrix",viewMatrix());
		getShader()->setUniform("uPMatrix",projectionMatrix());
		getShader()->setUniform("uNMatrix",normalMatrix());
		getShader()->setUniform("uNormalMap",mat->getNormalMap(),Texture::kTexture0);
		getShader()->setUniform("uUseNormalMap",mat->getNormalMap()!=NULL);
		getShader()->setUniform("uNormalMapOffset",mat->getNormalMapOffset());
		getShader()->setUniform("uNormalMapScale",mat->getNormalMapScale());
		getShader()->setUniform("uLightNormalMap",mat->getLightNormalMap(),Texture::kTexture1);
		getShader()->setUniform("uUseLightNormalMap", mat->getLightNormalMap()!=NULL);
		
		Texture * tex = mat->getTexture() ? mat->getTexture() : TextureManager::get()->whiteTexture();
		getShader()->setUniform("uTexture",tex,Texture::kTexture2);
		getShader()->setUniform("uTextureOffset", mat->getTextureOffset());
		getShader()->setUniform("uTextureScale", mat->getTextureScale());
		getShader()->setUniform("uAlphaCutoff", mat->getAlphaCutoff());

		getShader()->setUniform("uLightEmission", mat->getLightEmission());
		Texture * lightEmissionMask = mat->getLightEmissionMask() ? mat->getLightEmissionMask() : TextureManager::get()->blackTexture();
		getShader()->setUniform("uLightEmissionMask", lightEmissionMask, Texture::kTexture3);
		getShader()->setUniform("uLightEmissionMaskChannel", mat->getLightEmissionMaskChannel());
		getShader()->setUniform("uLightEmissionMaskInvert", mat->getInvertLightEmissionMask() ? 1.0f:0.0f);
	}
}


}
