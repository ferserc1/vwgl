
#include <vwgl/update_visitor.hpp>
#include <vwgl/group.hpp>

namespace vwgl {

UpdateVisitor::UpdateVisitor() {
	
}

UpdateVisitor::~UpdateVisitor() {
	
}

void UpdateVisitor::visit(vwgl::Node *node) {
	if (node->isEnabled()) {
		Group * group = dynamic_cast<Group*>(node);
	
		node->update();
		if (group) {
			NodeList::iterator it;
			
			for (it=group->getNodeList().begin(); it!=group->getNodeList().end(); ++it) {
				visit((*it).getPtr());
			}
		}
	}
}

}