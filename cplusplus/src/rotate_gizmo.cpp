
#include <vwgl/rotate_gizmo.hpp>
#include <vwgl/system.hpp>

#include <vwgl/physics/ray.hpp>
#include <vwgl/physics/plane.hpp>
#include <vwgl/physics/intersection.hpp>

namespace vwgl {
	

RotateGizmo::RotateGizmo() :Gizmo(System::get()->getResourcesPath() + "rotate_gizmo.vwglb") {
	loadPickIds();
	_resetRotation = true;
}

RotateGizmo::RotateGizmo(const std::string & model) :Gizmo(model) {
	loadPickIds();
	_resetRotation = true;
}

RotateGizmo::~RotateGizmo() {
	
}

void RotateGizmo::beginMouseEvents(const Position2Di & pos, vwgl::Camera * camera, const Viewport & vp, const Vector3 & gizmoPos, TransformNode * trx) {
	_transformNode = trx;
	_previousPosition = pos;
}

void RotateGizmo::mouseEvent(const vwgl::Position2Di & pos) {
//	TRSTransformNode * trs = dynamic_cast<TRSTransformNode*>(_transformNode);
	TRSTransformStrategy * trs = _transformNode->getTransformStrategy<TRSTransformStrategy>();
	float offsetMultiplier = 0.01f;
	float offsetValue = ((_previousPosition.x() - pos.x()) + (_previousPosition.y() - pos.y())) * offsetMultiplier;
	if (trs) {
		switch (_mode) {
			case kGizmoAxisX:
				trs->rotateX(trs->getRotateX() + offsetValue);
				break;
			case kGizmoAxisY:
				trs->rotateY(trs->getRotateY() + offsetValue);
				break;
			case kGizmoAxisZ:
				trs->rotateZ(trs->getRotateZ() + offsetValue);
				break;
			default:
				break;
		}
	}
	else {
		switch (_mode) {
			case kGizmoAxisX:
				_transformNode->getTransform().rotate(offsetValue, 1.0f, 0.0f, 0.0f);
				break;
			case kGizmoAxisY:
				_transformNode->getTransform().rotate(offsetValue, 0.0f, 1.0f, 0.0f);
				break;
			case kGizmoAxisZ:
				_transformNode->getTransform().rotate(offsetValue, 0.0f, 0.0f, 1.0f);
				break;
			default:
				break;
		}
	}
	_previousPosition = pos;
}

void RotateGizmo::endMouseEvents() {
	
}

void RotateGizmo::loadPickIds() {
	Solid * axisX = _drawable->getSolid("Material-xAxis");
	Solid * axisY = _drawable->getSolid("Material-yAxis");
	Solid * axisZ = _drawable->getSolid("Material-zAxis");
	
	_xAxisId = axisX->getPickId();
	_yAxisId = axisY->getPickId();
	_zAxisId = axisZ->getPickId();
}

}
