
#include <vwgl/shader.hpp>
#include <vwgl/opengl.hpp>
#include <vwgl/glerror.hpp>
#include <vwgl/system.hpp>
#include <vwgl/graphics.hpp>

#include <iostream>
#include <fstream>
#include <sstream>

#pragma warning ( disable : 4835 )

namespace vwgl {

std::string Shader::s_shaderPath = "";

void Shader::initDefaultPath() {
	if (s_shaderPath=="") {
		s_shaderPath = System::get()->getDefaultShaderPath();
		if (s_shaderPath.back()!='/') s_shaderPath += '/';
	}
}

Shader::Shader() :_shaderProgram(-1), _linked(false), _isTessellationShader(false) {
	Shader::initDefaultPath();
	_shaderProgram = glCreateProgram();
}

Shader::~Shader() {
	destroy();
}

bool Shader::loadAndAttachShader(ShaderType shaderType, const std::string & path, bool isAbsolutePath) {
	if (!_linked) {
		std::ifstream in;
		if (isAbsolutePath) {
			in.open(path);
		}
		else {
			in.open(s_shaderPath + path);
		}
		if (in.is_open()) {
			std::string shaderCode(static_cast<std::stringstream const&>(std::stringstream() << in.rdbuf()).str());
			in.close();
			return attachShader(shaderType, shaderCode, path);
		}
		else {
			std::cerr << "Could not open shader file. No such file or directory " << path << std::endl;
			return false;
		}
	}
	return true;
}

bool Shader::attachShader(ShaderType shaderType, const std::string & shaderSource, const std::string & debugContext) {
	_compileError = "";
	_isTessellationShader = _isTessellationShader || shaderType==kTypeTessControl || shaderType==kTypeTessEvaluation || shaderType==kTypeGeometry;
	if (!_linked) {
		if (_shaderProgram==-1) {
			_compileError = "ERROR - " + debugContext + ": Could not attach shader: shaderProgram is null";
			std::cerr << _compileError << std::endl;
			return false;
		}
		GLint shader = glCreateShader(shaderType);
		const char * source = shaderSource.c_str();
		glShaderSource(shader, 1, &source, NULL);
		glCompileShader(shader);
		
		if (!GlError::getCompileShaderStatus(shader)) {
			std::string error = GlError::getCompileErrorString(shader);
			_compileError = error;
			std::cerr << "ERROR - " << debugContext << ": " << error << std::endl;
			return false;
		}
		
		GlError::clearError();
		if (!glIsProgram(_shaderProgram)) {
			std::cout << "Error: shader program is not a program object" << std::endl;
			return false;
		}
		if (!glIsShader(shader)) {
			std::cout << "Error: shader is not a valid shader" << std::endl;
			return false;
		}
		GLuint shaders[100];
		GLsizei count;
		GLsizei newCount;
		glGetAttachedShaders(_shaderProgram, 100, &count, shaders);
		glAttachShader(_shaderProgram, shader);
		GlError::clearError();
		glGetAttachedShaders(_shaderProgram, 100, &newCount, shaders);
		if (newCount!=count+1) {
			std::string error = GlError::getCompileErrorString(shader);
			std::string link = GlError::getLinkErrorString(_shaderProgram);
			_compileError = GlError::getErrorString();
			std::cout << "Error - " << debugContext << ": Attach shader error: " << _compileError << std::endl;
			GlError::clearError();
			return false;
		}
		
		glDeleteShader(shader);
	}
	return true;
}

bool Shader::link(const std::string & debugContext) {
	_linkError = "";
	if (!_linked) {
		if (_shaderProgram==-1) {
			_linkError = "ERROR - " + debugContext + ": Could not link shader: shaderProgram is null";
			std::cerr << _linkError << std::endl;
			return false;
		}
		glLinkProgram(_shaderProgram);
		if (GlError::getLinkProgramStatus(_shaderProgram)==GL_FALSE) {
			_linkError = GlError::getLinkErrorString(_shaderProgram);
			std::cerr << "ERROR - " << debugContext << ": Shader link error: " << _linkError << std::endl;
			return false;
		}
		_linked = true;
	}
	return true;
}
	
void Shader::setOutputParameterName(ParameterType type, const std::string & name, int dataLocation) {
	if (Graphics::get()->getApi()==Graphics::kApiOpenGLAdvanced) {
		glBindFragDataLocation(_shaderProgram, dataLocation, name.c_str());
	}
}

void Shader::bind() {
	glUseProgram(_shaderProgram);
}
	
void Shader::unbind() {
	glUseProgram(0);
}

void Shader::bindBuffer(BindBufferTarget target, GLint vertexBuffer) {
	glBindBuffer(target, vertexBuffer);
}

void Shader::enableVertexAttribArray(GLint attribLocation) {
	glEnableVertexAttribArray(attribLocation);
}

void Shader::vertexAttribPointer(GLint attribPointer, int size, PointerType type, bool normalize, int stride, const void * pointer) {
	glVertexAttribPointer(attribPointer, size, type,
						normalize ? GL_TRUE:GL_FALSE, stride, static_cast<const GLvoid*>(pointer));
}

GLint Shader::getAttribLocation(const std::string & name) const {
	return glGetAttribLocation(_shaderProgram, name.c_str());
}

void Shader::initUniformLocation(const std::string &name) {
	_uniformLocations[name] = glGetUniformLocation(_shaderProgram, name.c_str());
}

void Shader::destroy() {
	if (_shaderProgram!=-1) {
		glDeleteProgram(_shaderProgram);
		_shaderProgram = -1;
		_uniformLocations.clear();
	}
}

void Shader::setUniform(const std::string & name, float v0) {
	glUniform1f(getUniformLocation(name), v0);
}

void Shader::setUniform(const std::string & name, float v0, float v1) {
	glUniform2f(getUniformLocation(name), v0, v1);
}

void Shader::setUniform(const std::string & name, float v0, float v1, float v2) {
	glUniform3f(getUniformLocation(name), v0, v1, v2);
}

void Shader::setUniform(const std::string & name, float v0, float v1, float v2, float v3) {
	glUniform4f(getUniformLocation(name), v0, v1, v2, v3);
}

void Shader::setUniform(const std::string & name, int v0) {
	glUniform1i(getUniformLocation(name), v0);
}

void Shader::setUniform(const std::string & name, int v0, int v1) {
	glUniform2i(getUniformLocation(name), v0, v1);
}

void Shader::setUniform(const std::string & name, int v0, int v1, int v2) {
	glUniform3i(getUniformLocation(name), v0, v1, v2);
}

void Shader::setUniform(const std::string & name, int v0, int v1, int v2, int v3) {
	glUniform4i(getUniformLocation(name), v0, v1, v2, v3);
}

void Shader::setUniform(const std::string & name, unsigned int v0) {
	glUniform1i(getUniformLocation(name), v0);
}

void Shader::setUniform(const std::string & name, unsigned int v0, unsigned int v1) {
	glUniform2i(getUniformLocation(name), v0, v1);
}

void Shader::setUniform(const std::string & name, unsigned int v0, unsigned int v1, unsigned int v2) {
	glUniform3i(getUniformLocation(name), v0, v1, v2);
}

void Shader::setUniform(const std::string & name, unsigned int v0, unsigned int v1, unsigned int v2, unsigned int v3) {
	glUniform4i(getUniformLocation(name), v0, v1, v2, v3);
}

void Shader::setUniform(const std::string & name, bool v) {
	glUniform1i(getUniformLocation(name), v);
}

void Shader::setUniform1v(const std::string & name, int count, const float *value) {
	glUniform1fv(getUniformLocation(name),count,value);
}

void Shader::setUniform2v(const std::string & name, int count, const float *value) {
	glUniform2fv(getUniformLocation(name),count,value);
}

void Shader::setUniform3v(const std::string & name, int count, const float *value) {
	glUniform3fv(getUniformLocation(name),count,value);
}

void Shader::setUniform4v(const std::string & name, int count, const float *value) {
	glUniform4fv(getUniformLocation(name),count,value);
}

void Shader::setUniform1v(const std::string & name, int count, const int *value) {
	glUniform1iv(getUniformLocation(name),count,value);
}

void Shader::setUniform2v(const std::string & name, int count, const int *value) {
	glUniform2iv(getUniformLocation(name),count,value);
}

void Shader::setUniform3v(const std::string & name, int count, const int *value) {
	glUniform3iv(getUniformLocation(name),count,value);
}

void Shader::setUniform4v(const std::string & name, int count, const int *value) {
	glUniform4iv(getUniformLocation(name),count,value);
}

void Shader::setUniform1v(const std::string & name, int count, const unsigned int *value) {
	glUniform1iv(getUniformLocation(name),count,reinterpret_cast<const int*>(value));
}

void Shader::setUniform2v(const std::string & name, int count, const unsigned int *value) {
	glUniform2iv(getUniformLocation(name),count,reinterpret_cast<const int*>(value));
}

void Shader::setUniform3v(const std::string & name, int count, const unsigned int *value) {
	glUniform3iv(getUniformLocation(name),count,reinterpret_cast<const int*>(value));
}

void Shader::setUniform4v(const std::string & name, int count, const unsigned int *value) {
	glUniform4iv(getUniformLocation(name),count,reinterpret_cast<const int*>(value));
}

void Shader::setUniformMatrix2(const std::string & name,int count, bool transpose, const float *value) {
	glUniformMatrix2fv(getUniformLocation(name), count, transpose, value);
}

void Shader::setUniformMatrix3(const std::string & name,int count, bool transpose, const float *value) {
	glUniformMatrix3fv(getUniformLocation(name), count, transpose, value);
}

void Shader::setUniformMatrix4(const std::string & name,int count, bool transpose, const float *value) {
	glUniformMatrix4fv(getUniformLocation(name), count, transpose, value);
}

void Shader::setUniform(const std::string & name, Vector2 & v) {
	setUniform2v(name, 1, v.raw());
}

void Shader::setUniform(const std::string & name, Vector3 & v) {
	setUniform3v(name, 1, v.raw());
}

void Shader::setUniform(const std::string & name, Vector4 & v) {
	setUniform4v(name, 1, v.raw());
}

void Shader::setUniform(const std::string & name, Quaternion & q) {
	setUniform4v(name, 1, q.raw());
}

void Shader::setUniform(const std::string & name, Matrix3 & m, bool transpose) {
	setUniformMatrix3(name, 1, transpose, m.raw());
}

void Shader::setUniform(const std::string & name, Matrix4 & m, bool transpose) {
	setUniformMatrix4(name, 1, transpose, m.raw());
}

void Shader::setUniform(const std::string & name, Texture * tex, Texture::TextureUnit unit) {
	if (tex) {
		Texture::setActiveTexture(unit);
		tex->bindTexture(Texture::kTargetTexture2D);
		setUniform(name, getTextureUnit(unit));
	}
}

void Shader::setUniform(const std::string & name, CubeMap * tex, Texture::TextureUnit unit) {
	if (tex) {
		Texture::setActiveTexture(unit);
		tex->bindTexture(Texture::kTargetTextureCubeMap);
		setUniform(name, getTextureUnit(unit));
	}
}
	
void Shader::setUniform(const std::string & name, const Vector2 & v) {
	setUniform2v(name, 1, v.raw());
}

void Shader::setUniform(const std::string & name, const Vector3 & v) {
	setUniform3v(name, 1, v.raw());
}

void Shader::setUniform(const std::string & name, const Vector4 & v) {
	setUniform4v(name, 1, v.raw());
}

void Shader::setUniform(const std::string & name, const Quaternion & q) {
	setUniform4v(name, 1, q.raw());
}

void Shader::setUniform(const std::string & name, const Matrix3 & m, bool transpose) {
	setUniformMatrix3(name, 1, transpose, m.raw());
}

void Shader::setUniform(const std::string & name, const Matrix4 & m, bool transpose) {
	setUniformMatrix4(name, 1, transpose, m.raw());
}


int Shader::getTextureUnit(Texture::TextureUnit unit) {
	switch (unit) {
		case Texture::kTexture0:
			return 0;
		case Texture::kTexture1:
			return 1;
		case Texture::kTexture2:
			return 2;
		case Texture::kTexture3:
			return 3;
		case Texture::kTexture4:
			return 4;
		case Texture::kTexture5:
			return 5;
		case Texture::kTexture6:
			return 6;
		case Texture::kTexture7:
			return 7;
		case Texture::kTexture8:
			return 8;
		case Texture::kTexture9:
			return 9;
		case Texture::kTexture10:
			return 10;
		case Texture::kTexture11:
			return 11;
		case Texture::kTexture12:
			return 12;
		case Texture::kTexture13:
			return 13;
		case Texture::kTexture14:
			return 14;
		case Texture::kTexture15:
			return 15;
		case Texture::kTexture16:
			return 16;
		case Texture::kTexture17:
			return 17;
		case Texture::kTexture18:
			return 18;
		case Texture::kTexture19:
			return 19;
		case Texture::kTexture20:
			return 20;
		case Texture::kTexture21:
			return 21;
		case Texture::kTexture22:
			return 22;
		case Texture::kTexture23:
			return 23;
		case Texture::kTexture24:
			return 24;
		case Texture::kTexture25:
			return 25;
		case Texture::kTexture26:
			return 26;
		case Texture::kTexture27:
			return 27;
		case Texture::kTexture28:
			return 28;
		case Texture::kTexture29:
			return 29;
		case Texture::kTexture30:
			return 30;
	}
	return 0;
}
	

ShaderLibrary * ShaderLibrary::s_shaderLibrary = nullptr;
ShaderLibrary * ShaderLibrary::get() {
	if (s_shaderLibrary==nullptr) {
		s_shaderLibrary = new ShaderLibrary();
	}
	return s_shaderLibrary;
}
	
void ShaderLibrary::finalize() {
	_shaderMap.clear();
}

ShaderLibrary::ShaderLibrary() {
	SingletonController::get()->registerSingleton(this);
}

ShaderLibrary::~ShaderLibrary() {
	SingletonController::get()->unregisterSingleton(this);
}

void ShaderLibrary::addShader(const std::string & name, Shader * shader) {
	_shaderMap[name] = shader;
}

Shader * ShaderLibrary::getShader(const std::string & name) {
	return _shaderMap[name].getPtr();
}

	
	
}
