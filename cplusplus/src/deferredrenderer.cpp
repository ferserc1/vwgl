
#include <vwgl/deferredrenderer.hpp>
#include <vwgl/texturedeferredmaterial.hpp>
#include <vwgl/normalmapdeferredmaterial.hpp>
#include <vwgl/selectiondeferredmaterial.hpp>
#include <vwgl/wpositiondeferredmaterial.hpp>
#include <vwgl/glerror.hpp>
#include <vwgl/gizmo_manager.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/system.hpp>
#include <vwgl/texture_manager.hpp>

#include <string>

namespace vwgl {

DeferredRenderer::DeferredRenderer() {
	_initVisitor = new InitVisitor();
	_mapSize = Size2Di();
	_clearColor = Color::black();
	_antiAliasing = 1;
	_renderPasses = kRenderPassDiffuse |
		kRenderPassNormal |
		kRenderPassSpecular |
		kRenderPassPosition |
		kRenderPassSelection |
		kRenderPassLighting |
		kRenderPassShadows |
		kRenderPassPostprocess;
}

DeferredRenderer::~DeferredRenderer() {
	destroy();
}

void DeferredRenderer::build(Group * sceneRoot, int width,int height) {
	_mapSize = Size2Di(width,height);
	destroy();
	_canvas = new HomogeneousRenderCanvas();
	_sceneRoot = sceneRoot;
	
	DiffuseRenderPassMaterial * diffuseMat = new DiffuseRenderPassMaterial();
	_diffuseRP = new RenderPass(sceneRoot,diffuseMat,new FramebufferObject(_mapSize.width(),_mapSize.height()));
	NormalMapDeferredMaterial * normalMat = new NormalMapDeferredMaterial();
	_normalRP = new RenderPass(sceneRoot,normalMat,new FramebufferObject(_mapSize.width(), _mapSize.height()));
	_normalRP->setClearColor(Color(0,0,0,0));
	SpecularRenderPassMaterial * specularMat = new SpecularRenderPassMaterial();
	_specularRP = new RenderPass(sceneRoot,specularMat,new FramebufferObject(_mapSize.width(), _mapSize.height()));
	_specularRP->setClearColor(Color(0,0,0,0));
	WPositionDeferredMaterial * positionMat = new WPositionDeferredMaterial();
	_positionRP = new RenderPass(sceneRoot,positionMat, new FramebufferObject(_mapSize.width(),_mapSize.height(),FramebufferObject::kTypeFloatTexture));
	ShadowRenderPassMaterial * shadowMat = new ShadowRenderPassMaterial();
	_shadowRP = new RenderPass(sceneRoot, shadowMat, new FramebufferObject(_mapSize.width(),_mapSize.height()));
	_filters.push_back(shadowMat);

	SelectionDeferredMaterial * selectionMat = new SelectionDeferredMaterial();
	_selectionRP = new RenderPass(sceneRoot, selectionMat, new FramebufferObject(_mapSize.width(),_mapSize.height()));
	
	_projectorMat = new ProjTextureDeferredMaterial();
	
	_lightingMaterial = new DeferredLighting();
	_lightingMaterial->setDiffuseMap(_diffuseRP->getFbo()->getTexture());
	_lightingMaterial->setNormalMap(_normalRP->getFbo()->getTexture());
	_lightingMaterial->setSpecularMap(_specularRP->getFbo()->getTexture());
	_lightingMaterial->setPositionMap(_positionRP->getFbo()->getTexture());
	_lightingMaterial->setShadowMap(_shadowRP->getFbo()->getTexture());
	_filters.push_back(_lightingMaterial.getPtr());
	
	_lightingCanvas = new HomogeneousRenderCanvas();
	_lightingCanvas->setMaterial(_lightingMaterial.getPtr());
	_lightingFbo = new FramebufferObject(_mapSize.width(), _mapSize.height(),FramebufferObject::kTypeFloatTexture);
	
	_ssaoMaterial = new SSAOMaterial();
	_ssaoMaterial->setPositionMap(_positionRP->getFbo()->getTexture());
	_ssaoMaterial->setNormalMap(_normalRP->getFbo()->getTexture());
	_ssaoMaterial->setViewportSize(Vector2(static_cast<float>(_viewport.width()),
										   static_cast<float>(_viewport.height())));
	std::string resources = System::get()->getDefaultShaderPath();
	_filters.push_back(_ssaoMaterial.getPtr());
	
	_ssaoCanvas = new HomogeneousRenderCanvas();
	_ssaoCanvas->setMaterial(_ssaoMaterial.getPtr());
	_ssaoFbo = new FramebufferObject(_mapSize.width(), _mapSize.height());
	
	_bloomMaterial = new BloomMapMaterial();
	_bloomMaterial->setLightingMap(_lightingFbo->getTexture());
	_bloomFbo = new FramebufferObject(_mapSize.width(), _mapSize.height());
	_bloomCanvas = new HomogeneousRenderCanvas();
	_bloomCanvas->setMaterial(_bloomMaterial.getPtr());
	_filters.push_back(_bloomMaterial.getPtr());
	
	_postprocessMat = new DeferredPostprocess();
	_postprocessMat->setDiffuseMap(_lightingFbo->getTexture());
	_postprocessMat->setPositionMap(_positionRP->getFbo()->getTexture());
	_postprocessMat->setSelectionMap(_selectionRP->getFbo()->getTexture());
	_postprocessMat->setSSAOMap(_ssaoFbo->getTexture());
	_postprocessMat->setBloomMap(_bloomFbo->getTexture());

	_filters.push_back(_postprocessMat.getPtr());

	
	_canvas->setMaterial(_postprocessMat.getPtr());
}

void DeferredRenderer::resize(const Size2Di & size) {
	_mapSize = Size2Di(size);
	
	_diffuseRP->getFbo()->resize(_mapSize.width()*_antiAliasing,_mapSize.height()*_antiAliasing);
	_normalRP->getFbo()->resize(_mapSize.width()*_antiAliasing,_mapSize.height()*_antiAliasing);
	_specularRP->getFbo()->resize(_mapSize.width()*_antiAliasing,_mapSize.height()*_antiAliasing);
	_positionRP->getFbo()->resize(_mapSize.width()*_antiAliasing,_mapSize.height()*_antiAliasing);
	_shadowRP->getFbo()->resize(_mapSize.width()*_antiAliasing,_mapSize.height()*_antiAliasing);
	_selectionRP->getFbo()->resize(_mapSize.width(), _mapSize.height());
	
	_lightingFbo->resize(_mapSize.width()*_antiAliasing,_mapSize.height()*_antiAliasing);
	
	_ssaoMaterial->setViewportSize(Vector2(static_cast<float>(_viewport.width()),
										   static_cast<float>(_viewport.height())));
	_ssaoFbo->resize(_mapSize.width(), _mapSize.height());
	_bloomFbo->resize(_mapSize.width(), _mapSize.height());
}

void DeferredRenderer::destroy() {
	if (!_canvas.valid()) return;
	_diffuseRP = nullptr;
	_normalRP = nullptr;
	_specularRP = nullptr;
	_positionRP = nullptr;
	_shadowRP = nullptr;
	_selectionRP = nullptr;
	
	_projectorMat = nullptr;
	
	_lightingMaterial = nullptr;
	_lightingCanvas = nullptr;
	_lightingFbo = nullptr;
	
	_ssaoMaterial = nullptr;
	_ssaoCanvas = nullptr;
	_ssaoFbo = nullptr;
	
	_bloomMaterial = nullptr;
	_bloomCanvas = nullptr;
	_bloomFbo = nullptr;
	
	_postprocessMat = nullptr;
	_canvas = nullptr;
}

void DeferredRenderer::draw(Camera * cam) {
	prepareFrame(cam);
	
	geometryPass();
	
	lightingPass();
	
	postprocessPass();
	
	presentCanvas(cam);
}

void DeferredRenderer::prepareFrame(Camera * cam) {
	_lightingMaterial->_shadowBlurIterations = getFilter<vwgl::ShadowRenderPassMaterial>()->getBlurIterations();
	_postprocessMat->setClearColor(_clearColor);
	if (!cam) cam = vwgl::CameraManager::get()->getMainCamera();
	RenderSettings::setCurrentRenderSettings(_postprocessMat->getRenderSettingsPtr());
	_postprocessMat->setSSAOBlur(_ssaoMaterial->getBlurIterations());
	_postprocessMat->setBloomAmount(_bloomMaterial->getBloomAmount());
	_initVisitor->visit(_sceneRoot.getPtr());

	LightManager::get()->prepareFrame();

	CameraManager::get()->applyTransform(cam);
}

void DeferredRenderer::geometryPass() {
	_clearColor.a(0.0);

	
	if (kRenderPassDiffuse & _renderPasses) {
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(1.0, 1.0);
		_diffuseRP->setClearColor(_clearColor);
		_diffuseRP->updateTexture(true);
		glDisable(GL_POLYGON_OFFSET_FILL);

		ProjectorManager::ProjectorList & proj = ProjectorManager::get()->getProjectorList();
		ProjectorManager::ProjectorList::iterator it;
		ptr<Material> diffuseMat = _diffuseRP->getMaterial();
		_diffuseRP->setMaterial(_projectorMat.getPtr());
		_diffuseRP->setClearBuffers(false);
		glDepthMask(GL_FALSE);
		for (it = proj.begin(); it != proj.end(); ++it) {
			if ((*it)->isVisible()) {
				glEnable(GL_BLEND);
				glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
				_projectorMat->setProjector(*it);
				_diffuseRP->updateTexture(true);
			}
		}
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
		_diffuseRP->setClearBuffers(true);
		_diffuseRP->setMaterial(diffuseMat.getPtr());
	}
	else {
		_diffuseRP->setClearColor(vwgl::Color::white());
		_diffuseRP->clear();
	}
	
	kRenderPassNormal & _renderPasses ? _normalRP->updateTexture():_normalRP->clear();
	kRenderPassSpecular & _renderPasses ? _specularRP->updateTexture():_specularRP->clear();
	kRenderPassPosition & _renderPasses ? _positionRP->updateTexture():_positionRP->clear();
	kRenderPassSelection & _renderPasses ? _selectionRP->updateTexture():_selectionRP->clear();
}

void DeferredRenderer::lightingPass() {
	if (kRenderPassLighting & _renderPasses) {
		_postprocessMat->setDiffuseMap(_lightingFbo->getTexture());
		LightManager::LightList & lightList = LightManager::get()->getLightList();
		LightManager::LightList::iterator it;
		_lightingCanvas->setClearBuffer(true);
		bool setBlend = false;

		_shadowRP->clear();
		LightManager::LightList enabledLights;
		for (it = lightList.begin(); it != lightList.end(); ++it) {
			if ((*it)->branchEnabled()) {
				enabledLights.push_back(*it);
			}
		}

		for (it = enabledLights.begin(); it != enabledLights.end(); ++it) {
			ShadowLight * shadowLight = dynamic_cast<ShadowLight*>((*it));
			ShadowRenderPassMaterial * shadowMat = dynamic_cast<ShadowRenderPassMaterial*>(_shadowRP->getMaterial());
			if (shadowLight && shadowLight->getCastShadows() && shadowMat && (kRenderPassShadows & _renderPasses) != 0) {
				glEnable(GL_DEPTH_TEST);
				glDisable(GL_BLEND);
				LightManager::get()->updateShadowMap(_sceneRoot.getPtr(), shadowLight);
				shadowMat->setLight(shadowLight);
				shadowMat->setShadowTexture(LightManager::get()->getShadowTexture());
				_shadowRP->setClearBuffers(true);
				_shadowRP->updateTexture();
				_lightingMaterial->setShadowMap(_shadowRP->getFbo()->getTexture());
			}
			else {
				_lightingMaterial->setShadowMap(TextureManager::get()->whiteTexture());
			}
			_lightingFbo->bind();

			if (setBlend) {
				glDisable(GL_DEPTH_TEST);
				glEnable(GL_BLEND);
				glBlendFunc(GL_ONE, GL_ONE);
			}
			_lightingMaterial->setLight(*it);
			_lightingMaterial->setEnabledLights(static_cast<int>(enabledLights.size()));
			_lightingCanvas->draw(_viewport.width()*_antiAliasing, _viewport.height()*_antiAliasing);
			_lightingCanvas->setClearBuffer(false);

			setBlend = true;
			_lightingFbo->unbind();
		}
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		if (!setBlend) {
			// The scene does not contain any light or there are all disabled
			_lightingFbo->bind();
			_lightingMaterial->setLight(static_cast<vwgl::Light*>(nullptr));
			_lightingCanvas->draw(_viewport.width()*_antiAliasing, _viewport.height()*_antiAliasing);
			_lightingCanvas->setClearBuffer(false);
			_lightingFbo->unbind();
		}
	}
	else {
		_postprocessMat->setDiffuseMap(_diffuseRP->getFbo()->getTexture());
	}
}

void DeferredRenderer::postprocessPass() {
	if (kRenderPassPostprocess & _renderPasses) {
		_ssaoFbo->bind();
		glClear(GL_DEPTH_BUFFER_BIT);
		_ssaoCanvas->setClearBuffer(true);
		_ssaoCanvas->draw(_viewport.width(), _viewport.height());
		_ssaoFbo->unbind();

		_bloomFbo->bind();
		glClear(GL_DEPTH_BUFFER_BIT);
		_bloomCanvas->setClearBuffer(true);
		_bloomCanvas->draw(_viewport.width(), _viewport.height());
		_bloomFbo->unbind();
	}
	else {
		_ssaoFbo->bind();
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_ssaoFbo->unbind();

		_bloomFbo->bind();
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_bloomFbo->unbind();
	}
	
}

void DeferredRenderer::presentCanvas(Camera * cam) {
	
	_canvas->draw(_viewport.width(), _viewport.height());
	glClear(GL_DEPTH_BUFFER_BIT);
	vwgl::GizmoManager::get()->drawGizmo(cam);
}
	
void DeferredRenderer::configureSceneRoot() {
	build(_sceneRoot.getPtr(), _mapSize.width(), _mapSize.height());
}


}
