
#include <vwgl/vwglb_plugin.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/scene/projector.hpp>
#include <vwgl/scene/joint.hpp>

namespace vwgl {

// Common: delegate
void VWGLBPluginDelegate::onError(const std::string & error) {
	std::cerr << error;
}

void VWGLBPluginDelegate::onWarning(const std::string & warning) {
	std::cerr << warning;
}

void VWGLBPluginDelegate::fileVersion(const FileVersion & version) {
	std::cout << "vwglb file version " << static_cast<int>(version.major)
		<< "." << static_cast<int>(version.minor)
		<< "." << static_cast<int>(version.revision) << std::endl;
}

void VWGLBPluginDelegate::polyList(PolyList * plist) {
	_drawable->addPolyList(plist, new GenericMaterial());
}

void VWGLBPluginDelegate::materials(const std::string & materialsString) {
	GenericMaterial::setMaterialsWithJson(_drawable.getPtr(), materialsString);
}

// Model reader
bool VWGLBModelReader::acceptFileType(const std::string & path) {
	std::string extension = System::get()->getExtension(path);
	toLower(extension);
	return extension=="vwglb";
}

vwgl::scene::Drawable * VWGLBModelReader::loadModel(const std::string & path) {
	_drawable = new vwgl::scene::Drawable();

	_fileReader.load(path);
	
	return _drawable.release();
}



// Prefab reader
vwgl::scene::Node * VWGLBPrefabReader::loadPrefab(const std::string & path) {
	_drawable = new vwgl::scene::Drawable();
	_prefab = new vwgl::scene::Node();
	_prefab->addComponent(_drawable.getPtr());

	_fileReader.load(path);

	return _prefab.release();
}
	
bool VWGLBPrefabReader::acceptFileType(const std::string & path) {
	std::string extension = System::get()->getExtension(path);
	toLower(extension);
	return extension=="vwglb";
}

void VWGLBPrefabReader::projector(Texture * projectorTexture, const Matrix4 & projection, const Matrix4 & transform, float attenuation) {
	scene::SceneObject * so = _drawable->sceneObject();
	if (so) {
		ptr<scene::Projector> projector = new scene::Projector();
		projector->setTexture(projectorTexture);
		projector->setProjection(projection);
		projector->setTransform(transform);
		projector->setAttenuation(attenuation);
		so->addComponent(projector.release());
	}
}

void VWGLBPrefabReader::projector(PolyList * plist, Texture * projectorTexture, const Matrix4 & projection, const Matrix4 & transform, float attenuation) {

	std::cerr << "Per-piece projectors are not supported in version 2." << std::endl;
	scene::SceneObject * so = _drawable->sceneObject();
	if (so && !so->getComponent<scene::Projector>()) {
		std::cout << "Adding projector as object projector" << std::endl;
		projector(projectorTexture,projection,transform,attenuation);
	}
}

void VWGLBPrefabReader::joint(JointType type, const Vector3 & offset, float pitch, float roll, float yaw) {
	physics::LinkJoint * joint = new physics::LinkJoint();
	joint->setOffset(offset);
	joint->setPitch(pitch);
	joint->setRoll(roll);
	joint->setYaw(yaw);
	if (type==JointType::kJointTypeInput) {
		_prefab->addComponent(new scene::InputChainJoint(joint));
	}
	else if (type==JointType::kJointTypeOutput) {
		_prefab->addComponent(new scene::OutputChainJoint(joint));
	}
}


// Writter plugin
bool VWGLBWritter::writeData(const std::string & path, scene::Node * node) {
	vwgl::scene::Drawable * model = node->getComponent<vwgl::scene::Drawable>();
	if (!model) return false;

	if (_fileUtils.open(path, VwglbUtils::kModeWrite)) {
		_dstPath = System::get()->getPath(path);
		writeHeader(node);

		model->eachElement([&](PolyList * plist, Material * mat, vwgl::Transform & transform) {
			writePolyList(plist);
		});

		_fileUtils.writeBlock(VwglbUtils::kEnd);
		_fileUtils.close();
		return true;
	}
	return false;
}

bool VWGLBWritter::writeData(const std::string & path, scene::Drawable * model) {
	if (_fileUtils.open(path, VwglbUtils::kModeWrite)) {
		_dstPath = System::get()->getPath(path);
		writeHeader(model);

		model->eachElement([&](PolyList * plist, Material * mat, vwgl::Transform & transform) {
			writePolyList(plist);
		});

		_fileUtils.writeBlock(VwglbUtils::kEnd);
		_fileUtils.close();
		return true;
	}
	return false;
}

void VWGLBWritter::writeHeader(scene::Drawable * drawable) {
	_fileUtils.setBigEndian();

	//// Write header
	// File endian 0=big endian, 1=little endian
	_fileUtils.writeByte(0);

	// Version (major, minor, rev)
	_fileUtils.writeByte(1);
	_fileUtils.writeByte(1);
	_fileUtils.writeByte(0);

	// File type
	_fileUtils.writeBlock(VwglbUtils::kHeader);

	// Number of poly list
	_fileUtils.writeInteger(static_cast<int>(drawable->getPolyListVector().size()));

	// Materials (as json string)
	ensurePolyListNames(drawable);	// Ensure that all poly list have name and material name
	writeMaterial(drawable);
}

void VWGLBWritter::writeHeader(scene::Node * node) {
	vwgl::scene::Drawable * drawable = node->getComponent<vwgl::scene::Drawable>();
	_fileUtils.setBigEndian();

	//// Write header
	// File endian 0=big endian, 1=little endian
	_fileUtils.writeByte(0);

	// Version (major, minor, rev)
	_fileUtils.writeByte(1);
	_fileUtils.writeByte(1);
	_fileUtils.writeByte(0);

	// File type
	_fileUtils.writeBlock(VwglbUtils::kHeader);

	// Number of poly list
	_fileUtils.writeInteger(static_cast<int>(drawable->getPolyListVector().size()));

	// Materials (as json string)
	ensurePolyListNames(drawable);	// Ensure that all poly list have name and material name
	writeMaterial(drawable, node);
	writeShadow(node);
	writeJoints(node);
}

void VWGLBWritter::ensurePolyListNames(scene::Drawable * drawable) {
	PolyListVector plist = drawable->getPolyListVector();
	PolyListVector::iterator it;
	std::unordered_map<std::string, std::string> plistNames;
	std::unordered_map<std::string, std::string> materialNames;

	int plistIndex = 0;
	int matIndex = 0;
	for (it = plist.begin(); it != plist.end(); ++it) {
		PolyList * p = (*it).getPtr();
		std::string plistname = p->getName();
		std::string matName = p->getMaterialName();
		std::stringstream newplistname;
		std::stringstream newmatname;
		if (plistname == "" || plistNames.find(plistname) != plistNames.end()) {
			newplistname << "polyList_" << plistIndex;
			plistname = newplistname.str();
			p->setName(plistname);
			++plistIndex;
		}
		if (matName == "" || materialNames.find(matName) != materialNames.end()) {
			newmatname << "material_" << matIndex;
			matName = newmatname.str();
			p->setMaterialName(matName);
			++matIndex;
		}
		plistNames[plistname] = plistname;
		materialNames[matName] = matName;
	}
}

void VWGLBWritter::writeMaterial(scene::Drawable * drawable, scene::Node * node) {
	PolyListVector plistVector = drawable->getPolyListVector();

	PolyListVector::iterator it;
	std::stringstream materials;
	materials << "[";

	std::string separator = "";
	for (it = plistVector.begin(); it != plistVector.end(); ++it) {
		PolyList * p = (*it).getPtr();
		GenericMaterial * m = dynamic_cast<GenericMaterial*>(drawable->getMaterial(p));
		if (m) {
			materials << separator << "{";
			materials << "\"name\":\"" << p->getMaterialName() << "\",";
			materials << "\"class\":\"GenericMaterial\",";
			materials << "\"diffuseR\":" << m->getDiffuse().r() << ",";
			materials << "\"diffuseG\":" << m->getDiffuse().g() << ",";
			materials << "\"diffuseB\":" << m->getDiffuse().b() << ",";
			materials << "\"diffuseA\":" << m->getDiffuse().a() << ",";

			materials << "\"specularR\":" << m->getSpecular().r() << ",";
			materials << "\"specularG\":" << m->getSpecular().g() << ",";
			materials << "\"specularB\":" << m->getSpecular().b() << ",";
			materials << "\"specularA\":" << m->getSpecular().a() << ",";

			materials << "\"shininess\":" << m->getShininess() << ",";
			materials << "\"refractionAmount\":" << m->getRefractionAmount() << ",";
			materials << "\"reflectionAmount\":" << m->getReflectionAmount() << ",";
			materials << "\"lightEmission\":" << m->getLightEmission() << ",";
			if (m->getTexture()) {
				std::string textureFile = System::get()->getLastPathComponent(m->getTexture()->getFileName());
				materials << "\"texture\":\"" << textureFile << "\",";
				saveTextureFile(m->getTexture()->getFileName());
			}
			if (m->getLightMap()) {
				std::string textureFile = System::get()->getLastPathComponent(m->getLightMap()->getFileName());
				materials << "\"lightmap\":\"" << textureFile << "\",";
				saveTextureFile(m->getLightMap()->getFileName());
			}
			if (m->getNormalMap()) {
				std::string textureFile = System::get()->getLastPathComponent(m->getNormalMap()->getFileName());
				materials << "\"normalMap\":\"" << textureFile << "\",";
				saveTextureFile(m->getNormalMap()->getFileName());
			}

			materials << "\"textureOffsetX\":" << m->getTextureOffset().x() << ",";
			materials << "\"textureOffsetY\":" << m->getTextureOffset().y() << ",";
			materials << "\"textureScaleX\":" << m->getTextureScale().x() << ",";
			materials << "\"textureScaleY\":" << m->getTextureScale().y() << ",";

			materials << "\"lightmapOffsetX\":" << m->getLightmapOffset().x() << ",";
			materials << "\"lightmapOffsetY\":" << m->getLightmapOffset().y() << ",";
			materials << "\"lightmapScaleX\":" << m->getLightmapScale().x() << ",";
			materials << "\"lightmapScaleY\":" << m->getLightmapScale().y() << ",";

			materials << "\"normalMapOffsetX\":" << m->getNormalMapOffset().x() << ",";
			materials << "\"normalMapOffsetY\":" << m->getNormalMapOffset().y() << ",";
			materials << "\"normalMapScaleX\":" << m->getNormalMapScale().x() << ",";
			materials << "\"normalMapScaleY\":" << m->getNormalMapScale().y() << ",";


			materials << "\"castShadows\":" << (m->getCastShadows() ? "true" : "false") << ",";
			materials << "\"receiveShadows\":" << (m->getReceiveShadows() ? "true" : "false") << ",";
			materials << "\"receiveProjections\":" << (m->getReceiveProjections() ? "true" : "false") << ",";
			materials << "\"alphaCutoff\":" << m->getAlphaCutoff() << ",";

			if (m->getShininessMask()) {
				std::string textureFile = System::get()->getLastPathComponent(m->getShininessMask()->getFileName());
				materials << "\"shininessMask\":\"" << textureFile << "\",";
				saveTextureFile(m->getShininessMask()->getFileName());
			}
			materials << "\"shininessMaskChannel\":" << m->getShininessMaskChannel() << ",";
			materials << "\"invertShininessMask\":" << (m->getInvertShininessMask() ? "true" : "false") << ",";

			if (m->getLightEmissionMask()) {
				std::string textureFile = System::get()->getLastPathComponent(m->getLightEmissionMask()->getFileName());
				materials << "\"lightEmissionMask\":\"" << textureFile << "\",";
				saveTextureFile(m->getLightEmissionMask()->getFileName());
			}
			materials << "\"lightEmissionMaskChannel\":" << m->getLightEmissionMaskChannel() << ",";
			materials << "\"invertLightEmissionMask\":" << (m->getInvertLightEmissionMask() ? "true" : "false") << ",";

			materials << "\"cullFace\":" << (m->getCullFace() ? "true" : "false");

			// Per-piece shadow projectors are not supported in API 2.0
			// Shadow projector
		/*	if (node) {	// and node->getComponent<shadowprojector etc...
//			if (s->getShadowProjector() &&
	//			s->getShadowProjector()->getTexture() &&
		//		s->getShadowProjector()->getTexture()->getFileName() != "") {
				materials << ",";
				Projector * proj = s->getShadowProjector();
				Texture * tex = proj->getTexture();
				std::string textureFile = System::get()->getLastPathComponent(tex->getFileName());
				materials << "\"shadowTexture\":\"" << textureFile << "\",";
				saveTextureFile(tex->getFileName());

				const float * projection = proj->getProjection().raw();
				const float * transform = proj->getRawTransform().raw();
				materials << "\"shadowAttenuation\":" << proj->getAttenuation() << ",";
				materials << "\"shadowProjection\":\"";
				for (int i = 0; i<16; ++i) {
					materials << projection[i];
					if (i<15) materials << " ";
				}
				materials << "\",";

				materials << "\"shadowTransform\":\"";
				for (int i = 0; i<16; ++i) {
					materials << transform[i];
					if (i<15) materials << " ";
				}
				materials << "\"";
			}
			*/

			// Visibility and group name
			materials << ",";
			materials << "\"visible\":" << p->isVisible() << ",";
			materials << "\"groupName\":\"" << p->getGroupName() << "\"";

			materials << "}";
			separator = ",";
		}
	}

	materials << "]";
	_fileUtils.writeBlock(VwglbUtils::kMaterials);
	_fileUtils.writeString(materials.str());
}

void VWGLBWritter::writeShadow(scene::Node * node) {
	scene::Projector * proj = node->getComponent<scene::Projector>();
	Texture * tex;
	if (proj && (tex=proj->getTexture())) {
		_fileUtils.writeBlock(vwgl::VwglbUtils::kShadowProjector);
		std::string file = System::get()->getLastPathComponent(tex->getFileName());
		const float * projection = proj->getProjection().raw();
		const float * transform = proj->getRawTransform().raw();
		_fileUtils.writeString(file);
		_fileUtils.writeFloat(proj->getAttenuation());
		for (int i = 0; i<16; ++i) {
			_fileUtils.writeFloat(projection[i]);
		}
		for (int i = 0; i<16; ++i) {
			_fileUtils.writeFloat(transform[i]);
		}
		saveTextureFile(tex->getFileName());
	}
}

void VWGLBWritter::writeJoints(scene::Node * node) {
	// TODO: Support for multiple types of joints, and multiple output joints
	scene::InputChainJoint * inJoint = node->getComponent<scene::InputChainJoint>();
	scene::OutputChainJoint * outJoint = node->getComponent<scene::OutputChainJoint>();
	
	if (inJoint || outJoint) {
		_fileUtils.writeBlock(vwgl::VwglbUtils::kJoint);
		std::stringstream joints;
		joints << "{";
		
		// In joint
		if (inJoint) {
			Vector3 offset = inJoint->getJoint()->offset();
			float pitch = inJoint->getJoint()->getPitch();
			float roll = inJoint->getJoint()->getRoll();
			float yaw = inJoint->getJoint()->getYaw();
			joints << "\"input\":{";
			joints << "\"type\":\"LinkJoint\",";
			joints << "\"offset\":[" << offset.x() << "," << offset.y() << "," << offset.z() << "],";
			joints << "\"pitch\":" << pitch << ",\"roll\":" << roll << ",\"yaw\":" << yaw;
			joints << "}";
			if (outJoint) joints << ",";
		}
		
		// Out joint
		if (outJoint) {
			Vector3 offset = outJoint->getJoint()->offset();
			float pitch = outJoint->getJoint()->getPitch();
			float roll = outJoint->getJoint()->getRoll();
			float yaw = outJoint->getJoint()->getYaw();
			joints << "\"output\":[{";
			joints << "\"type\":\"LinkJoint\",";
			joints << "\"offset\":[" << offset.x() << "," << offset.y() << "," << offset.z() << "],";
			joints << "\"pitch\":" << pitch << ",\"roll\":" << roll << ",\"yaw\":" << yaw;
			joints << "}]";
		}
		
		joints << "}";
		_fileUtils.writeString(joints.str());
	}
}

void VWGLBWritter::writePolyList(PolyList * plist) {
	_fileUtils.writeBlock(VwglbUtils::kPolyList);
	_fileUtils.writeBlock(VwglbUtils::kPlistName);
	_fileUtils.writeString(plist->getName());
	_fileUtils.writeBlock(VwglbUtils::kMatName);
	_fileUtils.writeString(plist->getMaterialName());

	if (plist->getVertexCount()>0) {
		_fileUtils.writeBlock(VwglbUtils::kVertexArray);
		_fileUtils.writeArray(plist->getVertexVector());
	}

	if (plist->getNormalCount()>0) {
		_fileUtils.writeBlock(VwglbUtils::kNormalArray);
		_fileUtils.writeArray(plist->getNormalVector());
	}

	if (plist->getTexCoord0Count()>0) {
		_fileUtils.writeBlock(VwglbUtils::kTexCoord0Array);
		_fileUtils.writeArray(plist->getTexCoord0Vector());
	}

	if (plist->getTexCoord1Count()>0) {
		_fileUtils.writeBlock(VwglbUtils::kTexCoord1Array);
		_fileUtils.writeArray(plist->getTexCoord1Vector());
	}

	if (plist->getTexCoord2Count()>0) {
		_fileUtils.writeBlock(VwglbUtils::kTexCoord2Array);
		_fileUtils.writeArray(plist->getTexCoord2Vector());
	}

	if (plist->getIndexCount()>0) {
		_fileUtils.writeBlock(VwglbUtils::kIndexArray);
		_fileUtils.writeArray(plist->getIndexVector());
	}
}

void VWGLBWritter::saveTextureFile(const std::string & imagePath) {
	std::string fileName = System::get()->getLastPathComponent(imagePath);
	std::string dstPath = _dstPath + fileName;

	if (dstPath != imagePath) {
		std::ifstream inFile(imagePath, std::ios::binary);
		if (inFile) {
			std::ofstream dstFile(dstPath, std::ios::binary);
			if (dstFile) {
				dstFile << inFile.rdbuf();
				dstFile.close();
			}
			inFile.close();
		}
	}
}

bool VWGLBModelWritter::acceptFileType(const std::string & path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext == "vwglb";
}

bool VWGLBModelWritter::writeModel(const std::string & path, vwgl::scene::Drawable * drawable) {
	return writeData(path, drawable);
}

bool VWGLBPrefabWritter::acceptFileType(const std::string & path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext == "vwglb";
}

bool VWGLBPrefabWritter::writePrefab(const std::string & path, vwgl::scene::Node * node) {
	return writeData(path, node);
}

}
