
#include <vwgl/basic_phong_mat.hpp>

namespace vwgl {

#pragma warning ( disable : 4835 )

std::string BasicPhongMaterial::_vshader = "\
	attribute vec3 aVertexPosition;\n\
	attribute vec3 aVertexNormal;\n\
	attribute vec2 aTexCoord0;\n\
	\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
	uniform mat4 uNMatrix;\n\
	\n\
	varying vec4 vNormal;\n\
	varying vec2 vTexCoord;\n\
	void main() {\n\
		vNormal = uNMatrix * vec4(aVertexNormal,1.0);\n\
		vTexCoord = aTexCoord0;\n\
		gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	}";
	
std::string BasicPhongMaterial::_fshader = "\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	\n\
	uniform vec4 uAmbient;\n\
	uniform vec4 uDiffuse;\n\
	uniform vec3 uLightDirection;\n\
	uniform sampler2D uTexture;\n\
	uniform bool uUseTexture;\n\
	varying vec4 vNormal;\n\
	varying vec2 vTexCoord;\n\
	void main() {\n\
		float diffuseLightWeight = max(dot(normalize(vNormal.xyz), -uLightDirection), 0.0);\n\
		vec4 texColor = vec4(1.0);\n\
		if (uUseTexture) {\n\
			texColor = texture2D(uTexture,vTexCoord);\n\
		}\n\
		gl_FragColor = clamp(uAmbient + diffuseLightWeight * uDiffuse,0.0,1.0) * texColor;\n\
	}";
	
	
void BasicPhongMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	getShader()->attachShader(vwgl::Shader::kTypeVertex, BasicPhongMaterial::_vshader);
	getShader()->attachShader(vwgl::Shader::kTypeFragment, BasicPhongMaterial::_fshader);
	getShader()->link("basic_phong_material");
	loadVertexAttrib("aVertexPosition");
	loadNormalAttrib("aVertexNormal");
	loadTexCoord0Attrib("aTexCoord0");
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uNMatrix");
	getShader()->initUniformLocation("uAmbient");
	getShader()->initUniformLocation("uDiffuse");
	getShader()->initUniformLocation("uLightDirection");
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uUseTexture");
}

void BasicPhongMaterial::setupUniforms() {
	getShader()->setUniform("uMVMatrix",modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());
	getShader()->setUniform("uNMatrix", normalMatrix());
	getShader()->setUniform("uLightDirection", _lightDirection);
	getShader()->setUniform("uAmbient", _ambient);
	getShader()->setUniform("uDiffuse", _diffuse);
	getShader()->setUniform("uUseTexture", _texture.valid());
	getShader()->setUniform("uTexture", _texture.getPtr(), Texture::kTexture0);
}
	
	
}
