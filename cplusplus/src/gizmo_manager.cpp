
#include <vwgl/gizmo_manager.hpp>

namespace vwgl {

GizmoManager * GizmoManager::_singleton = NULL;

GizmoManager * GizmoManager::get() {
	if (!_singleton) {
		_singleton = new GizmoManager();
	}
	return _singleton;
}
	
void GizmoManager::finalize() {
	disableGizmo();
	_boundingBox = nullptr;
	_gizmoTransformVisitor = nullptr;
}

GizmoManager::GizmoManager() {
	_boundingBox = new vwgl::BoundingBox();
	_gizmoTransformVisitor = new vwgl::TransformVisitor();
	_gizmoScale = 0.125f;
	SingletonController::get()->registerSingleton(this);
	
}

GizmoManager::~GizmoManager() {
	SingletonController::get()->unregisterSingleton(this);
}

void GizmoManager::enableGizmo(vwgl::TransformNode *trx, vwgl::Gizmo *gizmo) {
	disableGizmo();
	if (trx &&
		gizmo &&
		gizmo->getDrawable()) {
		_currentGizmo = gizmo;
		_currentTransform = trx;
		_boundingBox->setVertexData(trx);
	}
}

void GizmoManager::disableGizmo() {
	if (_currentGizmo.valid() && _currentTransform.valid()) {
		_currentTransform = nullptr;
		_currentGizmo = nullptr;
	}
}
	
bool GizmoManager::checkItemPicked(vwgl::Solid * solid) {
	return _currentGizmo.valid() && solid && _currentGizmo->checkItemPicked(solid);
}
	
void GizmoManager::beginMouseEvents(const vwgl::Position2Di pos, Camera * camera, const Viewport & vp) {
	Vector3 center = _boundingBox->center();
	if (_currentGizmo.valid()) _currentGizmo->beginMouseEvents(pos,camera,vp,center,_currentTransform.getPtr());
}
	
void GizmoManager::mouseEvent(const vwgl::Position2Di pos) {
	if (_currentGizmo.valid()) _currentGizmo->mouseEvent(pos);
}
	
void GizmoManager::endMouseEvents() {
	if (_currentGizmo.valid()) _currentGizmo->endMouseEvents();
}
	
void GizmoManager::drawGizmo(vwgl::Camera * cam, vwgl::ColorPickMaterial * gizmoMaterial) {
	if (_currentGizmo.valid() && _currentTransform.valid()) {
		glClear(GL_DEPTH_BUFFER_BIT);
		_gizmoTransformVisitor->visit(_currentTransform.getPtr());
		vwgl::Matrix4 & trx = _gizmoTransformVisitor->getTransform();
		
		float sx = Vector3(trx.get00(), trx.get01(), trx.get02()).magnitude();
		float sy = Vector3(trx.get10(), trx.get11(), trx.get12()).magnitude();
		float sz = Vector3(trx.get20(), trx.get21(), trx.get22()).magnitude();
		trx.scale(1.0f/sx,1.0f/sy,1.0f/sz);
		
		vwgl::State::get()->pushModelMatrix();
		if (_currentGizmo->getResetRotation()) {
			 vwgl::Vector3 pos = trx.getPosition();
			vwgl::State::get()->modelMatrix().identity().translate(pos.x(), pos.y(), pos.z());
		}
		else {
			vwgl::State::get()->modelMatrix() = trx;
		}
		
		
		
		float distance = vwgl::State::get()->modelViewMatrix().multVector(vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f)).z();
		distance *= _gizmoScale;
		vwgl::State::get()->modelMatrix().scale(-distance, -distance, -distance);
		
		if (gizmoMaterial==nullptr)  {
			_currentGizmo->getDrawable()->draw();
		}
		else {
			SolidList solids = _currentGizmo->getDrawable()->getSolidList();
			SolidList::iterator it;
			for (it=solids.begin(); it!=solids.end(); ++it) {
				if (!(*it)->getPickId().isZero() && (*it)->isVisible()) {
					gizmoMaterial->setColorPickId((*it)->getPickId());
					gizmoMaterial->bindPolyList((*it)->getPolyList());
					gizmoMaterial->activate();
					(*it)->getPolyList()->drawElements();
					gizmoMaterial->deactivate();
				}
			}
		}
		vwgl::State::get()->popModelMatrix();
	}
}

}