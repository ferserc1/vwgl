
#include <vwgl/solid.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/glerror.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/state.hpp>

namespace vwgl {

Solid::Solid() :_renderOpaque(true), _renderTransparent(true), _drawable(nullptr), _visible(true) {
	_transform.identity();
}

Solid::Solid(PolyList * plist) :_renderOpaque(true), _renderTransparent(true), _drawable(nullptr), _visible(true) {
	_polyList = plist;
	_material = new GenericMaterial();
	_transform.identity();
}
	
Solid::Solid(Material * mat) :_renderOpaque(true), _renderTransparent(true), _drawable(nullptr), _visible(true) {
	_material = mat;
	_transform.identity();
}

Solid::Solid(PolyList * plist, Material * mat) :_renderOpaque(true), _renderTransparent(true), _drawable(nullptr), _visible(true) {
	_polyList = plist;
	_material = mat;
	_transform.identity();
}
	
Solid::~Solid() {
	destroy();
}

Solid * Solid::clone(CloneMode polyListClone, CloneMode materialClone) {
	ptr<Solid> _newSolid = new vwgl::Solid();
	Material * mat = nullptr;
	PolyList * plist = nullptr;
	if (polyListClone==kCloneModeCopy) {
		plist = new PolyList(_polyList.getPtr());
	}
	else {
		plist = _polyList.getPtr();
	}
	
	plist->setName(_polyList->getName() + "_clone");
	_newSolid->setGroupName(getGroupName());
	if (_transformStrategy.valid()) _newSolid->setTransformStrategy(_transformStrategy.getPtr());
	_newSolid->setTransform(getTransform());
	_newSolid->setVisible(isVisible());
	
	if (getGenericMaterial()) {
		if (materialClone==kCloneModeCopy) {
			GenericMaterial * genMat = new GenericMaterial();
			MaterialModifier modifier = getGenericMaterial()->getModifierWithMask(0xFFFFFFFF);
			genMat->applyModifier(modifier, "", false);
			mat = genMat;
		}
		else {
			mat = getGenericMaterial();
		}
	}
	else {
		mat = new vwgl::Material();
	}
	_newSolid->setPolyList(plist);
	_newSolid->setMaterial(mat);
	
	return _newSolid.release();
}

void Solid::setPolyList(PolyList * plist) {
	_polyList = plist;
}

void Solid::setMaterial(Material * mat) {
	_material = mat;
};
	
PolyList * Solid::getPolyList() {
	return _polyList.getPtr();
}

Material * Solid::getMaterial() {
	return _material.getPtr();
}
	
void Solid::draw(Material * mat, bool useMaterialSettings) {
	if (_polyList.valid() && _visible) {
		vwgl::State::get()->pushModelMatrix();
		vwgl::State::get()->modelMatrix().mult(_transform);
		Material * currentMaterial = NULL;
		bool isTransparent = false;
		if (mat) {
			if (useMaterialSettings && _material.valid()) {
				mat->useSettingsOf(_material.getPtr());
				isTransparent = _material->isTransparent();
			}
			else {
				isTransparent = mat->isTransparent();
			}
			currentMaterial = mat;
		}
		else {
			if (!_material.valid()) _material = new GenericMaterial();
			currentMaterial = _material.getPtr();
			isTransparent = currentMaterial->isTransparent();
		}
		
		if (currentMaterial &&
			((isTransparent && _renderTransparent) ||
			(!isTransparent && _renderOpaque)) ) {
			currentMaterial->bindPolyList(_polyList.getPtr());
			currentMaterial->activate();
				
				
			glValidateProgram(currentMaterial->getShader()->getShaderProgram());
			GLint status;
			glGetProgramiv(currentMaterial->getShader()->getShaderProgram(), GL_VALIDATE_STATUS, &status);
			if (status == GL_FALSE){
				
				std::cout << "Error validating shader : " << GlError::getLinkErrorString(currentMaterial->getShader()->getShaderProgram()) << std::endl;
			}

			_polyList->drawElements();
			currentMaterial->deactivate();
		}
		
		if (mat) {
			mat->useSettingsOf(NULL);
		}
		vwgl::State::get()->popModelMatrix();
	}
}
	
void Solid::destroy() {
	_polyList = NULL;
	_material = NULL;
	removeProjectorFromScene();
	_shadowProjector = NULL;
}
	
void Solid::setShadowProjector(Projector * proj) {
	if (proj!=NULL) {
		_shadowProjector = proj;
		_shadowProjector->setVisible(this->_visible);
		Texture * tex = _shadowProjector->getTexture();
		if (tex) {
			tex->bindTexture(vwgl::Texture::kTargetTexture2D);
			tex->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
			tex->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		}
		if (_drawable && _drawable->getParent()) _drawable->getParent()->setInitialized(false);
	}
	else {
		removeProjectorFromScene();
		_shadowProjector = NULL;
	}
}

void Solid::removeProjectorFromScene() {
	if (_shadowProjector.valid() && _shadowProjector->getParent()) {
		_shadowProjector->getParent()->removeChild(_shadowProjector.getPtr());
	}
}


}
