
#include <vwgl/system.hpp>
#include <algorithm>
#include <sstream>

extern "C"
{
#ifdef WIN32
#include <Rpc.h>
#else
#include <uuid/uuid.h>
#endif
}

namespace vwgl {

System * System::s_singleton = NULL;

bool System::isMac() {
	#if VWGL_MAC==1
	return true;
	#else
	return false;
	#endif
}

bool System::isWindows() {
	#if VWGL_WINDOWS==1
	return true;
	#else
	return false;
	#endif
}

bool System::isLinux() {
	#if VWGL_LINUX==1
	return true;
	#else
	return false;
	#endif
}

bool System::isIOS() {
	#if VWGL_IOS==1
	return true;
	#else
	return false;
	#endif
}

bool System::isIOSSimulator() {
	#if VWGL_IOS_SIMULATOR==1
	return true;
	#else
	return false;
	#endif
}

void System::cleanTempDir() {
	if (_tempPath!="" && !isWindows()) {
		removeDirectory(_tempPath);
		_tempPath = "";
	}
}

void System::standarizePath(const std::string & inputPath, std::string & outputPath) {
	outputPath = inputPath;
	if (VWGL_WINDOWS==1) {
		std::replace(outputPath.begin(),outputPath.end(), '\\','/');
	}
}

bool System::isAbsolutePath(const std::string & path) {
	if (VWGL_WINDOWS==1) {
		if (path.size()<2) return false;
		return path.at(1)==':' || path.at(0)=='/' || path.at(0)=='\\';
	}
	else {
		if (path.size()==0) return false;
		return path.front()=='/';
	}
}

std::string System::getUniqueFileName(const std::string &path, int baseIndex) {
	if (path == "") return "";
	if (fileExists(path) || directoryExists(path)) {
		std::string extension = getExtension(path);
		std::string fileName = removeExtension(path);
		std::stringstream newFileName;
		newFileName << fileName << "_" << baseIndex;
		if (extension!="") {
			newFileName << "." << extension;
		}
		if (fileExists(newFileName.str()) || directoryExists(newFileName.str())) {
			return getUniqueFileName(path, baseIndex+1);
		}
		else {
			return newFileName.str();
		}
	}
	return path;
}

std::string System::getExtension(const std::string & path) {
	if (path == "") return "";
	std::string stdPath = getPath(path);
	std::string fileName = getLastPathComponent(path);
	if (fileName.find_last_of(".")!=std::string::npos) {
		fileName.erase(0,fileName.find_last_of(".") + 1);
		stdPath = fileName;
	}
	else {
		stdPath = "";
	}
	std::transform(stdPath.begin(),
		stdPath.end(),
		stdPath.begin(),
		::tolower);
	return stdPath;
}

std::string System::getSubpath(const std::string & fullPath, const std::string & subpath) {
	size_t start_pos = fullPath.find(subpath);
	std::string result = fullPath;
	if(start_pos == std::string::npos)
		return "";
	result.replace(start_pos, subpath.length(), "");
	return result;
}

std::string System::removeExtension(const std::string & path) {
	if (path == "") return "";
	std::string stdPath = getPath(path);
	std::string fileName = getLastPathComponent(path);
	if (fileName.find_last_of(".")!=std::string::npos) {
		fileName.erase(fileName.find_last_of("."));
	}
	stdPath = addPathComponent(stdPath, fileName);
	return stdPath;
}

std::string System::getFileName(const std::string & path) {
	if (path == "") return "";
	std::string stdPath;
	standarizePath(path, stdPath);
	if (stdPath.find_last_of("/")!=std::string::npos) stdPath.erase(0,stdPath.find_last_of("/") + 1);
	if (stdPath.find_last_of(".")!=std::string::npos) {
		stdPath.erase(stdPath.find_last_of("."));
	}
	return stdPath;
}

std::string System::getLastPathComponent(const std::string & path) {
	if (path == "") return "";
	std::string stdPath;
	standarizePath(path, stdPath);
	if (stdPath.find_last_of("/")==stdPath.size()-1) {
		stdPath.erase(stdPath.size()-1);
	}
	if (stdPath.find_last_of("/")!=std::string::npos) stdPath.erase(0,stdPath.find_last_of("/") + 1);
	return stdPath;
}

std::string System::getPath(const std::string & filePath) {
	if (filePath == "") return "";
	std::string stdPath;
	standarizePath(filePath, stdPath);
	if (stdPath.find_last_of("/")) {
		stdPath.erase(stdPath.find_last_of("/") + 1);
	}
	return stdPath;
}

std::string System::addPathComponent(const std::string & base, const std::string & append) {
	if (base == "") return append;
	std::string stdPath;
	std::string lastComponent = append;
	standarizePath(base, stdPath);

	if (lastComponent.find_first_of("/")==0) {
		lastComponent = lastComponent.erase(0,1);
	}
	if (stdPath.find_last_of("/")!=(stdPath.size() - 1)) {
		stdPath = stdPath + "/" + lastComponent;
	}
	else {
		stdPath += lastComponent;
	}
	return stdPath;
}

std::string System::addExtension(const std::string & path, const std::string & extension) {
	if (path == "") return extension;
	std::string stdPath;
	std::string lastComponent = extension;
	standarizePath(path, stdPath);
	if (extension.size()==0) {
		return stdPath;
	}

	if (lastComponent.find_first_of(".")==0) {
		lastComponent = lastComponent.erase(0,1);
	}
	if (stdPath.find_last_of(".")!=(stdPath.size() - 1)) {
		stdPath = stdPath + "." + lastComponent;
	}
	else {
		stdPath += lastComponent;
	}
	return stdPath;
}

void System::listFilesRecursive(const std::string & path, std::vector<std::string> & allFiles, bool clearVector, const std::string & basePath) {
	if (clearVector) {
		allFiles.clear();
	}
	std::vector<std::string> files;
	std::vector<std::string> directories;
	listDirectory(path, files, directories);
	std::vector<std::string>::iterator it;
	for (it=files.begin(); it!=files.end(); ++it) {
		if ((*it).front()=='.') continue;
		allFiles.push_back(addPathComponent(basePath, *it));
	}
	for (it=directories.begin(); it!=directories.end(); ++it) {
		std::string newBasePath = addPathComponent(basePath, *it);
		listFilesRecursive(addPathComponent(path, *it), allFiles, false, newBasePath);
	}
}

#if PLATFORM_MAC==0
void System::bindGLKViewDrawable() {
	// This function does nothing in windows platform
}
#endif

void System::_createTempDir() {
	if (_tempPath!="") {
		_tempPath = addPathComponent(_tempPath, "redpill_editor");
		if (!directoryExists(_tempPath)) {
			mkdir(_tempPath);
		}
	}
}

}
