
#include <vwgl/system.hpp>

#include <vwgl/app/window.hpp>

#include <vwgl/app/window_controller.hpp>

#ifdef _WIN32
#include <vwgl/app/win32_window.hpp>
#elif VWGL_MAC==1
#include <vwgl/app/cocoa_window.hpp>
#elif VWGL_LINUX==1
#include <vwgl/app/gdl_window.hpp>
#endif

namespace vwgl {
namespace app {

Window * Window::newWindowInstance() {
	Window * win = nullptr;
#ifdef _WIN32
	win = new Win32Window();
#elif VWGL_MAC==1
	win = new CocoaWindow();
#elif VWGL_LINUX==1
	win = new GdlWindow();
#endif
	return win;
}

Window::Window() :_title("Window"), _rect(vwgl::Rect(20,20,300,250)), _fullscreen(false), _context(nullptr) {
	_controller = new WindowController();
}

Window::~Window() {
}

void Window::setWindowController(WindowController * controller) {
	_controller = controller;
	if (controller) {
		_controller->setWindow(this);
	}
}

}
}
