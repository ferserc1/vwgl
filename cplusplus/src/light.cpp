
#include <vwgl/light.hpp>
#include <vwgl/state.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/shadow_light.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/system.hpp>
#include <vwgl/sphere.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/shadow_map_material.hpp>
#include <vwgl/drawable.hpp>

#include <iostream>
#include <algorithm>

#include <iostream>

namespace vwgl {

Light::Light()
	:ILight(),
	_intensity(1.0f),
	_constantAtt(1.0f),
	_linearAtt(0.5f),
	_expAtt(0.1f),
	_spotExponent(30.0f),
	_spotCutoff(20.0f),
	_shadowStrength(1.0f),
	_cutoffDistance(-1.0f)
{
	LightManager::get()->addLight(this);
	_ambient = Vector4(0.2f,0.2f,0.2f,1.0f);
	_diffuse = Vector4(0.9f,0.9f,0.9f,1.0f);
	_specular = Vector4(1.0f,1.0,1.0,1.0);
	_transformVisitor = new TransformVisitor();
}

Light::~Light() {
	LightManager::get()->removeLight(this);
}

Vector3 Light::getPosition() {
	if (_lightType==kTypeDirectional) return getDirection();
	_transformVisitor->visit(this);
	Matrix4 tr = _transformVisitor->getTransform();
	return tr.getPosition();
}

Vector3 Light::getDirection() {
	Vector3 look(0.0,0.0,1.0);
	Camera * cam = CameraManager::get()->getMainCamera();
	_transformVisitor->visit(this);
	Matrix4 tr = _transformVisitor->getTransform();
	tr.setRow(3, Vector4(0.0f,0.0f,0.0f,1.0f));
	if (cam) {
		Matrix4 vm = cam->getViewMatrix();
		vm.setRow(3, Vector4(0.0f,0.0f,0.0f,1.0f));
		vm.mult(tr);
		look = vm.multVector(look).xyz();
	}
	else {
		look = tr.multVector(look).xyz();
	}
	look.normalize();
	return look;
}


vwgl::Matrix4 & Light::getTransform() {
	vwgl::TransformNode * trNode = dynamic_cast<vwgl::TransformNode*>(getParent());
	if (trNode) {
		return trNode->getTransform();
	}
	return _fooTransform;
}
	
vwgl::Matrix4 & Light::getAbsoluteTransform() {
	_transformVisitor->visit(this);
	return _transformVisitor->getTransform();
}

Drawable * Light::getGizmoDrawable() {
	Drawable * drw = LightManager::get()->getGizmo(_lightType);
	if (drw && drw->getGenericMaterial() && drw->getGenericMaterial()->getLight()) {
		drw->getGenericMaterial()->getLight()->setAmbient(_diffuse);
		drw->getGenericMaterial()->setDiffuse(_diffuse);
		drw->getGenericMaterial()->getDiffuse().a(0.7f);
		drw->getGenericMaterial()->setLightEmission(0.0f);
	}
	return drw;
}


LightManager * LightManager::s_lightManager = nullptr;
int LightManager::_maxLightSources = 15;

LightManager::LightManager()
	:_typeArray(0L),
	_positionArray(0L),
	_directionArray(0L),
	_ambientArray(0L),
	_diffuseArray(0L),
	_specularArray(0L),
	_numberOfLights(0),
	_properties(0L),
	_shadowMapSize(512)
{
	SingletonController::get()->registerSingleton(this);
}

LightManager::~LightManager() {
	_lightList.clear();
	destroyUniforms();
}

LightManager * LightManager::get() {
	if (s_lightManager == nullptr) {
		s_lightManager = new LightManager();
	}
	return s_lightManager;
}

void LightManager::finalize() {
	_directionalGizmo = nullptr;
	_spotGizmo = nullptr;
	_pointGizmo = nullptr;
	_lightList.clear();
	destroyUniforms();
}

void LightManager::addLight(Light * light) {
	_lightList.push_back(light);
}

void LightManager::removeLight(Light * light) {
	LightList::iterator it = std::find(_lightList.begin(), _lightList.end(), light);
	if (it!=_lightList.end()) _lightList.erase(it);
}

void LightManager::prepareUniforms() {
	if (_numberOfLights!=_lightList.size()) {
		allocUniforms();
	}
	LightList::iterator it;
	int i = 0;
	for (it=_lightList.begin(); it!=_lightList.end(); ++it) {
		_ambientArray[i*4] = (*it)->getAmbient().r();
		_ambientArray[i*4+1] = (*it)->getAmbient().g();
		_ambientArray[i*4+2] = (*it)->getAmbient().b();
		_ambientArray[i*4+3] = (*it)->getAmbient().a();
		
		_diffuseArray[i*4] = (*it)->getDiffuse().r();
		_diffuseArray[i*4+1] = (*it)->getDiffuse().g();
		_diffuseArray[i*4+2] = (*it)->getDiffuse().b();
		_diffuseArray[i*4+3] = (*it)->getDiffuse().a();
		
		_specularArray[i*4] = (*it)->getSpecular().r();
		_specularArray[i*4+1] = (*it)->getSpecular().g();
		_specularArray[i*4+2] = (*it)->getSpecular().b();
		_specularArray[i*4+3] = (*it)->getSpecular().a();
		
		_typeArray[i] = (int) (*it)->isEnabled() ? (*it)->getType():Light::kTypeDisabled;
		
		Vector3 pos = (*it)->getPosition();
		_positionArray[i*3] = pos.x();
		_positionArray[i*3+1] = pos.y();
		_positionArray[i*3+2] = pos.z();
		
		Vector3 dir = (*it)->getDirection();
		_directionArray[i*3] = dir.x();
		_directionArray[i*3+1] = dir.y();
		_directionArray[i*3+2] = dir.z();
		
		_properties[i*4] = (*it)->getIntensity();
		_properties[i*4+1] = (*it)->getLinearAttenuation();
		_properties[i*4+2] = (*it)->getExpAttenuation();
		_properties[i*4+3] = 0.0f;
		
		++i;
	}
	
}

void LightManager::prepareFrame(bool noShadows) {
	prepareUniforms();
}

void LightManager::updateShadowMap(Group * sceneRoot, ShadowLight * light) {
	if (!_renderPass.valid()) createShadowMap();
	if (_renderPass.valid() && light->getCastShadows()) {
		_renderPass->setSceneRoot(sceneRoot);
		vwgl::State::get()->pushProjectionMatrix();
		vwgl::State::get()->pushViewMatrix();
		vwgl::State::get()->projectionMatrix() = light->getProjectionMatrix();
		vwgl::State::get()->viewMatrix() = light->getViewMatrix();
		_renderPass->updateTexture();
		vwgl::State::get()->popProjectionMatrix();
		vwgl::State::get()->popViewMatrix();
	}
}

Texture * LightManager::getShadowTexture() {
	Texture * result = nullptr;
	if (_renderPass.valid() && _renderPass->getFbo()) {
		result = _renderPass->getFbo()->getTexture();
	}
	return result;
}

void LightManager::allocUniforms() {
	destroyUniforms();
	_numberOfLights = (int) _lightList.size();
	if (_numberOfLights>0) {
		_typeArray = new int[_numberOfLights];
		_positionArray = new float[_numberOfLights * 3];
		_directionArray = new float[_numberOfLights * 3];
		_ambientArray = new float[_numberOfLights * 4];
		_diffuseArray = new float[_numberOfLights * 4];
		_specularArray = new float[_numberOfLights * 4];
		_properties = new float[_numberOfLights * 4];
	}
}

void LightManager::destroyUniforms() {
	if (_positionArray!=0L) {
		delete _typeArray;
		delete _positionArray;
		delete _directionArray;
		delete _ambientArray;
		delete _diffuseArray;
		delete _specularArray;
		delete _properties;
	}
	_typeArray = 0L;
	_positionArray = 0L;
	_directionArray = 0L;
	_ambientArray = 0L;
	_diffuseArray = 0L;
	_specularArray = 0L;
	_numberOfLights = 0L;
	_properties = 0L;
}
	
Drawable * LightManager::getGizmo(Light::LightType type) {
	if (!_gizmoLight.valid()) {
		_gizmoLight = new vwgl::Light();
		_gizmoLight->setType(Light::kTypeDirectional);
		_gizmoLight->setEnabled(false);
		_gizmoLight->setAmbient(Color(0.8f,0.8f,0.8f,1.0f));
		_gizmoLight->setDiffuse(Color::black());
	}
	switch (type) {
		case Light::kTypeDirectional:
			if (!_directionalGizmo.valid()) {
				_directionalGizmo = loadGizmo("directional_light.vwglb");
				_directionalGizmo->getGenericMaterial()->useLight(_gizmoLight.getPtr());
			}
			return _directionalGizmo.getPtr();
		case Light::kTypeSpot:
			if (!_spotGizmo.valid()) {
				_spotGizmo = loadGizmo("spot_light.vwglb");
				_spotGizmo->getGenericMaterial()->useLight(_gizmoLight.getPtr());
			}
			return _spotGizmo.getPtr();
		case Light::kTypePoint:
			if (!_pointGizmo.valid()) {
				_pointGizmo = loadGizmo("point_light.vwglb");
				_pointGizmo->getGenericMaterial()->useLight(_gizmoLight.getPtr());
			}
			return _pointGizmo.getPtr();
		case Light::kTypeDisabled:
			return nullptr;
	}
	return nullptr;
}

Drawable * LightManager::loadGizmo(const std::string & file) {
	std::string filePath = System::get()->getResourcesPath();
	filePath = System::get()->addPathComponent(filePath, file);
	Drawable * drw = Loader::get()->loadDrawable(filePath);
	if (!drw) {
		drw = new Sphere(0.1f,10,10);
	}
	return drw;
}

void LightManager::createShadowMap() {
	if (_renderPass.valid()) {
		_renderPass->getFbo()->resize(_shadowMapSize);
	}
	else {
		vwgl::ShadowMapMaterial * renderPassMat = new vwgl::ShadowMapMaterial();
		_renderPass = new RenderPass();
		_renderPass->create(renderPassMat, new vwgl::FramebufferObject(_shadowMapSize));
		_renderPass->setClearColor(vwgl::Color::black());
	}
}

void LightManager::destroyShadowMap() {
	if (_renderPass.valid()) {
		_renderPass->destroy();
		_renderPass = nullptr;
	}
}

}