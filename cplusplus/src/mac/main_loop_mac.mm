
#include <vwgl/app/main_loop.hpp>
#include <vwgl/app/window_controller.hpp>
#include <vwgl/app/cocoa_app_delegate.h>

#include <Cocoa/Cocoa.h>

namespace vwgl {
namespace app {

int MainLoop::run() {
	@autoreleasepool {
		[NSApplication sharedApplication];
		[NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
		
		id menubar = [NSMenu new];
		id appMenuItem = [NSMenuItem new];
		[menubar addItem:appMenuItem];
		[NSApp setMainMenu:menubar];
		id appMenu = [NSMenu new];
		id appName = [[NSProcessInfo processInfo] processName];
		id quitTitle = [@"Quit " stringByAppendingString:appName];
		id quitMenuItem = [[NSMenuItem alloc] initWithTitle:quitTitle
													  action:@selector(terminate:) keyEquivalent:@"q"];
		[appMenu addItem:quitMenuItem];
		[appMenuItem setSubmenu:appMenu];
		CocoaAppDelegate * appDelegate = [CocoaAppDelegate new];
		[[NSApplication sharedApplication] setDelegate:appDelegate];
		[NSApp activateIgnoringOtherApps:YES];
		[NSApp run];
	}
	return 0;
}

void MainLoop::quit(int code) {
	[[NSApplication sharedApplication] terminate:nil];
}

}
}
