
#include <vwgl/opengl.hpp>
#ifdef __OBJC__
#if TARGET_IPHONE_SIMULATOR

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

#import "NSFileManager+Tar.h"

#elif TARGET_OS_IPHONE

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

#else

#import <Cocoa/Cocoa.h>

#endif



#include <vwgl/system.hpp>

#include <iostream>


namespace vwgl {
	
#if VWGL_IOS
bool System::packFiles(const std::string & inPath, const std::vector<std::string> & files, const std::string & outFile, System::PackFormat fmt) {

	return false;
}
		
bool System::unpackFiles(const std::string & packedFile, const std::string & outPath) {
	NSData* tarData = [NSData dataWithContentsOfFile:[NSString stringWithUTF8String:packedFile.c_str()]];
	NSError *error;
	if (error) return false;
	[[NSFileManager defaultManager] createFilesAndDirectoriesAtPath:[NSString stringWithUTF8String:outPath.c_str()] withTarData:tarData error:&error];
	return true;
}

#endif
		
	

const std::string & System::getExecutablePath() {
	if (_executablePath=="") {
		NSString * executablePath = [[NSBundle mainBundle] executablePath];
		BOOL isDir;
		if ([[NSFileManager defaultManager] fileExistsAtPath:executablePath isDirectory:&isDir]) {
			if (!isDir) {
				executablePath = [executablePath stringByDeletingLastPathComponent];
			}
		}
		else {
			executablePath = @"";
		}
		_executablePath = [executablePath UTF8String];
		if (_executablePath.back()!='/') _executablePath += '/';
	}
	return _executablePath;
}

const std::string & System::getDefaultShaderPath() {
	if (_defaultShaderPath=="") {
		NSString * resourcesPath = [[NSBundle mainBundle] resourcePath];
		_defaultShaderPath = [resourcesPath UTF8String];
		if (_defaultShaderPath.back()!='/') _defaultShaderPath += '/';
	}
	return _defaultShaderPath;
}

const std::string & System::getResourcesPath() {
	if (_resourcesPath=="") {
		NSString * resourcesPath = [[NSBundle mainBundle] resourcePath];
		_resourcesPath = [resourcesPath UTF8String];
		if (_resourcesPath.back()!='/') _resourcesPath += '/';
	}
	return _resourcesPath;
}
	
const std::string & System::getTempPath() {
	if (_tempPath=="") {
		NSString * tmp = NSTemporaryDirectory();
		_tempPath = [tmp UTF8String];
		_createTempDir();
	}
	return _tempPath;
}

void System::bindGLKViewDrawable() {
	#ifdef VWGL_OPENGL_ES
	if (_glkView) {
		[((__bridge GLKView *) _glkView) bindDrawable];
	}
	#endif
}

bool System::directoryExists(const std::string & path) {
	BOOL isDir = NO;
	bool exists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithUTF8String:path.c_str()] isDirectory:&isDir];
	return exists && isDir;
}

bool System::fileExists(const std::string &path) {
	bool exists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithUTF8String:path.c_str()]];
	return exists;
}
	
bool System::mkdir(const std::string & dir) {
	NSError * err;
	[[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithUTF8String:dir.c_str()] withIntermediateDirectories:NO attributes:nil error:&err];
	if (err) {
		return false;
	}
	return true;
}
	
bool System::cp(const std::string & src, const std::string & dst, bool overwrite) {
	NSError * err;
	[[NSFileManager defaultManager] copyItemAtPath:[NSString stringWithUTF8String:src.c_str()] toPath:[NSString stringWithUTF8String:dst.c_str()] error:&err];
	if (err) {
		return false;
	}
	return true;
}

bool System::removeDirectory(const std::string & path) {
	if (directoryExists(path)) {
		NSError * err;
		[[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithUTF8String:path.c_str()] error:&err];
		return err!=nil;
	}
	return false;
}

bool System::removeFile(const std::string & path) {
	if (fileExists(path)) {
		NSError * err;
		[[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithUTF8String:path.c_str()] error:&err];
		return err!=nil;
	}
	return false;
}
	
void System::listDirectory(const std::string & path, std::vector<std::string> & files, std::vector<std::string> & directories, bool clearVectors) {
	if (!directoryExists(path)) return;
	
	if (clearVectors) {
		files.clear();
		directories.clear();
	}
	NSString * nspath = [NSString stringWithUTF8String:path.c_str()];
	NSArray * contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:nspath error:nil];
	BOOL isDir;
	for (NSString * item in contents) {
		NSString * fileFullPath = [nspath stringByAppendingPathComponent:item];
		[[NSFileManager defaultManager] fileExistsAtPath:fileFullPath isDirectory:&isDir];
		if (isDir) {
			directories.push_back(item.UTF8String);
		}
		else {
			files.push_back(item.UTF8String);
		}
	}
}

std::string System::getUuid() {
	std::string s;
	NSString * uuidString = [[NSUUID UUID] UUIDString];
	s = [uuidString UTF8String];

	return s;
}

}

#endif