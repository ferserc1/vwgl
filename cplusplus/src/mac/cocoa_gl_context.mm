#include <vwgl/app/cocoa_gl_context.hpp>

#include <vwgl/app/window_controller.hpp>

#include <vwgl/opengl.hpp>

#import <OpenGL/OpenGL.h>
#import <Cocoa/Cocoa.h>

namespace vwgl {
namespace app {

bool CocoaGLContext::createContext() {
	return true;
}

void CocoaGLContext::makeCurrent() {
	NSOpenGLContext * context =  (__bridge NSOpenGLContext*)_nativeContext;
	if (context) {
		[context makeCurrentContext];
	}
}

void CocoaGLContext::swapBuffers() {
	NSOpenGLContext * context =  (__bridge NSOpenGLContext*)_nativeContext;
	if (context) {
		[context flushBuffer];
	}
}

void CocoaGLContext::destroy() {
}

}
}
