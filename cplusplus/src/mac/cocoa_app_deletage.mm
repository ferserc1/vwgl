
#include <vwgl/app/cocoa_app_delegate.h>
#include <vwgl/app/main_loop.hpp>
#include <vwgl/app/window.hpp>
#include <vwgl/app/cocoa_window.hpp>
#include <vwgl/app/window_controller.hpp>

@interface CocoaAppDelegate ()

@end

@implementation CocoaAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	vwgl::app::Window * window = mainLoop->getWindow();
	vwgl::app::CocoaWindow * cocoaWindow = dynamic_cast<vwgl::app::CocoaWindow*>(window);
	if (cocoaWindow && cocoaWindow->createCocoaWindow()) {

	}
}

- (void)applicationWillTerminate:(NSNotification *)notification {
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	vwgl::app::Window * window = mainLoop->getWindow();
	vwgl::app::CocoaWindow * cocoaWindow = dynamic_cast<vwgl::app::CocoaWindow*>(window);
	if (cocoaWindow) {
		cocoaWindow->destroy();
		vwgl::app::MainLoop::destroy();
	}
}

@end
