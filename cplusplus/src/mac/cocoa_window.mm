
#include <vwgl/app/cocoa_window.hpp>
#include <vwgl/app/cocoa_glview.h>
#include <vwgl/app/cocoa_gl_context.hpp>

#include <Cocoa/Cocoa.h>
#include <OpenGL/OpenGL.h>

namespace vwgl {
namespace app {

bool CocoaWindow::create() {
	_createWindow = true;
	return true;
}

bool CocoaWindow::createCocoaWindow() {
	if (_createWindow) {
		NSRect frame = NSMakeRect(_rect.x(), _rect.y(), _rect.width(), _rect.height());
		NSUInteger styleMask =  NSTitledWindowMask |
		NSClosableWindowMask |
		NSMiniaturizableWindowMask |
		NSResizableWindowMask;
		NSRect rect = [NSWindow contentRectForFrameRect:frame styleMask:styleMask];
		NSWindow * window = [[NSWindow alloc] initWithContentRect:rect styleMask:styleMask backing:NSBackingStoreBuffered defer:false];
		
		CocoaView * glView = [[CocoaView alloc] initWithFrame:frame pixelFormat:nil];
		glView.cocoaWindow = this;
		[window setContentView:glView];
		NSOpenGLContext * nativeContext = glView.openGLContext;
		CocoaGLContext * context = new CocoaGLContext(this);
		context->setCocoaGLContext((__bridge void*)nativeContext);
		_context = context;
		[window setAcceptsMouseMovedEvents:YES];
		[window makeFirstResponder:glView];
		
		_windowPtr = (__bridge_retained void*)window;
		[window makeKeyAndOrderFront:window];
		
		return true;
	}
	return false;
}

void CocoaWindow::destroy() {
	if (_windowPtr) {
		NSWindow * window = (__bridge_transfer NSWindow*)_windowPtr;
		[window orderOut:window];
		window = nil;
		_windowPtr = nullptr;
	}
}

}
}