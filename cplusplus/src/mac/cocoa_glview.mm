
#include <vwgl/app/cocoa_glview.h>
#include <vwgl/app/cocoa_gl_context.hpp>
#include <vwgl/app/window_controller.hpp>
#include <vwgl/app/main_loop.hpp>
#include <vwgl/opengl.hpp>
#include <vwgl/graphics.hpp>


#import <Carbon/Carbon.h>
#import <Cocoa/Cocoa.h>

#include <iostream>

#define SUPPORT_RETINA_RESOLUTION 1

vwgl::Keyboard::KeyCode translateCode(char character, unsigned short code) {
	vwgl::Keyboard::KeyCode keyCode = vwgl::Keyboard::kKeyNull;
	
	if (character >= 'a' && character <= 'z') {
		keyCode = static_cast<vwgl::Keyboard::KeyCode>(vwgl::Keyboard::kKeyA + (character - 'a'));
	}
	else if (character >= 'A' && character <= 'Z') {
		keyCode = static_cast<vwgl::Keyboard::KeyCode>(vwgl::Keyboard::kKeyA + (character - 'A'));
	}
	else if (character >= '0' && character <= '9') {
		keyCode = static_cast<vwgl::Keyboard::KeyCode>(vwgl::Keyboard::kKey0 + (character - '0'));
	}
	else if (character=='+') {
		keyCode = vwgl::Keyboard::kKeyAdd;
	}
	else if (character=='-') {
		keyCode = vwgl::Keyboard::kKeySub;
	}
	else if (code == kVK_Escape) {
		keyCode = vwgl::Keyboard::kKeyEsc;
	}
	else if (code == kVK_ForwardDelete) {
		keyCode = vwgl::Keyboard::kKeyDel;
	}
	else if (code == kVK_Tab) {
		keyCode = vwgl::Keyboard::kKeyTab;
	}
	else if (code == kVK_Space) {
		keyCode = vwgl::Keyboard::kKeySpace;
	}
	else if (code == kVK_Delete) {
		keyCode = vwgl::Keyboard::kKeyBack;
	}
	else if (code == kVK_LeftArrow) {
		keyCode = vwgl::Keyboard::kKeyLeft;
	}
	else if (code == kVK_RightArrow) {
		keyCode = vwgl::Keyboard::kKeyRight;
	}
	else if (code == kVK_UpArrow) {
		keyCode = vwgl::Keyboard::kKeyUp;
	}
	else if (code == kVK_DownArrow) {
		keyCode = vwgl::Keyboard::kKeyDown;
	}
	else if (code == kVK_Return) {
		keyCode = vwgl::Keyboard::kKeyReturn;
	}
	
	return keyCode;
}

unsigned int getModifiers(NSUInteger flags) {
	unsigned int modifiers = 0;
	if ((flags & NSControlKeyMask)!=0) {
		modifiers = modifiers | vwgl::Keyboard::kCtrlKey | vwgl::Keyboard::kCommandOrControlKey;
	}
	if ((flags & NSShiftKeyMask)!=0) {
		modifiers = modifiers | vwgl::Keyboard::kShiftKey;
	}
	if ((flags & NSAlternateKeyMask)!=0) {
		modifiers = modifiers | vwgl::Keyboard::kAltKey;
	}
	if ((flags & NSCommandKeyMask)!=0) {
		modifiers = modifiers | vwgl::Keyboard::kCommandKey | vwgl::Keyboard::kCommandOrControlKey;
	}
	
	return modifiers;
}

void fillKeyboard(vwgl::Keyboard & kb, NSString * characters, unsigned short code, NSUInteger flags) {
	char character = [characters characterAtIndex:0];
	kb.setCharacter(character);
	kb.setKey(translateCode(character,code));
	kb.setModifierMask(getModifiers(flags));
}

void fillMouseEvent(vwgl::app::MouseEvent & mouseEvent, vwgl::app::MainLoop * mainLoop, NSPoint point) {
	mouseEvent.setPos(vwgl::Vector2i(point.x, point.y));
	mouseEvent.mouse().setButtonMask(mainLoop->getMouse().buttonMask());
	mouseEvent.setDelta(vwgl::Vector2());
}

@interface CocoaView ()

@property (readwrite) NSTimeInterval lastInterval;

- (void)drawView;
- (void)releaseOpenGL:(NSNotification*)notification;
- (void)closeWindow:(NSNotification*)notification;

@end

@implementation CocoaView



- (id)initWithFrame:(NSRect)frameRect pixelFormat:(NSOpenGLPixelFormat *)format {
	self = [super initWithFrame:frameRect pixelFormat:format];
	if (self) {
		self.lastInterval = [[NSDate date] timeIntervalSince1970];
		NSOpenGLPixelFormatAttribute attrsLegacy[] =
		{
			NSOpenGLPFADoubleBuffer,
			NSOpenGLPFADepthSize, 24,
			0
		};
		NSOpenGLPixelFormatAttribute attrs[] =
		{
			NSOpenGLPFADoubleBuffer,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
			0
		};
		
		NSOpenGLPixelFormat * pf = nil;
		if (vwgl::Graphics::get()->getApi()==vwgl::Graphics::kApiOpenGL) {
			pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrsLegacy];
		}
		else {
			pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
		}
		
		if (!pf) {
			NSLog(@"No OpenGL pixel format");
		}
		
		NSOpenGLContext * context = [[NSOpenGLContext alloc] initWithFormat:pf shareContext:nil];
		
		[self setPixelFormat:pf];
		[self setOpenGLContext:context];
		[self setWantsBestResolutionOpenGLSurface:YES];
	}
	return self;
}

- (void)prepareOpenGL {
	[super prepareOpenGL];
	
	[[self openGLContext] makeCurrentContext];
	
	GLint swapInt = 1;
	[self.openGLContext setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	
	[NSTimer scheduledTimerWithTimeInterval:1.0/30.0 target:self selector:@selector(drawView) userInfo:Nil repeats:YES];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeWindow:) name:NSWindowWillCloseNotification object:[self window]];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(releaseOpenGL:) name:NSApplicationWillTerminateNotification object:[self window]];
	
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->initGL();
}

- (void)reshape
{
	[super reshape];
	[self.openGLContext makeCurrentContext];
	
	NSRect viewRectPoints = [self bounds];
	
#if SUPPORT_RETINA_RESOLUTION
	NSRect viewRectPixels = [self convertRectToBacking:viewRectPoints];
#else
	NSRect viewRectPixels = viewRectPoints;
#endif
	
	if (self.cocoaWindow) {
		self.cocoaWindow->setSize(viewRectPoints.size.width, viewRectPoints.size.height);
		glViewport(0, 0, viewRectPixels.size.width, viewRectPixels.size.height);
		vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
		controller->reshape(viewRectPixels.size.width, viewRectPixels.size.height);
	}
}

- (void)renewGState
{
	[[self window] disableScreenUpdatesUntilFlush];
	
	[super renewGState];
}

- (void)drawRect:(NSRect)dirtyRect {
	[self.openGLContext makeCurrentContext];
	[self drawView];
}

- (void)drawView {
	[self.openGLContext makeCurrentContext];
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
	
	controller->frame(static_cast<float>(timeInMiliseconds - self.lastInterval));
	controller->draw();
	
	self.lastInterval = [[NSDate date] timeIntervalSince1970];
}

- (void)closeWindow:(NSNotification*)notification {
	[[NSApplication sharedApplication] terminate:self];
}

- (void)releaseOpenGL:(NSNotification*)notification {
	[self.openGLContext makeCurrentContext];
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	if (controller) {
		controller->destroy();
	}
	self.cocoaWindow->setWindowController(nullptr);
}

- (NSPoint)convertToEnginePoint:(NSEvent*)theEvent
{
	NSPoint clickPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	NSRect viewRect = self.frame;
	
#if SUPPORT_RETINA_RESOLUTION
	clickPoint = [self convertPointToBacking:clickPoint];
	viewRect = [self convertRectToBacking:viewRect];
#endif
	
	clickPoint.y = viewRect.size.height - clickPoint.y;
	return clickPoint;
}

- (void)mouseDown:(NSEvent*)theEvent
{
	NSPoint clickPoint = [self convertToEnginePoint:theEvent];
	
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	mainLoop->getMouse().setMouseDown(vwgl::Mouse::kLeftButton);

	vwgl::app::MouseEvent mouseEvent;
	fillMouseEvent(mouseEvent, mainLoop, clickPoint);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->mouseDown(mouseEvent);
}

- (void)mouseUp:(NSEvent*)theEvent
{
	NSPoint clickPoint = [self convertToEnginePoint:theEvent];

	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	mainLoop->getMouse().setMouseUp(vwgl::Mouse::kLeftButton);
	
	vwgl::app::MouseEvent mouseEvent;
	fillMouseEvent(mouseEvent, mainLoop, clickPoint);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->mouseUp(mouseEvent);
}

- (BOOL)isPointInWindow:(NSPoint)point {
	NSRect viewRectPoints = [self bounds];
	
#if SUPPORT_RETINA_RESOLUTION
	viewRectPoints = [self convertRectToBacking:viewRectPoints];
#endif

	return point.x>=0 && point.y>=0 && point.x<=viewRectPoints.size.width && point.y<=viewRectPoints.size.height;
}

- (void)mouseMoved:(NSEvent*)theEvent
{
	NSPoint movePoint = [self convertToEnginePoint:theEvent];
	if ([self isPointInWindow:movePoint]) {
		vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
		vwgl::app::MouseEvent mouseEvent;
		fillMouseEvent(mouseEvent, mainLoop, movePoint);
		vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
		controller->mouseMove(mouseEvent);
	}
}

- (void)mouseDragged:(NSEvent*)theEvent
{
	NSPoint movePoint = [self convertToEnginePoint:theEvent];
	if ([self isPointInWindow:movePoint]) {
		vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
		vwgl::app::MouseEvent mouseEvent;
		fillMouseEvent(mouseEvent, mainLoop, movePoint);
		vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
		controller->mouseMove(mouseEvent);
		controller->mouseDrag(mouseEvent);
	}
}

- (void)rightMouseDown:(NSEvent*)theEvent
{
	NSPoint clickPoint = [self convertToEnginePoint:theEvent];
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	mainLoop->getMouse().setMouseDown(vwgl::Mouse::kRightButton);
	
	vwgl::app::MouseEvent mouseEvent;
	fillMouseEvent(mouseEvent, mainLoop, clickPoint);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->mouseDown(mouseEvent);
}

- (void)rightMouseUp:(NSEvent*)theEvent
{
	NSPoint clickPoint = [self convertToEnginePoint:theEvent];
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	mainLoop->getMouse().setMouseUp(vwgl::Mouse::kRightButton);
	
	vwgl::app::MouseEvent mouseEvent;
	fillMouseEvent(mouseEvent, mainLoop, clickPoint);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->mouseUp(mouseEvent);
}

- (void)rightMouseDragged:(NSEvent*)theEvent
{
	NSPoint movePoint = [self convertToEnginePoint:theEvent];
	if ([self isPointInWindow:movePoint]) {
		vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
		vwgl::app::MouseEvent mouseEvent;
		fillMouseEvent(mouseEvent, mainLoop, movePoint);
		vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
		controller->mouseMove(mouseEvent);
		controller->mouseDrag(mouseEvent);
	}
}

- (void)otherMouseDown:(NSEvent*)theEvent
{
	NSPoint clickPoint = [self convertToEnginePoint:theEvent];
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	mainLoop->getMouse().setMouseDown(vwgl::Mouse::kMiddleButton);
	
	vwgl::app::MouseEvent mouseEvent;
	fillMouseEvent(mouseEvent, mainLoop, clickPoint);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->mouseDown(mouseEvent);
}

- (void)otherMouseUp:(NSEvent*)theEvent
{
	NSPoint clickPoint = [self convertToEnginePoint:theEvent];
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	mainLoop->getMouse().setMouseUp(vwgl::Mouse::kMiddleButton);
	
	vwgl::app::MouseEvent mouseEvent;
	fillMouseEvent(mouseEvent, mainLoop, clickPoint);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->mouseUp(mouseEvent);
}

- (void)otherMouseDragged:(NSEvent*)theEvent
{
	NSPoint movePoint = [self convertToEnginePoint:theEvent];
	if ([self isPointInWindow:movePoint]) {
		vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
		vwgl::app::MouseEvent mouseEvent;
		fillMouseEvent(mouseEvent, mainLoop, movePoint);
		vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
		controller->mouseMove(mouseEvent);
		controller->mouseDrag(mouseEvent);
	}
}

- (void)scrollWheel:(NSEvent*)theEvent
{
	NSPoint movePoint = [self convertToEnginePoint:theEvent];
	vwgl::app::MainLoop * mainLoop = vwgl::app::MainLoop::get();
	vwgl::app::MouseEvent mouseEvent;
	fillMouseEvent(mouseEvent, mainLoop, movePoint);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	float deltaX = [theEvent scrollingDeltaX];
	float deltaY = [theEvent scrollingDeltaY];
	deltaX = deltaX==0.0f ? 0.0f:deltaX>0 ? 1.0f:-1.0f;
	deltaY = deltaY==0.0f ? 0.0f:deltaY>0 ? 1.0f:-1.0f;
	mouseEvent.setDelta(vwgl::Vector2(deltaX,deltaY));
	controller->mouseWheel(mouseEvent);
}

- (void)keyDown:(NSEvent *)theEvent {
	vwgl::app::KeyboardEvent kbEvent;
	fillKeyboard(kbEvent.keyboard(), theEvent.characters, theEvent.keyCode, theEvent.modifierFlags);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->keyDown(kbEvent);
}

- (void)keyUp:(NSEvent *)theEvent {
	vwgl::app::KeyboardEvent kbEvent;
	fillKeyboard(kbEvent.keyboard(), theEvent.characters, theEvent.keyCode, theEvent.modifierFlags);
	vwgl::app::WindowController * controller = self.cocoaWindow->getWindowController();
	controller->keyUp(kbEvent);
}

@end
