
#include <vwgl/scenerenderer.hpp>

namespace vwgl {

SceneRenderer::SceneRenderer() :_renderTransparency(true), _renderOpaque(true) {
	_drawVisitor = new DrawNodeVisitor();
}

SceneRenderer::~SceneRenderer() {
}

void SceneRenderer::draw(Group * sceneRoot, bool renderAlpha) {
	if (renderAlpha) {
		if (_renderOpaque) {
			_drawVisitor->setRenderTransparent(false);
			_drawVisitor->setRenderOpaque(true);
			_drawVisitor->visit(sceneRoot);
		}
		
		if (_renderTransparency) {
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			_drawVisitor->setRenderTransparent(true);
			_drawVisitor->setRenderOpaque(false);
			_drawVisitor->visit(sceneRoot);
			glDisable(GL_BLEND);
		}
	}
	else {
		_drawVisitor->setRenderTransparent(true);
		_drawVisitor->setRenderOpaque(true);
		_drawVisitor->visit(sceneRoot);
	}
}

}
