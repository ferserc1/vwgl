
#include <vwgl/scene_writter.hpp>
#include <vwgl/system.hpp>
#include <vwgl/loader.hpp>


namespace vwgl {

const int SceneWritter::s_majorVersion = 2;
const int SceneWritter::s_minorVersion = 0;
const int SceneWritter::s_revision = 0;
	
SceneWritter::SceneWritter() {
	
}

SceneWritter::~SceneWritter() {
	
}

bool SceneWritter::acceptFileType(const std::string &path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext=="vitscn" || ext=="vitbundle";
}

bool SceneWritter::writeScene(const std::string &path, scene::Node *node) {
	try {
		std::string tmpPath = vwgl::System::get()->getTempPath();
		std::string fileName = vwgl::System::get()->getLastPathComponent(path);
		tmpPath = vwgl::System::get()->addPathComponent(tmpPath, fileName);
		makeContainer(tmpPath);
		//makeContainer(path);
		//_folderPath = path;
		_folderPath = tmpPath;

		openFile(System::get()->addPathComponent(tmpPath, "scene.vitscnj"));
		_serializer.writeKey("scene");
		_serializer.openArray(false, true);
			_isLastNode = true;
			writeNode(node);
		_serializer.closeArray(true, false, true);
		closeFile();
		
		std::string ext;
		getFileExtension(path, ext);
		if (ext=="vitbundle") {
			if (vwgl::System::get()->directoryExists(path)) {
				vwgl::System::get()->removeFile(path);
			}
			vwgl::System::get()->cp(tmpPath, path, true);
			vwgl::System::get()->removeDirectory(tmpPath);
		}
		else if (ext == "vitscn") {
			if (vwgl::System::get()->fileExists(path)) {
				vwgl::System::get()->removeFile(path);
			}
			std::vector<std::string> allFiles;
			std::vector<std::string>::iterator it;
			vwgl::System::get()->listFilesRecursive(tmpPath, allFiles);
			vwgl::System::get()->packFiles(tmpPath, allFiles, path);
		}
	}
	catch (FileException & e) {
		std::cerr << "Error: " << e.what() << std::endl;
		return false;
	}
	return true;
}
	
void SceneWritter::writeNode(scene::Node * node) {
	if (node) {
		_serializer.openObject(true, true);
		_serializer.writeProp<std::string>("type", "Node", true, true);
		_serializer.writeProp<std::string>("name", node->getName(), true, true);
		_serializer.writeProp("enabled", node->isEnabled(), true, true);
		
		scene::Node * parent = node->parent();
		bool isLastNode = false;
		if (parent==nullptr || (parent && parent->children().back()==node)) {
			isLastNode = true;
		}
		
		_serializer.writeKey("children");
		_serializer.openArray(false, true);
		node->iterateChildren([&] (scene::Node * child) {
			writeNode(child);
		});
		_serializer.closeArray(true, true, true);
		_serializer.writeKey("components");
		_serializer.openArray(false,true);
		size_t numberOfComponents = node->componentMap().size();
		size_t serializedComponents = 0;
		node->eachComponent([&](scene::Component * component) {
			++serializedComponents;
			writeComponent(component,serializedComponents==numberOfComponents);
		});
		_serializer.closeArray(true, false, true);
		_isLastNode = isLastNode;
		_serializer.closeObject(true, !isLastNode, true);
	}
}
	
void SceneWritter::writeComponent(scene::Component * component, bool lastItem) {
	if (component && !component->getIgnoreSerialize()) {
		component->serialize(_serializer, lastItem);
		component->saveResourcesToPath(_folderPath);
	}
}

void SceneWritter::makeContainer(const std::string & path) {
	if (System::get()->directoryExists(path)) {
		System::get()->removeDirectory(path);
	}
	if (!System::get()->mkdir(path)) {
		throw SceneContainerException();
	}
}
	
void SceneWritter::openFile(const std::string &path) {
	_file.open(path);
	if (!_file.is_open()) {
		throw SceneFileException();
	}
	_serializer.setOutputStream(_file);
	writeHeader();
}

void SceneWritter::writeHeader() {
	_serializer.openObject(true, true);
	_serializer.writeProp<std::string>("fileType","vwgl::scene",0,true,true);
	_serializer.writeKey("version");
	_serializer.openObject(false, false);
		_serializer.writeProp("major",SceneWritter::s_majorVersion,0,true,false);
		_serializer.writeProp("minor",SceneWritter::s_minorVersion,0,true,false);
		_serializer.writeProp("rev",SceneWritter::s_revision,0,false,false);
	_serializer.closeObject(false, true, true);
}

void SceneWritter::closeFile() {
	_serializer.closeObject(true, false, true);
	_file.close();
}

}