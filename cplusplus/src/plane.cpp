
#include <vwgl/plane.hpp>
#include <iostream>

namespace vwgl {

Plane::Plane() :Drawable(), _rows(1), _columns(1) {
	buildPlane(1.0f, 1.0f);
}

Plane::Plane(float side, int rows, int columns) :Drawable(), _rows(rows), _columns(columns) {
	buildPlane(side, side);
}

Plane::Plane(float width, float depth, int rows, int columns) :Drawable(), _rows(rows), _columns(columns) {
	buildPlane(width, depth);
}

Plane::~Plane() {
	
}

void Plane::buildPlane(float width, float height) {
	float x = width / 2.0f;
	float y = height / 2.0f;

	float v[] = {
		-x, 0.000000, -y,
		x, 0.000000, -y,
		x, 0.000000, y,
		x, 0.000000, y,
		-x, 0.000000, y,
		-x, 0.000000, -y
	};

	float n[] = {
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000
	};

	float t0[] = {
		0.000000, 0.000000,
		1.000000, 0.000000,
		1.000000, 1.000000,
		1.000000, 1.000000,
		0.000000, 1.000000,
		0.000000, 0.000000
	};

	float t1[] = {
		0.000000, 0.000000,
		1.000000, 0.000000,
		1.000000, 1.000000,
		1.000000, 1.000000,
		0.000000, 1.000000,
		0.000000, 0.000000
	};

	unsigned int i[] = {
		2, 1, 0, 5, 4, 3
	};

	int iSize = 3 * 2;	// 2 triangles per face, 3 vertex per triangle
	int vSize = iSize * 3;	// 3 elements (x.y.z) for each index
	int tSize = iSize * 2;	// 2 elements (u, v) for each index

	PolyList * plist = new PolyList();
	plist->addVertexVector(v, vSize);
	plist->addNormalVector(n, vSize);
	plist->addTexCoord0Vector(t0, tSize);
	plist->addTexCoord1Vector(t1, tSize);
	plist->addIndexVector(i, iSize);

	plist->buildPolyList();

	addSolid(new Solid(plist));

	/*
	float x = width / 2.0f;
	float y = height / 2.0f;
	
	PolyList * plist = new PolyList();
	
	
	float x0 = 0.0f;//-x;
	float y0 = 0.0f;//-y;
	float incX = width / static_cast<float>(_columns);
	float incY = height / static_cast<float>(_rows);
	
	for (int i=0; i<=_columns; ++i) {
		for (int j=0; j<=_rows; ++j) {
			plist->addVertex(Vector3(x0 - x,0.0f,y0 - y));
			plist->addNormal(Vector3(0.0f,1.0f,0.0f));
			plist->addTexCoord0(Vector2(x0/width,y0/height));
			plist->addTexCoord1(Vector2(x0/width,y0/height));
			x0 += incX;
			
			if (j<_rows && i<_columns) {
				plist->addIndex(i + j * (_columns + 1));
				plist->addIndex(i + (j+1) * (_columns + 1));
				plist->addIndex(i + 1 + (j+1) * (_columns + 1));
				plist->addIndex(i + 1 + j * (_columns + 1));
			}
		}
		y0 += incY;
		x0 = 0.0f;//-x;
	}
	
	plist->buildPolyList();
	plist->setDrawMode(PolyList::kQuads);
	
	addSolid(new vwgl::Solid(plist));
	*/
}
	
	

}
