
#include <vwgl/dictionary_writter.hpp>
#include <vwgl/system.hpp>
#include <fstream>

namespace vwgl {

void DictionaryWritter::tab(int tabs) {
	for (int i=0; i<tabs; ++i) {
		(*_out) << "\t";
	}
}

void DictionaryWritter::printArray(Dictionary * obj,int tabs) {
	if (!obj) return;
	Dictionary::DictionaryArray::iterator it;
	(*_out) << "[";
	for (it=obj->getArray().begin(); it!=obj->getArray().end(); ++it) {
		if (it!=obj->getArray().begin()) (*_out) << ",";
		(*_out) << std::endl;
		Dictionary * value = (*it).getPtr();
		tab(tabs);
		printObject(value, tabs);
	}
	(*_out) << std::endl;
	tab(tabs - 1);
	(*_out) << "]";
}

void DictionaryWritter::printObjectMap(Dictionary * obj, int tabs) {
	if (!obj) return;
	Dictionary::DictionaryMap::iterator it;
	(*_out) << "{";
	for (it=obj->getObject().begin(); it!=obj->getObject().end(); ++it) {
		if (it!=obj->getObject().begin()) (*_out) << ",";
		(*_out) << std::endl;
		std::string key = it->first;
		Dictionary * value = it->second.getPtr();
		tab(tabs + 1);
		(*_out) << '"' << key << '"' << ":";
		printObject(value,tabs + 1);
	}

	(*_out) << std::endl;
	tab(tabs);
	(*_out) << "}";
}

void DictionaryWritter::printObject(Dictionary * value,int tabs) {
	if (!value) {
		(*_out) << "0";
	}
	else {
		switch (value->getType()) {
		case Dictionary::kTypeString:
			(*_out) << '"' << value->getString() << '"';
			break;
		case Dictionary::kTypeBool:
			(*_out) << (value->getBoolean() ? "true" : "false");
			break;
		case Dictionary::kTypeNumber:
			(*_out) << value->getNumber();
			break;
		case Dictionary::kTypeArray:
			printArray(value, tabs + 1);
			break;
		case Dictionary::kTypeObject:
			printObjectMap(value, tabs);
			break;
		default:
			break;
		}
	}
}

bool DictionaryWritter::acceptFileType(const std::string & path){
	return vwgl::System::get()->getExtension(path)=="json";
}

bool DictionaryWritter::writeDictionary(const std::string & path, Dictionary * dict) {
	std::ofstream file;
	file.open(path);
	
	if (file.is_open()) {
		_out = &file;
		printObject(dict);
		return true;
	}
	else {
		_out = &std::cout;
		return false;
	}
}
	
}