
#include <vwgl/cmd/light_commands.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace cmd {

void ApplyLightModifierCommand::doCommand() {
	if (_lights.size()==0) {
		throw std::runtime_error("Could not execute ApplyLightModifierCommand: emtpy light list");
	}
	else {
		_backup.clear();
		eachLight([&](scene::Light * light) {
			_backup.push_back(light->getModifierWithMask(scene::LightModifier::kModifierAll));
			light->applyModifier(_modifier);
		});
	}
}

void ApplyLightModifierCommand::undoCommand() {
	ModifierVector::iterator it = _backup.begin();
	eachLight([&](scene::Light * light) {
		light->applyModifier(*it);
		++it;
	});
}
	
}
}