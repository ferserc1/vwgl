
#include <vwgl/cmd/polylist_commands.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace cmd {

void PListFlipFacesCommand::doCommand() {
	if (_plistVector.size()==0) throw std::runtime_error("Error executing PListFlipFacesCommand: Empty polyList vector found");
	eachPlist([](PolyList * plist) {
		plist->flipFaces();
	});
}
	
void PListFlipNormalsCommand::doCommand() {
	if (_plistVector.size()==0) throw std::runtime_error("Error executing PListFlipNormalsCommand: Empty polyList vector found");
	eachPlist([](PolyList * plist) {
		plist->flipNormals();
	});
}

void PListSetNameCommand::doCommand() {
	if (_plistVector.size()==0) throw std::runtime_error("Error executing PListSetNameCommand: Empty polyList vector found");
	_undoNames.clear();
	eachPlist([&](PolyList * plist) {
		_undoNames.push_back(plist->getName());
		plist->setName(_name);
	});
}

void PListSetNameCommand::undoCommand() {
	std::vector<std::string>::iterator it = _undoNames.begin();
	eachPlist([&](PolyList * plist) {
		plist->setName(*it);
		++it;
	});
}

void PListSetGroupNameCommand::doCommand() {
	if (_plistVector.size()==0) throw std::runtime_error("Error executing PListSetGroupNameCommand: Empty polyList vector found");
	_undoNames.clear();
	eachPlist([&](PolyList * plist) {
		_undoNames.push_back(plist->getGroupName());
		plist->setGroupName(_name);
	});
}

void PListSetGroupNameCommand::undoCommand() {
	std::vector<std::string>::iterator it = _undoNames.begin();
	eachPlist([&](PolyList * plist) {
		plist->setGroupName(*it);
		++it;
	});
}

void PListSetVisibleCommand::doCommand() {
	if (_plistVector.size()==0) throw std::runtime_error("Error executing PListSetVisibleCommand: Empty polyList vector found");
	_undoVisible.clear();
	eachPlist([&](PolyList * plist) {
		_undoVisible.push_back(plist->isVisible());
		plist->setVisible(_visible);
	});
}

void PListSetVisibleCommand::undoCommand() {
	std::vector<bool>::iterator it = _undoVisible	.begin();
	eachPlist([&](PolyList * plist) {
		plist->setVisible(*it);
		++it;
	});
}

void PListSwapUVsCommand::doCommand() {
	if (_plistVector.size() == 0) throw std::runtime_error("Error executing PListSwapUVsCommand: empty polyList vector found");
	eachPlist([&](PolyList * plist) {
		plist->switchUVs(_uvFrom, _uvTo);
	});
}

void PListSwapUVsCommand::undoCommand() {
	doCommand();
}

void PListDeleteCommand::doCommand() {
	if (_plistVector.size()==0) throw std::runtime_error("Error executing PListDeleteCommand: empty polyList vector found");
	if (_drawableVector.size()==0) throw std::runtime_error("Error executing PListDeleteCommand: empty parent drawable list found");
	
	if (!_initialized) {
		eachPlist([&](PolyList * plist) {
			std::vector<DrawablePolyList>::iterator it;
			for (it=_drawableVector.begin(); it!=_drawableVector.end(); ++it) {
				(*it).tryAddPolyList(plist);
			}
		});
		_initialized = true;
	}
	std::vector<DrawablePolyList>::iterator it;
	for (it=_drawableVector.begin(); it!=_drawableVector.end(); ++it) {
		(*it).removePlist();
	}
	
}

void PListDeleteCommand::undoCommand() {
	std::vector<DrawablePolyList>::iterator it;
	for (it=_drawableVector.begin(); it!=_drawableVector.end(); ++it) {
		(*it).undoRemovePlist();
	}
}

}
}