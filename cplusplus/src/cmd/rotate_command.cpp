
#include <vwgl/cmd/rotate_command.hpp>

namespace vwgl {
namespace cmd {

void RotateCommand::doCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("RotateCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		_undoRotate.set(strategy->getRotateX(), strategy->getRotateY(), strategy->getRotateZ());
		strategy->rotateX(_undoRotate.x() + _rotate.x());
		strategy->rotateY(_undoRotate.y() + _rotate.y());
		strategy->rotateZ(_undoRotate.z() + _rotate.z());
	}
	else {
		_undoMatrix = _trx->getTransform().getMatrix();
		_trx->getTransform().getMatrix()
				.rotate(_rotate.x(), 1.0f, 0.0f, 0.0f)
				.rotate(_rotate.y(), 0.0f, 1.0f, 0.0f)
				.rotate(_rotate.z(), 0.0f, 0.0f, 1.0f);
	}
}

void RotateCommand::undoCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("RotateCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		strategy->rotateX(_undoRotate.x());
		strategy->rotateY(_undoRotate.y());
		strategy->rotateZ(_undoRotate.z());
	}
	else {
		_trx->getTransform().setMatrix(_undoMatrix);
	}
}

	
	
void SetRotateCommand::doCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("RotateCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		_undoRotate.set(strategy->getRotateX(), strategy->getRotateY(), strategy->getRotateZ());
		strategy->rotateX(_rotate.x());
		strategy->rotateY(_rotate.y());
		strategy->rotateZ(_rotate.z());
	}
	else {
		_undoMatrix = _trx->getTransform().getMatrix();
		_trx->getTransform().getMatrix().resetRotation();
		_trx->getTransform().getMatrix()
			.rotate(_rotate.x(), 1.0f, 0.0f, 0.0f)
			.rotate(_rotate.y(), 0.0f, 1.0f, 0.0f)
			.rotate(_rotate.z(), 0.0f, 0.0f, 1.0f);
	}
}

}
}