
#include <vwgl/cmd/joint_command.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace cmd {
	
void EditJointCommand::doCommand() {
	if (_chainJointList.size()==0) throw std::runtime_error("Could not execute EditJointCommand: empty joint list");
	std::vector<ptr<scene::ChainJoint> >::iterator it = _chainJointList.begin();
	
	for (; it!=_chainJointList.end(); ++it) {
		(*it)->getJoint()->setOffset(_offset);
		(*it)->getJoint()->setEulerRotation(_rotation);
	}
}
	
void EditJointCommand::undoCommand() {
	std::vector<ptr<scene::ChainJoint> >::iterator it = _chainJointList.begin();
	std::vector<Vector3>::iterator o_it = _positionList.begin();
	std::vector<Vector3>::iterator e_it = _rotationList.begin();
	
	for (; it!=_chainJointList.end(); ++it, ++o_it, ++e_it) {
		(*it)->getJoint()->setOffset(*o_it);
		(*it)->getJoint()->setEulerRotation(*e_it);
	}
}
	
}
}
