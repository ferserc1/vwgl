
#include <vwgl/cmd/scene_commands.hpp>

namespace vwgl {
namespace cmd {
	
void AddNodeCommand::doCommand() {
	if (!_child.valid() || !_newParent.valid()) throw std::runtime_error("Error executing command: child or parent node not valid");

	if (!_newParent->addChild(_child.getPtr())) {
		throw std::runtime_error("Could not add child");
	}
}
	
void AddNodeCommand::undoCommand() {
	if (_prevParent.valid()) {
		_prevParent->addChild(_child.getPtr());
	}
	else {
		_newParent->removeChild(_child.getPtr());
	}
}
	
}
}
