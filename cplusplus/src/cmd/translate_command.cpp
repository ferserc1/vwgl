
#include <vwgl/cmd/translate_command.hpp>

namespace vwgl {
namespace cmd {

void TranslateCommand::doCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("TranslateCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		Vector3 translateVector = strategy->getTranslate() + _translate;
		strategy->translate(translateVector);
	}
	else {
		_trx->getTransform().getMatrix().translate(_translate);
	}
}
	
void TranslateCommand::undoCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("TranslateCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		Vector3 translateVector = strategy->getTranslate() - _translate;
		strategy->translate(translateVector);
	}
	else {
		_trx->getTransform().getMatrix().translate(-_translate.x(),-_translate.y(),-_translate.z());
	}
}
	
void SetTranslateCommand::doCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("SetTranslateCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		_translateBkp = strategy->getTranslate();
		Vector3 translateVector = _translate;
		strategy->translate(translateVector);
	}
	else {
		Matrix4 transform = _trx->getTransform().getMatrix();
		_translateBkp.set(transform.get30(),transform.get31(),transform.get32());
		_trx->getTransform().getMatrix().setRow(3,
											Vector4(
												_translate.x(),
												_translate.y(),
												_translate.y(), transform.get33()
											));
	}
}
	
void SetTranslateCommand::undoCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("SetTranslateCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		strategy->translate(_translateBkp);
	}
	else {
		Matrix4 transform = _trx->getTransform().getMatrix();
		_trx->getTransform().getMatrix().setRow(3, Vector4(
												_translateBkp.x(),
												_translateBkp.x(),
												_translateBkp.x(),
												transform.get33()
											));
	}
}

}
}
