
#include <vwgl/cmd/scale_command.hpp>

namespace vwgl {
namespace cmd {

void ScaleCommand::doCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("ScaleCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		_undoScale = strategy->getScale();
		Vector3 scaleVector = strategy->getScale();
		scaleVector.set(scaleVector.x() * _scale.x(), scaleVector.y() * _scale.y(), scaleVector.z() * _scale.z());
		strategy->scale(scaleVector);
	}
	else {
		_undoMatrix = _trx->getTransform().getMatrix();
		_trx->getTransform().getMatrix().scale(_scale);
	}
}

void ScaleCommand::undoCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("ScaleCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		strategy->scale(_undoScale);
	}
	else {
		_trx->getTransform().setMatrix(_undoMatrix);
	}
}

	
void SetScaleCommand::doCommand() {
	if (!_trx.valid()) {
		throw std::runtime_error("ScaleCommand: Invalid target specified");
	}
	vwgl::TRSTransformStrategy * strategy = _trx->getTransform().getTransformStrategy<TRSTransformStrategy>();
	if (strategy) {
		_undoScale = strategy->getScale();
		strategy->scale(_scale);
	}
	else {
		_undoMatrix = _trx->getTransform().getMatrix();
		_trx->getTransform().getMatrix().setScale(_scale);
	}
}

}
}