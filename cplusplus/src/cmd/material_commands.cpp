
#include <vwgl/cmd/material_commands.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace cmd {

void ApplyMaterialModifierCommand::doCommand() {
	if (_materials.size()==0) {
		throw std::runtime_error("Could not execute ApplyMaterialModifierCommand: emtpy material list");
	}
	else {
		_materialBackup.clear();
		std::string path = System::get()->getResourcesPath();
		eachMaterial([&](GenericMaterial * mat) {
			_materialBackup.push_back(mat->getModifierWithMask(MaterialModifier::kModifierAll));
			mat->applyModifier(_modifier, path, false);
		});
	}
}

void ApplyMaterialModifierCommand::undoCommand() {
	ModifierVector::iterator it = _materialBackup.begin();
	std::string path = System::get()->getResourcesPath();
	eachMaterial([&](GenericMaterial * mat) {
		mat->applyModifier(*it, path, false);
		++it;
	});
}

}
}