
#include <vwgl/cmd/delete_command.hpp>

namespace vwgl {
namespace cmd {
	
DeleteCommand::DeleteCommand(plain_ptr ctx)
	:ContextCommand(ctx)
{
}
	
void DeleteCommand::doCommand() {
	if (_children.size()==0) throw std::runtime_error("Error executing DeleteCommand: nothing to delete");
	
	std::vector<ptr<scene::Node> >::iterator pit;
	std::vector<ptr<scene::Node> >::iterator cit;
	for (pit=_parents.begin(), cit=_children.begin(); pit!=_parents.end(); ++pit, ++cit) {
		(*pit)->removeChild((*cit).getPtr());
		(*cit)->setDisabled();
	}
}
	
void DeleteCommand::undoCommand() {
	if (_children.size()==0) throw std::runtime_error("Error undoing DeleteCommand: nothing to restore");
	
	std::vector<ptr<scene::Node> >::iterator pit;
	std::vector<ptr<scene::Node> >::iterator cit;
	for (pit=_parents.begin(), cit=_children.begin(); pit!=_parents.end(); ++pit, ++cit) {
		(*pit)->addChild((*cit).getPtr());
		(*cit)->setEnabled();
	}
}
	
}
}
