
#include <vwgl/cmd/export_command.hpp>
#include <vwgl/system.hpp>
#include <vwgl/loader.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace cmd {

void ExportCommand::doCommand() {
	if (targetSize()==0) {
		throw std::runtime_error("Could not execute ExportCommand: no targets defined");
	}
	else if (!System::get()->directoryExists(_destinationPath)) {
		throw std::runtime_error("Could not execute ExportCommand: the specified path does not exists");
	}
	else {
		eachDrawable([&](scene::Drawable * drw) {
			std::string fileName = drw->getName();
			if (fileName == "") {
				fileName = "untitled";
			}
			std::string fullPath = System::get()->addPathComponent(_destinationPath, fileName);
			fullPath = System::get()->getUniqueFileName(fullPath);
			System::get()->mkdir(fullPath);
			fullPath = System::get()->addPathComponent(fullPath, fileName);
			fullPath = System::get()->addExtension(fullPath, "vwglb");
			if (drw->node()) {
				std::cout << "Export prefab to " << fullPath << std::endl;
				Loader::get()->write(fullPath, drw->node());
			}
			else {
				std::cout << "Export model file to " << fullPath << std::endl;
				Loader::get()->write(fullPath, drw);
			}
		});
	}
}

}
}