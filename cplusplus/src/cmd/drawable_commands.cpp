
#include <vwgl/cmd/drawable_commands.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/boundingbox.hpp>
#include <vwgl/material.hpp>
#include <vwgl/polylist.hpp>
#include <vwgl/generic_material.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace cmd {
	
void DrwSetNameCommand::doCommand() {
	if (targetSize()==0) {
		throw std::runtime_error("Could not execute SwitchUVsCommand: no targets defined");
	}
	else {
		_prevNames.clear();
		eachDrawable([&](scene::Drawable * drw) {
			_prevNames.push_back(drw->getName());
			drw->setName(_name);
		});
	}
}
	
void DrwSetNameCommand::undoCommand() {
	std::vector<std::string>::iterator it = _prevNames.begin();
	eachDrawable([&](scene::Drawable * drw) {
		drw->setName(*it);
		++it;
	});
}

void DrwSwitchUVsCommand::doCommand() {
	if (_from==_to) {
		throw std::runtime_error("Could not execute SwitchUVsCommand: UV's are identical");
	}
	else  if (targetSize()==0) {
		throw std::runtime_error("Could not execute SwitchUVsCommand: no targets defined");
	}
	else {
		eachDrawable([&](scene::Drawable * drw) {
			drw->switchUVs(_from, _to);
		});
	}
}

void DrwSwitchUVsCommand::undoCommand() {
	eachDrawable([&](scene::Drawable * drw) {
		drw->switchUVs(_from, _to);
	});
}

void DrwFlipNormalsCommand::doCommand() {
	if (targetSize()==0) {
		throw std::runtime_error("Could not execute FlipNormalsCommand: no targets defined");
	}
	else {
		eachDrawable([&](scene::Drawable * drw) {
			drw->flipNormals();
		});
	}
}

void DrwFlipNormalsCommand::undoCommand() {
	eachDrawable([&](scene::Drawable * drw) {
		drw->flipNormals();
	});
}

void DrwFlipFacesCommand::doCommand() {
	if (targetSize()==0) {
		throw std::runtime_error("Could not execute FlipNormalsCommand: no targets defined");
	}
	else {
		eachDrawable([&](scene::Drawable * drw) {
			drw->flipFaces();
		});
	}
}

void DrwFlipFacesCommand::undoCommand() {
	eachDrawable([&](scene::Drawable * drw) {
		drw->flipFaces();
	});
}


void DrawableTransformCommand::pushTransform(scene::Drawable * drw) {
	if (drw->transform()) {
		vwgl::TransformStrategy * strategy = drw->transform()->getTransform().getTransformStrategy();
		if (strategy) {
			strategy = strategy->clone();
		}
		_strategyVector.push_back(strategy);
		_matrixVector.push_back(drw->transform()->getTransform().getMatrix());
	}
}

void DrawableTransformCommand::restoreTransform(scene::Drawable * drw) {
	if (drw->transform()) {
		ptr<vwgl::TransformStrategy> strategy = _strategyVector.back();
		Matrix4 matrix = _matrixVector.back();
		_strategyVector.pop_back();
		_matrixVector.pop_back();
		if (strategy.valid()) {
			drw->transform()->getTransform().setTransformStrategy(strategy.getPtr());
		}
		else {
			drw->transform()->getTransform().setMatrix(matrix);
		}
	}
}

void DrawableTransformCommand::beginDoCommand() {
	_strategyVector.clear();
	_matrixVector.clear();
}

	
void DrwFreezeTransformCommand::doCommand() {
	if (targetSize()==0) {
		throw std::runtime_error("Could not execute DrwFreezeTransformCommand: no targets defined");
	}
	else {
		beginDoCommand();
		eachDrawable([&](scene::Drawable * drw) {
			pushTransform(drw);
			if (drw->transform()) {
				Matrix4 mat = drw->transform()->getTransform().getMatrix();
				drw->applyTransform(mat);
				drw->transform()->identity();
			}
		});
	}
}
	
void DrwFreezeTransformCommand::undoCommand() {
	eachDrawable([&](scene::Drawable * drw) {
		restoreTransform(drw);
		if (drw->transform()) {
			Matrix4 mat = drw->transform()->getTransform().getMatrix();
			mat.invert();
			drw->applyTransform(mat);
		}
	});
}
	
void DrwMoveToCenterCommand::doCommand() {
	if (targetSize()==0) {
		throw std::runtime_error("Could not execute DrwMoveToCenterCommand: no targets defined");
	}
	else {
		beginDoCommand();
		eachDrawable([&](scene::Drawable * drw) {
			pushTransform(drw);
			if (drw->transform()) {
				ptr<BoundingBox> bbox = new BoundingBox(drw);
				Vector3 center = bbox->center();
				TRSTransformStrategy * trs = drw->transform()->getTransform().getTransformStrategy<TRSTransformStrategy>();
				PolarTransformStrategy * polar = drw->transform()->getTransform().getTransformStrategy<PolarTransformStrategy>();
				center.scale(-1.0f);
				if (trs) {
					trs->translate(center);
				}
				else if (polar) {
					polar->setOrigin(center);
				}
				else {
					drw->transform()->getTransform().getMatrix().setRow(3, Vector4(center.x(),
																				   center.y(),
																				   center.z(),
																				   1.0f));
				}
			}
		});
	}
}

void DrwMoveToCenterCommand::undoCommand() {
	eachDrawable([&](scene::Drawable * drw) {
		restoreTransform(drw);
	});
}

void DrwPutOnFloorCommand::doCommand() {
	if (targetSize()==0) {
		throw std::runtime_error("Could not execute DrwPutOnFloorCommand: no targets defined");
	}
	else {
		beginDoCommand();
		eachDrawable([&](scene::Drawable * drw) {
			pushTransform(drw);
			if (drw->transform()) {
				ptr<BoundingBox> bbox = new BoundingBox(drw);
				Vector3 center = bbox->center();
				TRSTransformStrategy * trs = drw->transform()->getTransform().getTransformStrategy<TRSTransformStrategy>();
				PolarTransformStrategy * polar = drw->transform()->getTransform().getTransformStrategy<PolarTransformStrategy>();
				center.scale(-1.0f);
				center.y(center.y() + bbox->halfSize().y());
				if (trs) {
					Vector3 translate = trs->getTranslate();
					translate.y(center.y());
					trs->translate(translate);
				}
				else if (polar) {
					Vector3 origin = polar->getOrigin();
					origin.y(center.y());
					polar->setOrigin(origin);
				}
				else {
					drw->transform()->getTransform().getMatrix().set31(center.y());
				}
			}
		});
	}
}

void DrwPutOnFloorCommand::undoCommand() {
	eachDrawable([&](scene::Drawable * drw) {
		restoreTransform(drw);
	});
}

void DrwReplaceGeometryCommand::doCommand() {
	if (targetSize()==0) {
		throw std::runtime_error("Could not execute DrwReplaceGeometryCommand: no targets defined");
	}
	else if (!_newDrawable.valid()) {
		throw std::runtime_error("Could not execute DrwReplaceGeometryCommand: the new drawable is null");
	}
	else {
		_parentNodes.clear();
		_oldDrawables.clear();
		eachDrawable([&](scene::Drawable * drw) {
			scene::Node * n = drw->node();
			if (n) {
				_parentNodes.push_back(n);
				_oldDrawables.push_back(drw);
				n->removeComponent(drw);
			}
		});
		
		scene::Node::NodeVector::iterator it;
		for (it=_parentNodes.begin(); it!=_parentNodes.end(); ++it) {
			scene::Node * n = (*it).getPtr();
			ptr<scene::Drawable> drwClone = new scene::Drawable();
			drwClone->setName(_newDrawable->getName());
			_newDrawable->eachElement([&](vwgl::PolyList * plist, vwgl::Material * mat, vwgl::Transform &) {
				GenericMaterial * newMat = new GenericMaterial();
				PolyList * newPlist = new PolyList(plist);
				if (dynamic_cast<GenericMaterial*>(mat)) {
					MaterialModifier mod = dynamic_cast<GenericMaterial*>(mat)->getModifierWithMask(MaterialModifier::kModifierAll);
					newMat->applyModifier(mod, "");
				}
				drwClone->addPolyList(newPlist, newMat);
			});
			n->addComponent(drwClone.getPtr());
		}
	}
}
	
void DrwReplaceGeometryCommand::undoCommand() {
	scene::Node::NodeVector::iterator it;
	std::vector<ptr<scene::Drawable> >::iterator dit;
	
	for (it=_parentNodes.begin(), dit=_oldDrawables.begin();
		 it!=_parentNodes.end(); ++it, ++dit) {
		scene::Node * n = (*it).getPtr();
		scene::Drawable * drw = (*dit).getPtr();
		scene::Drawable * oldDrw = n->getComponent<scene::Drawable>();
		n->removeComponent(oldDrw);
		n->addComponent(drw);
	}
}

	
}
}