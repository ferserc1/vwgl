
#include <vwgl/cmd/import_command.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/transform.hpp>

namespace vwgl {
namespace cmd {

ImportCommand::ImportCommand(plain_ptr ctx, const std::string & path, vwgl::scene::Node * parent, const Vector3 & position, bool loadPrefab)
	:ContextCommand(ctx)
	,_path(path)
	,_parent(parent)
	, _position(position)
	, _loadPrefab(loadPrefab)
{

}

void ImportCommand::doCommand() {
	if (_importNode.valid()) {
		// This is a redo command: _importNode is already loaded
		_parent->addChild(_importNode.getPtr());
	}
	else {
		if (_loadPrefab) {
			_importNode = Loader::get()->loadPrefab(_path);
		}
		else {
			scene::Drawable * drw = Loader::get()->loadModel(_path);
			if (drw) {
				_importNode = new vwgl::scene::Node(System::get()->getFileName(_path));
				_importNode->addComponent(drw);
			}
		}

		if (_importNode.valid()) {
			TRSTransformStrategy * strategy = new TRSTransformStrategy();
			strategy->translate(_position);
			_importNode->addComponent(new scene::Transform(strategy));
			_parent->addChild(_importNode.getPtr());
		}
		else {
			throw std::runtime_error("Error importing object");
		}
	}
}

void ImportCommand::undoCommand() {
	if (_parent.valid() && _importNode.valid()) {
		_parent->removeChild(_importNode.getPtr());
	}
}

}
}