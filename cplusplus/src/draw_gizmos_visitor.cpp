
#include <vwgl/draw_gizmos_visitor.hpp>
#include <vwgl/group.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/state.hpp>
#include <vwgl/light.hpp>
#include <vwgl/color_pick_material.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/system.hpp>

namespace vwgl {
	

DrawGizmosVisitor::DrawGizmosVisitor() :_featureMask { kPhysicJoints | kPointLights | kDirectionalLights | kSpotLights }, _mat(nullptr) {
}
	
DrawGizmosVisitor::~DrawGizmosVisitor() {
}

void DrawGizmosVisitor::visit(vwgl::Node *node) {
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	
	Group * gn = dynamic_cast<Group*>(node);
	TransformNode * tn = dynamic_cast<TransformNode*>(node);
	DrawableNode * dn = dynamic_cast<DrawableNode*>(node);
	Light * light = dynamic_cast<Light*>(node);
	Drawable * lightGizmo = getDrawable(light);
	ColorPickMaterial * mat = dynamic_cast<ColorPickMaterial*>(_mat);
	
	State::get()->pushModelMatrix();
	if (tn) {
		tn->prepareMatrix(State::get()->modelMatrix());
		State::get()->modelMatrix().mult(tn->getTransform());
	}
	if (gn) {
		NodeList::iterator it;
		for (it=gn->getNodeList().begin(); it!=gn->getNodeList().end(); ++it) {
			visit((*it).getPtr());
		}
	}
	if (dn && dynamic_cast<vwgl::Drawable*>(dn->getDrawable())) {
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);
		Drawable * drw = dynamic_cast<vwgl::Drawable*>(dn->getDrawable());
		
		if (isFeatureEnabled(kDrawableBBox) && bboxDrawable()) {
			State::get()->pushModelMatrix();
			BoundingBox * bbox = drw->boundingBox();
			Matrix4 trx;
			trx.identity();
			trx.translate(bbox->center().x(), bbox->center().y(), bbox->center().z());
			trx.scale(bbox->size().x(), bbox->size().y(), bbox->size().z());
			State::get()->modelMatrix().mult(trx);
			bboxDrawable()->draw();
			State::get()->popModelMatrix();
		}
		if (isFeatureEnabled(kPhysicJoints) && drw->getInputJoint() && drw->getInputJoint()->getDrawable()) {
			State::get()->pushModelMatrix();
			vwgl::Matrix4 inputTransform = vwgl::Matrix4::makeIdentity();
			drw->getInputJoint()->applyTransform(inputTransform);
			inputTransform.invert();
			State::get()->modelMatrix().mult(inputTransform);
			drw->getInputJoint()->getDrawable()->draw();
			State::get()->popModelMatrix();
		}
		if (isFeatureEnabled(kPhysicJoints) && drw->getOutputJoint() && drw->getOutputJoint()->getDrawable()) {
			State::get()->pushModelMatrix();
			vwgl::Matrix4 inputTransform = vwgl::Matrix4::makeIdentity();
			drw->getOutputJoint()->applyTransform(inputTransform);
			State::get()->modelMatrix().mult(inputTransform);
			drw->getOutputJoint()->getDrawable()->draw();
			State::get()->popModelMatrix();
		}
	}
	if (lightGizmo) {
		if (mat) {
			glEnable(GL_DEPTH_TEST);
			glDisable(GL_BLEND);
			SolidList solids = lightGizmo->getSolidList();
			SolidList::iterator it;
			for (it=solids.begin(); it!=solids.end(); ++it) {
				if (!light->getPickId().isZero()) {
					mat->setColorPickId(light->getPickId());
					mat->bindPolyList((*it)->getPolyList());
					mat->activate();
					(*it)->getPolyList()->drawElements();
					mat->deactivate();
				} 
			}
		}
		else {
			glEnable(GL_BLEND);
			glDisable(GL_DEPTH_TEST);
			lightGizmo->draw();
		}
	}
	State::get()->popModelMatrix();
	
	vwgl::Drawable * axis = nullptr;
	if (isFeatureEnabled(kAxis) && node->getParent()==nullptr && (axis=getAxis())) {
		State::get()->pushViewMatrix();
		State::get()->pushModelMatrix();
		State::get()->pushProjectionMatrix();
		
		float aspect = _viewport.aspectRatio() ;
		State::get()->projectionMatrix().ortho(0.0f, aspect, 0.0f, 1.0f, 0.1f, 100.0f);
		Matrix4 rotation = State::get()->viewMatrix().getRotation();
		State::get()->viewMatrix().identity();
		State::get()->modelMatrix()
			.identity()
			.translate(0.07f, 0.07f, -1.0f)
			.mult(rotation)
			.scale(0.1f, 0.1f, 0.1f);
		
		axis->draw();

		State::get()->popProjectionMatrix();
		State::get()->popModelMatrix();
		State::get()->popViewMatrix();
	}

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}

vwgl::Drawable * DrawGizmosVisitor::getAxis() {
	if (!_axisDrawable.valid()) {
		std::string resources = System::get()->getResourcesPath();
		_axisDrawable = Loader::get()->loadDrawable(System::get()->addPathComponent(resources, "move_gizmo.vwglb"));
	}
	return _axisDrawable.getPtr();
}

vwgl::Drawable * DrawGizmosVisitor::bboxDrawable() {
	if (!_boundingBoxDrawable.valid()) {
		Drawable * drw = new Drawable();
		ptr<PolyList> _polyList = new PolyList();
		
		_polyList->addVertex(Vector3(-0.5,-0.5,-0.5));		// 0
		_polyList->addVertex(Vector3( 0.5,-0.5,-0.5));		// 1
		_polyList->addVertex(Vector3( 0.5, 0.5,-0.5));		// 2
		_polyList->addVertex(Vector3(-0.5, 0.5,-0.5));		// 3
		_polyList->addVertex(Vector3(-0.5,-0.5, 0.5));		// 4
		_polyList->addVertex(Vector3( 0.5,-0.5, 0.5));		// 5
		_polyList->addVertex(Vector3( 0.5, 0.5, 0.5));		// 6
		_polyList->addVertex(Vector3(-0.5, 0.5, 0.5));		// 7
		
		_polyList->addNormal(Vector3(-0.5,-0.5,-0.5).normalize());
		_polyList->addNormal(Vector3( 0.5,-0.5,-0.5).normalize());
		_polyList->addNormal(Vector3( 0.5, 0.5,-0.5).normalize());
		_polyList->addNormal(Vector3(-0.5, 0.5,-0.5).normalize());
		_polyList->addNormal(Vector3(-0.5,-0.5, 0.5).normalize());
		_polyList->addNormal(Vector3( 0.5,-0.5, 0.5).normalize());
		_polyList->addNormal(Vector3( 0.5, 0.5, 0.5).normalize());
		_polyList->addNormal(Vector3(-0.5, 0.5, 0.5).normalize());
		
		_polyList->addTexCoord0(Vector2(0.0, 0.0));
		_polyList->addTexCoord0(Vector2(1.0, 0.0));
		_polyList->addTexCoord0(Vector2(1.0, 1.0));
		_polyList->addTexCoord0(Vector2(0.0, 1.0));
		_polyList->addTexCoord0(Vector2(0.0, 0.0));
		_polyList->addTexCoord0(Vector2(1.0, 0.0));
		_polyList->addTexCoord0(Vector2(1.0, 1.0));
		_polyList->addTexCoord0(Vector2(0.0, 1.0));
		
		unsigned int index[] = {
			0, 1,	1, 2,	2, 3,	3, 0,
			4, 5,	5, 6,	6, 7,	7, 4,
			0, 4,	1, 5,	2, 6,	3, 7
		};
		_polyList->addIndexVector(index, 24);
		
		_polyList->setDrawMode(PolyList::kLines);
		_polyList->buildPolyList();
		
		drw->addSolid(new Solid(_polyList.getPtr()));
		drw->getGenericMaterial()->setDiffuse(Color::lightGray());
		drw->getGenericMaterial()->setLightEmission(0.7f);
		_boundingBoxDrawable = drw;
	}
	return _boundingBoxDrawable.getPtr();
}

}
