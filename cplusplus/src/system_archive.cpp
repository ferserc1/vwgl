#include <vwgl/system.hpp>

#include <vwgl/archive_file.hpp>

#include <iostream>

namespace vwgl {

bool System::packFiles(const std::string & inPath, const std::vector<std::string> & files, const std::string & outFile, System::PackFormat fmt) {
	ArchiveFile archive;

	if (archive.open(outFile, ArchiveFile::kModePack)) {
		std::vector<std::string>::const_iterator it;
		for (it = files.begin(); it != files.end(); ++it) {
			std::string fileName = *it;
			std::string packagePath = getPath(fileName);
			std::string filePath = addPathComponent(inPath, fileName);
			archive.addFile(filePath, packagePath);
		}
		return archive.close();
	}

	return false;
}

bool System::unpackFiles(const std::string & packedFile, const std::string & outPath) {
	ArchiveFile archive;

	if (archive.open(packedFile, ArchiveFile::kModeUnpack) && archive.unpackTo(outPath)) {
		return archive.close();
	}
	return false;
}

}
