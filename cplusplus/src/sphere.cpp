
#include <vwgl/sphere.hpp>
#include <vwgl/math.hpp>

namespace vwgl {

Sphere::Sphere() :Drawable() {
	buildSphere(1.0f, 15, 15);
}

Sphere::Sphere(float radius) :Drawable() {
	buildSphere(radius, 15, 15);
}

Sphere::Sphere(float radius, int divisions) :Drawable() {
	buildSphere(radius, divisions, divisions);
}

Sphere::Sphere(float radius, int slices, int stacks) :Drawable() {
	buildSphere(radius, slices, stacks);
}

Sphere::~Sphere() {
	
}

void Sphere::buildSphere(float radius, int slices, int stacks) {
	vwgl::PolyList * plist = new vwgl::PolyList();
	
	int latitudeBands = stacks;
	int longitudeBands = slices;
	for (int latNumber = 0; latNumber <= latitudeBands; latNumber++) {
		float theta = latNumber *  Math::kPi / latitudeBands;
		float sinTheta = Math::sin(theta);
		float cosTheta = Math::cos(theta);
		
		for (int longNumber = 0; longNumber <= longitudeBands; longNumber++) {
			float phi = longNumber * 2 * Math::kPi / longitudeBands;
			float sinPhi = Math::sin(phi);
			float cosPhi = Math::cos(phi);
			
			float x = cosPhi * sinTheta;
			float y = cosTheta;
			float z = sinPhi * sinTheta;
			float u = 1.0f - (static_cast<float>(longNumber) / static_cast<float>(longitudeBands));
			float v = 1.0f - (static_cast<float>(latNumber) / static_cast<float>(latitudeBands));
			
			plist->addNormal(vwgl::Vector3(x,y,z));
			plist->addTexCoord0(vwgl::Vector2(u,v));
			plist->addTexCoord1(vwgl::Vector2(u,v));
			plist->addVertex(vwgl::Vector3(radius * x, radius * y, radius * z));
		}
	}
	
	for (int latNumber = 0; latNumber < latitudeBands; latNumber++) {
		for (int longNumber = 0; longNumber < longitudeBands; longNumber++) {
			int first = (latNumber * (longitudeBands + 1)) + longNumber;
			int second = first + longitudeBands + 1;
			plist->addTriangle(first + 1, second, first);
			plist->addTriangle(first + 1, second + 1, second);
		}
	}
	
	plist->buildPolyList();
	addSolid(new vwgl::Solid(plist));
}
	
}
