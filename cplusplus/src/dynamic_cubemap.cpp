
#include <vwgl/dynamic_cubemap.hpp>
#include <vwgl/state.hpp>
#include <vwgl/drawable.hpp>

#pragma warning ( disable : 4835 )

namespace vwgl {

std::string DynamicCubemapMaterial::s_vshader = "\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	attribute vec3 aVertexPosition;\n\
	attribute vec2 aTexturePosition;\n\
	\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
	\n\
	varying vec2 vTexturePosition;\n\
	varying vec4 vPosition;\n\
	\n\
	void main() {\n\
		vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
		gl_Position = vPosition;\n\
		vTexturePosition = aTexturePosition;\n\
	}\n\
	";
	
std::string DynamicCubemapMaterial::s_fshader = "\
	varying vec2 vTexturePosition;\n\
	varying vec4 vPosition;\n\
	\n\
	uniform sampler2D uTexture;\n\
	uniform vec4 uColorTint;\n\
	uniform bool uIsEnabled;\n\
	\n\
	uniform vec2 uTextureOffset;\n\
	uniform vec2 uTextureScale;\n\
	\n\
	void main(void) {\n\
		vec4 texColor = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset);\n\
		if (!uIsEnabled) {\n\
			texColor = vec4(1.0);\n\
		}\n\
		\n\
		gl_FragColor = texColor * uColorTint;\n\
	}";
	
void DynamicCubemapMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	getShader()->attachShader(vwgl::Shader::kTypeVertex, vwgl::DynamicCubemapMaterial::s_vshader);
	getShader()->attachShader(vwgl::Shader::kTypeFragment, vwgl::DynamicCubemapMaterial::s_fshader);
	getShader()->link("def_texture");
	
	loadVertexAttrib("aVertexPosition");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uColorTint");
	getShader()->initUniformLocation("uIsEnabled");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
}

void DynamicCubemapMaterial::setupUniforms(){
	getShader()->setUniform("uMVMatrix", modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());
	
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uTexture", mat->getTexture(),vwgl::Texture::kTexture0);
		getShader()->setUniform("uIsEnabled", mat->getTexture()!=nullptr);
		getShader()->setUniform("uColorTint", mat->getDiffuse());
		getShader()->setUniform("uTextureOffset", mat->getTextureOffset());
		getShader()->setUniform("uTextureScale", mat->getTextureScale());
	}
}

DynamicCubemap::DynamicCubemap() {
	_trxVisitor = new vwgl::TransformVisitor();
	_drawVisitor = new vwgl::DrawNodeVisitor();
	_clearColor = vwgl::Color::black();
	DynamicCubemapManager::get()->addCubemap(this);
}

DynamicCubemap::~DynamicCubemap() {
	DynamicCubemapManager::get()->removeCubemap(this);
}

void DynamicCubemap::createCubemap(const vwgl::Size2Di & size) {
	_fbo = new vwgl::FramebufferObject();
	_fbo->create(size.width(), size.height(),FramebufferObject::kTypeCubeMap);
	_drawVisitor->setMaterial(new vwgl::DynamicCubemapMaterial());
	_drawVisitor->setUseMaterialSettings(true);
}

void DynamicCubemap::beginDraw() {
	if (_fbo.valid()) _fbo->bind();
}

void DynamicCubemap::drawToFace(Texture::TextureTarget target) {
	if (!_fbo.valid()) return;
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, target, _fbo->getTexture()->getTextureName(), 0);

	vwgl::State::get()->setViewport(vwgl::Viewport(0,0,_fbo->getWidth(),_fbo->getHeight()));
	
	vwgl::State::get()->projectionMatrix() = vwgl::Matrix4::makePerspective(90.0f, 1, 0.01f, 100.0f);

	switch (target) {
		case Texture::kTexturePositiveXFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(1.0f,0.0f,0.0f), Vector3(0.0f,1.0f,0.0f));
			break;
		case Texture::kTextureNegativeXFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(-1.0f,0.0f,0.0f), Vector3(0.0f,1.0f,0.0f));
			break;
		case Texture::kTexturePositiveYFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(0.0,-10.0f,0.0f), Vector3(0.0f,0.0f,-1.0f));
			break;
		case Texture::kTextureNegativeYFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(0.0,10.0f,0.0f), Vector3(1.0f,0.0f,-1.0f));
			break;
		case Texture::kTexturePositiveZFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(0.0,0.0f,-10.0f), Vector3(0.0f,1.0f,0.0f));
			break;
		case Texture::kTextureNegativeZFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(0.0,0.0f,10.0f), Vector3(0.0f,1.0f,0.0f));
			break;
		default:
			break;
	}
	
	glClearColor(_clearColor.r(), _clearColor.g(), _clearColor.b(), _clearColor.a());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	_trxVisitor->visit(this);
	vwgl::Vector3 pos = _trxVisitor->getTransform().getPosition();
	
	vwgl::State::get()->viewMatrix().translate(-pos.x(), -pos.y(), -pos.z());
}

void DynamicCubemap::endDraw() {
	if (!_fbo.valid()) return;
	_fbo->unbind();
}
	
void DynamicCubemap::drawScene(vwgl::Node *sceneRoot) {
	if (_fbo.valid()) {
		beginDraw();
		drawToFace(Texture::kTexturePositiveXFace);
		_drawVisitor->visit(sceneRoot);
		
		drawToFace(Texture::kTextureNegativeXFace);
		_drawVisitor->visit(sceneRoot);
		
		drawToFace(Texture::kTexturePositiveYFace);
		_drawVisitor->visit(sceneRoot);
		
		drawToFace(Texture::kTextureNegativeYFace);
		_drawVisitor->visit(sceneRoot);
		
		drawToFace(Texture::kTexturePositiveZFace);
		_drawVisitor->visit(sceneRoot);
		
		drawToFace(Texture::kTextureNegativeZFace);
		_drawVisitor->visit(sceneRoot);
		endDraw();
	}
}

DynamicCubemapManager DynamicCubemapManager::s_singleton;
Size2Di DynamicCubemapManager::s_defaultCubeMapSize = Size2Di(512,512);

DynamicCubemapManager * DynamicCubemapManager::get() {
	return &s_singleton;
}

DynamicCubemapManager::DynamicCubemapManager() {
	_updateIterator = _cubeMapList.end();
}

DynamicCubemapManager::~DynamicCubemapManager() {
	
}
	
	
const Size2Di & DynamicCubemapManager::defaultCubeMapSize() {
	return s_defaultCubeMapSize;
}

void DynamicCubemapManager::setDefaultCubeMapSize(const Size2Di & s) {
	s_defaultCubeMapSize = s;
}

void DynamicCubemapManager::update(Node * sceneRoot) {
	++_frameCount;
	if (_frameCount%10==0) updateCubemaps();
	if (_cubeMapList.size()>0 && _updateIterator!=_cubeMapList.end() && (*_updateIterator) && sceneRoot!=nullptr) {
		(*_updateIterator)->drawScene(sceneRoot);
		++_updateIterator;
	}
}

void DynamicCubemapManager::updateAllCubemaps(Node * sceneRoot) {
	DynamicCubemapList::iterator it;
	for (it=_cubeMapList.begin(); it!=_cubeMapList.end(); ++it) {
		(*it)->drawScene(sceneRoot);
	}
}

void DynamicCubemapManager::addCubemap(vwgl::DynamicCubemap *cm) {
	_cubeMapList.push_back(cm);
	updateCubemaps();
}

void DynamicCubemapManager::removeCubemap(vwgl::DynamicCubemap *cm) {
	DynamicCubemapList::iterator it = std::find(_cubeMapList.begin(), _cubeMapList.end(), cm);
	if (it!=_cubeMapList.end()) _cubeMapList.erase(it);
	updateCubemaps();
}

}
