
#include <vwgl/font_manager.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {

FontManager * FontManager::s_singleton = nullptr;
	
	
FontManager::FontManager() {
	SingletonController::get()->registerSingleton(this);
}
	
FontManager::~FontManager() {
	SingletonController::get()->unregisterSingleton(this);
}
	
FontManager * FontManager::get() {
	if (s_singleton==nullptr) {
		s_singleton = new FontManager();
	}
	return s_singleton;
}
	
TextFont * FontManager::getFont(const std::string & name, int size, int resolution) {
	std::string hash;
	getHash(name, size, resolution, hash);
	
	TextFont * result = nullptr;
	
	FontMap::iterator it = _fonts.find(hash);
	if (it!=_fonts.end()) {
		result = _fonts[hash];
	}
	return result;
}
	
void FontManager::registerFont(TextFont * font) {
	std::string hash;
	if (!getFont(font->getFontName(), font->getSize(), font->getResolution())) {
		getHash(font->getFontName(), font->getSize(), font->getResolution(), hash);
		_fonts[hash] = font;
	}
}

void FontManager::unregisterFont(TextFont * font) {
	std::string hash;
	getHash(font->getFontName(), font->getSize(), font->getResolution(), hash);
	
	FontMap::iterator it = _fonts.find(hash);
	if (it!=_fonts.end()) {
		_fonts.erase(it);
	}
}

void FontManager::finalize() {
	
}

void FontManager::getHash(const std::string & fontName, int size, int res, std::string & hash) {
	std::stringstream sstream;
	sstream << fontName << "_" << size << "pt_" << res << "px";
	hash = sstream.str();
}
	
}