
#include <vwgl/glyph_surface.hpp>

#include <vwgl/drawable.hpp>

#ifdef max
#undef max
#endif

namespace vwgl {

float GlyphSurface::s_scale = 0.01f;
	
GlyphSurface::GlyphSurface(const Glyph & g)
	:_gliph(g)
{
	build();
}
	
GlyphSurface::~GlyphSurface() {
}

void GlyphSurface::build() {
	float minX = _gliph.getBBox().min().x() * GlyphSurface::scale();
	float maxX = _gliph.getBBox().max().x() * GlyphSurface::scale();
	float minY = _gliph.getBBox().min().y() * GlyphSurface::scale();
	float maxY = _gliph.getBBox().max().y() * GlyphSurface::scale();
	
	_plist = new PolyList();
	_plist->addVertex(Vector3(maxX, maxY, 0.0f));
	_plist->addVertex(Vector3(minX, maxY, 0.0f));
	_plist->addVertex(Vector3(minX, minY, 0.0f));
	_plist->addVertex(Vector3(maxX, minY, 0.0f));
	
	_plist->addNormal(Vector3(0.0f, 0.0f, 1.0f));
	_plist->addNormal(Vector3(0.0f, 0.0f, 1.0f));
	_plist->addNormal(Vector3(0.0f, 0.0f, 1.0f));
	_plist->addNormal(Vector3(0.0f, 0.0f, 1.0f));
	
	_plist->addTexCoord0(Vector2(1.0f, 0.0f));
	_plist->addTexCoord0(Vector2(0.0f, 0.0f));
	_plist->addTexCoord0(Vector2(0.0f, 1.0f));
	_plist->addTexCoord0(Vector2(1.0f, 1.0f));
	
	_plist->addTriangle(0, 1, 2);
	_plist->addTriangle(2, 3, 0);
	
	_plist->buildPolyList();
	
	_mat = new vwgl::GenericMaterial();
	_mat->setTexture(vwgl::TextureManager::get()->loadTexture(_gliph.getBitmap(), true));
	_mat->setCullFace(false);
}
	
}