
#include <vwgl/scale_gizmo.hpp>
#include <vwgl/system.hpp>

#include <vwgl/physics/ray.hpp>
#include <vwgl/physics/plane.hpp>
#include <vwgl/physics/intersection.hpp>

namespace vwgl {
	

ScaleGizmo::ScaleGizmo() :Gizmo(System::get()->getResourcesPath() + "scale_gizmo.vwglb") {
	loadPickIds();
	_resetRotation = true;
}

ScaleGizmo::ScaleGizmo(const std::string & model) :Gizmo(model) {
	loadPickIds();
	_resetRotation = true;
}

ScaleGizmo::~ScaleGizmo() {
	
}

void ScaleGizmo::beginMouseEvents(const Position2Di & pos, vwgl::Camera * camera, const Viewport & vp, const Vector3 & gizmoPos, TransformNode * trx) {
	_transformNode = trx;
	_previousPosition = pos;
	_camera = camera;
}

void ScaleGizmo::mouseEvent(const vwgl::Position2Di & pos) {
//	TRSTransformNode * trs = dynamic_cast<TRSTransformNode*>(_transformNode);
	TRSTransformStrategy * trs = _transformNode->getTransformStrategy<TRSTransformStrategy>();
	Vector3 right = _camera->getTransform().rightVector();
	Vector3 up = _camera->getTransform().upVector();
	
	float offsetMultiplier = 0.01f;
	float offsetValue = ((_previousPosition.x() - pos.x()) + (_previousPosition.y() - pos.y())) * offsetMultiplier;
	float offsetX = static_cast<float>(_previousPosition.x() - pos.x());
	float offsetY = static_cast<float>(_previousPosition.y() - pos.y());
	
	if (trs) {
		Vector3 scale = trs->getScale();
		switch (_mode) {
			case kGizmoAxisX:
				offsetValue = offsetX * right.x() * -offsetMultiplier + offsetY * right.z() * -offsetMultiplier;
				scale.x(scale.x() + offsetValue);
				break;
			case kGizmoAxisY:
				offsetValue = offsetY * up.y() * -offsetMultiplier;
				scale.y(scale.y() + offsetValue);
				break;
			case kGizmoAxisZ:
				offsetValue = offsetY * right.x() * offsetMultiplier + offsetX * right.z() * -offsetMultiplier;
				scale.z(scale.z() + offsetValue);
				break;
			case kGizmoAllAxis:
				offsetValue = offsetX * right.x() * -offsetMultiplier + offsetY * right.z() * -offsetMultiplier;
				scale.x(scale.x() + offsetValue);
				scale.y(scale.y() + offsetValue);
				scale.z(scale.z() + offsetValue);
				break;
			default:
				break;
		}
		trs->scale(scale);
	}
	else {
		switch (_mode) {
			case kGizmoAxisX:
				offsetValue = offsetX * right.x() * -offsetMultiplier + offsetY * right.z() * -offsetMultiplier;
				_transformNode->getTransform().scale(1.0f + offsetValue, 1.0f, 1.0f);
				break;
			case kGizmoAxisY:
				offsetValue = offsetY * up.y() * -offsetMultiplier;
				_transformNode->getTransform().scale(1.0f, 1.0f + offsetValue, 1.0f);
				break;
			case kGizmoAxisZ:
				offsetValue = offsetY * right.x() * offsetMultiplier + offsetX * right.z() * -offsetMultiplier;
				_transformNode->getTransform().scale(1.0f, 1.0f, 1.0f + offsetValue);
				break;
			case kGizmoAllAxis:
				offsetValue = offsetX * right.x() * -offsetMultiplier + offsetY * right.z() * -offsetMultiplier;
				_transformNode->getTransform().scale(1.0f + offsetValue, 1.0f + offsetValue, 1.0f + offsetValue);
				break;
			default:
				break;
		}
	}
	_previousPosition = pos;
}

void ScaleGizmo::endMouseEvents() {
	
}

void ScaleGizmo::loadPickIds() {
	Solid * axisX = _drawable->getSolid("Material-xAxis");
	Solid * axisY = _drawable->getSolid("Material-yAxis");
	Solid * axisZ = _drawable->getSolid("Material-zAxis");
	Solid * axisAll = _drawable->getSolid("Material-allAxis");
	
	_xAxisId = axisX->getPickId();
	_yAxisId = axisY->getPickId();
	_zAxisId = axisZ->getPickId();
	_allAxisId = axisAll->getPickId();
}

}
