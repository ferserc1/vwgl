
#include <vwgl/node.hpp>
#include <vwgl/group.hpp>

namespace vwgl {

Node::Node() :_parent(0L), _initialized(false), _enabled(true) {
	
}

Node::~Node() {
	
}
	
bool Node::branchEnabled() {
	if (_parent==nullptr) {
		return _enabled;
	}
	else {
		return _enabled && _parent->branchEnabled();
	}
}

}
