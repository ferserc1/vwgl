
#include <vwgl/collada/colladautils.hpp>
#include <vwgl/exceptions.hpp>

#include <irrXML.h>

#include <sstream>

#include <algorithm>

#include <string.h>

namespace vwgl {
namespace collada {

static char node_COLLADA [] = "COLLADA";
static char node_library_geometries [] = "library_geometries";
static char node_geometry [] = "geometry";
static char node_mesh [] = "mesh";
static char node_source [] = "source";
static char node_vertices [] = "vertices";
static char node_polylist [] = "polylist";
static char node_float_array [] = "float_array";
static char node_technique_common [] = "technique_common";
static char node_accessor [] = "accessor";
static char node_param [] = "param";
static char node_input [] = "input";
static char node_vcount [] = "vcount";
static char node_p [] = "p";
static char node_triangles [] = "triangles";
static char node_library_visual_scenes [] = "library_visual_scenes";
static char node_visual_scene [] = "visual_scene";
static char node_scene_node [] = "node";
static char node_matrix [] = "matrix";
static char node_instance_geometry [] = "instance_geometry";
static char node_up_axis [] = "up_axis";
static char node_unit [] = "unit";
static char node_asset [] = "asset";

Collada::Collada() :Node("COLLADA") {
}

Collada::~Collada() {

}

void FloatArray::setArray(const std::string & array) {
	std::string item;
	std::istringstream iss(array,std::istringstream::in);

	while (iss >> item) {
		_array.push_back(static_cast<float>(atof(item.c_str())));
	}
}

void Vcount::setArray(const std::string &v) {
	std::string item;
	std::istringstream iss(v,std::istringstream::in);

	while (iss >> item) {
		_array.push_back(atoi(item.c_str()));
	}
}

void P::setArray(const std::string &v) {
	std::string item;
	std::istringstream iss(v,std::istringstream::in);

	while (iss >> item) {
		_array.push_back(atoi(item.c_str()));
	}
}

int Source::getStride() {
	if (_tc.valid() && _tc->getAccessor()) {
		return _tc->getAccessor()->getStride();
	}
	return 0;
}

int Source::getCount() {
	if (_floatArray.valid()) return _floatArray->getCount();
	return 0;
}

Vector2 Source::getVector2(int index) {
	int stride = getStride();
	int base = index * stride;
	if (stride==2 && static_cast<int>(_floatArray->getArray().size())>(index*stride+stride-1)) {
		return Vector2(_floatArray->getArray()[base],
					   _floatArray->getArray()[base + 1]);
	}
	else return Vector2(0.0f);
}

Vector3 Source::getVector3(int index) {
	int stride = getStride();
	int base = index * stride;
	if (stride==3 && static_cast<int>(_floatArray->getArray().size())>(index*stride+stride-1)) {
		return Vector3(_floatArray->getArray()[base],
					   _floatArray->getArray()[base + 1],
					   _floatArray->getArray()[base + 2]);
	}
	else return Vector3(0.0f);
}

Vector4 Source::getVector4(int index) {
	int stride = getStride();
	int base = index * stride;
	if (stride==4 && static_cast<int>(_floatArray->getArray().size())>(index*stride+stride-1)) {
		return Vector4(_floatArray->getArray()[base],
					   _floatArray->getArray()[base + 1],
					   _floatArray->getArray()[base + 2],
					   _floatArray->getArray()[base + 3]);
	}
	else return Vector4(0.0f);
}

const std::string & Vertices::getSourceId() {
	return _input->getSource();
}

void Input::setSemantic(const std::string &s) {
	_semantic = s;
	std::transform(_semantic.begin(), _semantic.end(), _semantic.begin(), ::toupper);
}

Source::Role Input::getRole() {
	if (_semantic=="VERTEX") return Source::kVertex;
	else if (_semantic=="NORMAL") return Source::kNormal;
	else if (_semantic=="TEXCOORD") {
		switch (_set) {
			case 0: return Source::kTexCoord0;
			case 1: return Source::kTexCoord1;
			case 2: return Source::kTexCoord2;
			case 3: return Source::kTexCoord3;
		}
	}
	return Source::kUnknown;
}

void Matrix::setMatrix(const std::string & str_matrix) {
	std::string item;
	std::istringstream iss(str_matrix,std::istringstream::in);
	float matrixArray[16];

	int i = 0;
	while ((iss >> item) && i<16) {
		matrixArray[i] = static_cast<float>(atof(item.c_str()));
		++i;
	}
	_matrix = matrixArray;
}

Node * Factory::getNode(const std::string &tag) {
	const char * req = tag.c_str();
	if (strcmp(req, node_COLLADA)==0) return new Collada();
	else if (strcmp(req, node_library_geometries)==0) return new LibraryGeometries();
	else if (strcmp(req, node_geometry)==0) return new Geometry();
	else if (strcmp(req, node_mesh)==0) return new Mesh();
	else if (strcmp(req, node_source)==0) return new Source();
	else if (strcmp(req, node_vertices)==0) return new Vertices();
	else if (strcmp(req, node_polylist)==0) return new Polylist();
	else if (strcmp(req, node_float_array)==0) return new FloatArray();
	else if (strcmp(req, node_technique_common)==0) return new TechniqueCommon();
	else if (strcmp(req, node_accessor)==0) return new Accessor();
	else if (strcmp(req, node_param)==0) return new Param();
	else if (strcmp(req, node_input)==0) return new Input();
	else if (strcmp(req, node_vcount)==0) return new Vcount();
	else if (strcmp(req, node_p)==0) return new P();
	else if (strcmp(req, node_triangles)==0) return new Triangles();
	else if (strcmp(req, node_library_visual_scenes)==0) return new LibraryVisualScenes();
	else if (strcmp(req, node_visual_scene)==0) return new VisualScene();
	else if (strcmp(req, node_scene_node)==0) return new SceneNode();
	else if (strcmp(req, node_matrix)==0) return new Matrix();
	else if (strcmp(req, node_instance_geometry)==0) return new InstanceGeometry();
	else if (strcmp(req, node_asset)==0) return new Asset();
	else if (strcmp(req, node_up_axis)==0) return new UpAxis();
	else if (strcmp(req, node_unit)==0) return new Unit();

	else return new Node(tag);
}

Context::Context()
{
}


std::string ColladaReader::getAttribute(XmlReader * p_xml, const std::string & key) {
	irr::io::IrrXMLReader * xml = reinterpret_cast<irr::io::IrrXMLReader*>(p_xml);
	const char * c_name = xml->getAttributeValue(key.c_str());
	if (c_name) return std::string(c_name);
	else return "";
}

collada::Collada * ColladaReader::parse(XmlReader * p_xml) {
	irr::io::IrrXMLReader * xml = reinterpret_cast<irr::io::IrrXMLReader*>(p_xml);

	ptr<collada::Collada> dae;
	ptr<collada::Node> node;
	collada::Context ctx;

	while (xml && xml->read()) {
		switch (xml->getNodeType()) {
			case irr::io::EXN_ELEMENT:
				node = ctx.pushContextNode(collada::Factory::getNode(xml->getNodeName()));
				if (ctx.collada()) {
					dae = ctx.collada();
				}
				else if (!ctx.valid()) {
					throw BaseException("DAE: unexpected node found. COLLADA root node not found");
				}
				else if (ctx.libraryGeometries()) {
					dae->setLibraryGeometries(ctx.libraryGeometries());
				}
				else if (ctx.geometry()) {
					ctx.geometry()->setId(getAttribute(p_xml, "id"));
					ctx.geometry()->setName(getAttribute(p_xml, "name"));
					if (dae->getLibraryGeometries()) {
						dae->getLibraryGeometries()->addGeometry(ctx.geometry());
					}
					else {
						throw BaseException("DAE: unexpected geometry node found");
					}
				}
				else if (ctx.mesh() && ctx.parentGeometry()) {
					ctx.parentGeometry()->setMesh(ctx.mesh());
				}
				else if (ctx.source() && ctx.parentMesh()) {
					ctx.source()->setId(getAttribute(p_xml, "id"));
					ctx.source()->setName(getAttribute(p_xml, "name"));
					ctx.parentMesh()->addSource(ctx.source());
				}
				else if (ctx.floatArray() && ctx.parentSource()) {
					ctx.floatArray()->setId(getAttribute(p_xml, "id"));
					ctx.floatArray()->setCount(getAttribute(p_xml, "count"));
					ctx.parentSource()->setFloatArray(ctx.floatArray());
				}
				else if (ctx.techniqueCommon() && ctx.parentSource()) {
					ctx.parentSource()->setTechniqueCommon(ctx.techniqueCommon());
				}
				else if(ctx.accessor() && ctx.parentTechniqueCommon()) {
					ctx.accessor()->setCount(getAttribute(p_xml, "count"));
					ctx.accessor()->setSource(getAttribute(p_xml, "source"));
					ctx.accessor()->setStride(getAttribute(p_xml, "stride"));
					ctx.parentTechniqueCommon()->setAccessor(ctx.accessor());
				}
				else if (ctx.param() && ctx.parentAccessor()) {
					ctx.param()->setName(getAttribute(p_xml, "name"));
					ctx.param()->setType(getAttribute(p_xml, "type"));
					ctx.parentAccessor()->addParam(ctx.param());
				}
				else if (ctx.vertices() && ctx.parentMesh()) {
					ctx.vertices()->setId(getAttribute(p_xml, "id"));
					ctx.parentMesh()->addVertices(ctx.vertices());
				}
				else if (ctx.input() && ctx.parentVertices()) {
					ctx.input()->setSemantic(getAttribute(p_xml, "semantic"));
					ctx.input()->setSource(getAttribute(p_xml, "source"));
					ctx.parentVertices()->setInput(ctx.input());
				}
				else if (ctx.polylist() && ctx.parentMesh()) {
					ctx.polylist()->setCount(getAttribute(p_xml, "count"));
					ctx.polylist()->setMaterial(getAttribute(p_xml, "material"));
					ctx.parentMesh()->addPolylist(ctx.polylist());
				}
				else if (ctx.input() && ctx.parentPolylist()) {
					ctx.input()->setSemantic(getAttribute(p_xml, "semantic"));
					ctx.input()->setSource(getAttribute(p_xml, "source"));
					ctx.input()->setOffset(getAttribute(p_xml, "offset"));
					ctx.input()->setSet(getAttribute(p_xml, "set"));
					ctx.parentPolylist()->addInput(ctx.input());
				}
				else if (ctx.vcount() && ctx.parentPolylist()) {
					ctx.parentPolylist()->setVcount(ctx.vcount());
				}
				else if (ctx.p() && ctx.parentPolylist()) {
					ctx.parentPolylist()->setP(ctx.p());
				}
				else if (ctx.triangles() && ctx.parentMesh()) {
					ctx.triangles()->setCount(getAttribute(p_xml, "count"));
					ctx.triangles()->setMaterial(getAttribute(p_xml, "material"));
					ctx.parentMesh()->addTriangles(ctx.triangles());
				}
				else if (ctx.input() && ctx.parentTriangles()) {
					ctx.input()->setSemantic(getAttribute(p_xml, "semantic"));
					ctx.input()->setSource(getAttribute(p_xml, "source"));
					ctx.input()->setOffset(getAttribute(p_xml, "offset"));
					ctx.input()->setSet(getAttribute(p_xml, "set"));
					ctx.parentTriangles()->addInput(ctx.input());
				}
				else if (ctx.p() && ctx.parentTriangles()) {
					ctx.parentTriangles()->setP(ctx.p());
				}
				else if (ctx.libraryVisualScenes()) {
					dae->setLibraryVisualScenes(ctx.libraryVisualScenes());
				}
				else if (ctx.visualScene()) {
					ctx.visualScene()->setId(getAttribute(p_xml, "id"));
					ctx.visualScene()->setName(getAttribute(p_xml, "name"));
					dae->getLibraryVisualScenes()->addVisualScene(ctx.visualScene());
				}
				else if (ctx.node() && ctx.parentVisualScene()) {
					ctx.node()->setName(getAttribute(p_xml, "name"));
					ctx.node()->setId(getAttribute(p_xml, "id"));
					ctx.parentVisualScene()->addSceneNode(ctx.node());
				}
				else if (ctx.matrix() && ctx.parentNode()) {
					ctx.parentNode()->setMatrix(ctx.matrix());
				}
				else if (ctx.instanceGeometry() && ctx.parentNode()) {
					ctx.instanceGeometry()->setUrl(getAttribute(p_xml, "url"));
					ctx.parentNode()->setInstanceGeometry(ctx.instanceGeometry());
				}
				else if (ctx.asset()) {
					dae->setAsset(ctx.asset());
				}
				else if (ctx.unit() && ctx.parentAsset()) {
					ctx.unit()->setMeter(getAttribute(p_xml, "meter"));
					ctx.unit()->setName(getAttribute(p_xml, "name"));
					ctx.parentAsset()->setUnit(ctx.unit());
				}
				else if (ctx.upAxis() && ctx.parentAsset()) {
					ctx.parentAsset()->setUpAxis(ctx.upAxis());
				}

				if (xml->isEmptyElement()) {
					ctx.popContextNode();
				}
				break;
			case irr::io::EXN_ELEMENT_END:
				ctx.popContextNode();
				break;
			case irr::io::EXN_TEXT:
				if (ctx.floatArray()) {
					ctx.floatArray()->setArray(xml->getNodeData());
				}
				else if (ctx.vcount()) {
					ctx.vcount()->setArray(xml->getNodeData());
				}
				else if (ctx.p()) {
					ctx.p()->setArray(xml->getNodeData());
				}
				else if (ctx.matrix()) {
					ctx.matrix()->setMatrix(xml->getNodeData());
				}
				else if (ctx.upAxis()) {
					ctx.upAxis()->setAxis(xml->getNodeData());
				}

				break;
			default:
				break;
		}
	}

	return dae.release();
}
}
}
