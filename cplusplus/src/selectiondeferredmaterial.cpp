
#include <vwgl/selectiondeferredmaterial.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

SelectionDeferredMaterial::SelectionDeferredMaterial() :DeferredMaterial() {
	initShader();
}

SelectionDeferredMaterial::~SelectionDeferredMaterial() {
}

void SelectionDeferredMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_selection.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_selection.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_selection.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_selection.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: selection deferred shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("def_selection");

	loadVertexAttrib("aVertexPosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uSelected");
	getShader()->initUniformLocation("uColor");
}

void SelectionDeferredMaterial::setupUniforms() {
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		updateColor();
		getShader()->setUniform("uMVMatrix", modelViewMatrix());
		getShader()->setUniform("uPMatrix", projectionMatrix());
		getShader()->setUniform("uSelected", mat->getSelectedMode());
		getShader()->setUniform("uColor", vwgl::Color(static_cast<float>(_color.r())/255.0f,
													  static_cast<float>(_color.g())/255.0f,
													  static_cast<float>(_color.b())/255.0f, 1.0f));
	}
}

void SelectionDeferredMaterial::updateColor() {
	_color = _color + Vector3i(20,30,40);
	if (_color.r()>250) _color.r(0);
	if (_color.g()>250) _color.g(0);
	if (_color.b()>250) _color.b(0);
}


}
