#include <vwgl/library_reader.hpp>
#include <vwgl/library/library.hpp>
#include <JsonBox.h>

#include <fstream>

namespace vwgl {

class LibraryJsonParser {
public:
	library::Group * parse(const std::string & lib) {
		ptr<library::Group> grp = new library::Group();
		std::string fileVersion;
	
		JsonBox::Value js_lib;
		
		js_lib.loadFromString(lib);
		if (!js_lib.isObject()) {
			throw LoaderException("Unexpected node found: expecting object");
		}
		
		if (!js_lib["fileType"].isString() || js_lib["fileType"].getString()!="vwgl::library") {
			throw LoaderException("Unexpected file type");
		}
		
		if (!js_lib["version"].isString()) {
			throw LoaderException("File version not found");
		}
		fileVersion = js_lib["version"].getString();
		
		JsonBox::Array root;
		if (!js_lib["root"].isArray()) {
			throw LoaderException("Node root not found");
		}
		root = js_lib["root"].getArray();
		
		grp->setName("root node");
		grp->addMetadata("fileType", "vwgl::library");
		grp->addMetadata("version", fileVersion);
		
		parseChildren(grp.getPtr(), root);

		return grp.release();
	}

protected:
	void parseChildren(library::Group * root, JsonBox::Array & children) {
		JsonBox::Array::iterator it;
		for (it=children.begin(); it!=children.end(); ++it) {
			ptr<library::Node> node = parseNode(*it);
			if (node.valid()) {
				root->addChild(node.getPtr());
			}
		}
	}
	
	library::Node * parseNode(const JsonBox::Value & js_node) {
		if (!js_node.isObject()) {
			throw LoaderException("Unexpected type found: expecting object");
		}
		std::string type;
		JsonBox::Value n = js_node;
		getString(n["type"], type, "unknown");
		if (type=="group") {
			return getGroup(n);
		}
		else if (type=="model") {
			return getModel(n);
		}
		else if (type=="material") {
			return getMaterial(n);
		}

		return nullptr;
	}
	
	void fillNodeProperties(JsonBox::Value & js_node, library::Node * node) {
		std::string id;
		std::string name;
		std::string icon;
		
		getString(js_node["id"], id, "undefined");
		getString(js_node["name"], name, "unnamed");
		getString(js_node["icon"], icon, "");
		
		node->setId(id);
		node->setName(name);
		node->setIcon(icon);
		
		fillMetadata(js_node["metadata"], node);
	}
	
	void fillMetadata(const JsonBox::Value & md, library::Node * node) {
		if (md.isObject()) {
			JsonBox::Object::const_iterator it;
			
			for (it=md.getObject().begin(); it!=md.getObject().end(); ++it) {
				std::string key = (*it).first;
				std::string value;
				if ((*it).second.isArray() && (*it).second.getArray().size()>0) {
					getString((*it).second.getArray()[0], value, "");
				}
				else {
					getString((*it).second, value, "");
				}
				node->addMetadata(key, value);
			}
		}
	}
	
	library::Group * getGroup(JsonBox::Value & js_grp) {
		ptr<library::Group> group = new library::Group();
		fillNodeProperties(js_grp, group.getPtr());
		
		if (js_grp["children"].isArray()) {
			JsonBox::Array::const_iterator it;
			for (it=js_grp["children"].getArray().begin(); it!=js_grp["children"].getArray().end(); ++it) {
				library::Node * child = parseNode((*it));
				if (child) {
					group->addChild(child);
				}
			}
		}
		return group.release();
	}
	
	library::Model * getModel(JsonBox::Value & js_model) {
		ptr<library::Model> model = new library::Model();
		fillNodeProperties(js_model, model.getPtr());
		
		std::string file;
		getString(js_model["file"], file, "");
		model->setFile(file);
		
		return model.release();
	}
	
	library::Material * getMaterial(JsonBox::Value & js_mat) {
		ptr<library::Material> material = new library::Material();
		fillNodeProperties(js_mat, material.getPtr());
		
		JsonBox::Value modifier = js_mat["materialModifier"];
		if (modifier.isObject()) {
			MaterialModifier mod;
			Color diffuse;
			Color specular;
			float shininess;
			float lightEmission;
			float refractionAmount;
			float reflectionAmount;
			std::string texture;
			std::string lightmap;
			std::string normalMap;
			std::string lightNormalMap;
			Vector2 texScale;
			Vector2 texOffset;
			Vector2 nmScale;
			Vector2 nmOffset;
			bool castShadows;
			bool receiveShadows;
			bool receiveProjections;

			float alphaCutoff;
			std::string shininessMask;
			int shininessMaskChannel;
			bool invertShininessMask;
			std::string lightEmissionMask;
			int lightEmissionMaskChannel;
			bool invertLightEmissionMask;
			
			if (getMaterialField(modifier["diffuseR"], modifier["diffuseG"],
								 modifier["diffuseB"], modifier["diffuseA"],diffuse)) {
				mod.setDiffuse(diffuse);
			}
			if (getMaterialField(modifier["specularR"], modifier["specularG"],
								 modifier["specularB"], modifier["specularA"],specular)) {
				mod.setSpecular(specular);
			}
			if (getMaterialField(modifier["shininess"], shininess)) mod.setShininess(shininess);
			if (getMaterialField(modifier["lightEmission"], lightEmission)) mod.setLightEmission(lightEmission);
			if (getMaterialField(modifier["refractionAmount"], refractionAmount)) mod.setRefractionAmount(refractionAmount);
			if (getMaterialField(modifier["reflectionAmount"], reflectionAmount)) mod.setReflectionAmount(reflectionAmount);
			if (getMaterialField(modifier["texture"], texture)) mod.setTexture(texture);
			if (getMaterialField(modifier["lightmap"], lightmap)) mod.setLightMap(lightmap);
			if (getMaterialField(modifier["normalMap"], normalMap)) mod.setNormalMap(normalMap);
			if (getMaterialField(modifier["lightNormalMap"], lightNormalMap)) mod.setLightNormalMap(lightNormalMap);
			if (getMaterialField(modifier["textureScaleX"],modifier["textureScaleY"],texScale)) mod.setTextureScale(texScale);
			if (getMaterialField(modifier["textureOffsetX"],modifier["textureOffsetY"],texOffset)) mod.setTextureOffset(texOffset);
			if (getMaterialField(modifier["normalMapScaleX"], modifier["normalMapScaleY"], nmScale)) mod.setNormalMapScale(nmScale);
			if (getMaterialField(modifier["normalMapOffsetX"], modifier["normalMapOffsetY"], nmOffset)) mod.setNormalMapOffset(nmOffset);
			if (getMaterialField(modifier["castShadows"], castShadows)) mod.setCastShadows(castShadows);
			if (getMaterialField(modifier["receiveShadows"], receiveShadows)) mod.setReceiveShadows(receiveShadows);
			if (getMaterialField(modifier["receiveProjections"], receiveProjections)) mod.setReceiveProjections(receiveProjections);
			if (getMaterialField(modifier["alphaCutoff"], alphaCutoff)) mod.setAlphaCutoff(alphaCutoff);
			if (getMaterialField(modifier["shininessMask"], shininessMask)) {
				if (!getMaterialField(modifier["shininessMaskChannel"], shininessMaskChannel)) shininessMaskChannel = 0;
				if (!getMaterialField(modifier["invertShininessMask"], invertShininessMask)) invertShininessMask = false;
				mod.setShininessMask(shininessMask, shininessMaskChannel, invertShininessMask);
			}
			if (getMaterialField(modifier["lightEmissionMask"], lightEmissionMask)) {
				if (!getMaterialField(modifier["lightEmissionMaskChannel"], lightEmissionMaskChannel)) lightEmissionMaskChannel = 0;
				if (!getMaterialField(modifier["invertlightEmissionMask"], invertLightEmissionMask)) invertLightEmissionMask = false;
				mod.setShininessMask(lightEmissionMask , lightEmissionMaskChannel, invertLightEmissionMask);
			}
			material->setMaterialModifier(mod);

			std::string materialType;
			getString(js_mat["materialType"],materialType,"");
			material->setMaterialType(materialType);
		}
		
		return material.release();
	}

	bool getMaterialField(JsonBox::Value & js_x, JsonBox::Value & js_y, Vector2 & vec) {
		float x=0.0f, y=0.0f;
		bool status = getMaterialField(js_x, x);
		status = getMaterialField(js_y, y) || status;
		if (status) {
			vec = Vector2(x,y);
		}
		return status;
	}

	bool getMaterialField(JsonBox::Value & js_r, JsonBox::Value & js_g,JsonBox::Value & js_b,JsonBox::Value & js_a,
						  vwgl::Color & color) {
		float r=0.0f, g=0.0f, b=0.0f, a=1.0f;
		bool status = getMaterialField(js_r, r);
		status = getMaterialField(js_g, g) || status;
		status = getMaterialField(js_b, b) || status;
		status = getMaterialField(js_a, a) || status;
		if (status) {
			color = vwgl::Color(r,g,b,a);
		}
		return status;
	}

	bool getMaterialField(JsonBox::Value &js_val, float & val) {
		if (js_val.isDouble()) {
			val = static_cast<float>(js_val.getDouble());
			return true;
		}
		else if (js_val.isInteger()) {
			val = static_cast<float>(js_val.getInt());
			return true;
		}
		return false;
	}

	bool getMaterialField(JsonBox::Value &js_val, int & val) {
		if (js_val.isDouble()) {
			val = static_cast<int>(js_val.getDouble());
			return true;
		}
		else if (js_val.isInteger()) {
			val = js_val.getInt();
			return true;
		}
		return false;
	}
	
	bool getMaterialField(JsonBox::Value & js_val, std::string & val) {
		if (js_val.isString()) {
			val = js_val.getString();
			return true;
		}
		return false;
	}
	
	bool getMaterialField(JsonBox::Value & js_val, bool & val) {
		if (js_val.isBoolean()) {
			val = js_val.getBoolean();
			return true;
		}
		return false;
	}
	
	int getInt(const JsonBox::Value & val, int defaultVal) {
		if (val.isInteger()) return val.getInt();
		else return defaultVal;
	}
	
	float getFloat(const JsonBox::Value & val, float defaultVal) {
		if (val.isDouble()) return static_cast<float>(val.getDouble());
		else return defaultVal;
	}
	
	bool getBool(const JsonBox::Value & val, bool defaultVal) {
		if (val.isBoolean()) return val.getBoolean();
		else return defaultVal;
	}

	void getString(const JsonBox::Value & val, std::string & string, const std::string & defaultVal) {
		std::stringstream conv;
		if (val.isString()) {
			conv << val.getString();
		}
		else if (val.isInteger()) {
			conv << val.getInt();
		}
		else if (val.isDouble()) {
			conv << val.getDouble();
		}
		else if (val.isBoolean()) {
			conv << val.getBoolean();
		}
		else {
			conv << defaultVal;
		}
		string = conv.str();
	}
};

LibraryLoader::LibraryLoader() {
}

LibraryLoader::~LibraryLoader() {
}

bool LibraryLoader::acceptFileType(const std::string &path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext=="vwlib" || ext=="json";
}

library::Node * LibraryLoader::loadLibrary(const std::string & path) {
	if (!acceptFileType(path)) return nullptr;
	std::ifstream inFile;
	inFile.open(path);
	
	if (inFile.is_open()) {
		std::stringstream strStream;
		strStream << inFile.rdbuf();
		std::string fileContent = strStream.str();
		LibraryJsonParser parser;
		vwgl::ptr<vwgl::library::Node> node = parser.parse(fileContent);
		std::string filePath = path.substr(0, path.find_last_of(".")) + "/";
		node->setPath(filePath);
		return node.release();
	}
	else {
		return nullptr;
	}
}

}
