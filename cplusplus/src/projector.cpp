
#include <vwgl/projector.hpp>
#include <vwgl/camera.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/drawable.hpp>

namespace vwgl {

Projector::Projector() {
	_projection.perspective(Math::degreesToRadians(45.0f), 1.0f, 0.1f, 100.0f);
	_transform = vwgl::Matrix4::makeIdentity();
	_transformVisitor = new TransformVisitor();
	_attenuation = 1.0f;
	_visible = true;
	ProjectorManager::get()->addProjector(this);
}

Projector::~Projector() {
	ProjectorManager::get()->removeProjector(this);
}

Vector3 Projector::getPosition() {
	_transformVisitor->visit(this);
	Matrix4 tr = _transformVisitor->getTransform();
	return tr.getPosition();
}

Vector3 Projector::getDirection() {
	Vector3 look(0.0f,0.0f,1.0f);
	Camera * cam = CameraManager::get()->getMainCamera();
	_transformVisitor->visit(this);
	Matrix4 tr = _transformVisitor->getTransform();
	if (cam) {
		Matrix4 vm = cam->getViewMatrix();
		vm.setRow(3, Vector4(0.0f, 0.0f, 0.0f, 1.0f));
		vm.mult(tr);
		look = vm.multVector(look).xyz();
	}
	else {
		look = tr.multVector(look).xyz();
	}
	look.normalize();
	return look;
}

Matrix4 Projector::getTransform() {
	_transformVisitor->visit(this);
	//TransformNode * trNode = dynamic_cast<TransformNode*>(getParent());
	//if (trNode) {
	//	return trNode->getTransform();
	//}
	return _transformVisitor->getTransform().mult(_transform);
}

static ProjectorManager s_projectorManager;

ProjectorManager * ProjectorManager::get() {
	return &s_projectorManager;
}

ProjectorManager::ProjectorManager() {
	
}

ProjectorManager::~ProjectorManager() {
	_projectorList.clear();
}

void ProjectorManager::addProjector(vwgl::Projector *proj) {
	_projectorList.push_back(proj);
}

void ProjectorManager::removeProjector(vwgl::Projector *proj) {
	ProjectorList::iterator it = std::find(_projectorList.begin(), _projectorList.end(), proj);
	if (it!=_projectorList.end()) _projectorList.erase(it);
	
}

}