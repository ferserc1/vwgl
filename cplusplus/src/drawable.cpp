
#include <vwgl/drawable.hpp>
#include <vwgl/drawable_node.hpp>

namespace vwgl {

DrawableBase::DrawableBase() :_name(""), _hidden(false), _parent(nullptr) {
}

DrawableBase::DrawableBase(const std::string & name) :_name(name), _hidden(false), _parent(nullptr) {
	
}
	

Drawable::Drawable() :DrawableBase(), _renderTransparent(true), _renderOpaque(true) {
	_identifier = Drawable::createIdentifier();
}

Drawable::Drawable(const std::string & name) :DrawableBase(name), _renderTransparent(true), _renderOpaque(true) {
	_identifier = Drawable::createIdentifier();
}

Drawable::~Drawable() {
	destroy();
}
	
int Drawable::s_identifier = 0;
int Drawable::createIdentifier() {
	++s_identifier;
	return s_identifier;
}

Solid * Drawable::getSolid(const std::string & plistName) {
	Solid * result = NULL;
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end() && result==NULL; ++it) {
		if ((*it)->getPolyList()->getName()==plistName) {
			result = (*it).getPtr();
		}
	}
	return result;
}
	
PolyList * Drawable::getPolyList(const std::string & name) {
	Solid * solid = getSolid(name);
	if (solid) {
		return solid->getPolyList();
	}
	return NULL;
}

void Drawable::switchUVs(PolyList::VertexBufferType uv1, PolyList::VertexBufferType uv2) {
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		(*it)->getPolyList()->switchUVs(uv1, uv2);
	}
}

void Drawable::setMaterial(Material * mat) {
	SolidList::iterator it;
	
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		(*it)->setMaterial(mat);
	}
}

Material * Drawable::getMaterial() {
	if (_solidList.size()>0) return _solidList.front()->getMaterial();
	else return NULL;
}

void Drawable::setCubemap(vwgl::CubeMap * cm) {
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		if ((*it)->getGenericMaterial()) {
			(*it)->getGenericMaterial()->setCubeMap(cm);
		}
	}
}
	
void Drawable::init() {
	addProjectorToScene();
	// It's not a good idea to automatically create the cubemap
	//createCubeMap();
}

void Drawable::draw(Material * mat, bool useMaterialSettings) {
	if (!_hidden ) {
		SolidList::iterator it;
	
		for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
			(*it)->setRenderTransparent(_renderTransparent);
			(*it)->setRenderOpaque(_renderOpaque);
			(*it)->draw(mat, useMaterialSettings);
		}
	}
}

void Drawable::destroy() {
	removeProjectorFromScene();
	_solidList.clear();
}
	
void Drawable::applyTransform(const vwgl::Matrix4 &transform) {
	// Apply to geometry
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		(*it)->getPolyList()->applyTransform(transform);
		if ((*it)->getShadowProjector()) {
			Matrix4 shadowTransform = transform;
			shadowTransform.mult((*it)->getShadowProjector()->getRawTransform());
			(*it)->getShadowProjector()->setTransform(shadowTransform);
		}
	}
	
	// Apply to shadow projectors
	if (_shadowProjector.valid()) {
		Matrix4 shadowTransform = transform;
		shadowTransform.mult(_shadowProjector->getRawTransform());
		_shadowProjector->setTransform(shadowTransform);
	}
	
	updateBoundingBox();
}

void Drawable::flipFaces() {
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		(*it)->getPolyList()->flipFaces();
	}
}
	
void Drawable::flipNormals() {
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		(*it)->getPolyList()->flipNormals();
	}
}

void Drawable::setShadowProjector(Projector * proj) {
	if (proj!=NULL) {
		_shadowProjector = proj;
		Texture * tex = _shadowProjector->getTexture();
		if (tex) {
			tex->bindTexture(vwgl::Texture::kTargetTexture2D);
			tex->setTextureWrapS(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
			tex->setTextureWrapT(vwgl::Texture::kTargetTexture2D, vwgl::Texture::kWrapClampToEdge);
		}
		if (getParent()) getParent()->setInitialized(false);
	}
	else {
		removeProjectorFromScene();
		_shadowProjector = NULL;
	}
}

void Drawable::hideGroup(const std::string & groupName) {
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		if ((*it)->getGroupName()==groupName) {
			(*it)->hide();
		}
	}
	updateBoundingBox();
}

void Drawable::showGroup(const std::string & groupName) {
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		if ((*it)->getGroupName()==groupName) {
			(*it)->show();
		}
	}
	updateBoundingBox();
}
	
void Drawable::showByName(const std::string & solidName) {
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		if ((*it)->getPolyList()->getName()==solidName) {
			(*it)->show();
		}
	}
	updateBoundingBox();
}

void Drawable::hideByName(const std::string & solidName) {
	SolidList::iterator it;
	for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
		if ((*it)->getPolyList()->getName()==solidName) {
			(*it)->hide();
		}
	}
	updateBoundingBox();
}

void Drawable::addProjectorToScene() {
	if (_shadowProjector.valid() && !_shadowProjector->getParent() && getParent()) {
		Group * group = dynamic_cast<Group*>(getParent()->getParent());
		if (group) {
			group->addChild(_shadowProjector.getPtr());
		}
	}
	SolidList::iterator it;
	for (it=_solidList.begin();it!=_solidList.end();++it) {
		Projector * proj = (*it)->getShadowProjector();
		if (proj && !proj->getParent() && getParent()) {
			Group * group = dynamic_cast<Group*>(getParent()->getParent());
			if (group) {
				group->addChild(proj);
			}
		}
	}
}

void Drawable::removeProjectorFromScene() {
	if (_shadowProjector.valid() && _shadowProjector->getParent()) {
		_shadowProjector->getParent()->removeChild(_shadowProjector.getPtr());
	}
	SolidList::iterator it;
	for (it=_solidList.begin();it!=_solidList.end();++it) {
		(*it)->removeProjectorFromScene();
	}
}

bool Drawable::needsCubeMap() {
	SolidList::iterator it;
	bool result = false;
	for (it=_solidList.begin(); it!=_solidList.end() && !result; ++it) {
		GenericMaterial * mat = (*it)->getGenericMaterial();
		if (mat && mat->getReflectionAmount()>0.0)  {
			CubeMap * cm = mat->getCubeMap();
			result = cm==NULL;
		}
	}
	return result && !_cubeMap.valid();
}

void Drawable::createCubeMap() {
	if (needsCubeMap()) {
		_cubeMap = new DynamicCubemap();
		_cubeMap->createCubemap(DynamicCubemapManager::get()->defaultCubeMapSize());
		SolidList::iterator it;
		for (it=_solidList.begin(); it!=_solidList.end(); ++it) {
			GenericMaterial * mat = (*it)->getGenericMaterial();
			if (mat) {
				if (mat->getReflectionAmount()>0.0) {
					mat->setCubeMap(_cubeMap->getCubeMap());
				}
			}
		}
		if (_cubeMap.valid()) {
			addToScene(_cubeMap.getPtr(),getParent());
		}
	}
}
	
void Drawable::removeCubeMap() {
	vwgl::Group * parent = NULL;
	if (_cubeMap.valid() && (parent=dynamic_cast<Group*>(_cubeMap->getParent()))) {
		parent->removeChild(_cubeMap.getPtr());
	}
	_cubeMap = NULL;
}

void Drawable::addToScene(Node * n, Node * parent) {
	if (parent==NULL) return;
	if (!n->getParent()) {
		Group * grp = dynamic_cast<Group*>(parent);
		if (grp) {
			grp->addChild(n);
		}
		else {
			addToScene(n, parent->getParent());
		}
	}
}

void Drawable::setupJointMaterial(physics::Joint * joint) {
	if (joint && joint->getDrawable() && joint->getDrawable()->getGenericMaterial()) {
		Color color = joint==_inputJoint.getPtr() ? Color(1.0f,1.0f,0.0f,0.9f):Color(0.0f,0.5f,1.0f,0.9f);
		joint->getDrawable()->getGenericMaterial()->setDiffuse(color);
		joint->getDrawable()->getGenericMaterial()->setLightEmission(1.0f);
	}
}
	

// DrawableInstance implementation
void DrawableInstance::setDrawable(vwgl::Drawable *drw) {
	_sourceDrawable = drw;
	if (_sourceDrawable.valid()) {
		
		// Initialize the new SolidList array
		_solidList.clear();
		SolidList::iterator it;
		for (it=drw->getSolidList().begin(); it!=drw->getSolidList().end(); ++it) {
			Solid * srcSolid = (*it).getPtr();
			Solid * newSolid = srcSolid->clone(Solid::kCloneModeInstance, Solid::kCloneModeCopy);
			addSolid(newSolid);
		}
		
		if (drw->getInputJoint()) {
			setInputJoint(drw->getInputJoint());
		}
		
		if (drw->getOutputJoint()){
			setOutputJoint(drw->getOutputJoint());
		}
	}
}
	
}

