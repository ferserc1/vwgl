
#include <vwgl/archive_file.hpp>
#include <vwgl/system.hpp>

#include <iostream>

namespace vwgl {

ArchiveFile::ArchiveFile() 
	:_status(kStatusClosed)
{

}

bool ArchiveFile::open(const std::string & packagePath, OpenMode mode) {
	bool status = mode == kModePack ? prepareToPack(packagePath) : prepareToUnpack(packagePath);
	if (status) {
		_status = static_cast<PackageStatus>(mode);
		_packagePath = System::get()->getPath(packagePath);
		_packageFileName = System::get()->getFileName(packagePath);
	}
	return status;
}

bool ArchiveFile::addFile(const std::string & filePath, const std::string & packagePath) {
	bool status = false;
	if (_status == kStatusPack) {
		if (System::get()->fileExists(filePath)) {
			std::string fileName = System::get()->addPathComponent(packagePath,System::get()->getLastPathComponent(filePath));

			std::fstream inFile;
			inFile.open(filePath, std::ios::binary | std::ios::in);
			if (!inFile.is_open()) {
				return false;
			}
			inFile.seekg(0, inFile.end);
			int fileSize = static_cast<int>(inFile.tellg());
			inFile.seekg(0, inFile.beg);

			_packageFile.writeString(fileName);
			_packageFile.writeInteger(fileSize);
			char * buffer = new char[fileSize];
			inFile.read(buffer, fileSize);
			_packageFile.stream().write(buffer, fileSize);
			delete [] buffer;
			inFile.close();
		}
	}
	return status;
}

bool ArchiveFile::unpackTo(const std::string & path) {
	bool status = false;
	if (_status == kStatusUnpack) {
		// Create output folder
		std::string location = System::get()->getPath(path);
		if (!System::get()->directoryExists(location)) {
			status = false;
		}
		else if (!System::get()->directoryExists(path)) {
			System::get()->mkdir(path);
		}

		// Unpack files
		std::string filePath;
		int fileSize = 0;
		char * buffer = nullptr;
		while (!_packageFile.stream().eof()) {
			status = _packageFile.readString(filePath);
			status = _packageFile.readInteger(fileSize);
			std::cout << "Unpacking file " << filePath << " to path " << path << std::endl;
			if (status && fileSize>0) {
				buffer = new char[fileSize];
				_packageFile.stream().read(buffer, fileSize);

				std::string outDirectory = System::get()->getPath(filePath);
				if (!outDirectory.empty()) {
					outDirectory = System::get()->addPathComponent(path, outDirectory);
					System::get()->mkdir(outDirectory);
				}

				std::fstream outFile(System::get()->addPathComponent(path, filePath), std::ios::binary | std::ios::out);
				outFile.write(buffer, fileSize);
				outFile.close();
				delete [] buffer;
				std::cout << "File unpacked" << std::endl;
			}
		}
		std::cout << "All files unpacked" << std::endl;
	}

	return status;
}

bool ArchiveFile::close() {
	if (!_packageFile.stream().is_open() || _status == kStatusClosed) {
		return false;
	}
	_packageFile.close();
	return true;
}

bool ArchiveFile::prepareToPack(const std::string & path) {
	if (_packageFile.stream().is_open() || _status != kStatusClosed) {
		return false;
	}
	return _packageFile.open(path, VwglbUtils::kModeWrite);
}

bool ArchiveFile::prepareToUnpack(const std::string & path) {
	if (_packageFile.stream().is_open() || _status != kStatusClosed) {
		return false;
	}
	return _packageFile.open(path, VwglbUtils::kModeRead);
}
	

}
