
#include <vwgl/vwglbwritter.hpp>
#include <vwgl/generic_material.hpp>

#include <sstream>

namespace vwgl {

VwglbWritter::VwglbWritter() {
}

VwglbWritter::~VwglbWritter() {
}

bool VwglbWritter::acceptFileType(const std::string & path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext=="vwglb";
}

bool VwglbWritter::writeDrawable(const std::string & path, Drawable * drawable) {
	if (_fileUtils.open(path, VwglbUtils::kModeWrite)) {
		removeLastPathComponent(path, _dstPath);
		writeHeader(drawable);
		
		SolidList slist = drawable->getSolidList();
		SolidList::iterator it;
		for (it=slist.begin(); it!=slist.end(); ++it) {
			writePolyList((*it).getPtr());
		}
		_fileUtils.writeBlock(VwglbUtils::kEnd);
		_fileUtils.close();
		return true;
	}
	return false;
}

void VwglbWritter::writeHeader(Drawable * drawable) {
	_fileUtils.setBigEndian();
	
	//// Write header
	// File endian 0=big endian, 1=little endian
	_fileUtils.writeByte(0);
	
	// Version (major, minor, rev)
	_fileUtils.writeByte(1);
	_fileUtils.writeByte(1);
	_fileUtils.writeByte(0);
	
	// File type
	_fileUtils.writeBlock(VwglbUtils::kHeader);
	
	// Number of poly list
	SolidList solidList = drawable->getSolidList();
	_fileUtils.writeInteger(static_cast<int>(solidList.size()));

	// Materials (as json string)
	ensurePolyListNames(drawable);	// Ensure that all poly list have name and material name
	writeMaterial(drawable);
	writeShadow(drawable);
	writeJoints(drawable);
}
	
void VwglbWritter::ensurePolyListNames(Drawable * drawable) {
	SolidList solids = drawable->getSolidList();
	SolidList::iterator it;
	std::unordered_map<std::string,std::string> plistNames;
	std::unordered_map<std::string,std::string> materialNames;
	
	int plistIndex = 0;
	int matIndex = 0;
	for (it=solids.begin();it!=solids.end();++it) {
		Solid * s = (*it).getPtr();
		PolyList * p = s->getPolyList();
		std::string plistname = p->getName();
		std::string matName = p->getMaterialName();
		std::stringstream newplistname;
		std::stringstream newmatname;
		if (plistname=="" || plistNames.find(plistname)!=plistNames.end()) {
			newplistname << "polyList_" << plistIndex;
			plistname = newplistname.str();
			p->setName(plistname);
			++plistIndex;
		}
		if (matName=="" || materialNames.find(matName)!=materialNames.end()) {
			newmatname << "material_" << matIndex;
			matName = newmatname.str();
			p->setMaterialName(matName);
			++matIndex;
		}
		plistNames[plistname] = plistname;
		materialNames[matName] = matName;
	}
}

void VwglbWritter::writeMaterial(Drawable * drawable) {
	SolidList solidList = drawable->getSolidList();
	
	SolidList::iterator it;
	std::stringstream materials;
	materials << "[";
	
	std::string separator = "";
	for (it=solidList.begin(); it!=solidList.end(); ++it) {
		Solid * s = (*it).getPtr();
		PolyList * p = s->getPolyList();
		GenericMaterial * m = dynamic_cast<GenericMaterial*>(s->getMaterial());
		if (m) {
			materials << separator << "{";
			materials << "\"name\":\"" << p->getMaterialName() << "\",";
			materials << "\"class\":\"GenericMaterial\",";
			materials << "\"diffuseR\":" << m->getDiffuse().r() << ",";
			materials << "\"diffuseG\":" << m->getDiffuse().g() << ",";
			materials << "\"diffuseB\":" << m->getDiffuse().b() << ",";
			materials << "\"diffuseA\":" << m->getDiffuse().a() << ",";

			materials << "\"specularR\":" << m->getSpecular().r() << ",";
			materials << "\"specularG\":" << m->getSpecular().g() << ",";
			materials << "\"specularB\":" << m->getSpecular().b() << ",";
			materials << "\"specularA\":" << m->getSpecular().a() << ",";
			
			materials << "\"shininess\":" << m->getShininess() << ",";
			materials << "\"refractionAmount\":" << m->getRefractionAmount() << ",";
			materials << "\"reflectionAmount\":" << m->getReflectionAmount() << ",";
			materials << "\"lightEmission\":" << m->getLightEmission() << ",";
			if (m->getTexture()) {
				std::string textureFile;
				getFileName(m->getTexture()->getFileName(), textureFile);
				materials << "\"texture\":\"" << textureFile << "\",";
				saveTextureFile(m->getTexture()->getFileName());
			}
			if (m->getLightMap()) {
				std::string textureFile;
				getFileName(m->getLightMap()->getFileName(), textureFile);
				materials << "\"lightmap\":\"" << textureFile << "\",";
				saveTextureFile(m->getLightMap()->getFileName());
			}
			if (m->getNormalMap()) {
				std::string textureFile;
				getFileName(m->getNormalMap()->getFileName(), textureFile);
				materials << "\"normalMap\":\"" << textureFile << "\",";
				saveTextureFile(m->getNormalMap()->getFileName());
			}
			
			materials << "\"textureOffsetX\":" << m->getTextureOffset().x() << ",";
			materials << "\"textureOffsetY\":" << m->getTextureOffset().y() << ",";
			materials << "\"textureScaleX\":" << m->getTextureScale().x() << ",";
			materials << "\"textureScaleY\":" << m->getTextureScale().y() << ",";
			
			materials << "\"lightmapOffsetX\":" << m->getLightmapOffset().x() << ",";
			materials << "\"lightmapOffsetY\":" << m->getLightmapOffset().y() << ",";
			materials << "\"lightmapScaleX\":" << m->getLightmapScale().x() << ",";
			materials << "\"lightmapScaleY\":" << m->getLightmapScale().y() << ",";
			
			materials << "\"normalMapOffsetX\":" << m->getNormalMapOffset().x() << ",";
			materials << "\"normalMapOffsetY\":" << m->getNormalMapOffset().y() << ",";
			materials << "\"normalMapScaleX\":" << m->getNormalMapScale().x() << ",";
			materials << "\"normalMapScaleY\":" << m->getNormalMapScale().y() << ",";
			
			
			materials << "\"castShadows\":" << (m->getCastShadows() ? "true":"false") << ",";
			materials << "\"receiveShadows\":" << (m->getReceiveShadows() ? "true":"false") << ",";
			materials << "\"receiveProjections\":" << (m->getReceiveProjections() ? "true":"false") << ",";
			materials << "\"alphaCutoff\":" << m->getAlphaCutoff() << ",";
			
			if (m->getShininessMask()) {
				std::string textureFile;
				getFileName(m->getShininessMask()->getFileName(), textureFile);
				materials << "\"shininessMask\":\"" << textureFile << "\",";
				saveTextureFile(m->getShininessMask()->getFileName());
			}
			materials << "\"shininessMaskChannel\":" << m->getShininessMaskChannel() << ",";
			materials << "\"invertShininessMask\":" << (m->getInvertShininessMask() ? "true":"false") << ",";

			if (m->getLightEmissionMask()) {
				std::string textureFile;
				getFileName(m->getLightEmissionMask()->getFileName(), textureFile);
				materials << "\"lightEmissionMask\":\"" << textureFile << "\",";
				saveTextureFile(m->getLightEmissionMask()->getFileName());
			}
			materials << "\"lightEmissionMaskChannel\":" << m->getLightEmissionMaskChannel() << ",";
			materials << "\"invertLightEmissionMask\":" << (m->getInvertLightEmissionMask() ? "true":"false") << ",";

			materials << "\"cullFace\":" << (m->getCullFace() ? "true":"false");
			
			// Shadow projector
			if (s->getShadowProjector() &&
				s->getShadowProjector()->getTexture() &&
				s->getShadowProjector()->getTexture()->getFileName()!="") {
				materials << ",";
				Projector * proj = s->getShadowProjector();
				Texture * tex = proj->getTexture();
				std::string textureFile;
				getFileName(tex->getFileName(), textureFile);
				materials << "\"shadowTexture\":\"" << textureFile << "\",";
				saveTextureFile(tex->getFileName());
				
				const float * projection = proj->getProjection().raw();
				const float * transform = proj->getRawTransform().raw();
				materials << "\"shadowAttenuation\":" << proj->getAttenuation() << ",";
				materials << "\"shadowProjection\":\"";
				for (int i=0;i<16;++i) {
					materials << projection[i];
					if (i<15) materials << " ";
				}
				materials << "\",";
				
				materials << "\"shadowTransform\":\"";
				for (int i=0;i<16;++i) {
					materials << transform[i];
					if (i<15) materials << " ";
				}
				materials << "\"";
			}
			
			// Visibility and group name
			materials << ",";
			materials << "\"visible\":" << s->isVisible() << ",";
			materials << "\"groupName\":\"" << s->getGroupName() << "\"";

			materials << "}";
			separator = ",";
		}
	}
	
	materials << "]";
	_fileUtils.writeBlock(VwglbUtils::kMaterials);
	_fileUtils.writeString(materials.str());
}
	
void VwglbWritter::writeShadow(Drawable * drawable) {
	Projector * proj;
	Texture * tex;
	if ((proj=drawable->getShadowProjector()) && (tex=proj->getTexture())) {
		_fileUtils.writeBlock(vwgl::VwglbUtils::kShadowProjector);
		std::string file;
		getFileName(tex->getFileName(), file);
		const float * projection = proj->getProjection().raw();
		const float * transform = proj->getRawTransform().raw();
		_fileUtils.writeString(file);
		_fileUtils.writeFloat(proj->getAttenuation());
		for (int i=0;i<16;++i) {
			_fileUtils.writeFloat(projection[i]);
		}
		for (int i=0;i<16;++i) {
			_fileUtils.writeFloat(transform[i]);
		}
		saveTextureFile(tex->getFileName());
	}
}

void VwglbWritter::writeJoints(Drawable * drawable) {
	// TODO: Support for multiple types of joints, and multiple output joints
	physics::LinkJoint * inJoint = dynamic_cast<physics::LinkJoint*>(drawable->getInputJoint());
	physics::LinkJoint * outJoint = dynamic_cast<physics::LinkJoint*>(drawable->getOutputJoint());
	if (inJoint || outJoint) {
		_fileUtils.writeBlock(vwgl::VwglbUtils::kJoint);
		std::stringstream joints;
		joints << "{";
	
		// In joint
		if (inJoint) {
			Vector3 offset = inJoint->offset();
			float pitch = inJoint->getPitch();
			float roll = inJoint->getRoll();
			float yaw = inJoint->getYaw();
			joints << "\"input\":{";
			joints << "\"type\":\"LinkJoint\",";
			joints << "\"offset\":[" << offset.x() << "," << offset.y() << "," << offset.z() << "],";
			joints << "\"pitch\":" << pitch << ",\"roll\":" << roll << ",\"yaw\":" << yaw;
			joints << "}";
			if (outJoint) joints << ",";
		}
		
		// Out joint
		if (outJoint) {
			Vector3 offset = outJoint->offset();
			float pitch = outJoint->getPitch();
			float roll = outJoint->getRoll();
			float yaw = outJoint->getYaw();
			joints << "\"output\":[{";
			joints << "\"type\":\"LinkJoint\",";
			joints << "\"offset\":[" << offset.x() << "," << offset.y() << "," << offset.z() << "],";
			joints << "\"pitch\":" << pitch << ",\"roll\":" << roll << ",\"yaw\":" << yaw;
			joints << "}]";
		}
		
		joints << "}";
		_fileUtils.writeString(joints.str());
	}
}

void VwglbWritter::writePolyList(Solid * solid) {
	_fileUtils.writeBlock(VwglbUtils::kPolyList);
	_fileUtils.writeBlock(VwglbUtils::kPlistName);
	_fileUtils.writeString(solid->getPolyList()->getName());
	_fileUtils.writeBlock(VwglbUtils::kMatName);
	_fileUtils.writeString(solid->getPolyList()->getMaterialName());
	
	PolyList * plist = solid->getPolyList();
	if (plist->getVertexCount()>0) {
		_fileUtils.writeBlock(VwglbUtils::kVertexArray);
		_fileUtils.writeArray(plist->getVertexVector());
	}

	if (plist->getNormalCount()>0) {
		_fileUtils.writeBlock(VwglbUtils::kNormalArray);
		_fileUtils.writeArray(plist->getNormalVector());
	}
	
	if (plist->getTexCoord0Count()>0) {
		_fileUtils.writeBlock(VwglbUtils::kTexCoord0Array);
		_fileUtils.writeArray(plist->getTexCoord0Vector());
	}
	
	if (plist->getTexCoord1Count()>0) {
		_fileUtils.writeBlock(VwglbUtils::kTexCoord1Array);
		_fileUtils.writeArray(plist->getTexCoord1Vector());
	}
	
	if (plist->getTexCoord2Count()>0) {
		_fileUtils.writeBlock(VwglbUtils::kTexCoord2Array);
		_fileUtils.writeArray(plist->getTexCoord2Vector());
	}
	
	if (plist->getIndexCount()>0) {
		_fileUtils.writeBlock(VwglbUtils::kIndexArray);
		_fileUtils.writeArray(plist->getIndexVector());
	}
}
	
void VwglbWritter::saveTextureFile(const std::string & imagePath) {
	std::string fileName;
	getFileName(imagePath, fileName);
	std::string dstPath = _dstPath + fileName;
	
	if (dstPath!=imagePath) {
		std::ifstream inFile(imagePath,std::ios::binary);
		if (inFile) {
			std::ofstream dstFile(dstPath,std::ios::binary);
			if (dstFile) {
				dstFile << inFile.rdbuf();
				dstFile.close();
			}
			inFile.close();
		}
	}
}

}
