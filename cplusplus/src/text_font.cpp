
#include <vwgl/text_font.hpp>

#include <vwgl/font_manager.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {

TextFont::TextFont(const std::string & fontName, int size, int res, const std::string & path)
	:_fontName(fontName)
	,_size(size)
	,_res(res)
	,_spaceSize(0)
	,_lineHeight(0)
	,_path(path)
{
	FontManager::get()->registerFont(this);
}
	
	
TextFont::~TextFont() {
	FontManager::get()->unregisterFont(this);
}
	
std::string TextFont::getDefaultFontPath(unsigned int type) {
	std::string resourcesPath = System::get()->getResourcesPath();
	if ((type & kTypeSansSerif) && (type & kTypeMonospace)) {
		// monospace sans-serif
		return System::get()->addPathComponent(resourcesPath, "Inconsolata-Regular.ttf");
	}
	else if ((type & kTypeSansSerif) && (type & kTypeMonospace)==0) {
		// sans-serif
		return System::get()->addPathComponent(resourcesPath, "OpenSans-Regular.ttf");
	}
	else if ((type & kTypeSerif) && (type & kTypeMonospace)) {
		// serif monospace
		return System::get()->addPathComponent(resourcesPath, "AnonymousPro-Regular.ttf");
	}
	else if ((type & kTypeSerif) && (type & kTypeMonospace)==0) {
		// serif
		return System::get()->addPathComponent(resourcesPath, "Lora-Regular.ttf");
	}
	else if (type & kTypeMonospace) {
		// Monospace
		return System::get()->addPathComponent(resourcesPath, "Inconsolata-Regular.ttf");
	}
	else {
		// Default: sans-serif
		return System::get()->addPathComponent(resourcesPath, "OpenSans-Regular.ttf");
	}
}
	
}