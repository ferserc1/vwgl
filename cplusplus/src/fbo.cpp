
#include <vwgl/opengl.hpp>
#include <vwgl/fbo.hpp>
#include <vwgl/system.hpp>
#include <vwgl/glerror.hpp>
#include <iostream>

namespace vwgl {

FramebufferObject::FramebufferObject()
	:_fbo(0),
	_width(0),
	_height(0)
{
}

FramebufferObject::FramebufferObject(int width, int height)
	:_fbo(0),
	_width(0),
	_height(0)
{
	create(width, height);
}
	
FramebufferObject::FramebufferObject(int width, int height, FBOType type)
	:_fbo(0),
	_width(0),
	_height(0)
{
	create(width, height, type);
}

FramebufferObject::FramebufferObject(const Size2Di & size)
	:_fbo(0),
	_width(0),
	_height(0)
{
	create(size);
}

FramebufferObject::FramebufferObject(const Size2Di & size, FBOType type)
	:_fbo(0),
	_width(0),
	_height(0)
{
	create(size, type);
}
	
FramebufferObject::~FramebufferObject() {
	destroy();
}

void FramebufferObject::create(int width, int height, FBOType type) {
	destroy();
	genFramebuffer(Size2Di(width,height));
	bind();
	
	if (type==kTypeDepthTexture) {
		addAttachment(kAttachmentColor0, kTypeDefault);
		addAttachment(kAttachmentDepth, kTypeDepthTexture);
	}
	else if (type==kTypeFloatTexture) {
		addAttachment(kAttachmentColor0, kTypeFloatTexture);
		addAttachment(kAttachmentDepth, kTypeDepthRenderbuffer);
	}
	else if (type==kTypeDefault) {
		addAttachment(kAttachmentColor0, kTypeDefault);
		addAttachment(kAttachmentDepth, kTypeDepthRenderbuffer);
	}
	else if (type==kTypeCubeMap) {
		addAttachment(kAttachmentColor0, kTypeCubeMap);
		addAttachment(kAttachmentDepth, kTypeDepthRenderbuffer);
	}
	unbind();
}
	
void FramebufferObject::genFramebuffer(const Size2Di &size) {
	_width = size.width();
	_height = size.height();
	glGenFramebuffers(1, &_fbo);
}
	
bool FramebufferObject::addAttachment(vwgl::FramebufferObject::Attachment att, vwgl::FramebufferObject::FBOType type) {
	switch (type) {
		case kTypeDefault: {
				Texture * texture = new Texture();
				texture->createTexture();
				texture->bindTexture(Texture::kTargetTexture2D);
				texture->setMinFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
				texture->setMagFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
				texture->setTextureWrapS(Texture::kTargetTexture2D, Texture::kWrapClamp);
				texture->setTextureWrapT(Texture::kTargetTexture2D, Texture::kWrapClamp);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0L);
				glFramebufferTexture2D(GL_FRAMEBUFFER, att, GL_TEXTURE_2D, texture->getTextureName(), 0);
			
				AttachmentData attData(texture, type);
				_textureMap[att] = attData;
				texture->unbindTexture(Texture::kTargetTexture2D);
			}
			break;
		case kTypeMultiTarget: {
				Texture * texture = new Texture();
				texture->createTexture();
				texture->bindTexture(Texture::kTargetTexture2D);
				texture->setMinFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
				texture->setMagFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
				texture->setTextureWrapS(Texture::kTargetTexture2D, Texture::kWrapClamp);
				texture->setTextureWrapT(Texture::kTargetTexture2D, Texture::kWrapClamp);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0L);
				glFramebufferTexture(GL_FRAMEBUFFER, att, texture->getTextureName(), 0);

				AttachmentData attData(texture, type);
				_textureMap[att] = attData;
				texture->unbindTexture(Texture::kTargetTexture2D);
			}
			break;
		case kTypeDepthTexture: {
				if (att==kAttachmentDepth) {
					Texture * depthTexture = new Texture();
					depthTexture->createTexture();
					depthTexture->bindTexture(Texture::kTargetTexture2D);
					depthTexture->setMinFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
					depthTexture->setMagFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
					depthTexture->setTextureWrapS(Texture::kTargetTexture2D, Texture::kWrapClamp);
					depthTexture->setTextureWrapT(Texture::kTargetTexture2D, Texture::kWrapClamp);
					glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, _width, _height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, 0L);
					
					glFramebufferTexture2D(GL_FRAMEBUFFER, att, GL_TEXTURE_2D, depthTexture->getTextureName(), 0);
					
					AttachmentData attData(depthTexture,type);
					_textureMap[att] = attData;
					
					depthTexture->unbindTexture(Texture::kTargetTexture2D);
				}
				else {
					return false;
				}
			}
			break;
		case kTypeFloatTexture: {
				Texture * texture = new Texture();
				texture->createTexture();
				texture->bindTexture(Texture::kTargetTexture2D);
				texture->setMinFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
				texture->setMagFilter(Texture::kTargetTexture2D, Texture::kFilterNearest);
				texture->setTextureWrapS(Texture::kTargetTexture2D, Texture::kWrapClamp);
				texture->setTextureWrapT(Texture::kTargetTexture2D, Texture::kWrapClamp);
				
	#ifdef VWGL_OPENGL_ES
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, _width, _height, 0, GL_RGBA, GL_HALF_FLOAT_OES, 0L);
	#else
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, _width, _height, 0, GL_RGBA, GL_FLOAT, 0L);
	#endif
			
				glFramebufferTexture2D(GL_FRAMEBUFFER, att, GL_TEXTURE_2D, texture->getTextureName(), 0);
			
				AttachmentData attachData = AttachmentData(texture, type);
				_textureMap[att] = attachData;
			
				texture->unbindTexture(Texture::kTargetTexture2D);
			}
			break;
		case kTypeCubeMap: {
				CubeMap * cm = new CubeMap();
				cm->createTexture();
				cm->bindTexture(Texture::kTargetTextureCubeMap);
				cm->setMinFilter(Texture::kTargetTextureCubeMap, Texture::kFilterLinear);
				cm->setMagFilter(Texture::kTargetTextureCubeMap, Texture::kFilterLinear);
				cm->setTextureWrapS(Texture::kTargetTextureCubeMap, Texture::kWrapClamp);
				cm->setTextureWrapT(Texture::kTargetTextureCubeMap, Texture::kWrapClamp);
				cm->setTextureWrapR(Texture::kTargetTextureCubeMap, Texture::kWrapClamp);
				
				for (int i=0; i<6; ++i) {
					glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
				}
				
				glFramebufferTexture2D(GL_FRAMEBUFFER, att, GL_TEXTURE_CUBE_MAP_POSITIVE_X, cm->getTextureName(), 0);
				AttachmentData attachment(cm, type);
				_textureMap[att] = attachment;
			
				cm->unbindTexture(Texture::kTargetTextureCubeMap);
			}
			break;
		case kTypeDepthRenderbuffer: {
				if (att==kAttachmentDepth) {
					GLuint renderbuffer;
					glGenRenderbuffers(1, &renderbuffer);
					glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
					glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, _width, _height);
					glFramebufferRenderbuffer(GL_FRAMEBUFFER, att, GL_RENDERBUFFER, _fbo);
					glBindRenderbuffer(GL_RENDERBUFFER, 0L);
					AttachmentData attachment(renderbuffer);
					_textureMap[att] = attachment;
				}
				else {
					return false;
				}
			}
			
			break;
		default:
			break;
	}

	return true;
}

void FramebufferObject::drawBuffers(int size, const GLuint att[]) {
	glDrawBuffers(size, att);
}
	
void FramebufferObject::resize(int width, int height) {
	_width = width;
	_height = height;
	bind();

	TextureMap::iterator it;
	
	for (it=_textureMap.begin(); it!=_textureMap.end(); ++it) {
		AttachmentData data = it->second;
		FBOType type = data.getType();
		GLuint renderbuffer = data.getRenderBuffer();
		ptr<Texture> texture = data.getTexture();
		
		if (type==kTypeDepthTexture && texture.valid()) {
			texture->bindTexture(Texture::kTargetTexture2D);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, _width, _height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, 0L);
			texture->unbindTexture(Texture::kTargetTexture2D);
		}
		else if (type==kTypeFloatTexture && texture.valid()) {
			texture->bindTexture(Texture::kTargetTexture2D);
			
#ifdef VWGL_OPENGL_ES
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, _width, _height, 0, GL_RGBA, GL_HALF_FLOAT_OES, 0L);
#else
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, _width, _height, 0, GL_RGBA, GL_FLOAT, 0L);
#endif
			texture->unbindTexture(Texture::kTargetTexture2D);
		}
		else if ((type == kTypeDefault || type == kTypeMultiTarget) && texture.valid()) {
			texture->bindTexture(Texture::kTargetTexture2D);
			texture->setMinFilter(Texture::kTargetTexture2D, Texture::kFilterLinear);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0L);
			texture->unbindTexture(Texture::kTargetTexture2D);
		}
		else if (type==kTypeCubeMap && dynamic_cast<CubeMap*>(texture.getPtr())) {
			CubeMap * cm = dynamic_cast<CubeMap*>(texture.getPtr());
			cm->bindTexture(Texture::kTargetTextureCubeMap);
			
			for (int i=0; i<6; ++i) {
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			}
		}
		else if (type==kTypeDepthRenderbuffer) {
			glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, _width, _height);
			glBindRenderbuffer(GL_RENDERBUFFER, 0);
		}
	}
	unbind();
}

void FramebufferObject::setTextureColorTarget(Attachment attachment, Texture::TextureTarget target) {
	Texture * tex = getTexture(attachment);
	if (tex) {
		glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, target, tex->getTextureName(), 0);
	}
}

void FramebufferObject::bind() {
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
}

void FramebufferObject::unbind() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	System::get()->bindGLKViewDrawable();
}

void FramebufferObject::readColor(const Viewport & rectangle, Attachment readFrom, unsigned char * pixels) {
	bind();
	glReadBuffer(readFrom);
	glReadPixels(rectangle.x(), rectangle.y(), rectangle.width(), rectangle.height(), GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	unbind();
}


void FramebufferObject::destroy() {
	unbind();
	if (_fbo!=0) {
		glDeleteBuffers(1, &_fbo);
	}
	if (_textureMap.find(kAttachmentDepth)!=_textureMap.end()) {
		AttachmentData data = _textureMap[kAttachmentDepth];
		if (data.getTexture()==nullptr) {
			GLuint renderbuffer = data.getRenderBuffer();
			glDeleteRenderbuffers(1, &renderbuffer);
		}
	}
	_fbo = 0;
	_textureMap.clear();
}

}