
#include <vwgl/image.hpp>

#import <UIKit/UIKit.h>

namespace vwgl {

Image::Image()
	:_buffer(0L),
	_format(kFormatNone),
	_bytesPerPixel(0),
	_width(0),
	_height(0),
	_lastStatus(true)
{
	
}
	
Image::Image(const std::string & path)
	:_buffer(0L),
	_format(kFormatNone),
	_bytesPerPixel(0),
	_width(0),
	_height(0),
	_lastStatus(true)
{
	load(path);
}

Image::~Image() {
	destroy();
}

Image * Image::load(const std::string &path) {
	destroy();
	NSString * sPath = [NSString stringWithUTF8String:path.c_str()];
	NSData * imgData = [NSData dataWithContentsOfFile:sPath];
	UIImage * image = [UIImage imageWithData:imgData];
	if (image) {
		_format = kFormatRGBA;
		_width = image.size.width;
		_height = image.size.height;
		_bytesPerPixel = 4;
		CGImageRef cgimage = [image CGImage];
		CFDataRef data = CGDataProviderCopyData(CGImageGetDataProvider(cgimage));
		const unsigned char * buffer =  CFDataGetBytePtr(data);
		_buffer = new unsigned char[_width*_height*_bytesPerPixel];
		for (unsigned int i=0; i<_height; ++i) {
			memcpy(&_buffer[(_height-i-1)*_width*_bytesPerPixel], &buffer[i*_width*_bytesPerPixel], _width*_bytesPerPixel);
		}
		_fileName = path;
		CFRelease(data);
		
	}
	_lastStatus = _buffer!=NULL ? true:false;
	return this;
}

Image * Image::saveBMP(const std::string & path) {
	//this->save(path, SOIL_SAVE_TYPE_BMP);
	return this;
}

Image * Image::saveTGA(const std::string & path) {
	//this->save(path, SOIL_SAVE_TYPE_TGA);
	return this;
}
	
void Image::save(const std::string & path, int format) {
	if (!valid()) {
		_lastStatus = false;
	}
	else {
	//	_lastStatus = SOIL_save_image(path.c_str(), format, _width, _height, _bytesPerPixel, _buffer)==0 ? false:true;
	}
	
}


void Image::destroy() {
	if (valid()) {
		delete [] _buffer;
		_buffer = 0L;
		_format = kFormatNone;
		_bytesPerPixel = 0;
		_width = 0;
		_height = 0;
	}
}
	
}