#import <UIKit/UIKit.h>

#include <vwgl/generic_material.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/system.hpp>
#include <vwgl/texture_manager.hpp>

namespace vwgl {

void ios_vwgl_get_color(NSNumber * r, NSNumber * g, NSNumber * b, NSNumber * a, Color & color) {
	color.r(r ? r.floatValue:1.0);
	color.g(g ? g.floatValue:1.0);
	color.b(b ? b.floatValue:1.0);
	color.a(a ? a.floatValue:1.0);
}
	
float ios_vwgl_floatValue(NSNumber * num, float defaultValue) {
	return num ? num.floatValue:defaultValue;
}

bool ios_vwgl_boolValue(NSNumber * num, bool defaultValue) {
	return num ? num.boolValue:defaultValue;
}
	
Texture * ios_vwgl_texture(NSString * texture, const std::string & path) {
	ptr<Texture> result;
	if (texture) {
		std::string fileName = texture.UTF8String;
		if (!vwgl::System::get()->isAbsolutePath(fileName)) {
			fileName = path + fileName;
		}
		result = Loader::get()->loadTexture(fileName);
	}
	return result.release();
}
	
void ios_vwgl_vector2(NSNumber * x, NSNumber * y, Vector2 & vec) {
	if (x) vec.x(x.floatValue);
	if (y) vec.y(y.floatValue);
}

void ios_vwgl_generic_material_applyjson(NSDictionary * material, Solid * solid, const std::string & currentDir) {
	ptr<GenericMaterial> mat = new GenericMaterial();
	solid->setMaterial(mat.getPtr());
	
	Color diffuse;
	Color specular;
	ios_vwgl_get_color(material[@"diffuseR"],
					   material[@"diffuseG"],
					   material[@"diffuseB"],
					   material[@"diffuseA"], diffuse);
	ios_vwgl_get_color(material[@"specularR"],
					   material[@"specularG"],
					   material[@"specularB"],
					   material[@"specularA"], specular);
	mat->setDiffuse(diffuse);
	mat->setSpecular(specular);
	mat->setShininess(ios_vwgl_floatValue(material[@"shininess"], 0.0f));
	mat->setLightEmission(ios_vwgl_floatValue(material[@"lightEmission"], 0.0f));
	
	mat->setTexture(ios_vwgl_texture(material[@"texture"], currentDir));
	mat->setLightMap(ios_vwgl_texture(material[@"lightmap"], currentDir));
	mat->setNormalMap(ios_vwgl_texture(material[@"normalMap"], currentDir));
	mat->setLightNormalMap(ios_vwgl_texture(material[@"lightNormalMap"], currentDir));
	
	Vector2 vec2;
	ios_vwgl_vector2(material[@"textureOffsetX"], material[@"textureOffsetY"], vec2);
	mat->setTextureOffset(vec2);
	ios_vwgl_vector2(material[@"textureScaleX"], material[@"textureScaleY"], vec2);
	mat->setTextureScale(vec2);
	
	ios_vwgl_vector2(material[@"normalMapOffsetX"], material[@"normalMapOffsetY"], vec2);
	mat->setNormalMapOffset(vec2);
	ios_vwgl_vector2(material[@"normalMapScaleX"], material[@"normalMapScaleY"], vec2);
	mat->setNormalMapScale(vec2);
	
	mat->setReflectionAmount(ios_vwgl_floatValue(material[@"reflectionAmount"], 0.0f));
	mat->setRefractionAmount(ios_vwgl_floatValue(material[@"refractionAmount"], 0.0f));
	
	mat->setCastShadows(ios_vwgl_boolValue(material[@"castShadows"], true));
	mat->setReceiveShadows(ios_vwgl_boolValue(material[@"receiveShadows"], true));
	mat->setReceiveProjections(ios_vwgl_boolValue(material[@"receiveProjections"], false));
}

void GenericMaterial::setMaterialsWithJson(Drawable * drawable, const std::string & jsonString) {
	std::string currentDir = Loader::get()->getCurrentDir();
	
	NSData * jsonData = [[NSString stringWithUTF8String:jsonString.c_str()] dataUsingEncoding:NSUTF8StringEncoding];
	id materials = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
	
	NSMutableDictionary * materialByName = [NSMutableDictionary dictionary];
	for (id materialItem in materials) {
		NSString * key = materialItem[@"name"];
		materialByName[key] = materialItem;
	}
	
	SolidList solidList = drawable->getSolidList();
	SolidList::iterator it;
	for (it=solidList.begin(); it!=solidList.end(); ++it) {
		Solid * s = (*it).getPtr();
		PolyList * p = s->getPolyList();
		NSString * matName = [NSString stringWithUTF8String:p->getMaterialName().c_str()];
		ios_vwgl_generic_material_applyjson(materialByName[matName], s, currentDir);
	}
}

}