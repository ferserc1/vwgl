
#include <vwgl/readerplugin.hpp>
#include <algorithm>

namespace vwgl {

void Plugin::getFileExtension(const std::string &path, std::string &extension) {
	std::string::const_iterator pivot = std::find(path.rbegin(), path.rend(), '.' ).base();
	std::string ext( pivot, path.end() );
	extension = ext;
}

void Plugin::removeLastPathComponent(const std::string & filePath, std::string & path) {
	std::string::const_iterator pivot = std::find(filePath.rbegin(), filePath.rend(), '/' ).base();
	std::string basename( pivot, filePath.end() );
	std::string dirname( filePath.begin(), pivot == filePath.begin() ? pivot : pivot - 1 );
	path = dirname + "/";
}

void Plugin::getFileName(const std::string & path, std::string & fileName) {
	std::string::const_iterator pivot = std::find(path.rbegin(), path.rend(), '/' ).base();
	std::string basename( pivot, path.end() );
	fileName = basename;
}

void Plugin::toLower(std::string &str) {
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}

}