
#include <vwgl/boundingbox.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/group.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/transform.hpp>

#include <float.h>

namespace vwgl {

void BuildBoundingBoxVisitor::visit(Node * node) {
	Group * grp = dynamic_cast<Group*>(node);
	DrawableNode * drw = dynamic_cast<DrawableNode*>(node);
	if (grp) {
		NodeList::iterator it;
		for (it=grp->getNodeList().begin(); it!=grp->getNodeList().end(); ++it) {
			visit((*it).getPtr());
		}
	}
	if (drw) {
		_boundingBox->addVertexData(drw->getDrawable());
	}
}
	

BoundingBox::BoundingBox()
	:_left(FLT_MAX)
	,_right(-FLT_MAX)
	,_top(-FLT_MAX)
	,_bottom(FLT_MAX)
	,_back(FLT_MAX)
	,_front(-FLT_MAX)
	,_max(-FLT_MAX)
	,_min(FLT_MAX)
	,_size(0.0f)
	,_halfSize(0.0f)
	,_center(0.0f)
{
}

BoundingBox::BoundingBox(const float * vertexData, unsigned int length)
	:_left(FLT_MAX)
	,_right(-FLT_MAX)
	,_top(-FLT_MAX)
	,_bottom(FLT_MAX)
	,_back(FLT_MAX)
	,_front(-FLT_MAX)
	,_max(-FLT_MAX)
	,_min(FLT_MAX)
	,_size(0.0f)
	,_halfSize(0.0f)
	,_center(0.0f)
{
	setVertexData(vertexData, length);
}
	
BoundingBox::BoundingBox(const PolyList * plist)
	:_left(FLT_MAX)
	,_right(-FLT_MAX)
	,_top(-FLT_MAX)
	,_bottom(FLT_MAX)
	,_back(FLT_MAX)
	,_front(-FLT_MAX)
	,_max(-FLT_MAX)
	,_min(FLT_MAX)
	,_size(0.0f)
	,_halfSize(0.0f)
	,_center(0.0f)
{
	setVertexData(plist);
}

BoundingBox::BoundingBox(const DrawableBase * drawable)
	:_left(FLT_MAX)
	,_right(-FLT_MAX)
	,_top(-FLT_MAX)
	,_bottom(FLT_MAX)
	,_back(FLT_MAX)
	,_front(-FLT_MAX)
	,_max(-FLT_MAX)
	,_min(FLT_MAX)
	,_size(0.0f)
	,_halfSize(0.0f)
	,_center(0.0f)
{
	setVertexData(drawable);
}
	
BoundingBox::BoundingBox(const scene::Drawable * drawable)
	:_left(FLT_MAX)
	,_right(-FLT_MAX)
	,_top(-FLT_MAX)
	,_bottom(FLT_MAX)
	,_back(FLT_MAX)
	,_front(-FLT_MAX)
	,_max(-FLT_MAX)
	,_min(FLT_MAX)
	,_size(0.0f)
	,_halfSize(0.0f)
	,_center(0.0f)
{
	setVertexData(drawable);
}
	
BoundingBox::BoundingBox(const PolyListVector & plistVector)
	:_left(FLT_MAX)
	,_right(-FLT_MAX)
	,_top(-FLT_MAX)
	,_bottom(FLT_MAX)
	,_back(FLT_MAX)
	,_front(-FLT_MAX)
	,_max(-FLT_MAX)
	,_min(FLT_MAX)
	,_size(0.0f)
	,_halfSize(0.0f)
	,_center(0.0f)
{
	setVertexData(plistVector);
}
	
BoundingBox::BoundingBox(Group * group)
	:_left(FLT_MAX)
	,_right(-FLT_MAX)
	,_top(-FLT_MAX)
	,_bottom(FLT_MAX)
	,_back(FLT_MAX)
	,_front(-FLT_MAX)
	,_max(-FLT_MAX)
	,_min(FLT_MAX)
	,_size(0.0f)
	,_halfSize(0.0f)
	,_center(0.0f)
{
	setVertexData(group);
}

	
BoundingBox::~BoundingBox() {
}

void BoundingBox::setVertexData(const float * vertexData, unsigned int length) {
	reset();
	addVertexData(vertexData, length);
}
	
void BoundingBox::setVertexData(const vwgl::DrawableBase *drawable) {
	reset();
	addVertexData(drawable);
}
	
void BoundingBox::setVertexData(const scene::Drawable *drawable) {
	reset();
	addVertexData(drawable);
}
	
void BoundingBox::setVertexData(const PolyListVector & plistVector) {
	reset();
	addVertexData(plistVector);
}
	
void BoundingBox::setVertexData(vwgl::Group *drawable) {
	reset();
	addVertexData(drawable);
}

	
void BoundingBox::addVertexData(const float * vertexData, unsigned int length) {
	if (vertexData && length>0 && length%3==0) {
		for (unsigned int i=0; i<length-2; i+=3) {
			addVertex(vwgl::Vector3(vertexData[i],vertexData[i+1],vertexData[i+2]));
		}
		calculateBounds();
	}
}

void BoundingBox::addVertexData(const vwgl::DrawableBase *drawable) {
	SolidList::const_iterator it;
	for (it=drawable->getSolidList().cbegin(); it!=drawable->getSolidList().cend(); ++it) {
		if ((*it)->isVisible()) {
			addVertexData((*it)->getPolyList());
		}
	}
}
	
void BoundingBox::addVertexData(const scene::Drawable *drawable) {
	addVertexData(drawable->getPolyListVector());
}
	
void BoundingBox::addVertexData(const vwgl::PolyListVector & plistVector) {
	PolyListVector::const_iterator it;
	for (it=plistVector.cbegin(); it!=plistVector.cend(); ++it) {
		if ((*it)->isVisible()) {
			addVertexData((*it).getPtr());
		}
	}
}
	
void BoundingBox::addVertexData(vwgl::Group *group) {
	BuildBoundingBoxVisitor visitor(this);
	visitor.visit(group);
}
	
void BoundingBox::addVertex(const vwgl::Vector3 & v) {
	_left = _left<v[0] ? _left:v[0];
	_right = _right>v[0] ? _right:v[0];
	_top = _top>v[1] ? _top:v[1];
	_bottom = _bottom<v[1] ? _bottom:v[1];
	_back = _back<v[2] ? _back:v[2];
	_front = _front>v[2] ? _front:v[2];
}

void BoundingBox::reset() {
	_left = FLT_MAX;
	_right = -FLT_MAX;
	_top = -FLT_MAX;
	_bottom = FLT_MAX;
	_back = FLT_MAX;
	_front = -FLT_MAX;
	_max = vwgl::Vector3(0.0f);
	_min = vwgl::Vector3(0.0f);
	_size = vwgl::Vector3(0.0f);
	_halfSize = vwgl::Vector3(0.0f);
	_center = vwgl::Vector3(0.0f);
}

void BoundingBox::calculateBounds() {
	_max = vwgl::Vector3(_right,_top,_front);
	_min = vwgl::Vector3(_left,_bottom,_back);
	_size = _max - _min;
	_halfSize = _size; _halfSize.scale(0.5f);
	_center = _halfSize + _min;
}

}
