
#include <vwgl/material.hpp>
#include <vwgl/polylist.hpp>

namespace vwgl {

Material::Material()
	:_name(""),
	_polygonMode(kFill),
	_cullFace(true),
	_lineWidth(1.0f),
	_vertexVBO(-1),
	_normalVBO(-1),
	_colorVBO(-1),
	_texCoord0VBO(-1),
	_texCoord1VBO(-1),
	_texCoord2VBO(-1),
	_tangentVBO(-1),
	_vertexAttrib(-1),
	_normalAttrib(-1),
	_colorAttrib(-1),
	_texCoord0Attrib(-1),
	_texCoord1Attrib(-1),
	_texCoord2Attrib(-1),
	_tangentAttrib(-1),
	_initialized(false),
	_reuseShader(false)
{
}

Material::Material(const std::string & name)
	:_name(name),
	_polygonMode(kFill),
	_cullFace(true),
	_lineWidth(1.0f),
	_vertexVBO(-1),
	_normalVBO(-1),
	_colorVBO(-1),
	_texCoord0VBO(-1),
	_texCoord1VBO(-1),
	_texCoord2VBO(-1),
	_tangentVBO(-1),
	_vertexAttrib(-1),
	_normalAttrib(-1),
	_colorAttrib(-1),
	_texCoord0Attrib(-1),
	_texCoord1Attrib(-1),
	_texCoord2Attrib(-1),
	_tangentAttrib(-1),
	_initialized(false),
	_reuseShader(false)
{
}

Material::~Material() {
	destroy();
}

void Material::bindPolyList(PolyList * polyList) {
	if (polyList->getUseVertexArrayObjects()) {
		_vao = polyList->getVertexArrayObject();
	}
	else {
		_vao = -1;
	}
	_vertexVBO = polyList->getVertexBuffer();
	_normalVBO = polyList->getNormalBuffer();
	_colorVBO = polyList->getColorBuffer();
	_texCoord0VBO = polyList->getTexCoord0Buffer();
	_texCoord1VBO = polyList->getTexCoord1Buffer();
	_texCoord2VBO = polyList->getTexCoord2Buffer();
	_tangentVBO = polyList->getTangentBuffer();
	
	if (isTessellationMaterial()) {
		polyList->enablePatchMode();
	}
	else {
		polyList->disablePatchMode();
	}
}
	
void Material::loadVertexAttrib(const std::string & vertexAttribName) {      
	_vertexAttrib = getShader()->getAttribLocation(vertexAttribName);
}

void Material::loadNormalAttrib(const std::string & normalAttribName) {      
	_normalAttrib = getShader()->getAttribLocation(normalAttribName);
}
	
void Material::loadColorAttrib(const std::string & colorAttribName) {
	_colorAttrib = getShader()->getAttribLocation(colorAttribName);
}

void Material::loadTexCoord0Attrib(const std::string & texCoordAttribName) { 
	_texCoord0Attrib = getShader()->getAttribLocation(texCoordAttribName);
}

void Material::loadTexCoord1Attrib(const std::string & texCoordAttribName) { 
	_texCoord1Attrib = getShader()->getAttribLocation(texCoordAttribName);
}

void Material::loadTexCoord2Attrib(const std::string & texCoordAttribName) { 
	_texCoord2Attrib = getShader()->getAttribLocation(texCoordAttribName);
}
	
void Material::loadTangentArray(const std::string &tangentAttribName) {
	_tangentAttrib = getShader()->getAttribLocation(tangentAttribName);
}
	
void Material::activate() {
#ifndef VWGL_OPENGL_ES
	glPolygonMode(GL_FRONT_AND_BACK, _polygonMode);
#endif
	glLineWidth(_lineWidth);
	if (_cullFace) {
		glEnable(GL_CULL_FACE);
	}
	else {
		glDisable(GL_CULL_FACE);
	}

	getShader()->bind();
	enableAttribs();
	setupUniforms();
}

void Material::deactivate() {
	disableAttribs();
	getShader()->unbind();
}
	
void Material::destroy() {
	deactivate();
	_shader = NULL;
	_vertexVBO = -1;
	_normalVBO = -1;
	_colorVBO = -1;
	_texCoord0VBO = -1;
	_texCoord1VBO = -1;
	_texCoord2VBO = -1;
}

void Material::enableAttribs() {
	if (_vao!=-1) {
		glBindVertexArray(_vao);
	}
	enableVertexAttrib();
	enableNormalAttrib();
	enableColorAttrib();
	enableTexCoord0Attrib();
	enableTexCoord1Attrib();
	enableTexCoord2Attrib();
	enableTangentAttrib();
}

void Material::disableAttribs() {
	disableVertexAttrib();
	disableNormalAttrib();
	disableColorAttrib();
	disableTexCoord0Attrib();
	disableTexCoord1Attrib();
	disableTexCoord2Attrib();
	disableTangentAttrib();
	if (glBindVertexArray!=nullptr) {
		glBindVertexArray(0);
	}
}

void Material::enableVertexAttrib() {
	if (_vertexVBO!=-1 && _vertexAttrib!=-1) {
		glBindBuffer(GL_ARRAY_BUFFER, _vertexVBO);
		glEnableVertexAttribArray(_vertexAttrib);
		glVertexAttribPointer(_vertexAttrib,3,GL_FLOAT,GL_FALSE,0,0);
	}
}

void Material::enableNormalAttrib() {
	if (_normalVBO!=-1 && _normalAttrib!=-1) {
		glBindBuffer(GL_ARRAY_BUFFER, _normalVBO);
		glEnableVertexAttribArray(_normalAttrib);
		glVertexAttribPointer(_normalAttrib,3,GL_FLOAT,GL_FALSE,0,0);
	}
}
	
void Material::enableColorAttrib() {
	if (_colorVBO!=-1 && _colorAttrib!=-1) {
		glBindBuffer(GL_ARRAY_BUFFER, _colorVBO);
		glEnableVertexAttribArray(_colorAttrib);
		glVertexAttribPointer(_colorAttrib, 4, GL_FLOAT, GL_FALSE, 0, 0);
	}
}

void Material::enableTexCoord0Attrib() {
	if (_texCoord0VBO!=-1 && _texCoord0Attrib!=-1) {
		glBindBuffer(GL_ARRAY_BUFFER, _texCoord0VBO);
		glEnableVertexAttribArray(_texCoord0Attrib);
		glVertexAttribPointer(_texCoord0Attrib,2,GL_FLOAT,GL_FALSE,0,0);
	}
}

void Material::enableTexCoord1Attrib() {
	if (_texCoord1VBO!=-1 && _texCoord1Attrib!=-1) {
		glBindBuffer(GL_ARRAY_BUFFER, _texCoord1VBO);
		glEnableVertexAttribArray(_texCoord1Attrib);
		glVertexAttribPointer(_texCoord1Attrib,2,GL_FLOAT,GL_FALSE,0,0);
	}
}

void Material::enableTexCoord2Attrib() {
	if (_texCoord2VBO!=-1 && _texCoord2Attrib!=-1) {
		glBindBuffer(GL_ARRAY_BUFFER, _texCoord2VBO);
		glEnableVertexAttribArray(_texCoord2Attrib);
		glVertexAttribPointer(_texCoord2Attrib,2,GL_FLOAT,GL_FALSE,0,0);
	}
}
	
void Material::enableTangentAttrib() {
	if (_tangentVBO!=-1 && _tangentAttrib!=-1) {
		glBindBuffer(GL_ARRAY_BUFFER, _tangentVBO);
		glEnableVertexAttribArray(_tangentAttrib);
		glVertexAttribPointer(_tangentAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
}

void Material::disableVertexAttrib() {
	if (_vertexVBO!=-1 && _vertexAttrib!=-1) {
		glDisableVertexAttribArray(_vertexAttrib);
	}
}

void Material::disableNormalAttrib() {
	if (_normalVBO!=-1 && _normalAttrib!=-1) {
		glDisableVertexAttribArray(_normalAttrib);
	}
}
	
void Material::disableColorAttrib() {
	if (_colorVBO!=-1 && _colorAttrib!=-1) {
		glDisableVertexAttribArray(_colorAttrib);
	}
}

void Material::disableTexCoord0Attrib() {
	if (_texCoord0VBO!=-1 && _texCoord0Attrib!=-1) {
		glDisableVertexAttribArray(_texCoord0Attrib);
	}
}

void Material::disableTexCoord1Attrib() {
	if (_texCoord1VBO!=-1 && _texCoord1Attrib!=-1) {
		glDisableVertexAttribArray(_texCoord1Attrib);
	}
}

void Material::disableTexCoord2Attrib() {
	if (_texCoord2VBO!=-1 && _texCoord2Attrib!=-1) {
		glDisableVertexAttribArray(_texCoord2Attrib);
	}
}
	
void Material::disableTangentAttrib() {
	if (_tangentVBO!=-1 && _tangentAttrib!=-1) {
		glDisableVertexAttribArray(_tangentAttrib);
	}
}

}
