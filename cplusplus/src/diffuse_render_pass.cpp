
#include <vwgl/diffuse_render_pass.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

DiffuseRenderPassMaterial::DiffuseRenderPassMaterial() {
	initShader();
}

DiffuseRenderPassMaterial::~DiffuseRenderPassMaterial() {
	
}

void DiffuseRenderPassMaterial::initShader() {
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "def_diffuse.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "def_diffuse.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "def_diffuse.gl3.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "def_diffuse.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: deferred diffuse shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("def_texture");
	
	loadVertexAttrib("aVertexPosition");
	loadNormalAttrib("aNormalPosition");
	loadTexCoord0Attrib("aTexturePosition");
	loadTexCoord1Attrib("aLightmapPosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uMMatrix");
	getShader()->initUniformLocation("uNMatrix");
	getShader()->initUniformLocation("uVMatrixInv");
	
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uSelectMode");
	getShader()->initUniformLocation("uColorTint");
	getShader()->initUniformLocation("uIsEnabled");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
	
	getShader()->initUniformLocation("uUseLightmap");
	getShader()->initUniformLocation("uLightmap");
	getShader()->initUniformLocation("uLightmapOffset");
	getShader()->initUniformLocation("uLightmapScale");
	
	getShader()->initUniformLocation("uUseNormalMap");
	getShader()->initUniformLocation("uNormalMap");
	getShader()->initUniformLocation("uNormalMapOffset");
	getShader()->initUniformLocation("uNormalMapScale");
	
	getShader()->initUniformLocation("uCubeMap");
	getShader()->initUniformLocation("uUseCubeMap");
	getShader()->initUniformLocation("uReflectionAmount");

	getShader()->initUniformLocation("uAlphaCutoff");
}

void DiffuseRenderPassMaterial::setupUniforms()  {
	getShader()->setUniform("uMVMatrix", modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());
	getShader()->setUniform("uMMatrix", modelMatrix());
	getShader()->setUniform("uNMatrix", normalMatrix());
	Matrix4 vMatrix(viewMatrix());
	vMatrix.invert();
	getShader()->setUniform("uVMatrixInv", vMatrix);
	
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uTexture", mat->getTexture(),vwgl::Texture::kTexture0);
		getShader()->setUniform("uIsEnabled", mat->getTexture()!=NULL);
		getShader()->setUniform("uColorTint", mat->getDiffuse());
		getShader()->setUniform("uTextureOffset", mat->getTextureOffset());
		getShader()->setUniform("uTextureScale", mat->getTextureScale());
		
		if (mat->getLightMap()) {
			getShader()->setUniform("uUseLightmap",true);
			getShader()->setUniform("uLightmap", mat->getLightMap(), vwgl::Texture::kTexture1);
			getShader()->setUniform("uLightmapScale", mat->getLightmapScale());
			getShader()->setUniform("uLightmapOffset", mat->getLightmapOffset());
		}
		else {
			getShader()->setUniform("uUseLightmap",false);
		}
		
		if (mat->getNormalMap()) {
			getShader()->setUniform("uUseNormalMap", true);
			getShader()->setUniform("uNormalMap", mat->getNormalMap(),vwgl::Texture::kTexture2);
			getShader()->setUniform("uNormalMapScale", mat->getNormalMapScale());
			getShader()->setUniform("uNormalMapOffset", mat->getNormalMapOffset());
		}
		else {
			getShader()->setUniform("uUseNormalMap", false);
		}
		
		if (mat->getCubeMap()) {
			getShader()->setUniform("uUseCubeMap", true);
			getShader()->setUniform("uCubeMap", mat->getCubeMap(),vwgl::Texture::kTexture3);
			getShader()->setUniform("uReflectionAmount", mat->getReflectionAmount());
		}
		else {
			getShader()->setUniform("uUseCubeMap", false);
			Texture::setActiveTexture(vwgl::Texture::kTexture3);
			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
			getShader()->setUniform("uCubeMap", 3);
			getShader()->setUniform("uReflectionAmount", 0.0f);
		}

		getShader()->setUniform("uAlphaCutoff", mat->getAlphaCutoff());
	}
}

}
