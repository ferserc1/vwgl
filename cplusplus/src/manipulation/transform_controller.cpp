
#include <vwgl/manipulation/transform_controller.hpp>
#include <vwgl/scene/transform.hpp>

namespace vwgl {
namespace manipulation {

void TargetInputController::update() {
	if (transform() && !transform()->getTransform().getTransformStrategy()) {
		Vector3 forward = transform()->getTransform().getMatrix().forwardVector();
		Vector3 left = transform()->getTransform().getMatrix().leftVector();
		forward.scale(_forward);
		left.scale(_left);
		_center.add(forward).add(left);
		
		transform()->getTransform().getMatrix()
			.identity()
			.translate(_center)
			.rotate(vwgl::Math::degreesToRadians(_rotation.y()), 0.0f, 1.0f, 0.0f)
			.rotate(vwgl::Math::degreesToRadians(_rotation.x()), 1.0f, 0.0f, 0.0f)
			.translate(vwgl::Vector3(0.0f, 0.0f, _distance));
	}
}

void TargetInputController::mouseDown(const app::MouseEvent & evt) {
	_lastPosition = evt.pos();
}
	
void TargetInputController::mouseDrag(const app::MouseEvent &evt) {
	Vector2 delta(static_cast<float>(_lastPosition.y() - evt.pos().y()),
				  static_cast<float>(_lastPosition.x() - evt.pos().x()));
	
	_lastPosition = evt.pos();
	
	bool left = evt.mouse().getButtonStatus(vwgl::Mouse::kLeftButton);
	bool middle = evt.mouse().getButtonStatus(vwgl::Mouse::kMiddleButton);
	bool right = evt.mouse().getButtonStatus(vwgl::Mouse::kRightButton);
	
	if (left && !middle && !right) {
		_rotation.add(delta.scale(0.5f));
	}
	else if (!left && !middle && right && transform()) {
		Vector3 up = transform()->getTransform().getMatrix().upVector();
		Vector3 left = transform()->getTransform().getMatrix().leftVector();
		
		up.scale(delta.x() * -0.001f * _distance);
		left.scale(delta.y() * -0.001f * _distance);
		_center.add(up).add(left);
	}
	else if (!left && middle && !right) {
		_distance += static_cast<float>(delta.x() * 0.01f * _distance);
	}
}
	
void TargetInputController::mouseWheel(const app::MouseEvent &evt) {
	_distance += static_cast<float>(evt.delta().y() * -0.1f * _distance);
}

void TargetInputController::keyDown(const app::KeyboardEvent & evt) {
	if (_flyMode && evt.keyboard().key()==_forwardKey) {
		_forward = -_movementVelocity;
	}
	else if (_flyMode && evt.keyboard().key()==_backwardKey) {
		_forward = _movementVelocity;
	}
	
	if (_flyMode && evt.keyboard().key()==_leftKey) {
		_left = _movementVelocity;
	}
	else if (_flyMode && evt.keyboard().key()==_rightKey) {
		_left = -_movementVelocity;
	}
}

void TargetInputController::keyUp(const app::KeyboardEvent & evt) {
	if (_flyMode && (evt.keyboard().key()==_forwardKey || evt.keyboard().key()==_backwardKey)) {
		_forward = 0.0f;
	}
	
	if (_flyMode && (evt.keyboard().key()==_leftKey || evt.keyboard().key()==_rightKey)) {
		_left = 0.0f;
	}
}

}
}