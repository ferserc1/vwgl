
#include <vwgl/manipulation/gizmo_manager.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/state.hpp>
#include <vwgl/scene/light.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/joint.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/loader.hpp>

namespace vwgl {
namespace manipulation {

static std::string kGizmoManagerIconLightSpot			= "gizmo_icon_light_spot.png";
static std::string kGizmoManagerIconLightPoint			= "gizmo_icon_light_point.png";
static std::string kGizmoManagerIconLightDirectional	= "gizmo_icon_directional_2.png";
static std::string kGizmoManagerIconLightDisabled		= "gizmo_icon_light_disabled.png";
static std::string kGizmoManagerIconCamera				= "gizmo_icon_camera.png";
static std::string kGizmoManagerIconTransform			= "gizmo_icon_transform.png";
static std::string kGizmoManagerIconDrawable			= "gizmo_icon_drawable.png";
static std::string kGizmoManagerIconJoint				= "gizmo_icon_joint.png";
	
GizmoManager::GizmoManager()
	:_visibleGizmos(kIconLightSpot | kIconLightPoint | kIconLightDirectional | kIconLightDisabled |
					kIconCamera | kIconTransform | kIconDrawable | kIconJoint)
{
	
}

GizmoManager::GizmoManager(scene::Node * root)
	:_sceneRoot(root)
{
}

void GizmoManager::draw() {
	if (_sceneRoot.valid()) {
		_sceneRoot->accept(*this);	// update the gizmo icons
		
		// Draw the gizmos over the current buffer
		vwgl::State::get()->clearBuffers(vwgl::State::kBufferDepth);
		vwgl::scene::Camera::applyTransform(vwgl::scene::Camera::getMainCamera());
		_drawGizmoVisitor.setIconPass();
		_sceneRoot->accept(_drawGizmoVisitor);
		_drawGizmoVisitor.setManipulatorPass();
		State::get()->clearBuffers(State::kBufferDepth);
		_sceneRoot->accept(_drawGizmoVisitor);
	}
}

void GizmoManager::destroy() {
	_drawGizmoVisitor.clean();
	_icons.clear();
	_plist.clear();
}

void GizmoManager::visit(scene::Node * node) {
	// As only one icon is allowed, it's important to check the priority
	vwgl::scene::Light * light = node->getComponent<vwgl::scene::Light>();
	vwgl::scene::Camera * cam = node->getComponent<vwgl::scene::Camera>();
	vwgl::scene::Drawable * drw = node->getComponent<vwgl::scene::Drawable>();
	vwgl::scene::Transform * trx = node->getComponent<vwgl::scene::Transform>();
	vwgl::scene::InputChainJoint * inJoint = node->getComponent<vwgl::scene::InputChainJoint>();
	vwgl::scene::OutputChainJoint * outJoint = node->getComponent<vwgl::scene::OutputChainJoint>();
	
	if (light) {
		switch (light->getType()) {
			case scene::Light::kTypeDirectional:
				node->setGizmoIcon(getIcon(kIconLightDirectional));
				node->setGizmoPolyList(getPolyList(kIconLightDirectional));
				break;
			case scene::Light::kTypeSpot:
				node->setGizmoIcon(getIcon(kIconLightSpot));
				node->setGizmoPolyList(getPolyList(kIconLightSpot));
				break;
			case scene::Light::kTypePoint:
				node->setGizmoIcon(getIcon(kIconLightPoint));
				break;
			case scene::Light::kTypeDisabled:
				node->setGizmoIcon(getIcon(kIconLightDisabled));
				break;
		}
	}
	else if (cam) {
		node->setGizmoIcon(getIcon(kIconCamera));
	}
	else if (trx) {
		node->setGizmoIcon(getIcon(kIconTransform));
	}
	else if (drw) {
		node->setGizmoIcon(getIcon(kIconDrawable));
	}
	if (inJoint) {
		inJoint->setGizmoIcon(getIcon(kIconJoint));
	}
	if (outJoint) {
		outJoint->setGizmoIcon(getIcon(kIconJoint));
	}
}
	
void GizmoManager::initDefaultIcons() {
	setIcon(kIconLightSpot, vwgl::Loader::get()->loadTexture(kGizmoManagerIconLightSpot, true));
	setIcon(kIconLightPoint, vwgl::Loader::get()->loadTexture(kGizmoManagerIconLightPoint, true));
	setIcon(kIconLightDirectional, vwgl::Loader::get()->loadTexture(kGizmoManagerIconLightDirectional, true));
	setIcon(kIconLightDisabled, vwgl::Loader::get()->loadTexture(kGizmoManagerIconLightDisabled, true));
	setIcon(kIconCamera, vwgl::Loader::get()->loadTexture(kGizmoManagerIconCamera, true));
	setIcon(kIconTransform, vwgl::Loader::get()->loadTexture(kGizmoManagerIconTransform, true));
	setIcon(kIconDrawable, vwgl::Loader::get()->loadTexture(kGizmoManagerIconDrawable, true));
	setIcon(kIconJoint, vwgl::Loader::get()->loadTexture(kGizmoManagerIconJoint, true));
	vwgl::Texture::unbindTexture(vwgl::Texture::kTargetTexture2D);
}
	
void GizmoManager::initDefaultPlist() {
	ptr<PolyList> directionArrow = new PolyList();
	
	directionArrow->addVertex(Vector3(0.0f));
	directionArrow->addVertex(Vector3(0.0f,0.0f,-1.0f));
	directionArrow->addVertex(Vector3(0.2f,0.0f,-0.8f));
	directionArrow->addVertex(Vector3(-0.2f,0.0f,-0.8f));
	
	directionArrow->addNormal(Vector3(1.0,0.0,0.0));
	directionArrow->addNormal(Vector3(1.0,0.0,0.0));
	directionArrow->addNormal(Vector3(1.0,0.0,0.0));
	directionArrow->addNormal(Vector3(1.0,0.0,0.0));
	
	directionArrow->addTexCoord0(Vector2(0.0f));
	directionArrow->addTexCoord0(Vector2(0.0f));
	directionArrow->addTexCoord0(Vector2(0.0f));
	directionArrow->addTexCoord0(Vector2(0.0f));

	directionArrow->addIndex(0);
	directionArrow->addIndex(1);
	directionArrow->addIndex(1);
	directionArrow->addIndex(2);
	directionArrow->addIndex(1);
	directionArrow->addIndex(3);
	directionArrow->setDrawMode(PolyList::kLines);
	directionArrow->buildPolyList();
	
	setPolyList(kIconLightSpot, directionArrow.getPtr());
	setPolyList(kIconLightDirectional, directionArrow.getPtr());
}

void GizmoManager::mouseDown(PolyList * gizmoPlist, const app::MouseEvent & evt) {
	_gizmoPlist = gizmoPlist;
	if (_gizmoPlist.getPtr() && currentGizmo()) {
		currentGizmo()->setTarget(_drawGizmoVisitor.currentTransform());
		_currentAxis = 0;
		if (_gizmoPlist->getName()=="xAxis") {
			_currentAxis = Gizmo::kAxisX;
		}
		else if (_gizmoPlist->getName()=="yAxis") {
			_currentAxis = Gizmo::kAxisY;
		}
		else if (_gizmoPlist->getName()=="zAxis") {
			_currentAxis = Gizmo::kAxisZ;
		}
		else if (_gizmoPlist->getName()=="allAxis") {
			_currentAxis = Gizmo::kAxisX | Gizmo::kAxisY | Gizmo::kAxisZ;
		}
		currentGizmo()->beginAction();

		eachObserver([&](IGizmoObserver * observer) {
			observer->gizmoPicked(*this);
		});
	}
	_lastMousePosition = evt.pos();
}

bool GizmoManager::mouseDrag(const app::MouseEvent & evt) {
	if (_gizmoPlist.valid() && currentGizmo() && _drawGizmoVisitor.currentTransform()) {
		Vector2i pos = evt.pos();
		currentGizmo()->setTarget(_drawGizmoVisitor.currentTransform());
		currentGizmo()->offsetChanged(_currentAxis,
									  Vector2i(pos.x() - _lastMousePosition.x(), pos.y() - _lastMousePosition.y()));
		_lastMousePosition = pos;

		eachObserver([&](IGizmoObserver * observer) {
			observer->gizmoMoved(*this);
		});
	}
	
	return _gizmoPlist.valid();
}

bool GizmoManager::mouseUp(const app::MouseEvent & evt) {
	bool gizmoAction = _gizmoPlist.valid();
	if (currentGizmo() && _currentAxis) {
		currentGizmo()->setTarget(_drawGizmoVisitor.currentTransform());
		currentGizmo()->commit();
		eachObserver([&](IGizmoObserver * observer) {
			observer->gizmoReleased(*this);
		});
	}
	_currentAxis = 0;
	_gizmoPlist = nullptr;
	return gizmoAction;
}

}
}
