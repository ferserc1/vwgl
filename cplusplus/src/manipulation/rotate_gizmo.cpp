
#include <vwgl/manipulation/rotate_gizmo.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/cmd/rotate_command.hpp>


namespace vwgl {
namespace manipulation {

RotateGizmo::RotateGizmo()
{
	_previewRotation.identity();
	loadDrawable(Loader::get()->loadModel("rotate_gizmo.vwglb",true));
}

RotateGizmo::~RotateGizmo() {
	
}

void RotateGizmo::draw() {
	State::get()->pushModelMatrix();
	State::get()->pushViewMatrix();
	float height = static_cast<float>(State::get()->getViewport().height()) / 150.0f;
	Matrix4 trx = State::get()->modelMatrix();
	
	trx.resetScale();
	
	vwgl::Vector3 pos = trx.getPosition();
	
	float distance = vwgl::State::get()->modelViewMatrix().multVector(vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f)).z();
	distance *= _scale / height;
	vwgl::State::get()->modelMatrix().identity().translate(pos).scale(-distance,-distance,-distance).mult(_previewRotation);
	
	if (drawable()) {
		drawable()->draw();
		selectable()->draw();
	}
	
	State::get()->popModelMatrix();
	State::get()->popViewMatrix();
}

void RotateGizmo::beginAction() {
	_rotate.set(0.0f);
	_previewRotation.identity();
	scene::Transform * trx = getTarget();
	if (trx && trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
		_restoreRotate.x(trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->getRotateX());
		_restoreRotate.y(trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->getRotateY());
		_restoreRotate.z(trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->getRotateZ());
	}
	else if (trx) {
		_restoreMatrix = trx->getTransform().getMatrix();
	}
}

void RotateGizmo::offsetChanged(unsigned int axis, const Vector2i &offset) {
	scene::Camera * cam = scene::Camera::getMainCamera();
	scene::Transform * trx = getTarget();
	if (cam && trx) {
		Vector3 right = cam->getTransform().rightVector();
		Vector3 up = cam->getTransform().upVector();
		float offsetMultiplier = 0.01f;
		Vector3 rotate;
		
		if (axis & kAxisX) {
			rotate.x(static_cast<float>(offset.y()) * up.y() * offsetMultiplier);
		}
		else if (axis & kAxisY) {
			rotate.y(static_cast<float>(offset.x()) * right.x() * offsetMultiplier +
					 static_cast<float>(offset.y()) * right.z() * -offsetMultiplier);
		}
		else if (axis & kAxisZ) {
			rotate.z(static_cast<float>(offset.y()) * right.x() * -offsetMultiplier +
					static_cast<float>(offset.x()) * right.z() * offsetMultiplier);
		}
		
		_rotate.add(rotate);
		if (trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateX(_restoreRotate.x() + _rotate.x());
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateY(_restoreRotate.y() + _rotate.y());
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateZ(_restoreRotate.z() + _rotate.z());
		}
		else {
			trx->getTransform().getMatrix().setRow(3, Vector4(0.0f,0.0f,0.0f,1.0f));
			trx->getTransform().getMatrix().rotate(rotate.x(), 1.0f, 0.0f, 0.0f);
			trx->getTransform().getMatrix().rotate(rotate.y(), 0.0f, 1.0f, 0.0f);
			trx->getTransform().getMatrix().rotate(rotate.z(), 0.0f, 0.0f, 1.0f);
		}
		
		_previewRotation.rotate(rotate.x(), 1.0f, 0.0f, 0.0f);
		_previewRotation.rotate(rotate.y(), 0.0f, 1.0f, 0.0f);
		_previewRotation.rotate(rotate.z(), 0.0f, 0.0f, 1.0f);
	}
}

void RotateGizmo::commit() {
	scene::Transform * trx = getTarget();
	_previewRotation.identity();
	if (trx && getCommandManager()) {
		if (trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateX(_restoreRotate.x());
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateY(_restoreRotate.y());
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateZ(_restoreRotate.z());
		}
		else {
			trx->getTransform().setMatrix(_restoreMatrix);
		}
		getCommandManager()->execute(new cmd::RotateCommand(trx,_rotate));
	}
}

void RotateGizmo::discard() {
	_previewRotation.identity();
	scene::Transform * trx = getTarget();
	if (trx) {
		if (trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateX(_restoreRotate.x());
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateY(_restoreRotate.y());
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->rotateZ(_restoreRotate.z());
		}
		else {
			trx->getTransform().setMatrix(_restoreMatrix);
		}
	}
}


}
}