
#include <vwgl/manipulation/pick_visitor.hpp>


namespace vwgl {
namespace manipulation {
	
void PickVisitor::visit(scene::Node * node) {
	Selectable * selectable = node->getComponent<manipulation::Selectable>();
	if (selectable && !_selectable.valid()) {
		selectable->eachIdentifier([&](PickerIdentifier & id) {
			if (id==_targetIdentifier) {
				_selectable = selectable;
				_polyList = id.getPolyList();
				return true;
			}
			return false;
		});
	}
}

}
}