
#include <vwgl/manipulation/mouse_picker.hpp>
#include <vwgl/manipulation/selectable.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/state.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace manipulation {

MousePicker::MousePicker()
	:_gizmoManager(nullptr)
	,_invertedY(true)
{

}

MousePicker::MousePicker(scene::Node * sceneRoot)
	:_sceneRoot(sceneRoot)
	,_gizmoManager(nullptr)
	,_invertedY(true)
{

}

bool MousePicker::gizmo(const Position2Di & mousePosition, const Viewport & viewport) {
	_pickedGizmo = nullptr;
	_pickedGizmoPlist = nullptr;
	if (_gizmoManager && _gizmoManager->currentGizmo() && _gizmoManager->currentGizmo()->selectable()) {
		beginPick(mousePosition, viewport);
		_gizmoManager->drawGizmosVisitor().setManipulatorPass();
		_sceneRoot->accept(_gizmoManager->drawGizmosVisitor());
		endPick();

		unsigned char rgba[4];
		getColor(mousePosition, viewport, rgba);
		if (rgba[0] != 0 || rgba[1] != 0 || rgba[2] != 0 || rgba[3] != 0) {
			PickerIdentifier target(rgba[0], rgba[1], rgba[2], rgba[3]);
			manipulation::Selectable * gizmoSelectable = _gizmoManager->currentGizmo()->selectable();
			gizmoSelectable->eachIdentifier([&](manipulation::PickerIdentifier & identifier) {
				if (identifier == target) {
					_pickedGizmo = _gizmoManager->currentGizmo();
					_pickedGizmoPlist = identifier.getPolyList();
					return true;
				}
				return false;
			});
		}
	}

	return _pickedGizmoPlist.valid();
}

bool MousePicker::pick(const Position2Di & mousePosition, const Viewport & viewport) {
	_selectableResult = nullptr;
	_polyListResult = nullptr;

	if (_sceneRoot.valid()) {
		// Draw pick buffer
		beginPick(mousePosition, viewport);
		_sceneRoot->accept(_drawVisitor);
		endPick();

		// Pick color
		unsigned char rgba[4];
		getColor(mousePosition, viewport, rgba);

		// Check picked color
		if (rgba[0] != 0 || rgba[1] != 0 || rgba[2] != 0 || rgba[3] != 0) {
			_pickVisitor.clear();
			_pickVisitor.setTarget(PickerIdentifier(rgba[0], rgba[1], rgba[2], rgba[3]));
			_sceneRoot->accept(_pickVisitor);

			_selectableResult = _pickVisitor.getSelectable();
			_polyListResult = _pickVisitor.getPolyList();
		}
	}

	return _selectableResult.valid();
}

void MousePicker::beginPick(const Position2Di & mousePosition, const Viewport & viewport) {
	if (!_fbo.valid()) {
		createCanvas(viewport);
	}
	else {
		resizeCanvas(viewport);
	}

	_restoreViewpor = State::get()->getViewport();

	_fbo->bind();
	State::get()->setClearColor(Color(0.0f, 0.0f, 0.0f, 0.0f));
	State::get()->clearBuffers(State::kBufferColor | State::kBufferDepth);
	State::get()->pushProjectionMatrix();
	State::get()->pushModelMatrix();
	State::get()->pushViewMatrix();

	State::get()->setViewport(viewport);
	scene::Camera * cam = scene::Camera::getMainCamera();
	if (cam) {
		scene::Camera::applyTransform(cam);
	}

	// Prevent draw and enable pick mode (draw picker identifier to color buffer)
	scene::Drawable::disableDraw();
	Selectable::enablePickMode();
}

void MousePicker::endPick() {
	// Disable pick mode and enable drawing
	scene::Drawable::enableDraw();
	Selectable::disablePickMode();

	State::get()->popViewMatrix();
	State::get()->popModelMatrix();
	State::get()->popProjectionMatrix();

	_fbo->unbind();

	State::get()->setViewport(_restoreViewpor);
}

void MousePicker::getColor(const Position2Di & mousePosition, const Viewport & viewport, unsigned char rgba[4]) {
	memset(rgba, 0, sizeof(unsigned char) * 4);
	Position2Di convertedPosition = _invertedY ? Position2Di(mousePosition.x(), viewport.height() - mousePosition.y()) : mousePosition;
	Size2Di pickSize(1, 1);
	size_t size = pickSize.width() * pickSize.height() * 4;
	if (size > 0) {
		unsigned char *pixels_rgba = new unsigned char[size];
		_fbo->readColor(Viewport(convertedPosition.x(), convertedPosition.y(), pickSize.width(), pickSize.height()), FramebufferObject::kAttachmentColor0, pixels_rgba);
		rgba[0] = (unsigned char)pixels_rgba[0];
		rgba[1] = (unsigned char)pixels_rgba[1];
		rgba[2] = (unsigned char)pixels_rgba[2];
		rgba[3] = (unsigned char)pixels_rgba[3];
		delete[] pixels_rgba;
	}
}

void MousePicker::createCanvas(const Viewport & vp) {
	_fbo = new FramebufferObject(vp.width(), vp.height());
}

void MousePicker::resizeCanvas(const Viewport & vp) {
	if (_fbo.valid() && (_fbo->getWidth()!=vp.width() || _fbo->getHeight()!=vp.height())) {
		_fbo->resize(Size2Di(vp.width(), vp.height()));
	}
}

}
}