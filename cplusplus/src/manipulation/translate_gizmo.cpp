
#include <vwgl/manipulation/translate_gizmo.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/state.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/cmd/translate_command.hpp>

namespace vwgl {
namespace manipulation {
	
TranslateGizmo::TranslateGizmo()
{
	loadDrawable(Loader::get()->loadModel("move_gizmo.vwglb",true));
}
	
TranslateGizmo::~TranslateGizmo() {
	
}

void TranslateGizmo::draw() {
	State::get()->pushModelMatrix();
	State::get()->pushViewMatrix();
	float height = static_cast<float>(State::get()->getViewport().height()) / 100.0f;
	Matrix4 trx = State::get()->modelMatrix();

	float sx = Vector3(trx.get00(), trx.get01(), trx.get02()).magnitude();
	float sy = Vector3(trx.get10(), trx.get11(), trx.get12()).magnitude();
	float sz = Vector3(trx.get20(), trx.get21(), trx.get22()).magnitude();
	trx.scale(sx, sy, sz);

	vwgl::Vector3 pos = trx.getPosition();
	
	
	float distance = vwgl::State::get()->modelViewMatrix().multVector(vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f)).z();
	distance *= _scale / height;
	vwgl::State::get()->modelMatrix().identity().translate(pos).scale(-distance,-distance,-distance);

	if (drawable()) {
		drawable()->draw();
		selectable()->draw();
	}
	
	State::get()->popModelMatrix();
	State::get()->popViewMatrix();
}

void TranslateGizmo::beginAction() {
	_translate.set(0.0f, 0.0f, 0.0f);
	scene::Transform * trx = getTarget();
	if (trx && trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
		_restorePosition = trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->getTranslate();
	}
	else if (trx) {
		_restoreMatrix = trx->getTransform().getMatrix();
	}
}

void TranslateGizmo::offsetChanged(unsigned int axis, const Vector2i &offset) {
	scene::Camera * cam = scene::Camera::getMainCamera();
	scene::Transform * trx = getTarget();
	if (cam && trx) {
		Vector3 right = cam->getTransform().rightVector();
		Vector3 up = cam->getTransform().upVector();
		float cameraToGizmoDistance = cam->getTransform().getPosition().distance(trx->getTransform().getMatrix().getPosition());
		float offsetMultiplier = 0.002f * cameraToGizmoDistance;
		Vector3 translate;
		
		if (axis & kAxisX) {
			translate.x(static_cast<float>(offset.x()) * right.x() * offsetMultiplier +
						static_cast<float>(offset.y()) * right.z() * -offsetMultiplier);
		}
		else if (axis & kAxisY) {
			translate.y(static_cast<float>(offset.y()) * up.y() * -offsetMultiplier);
		}
		else if (axis & kAxisZ) {
			translate.z(static_cast<float>(offset.y()) * right.x() * offsetMultiplier +
						static_cast<float>(offset.x()) * right.z() * offsetMultiplier);
		}
		
		_translate.add(translate);
		if (trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
			Vector3 prevTranslate = trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->getTranslate();
			translate.add(prevTranslate);
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->translate(translate);
		}
		else {
			trx->getTransform().getMatrix().translate(translate);
		}
	}
}

void TranslateGizmo::commit() {
	scene::Transform * trx = getTarget();
	if (trx && getCommandManager()) {
		if (trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->translate(_restorePosition);
		}
		else {
			trx->getTransform().setMatrix(_restoreMatrix);
		}
		getCommandManager()->execute(new cmd::TranslateCommand(trx,_translate));
	}
}

void TranslateGizmo::discard() {
	scene::Transform * trx = getTarget();
	if (trx && trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
		trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->translate(_restorePosition);
	}
	else if (trx) {
		trx->getTransform().setMatrix(_restoreMatrix);
	}
}

}
}