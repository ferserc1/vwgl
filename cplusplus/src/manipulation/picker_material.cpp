
#include <vwgl/manipulation/picker_material.hpp>
#include <vwgl/math.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {
namespace manipulation {

std::string PickerMaterial::_vshader = "\
	attribute vec3 aVertexPosition;\n\
	\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
	\n\
	void main() {\n\
		gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	}";
	
std::string PickerMaterial::_fshader = "\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	\n\
	uniform vec4 uPickId;\n\
	\n\
	void main() {\n\
		gl_FragColor = uPickId;\n\
	}";

std::string PickerMaterial::_vshader_gl3 = "\
	#version 150\n\
	in vec3 aVertexPosition;\n\
	\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
	\n\
	void main() {\n\
		gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	}";
	
std::string PickerMaterial::_fshader_gl3 = "\
	#version 150\n\
	\n\
	out vec4 out_FragColor;\n\
	\n\
	uniform vec4 uPickId;\n\
	\n\
	void main() {\n\
		out_FragColor = uPickId;\n\
	}";


void PickerMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL || Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLBasic) {
		getShader()->attachShader(vwgl::Shader::kTypeVertex, PickerMaterial::_vshader);
		getShader()->attachShader(vwgl::Shader::kTypeFragment, PickerMaterial::_fshader);
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->attachShader(vwgl::Shader::kTypeVertex, PickerMaterial::_vshader_gl3);
		getShader()->attachShader(vwgl::Shader::kTypeFragment, PickerMaterial::_fshader_gl3);
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	getShader()->link("color_pick_material");
	
	loadVertexAttrib("aVertexPosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uPickId");
}

void PickerMaterial::setupUniforms() {
	if (_pickerIdentifier != nullptr) {
		Vector4 colorPickId = Vector4(static_cast<float>(_pickerIdentifier->r()) / 255.0f,
			static_cast<float>(_pickerIdentifier->g()) / 255.0f,
			static_cast<float>(_pickerIdentifier->b()) / 255.0f,
			static_cast<float>(_pickerIdentifier->a()) / 255.0f);
		getShader()->setUniform("uMVMatrix", modelViewMatrix());
		getShader()->setUniform("uPMatrix", projectionMatrix());
		getShader()->setUniform("uPickId", colorPickId);
	}
}

}
}
