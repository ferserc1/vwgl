
#include <vwgl/manipulation/selectable.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/state.hpp>
#include <vwgl/manipulation/draw_gizmo_visitor.hpp>

namespace vwgl {
namespace manipulation {

unsigned char Selectable::s_r = 0;
unsigned char Selectable::s_g = 0;
unsigned char Selectable::s_b = 0;
unsigned char Selectable::s_a = 0;
bool Selectable::s_doPick = false;
	
Selectable::~Selectable() {
	
}
	
void Selectable::init() {
	updatePolyListRefs();
	_pickerMaterial = new PickerMaterial();
}

void Selectable::drawableChanged(scene::Drawable * drawable) {
	updatePolyListRefs();
}

void Selectable::updatePolyListRefs() {
	clearPolyListRefs();
	assignId(_pickerIdentifier);
	ptr<scene::Drawable> drw;
	if (sceneObject() && (drw = sceneObject()->getComponent<vwgl::scene::Drawable>()).valid()) {
		drw->eachElement([&](PolyList * plist, Material * mat, Transform & trx) {
			addPolyList(plist);
		});
	}
}
	
void Selectable::clearPolyListRefs() {
	_pickerIdVector.clear();
}
	
void Selectable::assignId(PickerIdentifier & id) {
	s_a+=20;
	if (s_a==0) {
		++s_b;
		if (s_b==0) {
			++s_g;
			if (s_g==0) {
				++s_r;
				if (s_r==0) {
					++s_b;	// 4.294.967.296 - 1 id's available (the 0:0:0:0 id is reserved). You shouldn't ever see this message
					std::cerr << "Warning: maximum number of picker identifiers reached. Maybe your scene is too large." << std::endl;
				}
			}
		}
	}
	id.set(s_r, s_g, s_b, s_a);
}

void Selectable::addPolyList(vwgl::PolyList *plist) {
	PickerIdentifier id(plist);
	assignId(id);
	_pickerIdVector.push_back(id);
}

void Selectable::draw() {
	scene::Drawable * drw = nullptr;
	if (s_doPick && sceneObject() && (drw=sceneObject()->getComponent<scene::Drawable>())) {
		scene::Drawable::setForcedMaterial(_pickerMaterial.getPtr());
		scene::Drawable::setUseMaterialSettings(false);
		PickerIdentifierVector::iterator it;
		for (it=_pickerIdVector.begin(); it!=_pickerIdVector.end(); ++it) {
			PolyList * plist = it->getPolyList();
			if (plist->isVisible()) {
				vwgl::Transform & trx = drw->getTransform(plist);
				vwgl::State::get()->pushModelMatrix();
				vwgl::State::get()->modelMatrix().mult(trx.getMatrix());
				
				_pickerMaterial->setPickerIdentifier(&(*it));
				_pickerMaterial->bindPolyList(plist);
				_pickerMaterial->activate();
				
				plist->drawElements();
				_pickerMaterial->deactivate();
				
				vwgl::State::get()->popModelMatrix();
			}
		}
		scene::Drawable::setForcedMaterial(nullptr);
	}
	
	if (s_doPick && sceneObject() && (sceneObject()->getGizmoIcon())) {
		GizmoMaterial * mat = DrawGizmosVisitor::getMaterial();
		PolyList * plist = DrawGizmosVisitor::getIconRect();
		if (plist && mat) {
			vwgl::State::get()->disableDepthTest();
			vwgl::State::get()->pushModelMatrix();
	
			Viewport vp = State::get()->getViewport();
			float distance = vwgl::State::get()->modelViewMatrix().multVector(vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f)).z();
			distance = vwgl::Math::abs(distance);
			
			mat->setScale(distance * 50.0f / vp.height());
			
			mat->setTexture(sceneObject()->getGizmoIcon());
			mat->setPickerIdentifier(_pickerIdentifier);
			mat->bindPolyList(plist);
			mat->activate();
			plist->drawElements();
			mat->deactivate();
			
			vwgl::State::get()->popModelMatrix();
			vwgl::State::get()->enableDepthTest();
		}
	}
}

}
}