
#include <vwgl/manipulation/gizmo.hpp>
#include <vwgl/manipulation/selectable.hpp>

namespace vwgl {
namespace manipulation {

app::CommandManager * Gizmo::s_commandManager = nullptr;

Gizmo::Gizmo()
	:_scale(1.0f)
	,_initialized(false)
{
	
}

Gizmo::~Gizmo() {
}

void Gizmo::loadDrawable(scene::Drawable *drw) {
	if (drw) {
		addComponent(drw);
		addComponent(new manipulation::Selectable());
		init();
	}
}

}
}