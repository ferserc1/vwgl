
#include <vwgl/manipulation/draw_gizmo_visitor.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/graphics.hpp>
#include <vwgl/state.hpp>

#include <vwgl/texture_manager.hpp>

namespace vwgl {
namespace manipulation {

std::string GizmoMaterial::s_vshader =
	"attribute vec4 in_Position;\n"
	"attribute vec2 in_TexCoord;\n"
	
	"uniform mat4 uMVPMatrix;\n"
	
	"varying vec2 vTexCoord;\n"
	"void main(void) {\n"
	"	gl_Position = uMVPMatrix * in_Position;\n"
	"	vTexCoord = in_TexCoord;\n"
	"}\n";
	
std::string GizmoMaterial::s_fshader =
	"varying vec2 vTexCoord;\n"
	
	"uniform sampler2D uTexture;\n"
	"uniform vec4 uPickId;\n"
	
	"void main(void) {\n"
	"	vec4 color = texture2D(uTexture,vTexCoord);"
	"	if (color.a==0.0) discard;\n"
	"	else gl_FragColor = color * uPickId;\n"
	"}\n";
	
std::string GizmoMaterial::s_vshader_gl3 =
	"#version 150\n"
	"in vec4 in_Position;\n"
	"in vec2 in_TexCoord;\n"
	
	"uniform mat4 uMVPMatrix;\n"
	
	"out vec2 f_TexCoord;\n"
	
	"void main(void) {\n"
	"	gl_Position = uMVPMatrix * in_Position;\n"
	"	f_TexCoord = in_TexCoord;\n"
	"}\n";;
	
std::string GizmoMaterial::s_fshader_gl3 =
	"#version 150\n"
	
	"in vec2 f_TexCoord;\n"
	"out vec4 out_Color;\n"
	
	"uniform sampler2D uTexture;\n"
	"uniform vec4 uPickId;\n"
	
	"void main(void) {\n"
	"	vec4 color = texture(uTexture,f_TexCoord);"
	"	if (color.a==0.0) discard;\n"
	"	else out_Color = color * uPickId;\n"
	"}\n";
	
	
GizmoMaterial::GizmoMaterial() :_texture(nullptr), _scale(1.0f) {
	setReuseShaderKey("gizmo_icon_material");
	initShader();
}
	
GizmoMaterial::~GizmoMaterial() {
	
}
	
void GizmoMaterial::initShader() {
	if (Graphics::get()->getApi() == Graphics::kApiOpenGL || Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLBasic) {
		getShader()->attachShader(Shader::kTypeVertex, s_vshader);
		getShader()->attachShader(Shader::kTypeFragment, s_fshader);
	}
	else if (Graphics::get()->getApi()==Graphics::kApiOpenGLAdvanced) {
		getShader()->attachShader(Shader::kTypeVertex, s_vshader_gl3);
		getShader()->attachShader(Shader::kTypeFragment, s_fshader_gl3);
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_Color");
	}
	else {
		std::cerr << "Warning: GizmoMaterial shader is not compatible with the current API" << std::endl;
	}
	getShader()->link();
	
	loadVertexAttrib("in_Position");
	loadTexCoord0Attrib("in_TexCoord");
	
	getShader()->initUniformLocation("uMVPMatrix");
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uPickId");
	
}

void GizmoMaterial::setupUniforms() {
	vwgl::Matrix4 mv = vwgl::State::get()->modelViewMatrix();
	mv.resetRotation();
	mv.resetScale();
	mv.scale(_scale, _scale, _scale);
	vwgl::Matrix4 mvp = vwgl::State::get()->projectionMatrix();
	mvp.mult(mv);
	getShader()->setUniform("uMVPMatrix", mvp);
	
	if (!_pickerId.isZero()) {
		getShader()->setUniform("uTexture", TextureManager::get()->whiteTexture(), Texture::kTexture0);
		Vector4 colorPickId = Vector4(static_cast<float>(_pickerId.r()) / 255.0f,
									  static_cast<float>(_pickerId.g()) / 255.0f,
									  static_cast<float>(_pickerId.b()) / 255.0f,
									  static_cast<float>(_pickerId.a()) / 255.0f);
		getShader()->setUniform("uPickId", colorPickId);
	}
	else if (_texture!=nullptr) {
		getShader()->setUniform("uTexture", _texture, Texture::kTexture0);
		getShader()->setUniform("uPickId", vwgl::Color::white());
	}
	else {
		getShader()->setUniform("uTexture", TextureManager::get()->whiteTexture(), Texture::kTexture0);
		getShader()->setUniform("uPickId", vwgl::Color::white());
	}
}


std::string GizmoPlistMaterial::s_vshader =
"attribute vec4 in_Position;\n"
"attribute vec2 in_TexCoord;\n"

"uniform mat4 uMVPMatrix;\n"

"varying vec2 vTexCoord;\n"
"void main(void) {\n"
"	gl_Position = uMVPMatrix * in_Position;\n"
"	vTexCoord = in_TexCoord;\n"
"}\n";

std::string GizmoPlistMaterial::s_fshader =
"varying vec2 vTexCoord;\n"

"uniform vec4 uColor;\n"

"void main(void) {\n"
"	gl_FragColor = uColor;\n"
"}\n";

std::string GizmoPlistMaterial::s_vshader_gl3 =
"#version 150\n"
"in vec4 in_Position;\n"
"in vec2 in_TexCoord;\n"

"uniform mat4 uMVPMatrix;\n"

"out vec2 f_TexCoord;\n"

"void main(void) {\n"
"	gl_Position = uMVPMatrix * in_Position;\n"
"	f_TexCoord = in_TexCoord;\n"
"}\n";;

std::string GizmoPlistMaterial::s_fshader_gl3 =
"#version 150\n"

"in vec2 f_TexCoord;\n"
"out vec4 out_Color;\n"

"uniform vec4 uColor;\n"

"void main(void) {\n"
"	out_Color = uColor;\n"
"}\n";


GizmoPlistMaterial::GizmoPlistMaterial() :_color(Color::white()), _scale(1.0f) {
	setReuseShaderKey("gizmo_plist_material");
	initShader();
}

GizmoPlistMaterial::~GizmoPlistMaterial() {
	
}

void GizmoPlistMaterial::initShader() {
	if (Graphics::get()->getApi() == Graphics::kApiOpenGL || Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLBasic) {
		getShader()->attachShader(Shader::kTypeVertex, s_vshader);
		getShader()->attachShader(Shader::kTypeFragment, s_fshader);
	}
	else if (Graphics::get()->getApi()==Graphics::kApiOpenGLAdvanced) {
		getShader()->attachShader(Shader::kTypeVertex, s_vshader_gl3);
		getShader()->attachShader(Shader::kTypeFragment, s_fshader_gl3);
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_Color");
	}
	else {
		std::cerr << "Warning: GizmoMaterial shader is not compatible with the current API" << std::endl;
	}
	getShader()->link();
	
	loadVertexAttrib("in_Position");
	loadTexCoord0Attrib("in_TexCoord");
	
	getShader()->initUniformLocation("uMVPMatrix");
	getShader()->initUniformLocation("uColor");
	
}

void GizmoPlistMaterial::setupUniforms() {
	vwgl::Matrix4 mv = vwgl::State::get()->modelViewMatrix();
	mv.resetScale();
	mv.scale(_scale, _scale, _scale);
	vwgl::Matrix4 mvp = vwgl::State::get()->projectionMatrix();
	mvp.mult(mv);
	getShader()->setUniform("uMVPMatrix", mvp);
	
	getShader()->setUniform("uColor", _color);
}


	
vwgl::ptr<vwgl::PolyList> DrawGizmosVisitor::s_iconRect;
vwgl::ptr<GizmoMaterial> DrawGizmosVisitor::s_material;
vwgl::ptr<GizmoPlistMaterial> DrawGizmosVisitor::s_plistMaterial;
	
DrawGizmosVisitor::DrawGizmosVisitor()
	:_manipulatorPass(false)
{
	
}
	
void DrawGizmosVisitor::visit(scene::Node * node) {
	node->willDraw();
	
	ptr<Texture> texture = node->getGizmoIcon();
	PolyList * plist = getIconRect();
	PolyList * gizmoPlist = node->getGizmoPolyList();
	GizmoMaterial * mat = getMaterial();
	GizmoPlistMaterial * plistMat = getPlistMaterial();
	
	vwgl::State::get()->pushModelMatrix();
	Viewport vp = State::get()->getViewport();
	float distance = vwgl::State::get()->modelViewMatrix().multVector(vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f)).z();
	distance = vwgl::Math::abs(distance);
	
	if (texture.valid() && plist && mat && !_manipulatorPass) {
		vwgl::State::get()->enableBlend(vwgl::State::kBlendFunctionAlpha);
		
		// Component icons:
		node->eachComponent([&](scene::Component* comp) {
			if (comp->getGizmoIcon()) {
				comp->willDrawIcon();
				vwgl::State::get()->pushModelMatrix();
				mat->setScale(distance * 50.0f / vp.height());
				
				mat->setTexture(comp->getGizmoIcon());
				mat->setPickerIdentifier(PickerIdentifier());
				mat->bindPolyList(plist);
				mat->activate();
				plist->drawElements();
				mat->deactivate();
				vwgl::State::get()->popModelMatrix();
				comp->didDrawIcon();
			}
		});

		
		// Node icon:
		mat->setScale(distance * 50.0f / vp.height());

		mat->setTexture(texture.getPtr());
		mat->setPickerIdentifier(PickerIdentifier());
		mat->bindPolyList(plist);
		mat->activate();
		plist->drawElements();
		mat->deactivate();
		
 
		vwgl::State::get()->disableBlend();
	}
	
	if (gizmoPlist && plistMat && !_manipulatorPass) {
		plistMat->setScale(distance * 50.0f / vp.height());
		
		// TODO: Get this color from the node
		plistMat->setColor(Color::white());
		plistMat->bindPolyList(gizmoPlist);
		plistMat->activate();
		gizmoPlist->drawElements();
		plistMat->deactivate();
	}
	
	
	vwgl::State::get()->popModelMatrix();
	
	if (node==_manipulateNode.getPtr() && _manipulateGizmo.valid() && _manipulatorPass) {
		_manipulateGizmo->draw();
	}
}
	
void DrawGizmosVisitor::didVisit(scene::Node *node) {
	node->didDraw();
}

vwgl::PolyList * DrawGizmosVisitor::getIconRect() {
	if (!s_iconRect.valid()) {
		s_iconRect = new vwgl::PolyList();
		
		s_iconRect->addVertex(Vector3(-0.5f, 0.5f, 0.0f));
		s_iconRect->addVertex(Vector3(-0.5f,-0.5f, 0.0f));
		s_iconRect->addVertex(Vector3( 0.5f,-0.5f, 0.0f));
		s_iconRect->addVertex(Vector3( 0.5f, 0.5f, 0.0f));
		
		s_iconRect->addTexCoord0(Vector2(0.0f, 1.0f));
		s_iconRect->addTexCoord0(Vector2(0.0f, 0.0f));
		s_iconRect->addTexCoord0(Vector2(1.0f, 0.0f));
		s_iconRect->addTexCoord0(Vector2(1.0f, 1.0f));
		
		s_iconRect->addTriangle(0, 1, 2);
		s_iconRect->addTriangle(2, 3, 0);
		
		s_iconRect->buildPolyList();
	}
	return s_iconRect.getPtr();
}

GizmoMaterial * DrawGizmosVisitor::getMaterial() {
	if (!s_material.valid()) {
		s_material = new GizmoMaterial();
	}
	return s_material.getPtr();
}
	
GizmoPlistMaterial * DrawGizmosVisitor::getPlistMaterial() {
	if (!s_plistMaterial.valid()) {
		s_plistMaterial = new GizmoPlistMaterial();
	}
	return s_plistMaterial.getPtr();
}

}
}

