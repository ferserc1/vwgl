
#include <vwgl/manipulation/selection_handler.hpp>

#include <vwgl/drawable.hpp>

#include <algorithm>

namespace vwgl {
namespace manipulation {

SelectionHandler::SelectionHandler()
	:_markMaterial(true)
	,_lockNotifications(false)
{
		
}

void SelectionHandler::selectNode(scene::Node *node, bool forceSelect) {
	bool additive = _additiveSelection;
	if (!additive) {
		_nodeList.clear();
	}
	NodeList::iterator it = std::find(_nodeList.begin(), _nodeList.end(), node);
	if (it==_nodeList.end()) {
		_nodeList.push_back(node);
	}
	else if (additive && !forceSelect) {
		_nodeList.remove(*it);
	}
	notifyObservers();
}

void SelectionHandler::selectDrawable(scene::Drawable * drw, bool forceSelect) {
	bool additive = _additiveSelection;
	if (!additive) {
		_drawableList.clear();
	}
	DrawableList::iterator it = std::find(_drawableList.begin(), _drawableList.end(), drw);
	if (it==_drawableList.end()) {
		_drawableList.push_back(drw);
	}
	else if (additive && !forceSelect) {
		_drawableList.remove(*it);
	}
	notifyObservers();
}

void SelectionHandler::selectPolyList(PolyList * plist) {
	bool additive = _additiveSelection;
	if (!additive) {
		_polyListList.clear();
	}
	PolyListList::iterator it = std::find(_polyListList.begin(), _polyListList.end(), plist);
	if (it==_polyListList.end()) {
		_polyListList.push_back(plist);
	}
	else if (additive) {
		_polyListList.remove(*it);
	}
	notifyObservers();
}

void SelectionHandler::selectMaterial(Material * mat) {
	bool additive = _additiveSelection;
	unmarkMaterials();
	if (!additive) {
		_materialList.clear();
	}
	MaterialList::iterator it = std::find(_materialList.begin(), _materialList.end(), mat);
	if (it==_materialList.end()) {
		_materialList.push_back(mat);
	}
	else if (additive) {
		_materialList.remove(*it);
	}
	markMaterials();
	notifyObservers();
}
	
void SelectionHandler::select(scene::Node * node, scene::Drawable * drw, PolyList * plist, Material * mat) {
	bool additive = _additiveSelection;
	lockNotifications();
	
	if (node) selectNode(node);
	if (drw) selectDrawable(drw);
	if (plist) selectPolyList(plist);
	if (mat) selectMaterial(mat);
	
	// The user picked a gizmo icon
	if (drw && !plist) {
		if (!additive) {
			unmarkMaterials();
			_polyListList.clear();
			_materialList.clear();
		}
		setAdditiveSelection(true);
		drw->eachElement([&] (PolyList * plist, Material * mat, vwgl::Transform & trx) {
			this->selectPolyList(plist);
			this->selectMaterial(mat);
		});
		setAdditiveSelection(additive);
	}
	
	if (additive) {
		// The drawable and node may be deselected, but if there are polyLists belonging to the drawable, we must to select it again
		eachPolyList([&](PolyList * selectedPolyList) {
			if (drw && drw->getPolyListIndex(selectedPolyList)!=-1) {
				selectDrawable(drw,true);
				if (node) selectNode(node,true);
			}
		});
	}
	
	unlockNotifications();
	notifyObservers();
}

void SelectionHandler::notifyObservers() {
	if (!_lockNotifications) {
		ObserverList::iterator it;
		for (it=_observers.begin(); it!=_observers.end(); ++it) {
			(*it)->selectionChanged(*this);
		}
	}
}

void SelectionHandler::markMaterials() {
	if (_markMaterial) {
		eachMaterial([&](Material * mat) {
			GenericMaterial * m = dynamic_cast<GenericMaterial*>(mat);
			if (m) {
				m->setSelectedMode(true);
			}
		});
	}
}

void SelectionHandler::unmarkMaterials() {
	if (_markMaterial) {
		eachMaterial([&](Material * mat) {
			GenericMaterial * m = dynamic_cast<GenericMaterial*>(mat);
			if (m) {
				m->setSelectedMode(false);
			}
		});
	}
}
	
void SelectionHandler::registerObserver(ISelectionObserver * observer) {
	ObserverList::iterator it = std::find(_observers.begin(), _observers.end(), observer);
	if (it==_observers.end()) {
		_observers.push_back(observer);
	}
}

void SelectionHandler::unregisterObserver(ISelectionObserver * observer) {
	_observers.remove(observer);
}

}
}