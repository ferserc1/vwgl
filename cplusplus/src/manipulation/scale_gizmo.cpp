
#include <vwgl/manipulation/scale_gizmo.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/state.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/cmd/scale_command.hpp>

namespace vwgl {
namespace manipulation {
		
ScaleGizmo::ScaleGizmo()
{
	loadDrawable(Loader::get()->loadModel("scale_gizmo.vwglb",true));
}
	
ScaleGizmo::~ScaleGizmo() {
	
}

void ScaleGizmo::draw() {
	State::get()->pushModelMatrix();
	State::get()->pushViewMatrix();
	float height = static_cast<float>(State::get()->getViewport().height()) / 100.0f;
	Matrix4 trx = State::get()->modelMatrix();
	
	float sx = Vector3(trx.get00(), trx.get01(), trx.get02()).magnitude();
	float sy = Vector3(trx.get10(), trx.get11(), trx.get12()).magnitude();
	float sz = Vector3(trx.get20(), trx.get21(), trx.get22()).magnitude();
	trx.scale(sx, sy, sz);
	
	Vector3 rx(trx.get00(), trx.get01(), trx.get02());
	Vector3 ry(trx.get10(), trx.get11(), trx.get12());
	Vector3 rz(trx.get20(), trx.get21(), trx.get22());
	rx.normalize();
	ry.normalize();
	rz.normalize();
	trx.set00(rx.x()); trx.set01(rx.y()); trx.set02(rx.z());
	trx.set10(ry.x()); trx.set11(ry.y()); trx.set12(ry.z());
	trx.set20(rz.x()); trx.set21(rz.y()); trx.set22(rz.z());
	
	Matrix4 rotation = trx.getRotation();
	
	vwgl::Vector3 pos = trx.getPosition();
	
	
	float distance = vwgl::State::get()->modelViewMatrix().multVector(vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f)).z();
	distance *= _scale / height;
	vwgl::State::get()->modelMatrix().identity().translate(pos).scale(-distance,-distance,-distance).mult(rotation);
	
	if (drawable()) {
		drawable()->draw();
		selectable()->draw();
	}
	
	State::get()->popModelMatrix();
	State::get()->popViewMatrix();
}

void ScaleGizmo::beginAction() {
	_scaleVector.set(0.0f);
	scene::Transform * trx = getTarget();
	if (trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
		_restoreScale = trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->getScale();
	}
	else if (trx) {
		_restoreMatrix = trx->getTransform().getMatrix();
	}
}

void ScaleGizmo::offsetChanged(unsigned int axis, const Vector2i &offset) {
	scene::Camera * cam = scene::Camera::getMainCamera();
	scene::Transform * trx = getTarget();
	if (cam && trx) {
		Vector3 right = cam->getTransform().rightVector();
		Vector3 up = cam->getTransform().upVector();
		float offsetMultiplier = 0.01f;
		Vector3 scale;
		
		if (axis & kAxisX && axis & kAxisY && axis & kAxisZ) {
			float value = static_cast<float>(offset.x()) * right.x() * offsetMultiplier +
						  static_cast<float>(offset.y()) * right.z() * -offsetMultiplier;
			scale.set(value, value, value);
			
		}
		else if (axis & kAxisX) {
			scale.x(static_cast<float>(offset.x()) * right.x() * offsetMultiplier +
						static_cast<float>(offset.y()) * right.z() * -offsetMultiplier);
		}
		else if (axis & kAxisY) {
			scale.y(static_cast<float>(offset.y()) * up.y() * -offsetMultiplier);
		}
		else if (axis & kAxisZ) {
			scale.z(static_cast<float>(offset.y()) * right.x() * offsetMultiplier +
						static_cast<float>(offset.x()) * right.z() * offsetMultiplier);
		}
		
		_scaleVector.add(scale);
		if (trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
			Vector3 totalScale(_restoreScale.x() * (1.0f + _scaleVector.x()),
							   _restoreScale.y() * (1.0f + _scaleVector.y()),
							   _restoreScale.z() * (1.0f + _scaleVector.z()));
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->scale(totalScale);
		}
		else {
			trx->getTransform().setMatrix(_restoreMatrix);
			trx->getTransform().getMatrix().scale(1.0f + _scaleVector.x(), 1.0f + _scaleVector.y(), 1.0f + _scaleVector.z());
		}
	}
}

void ScaleGizmo::commit() {
	scene::Transform * trx = getTarget();
	if (trx && getCommandManager()) {
		Vector3 scale(1.0f + _scaleVector.x(), 1.0f + _scaleVector.y(), 1.0f + _scaleVector.z());
		if (trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
			trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->scale(_restoreScale);
		}
		else {
			trx->getTransform().setMatrix(_restoreMatrix);
		}
		getCommandManager()->execute(new cmd::ScaleCommand(trx,scale));
	}
}

void ScaleGizmo::discard() {
	scene::Transform * trx = getTarget();
	if (trx && trx->getTransform().getTransformStrategy<TRSTransformStrategy>()) {
		trx->getTransform().getTransformStrategy<TRSTransformStrategy>()->scale(_restoreScale);
	}
	else if (trx) {
		trx->getTransform().setMatrix(_restoreMatrix);
	}
}


}
}