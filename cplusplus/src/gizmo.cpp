
#include <vwgl/gizmo.hpp>
#include <vwgl/loader.hpp>

namespace vwgl {

Gizmo::Gizmo(const std::string & modelPath) :_resetRotation(false) {
	init(modelPath);
}

Gizmo::~Gizmo() {
	
}

void Gizmo::init(const std::string &modelPath) {
	_drawable = Loader::get()->loadDrawable(modelPath);
	if (_drawable.valid()) {
		ptr<ColorPicker> colorPicker = new ColorPicker();
		colorPicker->assignPickId(_drawable.getPtr());
		
		//loadPickIds();
	}
}

}
