
#include <vwgl/image.hpp>
#include <SOIL.h>
#include <string.h>
#include <cstring>

namespace vwgl {

Image::Image()
	:_buffer(0L),
	_format(kFormatNone),
	_bytesPerPixel(0),
	_width(0),
	_height(0),
	_lastStatus(true)
{

}

Image::Image(const std::string & path)
	:_buffer(0L),
	_format(kFormatNone),
	_bytesPerPixel(0),
	_width(0),
	_height(0),
	_lastStatus(true)
{
	load(path);
}

Image::Image(const Image * clone)
	:_buffer(0L),
	_format(kFormatNone),
	_bytesPerPixel(0),
	_width(0),
	_height(0),
	_lastStatus(true)
{
	if (clone) {
		std::memcpy(_buffer, clone->_buffer, clone->_width * clone->_height * clone->_bytesPerPixel);
		_format = clone->_format;
		_bytesPerPixel = clone->_bytesPerPixel;
		_width = clone->_width;
		_height = clone->_height;
	}
	else {
		_lastStatus = false;
	}
}
	
Image::~Image() {
	destroy();
}

void Image::setData(unsigned char * buffer, const Size2Di size, int bytesPerPixel, ImageFormat fmt) {
	setBuffer(buffer);
	_width = size.width();
	_height = size.height();
	_bytesPerPixel = bytesPerPixel;
	_format = fmt;
}

Image * Image::load(const std::string &path) {
	unsigned char * imageBuffer = SOIL_load_image(path.c_str(), (int*)&_width, (int*)&_height, (int*)&_bytesPerPixel, SOIL_LOAD_AUTO);
	if (imageBuffer!=NULL) {
		// Flip vertical
		_buffer = new unsigned char[_width*_height*_bytesPerPixel];
		for (unsigned int i=0; i<_height; ++i) {
			memcpy(&_buffer[(_height-i-1)*_width*_bytesPerPixel], &imageBuffer[i*_width*_bytesPerPixel], _width*_bytesPerPixel);
		}
		_fileName = path;
	}
	_lastStatus = _buffer!=NULL ? true:false;
	return this;
}

Image * Image::saveBMP(const std::string & path) {
	this->save(path, SOIL_SAVE_TYPE_BMP);
	return this;
}

Image * Image::saveTGA(const std::string & path) {
	this->save(path, SOIL_SAVE_TYPE_TGA);
	return this;
}

void Image::save(const std::string & path, int format) {
	if (!valid()) {
		_lastStatus = false;
	}
	else {
		_lastStatus = SOIL_save_image(path.c_str(), format, _width, _height, _bytesPerPixel, _buffer)==0 ? false:true;
	}

}


void Image::destroy() {
	if (valid()) {
		delete _buffer;
		_buffer = 0L;
		_format = kFormatNone;
		_bytesPerPixel = 0;
		_width = 0;
		_height = 0;
	}
}

}
