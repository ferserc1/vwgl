
#include <vwgl/render_pass.hpp>


namespace vwgl {

RenderPass::RenderPass() :_clearColor(1.0), _sceneRoot(NULL), _clearBuffers(true), _renderTransparentObjects(true) {
	
}

RenderPass::RenderPass(Group * sceneRoot, Material * mat, FramebufferObject * fbo) :_clearColor(1.0), _clearBuffers(true), _renderTransparentObjects(true) {
	create(sceneRoot, mat, fbo);
}

RenderPass::~RenderPass() {
	destroy();
}
	
void RenderPass::setSceneRoot(Group * sceneRoot) {
	_sceneRoot = sceneRoot;
}

void RenderPass::create(Group *sceneRoot, Material *mat, FramebufferObject *fbo) {
	_sceneRoot = sceneRoot;
	create(mat, fbo);
}

void RenderPass::create(Material * mat, FramebufferObject * fbo) {
	_renderer = new SceneRenderer();
	_renderer->getNodeVisitor()->setMaterial(mat);
	_renderer->getNodeVisitor()->setUseMaterialSettings(true);
	_fbo = fbo;
}
	
void RenderPass::clear() {
	if (_fbo.valid() && _sceneRoot && _renderer.valid()) {
		_fbo->bind();
		glViewport(0, 0, _fbo->getWidth(), _fbo->getHeight());
		glClearColor(_clearColor.r(), _clearColor.g(), _clearColor.b(), _clearColor.a());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_fbo->unbind();
	}
}

void RenderPass::updateTexture(bool alphaRender) {
	if (_fbo.valid() && _sceneRoot && _renderer.valid()) {
		_fbo->bind();
		glViewport(0, 0, _fbo->getWidth(), _fbo->getHeight());
		glClearColor(_clearColor.r(), _clearColor.g(), _clearColor.b(), _clearColor.a());
		if (_clearBuffers) glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_renderer->setRenderTransparency(_renderTransparentObjects);
		_renderer->draw(_sceneRoot,alphaRender);
		_fbo->unbind();
	}
}
	
void RenderPass::setMaterial(vwgl::Material *mat) {
	_renderer->getNodeVisitor()->setMaterial(mat);
}

void RenderPass::destroy() {
	_sceneRoot = nullptr;
	_fbo = nullptr;
	//_visitor = NULL;
	_renderer = nullptr;
}

}
