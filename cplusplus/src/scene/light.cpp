
#include <vwgl/scene/light.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/camera.hpp>

// TODO: This include is required by <vwgl/light.hpp>
#include <vwgl/drawable.hpp>

#include <vwgl/scene/render_pass.hpp>
#include <vwgl/shadow_map_material.hpp>

namespace vwgl {
namespace scene {
	
ComponentFactory<scene::Light> lightFactory("Light");

Light::LightList Light::s_lightVector;
	
RenderPass Light::s_renderPass;

	
Size2Di Light::s_shadowMapSize(512);

void Light::updateShadowMap(scene::Node *sceneRoot, scene::Light *light) {
	if (s_renderPass.getFbo()==nullptr || s_renderPass.getFbo()->getWidth()!=s_shadowMapSize.width()) {
		createShadowMap();
	}
	if (s_renderPass.getFbo() && light->getCastShadows()) {
		vwgl::State::get()->pushProjectionMatrix();
		vwgl::State::get()->pushViewMatrix();

		vwgl::State::get()->viewMatrix() = light->getViewMatrix();
		vwgl::State::get()->projectionMatrix() = light->getProjectionMatrix();

		s_renderPass.updateTexture(sceneRoot,true,false,true);
		
		vwgl::State::get()->popProjectionMatrix();
		vwgl::State::get()->popViewMatrix();
	}
}
	
void Light::createShadowMap() {
	if (s_renderPass.getFbo() && s_renderPass.getFbo()->getWidth()!=s_shadowMapSize.width()) {
		s_renderPass.getFbo()->resize(s_shadowMapSize);
	}
	else {
		vwgl::ShadowMapMaterial * renderPassMat = new vwgl::ShadowMapMaterial();
		s_renderPass.setup(renderPassMat, new vwgl::FramebufferObject(s_shadowMapSize));
		s_renderPass.setClearColor(vwgl::Color::black());
	}
}

void Light::registerLight(Light * l) {
	s_lightVector.push_back(l);
}

void Light::unregisterLight(Light * l) {
	LightList::iterator it = std::find(s_lightVector.begin(), s_lightVector.end(), l);
	if (it!=s_lightVector.end()) {
		s_lightVector.erase(it);
	}
}
	
Light::Light()
	:ILight(),
	_intensity(1.0f),
	_constantAtt(1.0f),
	_linearAtt(0.5f),
	_expAtt(0.1f),
	_spotExponent(30.0f),
	_spotCutoff(20.0f),
	_shadowStrength(1.0f),
	_cutoffDistance(-1.0f),
	_castShadows(true),
	_shadowBias(0.0029f)
{
	_ambient = vwgl::Color(0.2f, 0.2f, 0.2f, 1.0f);
	_diffuse = vwgl::Color(0.9f, 0.9f, 0.9f, 1.0f);
	_specular = vwgl::Color::white();
	_transformMatrix.identity();
	_locationHelper.setComponent(this);
	Light::registerLight(this);
}

Light::~Light() {
	Light::unregisterLight(this);
}

Vector3 Light::getPosition() {
	return _locationHelper.getPosition();
}

Vector3 Light::getDirection() {
	return _locationHelper.getCameraDirection();
}
	
vwgl::Matrix4 & Light::getTransform() {
	return _locationHelper.getTransformMatrix();
}

vwgl::Matrix4 & Light::getAbsoluteTransform() {
	_locationHelper.getTransformMatrix().identity();
	return _locationHelper.getAbsoluteTransformMatrix();
}

vwgl::Matrix4 & Light::getViewMatrix() {
	_directionalViewMatrix = getAbsoluteTransform();
	Camera * cam = Camera::getMainCamera();
	if (getType()==kTypeDirectional && cam) {
		Matrix4 rotation = _directionalViewMatrix.getRotation();
		_directionalViewMatrix.identity();
		Vector3 targetPosition = cam->getTransform().getPosition();
		_directionalViewMatrix.translate(targetPosition);
		_directionalViewMatrix.mult(rotation);
		_directionalViewMatrix.translate(0.0f, 0.0f, 50.0f);
	}
	_directionalViewMatrix.invert();
	
	return _directionalViewMatrix;
}
	
bool Light::serialize(JsonSerializer & serializer, bool lastItem) {
	serializer.openObject(true, true);
	serializer.writeProp<std::string>("type", "Light", true, true);
	switch (_lightType) {
		case ILight::kTypeDirectional:
			serializer.writeProp<std::string>("lightType", "kTypeDirectional", true, true);
			break;
		case ILight::kTypePoint:
			serializer.writeProp<std::string>("lightType", "kTypePoint", true, true);
			break;
		case ILight::kTypeSpot:
			serializer.writeProp<std::string>("lightType", "kTypeSpot", true, true);
			break;
		case ILight::kTypeDisabled:
			serializer.writeProp<std::string>("lightType", "kTypeDisabled", true, true);
			break;
	}
	serializer.writeProp("ambient", _ambient, true, true);
	serializer.writeProp("diffuse", _diffuse, true, true);
	serializer.writeProp("specular", _specular, true, true);
	serializer.writeProp("intensity", _intensity, true, true);
	serializer.writeProp("constantAtt", _constantAtt, true, true);
	serializer.writeProp("linearAtt", _linearAtt, true, true);
	serializer.writeProp("expAtt", _expAtt, true, true);
	serializer.writeProp("spotCutoff", _spotCutoff, true, true);
	serializer.writeProp("shadowStrength", _shadowStrength, true, true);
	serializer.writeProp("cutoffDistance", _cutoffDistance, true, true);
	serializer.writeProp("projection", _projection, true, true);
	serializer.writeProp("castShadows", _castShadows, true, true);
	serializer.writeProp("shadowBias", _shadowBias, true, true);
	serializer.closeObject(true, !lastItem, true);
	return true;
}
	
void Light::deserialize(JsonDeserializer & deserializer, const std::string &resourcePath) {
	std::string lightType = deserializer.getString("lightType", "kTypeDirectional");
	if (lightType=="kTypeDirectional") {
		setType(kTypeDirectional);
	}
	else if (lightType=="kTypePoint") {
		setType(kTypePoint);
	}
	else if (lightType=="kTypeSpot") {
		setType(kTypeSpot);
	}
	else if (lightType=="kTypeDisabled") {
		setType(kTypeDisabled);
	}
	_ambient = deserializer.getVector4("ambient", _ambient);
	_diffuse = deserializer.getVector4("diffuse", _diffuse);
	_specular = deserializer.getVector4("specular", _specular);
	_intensity = deserializer.getFloat("intensity", _intensity);
	_constantAtt = deserializer.getFloat("constantAtt", _constantAtt);
	_linearAtt = deserializer.getFloat("linearAtt", _linearAtt);
	_expAtt = deserializer.getFloat("expAtt", _expAtt);
	_spotCutoff = deserializer.getFloat("spotCutoff", _spotCutoff);
	_shadowStrength = deserializer.getFloat("shadowStrength", _shadowStrength);
	_cutoffDistance = deserializer.getFloat("cutoffDistance", _cutoffDistance);
	_projection = deserializer.getMatrix4("projection", _projection);
	_castShadows = deserializer.getBool("castShadows", _castShadows);
	_shadowBias = deserializer.getFloat("shadowBias", _shadowBias);
}

LightModifier Light::getModifierWithMask(unsigned int mask) {
	LightModifier mod;
	if (mask & LightModifier::kType) {
		mod.setType(getType());
	}
	if (mask & LightModifier::kAmbient) {
		mod.setAmbient(getAmbient());
	}
	if (mask & LightModifier::kDiffuse) {
		mod.setDiffuse(getDiffuse());
	}
	if (mask & LightModifier::kSpecular) {
		mod.setSpecular(getSpecular());
	}
	if (mask & LightModifier::kConstantAtt) {
		mod.setConstantAttenuation(getConstantAttenuation());
	}
	if (mask & LightModifier::kLinearAtt) {
		mod.setLinearAttenuation(getLinearAttenuation());
	}
	if (mask & LightModifier::kExpAtt) {
		mod.setExpAttenuation(getExpAttenuation());
	}
	if (mask & LightModifier::kSpotCutoff) {
		mod.setSpotCutoff(getSpotCutoff());
	}
	if (mask & LightModifier::kSpotExponent) {
		mod.setSpotExponent(getSpotExponent());
	}
	if (mask & LightModifier::kShadowStrength) {
		mod.setShadowStrength(getShadowStrength());
	}
	if (mask & LightModifier::kCutoffDistance) {
		mod.setCutoffDistance(getCutoffDistance());
	}
	if (mask & LightModifier::kCastShadows) {
		mod.setCastShadows(getCastShadows());
	}
	if (mask & LightModifier::kShadowBias) {
		mod.setShadowBias(getShadowBias());
	}
	return mod;
}
	
void Light::applyModifier(const LightModifier & mod) {
	if (mod.isEnabled(LightModifier::kType)) {
		setType(mod.getType());
	}
	if (mod.isEnabled(LightModifier::kAmbient)) {
		setAmbient(mod.getAmbient());
	}
	if (mod.isEnabled(LightModifier::kDiffuse)) {
		setDiffuse(mod.getDiffuse());
	}
	if (mod.isEnabled(LightModifier::kSpecular)) {
		setSpecular(mod.getSpecular());
	}
	if (mod.isEnabled(LightModifier::kConstantAtt)) {
		setConstantAttenuation(mod.getConstantAttenuation());
	}
	if (mod.isEnabled(LightModifier::kLinearAtt)) {
		setLinearAttenuation(mod.getLinearAttenuation());
	}
	if (mod.isEnabled(LightModifier::kExpAtt)) {
		setExpAttenuation(mod.getExpAttenuation());
	}
	if (mod.isEnabled(LightModifier::kSpotCutoff)) {
		setSpotCutoff(mod.getSpotCutoff());
	}
	if (mod.isEnabled(LightModifier::kSpotExponent)) {
		setSpotExponent(mod.getSpotExponent());
	}
	if (mod.isEnabled(LightModifier::kShadowStrength)) {
		setShadowStrength(mod.getShadowStrength());
	}
	if (mod.isEnabled(LightModifier::kCutoffDistance)) {
		setCutoffDistance(mod.getCutoffDistance());
	}
	if (mod.isEnabled(LightModifier::kCastShadows)) {
		setCastShadows(mod.getCastShadows());
	}
	if (mod.isEnabled(LightModifier::kShadowBias)) {
		setShadowBias(mod.getShadowBias());
	}
}

}
}