
#include <vwgl/scene/scene_component.hpp>

#include <vwgl/scene/billboard.hpp>
#include <vwgl/scene/bounding_box.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/scene/chain.hpp>
#include <vwgl/scene/cubemap.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/scene/joint.hpp>
#include <vwgl/scene/light.hpp>
#include <vwgl/scene/projector.hpp>
#include <vwgl/scene/transform.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace scene {

Billboard * SceneComponent::billboard() {
	return component<Billboard>();
}

BoundingBox * SceneComponent::boundingBox() {
	return component<BoundingBox>();
}

Camera * SceneComponent::camera() {
	return component<Camera>();
}

Chain * SceneComponent::chain() {
	return component<Chain>();
}

Cubemap * SceneComponent::cubemap() {
	return component<Cubemap>();
}

Drawable * SceneComponent::drawable() {
	return component<Drawable>();
}

InputChainJoint * SceneComponent::inputJoint() {
	return component<InputChainJoint>();
}

OutputChainJoint * SceneComponent::outputJoint() {
	return component<OutputChainJoint>();
}

Light * SceneComponent::light() {
	return component<Light>();
}

Projector * SceneComponent::projector() {
	return component<Projector>();
}

Transform * SceneComponent::transform() {
	return component<Transform>();
}

}
}