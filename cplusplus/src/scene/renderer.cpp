
#include <vwgl/scene/renderer.hpp>
#include <vwgl/state.hpp>

#include <vwgl/scene/forward_renderer.hpp>
#include <vwgl/scene/deferred_renderer.hpp>
#include <vwgl/scene/advanced_deferred_renderer.hpp>
#include <vwgl/graphics.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace scene {

	
Renderer * Renderer::create(Renderer::RenderPath rp) {
	return create(rp, nullptr);
}
	
Renderer * Renderer::create(Renderer::RenderPath rp, scene::Node * sceneRoot) {
	switch (rp) {
	case kRenderPathDeferred:
		if (Graphics::get()->getApi() == Graphics::kApiOpenGLAdvanced) {
			return new scene::AdvancedDeferredRenderer(sceneRoot);
		}
		else if (Graphics::get()->getApi() == Graphics::kApiOpenGL) {
			return new scene::DeferredRenderer(sceneRoot);
		}
		else if (Graphics::get()->getApi() == Graphics::kApiOpenGLBasic) {
			return new scene::ForwardRenderer(sceneRoot);
		}
	case kRenderPathForward:
	default:
		return new scene::ForwardRenderer();
	};
}
	
Renderer::Renderer()
	:_clearColor(Color::black())
{
	
}

Renderer::Renderer(scene::Node * root)
	:_sceneRoot(root)
{
	
}
	
Renderer::~Renderer() {
	
}

void Renderer::init() {
	State::get()->enableDepthTest();
	if (_sceneRoot.valid()) _sceneRoot->accept(initVisitor());
}

void Renderer::resize(const Size2Di &size) {
	vwgl::State::get()->setViewport(Viewport(0,0,size.width(),size.height()));
	resizeVisitor().setSize(size);
	if (_sceneRoot.valid()) _sceneRoot->accept(resizeVisitor());
}

void Renderer::frame(float delta) {
	frameVisitor().setDelta(delta);
	if (_sceneRoot.valid()) _sceneRoot->accept(frameVisitor());
}

void Renderer::update() {
	if (_sceneRoot.valid()) _sceneRoot->accept(updateVisitor());
}
	
void Renderer::mouseDown(const app::MouseEvent & evt) {
	if (_sceneRoot.valid()) {
		mouseDownVisitor().setEvent(evt);
		_sceneRoot->accept(mouseDownVisitor());
	}
}

void Renderer::mouseUp(const app::MouseEvent & evt) {
	if (_sceneRoot.valid()) {
		mouseUpVisitor().setEvent(evt);
		_sceneRoot->accept(mouseUpVisitor());
	}
}

void Renderer::mouseMove(const app::MouseEvent & evt) {
	if (_sceneRoot.valid()) {
		mouseMoveVisitor().setEvent(evt);
		_sceneRoot->accept(mouseMoveVisitor());
	}
}

void Renderer::mouseDrag(const app::MouseEvent & evt) {
	if (_sceneRoot.valid()) {
		mouseDragVisitor().setEvent(evt);
		_sceneRoot->accept(mouseDragVisitor());
	}
}

void Renderer::keyDown(const app::KeyboardEvent & evt) {
	if (_sceneRoot.valid()) {
		keyDownVisitor().setEvent(evt);
		_sceneRoot->accept(keyDownVisitor());
	}
}

void Renderer::keyUp(const app::KeyboardEvent & evt) {
	if (_sceneRoot.valid()) {
		keyUpVisitor().setEvent(evt);
		_sceneRoot->accept(keyUpVisitor());
	}
}

void Renderer::mouseWheel(const app::MouseEvent &evt) {
	if (_sceneRoot.valid()) {
		mouseWheelVisitor().setEvent(evt);
		_sceneRoot->accept(mouseWheelVisitor());
	}
}

}
}