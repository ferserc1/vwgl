
#include <vwgl/scene/bounding_box.hpp>
#include <vwgl/scene/scene_object.hpp>

namespace vwgl {
namespace scene {

ComponentFactory<BoundingBox>  bboxFactory("BoundingBox");

BoundingBox::BoundingBox()
{
	_boundingBox = new vwgl::BoundingBox();
}

BoundingBox::~BoundingBox() {
	
}

void BoundingBox::init() {
	scene::SceneObject * object = sceneObject();
	scene::Drawable * drawable = object ? object->getComponent<scene::Drawable>():nullptr;
	
	if (drawable) {
		drawable->registerObserver(this);
		_boundingBox->setVertexData(drawable->getPolyListVector());
	}
}
	
void BoundingBox::drawableChanged(vwgl::scene::Drawable * drawable) {
	_boundingBox->setVertexData(drawable->getPolyListVector());
}
	
bool BoundingBox::serialize(vwgl::JsonSerializer &serializer, bool lastItem) {
	// The bounding box components are not exported as scene
	return true;
}
	
void BoundingBox::deserialize(JsonDeserializer & deserializer, const std::string & resourcePath) {
	// The bounding box components are not exported as scene
}

}
}