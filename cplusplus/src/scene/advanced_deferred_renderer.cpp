
#include <vwgl/scene/advanced_deferred_renderer.hpp>
#include <vwgl/deferred_gbuffer_material.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/shadow_render_pass.hpp>

#include <vwgl/loader.hpp>

namespace vwgl {
namespace scene {

AdvancedDeferredRenderer::AdvancedDeferredRenderer()
	:_renderPasses(kRenderPassGBuffers			|
				   kRenderPassTranslucent		|
				   kRenderPassLighting			|
				   kRenderPassShadows			|
				   kRenderPassPostprocess)
{
	build();
}
	
AdvancedDeferredRenderer::AdvancedDeferredRenderer(vwgl::scene::Node * sceneRoot)
	:Renderer(sceneRoot)
	,_renderPasses(kRenderPassGBuffers			|
				   kRenderPassTranslucent		|
				   kRenderPassLighting			|
				   kRenderPassShadows			|
				   kRenderPassPostprocess)
{
	build();
}
	
AdvancedDeferredRenderer::~AdvancedDeferredRenderer() {
	destroy();
}

void AdvancedDeferredRenderer::resize(const Size2Di & size) {
	Renderer::resize(size);
	if (!_canvas.valid()) {
		build();
	}
	
	_viewport = Viewport(0, 0, size.width(), size.height());
	
	_gbufferPass.getFbo()->resize(size);
	_shadowRP.getFbo()->resize(size);
	_translucentRP.getFbo()->resize(size);

	_lightingFbo->resize(size.width(), size.height());
	
	_ssaoMaterial->setViewportSize(Vector2(static_cast<float>(size.width()),
										   static_cast<float>(size.height())));
	_ssaoFbo->resize(size.width(), size.height());
	_bloomFbo->resize(size.width(), size.height());
}

void AdvancedDeferredRenderer::draw() {
	vwgl::State::get()->setClearColor(_clearColor);
	vwgl::State::get()->clearBuffers(State::kBufferColor | State::kBufferDepth);
	
	if (!_canvas.valid()) {
		build();
	}
	
	scene::Camera * cam = scene::Camera::getMainCamera();
	if (cam) {
		prepareFrame(cam);
		
		geometryPass();
		
		lightingPass();
		
		postprocessPass(cam);
		
		presentCanvas(cam);
		
		Drawable::setRenderFlags(Drawable::kRenderOpaque | Drawable::kRenderTransparent);
		Drawable::setForcedMaterial(nullptr);
		Drawable::setUseMaterialSettings(false);
	}
}

void AdvancedDeferredRenderer::build() {
	Size2Di canvasSize = resizeVisitor().getSize();
	_canvas = new HomogeneousRenderCanvas();
	

	DeferredGBufferMaterial * gbufferMat = new DeferredGBufferMaterial();
	_gbufferPass.setMaterial(gbufferMat);
	FramebufferObject * fbo = new FramebufferObject();
	fbo->genFramebuffer(canvasSize);
	fbo->bind();
	fbo->addAttachment(FramebufferObject::kAttachmentColor0, FramebufferObject::kTypeFloatTexture);
	fbo->addAttachment(FramebufferObject::kAttachmentColor1, FramebufferObject::kTypeFloatTexture);
	fbo->addAttachment(FramebufferObject::kAttachmentColor2, FramebufferObject::kTypeFloatTexture);
	fbo->addAttachment(FramebufferObject::kAttachmentColor3, FramebufferObject::kTypeFloatTexture);
	fbo->addAttachment(FramebufferObject::kAttachmentColor4, FramebufferObject::kTypeFloatTexture);
	fbo->addAttachment(FramebufferObject::kAttachmentDepth, FramebufferObject::kTypeDepthRenderbuffer);
	
	GLuint attachments[] = {
		FramebufferObject::kAttachmentColor0,
		FramebufferObject::kAttachmentColor1,
		FramebufferObject::kAttachmentColor2,
		FramebufferObject::kAttachmentColor3,
		FramebufferObject::kAttachmentColor4
	};
	fbo->drawBuffers(5, attachments);
	
	fbo->unbind();
	
	_gbufferPass.setup(gbufferMat, fbo);
	
	ShadowRenderPassMaterial * shadowMat = new ShadowRenderPassMaterial();
	_shadowRP.setup(shadowMat, new FramebufferObject(canvasSize));
	_filters.push_back(shadowMat);
	
	_translucentRP.setup(new GenericMaterial(), new FramebufferObject(canvasSize));
	
	_lightingMaterial = new DeferredLighting();
	_lightingMaterial->setDiffuseMap(fbo->getTexture(FramebufferObject::kAttachmentColor0));
	_lightingMaterial->setNormalMap(fbo->getTexture(FramebufferObject::kAttachmentColor1));
	_lightingMaterial->setSpecularMap(fbo->getTexture(FramebufferObject::kAttachmentColor2));
	_lightingMaterial->setPositionMap(fbo->getTexture(FramebufferObject::kAttachmentColor3));
	_lightingMaterial->setShadowMap(_shadowRP.getFbo()->getTexture());
	_filters.push_back(_lightingMaterial.getPtr());
	
	_lightingCanvas = new HomogeneousRenderCanvas();
	_lightingCanvas->setMaterial(_lightingMaterial.getPtr());
	_lightingFbo = new FramebufferObject(canvasSize, FramebufferObject::kTypeFloatTexture);
	
	_ssaoMaterial = new SSAOMaterial();
	_ssaoMaterial->setPositionMap(fbo->getTexture(FramebufferObject::kAttachmentColor3));
	_ssaoMaterial->setNormalMap(fbo->getTexture(FramebufferObject::kAttachmentColor1));
	_ssaoMaterial->setViewportSize(Vector2(static_cast<float>(_viewport.width()),
										   static_cast<float>(_viewport.height())));
	_filters.push_back(_ssaoMaterial.getPtr());
	
	_ssaoCanvas = new HomogeneousRenderCanvas();
	_ssaoCanvas->setMaterial(_ssaoMaterial.getPtr());
	_ssaoFbo = new FramebufferObject(canvasSize);
	
	_bloomMaterial = new BloomMapMaterial();
	_bloomMaterial->setLightingMap(_lightingFbo->getTexture());
	_bloomFbo = new FramebufferObject(canvasSize);
	_bloomCanvas = new HomogeneousRenderCanvas();
	_bloomCanvas->setMaterial(_bloomMaterial.getPtr());
	_filters.push_back(_bloomMaterial.getPtr());
	
	_postprocessMat = new DeferredPostprocess();
	_postprocessMat->setDiffuseMap(_lightingFbo->getTexture());
	_postprocessMat->setTranslucentMap(TextureManager::get()->blackTexture());
	_postprocessMat->setPositionMap(fbo->getTexture(FramebufferObject::kAttachmentColor3));
	_postprocessMat->setSelectionMap(fbo->getTexture(FramebufferObject::kAttachmentColor4));
	_postprocessMat->setSSAOMap(_ssaoFbo->getTexture());
	_postprocessMat->setBloomMap(_bloomFbo->getTexture());
	_postprocessMat->setTranslucentMap(_translucentRP.getFbo()->getTexture());
	_canvas->setMaterial(_postprocessMat.getPtr());
}

void AdvancedDeferredRenderer::destroy() {
	
}

void AdvancedDeferredRenderer::prepareFrame(scene::Camera * cam) {
	_postprocessMat->setClearColor(_clearColor);
	_postprocessMat->getRenderSettings() = _renderSettings;
	RenderSettings::setCurrentRenderSettings(_postprocessMat->getRenderSettingsPtr());
	_postprocessMat->setSSAOBlur(_ssaoMaterial->getBlurIterations());
	_postprocessMat->setBloomAmount(_bloomMaterial->getBloomAmount());
	scene::Camera::applyTransform(cam);
}

void AdvancedDeferredRenderer::geometryPass() {
	if (kRenderPassGBuffers & _renderPasses) {
		_gbufferPass.setClearColor(_clearColor);
		dynamic_cast<DeferredGBufferMaterial*>(_gbufferPass.getMaterial())->setClearColor(_clearColor);
		_gbufferPass.setClearBuffers(true);
		_gbufferPass.updateTexture(_sceneRoot.getPtr(), true, false, true);		// Opaque objects
	}
	
	if (kRenderPassTranslucent & _renderPasses) {
		_translucentRP.setClearColor(Color::transparent());
		_translucentRP.updateTexture(_sceneRoot.getPtr(), true, true, false);
	}
}

void AdvancedDeferredRenderer::lightingPass() {
	Camera * cam = Camera::getMainCamera();
	if (kRenderPassLighting & _renderPasses && cam) {
		_postprocessMat->setDiffuseMap(_lightingFbo->getTexture());
		scene::Light::LightList & lightList = scene::Light::getLightSources();
		scene::Light::LightList::iterator it;
		_lightingCanvas->setClearBuffer(true);
		bool setBlend = false;
		
		_shadowRP.clear();
		scene::Light::LightList enabledLights;
		
		float near = 0.0f;
		float far = 0.0f;
		if (cam->getProjectionMethod()) {
			near = cam->getProjectionMethod()->getNear();
			far = cam->getProjectionMethod()->getFar();
		}
		for (it = lightList.begin(); it != lightList.end(); ++it) {
			if ((*it)->sceneObject() && (*it)->sceneObject()->isEnabled()) {
				enabledLights.push_back(*it);
			}
		}
		
		for (it = enabledLights.begin(); it != enabledLights.end(); ++it) {
			_lightingMaterial->setShadowMap(getShadowMap(cam, *it));
			_lightingFbo->bind();
			
			if (setBlend) {
				State::get()->disableDepthTest();
				State::get()->enableBlend(State::kBlendFunctionAddLight);
			}
			_lightingMaterial->setLight(*it);
			_lightingMaterial->setEnabledLights(static_cast<int>(enabledLights.size()));
			_lightingCanvas->draw(_viewport.width(), _viewport.height());
			_lightingCanvas->setClearBuffer(false);
			
			setBlend = true;
			_lightingFbo->unbind();
		}
		State::get()->enableDepthTest();
		State::get()->disableBlend();
		
		if (!setBlend) {
			// The scene does not contain any light or there are all disabled
			_lightingFbo->bind();
			_lightingMaterial->setLight(static_cast<scene::Light*>(nullptr));
			_lightingCanvas->draw(_viewport.width(), _viewport.height());
			_lightingCanvas->setClearBuffer(false);
			_lightingFbo->unbind();
		}
	}
	else {
		_postprocessMat->setDiffuseMap(_gbufferPass.getFbo()->getTexture(FramebufferObject::kAttachmentColor0));
	}
}
	
Texture * AdvancedDeferredRenderer::getShadowMap(scene::Camera * cam, scene::Light * light) {
	Texture * texture = TextureManager::get()->whiteTexture();
	float near = 0.5f;
	float far = 1000.0f;
	if (cam->getProjectionMethod()) {
		near = cam->getProjectionMethod()->getNear();
		far = cam->getProjectionMethod()->getFar();
	}

	ShadowRenderPassMaterial * shadowMat = dynamic_cast<ShadowRenderPassMaterial*>(_shadowRP.getMaterial());
	_shadowRP.setClearColor(Color::white());
	if (light->getCastShadows() && shadowMat && (kRenderPassShadows & _renderPasses) != 0) {
		int cascades = shadowMat->getShadowCascades();
		if (light->getType()==Light::kTypeDirectional) {
			float cascadeNear = 10.0f;
			float cascadeFar = far;
			float shadowBias = light->getShadowBias();
			if (cascades==3) {
				_shadowRP.setClearBuffers(true);
				light->setShadowBias(shadowBias* 1.1f);
				renderShadowCascade(cam, light, shadowMat, cascadeNear, cascadeFar, 100.0f, 0.5f, 200.0f);
				_shadowRP.setClearColorBuffer(false);
				cascadeFar = 20.0f;
			}
			else {
				_shadowRP.setClearBuffers(true);
				cascadeFar = far;
				_shadowRP.clear();
			}
			cascadeNear = 5.0f;
			
			
			if (cascades>=2) {
				light->setShadowBias(shadowBias* 1.5f);
				renderShadowCascade(cam, light, shadowMat, cascadeNear, cascadeFar, 40.0f, 0.5f, 80.0f);
				_shadowRP.setClearColorBuffer(false);
				cascadeFar = 7.0f;
			}
			else {
				_shadowRP.setClearBuffers(true);
				_shadowRP.clear();
				cascadeFar = far;
			}
			cascadeNear = near;
			
			light->setShadowBias(shadowBias);
			renderShadowCascade(cam, light, shadowMat, cascadeNear, cascadeFar, 17.0f, 0.5f, 80.0f);
		}
		else {
			if (light->getType()==Light::kTypeSpot) {
				float cutoff = light->getSpotCutoff();
				light->getProjectionMatrix().perspective(cutoff * 2.0f, 1.0f, 0.1f, 100.0f);
			}
			State::get()->enableDepthTest();
			State::get()->disableBlend();
			scene::Light::updateShadowMap(_sceneRoot.getPtr(), light);
			shadowMat->setLight(light);
			shadowMat->setShadowTexture(scene::Light::getShadowTexture());
			_shadowRP.setClearColorBuffer(false);
			_shadowRP.updateTexture(_sceneRoot.getPtr(),true,false,true);
		}
		
		texture = _shadowRP.getFbo()->getTexture();
	}
	if (cam->getProjectionMethod()) {
		cam->getProjectionMethod()->setNear(near);
		cam->getProjectionMethod()->setFar(far);
		cam->getProjectionMethod()->updateMatrix();
		Camera::applyTransform(cam);
	}
	return texture;
}

void AdvancedDeferredRenderer::renderShadowCascade(scene::Camera * cam, scene::Light * light, ShadowRenderPassMaterial * shadowMat,
												   float near, float far, float boxWidth, float boxNear, float boxFar)
{
	if (cam->getProjectionMethod()) {
		cam->getProjectionMethod()->setNear(near);
		cam->getProjectionMethod()->setFar(far);
		cam->getProjectionMethod()->updateMatrix();
		Camera::applyTransform(cam);
	}
	
	float hWidth = boxWidth / 2.0f;
	float bias = light->getShadowBias();
	light->setShadowBias(bias);
	light->getProjectionMatrix().ortho(-hWidth,hWidth,-hWidth,hWidth, boxNear, boxFar);
	
	State::get()->enableDepthTest();
	State::get()->disableBlend();
	scene::Light::updateShadowMap(_sceneRoot.getPtr(), light);
	shadowMat->setLight(light);
	shadowMat->setShadowTexture(scene::Light::getShadowTexture());
	_shadowRP.updateTexture(_sceneRoot.getPtr(),true,false,true);
}
	
void AdvancedDeferredRenderer::postprocessPass(scene::Camera * cam) {
	if (kRenderPassPostprocess & _renderPasses) {
		_ssaoFbo->bind();
		if (_ssaoMaterial->isEnabled()) {
			State::get()->setClearColor(vwgl::Color::white());
			State::get()->clearBuffers(State::kBufferDepth);
			_ssaoCanvas->setClearBuffer(true);
			_ssaoCanvas->draw(_viewport.width(), _viewport.height());
		}
		else {
			State::get()->setClearColor(vwgl::Color::white());
			State::get()->clearBuffers(State::kBufferDepth | State::kBufferColor);
		}
		_ssaoFbo->unbind();
		
		_bloomFbo->bind();
		State::get()->clearBuffers(State::kBufferDepth);
		_bloomCanvas->setClearBuffer(true);
		_bloomCanvas->draw(_viewport.width(), _viewport.height());
		
		_bloomFbo->unbind();
	}
	else {
		_ssaoFbo->bind();
		State::get()->setClearColor(vwgl::Color::white());
		State::get()->clearBuffers(State::kBufferDepth | State::kBufferColor);
		_ssaoFbo->unbind();
		
		_bloomFbo->bind();
		State::get()->setClearColor(vwgl::Color::white());
		State::get()->clearBuffers(State::kBufferDepth | State::kBufferColor);
		_bloomFbo->unbind();
	}
}

void AdvancedDeferredRenderer::presentCanvas(scene::Camera * cam) {
	_canvas->draw(_viewport.width(), _viewport.height());
	State::get()->clearBuffers(State::kBufferDepth);
}
	
}
}