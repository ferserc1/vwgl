
#include <vwgl/transform_strategy.hpp>
#include <iostream>

#include <JsonBox.h>

namespace vwgl {

TransformStrategy * TransformStrategy::factory(vwgl::JsonDeserializer & deserializer) {
	ptr<TransformStrategy> strategy;
	std::string type = deserializer.getString("type", "");
	if (type=="TRSTransformStrategy") {
		strategy = new TRSTransformStrategy();
	}
	else if (type=="PolarTransformStrategy") {
		strategy = new PolarTransformStrategy();
	}
	if (strategy.valid()) {
		strategy->deserialize(deserializer);
	}
	return strategy.release();
}

void TRSTransformStrategy::deserialize(JsonDeserializer & deserializer) {
	_translate = deserializer.getVector3("translate", Vector3());
	_rotateX = deserializer.getFloat("rotateX", 0.0f);
	_rotateY = deserializer.getFloat("rotateY", 0.0f);
	_rotateZ = deserializer.getFloat("rotateZ", 0.0f);
	_scale = deserializer.getVector3("scale", Vector3());
	std::string rotationOrder = deserializer.getString("rotationOrder", "kOrderXYZ");
	if (rotationOrder=="kOrderXYZ") {
		_rotationOrder = kOrderXYZ;
	}
	else if (rotationOrder=="kOrderXZY") {
		_rotationOrder = kOrderXZY;
	}
	else if (rotationOrder=="kOrderYXZ") {
		_rotationOrder = kOrderYXZ;
	}
	else if (rotationOrder=="kOrderYZX") {
		_rotationOrder = kOrderYZX;
	}
	else if (rotationOrder=="kOrderZYX") {
		_rotationOrder = kOrderZYX;
	}
	else if (rotationOrder=="kOrderZXY") {
		_rotationOrder = kOrderZXY;
	}
}
	
void PolarTransformStrategy::deserialize(JsonDeserializer & deserializer) {
	_coords = deserializer.getVector2("coords", Vector2());
	_distance = deserializer.getFloat("distance", 0.0f);
	_origin = deserializer.getVector3("origin", Vector3());
}

}