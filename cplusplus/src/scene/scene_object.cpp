
#include <vwgl/scene/scene_object.hpp>

namespace vwgl {
namespace scene {

SceneObject::SceneObject()
	:_enabled(true)
{
	
}
	
SceneObject::SceneObject(const std::string & name)
	:_name(name)
	,_enabled(true)
{
	
}

SceneObject::~SceneObject() {
	
}

void SceneObject::init() {
	eachComponent([&] (Component * comp) {
		comp->init();
	});
}

void SceneObject::resize(const Size2Di & size) {
	eachComponent([&] (Component * comp) {
		comp->resize(size);
	});
}

void SceneObject::update() {
	eachComponent([&] (Component * comp) {
		comp->update();
	});
}

void SceneObject::frame(float delta) {
	eachComponent([&] (Component * comp) {
		comp->frame(delta);
	});
}

void SceneObject::willDraw() {
	eachComponent([&] (Component * comp) {
		comp->willDraw();
	});
}

void SceneObject::draw() {
	eachComponent([&] (Component * comp) {
		comp->draw();
	});
}

void SceneObject::didDraw() {
	eachComponent([&] (Component * comp) {
		comp->didDraw();
	});
}

void SceneObject::mouseDown(const app::MouseEvent & evt) {
	eachComponent([&] (Component * comp) {
		comp->mouseDown(evt);
	});
}

void SceneObject::mouseUp(const app::MouseEvent & evt) {
	eachComponent([&] (Component * comp) {
		comp->mouseUp(evt);
	});
}

void SceneObject::mouseMove(const app::MouseEvent & evt) {
	eachComponent([&] (Component * comp) {
		comp->mouseMove(evt);
	});
}

void SceneObject::mouseDrag(const app::MouseEvent & evt) {
	eachComponent([&] (Component * comp) {
		comp->mouseDrag(evt);
	});
}

void SceneObject::keyDown(const app::KeyboardEvent & evt) {
	eachComponent([&] (Component * comp) {
		comp->keyDown(evt);
	});
}

void SceneObject::keyUp(const app::KeyboardEvent & evt) {
	eachComponent([&] (Component * comp) {
		comp->keyUp(evt);
	});
}
	
void SceneObject::mouseWheel(const app::MouseEvent & evt) {
	eachComponent([&] (Component * comp) {
		comp->mouseWheel(evt);
	});
}

void SceneObject::willDrawIcon() {
	
}
	
void SceneObject::didDrawIcon() {
	
}
	
}
}
