
#include <vwgl/scene/joint.hpp>
#include <vwgl/boundingbox.hpp>
#include <vwgl/state.hpp>

#include <JsonBox.h>

// Resolve forward declaration
#include <vwgl/drawable.hpp>

namespace vwgl {
namespace scene {

ComponentFactory<InputChainJoint> inChainJoint("InputChainJoint");
ComponentFactory<OutputChainJoint> outChainJoint("OutputChainJoint");
	
void ChainJoint::deserialize(JsonDeserializer & deserializer, const std::string &resourcePath) {
	if (deserializer.keyExists("joint")) {
		JsonLibPtr * jsonPtr = deserializer.getJsonLibPtr();
		JsonBox::Value & val = *static_cast<JsonBox::Value*>(jsonPtr);
		JsonBox::Value js_object = val["joint"].getObject();
		JsonDeserializer jointDeserializer(static_cast<JsonLibPtr*>(&js_object));
		_joint = new vwgl::physics::LinkJoint();
		_joint->deserialize(jointDeserializer);
	}
}

void InputChainJoint::init() {
}

void InputChainJoint::willDrawIcon() {
	physics::LinkJoint * joint = getJoint();
	if (_gizmoIcon.valid() && joint) {
		Vector3 offset = joint->offset();
		vwgl::State::get()->modelMatrix().mult(Matrix4::makeTranslation(-offset.x(), -offset.y(), -offset.z()));
	}
}

void InputChainJoint::didDrawIcon() {
	physics::LinkJoint * joint = getJoint();
	if (_gizmoIcon.valid() && joint) {
		Vector3 offset = joint->offset();
		vwgl::State::get()->modelMatrix().mult(Matrix4::makeTranslation(offset.x(), offset.y(), offset.z()));
	}
}
	
void OutputChainJoint::init() {
}
	
void OutputChainJoint::willDrawIcon() {
	physics::LinkJoint * joint = getJoint();
	if (_gizmoIcon.valid() && joint) {
		Vector3 offset = joint->offset();
		vwgl::State::get()->modelMatrix().mult(Matrix4::makeTranslation(offset.x(), offset.y(), offset.z()));
	}
}

void OutputChainJoint::didDrawIcon() {
	physics::LinkJoint * joint = getJoint();
	if (_gizmoIcon.valid() && joint) {
		Vector3 offset = joint->offset();
		vwgl::State::get()->modelMatrix().mult(Matrix4::makeTranslation(-offset.x(), -offset.y(), -offset.z()));
	}
}
	
}
}