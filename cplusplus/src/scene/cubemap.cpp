
#include <vwgl/scene/cubemap.hpp>
#include <vwgl/deferredmaterial.hpp>
#include <vwgl/graphics.hpp>
#include <vwgl/scene/component.hpp>
#include <vwgl/scene/drawable.hpp>

// Resolve forward declarations
#include <vwgl/drawable.hpp>

namespace vwgl {
namespace scene {

class VWGLEXPORT SimpleCubemapMaterial : public DeferredMaterial {
public:
	SimpleCubemapMaterial() { initShader(); }

	virtual void initShader();

protected:
	virtual ~SimpleCubemapMaterial() {}

	virtual void setupUniforms();

	static std::string s_vshader;
	static std::string s_fshader;
	static std::string s_vshader_150;
	static std::string s_fshader_150;
};

std::string SimpleCubemapMaterial::s_vshader = "\
#ifdef GL_ES\n\
precision highp float;\n\
#endif\n\
attribute vec3 aVertexPosition;\n\
attribute vec2 aTexturePosition;\n\
\n\
uniform mat4 uMVMatrix;\n\
uniform mat4 uPMatrix;\n\
\n\
varying vec2 vTexturePosition;\n\
varying vec4 vPosition;\n\
\n\
void main() {\n\
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	gl_Position = vPosition;\n\
	vTexturePosition = aTexturePosition;\n\
}\n\
";

std::string SimpleCubemapMaterial::s_fshader = "\
varying vec2 vTexturePosition;\n\
varying vec4 vPosition;\n\
\n\
uniform sampler2D uTexture;\n\
uniform vec4 uColorTint;\n\
uniform bool uIsEnabled;\n\
\n\
uniform vec2 uTextureOffset;\n\
uniform vec2 uTextureScale;\n\
\n\
void main(void) {\n\
	vec4 texColor = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset);\n\
	if (!uIsEnabled) {\n\
		texColor = vec4(1.0);\n\
	}\n\
	\n\
	gl_FragColor = texColor * uColorTint;\n\
}";

std::string SimpleCubemapMaterial::s_vshader_150 = "\
#version 150\n\
in vec3 aVertexPosition;\n\
in vec2 aTexturePosition;\n\
\n\
uniform mat4 uMVMatrix;\n\
uniform mat4 uPMatrix;\n\
\n\
out vec2 vTexturePosition;\n\
out vec4 vPosition;\n\
\n\
void main() {\n\
	vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	gl_Position = vPosition;\n\
	vTexturePosition = aTexturePosition;\n\
}\n\
";

std::string SimpleCubemapMaterial::s_fshader_150 = "\
#version 150\n\
in vec2 vTexturePosition;\n\
in vec4 vPosition;\n\
\n\
out vec4 out_FragColor;\n\
\n\
uniform sampler2D uTexture;\n\
uniform vec4 uColorTint;\n\
uniform bool uIsEnabled;\n\
\n\
uniform vec2 uTextureOffset;\n\
uniform vec2 uTextureScale;\n\
\n\
void main(void) {\n\
	vec4 texColor = texture(uTexture,vTexturePosition * uTextureScale + uTextureOffset);\n\
	if (!uIsEnabled) {\n\
		texColor = vec4(1.0);\n\
	}\n\
	\n\
	out_FragColor = texColor * uColorTint;\n\
}";

void SimpleCubemapMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	if (vwgl::Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->attachShader(vwgl::Shader::kTypeVertex, SimpleCubemapMaterial::s_vshader);
		getShader()->attachShader(vwgl::Shader::kTypeFragment, SimpleCubemapMaterial::s_fshader);
	}
	else if (vwgl::Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->attachShader(vwgl::Shader::kTypeVertex, SimpleCubemapMaterial::s_vshader_150);
		getShader()->attachShader(vwgl::Shader::kTypeFragment, SimpleCubemapMaterial::s_fshader_150);
		getShader()->setOutputParameterName(vwgl::Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "SimpleCubemapMaterial: could not use material with the current API" << std::endl;
	}

	getShader()->link("def_texture");


	loadVertexAttrib("aVertexPosition");
	loadTexCoord0Attrib("aTexturePosition");

	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");

	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uColorTint");
	getShader()->initUniformLocation("uIsEnabled");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
}

void SimpleCubemapMaterial::setupUniforms(){
	getShader()->setUniform("uMVMatrix", modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());

	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uTexture", mat->getTexture(), vwgl::Texture::kTexture0);
		getShader()->setUniform("uIsEnabled", mat->getTexture() != nullptr);
		getShader()->setUniform("uColorTint", mat->getDiffuse());
		getShader()->setUniform("uTextureOffset", mat->getTextureOffset());
		getShader()->setUniform("uTextureScale", mat->getTextureScale());
	}
}

Size2Di Cubemap::s_cubemapSize = Size2Di(512);
bool Cubemap::s_cubemapSizeChanged = false;
ptr<DeferredMaterial> Cubemap::s_cubemapMaterial;
DeferredMaterial * Cubemap::cubemapMaterial() {
	if (!s_cubemapMaterial.valid()) {
		s_cubemapMaterial = new SimpleCubemapMaterial();
	}
	return s_cubemapMaterial.getPtr();
}

ComponentFactory<scene::Cubemap> cubemapFactory("Cubemap");

Cubemap::Cubemap()
	:_clearColor(Color::black())
{

}

Cubemap::~Cubemap() {

}

void Cubemap::init() {
	if (sceneObject()) {
		vwgl::scene::Drawable * drw = sceneObject()->getComponent<vwgl::scene::Drawable>();
		if (drw) {
			drw->eachElement([&](vwgl::PolyList * plist, vwgl::Material * mat, vwgl::Transform & trx) {
				GenericMaterial * gm = dynamic_cast<GenericMaterial*>(mat);
				gm->setCubeMap(this->getCubeMap());
			});
		}
	}
}

void Cubemap::update() {
	Node * node = this->node();
	Node * sceneRoot = node ? node->sceneRoot():nullptr;
	if (sceneRoot) {
		Viewport vp = State::get()->getViewport();
		scene::Drawable::setForcedMaterial(scene::Cubemap::cubemapMaterial());
		scene::Drawable::setUseMaterialSettings(true);
		State::get()->pushModelMatrix();
		State::get()->pushViewMatrix();
		State::get()->pushProjectionMatrix();
		
		beginDraw();
		drawToFace(Texture::kTexturePositiveXFace);
		sceneRoot->accept(_drawVisitor);

		drawToFace(Texture::kTextureNegativeXFace);
		sceneRoot->accept(_drawVisitor);

		drawToFace(Texture::kTexturePositiveYFace);
		sceneRoot->accept(_drawVisitor);

		drawToFace(Texture::kTextureNegativeYFace);
		sceneRoot->accept(_drawVisitor);

		drawToFace(Texture::kTexturePositiveZFace);
		sceneRoot->accept(_drawVisitor);

		drawToFace(Texture::kTextureNegativeZFace);
		sceneRoot->accept(_drawVisitor);

		endDraw();
		State::get()->popProjectionMatrix();
		State::get()->popViewMatrix();
		State::get()->popModelMatrix();
		scene::Drawable::setForcedMaterial(nullptr);
		scene::Drawable::setUseMaterialSettings(false);
		State::get()->setViewport(vp);
	}
}

bool Cubemap::serialize(vwgl::JsonSerializer &serializer, bool lastItem) {
	serializer.openObject(true, true);
	serializer.writeProp<std::string>("type", "Cubemap", true, true);
	serializer.writeProp("clearColor", _clearColor, false, true);
	serializer.closeObject(true, !lastItem, true);
	return true;
}
	
void Cubemap::deserialize(JsonDeserializer & deserializer, const std::string & resourcePath) {
	_clearColor = deserializer.getVector4("clearColor", Color::black());
}

FramebufferObject * Cubemap::getFbo() {
	if (_fbo.valid() && scene::Cubemap::s_cubemapSizeChanged) {	// Resize cubemap
		_fbo->resize(scene::Cubemap::s_cubemapSize);
	}
	else if (!_fbo.valid()) {	// Create cubemap
		_fbo = new vwgl::FramebufferObject();
		Size2Di size = scene::Cubemap::s_cubemapSize;
		_fbo->create(size.width(), size.height(), FramebufferObject::kTypeCubeMap);
	}
	return _fbo.getPtr();
}

void Cubemap::beginDraw() {
	getFbo()->bind();
}

void Cubemap::drawToFace(Texture::TextureTarget target) {
	scene::Node * node = this->node();
	FramebufferObject * fbo = getFbo();
	if (node && fbo) {
		fbo->setTextureColorTarget(FramebufferObject::kAttachmentColor0, target);
	
		vwgl::State::get()->setViewport(vwgl::Viewport(0, 0, fbo->getWidth(), fbo->getHeight()));
		vwgl::State::get()->projectionMatrix().perspective(90.0f, 1, 0.01f, 100.0f);

		switch (target) {
		case Texture::kTexturePositiveXFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(1.0f, 0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f));
			break;
		case Texture::kTextureNegativeXFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(-1.0f, 0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f));
			break;
		case Texture::kTexturePositiveYFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(0.0, -10.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f));
			break;
		case Texture::kTextureNegativeYFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(0.0, 10.0f, 0.0f), Vector3(1.0f, 0.0f, -1.0f));
			break;
		case Texture::kTexturePositiveZFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(0.0, 0.0f, -10.0f), Vector3(0.0f, 1.0f, 0.0f));
			break;
		case Texture::kTextureNegativeZFace:
			vwgl::State::get()->viewMatrix().lookAt(Vector3(0.0f), Vector3(0.0, 0.0f, 10.0f), Vector3(0.0f, 1.0f, 0.0f));
			break;
		default:
			break;
		}

		State::get()->setClearColor(_clearColor);
		State::get()->clearBuffers(State::kBufferColor | State::kBufferDepth);

		_transformVisitor.getMatrix().identity();
		node->acceptReverse(_transformVisitor);
		vwgl::Vector3 pos = _transformVisitor.getMatrix().getPosition();

		vwgl::State::get()->viewMatrix()
			.translate(-pos.x(), -pos.y(), -pos.z());
		vwgl::State::get()->modelMatrix().identity();
	}
}

void Cubemap::endDraw() {
	getFbo()->unbind();
}

}
}
