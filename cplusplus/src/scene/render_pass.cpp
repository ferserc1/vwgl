
#include <vwgl/scene/render_pass.hpp>
#include <vwgl/scene/drawable.hpp>
#include <vwgl/state.hpp>

namespace vwgl {
namespace scene {

RenderPass::RenderPass()
	:_clearColor(Color::black())
	,_clearColorBuffer(true)
	,_clearDepthBuffer(true)
{
	
}
	
RenderPass::RenderPass(vwgl::Material * mat, vwgl::FramebufferObject * fbo)
	:_clearColor(Color::black())
	,_clearColorBuffer(true)
	,_clearDepthBuffer(true)
{
	setup(mat, fbo);
}

void RenderPass::clear() {
	if (_fbo.valid()) {
		_fbo->bind();
		State::get()->setViewport(Viewport(0,0,_fbo->getWidth(), _fbo->getHeight()));
		State::get()->setClearColor(_clearColor);
		State::get()->clearBuffers(State::kBufferColor | State::kBufferDepth);
		_fbo->unbind();
	}
}

void RenderPass::updateTexture(scene::Node * sceneRoot, bool alphaRender, bool renderTransparent, bool renderOpaque) {
	if (_fbo.valid() && _material.valid()) {
		_fbo->bind();
		State::get()->setViewport(Viewport(0,0,_fbo->getWidth(), _fbo->getHeight()));
		State::get()->setClearColor(_clearColor);
		if (_clearColorBuffer && _clearDepthBuffer) {
			State::get()->clearBuffers(State::kBufferColor | State::kBufferDepth);
		}
		else if (_clearColorBuffer) {
			State::get()->clearBuffers(State::kBufferColor);
		}
		else if (_clearDepthBuffer) {
			State::get()->clearBuffers(State::kBufferDepth);
		}
		Drawable::setForcedMaterial(_material.getPtr());
		Drawable::setUseMaterialSettings(true);
		draw(sceneRoot,alphaRender, renderTransparent, renderOpaque);
		Drawable::setForcedMaterial(nullptr);
		Drawable::setUseMaterialSettings(false);
		_fbo->unbind();
	}
}

void RenderPass::draw(scene::Node * root, bool alphaRender, bool renderTransparent, bool renderOpaque) {
	if (alphaRender) {
		if (renderOpaque) drawOpaque(root);
		if (renderTransparent) drawTranslucent(root);
	}
	else {
		root->accept(_drawVisitor);
	}
}

void RenderPass::drawOpaque(scene::Node * root) {
	Drawable::setRenderFlags(Drawable::kRenderOpaque);
	root->accept(_drawVisitor);
	scene::Drawable::setRenderFlags(Drawable::kRenderOpaque | Drawable::kRenderTransparent);
}

void RenderPass::drawTranslucent(scene::Node * root) {
	Drawable::setRenderFlags(Drawable::kRenderTransparent);
	State::get()->enableBlend(State::kBlendFunctionAlpha);
	root->accept(_drawVisitor);
	State::get()->disableBlend();
	scene::Drawable::setRenderFlags(Drawable::kRenderOpaque | Drawable::kRenderTransparent);
}

}
}
