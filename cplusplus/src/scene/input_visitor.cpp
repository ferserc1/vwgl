
#include <vwgl/scene/input_visitor.hpp>

namespace vwgl {
namespace scene {

void MouseUpInputVisitor::visit(scene::Node * node) {
	node->mouseUp(getEvent());
}

void MouseDownInputVisitor::visit(scene::Node * node) {
	node->mouseDown(getEvent());
}

void MouseMoveInputVisitor::visit(scene::Node * node) {
	node->mouseMove(getEvent());
}

void MouseDragInputVisitor::visit(scene::Node * node) {
	node->mouseDrag(getEvent());
}

void MouseWheelInputVisitor::visit(scene::Node * node) {
	node->mouseWheel(getEvent());
}

void KeyUpInputVisitor::visit(scene::Node * node) {
	node->keyUp(getEvent());
}
void KeyDownInputVisitor::visit(scene::Node * node) {
	node->keyDown(getEvent());
}

}
}