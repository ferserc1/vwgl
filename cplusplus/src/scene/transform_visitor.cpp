
#include <vwgl/scene/transform_visitor.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/node.hpp>

namespace vwgl {
namespace scene {

TransformVisitor::TransformVisitor() {
	_matrix.identity();
}

void TransformVisitor::visit(scene::Node * node) {
	scene::Transform * transform = node->getComponent<scene::Transform>();

	if (transform) {
		_matrix.mult(transform->getTransform().getMatrix());
	}
}

}
}