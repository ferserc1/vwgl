
#include <vwgl/scene/billboard.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/scene/camera.hpp>

namespace vwgl {
namespace scene {

ComponentFactory<Billboard>  billboardFactory("Billboard");

Billboard::Billboard()
	:_size(1.0f)
{
	_material = new vwgl::GenericMaterial();
	create();
}
	
Billboard::Billboard(Texture * texture)
	:_size(1.0f)
{
	_material = new vwgl::GenericMaterial();
	_material->setTexture(texture);
	create();
}

Billboard::Billboard(Texture * texture, const Size2D & size)
	:_size(size)
{
	_material = new vwgl::GenericMaterial();
	_material->setTexture(texture);
	create();
}

Billboard::~Billboard() {
	
}
	
void Billboard::draw() {
	if (!scene::Drawable::isDrawingEnabled()) return;
	
	if (sceneObject() && !sceneObject()->isEnabled()) return;
	
	if (_polyList->isVisible()) {
		vwgl::State::get()->pushViewMatrix();
		vwgl::State::get()->pushModelMatrix();
		
		vwgl::State::get()->viewMatrix().resetRotation();
		vwgl::State::get()->modelMatrix().resetRotation();
		
		
		_material->bindPolyList(_polyList.getPtr());
		_material->activate();
		
		_polyList->drawElements();
		
		_material->deactivate();
		
		vwgl::State::get()->popModelMatrix();
		vwgl::State::get()->popViewMatrix();
	}
}
	
bool Billboard::serialize(JsonSerializer & serializer, bool lastItem) {
	serializer.openObject(true, true);
	serializer.writeProp<std::string>("type", "Billboard", true, true);
	std::string texture = _material->getTexture() ? _material->getTexture()->getFileName():"";
	if (texture!="") {
		texture = System::get()->getLastPathComponent(texture);
	}
	serializer.writeProp("texture", texture, false, true);
	serializer.writeProp("size", _size, true, true);
	serializer.closeObject(true, !lastItem, true);
	return true;

}

bool Billboard::saveResourcesToPath(const std::string & dstPath) {
	std::string srcTexture = _material->getTexture() ? _material->getTexture()->getFileName():"";
	if (srcTexture!="") {
		std::string texture = System::get()->getLastPathComponent(srcTexture);
		texture = System::get()->addPathComponent(dstPath, texture);
		System::get()->cp(srcTexture, texture, true);
	}
	return true;
}

void Billboard::deserialize(JsonDeserializer & deserializer, const std::string & resourcePath) {
	std::string texture = deserializer.getString("texture", "");
	texture = System::get()->addPathComponent(resourcePath, texture);
	setTexture(vwgl::Loader::get()->loadTexture(texture));
	create();
}
	
void Billboard::create() {
	_polyList = new PolyList();
	float w2 = _size.width() / 2.0f;
	float h2 = _size.height() / 2.0f;
	
	float vertex [] = {
		-w2, -h2, 0.0f,
		 w2, -h2, 0.0f,
		 w2,  h2, 0.0f,
		-w2,	 h2, 0.0f
	};
	
	float normal [] = {
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f
	};
	
	float texCoord [] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};
	
	unsigned int index [] = {
		0, 1, 2,
		2, 3, 0
	};
	
	_polyList->addVertexVector(vertex, 12);
	_polyList->addNormalVector(normal, 12);
	_polyList->addTexCoord0Vector(texCoord, 8);
	_polyList->addTexCoord1Vector(texCoord, 8);
	_polyList->addIndexVector(index, 6);
	_polyList->buildPolyList();
}
	
void Billboard::clear() {
	_polyList = nullptr;
}

}
}
