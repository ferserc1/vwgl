
#include <vwgl/scene/forward_renderer.hpp>
#include <vwgl/scene/camera.hpp>
#include <vwgl/scene/projector.hpp>
#include <vwgl/drawable.hpp>

namespace vwgl {
namespace scene {

ForwardRenderer::ForwardRenderer()
{
	_projectorMat = new ProjTextureDeferredMaterial();
}
	
ForwardRenderer::ForwardRenderer(vwgl::scene::Node * sceneRoot)
	:Renderer(sceneRoot)
{
	_projectorMat = new ProjTextureDeferredMaterial();
}
	
ForwardRenderer::~ForwardRenderer() {
	
}
	
void ForwardRenderer::draw() {
	vwgl::scene::Camera * cam = scene::Camera::getMainCamera();
	if (_sceneRoot.valid() && cam) {
		State::get()->setClearColor(getClearColor());
		State::get()->clearBuffers(State::kBufferDepth | State::kBufferColor);
		scene::Camera::applyTransform(cam);
		
		// Render opaque objects. The fillOffset is used to draw the projectors in a second
		// phase
		State::get()->enableFillOffset(1.0f, 1.0f);
		renderOpaque();
		
		State::get()->disableFillOffset();
		
		// Render projectors
		Drawable::setForcedMaterial(_projectorMat.getPtr());
		Drawable::setUseMaterialSettings(true);
		
		State::get()->disableDepthMask();
		scene::Projector::ProjectorList & projectorList = scene::Projector::getProjectorList();
		scene::Projector::ProjectorList::iterator it;
		for (it=projectorList.begin(); it!=projectorList.end(); ++it) {
			if ((*it)->isVisible()) {
				State::get()->enableBlend(State::kBlendFunctionAddShadow);
				_projectorMat->setProjector(*it);
				renderOpaque();
				renderTranslucent();
			}
		}
		
		Drawable::setForcedMaterial(nullptr);
		Drawable::setUseMaterialSettings(false);
		State::get()->disableBlend();
		State::get()->enableDepthMask();
		
		renderTranslucent();
		
		State::get()->clearBuffers(State::kBufferDepth);
		// TODO: draw gizmos

		// Restore draw masks
		// Restore drawable masks
		Drawable::setRenderFlags(Drawable::kRenderOpaque | Drawable::kRenderTransparent);
		Drawable::setForcedMaterial(nullptr);
		Drawable::setUseMaterialSettings(false);
	}
	else {
		vwgl::State::get()->setClearColor(_clearColor);
		vwgl::State::get()->clearBuffers(State::kBufferColor | State::kBufferDepth);
	}
}

void ForwardRenderer::renderTranslucent() {
	Drawable::setRenderFlags(Drawable::kRenderTransparent);
	State::get()->enableBlend(State::kBlendFunctionAlpha);
	_sceneRoot->accept(drawVisitor());
 	State::get()->disableBlend();
}

void ForwardRenderer::renderOpaque() {
	Drawable::setRenderFlags(Drawable::kRenderOpaque);
	_sceneRoot->accept(drawVisitor());
}

}
}
