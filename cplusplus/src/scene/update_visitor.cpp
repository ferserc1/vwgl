
#include <vwgl/scene/update_visitor.hpp>
#include <vwgl/scene/node.hpp>

namespace vwgl {
namespace scene {


void UpdateVisitor::visit(scene::Node * node) {
	node->update();
}

}
}
