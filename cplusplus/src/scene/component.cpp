
#include <vwgl/scene/component.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/scene/node.hpp>

#include <JsonBox.h>

// Resolve forward declaration
#include <vwgl/drawable.hpp>

#include <map>

namespace vwgl {
namespace scene {

ComponentRegistry::ComponentRegistry() {}
ComponentRegistry::~ComponentRegistry() {}

ComponentRegistry * ComponentRegistry::s_singleton = nullptr;
	
Component::Component()
	:_sceneObject(nullptr)
	,_ignoreSerialize(false)
{
	
}
	
Component::~Component()
{
	
}

void Component::init() {
	
}
	
void Component::resize(const Size2Di & size) {
	
}

void Component::update() {
	
}

void Component::frame(float delta) {
	
}

void Component::willDraw() {
	
}

void Component::draw() {
	
}

void Component::didDraw() {
	
}
	
void Component::mouseDown(const app::MouseEvent & evt) {
	
}

void Component::mouseUp(const app::MouseEvent & evt) {
	
}

void Component::mouseMove(const app::MouseEvent & evt) {
	
}

void Component::mouseDrag(const app::MouseEvent & evt) {
	
}

void Component::keyDown(const app::KeyboardEvent & evt) {
	
}

void Component::keyUp(const app::KeyboardEvent & evt) {
	
}

void Component::mouseWheel(const app::MouseEvent & evt) {
	
}
	
void Component::willDrawIcon() {
	
}
	
void Component::didDrawIcon() {
	
}

Node * Component::node() {
	return dynamic_cast<Node*>(sceneObject());
}

Component * Component::factory(const std::string & jsonString, const std::string & resourcePath) {
	JsonBox::Value js_data;
	js_data.loadFromString(jsonString);
	ptr<Component> component;
	if (js_data["type"].isString()) {
		std::string type = js_data["type"].getString();
		component = ComponentRegistry::get()->instantiate(type);
		if (!component) {
			std::cerr << "Error creating component. No such factory for component " << type << std::endl << ", or the factory is not registered" << std::endl;
		}
	}
	if (component.valid()) {
		JsonDeserializer deserializer(static_cast<JsonLibPtr*>(&js_data));
		component->deserialize(deserializer, resourcePath);
	}
	return component.release();
}

}
}
