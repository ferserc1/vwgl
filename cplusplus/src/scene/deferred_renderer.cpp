
#include <vwgl/scene/deferred_renderer.hpp>
#include <vwgl/normalmapdeferredmaterial.hpp>
#include <vwgl/specular_render_pass.hpp>
#include <vwgl/wpositiondeferredmaterial.hpp>
#include <vwgl/shadow_map_material.hpp>
#include <vwgl/shadow_render_pass.hpp>
#include <vwgl/selectiondeferredmaterial.hpp>
#include <vwgl/projtexturedeferredmaterial.hpp>
#include <vwgl/ssao.hpp>
#include <vwgl/scene/projector.hpp>
#include <vwgl/scene/light.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/drawable.hpp>

namespace vwgl {
namespace scene {

DeferredRenderer::DeferredRenderer()
	:_antiAliasing(1),
	_renderPasses(kRenderPassDiffuse |
				  kRenderPassNormal |
				  kRenderPassSpecular |
				  kRenderPassPosition |
				  kRenderPassSelection |
				  kRenderPassLighting |
				  kRenderPassShadows |
				  kRenderPassPostprocess)
{
	build();
}

DeferredRenderer::DeferredRenderer(vwgl::scene::Node * sceneRoot)
	:Renderer(sceneRoot),
	_antiAliasing(1),
	_renderPasses(kRenderPassDiffuse |
				  kRenderPassNormal |
				  kRenderPassSpecular |
				  kRenderPassPosition |
				  kRenderPassSelection |
				  kRenderPassLighting |
				  kRenderPassShadows |
				  kRenderPassPostprocess)
{
	build();
}

DeferredRenderer::~DeferredRenderer() {
	destroy();
}

void DeferredRenderer::resize(const Size2Di & size) {
	Renderer::resize(size);
	if (!_canvas.valid()) {
		build();
	}
	
	_viewport = Viewport(0, 0, size.width(), size.height());
	
	
	_diffuseRP.getFbo()->resize(size.width()*_antiAliasing,size.height()*_antiAliasing);
	_translucentRP.getFbo()->resize(size.width()*_antiAliasing, size.height()*_antiAliasing);
	_normalRP.getFbo()->resize(size.width()*_antiAliasing,size.height()*_antiAliasing);
	_specularRP.getFbo()->resize(size.width()*_antiAliasing,size.height()*_antiAliasing);
	_positionRP.getFbo()->resize(size.width()*_antiAliasing,size.height()*_antiAliasing);
	_shadowRP.getFbo()->resize(size.width()*_antiAliasing,size.height()*_antiAliasing);
	_selectionRP.getFbo()->resize(size.width(), size.height());
	
	_lightingFbo->resize(size.width()*_antiAliasing,size.height()*_antiAliasing);
	
	_ssaoMaterial->setViewportSize(Vector2(static_cast<float>(size.width()),
										   static_cast<float>(size.height())));
	_ssaoFbo->resize(size.width(), size.height());
	_bloomFbo->resize(size.width(), size.height());
}

void DeferredRenderer::draw() {
	vwgl::State::get()->setClearColor(_clearColor);
	vwgl::State::get()->clearBuffers(State::kBufferColor | State::kBufferDepth);
	
	if (!_canvas.valid()) {
		build();
	}
	vwgl::scene::Camera * cam = scene::Camera::getMainCamera();
	if (cam) {
		prepareFrame(cam);
		geometryPass();
		lightingPass();
		postprocessPass(cam);
		presentCanvas(cam);

		// Restore drawable masks
		Drawable::setRenderFlags(Drawable::kRenderOpaque | Drawable::kRenderTransparent);
		Drawable::setForcedMaterial(nullptr);
		Drawable::setUseMaterialSettings(false);
	}
}

void DeferredRenderer::build() {
	Size2Di canvasSize = resizeVisitor().getSize();
	_canvas = new HomogeneousRenderCanvas();
	
	DiffuseRenderPassMaterial * diffuseMat = new DiffuseRenderPassMaterial();
	_diffuseRP.setup(diffuseMat,new FramebufferObject(canvasSize));
	GenericMaterial * translucentMat = new GenericMaterial();
	_translucentRP.setup(translucentMat, new FramebufferObject(canvasSize));
	NormalMapDeferredMaterial * normalMat = new NormalMapDeferredMaterial();
	_normalRP.setup(normalMat,new FramebufferObject(canvasSize));
	_normalRP.setClearColor(Color(0.0f, 0.0f, 0.0f, 0.0f));
	SpecularRenderPassMaterial * specularMat = new SpecularRenderPassMaterial();
	_specularRP.setup(specularMat,new FramebufferObject(canvasSize));
	_specularRP.setClearColor(Color(0.0f, 0.0f, 0.0f, 0.0f));
	WPositionDeferredMaterial * positionMat = new WPositionDeferredMaterial();
	_positionRP.setup(positionMat, new FramebufferObject(canvasSize,FramebufferObject::kTypeFloatTexture));
	ShadowRenderPassMaterial * shadowMat = new ShadowRenderPassMaterial();
	_shadowRP.setup(shadowMat, new FramebufferObject(canvasSize));
	_filters.push_back(shadowMat);

	SelectionDeferredMaterial * selectionMat = new SelectionDeferredMaterial();
	_selectionRP.setup(selectionMat, new FramebufferObject(canvasSize));
	
	_projectorMat = new ProjTextureDeferredMaterial();
	
	_lightingMaterial = new DeferredLighting();
	_lightingMaterial->setDiffuseMap(_diffuseRP.getFbo()->getTexture());
	_lightingMaterial->setNormalMap(_normalRP.getFbo()->getTexture());
	_lightingMaterial->setSpecularMap(_specularRP.getFbo()->getTexture());
	_lightingMaterial->setPositionMap(_positionRP.getFbo()->getTexture());
	_lightingMaterial->setShadowMap(_shadowRP.getFbo()->getTexture());
	_filters.push_back(_lightingMaterial.getPtr());
	
	_lightingCanvas = new HomogeneousRenderCanvas();
	_lightingCanvas->setMaterial(_lightingMaterial.getPtr());
	_lightingFbo = new FramebufferObject(canvasSize,FramebufferObject::kTypeFloatTexture);
	
	_ssaoMaterial = new SSAOMaterial();
	_ssaoMaterial->setPositionMap(_positionRP.getFbo()->getTexture());
	_ssaoMaterial->setNormalMap(_normalRP.getFbo()->getTexture());
	_ssaoMaterial->setViewportSize(Vector2(static_cast<float>(_viewport.width()),
										   static_cast<float>(_viewport.height())));
	std::string resources = System::get()->getDefaultShaderPath();
	_filters.push_back(_ssaoMaterial.getPtr());
	
	_ssaoCanvas = new HomogeneousRenderCanvas();
	_ssaoCanvas->setMaterial(_ssaoMaterial.getPtr());
	_ssaoFbo = new FramebufferObject(canvasSize);
	
	_bloomMaterial = new BloomMapMaterial();
	_bloomMaterial->setLightingMap(_lightingFbo->getTexture());
	_bloomFbo = new FramebufferObject(canvasSize);
	_bloomCanvas = new HomogeneousRenderCanvas();
	_bloomCanvas->setMaterial(_bloomMaterial.getPtr());
	_filters.push_back(_bloomMaterial.getPtr());
	
	_postprocessMat = new DeferredPostprocess();
	_postprocessMat->setDiffuseMap(_lightingFbo->getTexture());
	_postprocessMat->setTranslucentMap(_translucentRP.getFbo()->getTexture());

	_postprocessMat->setPositionMap(_positionRP.getFbo()->getTexture());
	_postprocessMat->setSelectionMap(_selectionRP.getFbo()->getTexture());
	_postprocessMat->setSSAOMap(_ssaoFbo->getTexture());
	_postprocessMat->setBloomMap(_bloomFbo->getTexture());
	
	_filters.push_back(_postprocessMat.getPtr());
	
	_canvas->setMaterial(_postprocessMat.getPtr());
}

void DeferredRenderer::destroy() {
	
}

void DeferredRenderer::prepareFrame(scene::Camera * cam) {
	_lightingMaterial->_shadowBlurIterations = getFilter<vwgl::ShadowRenderPassMaterial>()->getBlurIterations();
	_postprocessMat->setClearColor(_clearColor);
	RenderSettings::setCurrentRenderSettings(_postprocessMat->getRenderSettingsPtr());
	_postprocessMat->setSSAOBlur(_ssaoMaterial->getBlurIterations());
	_postprocessMat->setBloomAmount(_bloomMaterial->getBloomAmount());

	scene::Camera::applyTransform(cam);
}

void DeferredRenderer::geometryPass() {
	_clearColor.a(0.0);
	
	
	if (kRenderPassDiffuse & _renderPasses) {
		// Render diffuse buffer
		State::get()->enableFillOffset(1.0f, 1.0f);
		_diffuseRP.setClearColor(_clearColor);
		_diffuseRP.updateTexture(_sceneRoot.getPtr(), true, false, true);	// Opaque objects
		_translucentRP.setClearColor(Color(0.0f,0.0f,0.0f,0.0f));
		_translucentRP.updateTexture(_sceneRoot.getPtr(),true, true, false);	// Transparent objects
		State::get()->disableFillOffset();
		
		// Render shadow projectors
		_diffuseRP.setClearBuffers(false);
		scene::Projector::ProjectorList & projectorList = scene::Projector::getProjectorList();
		if (projectorList.size()>0) {
			scene::Projector::ProjectorList::iterator it;
			ptr<Material> diffuseMat = _diffuseRP.getMaterial();
			_diffuseRP.setMaterial(_projectorMat.getPtr());

			State::get()->disableDepthMask();
			for (it=projectorList.begin(); it!=projectorList.end(); ++it) {
				if ((*it)->isVisible()) {
					State::get()->enableBlend(State::kBlendFunctionAddShadow);
					_projectorMat->setProjector(*it);
					_diffuseRP.updateTexture(_sceneRoot.getPtr(), true);
				}
			}
		
			State::get()->disableBlend();
			State::get()->enableDepthMask();
			_diffuseRP.setMaterial(diffuseMat.getPtr());
		}
		
		_diffuseRP.setClearBuffers(true);
		
	}
	else {
		_diffuseRP.setClearColor(vwgl::Color::white());
		_diffuseRP.clear();
	}
	
	if (kRenderPassNormal & _renderPasses) {
		_normalRP.updateTexture(_sceneRoot.getPtr());
	}
	else {
		_normalRP.clear();
	}
	kRenderPassSpecular & _renderPasses ? _specularRP.updateTexture(_sceneRoot.getPtr(),true,false,true):_specularRP.clear();
	kRenderPassPosition & _renderPasses ? _positionRP.updateTexture(_sceneRoot.getPtr(),true,false,true):_positionRP.clear();
	kRenderPassSelection & _renderPasses ? _selectionRP.updateTexture(_sceneRoot.getPtr(),true,true,true):_selectionRP.clear();
}

void DeferredRenderer::lightingPass() {
	Camera * cam = Camera::getMainCamera();
	if (kRenderPassLighting & _renderPasses && cam) {
		_postprocessMat->setDiffuseMap(_lightingFbo->getTexture());
		scene::Light::LightList & lightList = scene::Light::getLightSources();
		scene::Light::LightList::iterator it;
		_lightingCanvas->setClearBuffer(true);
		bool setBlend = false;
		
		_shadowRP.clear();
		scene::Light::LightList enabledLights;
		
		float near = 0.0f;
		float far = 0.0f;
		if (cam->getProjectionMethod()) {
			near = cam->getProjectionMethod()->getNear();
			far = cam->getProjectionMethod()->getFar();
		}
		for (it = lightList.begin(); it != lightList.end(); ++it) {
			if ((*it)->sceneObject() && (*it)->sceneObject()->isEnabled()) {
				enabledLights.push_back(*it);
			}
		}

		for (it = enabledLights.begin(); it != enabledLights.end(); ++it) {
			ShadowRenderPassMaterial * shadowMat = dynamic_cast<ShadowRenderPassMaterial*>(_shadowRP.getMaterial());
			if ((*it)->getCastShadows() && shadowMat && (kRenderPassShadows & _renderPasses) != 0) {
				Light * light = *it;
				
				
				if (light->getType()==Light::kTypeDirectional) {
					if (cam->getProjectionMethod()) {
						cam->getProjectionMethod()->setNear(10.0f);
						cam->getProjectionMethod()->setFar(far);
						cam->getProjectionMethod()->updateMatrix();
						Camera::applyTransform(cam);
					}
				
					float zfar = 100.0f;
					float bias = light->getShadowBias();
					light->setShadowBias(bias);
					light->getProjectionMatrix().ortho(-zfar,zfar,-zfar,zfar, 0.03f, 180.0f);
				
					State::get()->enableDepthTest();
					State::get()->disableBlend();
					scene::Light::updateShadowMap(_sceneRoot.getPtr(), *it);
					shadowMat->setLight(*it);
					shadowMat->setShadowTexture(scene::Light::getShadowTexture());
					_shadowRP.setClearBuffers(true);
					_shadowRP.updateTexture(_sceneRoot.getPtr(),true,false,true);
					
					cam->getProjectionMethod()->setNear(near);
					cam->getProjectionMethod()->setFar(10.0f);
					cam->getProjectionMethod()->updateMatrix();
					Camera::applyTransform(cam);
				
					if (light->getType()==Light::kTypeDirectional) {
						float zfar = 14.0f;
						light->getProjectionMatrix().ortho(-zfar,zfar,-zfar,zfar, 0.5f, 80.0f);
					}
					light->setShadowBias(bias);
					State::get()->enableDepthTest();
					State::get()->disableBlend();
					scene::Light::updateShadowMap(_sceneRoot.getPtr(), light);
					shadowMat->setLight(light);
					shadowMat->setShadowTexture(scene::Light::getShadowTexture());
					_shadowRP.setClearColorBuffer(false);
					_shadowRP.updateTexture(_sceneRoot.getPtr(),true,false,true);
					
				}
				else {
					if (light->getType()==Light::kTypeSpot) {
						float cutoff = light->getSpotCutoff();
						light->getProjectionMatrix().perspective(cutoff, 1.0f, 0.1f, 100.0f);
					}
					State::get()->enableDepthTest();
					State::get()->disableBlend();
					scene::Light::updateShadowMap(_sceneRoot.getPtr(), light);
					shadowMat->setLight(light);
					shadowMat->setShadowTexture(scene::Light::getShadowTexture());
					_shadowRP.setClearColorBuffer(false);
					_shadowRP.updateTexture(_sceneRoot.getPtr(),true,false,true);
				}
			
				
				_lightingMaterial->setShadowMap(_shadowRP.getFbo()->getTexture());
			}
			else {
				_lightingMaterial->setShadowMap(TextureManager::get()->whiteTexture());
			}
			if (cam->getProjectionMethod()) {
				cam->getProjectionMethod()->setNear(near);
				cam->getProjectionMethod()->setFar(far);
				cam->getProjectionMethod()->updateMatrix();
				Camera::applyTransform(cam);
			}
			_lightingFbo->bind();
			
			if (setBlend) {
				State::get()->disableDepthTest();
				State::get()->enableBlend(State::kBlendFunctionAddLight);
			}
			_lightingMaterial->setLight(*it);
			_lightingMaterial->setEnabledLights(static_cast<int>(enabledLights.size()));
			_lightingCanvas->draw(_viewport.width()*_antiAliasing, _viewport.height()*_antiAliasing);
			_lightingCanvas->setClearBuffer(false);
			
			setBlend = true;
			_lightingFbo->unbind();
		}
		State::get()->enableDepthTest();
		State::get()->disableBlend();

		if (!setBlend) {
			// The scene does not contain any light or there are all disabled
			_lightingFbo->bind();
			_lightingMaterial->setLight(static_cast<scene::Light*>(nullptr));
			_lightingCanvas->draw(_viewport.width()*_antiAliasing, _viewport.height()*_antiAliasing);
			_lightingCanvas->setClearBuffer(false);
			_lightingFbo->unbind();
		}
	}
	else {
		_postprocessMat->setDiffuseMap(_diffuseRP.getFbo()->getTexture());
	}
}

void DeferredRenderer::postprocessPass(scene::Camera * cam) {
	if (kRenderPassPostprocess & _renderPasses) {
		_ssaoFbo->bind();
		if (_ssaoMaterial->isEnabled()) {
			State::get()->setClearColor(vwgl::Color::white());
			State::get()->clearBuffers(State::kBufferDepth);
			_ssaoCanvas->setClearBuffer(true);
			_ssaoCanvas->draw(_viewport.width(), _viewport.height());
		}
		else {
			State::get()->setClearColor(vwgl::Color::white());
			State::get()->clearBuffers(State::kBufferDepth | State::kBufferColor);
		}
		_ssaoFbo->unbind();
			
		_bloomFbo->bind();
			State::get()->clearBuffers(State::kBufferDepth);
			_bloomCanvas->setClearBuffer(true);
			_bloomCanvas->draw(_viewport.width(), _viewport.height());
		
		_bloomFbo->unbind();
	}
	else {
		_ssaoFbo->bind();
		State::get()->setClearColor(vwgl::Color::white());
		State::get()->clearBuffers(State::kBufferDepth | State::kBufferColor);
		_ssaoFbo->unbind();
		
		_bloomFbo->bind();
		State::get()->setClearColor(vwgl::Color::white());
		State::get()->clearBuffers(State::kBufferDepth | State::kBufferColor);
		_bloomFbo->unbind();
	}
}

void DeferredRenderer::presentCanvas(scene::Camera * cam) {
	_canvas->draw(_viewport.width(), _viewport.height());
	State::get()->clearBuffers(State::kBufferDepth);
}


}
}
