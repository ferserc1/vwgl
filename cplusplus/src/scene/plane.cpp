
#include <vwgl/scene/plane.hpp>

namespace vwgl {
namespace scene {

Drawable * Plane::getDrawable()
{
	Drawable * drawable = new Drawable();
	float x = _w / 2.0f;
	float y = _d / 2.0f;

	float v[] = {
		-x, 0.000000, -y,
		x, 0.000000, -y,
		x, 0.000000, y,
		x, 0.000000, y,
		-x, 0.000000, y,
		-x, 0.000000, -y
	};

	float n[] = {
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000,
		0.000000, 1.000000, 0.000000
	};

	float t0[] = {
		0.000000, 0.000000,
		1.000000, 0.000000,
		1.000000, 1.000000,
		1.000000, 1.000000,
		0.000000, 1.000000,
		0.000000, 0.000000
	};

	float t1[] = {
		0.000000, 0.000000,
		1.000000, 0.000000,
		1.000000, 1.000000,
		1.000000, 1.000000,
		0.000000, 1.000000,
		0.000000, 0.000000
	};

	unsigned int i[] = {
		2, 1, 0, 5, 4, 3
	};

	int iSize = 3 * 2;	// 2 triangles per face, 3 vertex per triangle
	int vSize = iSize * 3;	// 3 elements (x.y.z) for each index
	int tSize = iSize * 2;	// 2 elements (u, v) for each index

	PolyList * plist = new PolyList();
	plist->addVertexVector(v, vSize);
	plist->addNormalVector(n, vSize);
	plist->addTexCoord0Vector(t0, tSize);
	plist->addTexCoord1Vector(t1, tSize);
	plist->addIndexVector(i, iSize);

	plist->buildPolyList();

	drawable->addPolyList(plist);
	return drawable;
}

}
}
