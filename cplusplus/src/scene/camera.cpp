
#include <vwgl/scene/camera.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/state.hpp>

#include <JsonBox.h>

namespace vwgl {
namespace scene {

ComponentFactory<scene::Camera>  cameraFactory("Camera");

Camera::CameraList Camera::s_cameraList;
Camera * Camera::s_mainCamera = nullptr;
Camera::CameraList & Camera::getCameraList() {
	return s_cameraList;
}

Camera * Camera::getMainCamera() {
	return s_mainCamera;
}

void Camera::setMainCamera(Camera * cam) {
	s_mainCamera = cam;
}

void Camera::applyTransform(Camera * cam) {
	if (cam) {
		vwgl::State::get()->projectionMatrix() = cam->getProjectionMatrix();
		vwgl::State::get()->viewMatrix() = cam->getViewMatrix();
	}
}

void Camera::registerCamera(Camera * cam) {
	s_cameraList.push_back(cam);
	if (s_mainCamera == nullptr) {
		s_mainCamera = cam;
	}
}

void Camera::unregisterCamera(Camera * cam) {
	CameraList::iterator it = std::find(s_cameraList.begin(), s_cameraList.end(), cam);
	if (it!=s_cameraList.end()) {
		s_cameraList.erase(it);
		if (cam == s_mainCamera) {
			s_mainCamera = s_cameraList.size()>0 ? s_cameraList.front():nullptr;
		}
	}
}
	

Camera::Camera()
{
	_projection.identity();
	_identityMatrix.identity();
	vwgl::ProjectionFactoryMethod * projMethod = new OpticalProjectionMethod();
	setProjectionMethod(projMethod);
	Camera::registerCamera(this);
}

Camera::~Camera() {
	Camera::unregisterCamera(this);
}

Matrix4 & Camera::getViewMatrix() {
	getTransform();		// This will place the current transform into _transformVisitor
	_transformVisitor.getMatrix().invert();	// Invert the camera's model matrix to get the view matrix from this camera
	return _transformVisitor.getMatrix();
}

Matrix4 & Camera::getTransform() {
	_transformVisitor.getMatrix().identity();
	Node * sceneNode = node();
	if (sceneNode) {
		sceneNode->acceptReverse(_transformVisitor);
	}
	return _transformVisitor.getMatrix();
}

const vwgl::Vector3 & Camera::getDirectionVector() {
	_direction.set(0.0f, 0.0f, -1.0f);
	Matrix4 transform = getTransform();
	transform.setRow(3, vwgl::Vector4(0.0f, 0.0f, 0.0f, 1.0f));
	_direction = transform.multVector(_direction).xyz();
	_direction.normalize();
	return _direction;
}

void Camera::resize(const Size2Di & size) {
	if (_projectionMethod.valid() && Camera::getMainCamera()==this) {
		_projectionMethod->setViewport(vwgl::Viewport(0,0,size.width(),size.height()));
		_projectionMethod->updateMatrix();
	}
}
	
bool Camera::serialize(vwgl::JsonSerializer &serializer, bool lastItem) {
	serializer.openObject(true, true);
	serializer.writeProp<std::string>("type", "Camera", true, true);
	if (_projectionMethod.valid()) {
		serializer.writeKey("projectionMethod");
		_projectionMethod->serialize(serializer);
	}
	else {
		serializer.writeProp("projection", _projection, false, true);
	}
	
	serializer.closeObject(true, !lastItem, true);
	return true;
}
	
void Camera::deserialize(JsonDeserializer & deserializer, const std::string & resourcePath) {
	if (deserializer.keyExists("projectionMethod")) {
		JsonLibPtr * jsonPtr = deserializer.getJsonLibPtr();
		JsonBox::Value & val = *static_cast<JsonBox::Value*>(jsonPtr);
		JsonBox::Value js_projMethod = val["projectionMethod"].getObject();
		JsonDeserializer projectionDeserializer(static_cast<JsonLibPtr*>(&js_projMethod));
		setProjectionMethod(dynamic_cast<ProjectionFactoryMethod*>(vwgl::MatrixFactory::factory(projectionDeserializer)));
	}
	else if (deserializer.keyExists("projection")) {
		_projection = deserializer.getMatrix4("projection", Matrix4::makePerspective(50.0f, 1.0f, 0.1f, 1000.0f));
	}
}
	
}
}
