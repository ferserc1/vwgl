
#include <vwgl/scene/frame_visitor.hpp>

namespace vwgl {
namespace scene {

FrameVisitor::FrameVisitor()
	:_delta(0.0f)
{
	
}

void FrameVisitor::visit(scene::Node *node) {
	node->frame(_delta);
}

}
}