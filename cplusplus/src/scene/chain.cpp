
#include <vwgl/scene/chain.hpp>
#include <vwgl/scene/node.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/joint.hpp>
#include <vwgl/scene/drawable.hpp>

// Needed to resolve a forward declaration
#include <vwgl/drawable.hpp>

namespace vwgl {
namespace scene {

ComponentFactory<Chain> chainFactory("Chain");

Chain::Chain()
{

}

Chain::~Chain() {

}

void Chain::willDraw() {
	vwgl::scene::Node * node = this->node();
	if (node) {
		vwgl::Matrix4 matrix;
		matrix.identity();
		node->iterateChildren([&](vwgl::scene::Node * child) {
			vwgl::scene::Transform * trx = child->getComponent<vwgl::scene::Transform>();
			vwgl::scene::InputChainJoint * inJoint = child->getComponent<vwgl::scene::InputChainJoint>();
			vwgl::scene::OutputChainJoint * outJoint = child->getComponent<vwgl::scene::OutputChainJoint>();

			if (inJoint) {
				inJoint->getJoint()->applyTransform(matrix);
				if (trx) {
					trx->getTransform().setMatrix(matrix);
				}
			}
			else {
				matrix = trx->getTransform().getMatrix();
			}
			if (outJoint) {
				outJoint->getJoint()->applyTransform(matrix);
			}
		});
	}
}
	
bool Chain::serialize(vwgl::JsonSerializer &serializer, bool lastItem) {
	serializer.openObject(true, true);
	serializer.writeProp<std::string>("type", "Chain", false, true);
	serializer.closeObject(true, !lastItem, true);
	return true;
}
	
void Chain::deserialize(JsonDeserializer & deserializer, const std::string & resourcePath) {
	// Nothing to deserialize
}

}
}