
#include <vwgl/scene/text.hpp>
#include <vwgl/transform.hpp>
#include <vwgl/glyph_surface.hpp>
#include <vwgl/loader.hpp>

#include <vwgl/drawable.hpp>

namespace vwgl {
namespace scene {


ComponentFactory<scene::Text> textFactory("Text");
	
const Size2D & Text::getSize() {
	if (_dirtySize) {
		_size.set(0.0f);
		if (_font.valid() && !_text.empty()) {
			std::string::iterator it;
			vwgl::Transform trx;
			int numLines = 0;
			float maxX = 0;
			float currentLine = 0;
			for (it=_text.begin(); it!=_text.end(); ++it) {
				if ((*it)==' ') {
					currentLine += _font->getSpaceSize() * GlyphSurface::scale();
				}
				else if ((*it)=='\t') {
					currentLine += _font->getTabSize() * GlyphSurface::scale();
				}
				else if ((*it)=='\n' || (*it)=='\r') {
					if (maxX<currentLine) {
						maxX = currentLine;
					}
					currentLine = 0.0f;
					++numLines;
				}
				else {
					vwgl::GlyphSurface * glyph = _font->get(*it);
					if (glyph) {
						currentLine += glyph->getGlyph().getAdvance() * GlyphSurface::scale();
					}
				}
			}
			if (maxX==0) maxX = currentLine;
			_size.set(maxX, static_cast<float>(numLines) * _font->getLineHeight() * GlyphSurface::scale());
		}
		_dirtySize = false;
	}
	return _size;
}

void Text::draw() {
	if (!_font.valid()) return;
	if (_text.empty()) return;
	std::string::iterator it;
	vwgl::Transform trx;
	int numLines = 0;
	
	Material * forcedMaterial = Drawable::getForcedMaterial();
	bool useMaterialSettings = Drawable::getUseMaterialSettings();
	
	for (it=_text.begin(); it!=_text.end(); ++it) {
		if ((*it)==' ') {
			trx.getMatrix().translate(_font->getSpaceSize() * GlyphSurface::scale(), 0.0f, 0.0f);
		}
		else if ((*it)=='\t') {
			trx.getMatrix().translate(_font->getTabSize() * GlyphSurface::scale(), 0.0f, 0.0f);
		}
		else if ((*it)=='\n' || (*it)=='\r') {
			++numLines;
			trx.getMatrix().identity().translate(0.0f, static_cast<float>(numLines) * -_font->getLineHeight() * GlyphSurface::scale(), 0.0f);
		}
		else {
			vwgl::GlyphSurface * glyph = _font->get(*it);
			if (glyph) {
				vwgl::State::get()->pushModelMatrix();
				vwgl::State::get()->modelMatrix().mult(trx.getMatrix());
				
				Material * currentMaterial = forcedMaterial ? forcedMaterial : glyph->getMaterial();
				bool isTransparent = currentMaterial->isTransparent();
				if (forcedMaterial && useMaterialSettings) {
					forcedMaterial->useSettingsOf(glyph->getMaterial());
					isTransparent = glyph->getMaterial()->isTransparent();
				}
				
				if ((scene::Drawable::isRenderFlagEnabled(scene::Drawable::kRenderTransparent) && isTransparent) ||
					(scene::Drawable::isRenderFlagEnabled(scene::Drawable::kRenderOpaque) && !isTransparent)) {
					currentMaterial->bindPolyList(glyph->getPolyList());
					currentMaterial->activate();
					
					glyph->getPolyList()->drawElements();
					currentMaterial->deactivate();
				}
				
				if (forcedMaterial) {
					forcedMaterial->useSettingsOf(nullptr);
				}
				vwgl::State::get()->popModelMatrix();
				trx.getMatrix().translate(glyph->getGlyph().getAdvance() * GlyphSurface::scale(), 0.0f, 0.0f);
			}
		}
	}
}

bool Text::serialize(vwgl::JsonSerializer &serializer, bool lastItem) {
	if (_font.valid() && !_font->getPath().empty()) {
		serializer.openObject(true, true);
		serializer.writeProp<std::string>("type", "Text", true, true);

		serializer.writeProp<std::string>("path", System::get()->getLastPathComponent(_font->getPath()), true, true);
		serializer.writeProp<int>("size", _font->getSize(), true, true);
		serializer.writeProp<int>("resolution", _font->getResolution(), true, true);
		serializer.writeProp<std::string>("text", getText(),true,true);
		
		serializer.writeProp("color", _font->getMaterial()->getDiffuse(), true, true);
		serializer.writeProp("specular", _font->getMaterial()->getSpecular(), true, true);
		serializer.writeProp<float>("shininess", _font->getMaterial()->getShininess(),true, true);
		serializer.writeProp<float>("lightEmission", _font->getMaterial()->getLightEmission(),true, true);
		serializer.writeProp<bool>("castShadows", _font->getMaterial()->getCastShadows(), true, true);
		serializer.writeProp<bool>("receiveShadows", _font->getMaterial()->getReceiveShadows(), true, true);

		serializer.closeObject(true, !lastItem, true);
	}
	return true;
}

bool Text::saveResourcesToPath(const std::string & dstPath) {
	if (_font.valid() && !_font->getPath().empty()) {
		std::string srcPath = _font->getPath();
		std::string fontFile = System::get()->getLastPathComponent(_font->getPath());
		std::string dstFile = System::get()->addPathComponent(dstPath, fontFile);
		if (System::get()->fileExists(srcPath) && !System::get()->fileExists(dstFile)) {
			System::get()->cp(srcPath,dstFile,false);
		}
	}
	return true;
}

void Text::deserialize(JsonDeserializer & deserializer, const std::string & resourcePath) {
	std::string fontFile = deserializer.getString("path","");
	std::string fontPath = System::get()->addPathComponent(resourcePath, fontFile);
	int size = deserializer.getInt("size",50);
	int resolution = deserializer.getInt("resolution",100);
	setFont(Loader::get()->loadFont(fontPath, size, resolution));
	setText(deserializer.getString("text", ""));

	if (_font.valid()) {
		_font->setColor(deserializer.getVector4("color", Color::white()));
		_font->setSpecular(deserializer.getVector4("specular", Color::white()));
		_font->setLighEmission(deserializer.getFloat("lightEmission", 0.0f));
		_font->setShininess(deserializer.getFloat("shininess", 0.0f));
		_font->setCastShadows(deserializer.getBool("castShadows", true));
		_font->setReceiveShadows(deserializer.getBool("receiveShadows", true));
	}
}

}
}