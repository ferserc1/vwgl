
#include <vwgl/scene/drawable.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/state.hpp>

// Needed to include GenericMaterial's dependencies
#include <vwgl/drawable.hpp>

#include <vwgl/loader.hpp>
#include <vwgl/system.hpp>

#include <algorithm>

namespace vwgl {
namespace scene {

unsigned int Drawable::s_renderFlags = Drawable::kRenderTransparent | Drawable::kRenderOpaque;
Material * Drawable::s_fordedMaterial = nullptr;
bool Drawable::s_useMaterialSettings = false;
bool Drawable::s_preventDraw = false;

ComponentFactory<scene::Drawable> drawableFactory("Drawable");

Drawable::Drawable()
{
}

Drawable::~Drawable() {	
}

int Drawable::addPolyList(vwgl::PolyList * plist, vwgl::Material * mat, const vwgl::Transform & trx) {
	int returnValue = -1;
	if (plist == nullptr) {
		returnValue = -1;
	}
	if ((returnValue = getPolyListIndex(plist)) == -1) {
		_polyList.push_back(plist);
		_material.push_back(mat ? mat:new GenericMaterial());
		_transform.push_back(trx);
		returnValue = static_cast<int>(_polyList.size()) - 1;
		setDirty();
	}
	return returnValue;
}

void Drawable::removePolyList(PolyList * plist) {
	int index = getPolyListIndex(plist);
	if (index != -1) {
		removePolyList(index);
	}
	else {
		throw DrawableElementNotFoundException();
	}
}

void Drawable::removePolyList(int index) {
	if (index < _polyList.size() && index>=0) {
		_polyList.erase(_polyList.begin() + index);
		_material.erase(_material.begin() + index);
		_transform.erase(_transform.begin() + index);
		setDirty();
	}
	else {
		throw DrawableIndexOutOfBoundsException();
	}
}

int Drawable::getPolyListIndex(PolyList * plist) {
	PolyListVector::iterator pos = std::find(_polyList.begin(), _polyList.end(), plist);
	return pos == _polyList.end() ? -1 : static_cast<int>(pos - _polyList.begin());
}

void Drawable::setPolyList(int index, PolyList * plist) {
	if (plist == nullptr) {
		throw DrawableInvalidParameterException();
	}
	if (index >= 0 && index < _polyList.size()) {
		_polyList[index] = plist;
		setDirty();
	}
	else {
		throw DrawableIndexOutOfBoundsException();
	}
}

void Drawable::setMaterial(int index, Material * mat) {
	if (mat == nullptr) {
		throw DrawableInvalidParameterException();
	}
	if (index >= 0 && index < _material.size()) {
		_material[index] = mat;
	}
	else {
		throw DrawableIndexOutOfBoundsException();
	}
}

void Drawable::setTransform(int index, vwgl::Transform & trx) {
	if (index >= 0 && index < _transform.size()) {
		_transform[index] = trx;
	}
	else {
		throw DrawableIndexOutOfBoundsException();
	}
}

PolyList * Drawable::getPolyList(int index) {
	if (index >= 0 && index < _polyList.size()) {
		return _polyList[index].getPtr();
	}
	throw DrawableIndexOutOfBoundsException();
}

Material * Drawable::getMaterial(PolyList * plist) {
	auto index = getPolyListIndex(plist);
	if (index >= 0 && index<_material.size()) {
		return _material[index].getPtr();
	}
	else {
		throw DrawableElementNotFoundException();
	}
}

Material * Drawable::getMaterial(int index) {
	if (index >= 0 && index < _material.size()) {
		return _material[index].getPtr();
	}
	else {
		throw DrawableIndexOutOfBoundsException();
	}
}

vwgl::Transform & Drawable::getTransform(vwgl::PolyList * plist) {
	auto index = getPolyListIndex(plist);
	if (index >= 0 && index < _transform.size()) {
		return _transform[index];
	}
	else {
		throw DrawableElementNotFoundException();
	}
}

vwgl::Transform & Drawable::getTransform(int index) {
	if (index >= 0 && index < _transform.size()) {
		return _transform[index];
	}
	else {
		throw DrawableIndexOutOfBoundsException();
	}
}

GenericMaterial * Drawable::getGenericMaterial(int index) {
	return dynamic_cast<vwgl::GenericMaterial*>(getMaterial(index));
}
	
GenericMaterial * Drawable::getGenericMaterial(PolyList * plist) {
	return dynamic_cast<vwgl::GenericMaterial*>(getMaterial(plist));
}
	
void Drawable::registerObserver(IDrawableObserver * observer) {
	DrawableObserverVector::iterator it = std::find(_observerVector.begin(), _observerVector.end(), observer);
	if (it == _observerVector.end()) {
		_observerVector.push_back(observer);
	}
}

void Drawable::unregisterObserver(IDrawableObserver * observer) {
	DrawableObserverVector::iterator it = std::find(_observerVector.begin(), _observerVector.end(), observer);
	if (it != _observerVector.end()) {
		_observerVector.erase(it);
	}
}

void Drawable::draw() {
	if (s_preventDraw) return;	// if drawing is disabled, return

	Material * forcedMaterial = Drawable::getForcedMaterial();
	bool useMaterialSettings = Drawable::getUseMaterialSettings();

	if (sceneObject() && !sceneObject()->isEnabled()) return;

	eachElement([&](vwgl::PolyList * polyList, vwgl::Material * plistMaterial, vwgl::Transform & transform) {
		if (polyList->isVisible()) {
			vwgl::State::get()->pushModelMatrix();
			vwgl::State::get()->modelMatrix().mult(transform.getMatrix());

			Material * currentMaterial = forcedMaterial ? forcedMaterial : plistMaterial;
			bool isTransparent = currentMaterial->isTransparent();
			if (forcedMaterial && useMaterialSettings) {
				forcedMaterial->useSettingsOf(plistMaterial);
				isTransparent = plistMaterial->isTransparent();
			}

			if ((isRenderFlagEnabled(kRenderTransparent) && isTransparent) ||
				(isRenderFlagEnabled(kRenderOpaque) && !isTransparent)) {
				currentMaterial->bindPolyList(polyList);
				currentMaterial->activate();

				polyList->drawElements();
				currentMaterial->deactivate();
			}

			if (forcedMaterial) {
				forcedMaterial->useSettingsOf(nullptr);
			}
			vwgl::State::get()->popModelMatrix();
		}
	});
}

void Drawable::switchUVs(PolyList::VertexBufferType uv1, PolyList::VertexBufferType uv2) {
	eachElement([&](vwgl::PolyList * polyList, vwgl::Material * material, vwgl::Transform & trx) {
		polyList->switchUVs(uv1, uv2);
	});
}

void Drawable::setCubemap(vwgl::CubeMap * cm) {
	eachElement([&](vwgl::PolyList * polyList, vwgl::Material * material, vwgl::Transform & trx) {
		GenericMaterial * mat = dynamic_cast<GenericMaterial*>(material);
		if (mat) {
			mat->setCubeMap(cm);
		}
	});
}

void Drawable::applyTransform(const vwgl::Matrix4 & transform) {
	eachElement([&](vwgl::PolyList * polyList, vwgl::Material * material, vwgl::Transform & trx) {
		polyList->applyTransform(transform);
	});

	eachObserver([&](IDrawableObserver* observer) {
		observer->applyMatrix(transform);
	});
}

void Drawable::flipFaces() {
	eachElement([&](vwgl::PolyList * polyList, vwgl::Material * material, vwgl::Transform & trx) {
		polyList->flipFaces();
	});
}

void Drawable::flipNormals() {
	eachElement([&](vwgl::PolyList * polyList, vwgl::Material * material, vwgl::Transform & trx) {
		polyList->flipNormals();
	});
}

void Drawable::hideGroup(const std::string & groupName) {
	eachElement([&](PolyList * plist, Material * mat, vwgl::Transform & trx) {
		if (plist->getGroupName() == groupName) {
			plist->hide();
		}
	});
	setDirty();
}

void Drawable::showGroup(const std::string & groupName) {
	eachElement([&](PolyList * plist, Material * mat, vwgl::Transform & trx) {
		if (plist->getGroupName() == groupName) {
			plist->show();
		}
	});
	setDirty();
}

void Drawable::showByName(const std::string & name) {
	eachElement([&](PolyList * plist, Material * mat, vwgl::Transform & trx) {
		if (plist->getName()==name) {
			plist->show();
		}
	});
	setDirty();
}

void Drawable::hideByName(const std::string & name) {
	eachElement([&](PolyList * plist, Material * mat, vwgl::Transform & trx) {
		if (plist->getName() == name) {
			plist->hide();
		}
	});
	setDirty();
}

bool Drawable::serialize(vwgl::JsonSerializer &serializer, bool lastItem) {
	if (_name=="") {
		_name = System::get()->getUuid();
	}
	serializer.openObject(true, true);
	serializer.writeProp<std::string>("type", "Drawable", true, true);
	serializer.writeProp<std::string>("name", _name, false, true);
	serializer.closeObject(true, !lastItem, true);
	return true;
}
	
bool Drawable::saveResourcesToPath(const std::string & dstPath) {
	std::string filePath = System::get()->addPathComponent(dstPath, getName());
	filePath = System::get()->addExtension(filePath, "vwglb");
	vwgl::Loader::get()->writeModel(filePath,this);
	return true;
}

void Drawable::deserialize(JsonDeserializer & deserializer, const std::string & resourcePath) {
	_name = deserializer.getString("name", "");
	std::string filePath = _name;
	ptr<vwgl::scene::Drawable> drawable;
	if (filePath!="") {
		filePath = System::get()->addPathComponent(resourcePath, filePath);
		filePath = System::get()->addExtension(filePath, "vwglb");
		drawable = vwgl::Loader::get()->loadModel(filePath);
	}
	
	if (drawable.getPtr()) {
		int plistCount = static_cast<int>(drawable->getPolyListVector().size());
		for (int i=0; i<plistCount; ++i) {
			PolyList * plist = drawable->getPolyList(i);
			Material * mat = drawable->getMaterial(i);
			vwgl::Transform trx = drawable->getTransform(i);
			addPolyList(plist, mat, trx);
		}
	}
}

}
}
