
#include <vwgl/scene/draw_visitor.hpp>

namespace vwgl {
namespace scene {

DrawVisitor::DrawVisitor()
{
	
}

void DrawVisitor::visit(scene::Node * node) {
	node->willDraw();
	node->draw();
}

void DrawVisitor::didVisit(scene::Node * node) {
	node->didDraw();
}


}
}