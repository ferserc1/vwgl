
#include <vwgl/scene/primitive_factory.hpp>
#include <vwgl/scene/cube.hpp>
#include <vwgl/scene/plane.hpp>
#include <vwgl/scene/sphere.hpp>

namespace vwgl {
namespace scene {
		
Drawable * PrimitiveFactory::cube() {
	Cube cube;
	return cube.getDrawable();
}

Drawable * PrimitiveFactory::cube(float side) {
	Cube cube(side);
	return cube.getDrawable();
}

Drawable * PrimitiveFactory::cube(float w, float h, float d) {
	Cube cube(w,h,d);
	return cube.getDrawable();
}

Drawable * PrimitiveFactory::plane() {
	Plane plane;
	return plane.getDrawable();
}

Drawable * PrimitiveFactory::plane(float side) {
	Plane plane(side);
	return plane.getDrawable();
}

Drawable * PrimitiveFactory::plane(float w, float d) {
	Plane plane(w,d);
	return plane.getDrawable();
}

Drawable * PrimitiveFactory::sphere() {
	Sphere sphere;
	return sphere.getDrawable();
}

Drawable * PrimitiveFactory::sphere(float radius) {
	Sphere sphere(radius);
	return sphere.getDrawable();
}

Drawable * PrimitiveFactory::sphere(float radius, int divisions) {
	Sphere sphere(radius,divisions);
	return sphere.getDrawable();
}

Drawable * PrimitiveFactory::sphere(float radius, int slices, int stacks) {
	Sphere sphere(radius,slices,stacks);
	return sphere.getDrawable();
}


}
}