
#include <vwgl/matrix_factory.hpp>
#include <iostream>

namespace vwgl {

MatrixFactory * MatrixFactory::factory(vwgl::JsonDeserializer &deserializer) {
	ptr<MatrixFactory> matrixFactory;
	std::string type = deserializer.getString("type", "");
	if (type=="PerspectiveProjectionMethod") {
		matrixFactory = new PerspectiveProjectionMethod();
	}
	else if (type=="OpticalProjectionMethod") {
		matrixFactory = new OpticalProjectionMethod();
	}
	if (matrixFactory.valid()) {
		matrixFactory->deserialize(deserializer);
	}
	return matrixFactory.release();
}

void PerspectiveProjectionMethod::deserialize(JsonDeserializer & deserializer) {
	_near = deserializer.getFloat("near", 0.1f);
	_far = deserializer.getFloat("far", 1000.0f);
	_fov = deserializer.getFloat("fov", 50.0f);
}

void OpticalProjectionMethod::deserialize(JsonDeserializer & deserializer) {
	_near = deserializer.getFloat("near", 0.1f);
	_far = deserializer.getFloat("far", 1000.0f);
	_focalLength = deserializer.getFloat("focalLength", 50.0f);
	_frameSize = deserializer.getFloat("frameSize", 35.0f);
}



}