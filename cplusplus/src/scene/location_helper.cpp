
#include <vwgl/scene/location_helper.hpp>

#include <vwgl/scene/node.hpp>
#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/camera.hpp>

namespace vwgl {
namespace scene {


Vector3 & LocationHelper::getPosition() {
	scene::Node * node = this->node();
	if (node) {
		_transformVisitor.getMatrix().identity();
		node->acceptReverse(_transformVisitor);
		_position = _transformVisitor.getMatrix().getPosition();
	}
	return _position;
}

Vector3 & LocationHelper::getCameraDirection() {
	scene::Node * node = this->node();
	if (node) {
		Vector3 look(0.0f, 0.0f, 1.0f);
		scene::Camera * cam = scene::Camera::getMainCamera();
		_transformVisitor.getMatrix().identity();
		node->acceptReverse(_transformVisitor);
		_transformVisitor.getMatrix().setRow(3, Vector4(0.0f, 0.0f, 0.0f, 1.0f));
		if (cam) {
			Matrix4 vm = cam->getViewMatrix();
			vm.setRow(3, Vector4(0.0f, 0.0f, 0.0f, 1.0f));
			vm.mult(_transformVisitor.getMatrix());
			_direction = vm.multVector(look).xyz();
		}
		else {
			_direction = _transformVisitor.getMatrix().multVector(look).xyz();
		}
		_direction.normalize();
	}
	return _direction;
}

Matrix4 & LocationHelper::getTransformMatrix() {
	Node * sceneNode = node();
	scene::Transform * transform = sceneNode ? sceneNode->getComponent<scene::Transform>():nullptr;
	if (transform) {
		_transformMatrix = transform->getTransform().getMatrix();
	}
	else {
		_transformMatrix.identity();
	}
	return _transformMatrix;
}

Matrix4 & LocationHelper::getAbsoluteTransformMatrix() {
	_transformVisitor.getMatrix().identity();
	_transformMatrix.identity();
	Node * sceneNode = node();
	if (sceneNode) {
		sceneNode->acceptReverse(_transformVisitor);
		_transformMatrix = _transformVisitor.getMatrix();
	}
	return _transformMatrix;
}
	
scene::Node * LocationHelper::node() {
	return (_component && _component->node()) ? _component->node():nullptr;
}

}
}