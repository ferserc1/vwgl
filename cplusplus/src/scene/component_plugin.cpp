
#include <vwgl/scene/component_plugin.hpp>

namespace vwgl {
namespace scene {
	
ComponentPlugin::ComponentPlugin() {
	
}
	
ComponentPlugin::~ComponentPlugin() {
	
}
	
ComponentPlugin * ComponentPlugin::loadFromLibrary(DynamicLibrary * library) {
	ComponentPlugin * componentPlugin = nullptr;
	
	if (library) {
		VWGL_SCENE_GET_COMPONENT_PLUGIN_T getComponentPlugin = reinterpret_cast<VWGL_SCENE_GET_COMPONENT_PLUGIN_T>(library->getProcAddress("getComponentPlugin"));
		if (getComponentPlugin) {
			componentPlugin = getComponentPlugin();
		}
		
	}
	
	return componentPlugin;
}
	
}
}