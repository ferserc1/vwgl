
#include <vwgl/scene/node.hpp>

namespace vwgl {
namespace scene {


Node::Node()
	:_parent(nullptr)
{
	
}

Node::Node(const std::string & name)
	:SceneObject(name)
	,_parent(nullptr)
{
	
}

Node::~Node()
{
	
}
	
bool Node::addChild(scene::Node * child)  {
	if (child && !isAncientOf(child) && child!=this) {
		ptr<scene::Node> node = child;
		if (node->_parent) {
			node->_parent->removeChild(node.getPtr());
		}
		_children.push_back(node.getPtr());
		node->_parent = this;
		return true;
	}
	return false;
}

bool Node::removeChild(scene::Node * child) {
	if (child) {
		NodeVector::iterator it = std::find(_children.begin(), _children.end(), child);
		if (it!=_children.end()) {
			(*it)->_parent = nullptr;
			_children.erase(it);
			return true;
		}
	}
	return false;
}
	
Node * Node::nextChildOf(Node * node) {
	NodeVector::iterator it = std::find(_children.begin(), _children.end(), node);
	NodeVector::iterator nextIt = it + 1;
	if (it!=_children.end() && nextIt!=_children.end()) {
		return (*nextIt).getPtr();
	}
	return nullptr;
}

Node * Node::prevChildOf(Node * node) {
	NodeVector::iterator it = std::find(_children.begin(), _children.end(), node);
	NodeVector::iterator prevIt = it - 1;
	if (it!=_children.begin()) {
		return (*prevIt).getPtr();
	}
	return nullptr;
}

bool Node::swapChildren(Node * childA, Node * childB) {
	NodeVector::iterator iterA = std::find(_children.begin(), _children.end(), childA);
	NodeVector::iterator iterB = std::find(_children.begin(), _children.end(), childB);
	
	if (iterA!=_children.end() && iterB!=_children.end() && iterA!=iterB) {
		std::iter_swap(iterA, iterB);
		return true;
	}
	
	return false;
}

bool Node::moveChildNextTo(Node * nextToNode, Node * movingNode) {
	NodeVector::iterator nextToIterator = std::find(_children.begin(), _children.end(), nextToNode);
	NodeVector::iterator movingIterator = std::find(_children.begin(), _children.end(), movingNode);
	nextToIterator++;
	
	if (nextToIterator!=_children.end() && movingIterator!=_children.end() && nextToIterator!=movingIterator) {
		ptr<Node> movingNodePtr = movingNode;
		_children.erase(movingIterator);
		
		_children.insert(nextToIterator, movingNodePtr.getPtr());
		
		return true;
	}
	return false;
}

Node * Node::sceneRoot() {
	if (_parent) {
		return _parent->sceneRoot();
	}
	return this;
}

}
}