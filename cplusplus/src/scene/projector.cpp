
#include <vwgl/scene/projector.hpp>

#include <vwgl/system.hpp>
#include <vwgl/loader.hpp>

namespace vwgl {
namespace scene {

ComponentFactory<Projector> projectorFactory("Projector");

Projector::ProjectorList Projector::s_projectorList;

	
Projector::Projector()
	:_visible(true)
{
	_locationHelper.setComponent(this);
	Projector::registerProjector(this);
}

Projector::~Projector() {
	Projector::unregisterProjector(this);
}

void Projector::setTexture(Texture * tex) {
	_texture = tex;
	if (_texture.valid()) {
		_texture->setTextureWrapS(vwgl::Texture::kTargetTexture2D,vwgl::Texture::kWrapClampToEdge);
		_texture->setTextureWrapT(vwgl::Texture::kTargetTexture2D,vwgl::Texture::kWrapClampToEdge);
	}
}
	
bool Projector::serialize(JsonSerializer & serializer, bool lastItem) {
	serializer.openObject(true, true);
	std::string textureFile;
	serializer.writeProp<std::string>("type", "Projector", true, true);
	if (_texture.valid() && (textureFile=_texture->getFileName())!="") {
		textureFile = System::get()->getLastPathComponent(textureFile);
		serializer.writeProp<std::string>("texture", textureFile, true, true);
	}
	else {
		serializer.writeProp<std::string>("texture", "", true, true);
	}
	serializer.writeProp("projection", _projection, true, true);
	serializer.writeProp("transform", _transform, true, true);
	serializer.writeProp("attenuation", _attenuation, true, true);
	serializer.writeProp("visible", _visible, false, true);
	serializer.closeObject(true, !lastItem, true);
	return true;
}
	
bool Projector::saveResourcesToPath(const std::string & dstPath) {
	std::string srcPath;
	if (_texture.valid() && (srcPath=_texture->getFileName())!="") {
		std::string fileName = System::get()->getLastPathComponent(srcPath);
		std::string textureDstPath = System::get()->addPathComponent(dstPath, fileName);
		System::get()->cp(srcPath, textureDstPath, true);
	}
	return true;
}

void Projector::deserialize(JsonDeserializer & deserializer, const std::string &resourcePath) {
	std::string texture = deserializer.getString("texture", "");
	if (texture!="") {
		texture = System::get()->addPathComponent(resourcePath, texture);
		setTexture(Loader::get()->loadTexture(texture));
	}
	_projection = deserializer.getMatrix4("projection", _projection);
	_transform = deserializer.getMatrix4("transform", _transform);
	_attenuation = deserializer.getFloat("attenuation", _attenuation);
	_visible = deserializer.getBool("visible", _visible);
}

}
}