
#include <vwgl/scene/transform.hpp>
#include <vwgl/scene/scene_object.hpp>
#include <vwgl/state.hpp>

#include <JsonBox.h>

namespace vwgl {
namespace scene {

ComponentFactory<Transform> transformFactory("Transform");

Transform::Transform()
{
	
}
	
Transform::Transform(const Matrix4 & t)
{
	_transform.setMatrix(t);
}

Transform::Transform(TransformStrategy * strategy)
{
	_transform.setTransformStrategy(strategy);
}

Transform::~Transform() {
	
}

void Transform::willDraw() {
	if (sceneObject() && sceneObject()->isEnabled()) {
		State::get()->pushModelMatrix();
		State::get()->modelMatrix().mult(_transform.getMatrix());
	}
}

void Transform::didDraw() {
	if (sceneObject() && sceneObject()->isEnabled()) {
		State::get()->popModelMatrix();
	}
}
	
void Transform::identity() {
	if (_transform.getTransformStrategy()) {
		_transform.getTransformStrategy()->reset();
	}
	else {
		_transform.getMatrix().identity();
	}
}
	
bool Transform::serialize(JsonSerializer & serializer, bool lastItem) {
	serializer.openObject(true, true);
	serializer.writeProp<std::string>("type", "Transform", true, true);
	if (_transform.getTransformStrategy()) {
		serializer.writeKey("transformStrategy");
		_transform.getTransformStrategy()->serialize(serializer);
	}
	else {
		serializer.writeProp("transformMatrix", _transform.getMatrix(), false, true);
	}
	serializer.closeObject(true, !lastItem, true);
	return true;
}

void Transform::deserialize(JsonDeserializer & deserializer, const std::string &resourcePath) {
	if (deserializer.keyExists("transformStrategy")) {
		JsonLibPtr * jsonPtr = deserializer.getJsonLibPtr();
		JsonBox::Value & val = *static_cast<JsonBox::Value*>(jsonPtr);
		JsonBox::Value js_trxStrategy = val["transformStrategy"].getObject();
		JsonDeserializer trxStrategyDeserializer(static_cast<JsonLibPtr*>(&js_trxStrategy));
		vwgl::TransformStrategy * strategy = vwgl::TransformStrategy::factory(trxStrategyDeserializer);
		_transform.setTransformStrategy(strategy);
	}
	else if (deserializer.keyExists("transformMatrix")) {
		_transform.setMatrix(deserializer.getMatrix4("transformMatrix", Matrix4::makeIdentity()));
	}
}

}
}