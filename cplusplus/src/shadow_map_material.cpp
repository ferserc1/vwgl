
#include <vwgl/shadow_map_material.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

ShadowMapMaterial::ShadowMapMaterial() :Material() {
	initShader();
}

ShadowMapMaterial::~ShadowMapMaterial() {
	
}

void ShadowMapMaterial::initShader() {
	if (_initialized) return;
	_initialized = true;
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "shadow_map_cast.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "shadow_map_cast.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "shadow_map_cast.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "shadow_map_cast.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: shadow map cast shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("shadow_map_cast");
	
	loadVertexAttrib("aVertexPosition");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	getShader()->initUniformLocation("uCastShadow");
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uAlphaCutoff");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
}

void ShadowMapMaterial::setupUniforms() {
	getShader()->setUniform("uMVMatrix", modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());

	if (_settingsMaterial.valid()) {
		getShader()->setUniform("uCastShadow", _settingsMaterial->getCastShadows());
		Texture * tex = _settingsMaterial->getTexture() ? _settingsMaterial->getTexture() : TextureManager::get()->whiteTexture();

		getShader()->setUniform("uTexture", tex, Texture::kTexture0);
		getShader()->setUniform("uTextureOffset", _settingsMaterial->getTextureOffset());
		getShader()->setUniform("uTextureScale", _settingsMaterial->getTextureScale());
		getShader()->setUniform("uAlphaCutoff", _settingsMaterial->getAlphaCutoff());
	}
	else {
		getShader()->setUniform("uCastShadow", false);
	}
}

}
