
#include <vwgl/bloom_map_material.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

	
BloomMapMaterial::BloomMapMaterial() :_bloomAmount(100) {
	initShader();
}

BloomMapMaterial::~BloomMapMaterial() {
	
}

void BloomMapMaterial::initShader() {
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_bloom.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_bloom.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_bloom.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_bloom.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: bloom map shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("deferred_bloom");
	
	loadVertexAttrib("aVertexPos");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uLightingMap");
}
	
void BloomMapMaterial::setupUniforms() {
	Texture * lightingMap = _lightingMap.valid() ? _lightingMap.getPtr():TextureManager::get()->blackTexture();
	
	getShader()->setUniform("uLightingMap", lightingMap,Texture::kTexture0);
}

	

}