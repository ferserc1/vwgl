
#include <vwgl/vwglbloader.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/exceptions.hpp>
#include <vwgl/projector.hpp>
#include <vwgl/system.hpp>

#include <JsonBox.h>


namespace vwgl {
	
float getJsonNumber(const JsonBox::Value & val) {
	if (val.isInteger()) return static_cast<float>(val.getInt());
	else if (val.isDouble()) return static_cast<float>(val.getDouble());
	else return 0.0f;
}

Vector3 getJsonVector(const JsonBox::Value & val) {
	if (val.isArray() && val.getArray().size()==3) {
		return Vector3(getJsonNumber(val.getArray()[0]),
							 getJsonNumber(val.getArray()[1]),
							 getJsonNumber(val.getArray()[2]));
	}
	return Vector3();
}
	
class SolidSettingsJsonParser {
public:
	static float getFloat(JsonBox::Value & data, float defaultValue) {
		if (data.isDouble()) return static_cast<float>(data.getDouble());
		else if (data.isString()) return static_cast<float>(atof(data.getString().c_str()));
		else if (data.isBoolean()) return data.getBoolean() ? 1.0f:0.0f;
		else if (data.isInteger()) return static_cast<float>(data.getInt());
		else return defaultValue;
	}

	static Matrix4 getMatrix(JsonBox::Value & data) {
		Matrix4 result;
		
		if (data.isString()) {
			std::string matrixData = data.getString();
			std::stringstream stream(matrixData);
			float value[16];
			
			for (int i = 0; i<16; ++i) {
				stream >> value[i];
			}
			
			return Matrix4(value);
		}
		
		return result;
	}
	
	static Texture * getTexture(JsonBox::Value & js_tex, const std::string & path) {
		ptr<Texture> tex = NULL;
		
		if (js_tex.isString()) {
			std::string fileName = js_tex.getString();
			if (!vwgl::System::get()->isAbsolutePath(fileName)) {
				fileName = path + fileName;
			}
			tex = TextureManager::get()->loadTexture(Loader::get()->loadImage(fileName));
		}
		
		return tex.release();
	}
	
	static bool getBool(JsonBox::Value & js_val, bool defaultValue) {
		if (js_val.isBoolean()) {
			return js_val.getBoolean();
		}
		else if (js_val.isInteger()) {
			return js_val.getInt()==1;
		}
		return defaultValue;
	}

	static void getString(JsonBox::Value & js_val, std::string & inputString, const std::string & defaultValue) {
		if (js_val.isString()) {
			inputString = js_val.getString();
		}
		else {
			inputString = defaultValue;
		}
	}

	static void applyJson(Solid * solid, JsonBox::Value & projectorData, const std::string & absPath) {
		Texture * tex;
		if (projectorData.isObject()) {
			if ((tex=getTexture(projectorData["shadowTexture"],absPath))!=nullptr) {
				float attenuation = getFloat(projectorData["shadowAttenuation"], 1.0f);
				Matrix4 projection = getMatrix(projectorData["shadowProjection"]);
				Matrix4 transform = getMatrix(projectorData["shadowTransform"]);
				ptr<Projector> proj = new Projector();
			
				proj->setTexture(tex);
				proj->setAttenuation(attenuation);
				proj->setProjection(projection);
				proj->setTransform(transform);
			
				solid->setShadowProjector(proj.getPtr());
			}
			
			bool visible = getBool(projectorData["visible"], true);
			std::string groupName;
			getString(projectorData["groupName"], groupName, "");
			solid->setVisible(visible);
			solid->setGroupName(groupName);
		}
	}
};


VwglbLoader::VwglbLoader() {
}

VwglbLoader::~VwglbLoader() {
}
	
bool VwglbLoader::acceptFileType(const std::string & path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext=="vwglb";
}
	
Drawable * VwglbLoader::loadDrawable(const std::string & path) {
	if (_fileUtils.open(path, VwglbUtils::kModeRead)) {
		try {
			FileVersion version;
			int numberOfPlist;
			std::string materials;
			std::string projectorFile;
			ptr<Drawable> drawable = new Drawable();
			readHeader(drawable.getPtr(),version,numberOfPlist,materials,projectorFile);
			
			std::cout << "vwglb file version " << static_cast<int>(version.major) << "." <<
												  static_cast<int>(version.minor) << "." <<
												  static_cast<int>(version.revision) << std::endl;
			
			std::cout << numberOfPlist << " polygon lists found" << std::endl;
			
			readPolyLists(drawable.getPtr(),numberOfPlist);
			
			applyMaterials(drawable.getPtr(), materials);
			
			// Apply per-solid projectors, defined in material settings
			applyProjectors(drawable.getPtr(), materials);
			
			_fileUtils.close();
			
			drawable->initJoints();
			return drawable.release();
		}
		catch (std::exception const  & e) {
			std::cerr << "Error loading file:" << e.what() << std::endl;
			return NULL;
		}
	}
	return NULL;
}

void VwglbLoader::getDependencies(const std::string & path, std::vector<std::string> & deps) {
	if (_fileUtils.open(path, VwglbUtils::kModeRead)) {
		try {
			FileVersion version;
			int numberOfPlist;
			std::string materials;
			std::string projectorTexture;
			readHeader(version,numberOfPlist,materials,projectorTexture);
			
			GenericMaterial::getMaterialResourcesWithJson(deps, materials);

			if (projectorTexture!="") {
				deps.push_back(projectorTexture);
			}
			
			_fileUtils.close();
		}
		catch (std::exception const  & e) {
			std::cerr << "Error loading file:" << e.what() << std::endl;
		}
	}
}

void VwglbLoader::readHeader(Drawable * drawable, FileVersion & version, int & numberOfPlist, std::string &materials, std::string & projectorTexture) {
	//// File header
	// File endian 0=big endian, 1=little endian
	unsigned char endian;
	_fileUtils.readByte(endian);
	if (endian==0) _fileUtils.setBigEndian();
	else _fileUtils.setLittleEndian();

	// Version (major, minor, rev)
	_fileUtils.readByte(version.major);
	_fileUtils.readByte(version.minor);
	_fileUtils.readByte(version.revision);
	
	// Header type
	VwglbUtils::BlockType btype;
	_fileUtils.readBlock(btype);
	if (btype!=VwglbUtils::kHeader) {
		throw BaseException("VWGLB: File format exception. Expecting begin of file header");
	}
	
	// Number of poly list
	_fileUtils.readInteger(numberOfPlist);
	
	// Materials
	VwglbUtils::BlockType block;
	_fileUtils.readBlock(block);
	if (block!=VwglbUtils::kMaterials) {
		throw BaseException("VWGLB: File format exception. Expecting material list.");
	}

	_fileUtils.readString(materials);
	
	// Projector
	_fileUtils.readBlock(block);
	if (block==VwglbUtils::kShadowProjector) {
		std::string currentDir = Loader::get()->getCurrentDir();
		std::string fileName;
		float proj[16];
		float trans[16];
		float attenuation;
		_fileUtils.readString(fileName);
		_fileUtils.readFloat(attenuation);
		for (int i=0;i<16;++i) {
			_fileUtils.readFloat(proj[i]);
		}
		for (int i=0;i<16;++i) {
			_fileUtils.readFloat(trans[i]);
		}
		ptr<Projector> shadowProj = new Projector();
		
		projectorTexture = fileName;
		if (!vwgl::System::get()->isAbsolutePath(fileName))
			fileName = currentDir + fileName;
		shadowProj->setTexture(vwgl::Loader::get()->loadTexture(fileName));
		shadowProj->setAttenuation(attenuation);
		shadowProj->setProjection(vwgl::Matrix4(proj));
		shadowProj->setTransform(vwgl::Matrix4(trans));
		drawable->setShadowProjector(shadowProj.getPtr());
	}
	else {
		_fileUtils.seekForward(-4);
	}
	
	// Joint list
	// TODO: Support for multiple types of joints, and multiple output joints
	_fileUtils.readBlock(block);
	if (block==VwglbUtils::kJoint) {
		std::string jointData;
		_fileUtils.readString(jointData);
		
		JsonBox::Value joints;
		joints.loadFromString(jointData);
		
		if (joints["input"].isObject()) {
			JsonBox::Object js_inJoint = joints["input"].getObject();
			// TODO: Support for multiple types of joints
			ptr<physics::LinkJoint> inJoint = new physics::LinkJoint();
			inJoint->setOffset(getJsonVector(js_inJoint["offset"]));
			inJoint->setPitch(getJsonNumber(js_inJoint["pitch"]));
			inJoint->setRoll(getJsonNumber(js_inJoint["roll"]));
			inJoint->setYaw(getJsonNumber(js_inJoint["yaw"]));
			drawable->setInputJoint(inJoint.getPtr());
		}
		
		if (joints["output"].isArray() &&
			joints["output"].getArray().size()>0 &&
			joints["output"].getArray()[0].isObject()) {
			JsonBox::Object js_outJoint = joints["output"].getArray()[0].getObject();
			// TODO: Support for multiple types of joints
			physics::LinkJoint * joint = new physics::LinkJoint();
			joint->setOffset(getJsonVector(js_outJoint["offset"]));
			joint->setPitch(getJsonNumber(js_outJoint["pitch"]));
			joint->setRoll(getJsonNumber(js_outJoint["roll"]));
			joint->setYaw(getJsonNumber(js_outJoint["yaw"]));
			drawable->setOutputJoint(joint);
		}
	}
	else {
		_fileUtils.seekForward(-4);
	}
}

void VwglbLoader::readHeader(FileVersion & version, int & numberOfPlist, std::string & materials, std::string & projectorTexture) {
	//// File header
	// File endian 0=big endian, 1=little endian
	unsigned char endian;
	_fileUtils.readByte(endian);
	if (endian==0) _fileUtils.setBigEndian();
	else _fileUtils.setLittleEndian();
	
	// Version (major, minor, rev)
	_fileUtils.readByte(version.major);
	_fileUtils.readByte(version.minor);
	_fileUtils.readByte(version.revision);
	
	// Header type
	VwglbUtils::BlockType btype;
	_fileUtils.readBlock(btype);
	if (btype!=VwglbUtils::kHeader) {
		throw BaseException("VWGLB: File format exception. Expecting begin of file header");
	}
	
	// Number of poly list
	_fileUtils.readInteger(numberOfPlist);
	
	// Materials
	VwglbUtils::BlockType block;
	_fileUtils.readBlock(block);
	if (block!=VwglbUtils::kMaterials) {
		throw BaseException("VWGLB: File format exception. Expecting material list.");
	}
	
	_fileUtils.readString(materials);
	
	// Projector
	_fileUtils.readBlock(block);
	if (block==VwglbUtils::kShadowProjector) {
		std::string currentDir = Loader::get()->getCurrentDir();
		std::string fileName;
		float proj[16];
		float trans[16];
		float attenuation;
		_fileUtils.readString(fileName);
		_fileUtils.readFloat(attenuation);
		for (int i=0;i<16;++i) {
			_fileUtils.readFloat(proj[i]);
		}
		for (int i=0;i<16;++i) {
			_fileUtils.readFloat(trans[i]);
		}
		ptr<Projector> shadowProj = new Projector();
		
		projectorTexture = fileName;
	}
	else {
		_fileUtils.seekForward(-4);
	}
	
	// Joint list
	_fileUtils.readBlock(block);
	if (block==VwglbUtils::kJoint) {
		std::string jointData;
		_fileUtils.readString(jointData);
		//  The joint list has no dependencies
	}
	else {
		_fileUtils.seekForward(-4);
	}
}

void VwglbLoader::readPolyLists(Drawable * drawable, int numberOfPlist) {
	std::cout << "reading " << numberOfPlist << " polyList" << std::endl;
	VwglbUtils::BlockType block;
	
	_fileUtils.readBlock(block);
	if (block!=VwglbUtils::kPolyList) throw BaseException("VWGLB: File format exception. Expecting poly list.");
	for (int i=0; i<numberOfPlist; ++i) {
		readSinglePolyList(drawable);
	}
}

void VwglbLoader::readSinglePolyList(Drawable * drawable) {
	VwglbUtils::BlockType block;
	bool done = false;
	bool vertexFound = false;
	bool normalFound = false;
	bool tex0Found = false;
	bool tex1Found = false;
	bool tex2Found = false;
	bool indexFound = false;
	std::vector<float> vector;
	std::vector<unsigned int> ivector;
	std::string name;
	
	ptr<Solid> s = new Solid();
	ptr<PolyList> plist = new PolyList();
	while (!done) {
		vector.clear();
		_fileUtils.readBlock(block);
		switch (block) {
			case VwglbUtils::kPlistName:
				_fileUtils.readString(name);
				plist->setName(name);
				break;
			case VwglbUtils::kMatName:
				_fileUtils.readString(name);
				plist->setMaterialName(name);
				break;
			case VwglbUtils::kVertexArray:
				if (vertexFound) throw BaseException("VWGLB: File format exception. duplicate vertex array");
				vertexFound = true;
				_fileUtils.readArray(vector);
				plist->addVertexVector(&vector[0], static_cast<int>(vector.size()));
				break;
			case VwglbUtils::kNormalArray:
				if (normalFound) throw BaseException("VWGLB: File format exception. duplicate normal array");
				normalFound = true;
				_fileUtils.readArray(vector);
				plist->addNormalVector(&vector[0], static_cast<int>(vector.size()));
				break;
			case VwglbUtils::kTexCoord0Array:
				if (tex0Found) throw BaseException("VWGLB: File format exception. duplicate texcoord0 array");
				tex0Found = true;
				_fileUtils.readArray(vector);
				plist->addTexCoord0Vector(&vector[0], static_cast<int>(vector.size()));
				break;
			case VwglbUtils::kTexCoord1Array:
				if (tex1Found) throw BaseException("VWGLB: File format exception. duplicate texcoord1 array");
				tex1Found = true;
				_fileUtils.readArray(vector);
				plist->addTexCoord1Vector(&vector[0], static_cast<int>(vector.size()));
				break;
			case VwglbUtils::kTexCoord2Array:
				if (tex2Found) throw BaseException("VWGLB: File format exception. duplicate texcoord2 array");
				tex2Found = true;
				_fileUtils.readArray(vector);
				plist->addTexCoord0Vector(&vector[1], static_cast<int>(vector.size()));
				break;
			case VwglbUtils::kIndexArray:
				if (indexFound) throw BaseException("VWGLB: File format exception. duplicate index array");
				indexFound = true;
				_fileUtils.readArray(ivector);
				plist->addIndexVector(&ivector[0], static_cast<int>(ivector.size()));
				break;
			case VwglbUtils::kPolyList:
			case VwglbUtils::kEnd:
				done = true;
				break;
			default:
				throw BaseException("VWGLB: File format exception. Unexpected poly list member found");
		}
	}
	plist->buildPolyList();
	s->setPolyList(plist.getPtr());
	drawable->addSolid(s.getPtr());
}

void VwglbLoader::applyMaterials(Drawable * drawable, const std::string & strData) {
	GenericMaterial::setMaterialsWithJson(drawable, strData);
}

void VwglbLoader::applyProjectors(Drawable * drawable, const std::string & strData) {
	std::string currentDir = Loader::get()->getCurrentDir();
	
	JsonBox::Value js_materials;
	js_materials.loadFromString(strData);
	
	std::map<std::string,JsonBox::Value> materials;
	
	if (js_materials.isArray()) {
		JsonBox::Array matArray = js_materials.getArray();
		JsonBox::Array::iterator it;
		for (it=matArray.begin(); it!=matArray.end(); ++it) {
			JsonBox::Value name = (*it)["name"];
			if (name.isString()) {
				materials[name.getString()] = *it;
			}
		}
	}
	
	SolidList solidList = drawable->getSolidList();
	SolidList::iterator it;
	for (it=solidList.begin(); it!=solidList.end(); ++it) {
		Solid * s = (*it).getPtr();
		PolyList * p = s->getPolyList();
		JsonBox::Value matJson = materials[p->getMaterialName()];
		if (matJson.isObject()) {
			SolidSettingsJsonParser::applyJson(s, matJson, currentDir);
		}
	}
}
	
}
