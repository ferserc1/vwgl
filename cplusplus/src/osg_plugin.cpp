
#include <vwgl/osg_plugin.hpp>
#include <vwgl/vwgl.hpp>

#include <fstream>

namespace vwgl {

OSGWritter::OSGWritter()
{

}

OSGWritter::~OSGWritter()
{

}

bool OSGWritter::acceptFileType(const std::string & path) {
	return vwgl::System::get()->getExtension(path)=="osg";
}

bool OSGWritter::writeScene(const std::string & path, vwgl::scene::Node * node) {
	if (_file.is_open()) {
		_file.close();
	}

	_file.open(path);
	if (_file.is_open()) {
		_path = path;
		_groupUID = 0;
		_geoUID = 0;
		_stateSetUID = 0;
		_numTabs = 0;
		_texEnvId = 0;
		_tabs = "";
		vwgl::ptr<vwgl::scene::Transform> trx = new vwgl::scene::Transform(
					vwgl::Matrix4::makeRotation(vwgl::Math::kPiOver2, 1.0f, 0.0f, 0.0f)
					.translate(0.0f, -1.17f, 0.0f));
		node->addComponent(trx.getPtr());
		writeNode(node);
		node->removeComponent(trx.getPtr());
		trx = nullptr;
		_file.close();
		_path = "";
	}

	return true;
}

void OSGWritter::writeNode(vwgl::scene::Node * node) {
	vwgl::scene::Transform * trx = node->getComponent<vwgl::scene::Transform>();
	vwgl::scene::Drawable * drw = node->getComponent<vwgl::scene::Drawable>();
	if (trx) {
		_file << tabs() << "MatrixTransform";
		openBlock();
		_file << tabs() << "name \"" << node->getName() << "\"\n";
		_file << tabs() << "referenceFrame RELATIVE\n";
		_file << tabs() << "Matrix ";
			openBlock();
			vwgl::Matrix4 mat = trx->getTransform().getMatrix();
			std::cout << mat.toString() << std::endl;
			_file << tabs() << mat.get00() << " " << mat.get01() << " " << mat.get02() << " " << mat.get03() << "\n";
			_file << tabs() << mat.get10() << " " << mat.get11() << " " << mat.get12() << " " << mat.get13() << "\n";
			_file << tabs() << mat.get20() << " " << mat.get21() << " " << mat.get22() << " " << mat.get23() << "\n";
			_file << tabs() << mat.get30() << " " << mat.get31() << " " << mat.get32() << " " << mat.get33() << "\n";
			closeBlock();
	}
	else {
		_file << tabs() << "Group";
		openBlock();
		_file << tabs() << "UniqueID Group_" << _groupUID << "\n";
		++_groupUID;
	}
	_file << tabs() << "nodeMask 0xffffffff\n";
	_file << tabs() << "cullingActive TRUE\n";
	_file << tabs() << "num_children " << node->children().size() + (drw ? drw->getPolyListVector().size():0) << "\n";
	node->iterateChildren([&](vwgl::scene::Node * node) {
		writeNode(node);
	});
	if (drw) {
		writeDrawable(drw);
	}
	closeBlock();

}

void OSGWritter::writeDrawable(vwgl::scene::Drawable * drw) {
	drw->eachElement([&](vwgl::PolyList * plist, vwgl::Material * mat, vwgl::Transform &) {
		_file << tabs() << "Geode ";
		openBlock();
		_file << tabs() << "name \"" << drw->getName() << "_" << _geoUID << "\"\n";
		_file << tabs() << "nodeMask 0xffffffff\n";
		_file << tabs() << "cullingActive TRUE\n";
		_file << tabs() << "num_geodes 1\n";

			_file << tabs() << "Geometry ";
			openBlock();
			std::string geoId = "Geode_" + std::to_string(_geoUID);
			std::string stateSetID = "StateSet_" + std::to_string(_stateSetUID);
			++_geoUID;
			++_stateSetUID;

			writeStateSet(dynamic_cast<vwgl::GenericMaterial*>(mat),stateSetID);
			writeGeom(plist,geoId);

			closeBlock();
		closeBlock();
	});
}

void OSGWritter::writeStateSet(vwgl::GenericMaterial * mat, const std::string & id) {
	_file << tabs() << "StateSet ";
	openBlock();
		_file << tabs() << "UniqueID " << id << "\n";
		_file << tabs() << "rendering_hint DEFAULT_BIN\n";
		_file << tabs() << "renderBinMode INHERIT\n";
		_file << tabs() << "Material ";
		openBlock();
			_file << tabs() << "ColorMode OFF\n";
			_file << tabs() << "ambientColor 0 0 0 1\n";
			_file << tabs() << "diffuseColor ";
			printVector(mat->getDiffuse()); _file << "\n";
			_file << tabs() << "specularColor 0 0 0 1\n";
			_file << tabs() << "emissionColor 0 0 0 1\n";
			_file << tabs() << "shininess " << mat->getShininess() << "\n";
		closeBlock();

		if (mat->getTexture()) {
			_file << tabs() << "GL_TEXTURE_2D ON\n";
			_file << tabs() << "Texture2D ";
			openBlock();
				std::string destPath = vwgl::System::get()->getPath(_path);
				std::string texFile = vwgl::System::get()->getLastPathComponent(mat->getTexture()->getFileName());
				destPath = vwgl::System::get()->addPathComponent(destPath,texFile);
				vwgl::System::get()->cp(mat->getTexture()->getFileName(), destPath, true);

				_file << tabs() << "DataVariance STATIC\n";
				_file << tabs() << "file \"" << texFile << "\"\n";
				_file << tabs() << "wrap_s REPEAT\n";
				_file << tabs() << "wrap_t REPEAT\n";
				_file << tabs() << "wrap_r REPEAT\n";
				_file << tabs() << "min_filter LINEAR_MIPMAP_LINEAR\n";
				_file << tabs() << "mag_filter LINEAR\n";
				_file << tabs() << "maxAnisotropy 1\n";
			closeBlock();
			_file << tabs() << "TexMat ";
			openBlock();
				vwgl::Vector2 scale = mat->getTextureScale();
				vwgl::Vector2 offset = mat->getTextureOffset();
				_file << tabs() << scale.x() << " 0 0 0\n";
				_file << tabs() << "0 " << scale.y() << " 0 0\n";
				_file << tabs() << "0 0 1 0\n";
				_file << tabs() << offset.x() << " " << offset.y() << " 0 1\n";
				_file << tabs() << "scaleByTextureRectangleSize TRUE\n";
			closeBlock();
		}

	closeBlock();
}

void OSGWritter::writeGeom(vwgl::PolyList * plist, const std::string & id) {
	_file << tabs() << "useDisplayList TRUE\n";
	_file << tabs() << "userVertexBufferObjects FALSE\n";
	_file << tabs() << "PrimitiveSets 1 ";
	openBlock();

	_file << tabs() << "DrawElementsUInt TRIANGLES " << plist->getIndexCount();
		openBlock();
		_file << tabs();
		vwgl::PolyList::UIntVector::iterator iit;
		for (iit=plist->getIndexVector().begin(); iit!=plist->getIndexVector().end(); ++iit) {
			_file << *iit << " ";
		}
		closeBlock();
	closeBlock();

	_file << tabs() << "VertexArray Vec3Array " << plist->getVertexCount() / 3;
		openBlock();
		_file << tabs();
		vwgl::PolyList::FloatVector::iterator vit;
		for (vit=plist->getVertexVector().begin(); vit!=plist->getVertexVector().end(); ++vit) {
			_file << *vit << " ";
		}
		closeBlock();

	_file << tabs() << "NormalBinding PER_VERTEX\n";
	_file << tabs() << "NormalArray Vec3Array " << plist->getNormalCount() / 3;
		openBlock();
		_file << tabs();
		vwgl::PolyList::FloatVector::iterator nit;
		for (nit=plist->getNormalVector().begin(); nit!=plist->getNormalVector().end(); ++nit) {
			_file << *nit << " ";
		}
		closeBlock();

	_file << tabs() << "TexCoordArray 0 Vec2Array " << plist->getTexCoord0Count() / 2;
		openBlock();
		_file << tabs();
		vwgl::PolyList::FloatVector::iterator tit;
		for (tit=plist->getTexCoord0Vector().begin(); tit!=plist->getTexCoord0Vector().end(); ++tit) {
			_file << *tit << " ";
		}

		closeBlock();
}

}
