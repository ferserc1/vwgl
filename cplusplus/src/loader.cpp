
#include <vwgl/loader.hpp>
#include <vwgl/generic_material.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/system.hpp>
//#include <JsonBox.h>

#include <map>

namespace vwgl {

Image * Loader::loadImage(const std::string & path, bool loadFromResources) {
	std::string standardPath;
	System::get()->standarizePath(path,standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	if (_imageCache.find(standardPath)==_imageCache.end()) {
		ptr<Image> img = new Image();
		img->load(standardPath);
		_imageCache[standardPath] = img;
		return img.release();
	}
	else {
		return _imageCache[standardPath].getPtr();
	}
}

vwgl::Node * Loader::loadNode(const std::string & path, bool loadFromResources) {
	ReadNodePlugin * selected = nullptr;
	std::string standardPath;
	System::get()->standarizePath(path, standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		selected = dynamic_cast<ReadNodePlugin*>(p);
		if (selected && selected->acceptFileType(standardPath)) {
			break;
		}
	}
	
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		ptr<Node> result = selected->loadNode(standardPath);
		std::vector<std::string>::iterator it;
		for (it=selected->getWarningMessages().begin(); it!=selected->getWarningMessages().end(); ++it) {
			_warningMessages.push_back((*it));
		}
		return result.release();
	}
	return nullptr;
}
	
vwgl::library::Node * Loader::loadLibrary(const std::string & path, bool loadFomResources) {
	ReadLibraryPlugin * selected = nullptr;
	std::string standardPath;
	System::get()->standarizePath(path, standardPath);
	if (loadFomResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		selected = dynamic_cast<ReadLibraryPlugin*>(p);
		if (selected && selected->acceptFileType(standardPath)) {
			break;
		}
	}
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		library::Node * result = selected->loadLibrary(standardPath);
		std::vector<std::string>::iterator it;
		for (it=selected->getWarningMessages().begin(); it!=selected->getWarningMessages().end(); ++it) {
			_warningMessages.push_back((*it));
		}
		return result;
	}
	return nullptr;
}

Drawable * Loader::loadDrawable(const std::string & path, bool loadFromResources) {
	ReadDrawablePlugin * selected = NULL;
	std::string standardPath;
	System::get()->standarizePath(path,standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		if (p->acceptFileType(standardPath) && (selected=dynamic_cast<ReadDrawablePlugin*>(p))) {
			break;
		}
	}
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		Drawable * result = selected->loadDrawable(standardPath);
		std::vector<std::string>::iterator it;
		for (it=selected->getWarningMessages().begin(); it!=selected->getWarningMessages().end(); ++it) {
			_warningMessages.push_back((*it));
		}
		std::string drawableName;
		selected->getFileName(path, drawableName);
		if (result) result->setName(drawableName);
		return result;
	}
	return nullptr;
}
	
vwgl::scene::Node * Loader::loadScene(const std::string & path, bool loadFromResources) {
	ReadScenePlugin * selected = nullptr;
	std::string standardPath;
	System::get()->standarizePath(path, standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		if (p->acceptFileType(standardPath) && (selected=dynamic_cast<ReadScenePlugin*>(p))) {
			break;
		}
	}
	
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		return selected->loadScene(standardPath);
	}
	return nullptr;
}

vwgl::scene::Node * Loader::loadPrefab(const std::string & path, bool loadFromResources) {
	ReadPrefabPlugin * selected = nullptr;
	std::string standardPath;
	System::get()->standarizePath(path, standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		if (p->acceptFileType(standardPath) && (selected=dynamic_cast<ReadPrefabPlugin*>(p))) {
			break;
		}
	}
	
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		vwgl::scene::Node * result = selected->loadPrefab(standardPath);
		if (result) {
			result->setName(System::get()->getFileName(standardPath));
		}
		return result;
	}
	return nullptr;
}

vwgl::scene::Drawable * Loader::loadModel(const std::string & path, bool loadFromResources) {
	ReadModelPlugin * selected = nullptr;
	std::string standardPath;
	System::get()->standarizePath(path, standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		if (p->acceptFileType(standardPath) && (selected=dynamic_cast<ReadModelPlugin*>(p))) {
			break;
		}
	}
	
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		vwgl::scene::Drawable * result = selected->loadModel(standardPath);
		return result;
	}
	return nullptr;
}

vwgl::TextFont * Loader::loadFont(const std::string & path, int size, int res, bool loadFromResources) {
	ReadFontPlugin * selected = nullptr;
	std::string standardPath;
	System::get()->standarizePath(path, standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		if (p->acceptFileType(standardPath) && (selected=dynamic_cast<ReadFontPlugin*>(p))) {
			break;
		}
	}
	
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		vwgl::TextFont * result = selected->loadFont(standardPath, size, res);
		return result;
	}
	return nullptr;
}

void Loader::getDrawableDependencies(const std::string & path, std::vector<std::string> & deps, bool loadFromResources) {
	ReadDrawablePlugin * selected = NULL;
	std::string standardPath;
	System::get()->standarizePath(path,standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		if (p->acceptFileType(standardPath) && (selected=dynamic_cast<ReadDrawablePlugin*>(p))) {
			break;
		}
	}
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		selected->getDependencies(standardPath, deps);
	}
}

Texture * Loader::loadTexture(const std::string &path, bool loadFromResources) {
	std::string standardPath;
	System::get()->standarizePath(path,standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	ptr<Texture> tex = TextureManager::get()->loadTexture(loadImage(standardPath));
	return tex.release();
}

vwgl::CubeMap * Loader::loadCubemap(const std::string & posX,
							const std::string & negX,
							const std::string & posY,
							const std::string & negY,
							const std::string & posZ,
							const std::string & negZ,
							bool loadFromResources) {
	
	std::string stdPosX;
	System::get()->standarizePath(posX,stdPosX);
	std::string stdNegX;
	System::get()->standarizePath(negX,stdNegX);
	std::string stdPosY;
	System::get()->standarizePath(posY,stdPosY);
	std::string stdNegY;
	System::get()->standarizePath(negY,stdNegY);
	std::string stdPosZ;
	System::get()->standarizePath(posZ,stdPosZ);
	std::string stdNegZ;
	System::get()->standarizePath(negZ,stdNegZ);
	if (loadFromResources) {
		stdPosX = System::get()->getResourcesPath(stdPosX);
		stdPosX = System::get()->getResourcesPath(stdPosX);
		stdPosY = System::get()->getResourcesPath(stdPosY);
		stdPosY = System::get()->getResourcesPath(stdPosY);
		stdPosZ = System::get()->getResourcesPath(stdPosZ);
		stdPosZ = System::get()->getResourcesPath(stdPosZ);
	}
	ptr<CubeMap> cm = TextureManager::get()->loadCubeMap(loadImage(stdPosX),
												  loadImage(stdNegX),
												  loadImage(stdPosY),
												  loadImage(stdNegY),
												  loadImage(stdPosZ),
												  loadImage(stdNegZ));
	return cm.release();
}
	
Dictionary * Loader::loadDictionary(const std::string & path, bool loadFromResources) {
	ReadDictionaryPlugin * selected = NULL;
	std::string standardPath;
	System::get()->standarizePath(path,standardPath);
	if (loadFromResources) {
		standardPath = System::get()->getResourcesPath(standardPath);
	}
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		if (p->acceptFileType(standardPath) && (selected=dynamic_cast<ReadDictionaryPlugin*>(p))) {
			break;
		}
	}
	if (selected) {
		selected->removeLastPathComponent(standardPath, _currentDir);
		Dictionary * result = selected->loadDictionary(standardPath);
		std::vector<std::string>::iterator it;
		for (it=selected->getWarningMessages().begin(); it!=selected->getWarningMessages().end(); ++it) {
			_warningMessages.push_back((*it));
		}
		return result;
	}
	return nullptr;
}

bool Loader::canReadFile(const std::string & file) {
	ReaderList::iterator it;
	for (it=_readerPluginList.begin(); it!=_readerPluginList.end(); ++it) {
		ReaderPlugin * p = (*it).getPtr();
		if (p->acceptFileType(file)) return true;
	}
	return false;
}
	
bool Loader::canWriteFile(const std::string & file) {
	WritterList::iterator it;
	for (it=_writterPluginList.begin(); it!=_writterPluginList.end(); ++it) {
		WritterPlugin * p = (*it).getPtr();
		if (p->acceptFileType(file)) return true;
	}
	return false;
}
	
bool Loader::registerWritter(WritterPlugin * plugin) {
	_writterPluginList.push_back(plugin);
	return true;
}

bool Loader::registerReader(ReaderPlugin * plugin) {
	_readerPluginList.push_back(plugin);
	return true;
}

bool Loader::writeNode(const std::string & path, Node * node) {
	WriteNodePlugin * selected = nullptr;
	std::string stdPath;
	System::get()->standarizePath(path,stdPath);
	
	WritterList::iterator it;
	for (it=_writterPluginList.begin(); it!=_writterPluginList.end(); ++it) {
		WritterPlugin * p = (*it).getPtr();
		if (p->acceptFileType(stdPath) && (selected=dynamic_cast<WriteNodePlugin*>(p))) {
			break;
		}
	}
	if (selected) {
		selected->removeLastPathComponent(stdPath, _currentDir);
		return selected->writeNode(stdPath, node);
	}
	return false;
}
	
bool Loader::writeLibrary(const std::string & path, library::Node * lib) {
	WriteLibraryPlugin * selected = nullptr;
	std::string stdPath;
	System::get()->standarizePath(path,stdPath);

	WritterList::iterator it;
	for (it=_writterPluginList.begin(); it!=_writterPluginList.end(); ++it) {
		WritterPlugin * p = (*it).getPtr();
		if (p->acceptFileType(stdPath) && (selected=dynamic_cast<WriteLibraryPlugin*>(p))) {
			break;
		}
	}
	if (selected) {
		selected->removeLastPathComponent(stdPath, _currentDir);
		return selected->writeLibrary(stdPath, lib);
	}
	return false;
}

bool Loader::writeDrawable(const std::string & path, Drawable * drawable) {
	WriteDrawablePlugin * selected = NULL;
	std::string stdPath;
	System::get()->standarizePath(path,stdPath);

	WritterList::iterator it;
	for (it=_writterPluginList.begin(); it!=_writterPluginList.end(); ++it) {
		WritterPlugin * p = (*it).getPtr();
		if (p->acceptFileType(stdPath) && (selected=dynamic_cast<WriteDrawablePlugin*>(p))) {
			break;
		}
	}
	if (selected) {
		selected->removeLastPathComponent(stdPath, _currentDir);
		return selected->writeDrawable(stdPath, drawable);
	}
	return false;
}

bool Loader::writeDictionary(const std::string & path, Dictionary * drawable) {
	WriteDictionaryPlugin * selected = NULL;
	std::string stdPath;
	System::get()->standarizePath(path,stdPath);
	
	WritterList::iterator it;
	for (it=_writterPluginList.begin(); it!=_writterPluginList.end(); ++it) {
		WritterPlugin * p = (*it).getPtr();
		if (p->acceptFileType(stdPath) && (selected=dynamic_cast<WriteDictionaryPlugin*>(p))) {
			break;
		}
	}
	if (selected) {
		return selected->writeDictionary(stdPath, drawable);
	}
	return false;
}

bool Loader::writeModel(const std::string & path, scene::Drawable * model) {
	WriteModelPlugin * selected = NULL;
	std::string stdPath;
	System::get()->standarizePath(path, stdPath);

	WritterList::iterator it;
	for (it = _writterPluginList.begin(); it != _writterPluginList.end(); ++it) {
		WritterPlugin * p = (*it).getPtr();
		if (p->acceptFileType(stdPath) && (selected = dynamic_cast<WriteModelPlugin*>(p))) {
			break;
		}
	}
	if (selected) {
		return selected->writeModel(stdPath, model);
	}
	return false;
}

// This function will write a prefab or a model, depending on the file extension (vwglb: prefab, vitscn: scene)
bool Loader::writeNode(const std::string &path, scene::Node * node) {
	std::string stdPath;
	System::get()->standarizePath(path, stdPath);
	WritePrefabPlugin * prefabPlugin = nullptr;
	WriteScenePlugin * scenePlugin = nullptr;

	WritterList::iterator it;
	for (it = _writterPluginList.begin(); it != _writterPluginList.end(); ++it) {
		WritterPlugin * p = (*it).getPtr();
		if (p->acceptFileType(stdPath) && (prefabPlugin = dynamic_cast<WritePrefabPlugin*>(p))) {
			break;
		}
		else if (p->acceptFileType(stdPath) && (scenePlugin = dynamic_cast<WriteScenePlugin*>(p))) {
			break;
		}
	}
	if (prefabPlugin) {
		return prefabPlugin->writePrefab(stdPath, node);
	}
	else if (scenePlugin) {
		return scenePlugin->writeScene(stdPath, node);
	}

	return false;
}

Loader * Loader::s_loader = NULL;

Loader * Loader::get() {
	if (s_loader==NULL) {
		s_loader = new Loader();
	}
	return s_loader;
}
	
void Loader::finalize() {
	_readerPluginList.clear();
	_writterPluginList.clear();
	_imageCache.clear();
}

Loader::Loader() {
	SingletonController::get()->registerSingleton(this);
}

Loader::~Loader() {
	SingletonController::get()->unregisterSingleton(this);
}

std::string removeLastPathComponent(const std::string & path) {
	std::string::const_iterator pivot = std::find( path.rbegin(), path.rend(), '/' ).base();
	std::string basename( pivot, path.end() );
	std::string dirname( path.begin(), pivot == path.begin() ? pivot : pivot - 1 );
	return dirname + "/";
}


}
