
#include <vwgl/advanced_def_renderer_material.hpp>

namespace vwgl {

AdvancedDefRendererMaterial::AdvancedDefRendererMaterial()
{
	initShader();
}
	
AdvancedDefRendererMaterial::~AdvancedDefRendererMaterial() {
	
}

void AdvancedDefRendererMaterial::initShader() {
	getShader()->loadAndAttachShader(Shader::kTypeVertex, "advanced_def_mix.vsh");
	getShader()->loadAndAttachShader(Shader::kTypeFragment, "advanced_def_mix.fsh");
	getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_Color");
	getShader()->link();
	
	loadVertexAttrib("in_Vertex");
	loadTexCoord0Attrib("in_TexCoord");
	
	getShader()->initUniformLocation("uDiffuse");
	getShader()->initUniformLocation("uNormal");
	getShader()->initUniformLocation("uSpecular");
	getShader()->initUniformLocation("uPosition");
}

void AdvancedDefRendererMaterial::setupUniforms() {
	getShader()->setUniform("uDiffuse", _diffuseMap.getPtr(), Texture::kTexture0);
	getShader()->setUniform("uNormal", _normalMap.getPtr(), Texture::kTexture1);
	getShader()->setUniform("uSpecular", _specularMap.getPtr(), Texture::kTexture2);
	getShader()->setUniform("uPosition", _positionMap.getPtr(), Texture::kTexture3);
}

}