
#include <vwgl/color_picker.hpp>
#include <vwgl/solid.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/drawable_node.hpp>
#include <vwgl/group.hpp>
#include <vwgl/color_pick_material.hpp>
#include <vwgl/transform_node.hpp>
#include <vwgl/gizmo_manager.hpp>

#pragma warning ( disable : 4835 )

namespace vwgl {

// The "part" component of the pickerId structure has been removed from the equal operator due to incompatibilities
// with some graphic cards in Windows platform showing the alpha channel

PickId ColorPicker::_newPickId;

ColorPicker::ColorPicker() :_pickGizmos(true) {
	_findNodeVisitor = new PickIdFindVisitor();
	_drawPickIdsVisitor = new PickDrawVisitor();
	_drawGizmosVisitor = new DrawGizmosVisitor();
}

ColorPicker::~ColorPicker() {
	
}

void ColorPicker::assignPickId(vwgl::Solid *solid) {
	//ColorPicker::_newPickId.part++;
	//if (ColorPicker::_newPickId.part==0) {
		_newPickId.item++;
		if (ColorPicker::_newPickId.item==0) {
			ColorPicker::_newPickId.section++;
			if (ColorPicker::_newPickId.section==0) {
				ColorPicker::_newPickId.group++;
			}
		}
	//}
	solid->_pickId = _newPickId;
}

void ColorPicker::assignPickId(vwgl::Drawable * drawable) {
	SolidList::iterator it;
	for (it=drawable->getSolidList().begin(); it!=drawable->getSolidList().end(); ++it) {
		assignPickId((*it).getPtr());
	}
}
	
void ColorPicker::assignPickId(vwgl::Light * light) {
	if (light) {
		_newPickId.item++;
		if (ColorPicker::_newPickId.item==0) {
			ColorPicker::_newPickId.section++;
			if (ColorPicker::_newPickId.section==0) {
				ColorPicker::_newPickId.group++;
			}
		}
		light->setPickId(_newPickId);
	}
}

void ColorPicker::destroy() {
	_findNodeVisitor = nullptr;
	_drawPickIdsVisitor = nullptr;
	_drawGizmosVisitor = nullptr;
}
	
void ColorPicker::pick(Node * sceneRoot, PickId id) {
	_findNodeVisitor->setTarget(id);
	_findNodeVisitor->visit(sceneRoot);
}
	
void ColorPicker::pick(Node * sceneRoot, int mouseX, int mouseY, int viewportW, int viewportH, Camera * cam) {
	glViewport(0, 0, viewportW, viewportH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	vwgl::CameraManager::get()->applyTransform(cam);
	_drawPickIdsVisitor->setCamera(cam);
	_drawPickIdsVisitor->visit(sceneRoot);
	glClear(GL_DEPTH_BUFFER_BIT);
	_drawGizmosVisitor->useMaterial(_drawPickIdsVisitor->getMaterial());
	_drawGizmosVisitor->visit(sceneRoot);
	unsigned char pixels[4];
	glReadPixels(mouseX, mouseY, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &pixels);
	_resultId.group = (unsigned char)pixels[0];
	_resultId.section = (unsigned char)pixels[1];
	_resultId.item = (unsigned char)pixels[2];
	//_resultId.part = (unsigned char)pixels[3];
	_resultId.part = (unsigned char)0;
	_findNodeVisitor->clear();
	_findNodeVisitor->setTarget(_resultId);
	_findNodeVisitor->visit(sceneRoot);
	
	// Try to pick a manipulator gizmo
	if (vwgl::GizmoManager::get()->getCurrentGizmo() && _findNodeVisitor->getDrawable()==nullptr && _findNodeVisitor->getLight()==nullptr) {
		vwgl::ptr<vwgl::DrawableNode> gizmo = new vwgl::DrawableNode(vwgl::GizmoManager::get()->getCurrentGizmo()->getDrawable());
		_findNodeVisitor->visit(gizmo.getPtr());
		gizmo->setDrawable(nullptr);
	}
}

const Drawable * ColorPicker::getDrawable() const {
	return _findNodeVisitor->getDrawable();
}

const Solid * ColorPicker::getSolid() const {
	return _findNodeVisitor->getSolid();
}
	
const Light * ColorPicker::getLight() const {
	return _findNodeVisitor->getLight();
}

Light * ColorPicker::getLight() {
	return _findNodeVisitor->getLight();
}
	
Drawable * ColorPicker::getDrawable() {
	return _findNodeVisitor->getDrawable();
}

Solid * ColorPicker::getSolid() {
	return _findNodeVisitor->getSolid();
}
	
void ColorPicker::clearResult() {
	_findNodeVisitor->clear();
}


PickIdFindVisitor::PickIdFindVisitor() {
	
}

PickIdFindVisitor::~PickIdFindVisitor() {
	
}

void PickIdFindVisitor::visit(vwgl::Node *node) {
	_drawable = nullptr;
	_solid = nullptr;
	DrawableNode * dn = dynamic_cast<DrawableNode*>(node);
	Group * g = dynamic_cast<Group*>(node);
	Light * l = dynamic_cast<Light*>(node);
	
	if (dn && dynamic_cast<vwgl::Drawable*>(dn->getDrawable())) {
		Drawable * d = dynamic_cast<vwgl::Drawable*>(dn->getDrawable());
		SolidList::iterator it;
		for (it=d->getSolidList().begin();it!=d->getSolidList().end();++it) {
			if ((*it)->_pickId==_target && !(*it)->_pickId.isZero()) {
				_solid = (*it).getPtr();
				_drawable = d;
				return;
			}
		}
	}
	else if (g) {
		NodeList::iterator it;
		for (it=g->getNodeList().begin(); it!=g->getNodeList().end(); ++it) {
			visit((*it).getPtr());
			if (_drawable.valid()) {
				return;
			}
		}
	}
	else if (l) {
		if (l->getPickId()==_target && !l->getPickId().isZero()) {
			_light = l;
			return;
		}
	}
}



PickDrawVisitor::PickDrawVisitor()  {
	_material = new ColorPickMaterial();
}

PickDrawVisitor::~PickDrawVisitor() {
	
}

void PickDrawVisitor::visit(vwgl::Node *node) {
	doVisit(node);
	vwgl::GizmoManager::get()->drawGizmo(_camera.getPtr(),_material.getPtr());
}
	
void PickDrawVisitor::doVisit(vwgl::Node *node) {
	Group * gn = dynamic_cast<Group*>(node);
	TransformNode * tn = dynamic_cast<TransformNode*>(node);
	DrawableNode * dn = dynamic_cast<DrawableNode*>(node);
	
	State::get()->pushModelMatrix();
	if (tn) {
		State::get()->modelMatrix().mult(tn->getTransform());
	}
	if (gn) {
		NodeList::iterator it;
		for (it=gn->getNodeList().begin(); it!=gn->getNodeList().end(); ++it) {
			visit((*it).getPtr());
		}
	}
	if (dn && dn->getDrawable()) {
		SolidList solids = dn->getDrawable()->getSolidList();
		SolidList::iterator it;
		for (it=solids.begin(); it!=solids.end(); ++it) {
			if (!(*it)->getPickId().isZero() && (*it)->isVisible()) {
				State::get()->pushModelMatrix();
				State::get()->modelMatrix().mult((*it)->getTransform());
				_material->setColorPickId((*it)->getPickId());
				_material->bindPolyList((*it)->getPolyList());
				_material->activate();
				(*it)->getPolyList()->drawElements();
				_material->deactivate();
				State::get()->popModelMatrix();
			}
		}
	}
	State::get()->popModelMatrix();
}


}

