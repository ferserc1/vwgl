
#include <vwgl/specular_render_pass.hpp>
#include <vwgl/drawable.hpp>
#include <vwgl/texture_manager.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

SpecularRenderPassMaterial::SpecularRenderPassMaterial() {
	initShader();
}

SpecularRenderPassMaterial::~SpecularRenderPassMaterial() {
	
}

void SpecularRenderPassMaterial::initShader()  {
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_specular.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_specular.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(Shader::kTypeVertex, "def_specular.gl3.vsh");
		getShader()->loadAndAttachShader(Shader::kTypeFragment, "def_specular.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: specular deferred shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("def_specular");
	
	loadVertexAttrib("aVertexPosition");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uMVMatrix");
	getShader()->initUniformLocation("uPMatrix");
	
	getShader()->initUniformLocation("uSpecularColor");
	getShader()->initUniformLocation("uShininess");
	getShader()->initUniformLocation("uTexture");
	getShader()->initUniformLocation("uTextureOffset");
	getShader()->initUniformLocation("uTextureScale");
	getShader()->initUniformLocation("uAlphaCutoff");
	getShader()->initUniformLocation("uShininessMask");
	getShader()->initUniformLocation("uShininessMaskChannel");
	getShader()->initUniformLocation("uShininessMaskInvert");
}

void SpecularRenderPassMaterial::setupUniforms() {
	getShader()->setUniform("uMVMatrix", modelViewMatrix());
	getShader()->setUniform("uPMatrix", projectionMatrix());
	
	GenericMaterial * mat = getSettingsMaterial();
	if (mat) {
		getShader()->setUniform("uSpecularColor", mat->getSpecular());
		getShader()->setUniform("uShininess", mat->getShininess());

		Texture * tex = mat->getTexture() ? mat->getTexture() : TextureManager::get()->whiteTexture();
		Texture * shininessMask = mat->getShininessMask() ? mat->getShininessMask() : TextureManager::get()->whiteTexture();
		getShader()->setUniform("uTexture", tex, vwgl::Texture::kTexture0);
		getShader()->setUniform("uTextureOffset", mat->getTextureOffset());
		getShader()->setUniform("uTextureScale", mat->getTextureScale());
		getShader()->setUniform("uAlphaCutoff", mat->getAlphaCutoff());
		getShader()->setUniform("uShininessMask", shininessMask, vwgl::Texture::kTexture1);
		getShader()->setUniform("uShininessMaskChannel", mat->getShininessMaskChannel());
		getShader()->setUniform("uShininessMaskInvert", mat->getInvertShininessMask() ? 1.0f:0.0f);
	}
}

}