
#include <vwgl/translate_gizmo.hpp>
#include <vwgl/system.hpp>

#include <vwgl/physics/ray.hpp>
#include <vwgl/physics/plane.hpp>
#include <vwgl/physics/intersection.hpp>

namespace vwgl {
	

TranslateGizmo::TranslateGizmo() :Gizmo(System::get()->getResourcesPath() + "move_gizmo.vwglb") {
	loadPickIds();
	_resetRotation = true;
}

TranslateGizmo::TranslateGizmo(const std::string & model) :Gizmo(model) {
	loadPickIds();
	_resetRotation = true;
}

TranslateGizmo::~TranslateGizmo() {
	
}

void TranslateGizmo::beginMouseEvents(const Position2Di & pos, vwgl::Camera * camera, const Viewport & vp, const Vector3 & gizmoPos, TransformNode * trx) {
	_transformNode = trx;
	_previousPosition = pos;
	_camera = camera;
}

void TranslateGizmo::mouseEvent(const vwgl::Position2Di & pos) {
//	TRSTransformNode * trs = dynamic_cast<TRSTransformNode*>(_transformNode);
	TRSTransformStrategy * trs = _transformNode->getTransformStrategy<TRSTransformStrategy>();
	Vector3 right = _camera->getTransform().rightVector();
	Vector3 up = _camera->getTransform().upVector();

	vwgl::Vector3 offset;
	float offsetMultiplier = 0.01f;
	float offsetX = static_cast<float>(_previousPosition.x() - pos.x());
	float offsetY = static_cast<float>(_previousPosition.y() - pos.y());
	switch (_mode) {
		case kGizmoAxisX:
			offset.x(offsetX * right.x() * -offsetMultiplier + offsetY * right.z() * -offsetMultiplier);
			break;
		case kGizmoAxisY:
			offset.y(offsetY * up.y() * -offsetMultiplier);
			break;
		case kGizmoAxisZ:
			offset.z(offsetY * right.x() * offsetMultiplier + offsetX * right.z() * -offsetMultiplier);
			break;
		default:
			break;
	}
	if (trs) {
		trs->translate(offset.add(trs->getTranslate()));
	}
	else {
		_transformNode->getTransform().translate(offset.x(), offset.y(), offset.z());
	}
	_previousPosition = pos;
}

void TranslateGizmo::endMouseEvents() {
	
}

void TranslateGizmo::loadPickIds() {
	Solid * axisX = _drawable->getSolid("Material-xAxis");
	Solid * axisY = _drawable->getSolid("Material-yAxis");
	Solid * axisZ = _drawable->getSolid("Material-zAxis");
	
	_xAxisId = axisX->getPickId();
	_yAxisId = axisY->getPickId();
	_zAxisId = axisZ->getPickId();
}

}
