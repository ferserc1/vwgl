
#include <vwgl/shadow_light.hpp>
#include <vwgl/shadow_map_material.hpp>
#include <vwgl/fbo.hpp>
#include <vwgl/state.hpp>
#include <vwgl/drawable.hpp>

namespace vwgl {

ShadowLight::ShadowLight() :Light(), _castShadows(true), _shadowBias(0.00005f) {
	_transformVisitor = new TransformVisitor();
}

ShadowLight::~ShadowLight() {
	destroy();
}

void ShadowLight::updateShadowMap() {
	
}

void ShadowLight::destroy() {
	_transformVisitor = NULL;
}

Matrix4 & ShadowLight::getViewMatrix() {
	_transformVisitor->visit(this);
	_transformVisitor->getTransform().invert();
	return _transformVisitor->getTransform();
}

}
