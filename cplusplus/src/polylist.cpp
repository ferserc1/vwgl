
#include <vwgl/polylist.hpp>
#include <vwgl/matrix.hpp>
#include <vwgl/glerror.hpp>
#include <vwgl/graphics.hpp>

#include <iostream>

namespace vwgl {

PolyList::PolyList()
	:_name(""),
	_materialName(""),
	_vertexArrayObject(0),
	_vertexBuffer(-1),
	_normalBuffer(-1),
	_texCoord0Buffer(-1),
	_texCoord1Buffer(-1),
	_texCoord2Buffer(-1),
	_colorBuffer(-1),
	_indexBuffer(-1),
	_tangentBuffer(-1),
	_drawMode(kTriangles),
	_useVAO(false),
	_visible(true),
	_patchVertices(3),
	_patchMode(false)
{
	if (Graphics::get()->getApi()==Graphics::kApiOpenGLAdvanced) {
		_useVAO = true;
	}
}

PolyList::PolyList(const PolyList * clone)
	:_name(""),
	_materialName(""),
	_vertexArrayObject(0),
	_vertexBuffer(-1),
	_normalBuffer(-1),
	_texCoord0Buffer(-1),
	_texCoord1Buffer(-1),
	_texCoord2Buffer(-1),
	_colorBuffer(-1),
	_indexBuffer(-1),
	_tangentBuffer(-1),
	_drawMode(kTriangles),
	_useVAO(false),
	_visible(true),
	_patchVertices(3),
	_patchMode(false)
{
	if (Graphics::get()->getApi()==Graphics::kApiOpenGLAdvanced) {
		_useVAO = true;
	}
	
	FloatVector::const_iterator vit;
	FloatVector::const_iterator nit;
	FloatVector::const_iterator t0it;
	FloatVector::const_iterator t1it;
	FloatVector::const_iterator t2it;
	UIntVector::const_iterator iit;
	
	_name = clone->_name;
	_materialName = clone->_materialName;
	_drawMode = clone->_drawMode;
	
	for (vit=clone->_vertex.begin(); vit!=clone->_vertex.end(); ++vit) {
		_vertex.push_back(*vit);
	}
	for (nit=clone->_normal.begin(); nit!=clone->_normal.end(); ++nit) {
		_normal.push_back(*nit);
	}
	for (t0it=clone->_texCoord0.begin(); t0it!=clone->_texCoord0.end(); ++t0it) {
		_texCoord0.push_back(*t0it);
	}
	for (t1it=clone->_texCoord1.begin(); t1it!=clone->_texCoord1.end(); ++t1it) {
		_texCoord1.push_back(*t1it);
	}
	for (t2it=clone->_texCoord2.begin(); t2it!=clone->_texCoord2.end(); ++t2it) {
		_texCoord2.push_back(*t2it);
	}
	for (iit=clone->_index.begin(); iit!=clone->_index.end(); ++iit) {
		_index.push_back(*iit);
	}
	buildPolyList();
}

PolyList::~PolyList() {
	destroy();
}
	
void PolyList::addVertex(const Vector3 & v) {
	_vertex.push_back(v.x());
	_vertex.push_back(v.y());
	_vertex.push_back(v.z());
}
	
void PolyList::addNormal(const Vector3 & n) {
	_normal.push_back(n.x());
	_normal.push_back(n.y());
	_normal.push_back(n.z());
}

void PolyList::addTexCoord0(const Vector2 & tc) {
	_texCoord0.push_back(tc.x());
	_texCoord0.push_back(tc.y());
}

void PolyList::addTexCoord1(const Vector2 & tc) {
	_texCoord1.push_back(tc.x());
	_texCoord1.push_back(tc.y());
}

void PolyList::addTexCoord2(const Vector2 & tc) {
	_texCoord2.push_back(tc.x());
	_texCoord2.push_back(tc.y());
}
	
void PolyList::addColor(const Color &c) {
	_color.push_back(c.r());
	_color.push_back(c.g());
	_color.push_back(c.b());
	_color.push_back(c.a());
}

void PolyList::addIndex(unsigned int i) {
	_index.push_back(i);
}

void PolyList::addTriangle(unsigned int v0, unsigned int v1, unsigned int v2) {
	addIndex(v0); addIndex(v1); addIndex(v2);
}
	
void PolyList::addVertexVector(const float * v, int size) {
	for (int i=0; i<size; ++i) {
		_vertex.push_back(v[i]);
	}
}

void PolyList::addNormalVector(const float * v, int size) {
	for (int i=0; i<size; ++i) {
		_normal.push_back(v[i]);
	}
}

void PolyList::addTexCoord0Vector(const float * v, int size) {
	for (int i=0; i<size; ++i) {
		_texCoord0.push_back(v[i]);
	}
}

void PolyList::addTexCoord1Vector(const float * v, int size) {
	for (int i=0; i<size; ++i) {
		_texCoord1.push_back(v[i]);
	}
}

void PolyList::addTexCoord2Vector(const float * v, int size) {
	for (int i=0; i<size; ++i) {
		_texCoord2.push_back(v[i]);
	}
}
	
void PolyList::addColorVector(const float *v, int size) {
	for (int i=0; i<size; ++i) {
		_color.push_back(v[i]);
	}
}

void PolyList::addIndexVector(const unsigned int * v, int size) {
	for (int i=0; i<size; ++i) {
		_index.push_back(v[i]);
	}
}

void PolyList::switchUVs(VertexBufferType uv1, VertexBufferType uv2) {
	if (uv1==kVertex || uv1==kNormal || uv1==kTangent ||
		uv2==kVertex || uv2==kNormal || uv2==kTangent ) {
		std::cout << "Warning: invalid uv specified in switchUVs function. The specified buffer is not a vertex buffer" << std::endl;
	}
	else if ((uv1==kTexCoord0 || uv2==kTexCoord0) && _texCoord0.size()==0) {
		std::cout << "Warning: invalid uv. The specified buffer is null" << std::endl;
	}
	else if ((uv1==kTexCoord1 || uv2==kTexCoord1) && _texCoord1.size()==0) {
		std::cout << "Warning: invalid uv. The specified buffer is null" << std::endl;
	}
	else if ((uv1==kTexCoord2 || uv2==kTexCoord2) && _texCoord2.size()==0) {
		std::cout << "Warning: invalid uv. The specified buffer is null" << std::endl;
	}
	else {
		FloatVector & src = uv1==kTexCoord0 ? _texCoord0:(uv1==kTexCoord1 ? _texCoord1:_texCoord2);
		FloatVector & dst = uv2==kTexCoord0 ? _texCoord0:(uv2==kTexCoord1 ? _texCoord1:_texCoord2);
		FloatVector tmp = src;
		src = dst;
		dst = tmp;
		buildPolyList();
	}
}
	
void PolyList::buildPolyList() {
	destroyGlBuffers();
	if (_useVAO) {
		glGenVertexArrays(1, &_vertexArrayObject);
		glBindVertexArray(_vertexArrayObject);
	}
	if (_vertex.size()>0) {
		glGenBuffers(1, reinterpret_cast<GLuint*>(&_vertexBuffer));
		// Hack: In OS X, the VBO with index 1 make very strange things, like storing malformed meshes.
		// This hack fix this rare behaviour, but it is a little bit disturbing
		// It would be interesting to investigate why this is happening
		if (_vertexBuffer==1) {
			glGenBuffers(1, reinterpret_cast<GLuint*>(&_vertexBuffer));
		}
		glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _vertex.size(), &_vertex[0], GL_STATIC_DRAW);
	}
	
	if (_normal.size()>0) {
		glGenBuffers(1, reinterpret_cast<GLuint*>(&_normalBuffer));
		glBindBuffer(GL_ARRAY_BUFFER, _normalBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _normal.size(), &_normal[0], GL_STATIC_DRAW);
	}
	
	if (_texCoord0.size()>0) {
		glGenBuffers(1, reinterpret_cast<GLuint*>(&_texCoord0Buffer));
		glBindBuffer(GL_ARRAY_BUFFER, _texCoord0Buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _texCoord0.size(), &_texCoord0[0], GL_STATIC_DRAW);
	}
	
	// TODO: performance improvement. Fix the deferred render pipeline to avoid the need for having
	// a lightmap vbo
	if (_texCoord1.size()==0) {
		_texCoord1 = _texCoord0;
	}
	
	if (_texCoord1.size()>0) {
		glGenBuffers(1, reinterpret_cast<GLuint*>(&_texCoord1Buffer));
		glBindBuffer(GL_ARRAY_BUFFER, _texCoord1Buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _texCoord1.size(), &_texCoord1[0], GL_STATIC_DRAW);
	}
	
	if (_texCoord2.size()>0) {
		glGenBuffers(1, reinterpret_cast<GLuint*>(&_texCoord2Buffer));
		glBindBuffer(GL_ARRAY_BUFFER, _texCoord2Buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _texCoord2.size(), &_texCoord2[0], GL_STATIC_DRAW);
	}
	
	if (_color.size()>0) {
		glGenBuffers(1, reinterpret_cast<GLuint*>(&_colorBuffer));
		glBindBuffer(GL_ARRAY_BUFFER, _colorBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _color.size(), &_color[0], GL_STATIC_DRAW);
	}
	
	if (_index.size()>0) {
		glGenBuffers(1, reinterpret_cast<GLuint*>(&_indexBuffer));
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer);
		unsigned int *indexPtr = &_index[0];
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * _index.size(), indexPtr, GL_STATIC_DRAW);
	}
	calculateTangent();
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	if (_useVAO) {
		glBindVertexArray(0);
	}
}
	
void PolyList::drawElements() {
	if (_useVAO) {
		glBindVertexArray(_vertexArrayObject);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getIndexBuffer());
	
	if (_patchMode) {
		glPatchParameteri(GL_PATCH_VERTICES, _patchVertices);
		glDrawElements(GL_PATCHES, (GLsizei)numberOfIndices(), GL_UNSIGNED_INT, 0);
	}
	else {
		glDrawElements(_drawMode, (GLsizei)numberOfIndices(), GL_UNSIGNED_INT, 0);
	}
	
	if (_useVAO ) {
		glBindVertexArray(0);
	}
}

void PolyList::destroyGlBuffers() {
	if (_useVAO) {
		glBindVertexArray(0);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	if (_vertexBuffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_vertexBuffer));
		_vertexBuffer = -1;
	}
	if (_normalBuffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_normalBuffer));
		_normalBuffer = -1;
	}
	if (_texCoord0Buffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_texCoord0Buffer));
		_texCoord0Buffer = -1;
	}
	if (_texCoord1Buffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_texCoord1Buffer));
		_texCoord1Buffer = -1;
	}
	if (_texCoord2Buffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_texCoord2Buffer));
		_texCoord2Buffer = -1;
	}
	if (_colorBuffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_colorBuffer));
		_colorBuffer = -1;
	}
	if (_indexBuffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_indexBuffer));
		_indexBuffer = -1;
	}
	if (_tangentBuffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_tangentBuffer));
		_tangentBuffer = -1;
	}
	if (_useVAO) {
		glDeleteVertexArrays(1, &_vertexArrayObject);
	}
}
	
void PolyList::destroy() {
	destroyGlBuffers();
	_vertex.clear();
	_normal.clear();
	_texCoord0.clear();
	_texCoord1.clear();
	_texCoord2.clear();
	_color.clear();
	_index.clear();
	_tangent.clear();
}

void PolyList::applyTransform(const vwgl::Matrix4 & trx) {
	vwgl::Matrix4 transform = trx;
	vwgl::Matrix4 rotation = trx.getMatrix3();
	if (_normal.size()>0 && _normal.size()!=_vertex.size()) throw MalformedPolyListException("Unexpected number of normal coordinates found in polyList");
	for (int i=0;i<_vertex.size()-2;i+=3) {
		vwgl::Vector4 vertex = vwgl::Vector4(_vertex[i],_vertex[i+1], _vertex[i+2], 1.0f);
		vertex = transform.multVector(vertex);
		_vertex[i] = vertex.x();
		_vertex[i+1] = vertex.y();
		_vertex[i+2] = vertex.z();
		
		if (_normal.size()>0) {
			vwgl::Vector4 normal = vwgl::Vector4(_normal[i],_normal[i+1], _normal[i+2], 1.0f);
			normal = rotation.multVector(normal);
			normal.normalize();
			_normal[i] = normal.x();
			_normal[i+1] = normal.y();
			_normal[i+2] = normal.z();
		}
	}
	buildPolyList();
}

void PolyList::flipFaces() {
	for (int i=0; (i+2)<_index.size(); i+=3) {
		int a = _index[i];
		int c = _index[i+2];
		_index[i] = c;
		_index[i+2] = a;
	}
	buildPolyList();
}
	
void PolyList::flipNormals() {
	for (int i=0; i<_normal.size(); i++) {
		_normal[i] = -_normal[i];
	}
	buildPolyList();
}

void PolyList::calculateTangent() {
	_tangent.clear();
	if (_tangentBuffer>=0) {
		glDeleteBuffers(1, reinterpret_cast<GLuint*>(&_tangentBuffer));
		_tangentBuffer = -1;
	}
	
	if (_texCoord0.size()==0 || _vertex.size()==0) return;
	if (_index.size()%3==0) {
	for (int i=0; i<_index.size() - 2; i+=3) {
		int v0i = _index[i] * 3;
		int v1i = _index[i + 1] * 3;
		int v2i = _index[i + 2] * 3;
		
		int t0i = _index[i] * 2;
		int t1i = _index[i + 1] * 2;
		int t2i = _index[i + 2] * 2;
		
		Vector3 v0(_vertex[v0i], _vertex[v0i + 1], _vertex[v0i + 2]);
		Vector3 v1(_vertex[v1i], _vertex[v1i + 1], _vertex[v1i + 2]);
		Vector3 v2(_vertex[v2i], _vertex[v2i + 1], _vertex[v2i + 2]);
		
		Vector2 t0(_texCoord0[t0i], _texCoord0[t0i + 1]);
		Vector2 t1(_texCoord0[t1i], _texCoord0[t1i + 1]);
		Vector2 t2(_texCoord0[t2i], _texCoord0[t2i + 1]);
		
		Vector3 edge1(v1);
		edge1.sub(v0);
		Vector3 edge2(v2);
		edge2.sub(v0);
		
		float deltaU1 = t1.x() - t0.x();
		float deltaV1 = t1.y() - t0.y();
		float deltaU2 = t2.x() - t0.x();
		float deltaV2 = t2.y() - t0.y();
		
		float f = 1.0f / (deltaU1 * deltaV2 - deltaU2 * deltaV1);
		
		Vector3 tangent(f * (deltaV2 * edge1.x() - deltaV1 * edge2.x()),
						f * (deltaV2 * edge1.y() - deltaV1 * edge2.y()),
						f * (deltaV2 * edge1.z() - deltaV1 * edge2.z()));
		tangent.normalize();
		
		_tangent.push_back(tangent.x());
		_tangent.push_back(tangent.y());
		_tangent.push_back(tangent.z());
		
		_tangent.push_back(tangent.x());
		_tangent.push_back(tangent.y());
		_tangent.push_back(tangent.z());
		
		_tangent.push_back(tangent.x());
		_tangent.push_back(tangent.y());
		_tangent.push_back(tangent.z());
	}
	}
	else {
		for (int i=0; i<_index.size() - 3; i+=4) {
		int v0i = _index[i] * 3;
		int v1i = _index[i + 1] * 3;
		int v2i = _index[i + 2] * 3;
		
		int t0i = _index[i] * 2;
		int t1i = _index[i + 1] * 2;
		int t2i = _index[i + 2] * 2;
		
		Vector3 v0(_vertex[v0i], _vertex[v0i + 1], _vertex[v0i + 2]);
		Vector3 v1(_vertex[v1i], _vertex[v1i + 1], _vertex[v1i + 2]);
		Vector3 v2(_vertex[v2i], _vertex[v2i + 1], _vertex[v2i + 2]);
		
		Vector2 t0(_texCoord0[t0i], _texCoord0[t0i + 1]);
		Vector2 t1(_texCoord0[t1i], _texCoord0[t1i + 1]);
		Vector2 t2(_texCoord0[t2i], _texCoord0[t2i + 1]);
		
		Vector3 edge1(v1);
		edge1.sub(v0);
		Vector3 edge2(v2);
		edge2.sub(v0);
		
		float deltaU1 = t1.x() - t0.x();
		float deltaV1 = t1.y() - t0.y();
		float deltaU2 = t2.x() - t0.x();
		float deltaV2 = t2.y() - t0.y();
		
		float f = 1.0f / (deltaU1 * deltaV2 - deltaU2 * deltaV1);
		
		Vector3 tangent(f * (deltaV2 * edge1.x() - deltaV1 * edge2.x()),
						f * (deltaV2 * edge1.y() - deltaV1 * edge2.y()),
						f * (deltaV2 * edge1.z() - deltaV1 * edge2.z()));
		tangent.normalize();
		
		_tangent.push_back(tangent.x());
		_tangent.push_back(tangent.y());
		_tangent.push_back(tangent.z());
		
		_tangent.push_back(tangent.x());
		_tangent.push_back(tangent.y());
		_tangent.push_back(tangent.z());
		
		_tangent.push_back(tangent.x());
		_tangent.push_back(tangent.y());
		_tangent.push_back(tangent.z());

		_tangent.push_back(tangent.x());
		_tangent.push_back(tangent.y());
		_tangent.push_back(tangent.z());
	}
	}
	
	if (_tangent.size()>0) {
		glGenBuffers(1, reinterpret_cast<GLuint*>(&_tangentBuffer));
		glBindBuffer(GL_ARRAY_BUFFER, _tangentBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _tangent.size(), &_tangent[0], GL_STATIC_DRAW);
	}
}

}