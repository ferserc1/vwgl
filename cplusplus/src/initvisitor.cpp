
#include <vwgl/initvisitor.hpp>
#include <vwgl/group.hpp>
namespace vwgl {

InitVisitor::InitVisitor() {
	
}
	
InitVisitor::~InitVisitor() {
	_initNodes.clear();
}

void InitVisitor::visit(vwgl::Node *node) {
	_initNodes.clear();
	doVisit(node);
	init();
}

void InitVisitor::doVisit(vwgl::Node *node) {
	Group * grp = dynamic_cast<Group*>(node);
	
	if (grp) {
		NodeList::iterator it;
		NodeList & list = grp->getNodeList();
		for (it=list.begin(); it!=list.end(); ++it) {
			doVisit((*it).getPtr());
		}
	}
	if (!node->isInitialized()) {
		_initNodes.push_back(node);
	}
}

void InitVisitor::init() {
	std::vector<Node*>::iterator it;
	for (it=_initNodes.begin(); it!=_initNodes.end(); ++it) {
		if (!(*it)->isInitialized()) {
			(*it)->init();
			(*it)->setInitialized(true);
		}
	}
}

}