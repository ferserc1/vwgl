

#include <vwgl/deferredmixmaterial.hpp>
#include <vwgl/graphics.hpp>

namespace vwgl {

DeferredPostprocess::DeferredPostprocess()
	:_targetDistance(0.0f),
	_dofAmount(1.0f),
	_ssaoBlur(4),
	_bloomAmount(100)
	//,_brightness(0.5f)
	//,_contrast(0.5f)
	//,_hue(1.0f)
	//,_saturation(1.0f)
	//,_lightness(1.0f)
{
	initShader();
}

DeferredPostprocess::~DeferredPostprocess() {
	
}

void DeferredPostprocess::initShader() {
	if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGL) {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "deferred_mix.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "deferred_mix.fsh");
	}
	else if (Graphics::get()->getApi() == vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeVertex, "deferred_mix.gl3.vsh");
		getShader()->loadAndAttachShader(vwgl::Shader::kTypeFragment, "deferred_mix.gl3.fsh");
		getShader()->setOutputParameterName(Shader::kOutTypeFragmentDataLocation, "out_FragColor");
	}
	else {
		std::cerr << "Warning: deferred mix shader does not support the current API. (compatible APIs are kApiOpenGL or kApiOpenGLAdvanced)" << std::endl;
	}
	getShader()->link("deferred_postprocess");
	
	loadVertexAttrib("aVertexPos");
	loadTexCoord0Attrib("aTexturePosition");
	
	getShader()->initUniformLocation("uDiffuseMap");
	getShader()->initUniformLocation("uTranslucentMap");
	getShader()->initUniformLocation("uPositionMap");
	getShader()->initUniformLocation("uSelectionMap");
	getShader()->initUniformLocation("uSSAOMap");
	
	getShader()->initUniformLocation("uSize");
	getShader()->initUniformLocation("uBlurKernel");
	getShader()->initUniformLocation("uSelectionKernel");
	getShader()->initUniformLocation("uTargetDistance");
	getShader()->initUniformLocation("uDOFAmount");
	getShader()->initUniformLocation("uSSAOBlur");
	getShader()->initUniformLocation("uBloomMap");
	
	getShader()->initUniformLocation("uBrightness");
	getShader()->initUniformLocation("uContrast");
	getShader()->initUniformLocation("uHue");
	getShader()->initUniformLocation("uSaturation");
	getShader()->initUniformLocation("uLightness");
	
	getShader()->initUniformLocation("uClearColor");
	
	getShader()->initUniformLocation("uBloomAmount");
	
	if (Graphics::get()->getApi()==vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->initUniformLocation("uAntialias");
	}
}
	
void DeferredPostprocess::setupUniforms() {
	getShader()->setUniform("uDiffuseMap",_diffuseMap.getPtr(),vwgl::Texture::kTexture0);
	getShader()->setUniform("uTranslucentMap", _translucentMap.getPtr(),vwgl::Texture::kTexture1);
	getShader()->setUniform("uPositionMap",_positionMap.getPtr(),vwgl::Texture::kTexture3);
	getShader()->setUniform("uSelectionMap", _selectionMap.getPtr(),vwgl::Texture::kTexture4);
	getShader()->setUniform("uSSAOMap", _ssaoMap.getPtr(),vwgl::Texture::kTexture5);
	getShader()->setUniform("uBloomMap", _bloomMap.getPtr(),vwgl::Texture::kTexture6);
	
	getShader()->setUniform("uSize",Vector2(2048));
	float blurKernel[] = {
					0.045f, 0.122f, 0.045f,
					0.122f, 0.332f, 0.122f,
					0.045f, 0.122f, 0.045f
	};
	float selectionKernel[] = {
					0.0f, 1.0f, 0.0f,
					1.0f,-4.0f, 1.0f,
					0.0f, 1.0f, 0.0f
	};
	getShader()->setUniform1v("uBlurKernel",9,blurKernel);
	getShader()->setUniform1v("uSelectionKernel",9,selectionKernel);
	getShader()->setUniform("uTargetDistance",_targetDistance);
	getShader()->setUniform("uDOFAmount", _dofAmount);
	getShader()->setUniform("uBrightness",_renderSettings.getBrightness());
	getShader()->setUniform("uContrast",_renderSettings.getContrast());
	getShader()->setUniform("uHue", _renderSettings.getHue());
	getShader()->setUniform("uSaturation", _renderSettings.getSaturation());
	getShader()->setUniform("uLightness", _renderSettings.getLightness());
	getShader()->setUniform("uClearColor", _clearColor);
	getShader()->setUniform("uSSAOBlur", _ssaoBlur);
	getShader()->setUniform("uBloomAmount", _bloomAmount);
	
	if (Graphics::get()->getApi()==vwgl::Graphics::kApiOpenGLAdvanced) {
		getShader()->setUniform("uAntialias", _renderSettings.isAntialiasingEnabled());
	}
}

}
