
#include <vwgl/opengl.hpp>

#include <vwgl/system.hpp>

#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>

extern "C"
{
#include <uuid/uuid.h>
}

#define VWGL_MAX_PATH 512

namespace vwgl {

const std::string & System::getExecutablePath() {
  if (_executablePath=="") {
    char exec_path[VWGL_MAX_PATH];
    readlink("/proc/self/exe", exec_path, VWGL_MAX_PATH);
    _executablePath = exec_path;
    // "/proc/self/exe" returns the executable path, including the executable file.
    // This will remove the executable file name
    _executablePath = getPath(_executablePath);
  }
  return _executablePath;
}

const std::string & System::getDefaultShaderPath() {
  if (_defaultShaderPath=="") {
    _defaultShaderPath = getExecutablePath();
    _defaultShaderPath = addPathComponent(_defaultShaderPath, "shaders");
  }
  return _defaultShaderPath;
}

const std::string & System::getResourcesPath() {
  if (_resourcesPath=="") {
    _resourcesPath = getExecutablePath();
    _resourcesPath = addPathComponent(_resourcesPath, "resources");
  }
  return _resourcesPath;
}

const std::string & System::getTempPath() {
  if (_tempPath=="") {
    char tmp_path[L_tmpnam];
    tmpnam(tmp_path);
    _tempPath = tmp_path;
    mkdir(_tempPath);
  }
  return _tempPath;
}

bool System::directoryExists(const std::string & path) {
  struct stat sb;

  if (stat(path.c_str(), &sb)==0 && S_ISDIR(sb.st_mode)) {
    return true;
  }

  return false;
}

bool System::fileExists(const std::string &path) {
  struct stat sb;

  if (stat(path.c_str(), &sb)==0) {
    if (S_ISDIR(sb.st_mode)) {
      return false;
    }
    else {
      return true;
    }
  }
  return false;
}

bool System::mkdir(const std::string & dir) {
  if (directoryExists(dir)) {
    return false;
  }
  return ::mkdir(dir.c_str(), 0755) == 0;
}

bool System::cp(const std::string & src, const std::string & dst, bool overwrite) {
  if (!fileExists(src)) {
    return false;
  }
  if (!overwrite && fileExists(dst)) {
    return false;
  }
  std::ifstream f1(src, std::fstream::binary);
  std::ofstream f2(dst, std::fstream::trunc|std::fstream::binary);
  f2 << f1.rdbuf();
  return true;
}

bool System::removeDirectory(const std::string & path) {
  if (directoryExists(path)) {
    return rmdir(path.c_str()) == 0;
  }
  return false;
}

bool System::removeFile(const std::string & path) {
  if (fileExists(path)) {
    return remove(path.c_str()) == 0;
  }
  return false;
}

void System::listDirectory(const std::string & path, std::vector<std::string> & files, std::vector<std::string> & directories, bool clearVectors) {
  if (clearVectors) {
    files.clear();
    directories.clear();
  }
  DIR * dp;
  struct dirent *dirp;
  struct stat entryStat;
  if ((dp=opendir(path.c_str()))!=nullptr) {
    while ((dirp=readdir(dp))!=nullptr) {
      std::string name = std::string(dirp->d_name);
      if (name=="." || name=="..") {
        // ignore
      }
      else if (stat(dirp->d_name, &entryStat)==0) {
        if (S_ISDIR(entryStat.st_mode)) {
          directories.push_back(std::string(dirp->d_name));
        }
        else {
          files.push_back(std::string(dirp->d_name));
        }
      }
    }
    closedir(dp);
  }
}

std::string System::getUuid() {
  uuid_t uuid;
  uuid_generate_random(uuid);
  char s[37];
  uuid_unparse(uuid, s);
  return s;
}

}
