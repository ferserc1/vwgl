
#include <vwgl/app/gdl_window.hpp>
#include <vwgl/app/gdl_context.hpp>
#include <vwgl/app/window_controller.hpp>

#include "linux_common.hpp"

GLWindow * create_window(const std::string title, const vwgl::Rect & rect, bool fullscreen) {
	GLWindow * windowData = new GLWindow;

	int width = rect.width();
	int height = rect.height();

	XVisualInfo *vi;
    Colormap cmap;
    int dpyWidth, dpyHeight;
    int i;
    int glxMajorVersion, glxMinorVersion;
    int vidModeMajorVersion, vidModeMinorVersion;
    XF86VidModeModeInfo **modes;
    int modeNum;
    int bestMode;
    Atom wmDelete;
    Window winDummy;
    unsigned int borderDummy;
    
    windowData->fs = fullscreen;
    /* set best mode to current */
    bestMode = 0;
    /* get a connection */
    windowData->dpy = XOpenDisplay(0);
    windowData->screen = DefaultScreen(windowData->dpy);
    XF86VidModeQueryVersion(windowData->dpy, &vidModeMajorVersion,&vidModeMinorVersion);
    std::cout << "XF86VidModeExtension-Version " << vidModeMajorVersion << "." << vidModeMinorVersion << std::endl;
    XF86VidModeGetAllModeLines(windowData->dpy, windowData->screen, &modeNum, &modes);
    /* save desktop-resolution before switching modes */
    windowData->deskMode = *modes[0];
    /* look for mode with requested resolution */
    for (i = 0; i < modeNum; i++) {
            if ((modes[i]->hdisplay == width) && (modes[i]->vdisplay == height)) {
            bestMode = i;
        }
    }
    /* get an appropriate visual */
    vi = glXChooseVisual(windowData->dpy, windowData->screen, attrListDbl);
    if (vi == NULL) {
        vi = glXChooseVisual(windowData->dpy, windowData->screen, attrListSgl);
        windowData->doubleBuffered = False;
        std::cout << "Only Singlebuffered Visual" << std::endl;
    }
    else {
        windowData->doubleBuffered = True;
    }
    glXQueryVersion(windowData->dpy, &glxMajorVersion, &glxMinorVersion);
    std::cout << "glX-Version " << glxMajorVersion << "." << glxMinorVersion << std::endl;
    /* create a GLX context */
    windowData->ctx = glXCreateContext(windowData->dpy, vi, 0, GL_TRUE);
    /* create a color map */
    cmap = XCreateColormap(windowData->dpy, RootWindow(windowData->dpy, vi->screen),
        vi->visual, AllocNone);
    windowData->attr.colormap = cmap;
    windowData->attr.border_pixel = 0;

    if (windowData->fs) {
        XF86VidModeSwitchToMode(windowData->dpy, windowData->screen, modes[bestMode]);
        XF86VidModeSetViewPort(windowData->dpy, windowData->screen, 0, 0);
        dpyWidth = modes[bestMode]->hdisplay;
        dpyHeight = modes[bestMode]->vdisplay;
        std::cout << "Resolution " << dpyWidth << "x" << dpyHeight << std::endl;
        XFree(modes);
    
        /* create a fullscreen window */
        windowData->attr.override_redirect = True;
        windowData->attr.event_mask = ExposureMask | KeyPressMask | ButtonPressMask |
            StructureNotifyMask;
        windowData->win = XCreateWindow(windowData->dpy, RootWindow(windowData->dpy, vi->screen),
            0, 0, dpyWidth, dpyHeight, 0, vi->depth, InputOutput, vi->visual,
            CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect,
            &windowData->attr);
        XWarpPointer(windowData->dpy, None, windowData->win, 0, 0, 0, 0, 0, 0);
		XMapRaised(windowData->dpy, windowData->win);
        XGrabKeyboard(windowData->dpy, windowData->win, True, GrabModeAsync,
            GrabModeAsync, CurrentTime);
        XGrabPointer(windowData->dpy, windowData->win, True, ButtonPressMask,
            GrabModeAsync, GrabModeAsync, windowData->win, None, CurrentTime);
    }
    else {
        /* create a window in window mode*/
        windowData->attr.event_mask = ExposureMask | KeyPressMask | ButtonPressMask |
            StructureNotifyMask;
        windowData->win = XCreateWindow(windowData->dpy, RootWindow(windowData->dpy, vi->screen),
            0, 0, width, height, 0, vi->depth, InputOutput, vi->visual,
            CWBorderPixel | CWColormap | CWEventMask, &windowData->attr);
        /* only set window title and handle wm_delete_events if in windowed mode */
        wmDelete = XInternAtom(windowData->dpy, "WM_DELETE_WINDOW", True);
        XSetWMProtocols(windowData->dpy, windowData->win, &wmDelete, 1);
        XSetStandardProperties(windowData->dpy, windowData->win, title.c_str(),
            title.c_str(), None, NULL, 0, NULL);
        XMapRaised(windowData->dpy, windowData->win);
    }       
    /* connect the glx-context to the window */
    glXMakeCurrent(windowData->dpy, windowData->win, windowData->ctx);
    XGetGeometry(windowData->dpy, windowData->win, &winDummy, &windowData->x, &windowData->y,
        &windowData->width, &windowData->height, &borderDummy, &windowData->depth);

    if (!glXIsDirect(windowData->dpy, windowData->ctx)) {
    	std::cout << "Sorry, no Direct Rendering possible" << std::endl;
    }

    return windowData;
}

namespace vwgl {
namespace app {

bool GdlWindow::create() {
	GLWindow * windowData = create_window(_title, _rect, _fullscreen);
	_windowData = windowData;

    _context = new GdlGLContext(this);
    _context->createContext();

    _rect.x(windowData->x);
  	_rect.y(windowData->y);

 //   initGL();
	return true;
}

void GdlWindow::destroy() {
	GLWindow * windowData = native_cast<GLWindow*>(_windowData);
	if (windowData->ctx) {
		if (!glXMakeCurrent(windowData->dpy, None, NULL)) {
			std::cout << "Could not release drawing context" << std::endl;
        }
        glXDestroyContext(windowData->dpy, windowData->ctx);
        windowData->ctx = NULL;
    }
    /* switch back to original desktop resolution if we were in fs */
    if (windowData->fs)
    {
        XF86VidModeSwitchToMode(windowData->dpy, windowData->screen, &windowData->deskMode);
        XF86VidModeSetViewPort(windowData->dpy, windowData->screen, 0, 0);
    }
    XCloseDisplay(windowData->dpy);
	
	delete windowData;
	windowData = nullptr;
}

}
}
