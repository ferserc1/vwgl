#include <vwgl/app/gdl_window.hpp>
#include <vwgl/app/gdl_context.hpp>
#include <vwgl/app/window_controller.hpp>

#include "linux_common.hpp"


namespace vwgl {
namespace app {

bool GdlGLContext::createContext() {
	GdlWindow * window = dynamic_cast<GdlWindow*>(_window);
	if (window) {
		GLWindow * windowData = native_cast<GLWindow*>(window->windowDataPtr());
		if (windowData) {
			_nativeContext = windowData;
		}
	}
	
	return _nativeContext!=nullptr;
}

void GdlGLContext::makeCurrent() {
	GLWindow * winData = native_cast<GLWindow*>(_nativeContext);
	if (winData) {
		glXMakeCurrent(winData->dpy, winData->win, winData->ctx);
	}
}

void GdlGLContext::swapBuffers() {
	GLWindow * winData = native_cast<GLWindow*>(_nativeContext);
	if (winData) {
		glXSwapBuffers(winData->dpy, winData->win);
	}	
}

void GdlGLContext::destroy() {
	_nativeContext = nullptr;
}

}
}
