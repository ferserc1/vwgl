#include <vwgl/app/win32_window.hpp>
#include <vwgl/app/win32_gl_context.hpp>
#include <vwgl/app/window_controller.hpp>

namespace vwgl {
namespace app {

bool Win32GLContext::createContext() {
  return false;
}

void Win32GLContext::makeCurrent() {

}

void Win32GLContext::swapBuffers() {

}

void Win32GLContext::destroy() {

}

}
}
