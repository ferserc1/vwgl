
#include <vwgl/app/cocoa_gl_context.hpp>
#include <vwgl/app/window_controller.hpp>

namespace vwgl {
namespace app {

bool CocoaGLContext::createContext() {
    return false;
}

void CocoaGLContext::makeCurrent() {
}

void CocoaGLContext::swapBuffers() {
}

void CocoaGLContext::destroy() {
}

}
}
