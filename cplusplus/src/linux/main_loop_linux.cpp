
#include <vwgl/app/main_loop.hpp>
#include <vwgl/app/window_controller.hpp>

namespace vwgl {
namespace app {

class MainLoopLinuxPrivate {
public:
	static MainLoopLinuxPrivate * get() {
		if (s_singleton==nullptr) {
			s_singleton = new MainLoopLinuxPrivate();
		}
		return s_singleton;
	}

	inline void run() { _running = true; }
	inline bool isRunning() const { return _running; }
	inline void quit() { _running = false; }
	inline void setReturnCode(int code) { _returnCode = code; }
	inline int getReturnCode() const { return _returnCode; }

protected:
	MainLoopLinuxPrivate() :_running(false), _returnCode(0) {}
	~MainLoopLinuxPrivate() {}

	static MainLoopLinuxPrivate * s_singleton;

	bool _running;
	int _returnCode;
};

MainLoopLinuxPrivate * MainLoopLinuxPrivate::s_singleton = nullptr;

int MainLoop::run() {
	MainLoopLinuxPrivate::get()->run();
	std::cout << MainLoopLinuxPrivate::get()->isRunning() << std::endl;

	while (MainLoopLinuxPrivate::get()->isRunning()) {
		std::cout << "Run" << std::endl;
	}
	return MainLoopLinuxPrivate::get()->getReturnCode();
}

void MainLoop::quit(int code) {
	MainLoopLinuxPrivate::get()->setReturnCode(code);
	MainLoopLinuxPrivate::get()->quit();
}

}
}
