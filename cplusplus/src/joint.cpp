
#include <vwgl/physics/joint.hpp>

#include <vwgl/drawable.hpp>
#include <vwgl/sphere.hpp>

#include <vwgl/loader.hpp>
#include <vwgl/system.hpp>

namespace vwgl {
namespace physics {

Joint::Joint() {
	
}
	
Joint::~Joint() {
}

LinkJoint::LinkJoint() :_transform { Matrix4::makeIdentity() } {
}
	
LinkJoint::~LinkJoint() {
}
	
void LinkJoint::applyTransform(Matrix4 & modelview) {
	modelview.mult(_transform);
}

void LinkJoint::initDrawable() {
	std::string resources = vwgl::System::get()->getResourcesPath();
	_drawable = vwgl::Loader::get()->loadDrawable(resources + "generic_joint.vwglb");
	if (!_drawable.valid()) {
		_drawable = new vwgl::Sphere(0.1f);
	}
}

void LinkJoint::calculateTransform() {
	_transform.identity()
		.translate(_offset.x(), _offset.y(), _offset.z())
		.rotate(_eulerRotation.z(), 0.0f, 0.0f, 1.0f)
		.rotate(_eulerRotation.y(), 0.0f, 1.0f, 0.0f)
		.rotate(_eulerRotation.x(), 1.0f, 0.0f, 0.0f);
}

}
}