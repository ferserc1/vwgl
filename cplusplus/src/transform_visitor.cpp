
#include <vwgl/transform_visitor.hpp>
#include <vwgl/group.hpp>
#include <vwgl/transform_node.hpp>

namespace vwgl {

TransformVisitor::TransformVisitor() {
	
}

TransformVisitor::~TransformVisitor() {
	
}

void TransformVisitor::visit(Node *node) {
	TransformNode * trn = dynamic_cast<TransformNode*>(node);
	Group * parent = node->getParent();

	if (parent) {
		visit(parent);
	}
	else {
		_transform.identity();
	}

	if (trn) {
		_transform.mult(trn->getTransform());
	}
}

}
