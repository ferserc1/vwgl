
#include <vwgl/library_writter.hpp>
#include <vwgl/library/library.hpp>
#include <vwgl/loader.hpp>
#include <vwgl/system.hpp>
#include <vector>

#include <fstream>

namespace vwgl {

LibraryWritter::LibraryWritter() {
	
}

LibraryWritter::~LibraryWritter() {
	
}

bool LibraryWritter::acceptFileType(const std::string &path) {
	std::string ext;
	getFileExtension(path, ext);
	toLower(ext);
	return ext=="vwlib" || ext=="json";
}

bool LibraryWritter::writeLibrary(const std::string &path, library::Node *node) {
	_file.open(path);
	if (_file.is_open()) {
		_sourcePath = node->getPath();
		_dstFileName = path;
		_dstPath = _dstFileName.substr(0,_dstFileName.find_last_of("\\/") + 1);
		_folderName = _dstFileName.substr(_dstFileName.find_last_of("\\/") + 1);
		_folderName = _folderName.substr(0,_folderName.find_last_of("."));
		_dstFolder = _dstPath + _folderName + '/';
		if (System::get()->fileExists(_dstFolder) && !System::get()->directoryExists(_dstFolder)) {
			throw new vwgl::FileException("Could not create the library resource folder. File exists at resource folder path.");
		}
		if (!System::get()->directoryExists(_dstFolder) && !System::get()->mkdir(_dstFolder)) {
			throw new vwgl::FileException("Could not create library resource folder");
		}
		writeFile(node);
		_file.close();
		return true;
	}
	else {
		return false;
	}
}

void LibraryWritter::writeFile(library::Node * node) {
	library::AssertLibraryIdentifiersVisitor assertIdVisitor;
	assertIdVisitor.visit(node);

	_file << "{\n";
	writeProp(1,"fileType",std::string("vwgl::library"));
	writeProp(1,"version",std::string("1.1"));
	writeProp(1,"root");
	_file << "[\n";

	vwgl::library::Group * grp = dynamic_cast<vwgl::library::Group*>(node);
	if (grp) {
		library::NodeList & list = grp->getNodeList();
		writeNodeArray(2,list);
	}
	else {
		writeNode(2,node);
	}

	_file << "\n\t]\n";
	_file << "}\n";
}

void LibraryWritter::writeProp(int tabs, const std::string & key) {
	writeTabs(tabs);
	_file << "\"" << key << "\":";
}

void LibraryWritter::writeProp(int tabs, const std::string & key, const std::string & value, const std::string & separator) {
	writeTabs(tabs);
	_file << "\"" << key << "\":\"" << value << "\"" << separator;
}

void LibraryWritter::writeProp(int tabs, const std::string & key, int value, const std::string & separator) {
	writeTabs(tabs);
	_file << "\"" << key << "\":" << value << separator;
}

void LibraryWritter::writeProp(int tabs, const std::string & key, float value, const std::string & separator) {
	writeTabs(tabs);
	_file << "\"" << key << "\":" << value << separator;
}

void LibraryWritter::writeProp(int tabs, const std::string & key, bool value, const std::string & separator) {
	writeTabs(tabs);
	_file << "\"" << key << "\":" << value << separator;
}

void LibraryWritter::writeNodeArray(int tabs, library::NodeList & nodeList) {
	library::NodeList::iterator it;
	std::string separator = "";
	for (it=nodeList.begin(); it!=nodeList.end();++it) {
		_file << separator;
		writeNode(tabs,(*it).getPtr());
		separator = ",\n";
	}
}

void LibraryWritter::writeNode(int tabs, library::Node * node) {
	writeTabs(tabs);
	_file << "{\n";
	library::Group * grp = dynamic_cast<library::Group*>(node);
	library::Material * mat = dynamic_cast<library::Material*>(node);
	library::Model * mod = dynamic_cast<library::Model*>(node);

	if (grp) writeProp(tabs + 1, "type",std::string("group"));
	else if (mat) writeProp(tabs + 1, "type",std::string("material"));
	else if (mod) writeProp(tabs + 1, "type",std::string("model"));

	writeProp(tabs + 1, "id", node->getId());
	writeProp(tabs + 1, "name", node->getName());
	std::string fileName;
	extractFileName(node->getIcon(),fileName);
	writeProp(tabs + 1, "icon", fileName);

	if (mod) {
		std::string newFilePath = vwgl::System::get()->getLastPathComponent(mod->getFile());
		std::string subpath = vwgl::System::get()->removeExtension(newFilePath);
		newFilePath = vwgl::System::get()->addPathComponent(subpath, newFilePath);
		writeProp(tabs + 1, "file",newFilePath);
	}
	if (mat) {
		writeProp(tabs + 1, "materialType",mat->getMaterialType());
		writeMaterialModifier(tabs + 1, mat->getMaterialModifier());
		_file << ",\n";
	}

	writeDictionary(tabs + 1, "metadata", node->getMetadata());

	if (grp) {
		_file << ",\n";
		writeProp(tabs + 1, "children");
		_file << "[\n";

		writeNodeArray(tabs + 2, grp->getNodeList());

		_file << "\n";
		writeTabs(tabs + 1);
		_file << "]\n";
	}
	else {
		_file << "\n";
	}

	writeTabs(tabs);
	_file << "}";

	copyResources(node);
}

void LibraryWritter::writeTabs(int tabs) {
	for (int i=0;i<tabs;++i) _file << "\t";
}

void LibraryWritter::writeDictionary(int tabs, const std::string & name, library::Dictionary & dict) {
	writeProp(tabs, std::string(name));
	_file << "{\n";
	library::Dictionary::iterator it;
	library::Dictionary::iterator last = dict.end();
	if (last!=dict.begin()) --last;
	std::string separator = ",\n";
	for (it=dict.begin(); it!=dict.end(); ++it) {
		if (it==last) separator = "\n";
		writeProp(tabs + 1, (*it).first, (*it).second,separator);
	}
	writeTabs(tabs);
	_file << "}";
}

void LibraryWritter::writeMaterialModifier(int tabs, MaterialModifier & mod) {
	writeProp(tabs, "materialModifier");
	_file << "{\n";
	std::string separator = "";
	std::string file;
	if (mod.isEnabled(MaterialModifier::kDiffuse)) {
		writeProp(tabs + 1, "diffuseR",mod.getDiffuse().r());
		writeProp(tabs + 1, "diffuseG",mod.getDiffuse().g());
		writeProp(tabs + 1, "diffuseB",mod.getDiffuse().b());
		writeProp(tabs + 1, "diffuseA",mod.getDiffuse().a(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kSpecular)) {
		_file << separator;
		writeProp(tabs + 1, "specularR",mod.getSpecular().r());
		writeProp(tabs + 1, "specularG",mod.getSpecular().g());
		writeProp(tabs + 1, "specularB",mod.getSpecular().b());
		writeProp(tabs + 1, "specularA",mod.getSpecular().a(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kShininess)) {
		_file << separator;
		writeProp(tabs + 1, "shininess",mod.getShininess(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kLightEmission)) {
		_file << separator;
		writeProp(tabs + 1, "lightEmission",mod.getLightEmission(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kTexture)) {
		_file << separator;
		extractFileName(mod.getTexture(),file);
		writeProp(tabs + 1, "texture",file,"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kTextureOffset)) {
		_file << separator;
		writeProp(tabs + 1, "textureOffsetX",mod.getTextureOffset().x());
		writeProp(tabs + 1, "textureOffsetY",mod.getTextureOffset().y(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kTextureScale)) {
		_file << separator;
		writeProp(tabs + 1, "textureScaleX",mod.getTextureScale().x());
		writeProp(tabs + 1, "textureScaleY",mod.getTextureScale().y(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kLightmap)) {
		_file << separator;
		extractFileName(mod.getLightMap(),file);
		writeProp(tabs + 1, "lightmap",file,"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kNormalMap)) {
		_file << separator;
		extractFileName(mod.getNormalMap(),file);
		writeProp(tabs + 1, "normalMap",file,"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kNormalMapOffset)) {
		_file << separator;
		writeProp(tabs + 1, "normalMapOffsetX",mod.getNormalMapOffset().x());
		writeProp(tabs + 1, "normalMapOffsetY",mod.getNormalMapOffset().y(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kNormalMapScale)) {
		_file << separator;
		writeProp(tabs + 1, "normalMapScaleX",mod.getNormalMapScale().x());
		writeProp(tabs + 1, "normalMapScaleY",mod.getNormalMapScale().y(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kLightNormalMap)) {
		_file << separator;
		extractFileName(mod.getLightNormalMap(),file);
		writeProp(tabs + 1, "lightNormalMap",file,"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kReflectionAmount)) {
		_file << separator;
		writeProp(tabs + 1, "reflectionAmount",mod.getReflectionAmount(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kRefractionAmount)) {
		_file << separator;
		writeProp(tabs + 1, "refractionAmount",mod.getRefractionAmount(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kCastShadows)) {
		_file << separator;
		writeProp(tabs + 1, "castShadows",mod.getCastShadows(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kReceiveShadows)) {
		_file << separator;
		writeProp(tabs + 1, "receiveShadows",mod.getReceiveShadows(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kReceiveProjections)) {
		_file << separator;
		writeProp(tabs + 1, "receiveProjections",mod.getReceiveProjections(),"");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kAlphaCutoff)) {
		_file << separator;
		writeProp(tabs + 1, "alphaCutoff", mod.getAlphaCutoff(), "");
		separator = ",\n";
	}
	if (mod.isEnabled(MaterialModifier::kShininessMask)) {
		_file << separator;
		separator = ",\n";
		extractFileName(mod.getShininessMask(), file);
		writeProp(tabs + 1, "shininessMask", file, "");
		_file << separator;
		writeProp(tabs + 1, "shininessMaskChannel", mod.getShininessMaskChannel(), "");
		_file << separator;
		writeProp(tabs + 1, "invertShininessMask", mod.getInvertShininessMask(), "");
	}
	if (mod.isEnabled(MaterialModifier::kLightEmissionMask)) {
		_file << separator;
		separator = ",\n";
		extractFileName(mod.getShininessMask(), file);
		writeProp(tabs + 1, "lightEmissionMask", file, "");
		_file << separator;
		writeProp(tabs + 1, "lightEmissionMaskChannel", mod.getShininessMaskChannel(), "");
		_file << separator;
		writeProp(tabs + 1, "invertLightEmissionMask", mod.getInvertShininessMask(), "");
	}
	_file << "\n";
	writeTabs(tabs);
	_file << "}";
}

void LibraryWritter::copyResources(library::Node * node) {
	library::Material * mat = dynamic_cast<library::Material*>(node);
	library::Model * mod = dynamic_cast<library::Model*>(node);
	copyResource(node->getIcon());
	if (mat) {
		MaterialModifier & mod = mat->getMaterialModifier();
		copyResource(mod.getTexture());
		copyResource(mod.getLightMap());
		copyResource(mod.getNormalMap());
		copyResource(mod.getLightNormalMap());
		copyResource(mod.getShininessMask());
		copyResource(mod.getLightEmissionMask());
	}

	if (mod) {
		std::string file = mod->getFile();
		std::string fileName = vwgl::System::get()->getLastPathComponent(file);
		std::string subdir = vwgl::System::get()->removeExtension(fileName);
		std::string srcPath = file;
		std::string resourcePath = "";
		copyResource(file,subdir);
		if (!vwgl::System::get()->isAbsolutePath(file)) {
			srcPath = vwgl::System::get()->addPathComponent(_sourcePath, file);
			resourcePath = vwgl::System::get()->getPath(file);
		}
		else {
			resourcePath = vwgl::System::get()->getPath(file);
		}

		if (srcPath!="") {
			std::vector<std::string> deps;
			std::vector<std::string>::iterator it;
			vwgl::Loader::get()->getDrawableDependencies(srcPath, deps);
			for (it=deps.begin(); it!=deps.end(); ++it) {
				copyResource(resourcePath + *it,subdir);
			}
		}
	}
}

std::string LibraryWritter::copyResource(const std::string & src, const std::string & folder) {
	std::string absPath = src;
	if (!System::get()->isAbsolutePath(src)) {
		absPath = _sourcePath + src;
	}
	if (System::get()->fileExists(absPath)) {
		std::string fileName = absPath.substr(absPath.find_last_of("\\/") + 1);
		std::string dst;
		if (folder=="") {
			dst = vwgl::System::get()->addPathComponent(_dstFolder, fileName);
		}
		else {
			dst = vwgl::System::get()->addPathComponent(_dstFolder, folder);
			if (!vwgl::System::get()->directoryExists(dst)) {
				vwgl::System::get()->mkdir(dst);
			}
			dst = vwgl::System::get()->addPathComponent(dst, fileName);
		}
		System::get()->cp(absPath,dst,true);
		return dst;
	}
	return "";
}


}
