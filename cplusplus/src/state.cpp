
#include <vwgl/state.hpp>
#include <vwgl/opengl.hpp>
#include <iostream>

#pragma warning ( disable : 4835 )

namespace vwgl {

State * State::s_state = NULL;

State * State::get() {
	if (!s_state) s_state = new State();
	return s_state;
}

State * State::set(State * state) {
	if (s_state) delete s_state;
	s_state = state;
	return s_state;
}
	

State::State() :_mvMatrixDirty(true), _nMatrixDirty(true) {
	_modelMatrix = Matrix4::makeIdentity();
	_viewMatrix = Matrix4::makeIdentity();
	_modelMatrix = Matrix4::makeIdentity();
	SingletonController::get()->registerSingleton(this);
}

State::~State() {
	SingletonController::get()->unregisterSingleton(this);
}
	
void State::finalize() {
	
}

void State::pushViewMatrix() {
	_viewMatrixStack.push_back(_viewMatrix);
}

void State::popViewMatrix() {
	_viewMatrix = _viewMatrixStack.back();
	_viewMatrixStack.pop_back();
}

void State::pushModelMatrix() {
	_modelMatrixStack.push_back(_modelMatrix);
}

void State::popModelMatrix() {
	_modelMatrix = _modelMatrixStack.back();
	_modelMatrixStack.pop_back();
}

void State::pushProjectionMatrix() {
	_projectionMatrixStack.push_back(_projectionMatrix);
}

void State::popProjectionMatrix() {
	_projectionMatrix = _projectionMatrixStack.back();
	_projectionMatrixStack.pop_back();
}

void State::setViewport(const Viewport & vp) {
	_viewport = vp;
	// TODO: generalize to use multiple APIs
	glViewport(vp.x(), vp.y(), vp.width(), vp.height());
}
	
void State::setOpenGLState() {
	#if VWGL_LEGACY_OPENGL==1
	/*
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(_projectionMatrix.raw());
	
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(_viewMatrix.raw());
	glMultMatrixf(_modelMatrix.raw());
	*/
	#else
	std::cerr << "WARNING: Use of State::setOpenGLState(). Legacy OpenGL not supported." << std::endl;
	#endif
}

void State::enableDepthMask() {
	glDepthMask(GL_TRUE);
}
	
void State::disableDepthMask() {
	glDepthMask(GL_FALSE);
}
	
void State::enableFillOffset() {
	glEnable(GL_POLYGON_OFFSET_FILL);
}

void State::enableFillOffset(float factor, float units) {
	glPolygonOffset(factor, units);
	enableFillOffset();
}

void State::disableFillOffset() {
	glDisable(GL_POLYGON_OFFSET_FILL);
}
	
void State::enableLineOffset() {
	glEnable(GL_POLYGON_OFFSET_LINE);
}
	
void State::enableLineOffset(float factor, float units) {
	glPolygonOffset(factor, units);
	enableLineOffset();
}
	
void State::disableLineOffset() {
	glDisable(GL_POLYGON_OFFSET_LINE);
}

void State::enablePointOffset() {
	glEnable(GL_POLYGON_OFFSET_POINT);
}

void State::enablePointOffset(float factor, float units) {
	glPolygonOffset(factor, units);
	enablePointOffset();
}

void State::disablePointOffset() {
	glDisable(GL_POLYGON_OFFSET_POINT);
}


void State::enableBlend(BlendFunction function) {
	// TODO: use different APIs
	switch (function) {
		case kBlendFunctionAlpha:
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			break;
		case kBlendFunctionAddShadow:
			glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
			break;
		case kBlendFunctionAddLight:
			glBlendFunc(GL_ONE, GL_ONE);
			break;
		default:
			break;
	}
	glEnable(GL_BLEND);
}

void State::disableBlend() {
	glDisable(GL_BLEND);
}

void State::enableDepthTest() {
	// TODO: use multiple APIs
	glEnable(GL_DEPTH_TEST);
}
	
void State::disableDepthTest() {
	// TODO: use multiple APIs
	glDisable(GL_DEPTH_TEST);
}

void State::setClearColor(const Color & color) {
	// TODO: use multiple APIs
	glClearColor(color.r(), color.g(), color.b(), color.a());
}

void State::clearBuffers(int buffers) {
	// TODO: use multiple APIs
	GLuint clearBuffers = 0;
	if (buffers & kBufferColor) {
		clearBuffers = clearBuffers | GL_COLOR_BUFFER_BIT;
	}
	if (buffers & kBufferDepth) {
		clearBuffers = clearBuffers | GL_DEPTH_BUFFER_BIT;
	}
	glClear(clearBuffers);
}

}
