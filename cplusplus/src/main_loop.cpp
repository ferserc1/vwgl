#include <vwgl/app/main_loop.hpp>
#include <vwgl/app/window_controller.hpp>

namespace vwgl {
namespace app {

MainLoop * MainLoop::s_singleton = nullptr;

void MainLoop::finalize() {
	_window = nullptr;
}

}
}
