// Canvas material: a material to render the scene in the canvas and apply a simple gausian blur
Class ("CanvasMaterial", vwgl.Material, {
	_texture:null,
	_radius:1,

	// This material have built-in shaders, so it only have a synchronous load
	initialize:function(name) {
		this.parent(name);
	},
	
	initShader:function() {
		if (this._initialized) return;
		this._initialized = true;

		this.getShader().attachShader(vwgl.Shader.kTypeVertex, CanvasMaterial._vshader);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, CanvasMaterial._fshader);
		this.getShader().link("test_canvas_material");

		this.loadVertexAttrib("a_position");
		this.loadTexCoord0Attrib("a_texCoord");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uTextureSize");
		this.getShader().initUniformLocation("uKernel");
		this.getShader().initUniformLocation("uRadius");
		
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		this.getShader().setUniform("uTexture", this._texture);
		
		var vp = vwgl.State.get().getViewport();
		var size = new vwgl.Size2D(vp.width(),vp.height());
		this.getShader().setUniform("uTextureSize", size);
		this.getShader().setUniform1f("uRadius", this._radius);
		
		var blurKernel = [
			0.045, 0.122, 0.045,
		    0.122, 0.332, 0.122,
		    0.045, 0.122, 0.045
		]
		this.getShader().setUniform1fv("uKernel",blurKernel);
	},
	
	setRadius:function(r) { this._radius = r; },
	getRadius:function() { return this._radius; },
	
	setTexture:function(tex) { this._texture = tex; },
	getTexture:function() { return this._texture; }
});

CanvasMaterial._vshader = "#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	attribute vec4 a_position;\n\
	attribute vec2 a_texCoord;\n\
\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
 \n\
	varying vec2 v_texCoord;\n\
 \n\
	void main(){\n\
	    gl_Position = uPMatrix * uMVMatrix * a_position;\n\
	    v_texCoord = a_texCoord;\n\
	}";

CanvasMaterial._fshader = "#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	uniform sampler2D uTexture;\n\
	uniform vec2 uTextureSize;\n\
	uniform float uKernel[9];\n\
	uniform float uRadius;\n\
\n\
	// the texCoords passed in from the vertex shader.\n\
	varying vec2 v_texCoord;\n\
\n\
	void main() {\n\
	   vec2 onePixel = vec2(1.0, 1.0) / uTextureSize * uRadius;\n\
	   vec4 colorSum =\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2(-1, -1)) * uKernel[0] +\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2( 0, -1)) * uKernel[1] +\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2( 1, -1)) * uKernel[2] +\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2(-1,  0)) * uKernel[3] +\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2( 0,  0)) * uKernel[4] +\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2( 1,  0)) * uKernel[5] +\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2(-1,  1)) * uKernel[6] +\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2( 0,  1)) * uKernel[7] +\n\
	     texture2D(uTexture, v_texCoord + onePixel * vec2( 1,  1)) * uKernel[8] ;\n\
	   float kernelWeight =\n\
	     uKernel[0] +\n\
	     uKernel[1] +\n\
	     uKernel[2] +\n\
	     uKernel[3] +\n\
	     uKernel[4] +\n\
	     uKernel[5] +\n\
	     uKernel[6] +\n\
	     uKernel[7] +\n\
	     uKernel[8] ;\n\
\n\
	   if (kernelWeight <= 0.0) {\n\
	     kernelWeight = 1.0;\n\
	   }\n\
\n\
	   // Divide the sum by the weight but just use rgb\n\
	   // we'll set alpha to 1.0\n\
	   gl_FragColor = vec4((colorSum / kernelWeight).rgb, 1.0);\n\
	}";




Class ("MyApp",jsglut.Application,{
	_sceneRoot:null,		// Scene root
	
	_initVisitor:null,		// Init visitor: perform initialization in the nodes that require it
	_projectorMaterial:null,	// Special material used to project shadows
	
	_camera:null,			// Camera
	
	_ready:false,
	
	_lightRot:45,
	_lightElevation:-35,
	
	_nodeManipulator:null,
	
	// NEW
	_renderPass:null,		// The renderPass wraps a FBO and a material to render a scene to a texture
	_renderCanvas:null,		// The renderCanvas allows to draw a texture in the screen, applying a material
	_renderMaterial:null,
	
	initGL:function() {
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());
		
		var loader = new vwgl.Loader();
		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");	
		resources.push("star.vwglb");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createScene(loader);
			This.createCanvas(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	createCanvas:function(loader) {
		// In this example we'll draw all the objects in the scene using a generic material,
		// but the real power of this technique is to use several materials to draw the entire
		// scene in different passes, and then mix all the resulting textures in the canvas.
		// This technique is known as "deferred render".
		var viewport = vwgl.State.get().getViewport();
		var renderMaterial = new vwgl.GenericMaterial();
		var canvasMaterial = new CanvasMaterial();
		this._renderPass = new vwgl.RenderPass(this._sceneRoot, renderMaterial, new vwgl.FramebufferObject(viewport.width(),viewport.height()));
		canvasMaterial.setTexture(this._renderPass.getFbo().getTexture());
		canvasMaterial.setRadius(3.0);
		this._renderCanvas = new vwgl.RenderCanvas();
		this._renderCanvas.setMaterial(canvasMaterial);
		this._renderMaterial = renderMaterial;
	},
	
	resizeCanvas:function(vp) {
		this._renderPass.getFbo().resize(vp.width(),vp.height());
	},

	createScene:function(loader) {
		gl.clearColor(0.0,0.0,0,1);
		gl.enable(gl.DEPTH_TEST);
		
		this._sceneRoot = new vwgl.Group();
		
		this._initVisitor = new vwgl.InitVisitor();
		this._projectorMaterial = new vwgl.ProjTextureDeferredMaterial(); 
		
		var star = loader.loadDrawable("star.vwglb");
		var starTrx = new vwgl.TransformNode();
		starTrx.addChild(new vwgl.DrawableNode(star));
		this._sceneRoot.addChild(starTrx);
		
		var light = new vwgl.Light();
		var lightTrx = new vwgl.TransformNode(vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(this._lightElevation),1,0,0).translate(0,0,10));
		lightTrx.addChild(light);
		this._sceneRoot.addChild(lightTrx);
		
		var lightDirection = light.getDirection();
		
		var floor = new vwgl.Plane(10);
		var floorMat = new vwgl.GenericMaterial();
		
		floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		floorMat.setTextureScale(new vwgl.Vector2(5,5));
		floorMat.setNormalMap(loader.loadTexture("bricks_nm.png"));
		floorMat.setNormalMapScale(new vwgl.Vector2(5,5));
		floorMat.setShininess(50.0);		
		floorMat.setReceiveProjections(true);
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode();
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		this._sceneRoot.addChild(floorTrx);

		this._camera = new vwgl.Camera();
		var cameraNode = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,0,5));
		cameraNode.addChild(this._camera);
		this._sceneRoot.addChild(cameraNode);
		
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,1,0));
		this._nodeManipulator.setTransform();
	},

	drawScene:function() {
		var vp = vwgl.State.get().getViewport();
		this._camera.getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		
		vwgl.LightManager.get().prepareFrame();
		vwgl.CameraManager.get().applyTransform();	
	
		gl.enable(gl.POLYGON_OFFSET_FILL);
		gl.polygonOffset(1.0,1.0);
		this._renderPass.setClearBuffers(true);
		this._renderPass.setRenderTransparentObjects(true);
		this._renderPass.setRenderOpaqueObjects(true);
		this._renderPass.updateTexture(true);

		gl.disable(gl.POLYGON_OFFSET_FILL);
		
		gl.depthMask(false);

		this._renderPass.setMaterial(this._projectorMaterial);
		this._renderPass.setClearBuffers(false);	
				
		var projectorList = vwgl.ProjectorManager.get().getProjectorList();
		for (var i=0; i<projectorList.length; ++i) {
			gl.enable(gl.BLEND);	// Enable blend
			gl.blendFunc(gl.DST_COLOR,gl.ONE_MINUS_SRC_ALPHA);
	
			this._projectorMaterial.setProjector(projectorList[i]);
			this._renderPass.updateTexture(true);
		}
		gl.enable(gl.BLEND);
		gl.blendFunc(gl.DST_COLOR,gl.ONE_MINUS_SRC_ALPHA);
		
		this._renderPass.setMaterial(this._renderMaterial);
		this._renderPass.setRenderTransparentObjects(true);
		this._renderPass.setRenderOpaqueObjects(false);
		this._renderPass.updateTexture(true);
		
		
		gl.disable(gl.BLEND);
		gl.depthMask(true);
		this._renderPass.setClearBuffers(true);
	},
	
	drawCanvas:function() {
		if (this._ready) {
			var vp = vwgl.State.get().getViewport();
			this._renderCanvas.draw(vp.width(),vp.height());
		}
	},

	display:function() {
		if (this._ready) {
			this._initVisitor.visit(this._sceneRoot);
			
			this.drawScene();
			
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
			this.drawCanvas();
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		vwgl.State.get().setViewport(vp);
		
		if (this._ready) {
			this.resizeCanvas(vp);
		}
	},

	idle:function() {
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},
	
	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta));
	},
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
