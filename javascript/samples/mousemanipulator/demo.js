

Class ("MyApp",jsglut.Application,{
	_sceneRoot:null,	// Scene root
	_drawVisitor:null,	// DrawNodeVisitor to draw our scene
	_camera:null,		// Camera
	
	
	_nodeManipulator:null,	// New thing! node manipulator, to move the camera with the mouse
	
	initGL:function() {
		// In this example we create a simple scene, based on the previous example
		vwgl.System.get().setResourcesPath("../../resources");
		var loader = new vwgl.Loader();
		
		gl.clearColor(0.2,0.5,1,1);
		gl.enable(gl.DEPTH_TEST);
		
		var lightDirection = new vwgl.Vector3(0,-1,-1);
		lightDirection.normalize();
	
		this._sceneRoot = new vwgl.Group();
		
		this._drawVisitor = new vwgl.DrawNodeVisitor();
			
	
		var cube = new vwgl.Cube(1,1,1);
		var cubeMat = new vwgl.BasicPhongMaterial();
		cubeMat.setDiffuse(vwgl.Color.green());	
		cubeMat.setLightDirection(lightDirection);
		cubeMat.setTexture(loader.loadTexture("bricks.jpg"));
		cube.setMaterial(cubeMat);
		var cubeTrx = new vwgl.TransformNode();
		cubeTrx.addChild(new vwgl.DrawableNode(cube));
		this._sceneRoot.addChild(cubeTrx);
		
		var floor = new vwgl.Plane(5);
		var floorMat = new vwgl.BasicPhongMaterial();
		floorMat.setDiffuse(vwgl.Color.red());
		floorMat.setLightDirection(lightDirection);
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,-1.5,0));
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		this._sceneRoot.addChild(floorTrx);

		this._camera = new vwgl.Camera();
		var cameraNode = new vwgl.TransformNode();
		cameraNode.addChild(this._camera);
		this._sceneRoot.addChild(cameraNode);
		
		// Now, we create the node manipulator to move the camera
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		
		// Center the manipulator in our cube
		this._nodeManipulator.setCenter(cubeTrx.getTransform().getPosition());
		
		// Update the cameraNode transformation, according to the node manipulator's current setup
		this._nodeManipulator.setTransform();
	},

	display:function() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		
		vwgl.CameraManager.get().applyTransform();
		this._drawVisitor.visit(this._sceneRoot);
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		vwgl.State.get().setViewport(vp);
		
		this._camera.getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
	},

	idle:function() {
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		// The MouseTargetManipulator class works in two steps: the first step is to setup the action that you want to perform.
		// This action is setup in the mouseDown event, and could be:
		//	1) vwgl.MouseTargetManipulator.kManipulationRotate: Rotate around the center
		//	2) vwgl.MouseTargetManipulator.kManipulationDrag: Move the center. Depending on the camera orientation, this movement
		//	   												  will be produced in the front plane or in the Y plane
		//	3) vwgl.MouseTargetManipulator.kManipulationZoom: Zoom the scene (change the distance to the center)
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		// The second step is to send the new values to the mouse manipulator
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},
	
	mouseWheel:function(delta, x, y) {
		// You can use also the mouseWheel function to zoom the scene:
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta));
	},
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
