var appInstance;

Class ("MyApp",jsglut.Application,{
	_sceneRoot:null,		// Scene root
	_camera:null,			// Camera
	_nodeManipulator:null,

	// NEW: Two renderers, forward and deferred
	_fwRenderer:null,
	_defRenderer:null,
	_renderer:null,		// Selected renderer

	_lightRot:0,		// To animate the light rotation
	_lightElevation:-45,

	_ready:false,

	initGL:function() {
		var loader = new vwgl.Loader();
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());

		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");
		resources.push("star.vwglb");
		resources.push("alpha.png");
		resources.push("alpha_bm.png");
		resources.push("alpha_nm.png");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createRenderers(loader);
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	createRenderers:function(loader) {
		// Prepare the scene root and create the renderer
		this._sceneRoot = new vwgl.Group();
		var vp = vwgl.State.get().getViewport();
		this._fwRenderer = new vwgl.ForwardRenderer();
		this._fwRenderer.build(this._sceneRoot, new vwgl.Size2D(vp.width(),vp.height()));
		this._fwRenderer.setClearColor(new vwgl.Color(0.2,0.25,0.7));

		this._defRenderer = new vwgl.DeferredRenderer();
		this._defRenderer.build(this._sceneRoot, new vwgl.Size2D(vp.width(),vp.height()));
		this._defRenderer.setClearColor(new vwgl.Color(0.2,0.25,0.7));

		this._renderer = this._defRenderer;
		
		var ssao = this._renderer.getFilter("vwgl.SSAOMaterial");
		if (ssao) {
			ssao.setBlurIterations(8);
			ssao.setKernelSize(64);
			ssao.setSampleRadius(0.3);
		}
	},

	createScene:function(loader) {
		gl.clearColor(0.0,0.0,0,1);
		gl.enable(gl.DEPTH_TEST);

		//var star = loader.loadDrawable("star.vwglb");
		var star = new vwgl.Cube(1.0);
		window.starModel = star;
		var starTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0.0,0.5,0.0));
		starTrx.addChild(new vwgl.DrawableNode(star));
		this._sceneRoot.addChild(starTrx);
		
		star.getGenericMaterial().setTexture(loader.loadTexture("alpha.png"));
		star.getGenericMaterial().setNormalMap(loader.loadTexture("alpha_nm.png"));
		star.getGenericMaterial().setShininess(130);
		star.getGenericMaterial().setCullFace(false);

		// NEW: vwgl.ShadowLight instead of vwgl.Light
		var light = new vwgl.ShadowLight();
		// Define the size of the shadow map texture
		light.getProjectionMatrix().ortho(-3.0, 3.0, -3.0, 3.0, 0.1, 100.0);

		var lightTrx = new vwgl.TransformNode(vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(-35),1,0,0).translate(0,0,10));
		lightTrx.addChild(light);
		this._sceneRoot.addChild(lightTrx);

		var lightDirection = light.getDirection();

		var floor = new vwgl.Plane(10);
		window.floor = floor;
		var floorMat = new vwgl.GenericMaterial();

		floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		floorMat.setTextureScale(new vwgl.Vector2(5,5));
		floorMat.setNormalMap(loader.loadTexture("bricks_nm.png"));
		floorMat.setNormalMapScale(new vwgl.Vector2(5,5));
		floorMat.setShininess(50.0);
		floorMat.setReceiveProjections(true);
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode();
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		this._sceneRoot.addChild(floorTrx);

		this._camera = new vwgl.Camera();
		var cameraNode = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,0,5));
		cameraNode.addChild(this._camera);
		this._sceneRoot.addChild(cameraNode);

		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,1,0));
		this._nodeManipulator.setTransform();
	},

	display:function() {
		if (this._ready) {
			this._renderer.draw();
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		if (this._ready) {
			this._fwRenderer.setViewport(vp);
			this._defRenderer.setViewport(vp);
			this._defRenderer.resize(new vwgl.Size2D(vp.width(),vp.height()));

			this._camera.getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		}
		else {
			vwgl.State.get().setViewport(vp);
		}
	},

	idle:function() {
		if (this._ready) {
			// Usage of the light manager to get a light and modify its transformation
			var lm = vwgl.LightManager.get();
			var l = lm.getLightList()[0];

			// Light.getTransform() returns the closest vwgl.TransformNode transform matrix, but the light will
			// be affected by all the transform nodes over it's node. Use this carefully
			var lightTrx = l.getTransform();
			lightTrx.identity()
					.rotate(vwgl.Math.degreesToRadians(this._lightRot),0,1,0)
					.rotate(vwgl.Math.degreesToRadians(this._lightElevation),1,0,0)
					.translate(0,0,10);
			this._lightRot+=0.5;
		}
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},

	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta*0.05));
	},

	keyboard:function(key, stat, x, y) {
		console.log(key);
		if (key=='1') {
			vwgl.LightManager.get().setShadowMapSize(new vwgl.Size2D(512));
		}
		else if (key=='2') {
			vwgl.LightManager.get().setShadowMapSize(new vwgl.Size2D(1024));
		}
		else if (key=='3') {
			vwgl.LightManager.get().setShadowMapSize(new vwgl.Size2D(2048));
		}
		else if (key=='4') {
			vwgl.LightManager.get().setShadowMapSize(new vwgl.Size2D(4096));
		}
		else if (key=='F') {
			this._renderer = this._fwRenderer;
		}
		else if (key=='D') {
			this._renderer = this._defRenderer;
		}
	}
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);

	appInstance = new MyApp();
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(appInstance);
}
