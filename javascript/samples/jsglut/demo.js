
Class("MyApp",jsglut.Application,{
	initGL:function() {
		gl.clearColor(0.2,0.5,1,1.0);
	},

	display:function() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	},

	reshape:function(width,height) {
		gl.viewport(0,0,width,height);
	},

	idle:function() {
		//console.log("Idle");
	//	this.postRedisplay();
	},

	keyboard:function(key,x,y) {
		console.log("key down: " + key);
	},

	keyboardUp:function(key, x, y) {
		console.log("key up: " + key);
	},

	special:function(key, x, y) {
		console.log("special down: " + key);
	},

	specialUp:function(key, x, y) {
		console.log("special up: " + key);
	},

	mouse:function(button, state, x, y) {
		console.log("Mouse button: " + button + ", state:" + state + ", position: x=" + x + ", y=" + y);
	},

	motion:function(x, y) {
		console.log("Mouse motion: x=" + x + ", y=" + y);
	},

	motionPassive:function(x, y) {
		//console.log("Mouse motion passive: x=" + x + ", y=" + y);
	},
	
	mouseWheel:function(delta, x, y) {
		console.log("Mouse wheel. Delta:" + delta);
	},

	entry:function(state) {
		if (state==jsglut.Mouse.kMouseEntry) {
			console.log("Mouse entry");
		}
		else {
			console.log("Mouse exit");
		}
	},

	destroy:function() {
	}
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
