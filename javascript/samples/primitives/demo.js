
// We going to create a scene with the following structure:
//	sceneRoot
//		|_ sphereTransformNode
//		|	|_ sphereDrawableNode ( sphere drawable )
//		|_ cubeTransformNode
//		|	|_ cubeDrawableNode ( cube drawable )
//		|_ floorTransformNode
//		|	|_ floorDrawableNode ( plane drawable )
//		|_cameraTransformNode
//			|_ cameraNode

// NOTE about MATERIALS in this sample:
// This material have built-in shader code, so, it's not necesary to load any resource.
// You can check if a material have built-in shaders this with this code snippet:
//		vwgl.Material.getResourceList(vwgl.BasicPhongMaterial).length==0

// And other note about TEXTURES
// All the textures can be loaded synchronously: the webgl texture is generated immediatly with a black image,
// and the image will be set to the texture when the load be completed. So, you can load texturas as you would do
// it in the C++ API

Class ("MyApp",jsglut.Application,{
	_sceneRoot:null,	// Scene root
	_drawVisitor:null,	// DrawNodeVisitor to draw our scene
	_cubeTrx:null,		// TransformNode to rotate the cube
	_camera:null,		// Camera
	
	_cameraRot:0,		// Camera rotation

	initGL:function() {
		vwgl.System.get().setResourcesPath("../../resources");
		var loader = new vwgl.Loader();
		
		gl.clearColor(0.2,0.5,1,1);
		gl.enable(gl.DEPTH_TEST);
		
		// Light direction to use with the Phong Material
		var lightDirection = new vwgl.Vector3(0,-1,-1);
		lightDirection.normalize();
	
		// Scene root node
		this._sceneRoot = new vwgl.Group();
		
		// Draw visitor
		this._drawVisitor = new vwgl.DrawNodeVisitor();
	
		// Sphere: Create a sphere, add it to the scene graph
		// 1) Create a sphere and set a material
		var sphere = new vwgl.Sphere(0.5,30);
		var sphereMat = new vwgl.BasicPhongMaterial();
		sphereMat.setDiffuse(vwgl.Color.yellow());
		sphereMat.setLightDirection(lightDirection);
		sphereMat.setTexture(loader.loadTexture("bricks.jpg"));
		sphere.setMaterial(sphereMat);
		
		// 2) Create a drawable node to link the sphere to the scene graph
		var sphereNode = new vwgl.DrawableNode(sphere);
		
		// 3) Create a transformation node to place the sphere in the desired position
		var sphereTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,1.5,0));
		sphereTrx.addChild(sphereNode);
		
		// 4) Add the transform node to the scene root
		this._sceneRoot.addChild(sphereTrx);
		
		// Cube: Create a cube and add it to the scene graph.
		// 1) Create a cube and set a material
		var cube = new vwgl.Cube(1,1,1);
		var cubeMat = new vwgl.BasicPhongMaterial();
		cubeMat.setDiffuse(vwgl.Color.green());
		
		
		
		cubeMat.setLightDirection(lightDirection);
		cubeMat.setTexture(loader.loadTexture("bricks.jpg"));
		cube.setMaterial(cubeMat);
		
		// 2) Create a DrawableNode to link the cube in the scene graph
		var cubeNode = new vwgl.DrawableNode(cube);
		
		// 3) Create a TransformNode to place and rotate the cube
		this._cubeTrx = new vwgl.TransformNode();
		this._cubeTrx.addChild(cubeNode);
		
		// 4) Add the transform node to the scene root
		this._sceneRoot.addChild(this._cubeTrx);
		
		
		// Floor: create a floor and add it to the previous scene structure:
		// 1) Create a plane and set the material
		var floor = new vwgl.Plane(5);
		var floorMat = new vwgl.BasicPhongMaterial();
		floorMat.setDiffuse(vwgl.Color.red());
		floorMat.setLightDirection(lightDirection);
		floor.setMaterial(floorMat);
		// 2) Create a transform node for the floor
		var floorTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,-1.5,0));
		// 3) Create and add the DrawableNode to the transform in one single step
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		// 4) Add the transform node to the scene root
		this._sceneRoot.addChild(floorTrx);


		// Camera: add a camera to the scene
		// 1) Create a camera
		this._camera = new vwgl.Camera();
		// 2) Create the camera transform node
		var cameraNode = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,0,5));
		// 3) Add the camera to the transform
		cameraNode.addChild(this._camera);
		// 4) Add the camera transform to the scene
		this._sceneRoot.addChild(cameraNode);
	},

	display:function() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		
		// Apply the projection and view matrix with this
		vwgl.CameraManager.get().applyTransform(this._camera);
		
		// In this particular case, with only one camera, we also could do this
		// vwgl.CameraManager.get().applyTransform();	<< If no camera is passed, the CameraManager will use the main camera

		// Draw the scene using the DrawNodeVisitor, passing it the scene
		this._drawVisitor.visit(this._sceneRoot);
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		vwgl.State.get().setViewport(vp);
		
		// Change the projection matrix to our camera on reshape
		this._camera.getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
	},

	idle:function() {
		// Update scene: modify the model matrix in the scene. In this case, we only modify
		// the model matrix of our cube
		this._cubeTrx.getTransform().rotate(vwgl.Math.degreesToRadians(1),1,0,0);
		
		// Update the camera: changing the position of the camera, we update the view matrix
		var cam = vwgl.CameraManager.get().getMainCamera();
		if (cam) {
			// The Camera.getTransform() function returns the camera parent node transform, if the parent
			// is a vwgl.TransformNode. This is a quick trick to update the camera position, but
			// it must to have into account that the camera position is affected by all the parent
			// nodes in the scene over the camera
			cam.getTransform()
				.identity()
				.rotate(vwgl.Math.degreesToRadians(this._cameraRot),0,1,0)
				.translate(0,0,8);
			this._cameraRot++;
		}

		this.postRedisplay();
	}

});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
