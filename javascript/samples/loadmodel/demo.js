

Class ("MyApp",jsglut.Application,{
	_sceneRoot:null,	// Scene root
	_drawVisitor:null,	// DrawNodeVisitor to draw our scene
	_camera:null,		// Camera
	
	_ready:false,
	
	_lightRot:0,
	_lightElevation:-25,
	
	_nodeManipulator:null,
	
	initGL:function() {
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());
		
		var loader = new vwgl.Loader();
		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");
		
		//// NEW: add the model to the resource list. A model must be loaded using loader.loadResource()
		// or loader.loadResourceList() before call to loader.loadDrawable(), because loadDrawable() is a
		// synchronous funciton.
		resources.push("star.vwglb");
		
		
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	createScene:function(loader) {
		gl.clearColor(0.0,0.0,0,1);
		gl.enable(gl.DEPTH_TEST);
		
		this._sceneRoot = new vwgl.Group();
		
		this._drawVisitor = new vwgl.DrawNodeVisitor();
		
		///// NEW: Load a model. To load a model using the loader, it's necesary to previously load the resource
		// using loader.loadResource() or loader.loadResourceList()
		var star = loader.loadDrawable("star.vwglb");
		var starTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,-1,0));
		starTrx.addChild(new vwgl.DrawableNode(star));
		this._sceneRoot.addChild(starTrx);
		
		
		// The rest of the scene is equal to the previous example
		var light = new vwgl.Light();
		var lightTrx = new vwgl.TransformNode(vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(this._lightElevation),1,0,0).translate(0,0,10));
		lightTrx.addChild(light);
		this._sceneRoot.addChild(lightTrx);
		
		var lightDirection = light.getDirection();
		
		var floor = new vwgl.Plane(10);
		var floorMat = new vwgl.GenericMaterial();
		floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		floorMat.setNormalMap(loader.loadTexture("bricks_nm.png"));
		floorMat.setTextureScale(new vwgl.Vector2(4,4));
		floorMat.setNormalMapScale(new vwgl.Vector2(4,4));
		floorMat.setShininess(10.0);
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,-1,0));
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		this._sceneRoot.addChild(floorTrx);

		this._camera = new vwgl.Camera();
		var cameraNode = new vwgl.TransformNode();
		cameraNode.addChild(this._camera);
		this._sceneRoot.addChild(cameraNode);
		
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0));
		this._nodeManipulator.setTransform();
	},

	display:function() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		
		vwgl.LightManager.get().prepareFrame();
		
		if (this._ready) {
			vwgl.CameraManager.get().applyTransform();
			this._drawVisitor.visit(this._sceneRoot);
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		vwgl.State.get().setViewport(vp);
		
		if (this._ready) {
			this._camera.getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		}
	},

	idle:function() {
		if (this._ready) {
			var lm = vwgl.LightManager.get();
			var l = lm.getLightList()[0];
		
			var lightTrx = l.getTransform();
			lightTrx.identity()
					.rotate(vwgl.Math.degreesToRadians(this._lightRot),0,1,0)
					.rotate(vwgl.Math.degreesToRadians(this._lightElevation),1,0,0)
					.translate(0,0,10);
			this._lightRot++;			
		}
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},
	
	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta));
	},
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
		
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
