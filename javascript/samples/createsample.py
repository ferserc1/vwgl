#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import subprocess
import shutil
import json
import argparse
from subprocess import call

htmlSource = 'jsglut/index.html'
jsSource = 'jsglut/demo.js'
htmlDst = 'index.html'
jsDst = 'demo.js'

def create_sample_dir(name):
	if os.path.exists(name):
		print("ERROR: There is a sample with the same name")
		exit(-1);
	else:
		os.makedirs(name)

if len(sys.argv)!=2:
	print("Usage: createsample.py sample_name")
else:
	sampleName = sys.argv[1];
	create_sample_dir(sampleName)
	shutil.copyfile(htmlSource, sampleName + '/' + htmlDst);
	shutil.copyfile(jsSource, sampleName + '/' + jsDst);
	print("Done. Use the following line to add it in index file:")
	print('<p><a href="' + sampleName + '/">New sample:</a> new sample description. <a href="' + sampleName + '/demo.js" target="_blank">source</a></p>')
