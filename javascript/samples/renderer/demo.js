

Class ("MyApp",jsglut.Application,{
	_sceneRoot:null,		// Scene root
	_camera:null,			// Camera
	_nodeManipulator:null,
	
	_renderer:null,	// Our renderer

	_ready:false,	
	
	initGL:function() {
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());
		
		// Prepare the scene root and create the renderer
		this._sceneRoot = new vwgl.Group();
		var vp = vwgl.State.get().getViewport();
		this._renderer = new vwgl.ForwardRenderer();
		this._renderer.build(this._sceneRoot, new vwgl.Size2D(vp.width(),vp.height()));
		this._renderer.setClearColor(vwgl.Color.black());
		
		var loader = new vwgl.Loader();
		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");	
		resources.push("star.vwglb");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	createScene:function(loader) {
		gl.clearColor(0.0,0.0,0,1);
		gl.enable(gl.DEPTH_TEST);
		
		var star = loader.loadDrawable("star.vwglb");
		var starTrx = new vwgl.TransformNode();
		starTrx.addChild(new vwgl.DrawableNode(star));
		this._sceneRoot.addChild(starTrx);
		
		var light = new vwgl.Light();
		var lightTrx = new vwgl.TransformNode(vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(-35),1,0,0).translate(0,0,10));
		lightTrx.addChild(light);
		this._sceneRoot.addChild(lightTrx);
		
		var lightDirection = light.getDirection();
		
		var floor = new vwgl.Plane(10);
		var floorMat = new vwgl.GenericMaterial();
		
		floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		floorMat.setTextureScale(new vwgl.Vector2(5,5));
		floorMat.setNormalMap(loader.loadTexture("bricks_nm_2.png"));
		floorMat.setNormalMapScale(new vwgl.Vector2(5,5));
		floorMat.setShininess(50.0);
		floorMat.setReceiveProjections(true);	
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode();
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		this._sceneRoot.addChild(floorTrx);

		this._camera = new vwgl.Camera();
		var cameraNode = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,0,5));
		cameraNode.addChild(this._camera);
		this._sceneRoot.addChild(cameraNode);
		
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,1,0));
		this._nodeManipulator.setTransform();
	},

	display:function() {
		this._renderer.draw();
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		this._renderer.setViewport(vp);
		
		if (this._ready) {
			this._camera.getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		}
	},

	idle:function() {
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},
	
	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta*0.05));
	},
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
