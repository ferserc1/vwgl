
Class ("MyApp",jsglut.Application,{
	_sceneSet:null,	// NEW: The scene set, here will be all our scene's elements
	
	_nodeManipulator:null,
	_renderer:null,
	_ready:false,	
	
	initGL:function() {
		var loader = new vwgl.Loader();
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());
		
		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");	
		resources.push("star.vwglb");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createSceneSet();
			This.createRenderers(loader);
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	createSceneSet:function() {
		// NEW:
		// As simple as you can see: with the buildDefults() function, the scene set will
		// create for us a camera and a shadow light. The shadow map size for the lights
		// is passed as parameter. (remember that the shadows are only visible in
		// deferred render mode)
		this._sceneSet = new vwgl.SceneSet();
		this._sceneSet.buildDefault(new vwgl.Size2D(512,512));
	},

	createRenderers:function(loader) {
		var vp = vwgl.State.get().getViewport();
		this._renderer = new vwgl.ForwardRenderer();
		
		// NEW: You can access to the scene root node with getSceneRoot()
		this._renderer.build(this._sceneSet.getSceneRoot(), new vwgl.Size2D(vp.width(),vp.height()));
		this._renderer.setClearColor(new vwgl.Color(0.2,0.25,0.7,1.0));		
	},

	createScene:function(loader) {
		gl.clearColor(0.0,0.0,0,1);
		gl.enable(gl.DEPTH_TEST);
		
		// NEW: Our scene's root
		var root = this._sceneSet.getSceneRoot();
	
		var star = loader.loadDrawable("star.vwglb");
		var starTrx = new vwgl.TransformNode();
		starTrx.addChild(new vwgl.DrawableNode(star));
		root.addChild(starTrx);
		
		var floor = new vwgl.Plane(10);
		var floorMat = new vwgl.GenericMaterial();
		
		floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		floorMat.setTextureScale(new vwgl.Vector2(5,5));
		floorMat.setNormalMap(loader.loadTexture("bricks_nm_2.png"));
		floorMat.setNormalMapScale(new vwgl.Vector2(5,5));
		floorMat.setShininess(50.0);
		floorMat.setReceiveProjections(true);	
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode();
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		root.addChild(floorTrx);

		// NEW: Get the main camera transform with getMainCameraTransformFunction()
		var cameraNode = this._sceneSet.getMainCameraTransform();
		// Other usefull camera functions:
		//		- addCamera(name,transformMatrix);
		//		- getCamera(name);
		//		- getCameraTransform(name);
		//		- getMainCamera();
		// There are also similar functions to work with lights
		//		- addLight(name,transformMatrix);
		//		- getLight(name);
		//		- getLightTransform(name);
		//		- getMainLight();
		
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,1,0));
		this._nodeManipulator.setTransform();
		
	},

	display:function() {
		if (this._ready) {
			this._renderer.draw();	
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		if (this._ready) {
			this._sceneSet.getMainCamera().getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
			this._renderer.setViewport(vp);
		}
		else {
			vwgl.State.get().setViewport(vp);
		}
	},

	idle:function() {
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},
	
	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta*0.05));
	}
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
