
var modelTransf;
var sphereTransf;
var gizmo;

Class ("MyApp",jsglut.Application,{
	_nodeManipulator:null,
	_sceneSet:null,

	_renderer:null,		// Selected renderer

	_cubeMap:null,

	_ready:false,	

	_gizmo:null,
	_colorPicker:null,
	_doPick:false,
	_doManipulate:false,
	_selectedDrawable:null,
	_selectedSolid:null,

	initGL:function() {
		var loader = new vwgl.Loader();
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());

		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		var gizmoRes = vwgl.Gizmo.getResourceList(vwgl.PlaneGizmo);
		resources = resources.concat(gizmoRes);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");	
		resources.push("star.vwglb");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.loadGizmo();
			This.createSceneSet();
			This.createRenderers(loader);
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	loadGizmo:function() {
		this._gizmo = new vwgl.PlaneGizmo();
		gizmo = this._gizmo;
		this._colorPicker = new vwgl.ColorPicker();
	},

	createSceneSet:function() {
		this._sceneSet = new vwgl.SceneSet();
		this._sceneSet.buildDefault();
	},

	createRenderers:function(loader) {
		// Prepare the scene root and create the renderer
		var vp = vwgl.State.get().getViewport();
		this._renderer = new vwgl.ForwardRenderer();
		this._renderer.build(this._sceneSet.getSceneRoot(), new vwgl.Size2D(vp.width(),vp.height()));
		this._renderer.setClearColor(new vwgl.Color(0.0,0.0,0.0,1.0));		
	},

	createScene:function(loader) {
		gl.enable(gl.DEPTH_TEST);

		var root = this._sceneSet.getSceneRoot();

		var star = loader.loadDrawable("star.vwglb");
		var starTrx = new vwgl.TransformNode();
		starTrx.addChild(new vwgl.DrawableNode(star));
		root.addChild(starTrx);
		modelTransf = starTrx;
		this._colorPicker.assignPickId(star);

		var sphere = new vwgl.Sphere(0.5,40,40);
		this._cubeMap = new vwgl.DynamicCubemap();
		this._cubeMap.createCubemap(new vwgl.Size2D(512));
		var sphereMat = new vwgl.GenericMaterial();
		sphereMat.setCubeMap(this._cubeMap.getCubeMap());
		sphereMat.setReflectionAmount(0.9);
		sphere.setMaterial(sphereMat);
		var sphereTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(2,1,0));
		sphereTrx.addChild(new vwgl.DrawableNode(sphere));
		sphereTrx.addChild(this._cubeMap);
		root.addChild(sphereTrx);
		sphereTransf = sphereTrx;
		this._colorPicker.assignPickId(sphere);

		var floor = new vwgl.Plane(10);
		var floorMat = new vwgl.GenericMaterial();

		floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		floorMat.setTextureScale(new vwgl.Vector2(5,5));
		floorMat.setNormalMap(loader.loadTexture("bricks_nm_2.png"));
		floorMat.setNormalMapScale(new vwgl.Vector2(5,5));
		floorMat.setShininess(50.0);
		floorMat.setReceiveProjections(true);	
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode();
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		root.addChild(floorTrx);


		var cameraNode = this._sceneSet.getMainCameraTransform();
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,1,0));
		this._nodeManipulator.setTransform();
	},

	display:function() {
		if (this._ready) {
			vwgl.DynamicCubemapManager.get().update(this._sceneSet.getSceneRoot());
			//vwgl.DynamicCubemapManager.get().updateAllCubemaps(this._sceneSet.getSceneRoot());

			this._renderer.draw();	
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		if (this._ready) {
			this._sceneSet.getMainCamera().getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
			this._renderer.setViewport(vp);
		}
		else {
			vwgl.State.get().setViewport(vp);
		}
	},

	idle:function() {
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			var vp = vwgl.State.get().getViewport();
			this._colorPicker.pick(this._sceneSet.getSceneRoot(), x, vp.height() - y,
									vp.width(), vp.height(),
									vwgl.CameraManager.get().getMainCamera());
			if (vwgl.GizmoManager.get().checkItemPicked(this._colorPicker.getSolid())) {
				this._doPick = false;
				this._doManipulate = true;
				vwgl.GizmoManager.get().beginMouseEvents(new vwgl.Position2D(x,vp.height() - y), vwgl.CameraManager.get().getMainCamera(),vp);
			}
			else {
				this._doPick = true;
				this._doManipulate = false;
				if (button==jsglut.Mouse.kLeftButton) {
					this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
				}
				else if (button==jsglut.Mouse.kRightButton) {
					this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
				}
				else if (button==jsglut.Mouse.kMiddleButton) {
					this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
				}
			}
		}
		else if (state==jsglut.Mouse.kUpState && this._doManipulate) {
			this._doManipulate = false;
			vwgl.GizmoManager.get().endMouseEvents();
		}
		else if (state==jsglut.Mouse.kUpState && this._doPick) {
			this._doManipulate = false;
			if (this._selectedSolid) {
				mat = this._selectedSolid.getGenericMaterial();
				if (mat) {
					mat.setSelectedMode(false);
				}
			}
			this._selectedSolid = null;
			this._selectedDrawable = null;

			if (this._selectedDrawable = this._colorPicker.getDrawable()) {
				var drw = this._selectedDrawable.getParent();
				var trx = dynamic_cast("vwgl.TransformNode",drw.getParent());
				if (trx) {
					vwgl.GizmoManager.get().enableGizmo(trx,this._gizmo);
				}
			}
			else {
				vwgl.GizmoManager.get().disableGizmo();
			}
			if (this._selectedSolid = this._colorPicker.getSolid()) {
				var mat = this._colorPicker.getSolid().getGenericMaterial();
				if (mat) {
					mat.setSelectedMode(true);
				}
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._doPick = false;
		if (this._doManipulate) {
			var vp = vwgl.State.get().getViewport();
			var pos = new vwgl.Position2D(x, vp.height() - y);
			vwgl.GizmoManager.get().mouseEvent(pos);
		}
		else {
			var pos = new vwgl.Position2D(x,y);
			this._nodeManipulator.mouseMove(pos);
		}
	},

	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta*0.05));
	}
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);

	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
