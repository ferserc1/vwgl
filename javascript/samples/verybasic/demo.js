
Class ("MyApp",jsglut.Application,{
	_polyList:null,
	_shader:null,
	_rot:0,
	
	_ready:false,
	
	_vertexAttribLocation:null,
	_colorAttribLocation:null,

	initGL:function() {
		var This = this;
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		var loader = new vwgl.Loader();
		var resources = ["basic.fsh","basic.vsh"];
		loader.loadResourceList(resources,function(loadedItems,errors) {
			This.doInitGL(loader);
		});
	},
	
	doInitGL:function(loader) {
		gl.clearColor(0.2,0.5,1,1.0);
		gl.enable(gl.DEPTH_TEST);
		
		this._polyList = new vwgl.PolyList();
		
		this._polyList.addVertex(new vwgl.Vector3(-1,-1,0));	// 0
		this._polyList.addVertex(new vwgl.Vector3( 1,-1,0));	// 1
		this._polyList.addVertex(new vwgl.Vector3( 1, 1,0));	// 2
		this._polyList.addVertex(new vwgl.Vector3(-1, 1,0));	// 3
		
		this._polyList.addColor(vwgl.Color.red());
		this._polyList.addColor(vwgl.Color.green());
		this._polyList.addColor(vwgl.Color.blue());
		this._polyList.addColor(vwgl.Color.yellow());

		this._polyList.addTriangle(0,1,2);
		this._polyList.addTriangle(2,3,0);
		
		this._polyList.buildPolyList();
		
		this._shader = new vwgl.Shader();
		var status = this._shader.attachShader(vwgl.Shader.kTypeVertex, loader.getResource("basic.vsh"));
		status = status && this._shader.attachShader(vwgl.Shader.kTypeFragment, loader.getResource("basic.fsh"));
		status = status && this._shader.link();
		if (!status) {
			base.debug.log("Error loading shader");
		}
		else {
			this._vertexAttribLocation = this._shader.getAttribLocation("aVertexPosition");
			this._colorAttribLocation = this._shader.getAttribLocation("aVertexColor");
			this._shader.initUniformLocation("uMVMatrix");
			this._shader.initUniformLocation("uPMatrix");
		}
		this._ready = true;
		this.postRedisplay();
	},

	display:function() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		var vp = vwgl.State.get().getViewport();
		vwgl.State.get().projectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		
		vwgl.State.get().viewMatrix().identity().translate(0,0,-5);
		vwgl.State.get().modelMatrix().identity().rotate(vwgl.Math.degreesToRadians(this._rot),0,1,0);
		this._rot++;
		
		if (this._ready) {
			this._shader.bind();
			
			gl.bindBuffer(gl.ARRAY_BUFFER, this._polyList.getVertexBuffer());
			gl.enableVertexAttribArray(this._vertexAttribLocation);
			gl.vertexAttribPointer(this._vertexAttribLocation,this._polyList.getVertexBuffer().itemSize,gl.FLOAT,false,0,0);
			
			gl.bindBuffer(gl.ARRAY_BUFFER, this._polyList.getColorBuffer());
			gl.enableVertexAttribArray(this._colorAttribLocation);
			gl.vertexAttribPointer(this._colorAttribLocation,this._polyList.getColorBuffer().itemSize,gl.FLOAT,false,0,0);

			this._shader.setUniform("uMVMatrix",vwgl.State.get().modelViewMatrix());
			this._shader.setUniform("uPMatrix",vwgl.State.get().projectionMatrix());
		
			this._polyList.drawElements();
		
			gl.disableVertexAttribArray(this._vertexAttribLocation);
			gl.disableVertexAttribArray(this._colorAttribLocation);
				
			this._shader.unbind();
		}
	},

	reshape:function(width,height) {
		vwgl.State.get().setViewport(new vwgl.Viewport(0,0,width,height));
	},

	idle:function() {
		this.postRedisplay();
	}

});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
