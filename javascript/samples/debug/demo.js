var app;
var root;

Class ("MyApp",jsglut.Application,{
	_nodeManipulator:null,
	_sceneSet:null,
	_renderer:null,
	_drend:null,
	_frend:null,

	_ready:false,

	initGL:function() {
		var loader = new vwgl.Loader();
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());

		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("ciclorama.vwglb");
		resources.push("round_shadow.jpg");
		resources.push("sphere.vwglb");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createSceneSet();
			This.createRenderers(loader);
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	createSceneSet:function() {
		this._sceneSet = new vwgl.SceneSet();
		this._sceneSet.buildDefault(new vwgl.Size2D(2048));
		this._sceneSet.getMainLightTransform().getTransform()
				.identity()
				.rotate(vwgl.Math.degreesToRadians(-45),1,0,0)
				.rotate(vwgl.Math.degreesToRadians(20),0,1,0)
				.translate(0,0,10);
		this._sceneSet.getMainLight().setDiffuse(new vwgl.Color(0.8,0.8,0.8,1.0));
		this._sceneSet.getMainLight().setAmbient(new vwgl.Color(0.2,0.2,0.23,1.0));
		this._sceneSet.getMainLight().getProjectionMatrix().ortho(-3,3,-3,3,1,100);
		this._sceneSet.getMainLight().setEnabled(true);
		this._sceneSet.getMainLight().setShadowStrength(0.4);
	},

	createRenderers:function(loader) {
		var vp = vwgl.State.get().getViewport();
		this._drend = new vwgl.DeferredRenderer();
		this._drend.build(this._sceneSet.getSceneRoot(), new vwgl.Size2D(vp.width(),vp.height()));
		this._drend.setClearColor(new vwgl.Color(0.0,0.0,0.0,1.0));

		this._frend = new vwgl.ForwardRenderer();
		this._frend.build(this._sceneSet.getSceneRoot(), new vwgl.Size2D(vp.width(),vp.height()));
		this._frend.setClearColor(new vwgl.Color(0.0,0.0,0.0,1.0));

		this._renderer = this._drend;
	},

	createScene:function(loader) {
		gl.enable(gl.DEPTH_TEST);
		vwgl.System.get().setResourcesPath("../../resources/");

		//vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
		//vwgl.TextureManager.setMagFilter(vwgl.Texture.kFilterLinear);


		//var root = this._sceneSet.getSceneRoot();

		root = new vwgl.TransformNode();
		this._sceneSet.getSceneRoot().addChild(root);

		/*this._sphere = new vwgl.Sphere(1,30,30);


		var solid = this._sphere.getSolidList()[0];
		var proj = new vwgl.Projector();
		proj.setProjection(vwgl.Matrix4.makeOrtho(-1,1,-1,1,1,100));
		proj.setTransform(vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(90),1,0,0));
		proj.setTexture(loader.loadTexture("round_shadow.jpg"));
		solid.setShadowProjector(proj);
		*/

		this._sphere = loader.loadDrawable("sphere.vwglb");
		this._sphere.getGenericMaterial().setDiffuse(new vwgl.Color(1.0,0.0,0.0,1.0));
		this._sphere.getGenericMaterial().setLightEmission(1.0);

		var sphereTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(1,1,0));
		sphereTrx.addChild(new vwgl.DrawableNode(this._sphere));
		root.addChild(sphereTrx);

		var scene = loader.loadDrawable("ciclorama.vwglb");
		var sceneTrx = new vwgl.TransformNode();
		sceneTrx.addChild(new vwgl.DrawableNode(scene));
		root.addChild(sceneTrx);





		var cameraNode = this._sceneSet.getMainCameraTransform();
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,0.5,0));
		this._nodeManipulator.setDistance(3.5);
		app._nodeManipulator.setPitch(vwgl.Math.degreesToRadians(-12));
		app._nodeManipulator.setYaw(vwgl.Math.degreesToRadians(230));
		this._nodeManipulator.setTransform();
	},

	display:function() {
		if (this._ready) {
			this._renderer.draw();
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		if (this._ready) {
			this._sceneSet.getMainCamera().getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
			this._frend.setViewport(vp);
			this._frend.resize(new vwgl.Size2D(width,height));

			this._drend.setViewport(vp);
			this._drend.resize(new vwgl.Size2D(width,height));
		}
		else {
			vwgl.State.get().setViewport(vp);
		}
	},

	idle:function() {
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},

	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta*0.05));
	},

	keyboardUp:function(key,x,y) {
		if (key==' ') {
			if (this._renderer==this._drend) {
				this._renderer = this._frend;
			}
			else {
				this._renderer = this._drend;
			}
		}
		if (key=='S') {
			var v = this._sphere.getSolidList()[0].isVisible();
			this._sphere.getSolidList()[0].setVisible(!v);
		}
	}
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);

	app = new MyApp();
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(app);

	//vwgl.events.bind(vwgl.events.loadComplete,function(event,params) { console.log("Load completed"); console.log(params); });
	//vwgl.events.bind(vwgl.events.loadCached,function(event,params) { console.log("Load cached"); console.log(params); });
}
