
Class ("MyApp",jsglut.Application,{
	_nodeManipulator:null,
	_sceneSet:null,
	_renderer:null,

	// NEW: Our cubemap.
	_dynCubeMap:null,

	_ready:false,	
	
	initGL:function() {
		var loader = new vwgl.Loader();
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());
		
		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");	
		resources.push("star.vwglb");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createSceneSet();
			This.createRenderers(loader);
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	createSceneSet:function() {
		this._sceneSet = new vwgl.SceneSet();
		this._sceneSet.buildDefault();
	},

	createRenderers:function(loader) {
		var vp = vwgl.State.get().getViewport();
		this._renderer = new vwgl.ForwardRenderer();
		this._renderer.build(this._sceneSet.getSceneRoot(), new vwgl.Size2D(vp.width(),vp.height()));
		this._renderer.setClearColor(new vwgl.Color(0.0,0.0,0.0,1.0));		
	},

	createScene:function(loader) {
		gl.enable(gl.DEPTH_TEST);
		
		var root = this._sceneSet.getSceneRoot();
		
		var star = loader.loadDrawable("star.vwglb");
		var starTrx = new vwgl.TransformNode();
		starTrx.addChild(new vwgl.DrawableNode(star));
		root.addChild(starTrx);
		
		// NEW and shiny sphere
		var sphere = new vwgl.Sphere(0.5,40,40);
		
		// Create the cube map
		this._dynCubeMap = new vwgl.DynamicCubemap();
		this._dynCubeMap.createCubemap(new vwgl.Size2D(512));	// Size of the cube map FBO
			
		var sphereMat = new vwgl.GenericMaterial();
		
		// Set the cubeMap to the sphere material. Note that a DynamicCubemap is not a CubeMap:
		// a DynamicCubeMap has a CubeMap
		sphereMat.setCubeMap(this._dynCubeMap.getCubeMap());	
		
		// Very important: the cubeMap mix is controlled by the reflectionAmount property. If you
		// keep this variable in its default value (0) you will not see anything
		sphereMat.setReflectionAmount(0.9);
		
		sphere.setMaterial(sphereMat);
		var sphereTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(2,1,0));
		sphereTrx.addChild(new vwgl.DrawableNode(sphere));
		
		// Add the cube map in the same transform node as the sphere. You can create a cube map for each
		// object that have reflactions, or you can create only one cube map. As you will see in display()
		// method, it's almost computationally irrelevant to have various dynamic cube maps in the scene
		sphereTrx.addChild(this._dynCubeMap);
		root.addChild(sphereTrx);
		// END OF NEW things
		
		var floor = new vwgl.Plane(10);
		var floorMat = new vwgl.GenericMaterial();
		floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		floorMat.setTextureScale(new vwgl.Vector2(5,5));
		floorMat.setNormalMap(loader.loadTexture("bricks_nm_2.png"));
		floorMat.setNormalMapScale(new vwgl.Vector2(5,5));
		floorMat.setShininess(50.0);
		floorMat.setReceiveProjections(true);	
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode();
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		root.addChild(floorTrx);


		var cameraNode = this._sceneSet.getMainCameraTransform();
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,1,0));
		this._nodeManipulator.setTransform();
	},

	display:function() {
		if (this._ready) {
			// NEW: Update the cube maps. You can make it in two ways
			// 1) This function updates a cube map each 10 frames, and it do it one by one; that is,
			//	  every 10 frames it update one and only one cubemap. The following cubemaps (if there
			//	  are more than one) will be updated after 20, 30, 40,... etc.
			vwgl.DynamicCubemapManager.get().update(this._sceneSet.getSceneRoot());
			
			// 2) Update all the cubemaps, each frame. This is only needed if you scene have a lot
			//	  of  movement, and if you use this method, is recommendable to have only a cubemap
			//	  for all the scene
			//vwgl.DynamicCubemapManager.get().updateAllCubemaps(this._sceneSet.getSceneRoot());
			
			this._renderer.draw();	
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		if (this._ready) {
			this._sceneSet.getMainCamera().getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
			this._renderer.setViewport(vp);
		}
		else {
			vwgl.State.get().setViewport(vp);
		}
	},

	idle:function() {
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},
	
	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta*0.05));
	}
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
