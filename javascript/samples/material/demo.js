
// 1) Extend vwgl.Material to create a new material
Class ("BasicMaterial",vwgl.Material,{
	// 2) Material parameters:
	//		- name: name of the material. This parameter is optional.
	//		- readyCallback: If specified, the resources are loaded asyncrhonouslly, and when
	//						 the load is done, this callback will be called.
	//						 If not specified, the resources are loaded using the vwgl.Loader cache,
	//						 and in this case it's necesary to previously load the Material.getResourceList()
	//						 resources using the loader.
	//						 This parameter is optional and can be passed in first or second place.
	initialize:function(nameOrReadyCallback,readyCallback) {
		this.parent(nameOrReadyCallback,readyCallback);
	},
	
	// 3) It returns an array with the resources needed by this material. The location of this resources is
	//	  defined by the vwgl.System class
	getResourceList:function() { return ["basic.vsh","basic.fsh"]; },

	// 4) This function implements the initialization of the shader attributes and uniforms. All the resources
	//	  needed by the material are automatically loaded using the getResourceList() function, and will be available
	//	  using loader.getResource(), if the user specify the readyCallback
	initShaderSync:function(loader) {
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, loader.getResource("basic.vsh"));
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, loader.getResource("basic.fsh"));
		this.getShader().link();
		
		this.loadVertexAttrib("aVertexPosition");
		this.loadColorAttrib("aVertexColor");
		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");

		this._initialized = true;
	},

	// 5) Use this function to setup the shader's uniform variables before draw the polyList
	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix",vwgl.State.get().modelViewMatrix());
		this.getShader().setUniform("uPMatrix",vwgl.State.get().projectionMatrix());
	},
});

Class ("MyApp",jsglut.Application,{
	_polyList:null,
	_material:null,
	_rot:0,
	
	_ready:false,
	
	_vertexAttribLocation:null,

	initGL:function() {
		
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		gl.clearColor(0.2,0.5,1,1);
		gl.enable(gl.DEPTH_TEST);
		
		this._polyList = new vwgl.PolyList();
		
		this._polyList.addVertex(new vwgl.Vector3(-1,-1,0));	// 0
		this._polyList.addVertex(new vwgl.Vector3( 1,-1,0));	// 1
		this._polyList.addVertex(new vwgl.Vector3( 1, 1,0));	// 2
		this._polyList.addVertex(new vwgl.Vector3(-1, 1,0));	// 3
		
		this._polyList.addColor(vwgl.Color.red());
		this._polyList.addColor(vwgl.Color.green());
		this._polyList.addColor(vwgl.Color.blue());
		this._polyList.addColor(vwgl.Color.yellow());

		this._polyList.addTriangle(0,1,2);
		this._polyList.addTriangle(2,3,0);
		
		this._polyList.buildPolyList();
		
		// We have two options to load a material
		// Option 1: the powerful one. Manually load the material resources. It's useful if you need
		// to load more resources than the material's ones. See the implementation of the function for more
		// information
		this.loadMaterialOption1();
		
		// Option 2: the simplest one. The material class will load it's resources automatically.
		// This option is usefull if you need to load only one material, and you don't need any more
		// resources.
		//this.loadMaterialOption2();
	},
	
	// - Option 1: load the resources manually using the vwgl.Loader():
	loadMaterialOption1:function() {
		var loader = new vwgl.Loader();
		
		// You can use the vwgl.Material.getResourceList() static
		// function to get the resources needed by a material class without instance it.
		var resourceList = vwgl.Material.getResourceList(BasicMaterial);
		
		// You can also pass an array of material classes if you need to load several material resources
		//var resourceList = vwgl.Material.getResourceList([BasicMaterial,Material2,Material3]);
		
		// This method is usefull to load all scene resources
		// resourceList = resourceList.concat(["otherResource1.jpg","otherResource2.png","otherResource3.vwglb"]);

		var This = this;
		loader.loadResourceList(resourceList,function(loaded,errors) {
			// If you don't specify the readyCallback, this material will be syncrhonouslly loaded
			This._material = new BasicMaterial();
			This._material.setCullFace(false);
			This._ready = true;
			This.postRedisplay();
		});
	},
	
	// - Option 2: Automatically load the material resources. 
	loadMaterialOption2:function() {
		// Passing the readyCallback to the material constructor, the material will
		// loads the resources automatically and will call the callback function when done.
		var This = this;
		this._material = new BasicMaterial(function(mat) {
			mat.setCullFace(false);
			This._ready = true;
			This.postRedisplay();
		});
	},

	display:function() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		var vp = vwgl.State.get().getViewport();
		vwgl.State.get().projectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		
		vwgl.State.get().viewMatrix().identity().translate(0,0,-5);
		vwgl.State.get().modelMatrix().identity().rotate(vwgl.Math.degreesToRadians(this._rot),0,1,0);
		this._rot++;
		
		if (this._ready) {
			this._material.bindPolyList(this._polyList);
			this._material.activate();
			this._polyList.drawElements();
			this._material.deactivate();
		}
	},

	reshape:function(width,height) {
		vwgl.State.get().setViewport(new vwgl.Viewport(0,0,width,height));
	},

	idle:function() {
		this.postRedisplay();
	}

});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
