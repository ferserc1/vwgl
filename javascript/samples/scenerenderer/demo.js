

Class ("MyApp",jsglut.Application,{
	_sceneRoot:null,		// Scene root
	
	_sceneRenderer:null,	// New: remove DrawVistitor and add SceneRenderer
	_initVisitor:null,		// Init visitor: perform initialization in the nodes that require it
	
	// New: Projected shadows
	_projectorMaterial:null,	// Special material used to project shadows
	
	_camera:null,			// Camera
	
	_ready:false,
	
	_lightRot:0,
	_lightElevation:-25,
	
	_nodeManipulator:null,
	
	initGL:function() {
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());
		
		var loader = new vwgl.Loader();
		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");	
		resources.push("star.vwglb");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	createScene:function(loader) {
		gl.clearColor(0.0,0.0,0,1);
		gl.enable(gl.DEPTH_TEST);
		
		this._sceneRoot = new vwgl.Group();
		
		this._sceneRenderer = new vwgl.SceneRenderer();
		this._initVisitor = new vwgl.InitVisitor();
		// This material have built-in shaders. Remember: to check if a material have built-in shaders:
		// 		vwgl.Material.getResourceList(vwgl.ProjTextureDeferredMaterial)
		this._projectorMaterial = new vwgl.ProjTextureDeferredMaterial(); 
		
		// NOTE: This model includes a shadow projector. You also can create your own shadow projectors, but in this case
		// we'll use the projector included in this model.
		var star = loader.loadDrawable("star.vwglb");
		var starTrx = new vwgl.TransformNode();
		starTrx.addChild(new vwgl.DrawableNode(star));
		this._sceneRoot.addChild(starTrx);
		
		var light = new vwgl.Light();
		var lightTrx = new vwgl.TransformNode(vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(this._lightElevation),1,0,0).translate(0,0,10));
		lightTrx.addChild(light);
		this._sceneRoot.addChild(lightTrx);
		
		var lightDirection = light.getDirection();
		
		var floor = new vwgl.Plane(10);
		var floorMat = new vwgl.GenericMaterial();
		
		// NEW: Set only the normal map, to better appreciate the projecter shadow
		//floorMat.setTexture(loader.loadTexture("bricks.jpg"));

		floorMat.setNormalMap(loader.loadTexture("bricks_nm.png"));
		floorMat.setNormalMapScale(new vwgl.Vector2(10,10));
		floorMat.setShininess(50.0);
		
		// NEW (and very important): Enable receive projections in the floor material.
		floorMat.setReceiveProjections(true);
	
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode();
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		this._sceneRoot.addChild(floorTrx);

		this._camera = new vwgl.Camera();
		var cameraNode = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,0,5));
		cameraNode.addChild(this._camera);
		this._sceneRoot.addChild(cameraNode);
		
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,1,0));
		this._nodeManipulator.setTransform();
	},

	display:function() {
		// To draw the shadows we going to render several passes:
		//		1) In the first pass, render the color buffer and prepare the depth buffer
		//		   with a little offset applied
		//		2) After that, one pass per projector. Disable the depth map writting and
		//		   blend the shadow with the existing color buffer
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		
		if (this._ready) {
			this._initVisitor.visit(this._sceneRoot);
			vwgl.LightManager.get().prepareFrame();
			vwgl.CameraManager.get().applyTransform();
		
			// Render the scene with each object's original material
			this._sceneRenderer.getNodeVisitor().setMaterial(null);
			this._sceneRenderer.getNodeVisitor().setUseMaterialSettings(false);
		
			// Setup a little offset, to render the shadows over the color buffer
			gl.enable(gl.POLYGON_OFFSET_FILL);
			gl.polygonOffset(1.0,1.0);
			
			// Render the scene
			this._sceneRenderer.draw(this._sceneRoot,true);
			
			// Disable polygon offset
			gl.disable(gl.POLYGON_OFFSET_FILL);
			
			// Now is time to render the projector shadows
			gl.depthMask(false);		// We have our depth buffer ready in the previous render pass, so now we disable the depth buffer writting
			
			// Setup the projected texture material to the scene renderer. One adventage of using a scene renderer
			// is that it allows to render a scene using other material.
			this._sceneRenderer.getNodeVisitor().setMaterial(this._projectorMaterial);	// Render all objects using this._projectorMat...
			this._sceneRenderer.getNodeVisitor().setUseMaterialSettings(true);		// But use each object settings: for instance, an object may not receive projected textures
			
			// Render one pass for each projector
			var projectorList = vwgl.ProjectorManager.get().getProjectorList();
			for (var i=0; i<projectorList.length; ++i) {
				gl.enable(gl.BLEND);	// Enable blend
				gl.blendFunc(gl.DST_COLOR,gl.ONE_MINUS_SRC_ALPHA);	// Blend the projector shadow with the current color buffer
				
				// Setup the projector to the projected texture material
				this._projectorMaterial.setProjector(projectorList[i]);
				this._sceneRenderer.draw(this._sceneRoot,true);
			}
			
			gl.disable(gl.BLEND);	// Disable blend
			gl.depthMask(true);	// Enable depth buffer writting again
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		vwgl.State.get().setViewport(vp);
		
		if (this._ready) {
			this._camera.getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		}
	},

	idle:function() {
		if (this._ready) {
			var lm = vwgl.LightManager.get();
			var l = lm.getLightList()[0];
		
			var lightTrx = l.getTransform();
			lightTrx.identity()
					.rotate(vwgl.Math.degreesToRadians(this._lightRot),0,1,0)
					.rotate(vwgl.Math.degreesToRadians(this._lightElevation),1,0,0)
					.translate(0,0,10);
			this._lightRot++;			
		}
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},
	
	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta));
	},
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
