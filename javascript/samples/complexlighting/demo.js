var app;

Class ("MyApp",jsglut.Application,{
	_nodeManipulator:null,
	_sceneSet:null,
	_renderer:null,
	_drend:null,
	_frend:null,

	// NEW: Our cubemap.
	_dynCubeMap:null,

	_ready:false,

	_lights:null,
	_lightTimer:null,
	_currentLightIndex:-1,
	_multilight:true,

	_projectionMethod:null,

	_updateVisitor:null,

	initGL:function() {
		this._updateVisitor = new vwgl.UpdateVisitor();
		var loader = new vwgl.Loader();
		this._ready = false;
		vwgl.System.get().setResourcesPath("../../resources/");
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		vwgl.Loader.registerReader(new vwgl.VwglbLoader());

		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");
		resources.push("cubemap/cubemap_posx.jpg");
		resources.push("cubemap/cubemap_negx.jpg");
		resources.push("cubemap/cubemap_posy.jpg");
		resources.push("cubemap/cubemap_negy.jpg");
		resources.push("cubemap/cubemap_posz.jpg");
		resources.push("cubemap/cubemap_negz.jpg");
		resources.push("star.vwglb");
		resources.push("sphere/sphere.vwglb");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.createSceneSet();
			This.createRenderers(loader);
			This.createScene(loader);
			This._ready = true;
			This.postReshape();
		});
	},

	cancelLightTimer:function() {
		if (this._lightTimer) {
			this._lightTimer.repeat = false;
			this._lightTimer.cancel();
			this._lightTimer = null;
			this._currentLightIndex = -1;
		}
	},

	turnOnTheLights:function() {
		this.cancelLightTimer();
		var This = this;
		This._currentLightIndex = 1;
		This._lights[0].setEnabled(true);
		this._lightTimer = new base.Timer(function(timer) {
			if (This._currentLightIndex>=This._lights.length) {
				This.cancelLightTimer();
			}
			else {
				This._lights[This._currentLightIndex].setEnabled(true);
				++This._currentLightIndex;
			}
		}, 100);
		this._lightTimer.repeat = true;
	},

	turnOffTheLights:function() {
		this.cancelLightTimer();
		var This = this;
		This._currentLightIndex = 1;
		This._lights[0].setEnabled(false);
		this._lightTimer = new base.Timer(function(timer) {
			if (This._currentLightIndex>=This._lights.length) {
				This.cancelLightTimer();
			}
			else {
				This._lights[This._currentLightIndex].setEnabled(false);
				++This._currentLightIndex;
			}
		}, 100);
		this._lightTimer.repeat = true;
	},

	switchOffSpotlight:function() {
		this._spotLight.setEnabled(false);
	},

	switchOnSpotlight:function() {
		this._spotLight.setEnabled(true);
	},

	switchOnMainLight:function() {
		this._sceneSet.getMainLight().setEnabled(true);
	},

	switchOffMainLight:function() {
		this._sceneSet.getMainLight().setEnabled(false);
	},

	getPointLight:function() {
		var pointLight = new vwgl.Light();
		pointLight.setAmbient(new vwgl.Color(0.0,0.0,0.0,1.0));
		pointLight.setDiffuse(new vwgl.Color(0.4,0.2,0.1,1.0));
		pointLight.setSpecular(new vwgl.Color(1.0,0.8,0.6,1.0));
		pointLight.setConstantAttenuation(0.0);
		pointLight.setLinearAttenuation(0.95);
		pointLight.setExpAttenuation(0.2);
		pointLight.setCutoffDistance(5.0);
		var pointLightNode = new vwgl.TransformNode();
		pointLightNode.addChild(pointLight);
		pointLight.setType(vwgl.Light.kTypePoint);
		this._sceneSet.getSceneRoot().addChild(pointLightNode);
		this.pointLight = pointLight;
		this.pointLight.setEnabled(false);
		return pointLight;
	},

	createSceneSet:function() {
		this._sceneSet = new vwgl.SceneSet();
		this._sceneSet.buildDefault(new vwgl.Size2D(2048));
		this._sceneSet.getMainLightTransform().getTransform()
				.identity()
				.rotate(vwgl.Math.degreesToRadians(-45),1,0,0)
				.rotate(vwgl.Math.degreesToRadians(20),0,1,0)
				.translate(0,0,10);
		this._sceneSet.getMainLight().setDiffuse(new vwgl.Color(0.8,0.8,0.8,1.0));
		this._sceneSet.getMainLight().setAmbient(new vwgl.Color(0.2,0.2,0.23,1.0));
		this._sceneSet.getMainLight().getProjectionMatrix().ortho(-3,3,-3,3,1,100);
		this._sceneSet.getMainLight().setEnabled(true);
		this._sceneSet.getMainLight().setShadowStrength(0.4);

		if (this._multilight) {
			var numLights = 30;
			this._lights = [];
			for (var i = 0; i<numLights; ++i) {
				var l = this.getPointLight();
				this._lights.push(l);
				var alpha = (vwgl.Math.k2Pi / numLights) * i;
				l.getTransform()
					.identity()
					.translate(0,0.5,0)
					.rotate(alpha,0,1,0)
					.translate(0,0,5);
			}

			var spotLight = new vwgl.ShadowLight();
			spotLight.getProjectionMatrix().perspective(100.0,1,0.01,100.0);
			//spotLight.getProjectionMatrix().ortho(-5,5,-5,5,1.0,10.0);
			spotLight.setAmbient(new vwgl.Color(0.3,0.3,0.3,1.0));
			spotLight.setDiffuse(new vwgl.Color(0.9,0.9,0.9,1.0));
			spotLight.setSpecular(new vwgl.Color(1.0,1.0,1.0,1.0));
			spotLight.setType(vwgl.Light.kTypeSpot);
			spotLight.setConstantAttenuation(0.0);
			spotLight.setLinearAttenuation(0.15);
			spotLight.setExpAttenuation(0.0);
			spotLight.setSpotCutoff(52);
			spotLight.setSpotExponent(15.0);
			spotLight.setShadowBias(0.0001);
			var spotLightNode = new vwgl.TransformNode();
			spotLightNode.getTransform()
				.identity()
				.translate(new vwgl.Vector3(0,1.0,-2))
				.rotate(vwgl.Math.degreesToRadians(-15.0),1,0,0)
				.translate(0,0,5);
			spotLightNode.addChild(spotLight);
			this._sceneSet.getSceneRoot().addChild(spotLightNode);
			this._spotLight = spotLight;
			this._spotLight.setEnabled(false);
		}
	},

	createRenderers:function(loader) {
		var vp = vwgl.State.get().getViewport();
		this._drend = new vwgl.DeferredRenderer();
		this._drend.build(this._sceneSet.getSceneRoot(), new vwgl.Size2D(vp.width(),vp.height()));
		this._drend.setClearColor(new vwgl.Color(0.0,0.0,0.0,1.0));

		this._frend = new vwgl.ForwardRenderer();
		this._frend.build(this._sceneSet.getSceneRoot(), new vwgl.Size2D(vp.width(),vp.height()));
		this._frend.setClearColor(new vwgl.Color(0.0,0.0,0.0,1.0));

		this._renderer = this._drend;
		
		var ssao = this._drend.getFilter("vwgl.SSAOMaterial");
		if (ssao) {
			ssao.setKernelSize(32);
			ssao.setSampleRadius(0.35);
			ssao.setBlurIterations(8);
		}
	},

	createScene:function(loader) {
		vwgl.LightManager.get().setShadowMapSize(new vwgl.Size2D(2048));
		
		gl.enable(gl.DEPTH_TEST);

		vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
		vwgl.TextureManager.setMagFilter(vwgl.Texture.kFilterLinear);

		var cubemap = loader.loadCubemap("cubemap/cubemap_posx.jpg",
										"cubemap/cubemap_negx.jpg",
										"cubemap/cubemap_posy.jpg",
										"cubemap/cubemap_negy.jpg",
										"cubemap/cubemap_posz.jpg",
										"cubemap/cubemap_negz.jpg");

		var root = this._sceneSet.getSceneRoot();
		vwgl.System.get().setResourcesPath("../../resources/");
		var test = loader.loadDrawable("star.vwglb");
		var solidList = test.getSolidList();
		for (var i=0; i<solidList.length; ++i) {
			solidList[i].getGenericMaterial().setCubeMap(cubemap);
		}
		var testTrx = new vwgl.TransformNode();
		testTrx.addChild(new vwgl.DrawableNode(test));
		this.cup = test;
		root.addChild(testTrx);

		// Create the cube map
		/*this._dynCubeMap = new vwgl.DynamicCubemap();
		this._dynCubeMap.createCubemap(new vwgl.Size2D(512));	// Size of the cube map FBO
		//sofaTrx.addChild(this._dynCubeMap);
		testTrx.addChild(this._dynCubeMap);
		test.getMaterial().setCubeMap(this._dynCubeMap.getCubeMap());*/
		//this.sofa.getMaterial("Material-patas").setCubeMap(this._dynCubeMap.getCubeMap());




		vwgl.System.get().setResourcesPath("../../resources/");
		//var sphereMat = new vwgl.GenericMaterial();

		// Set the cubeMap to the sphere material. Note that a DynamicCubemap is not a CubeMap:
		// a DynamicCubeMap has a CubeMap
		//sphereMat.setCubeMap(this._dynCubeMap.getCubeMap());

		// Very important: the cubeMap mix is controlled by the reflectionAmount property. If you
		// keep this variable in its default value (0) you will not see anything
		//sphereMat.setReflectionAmount(0.9);

		//sphere.setMaterial(sphereMat);
		//var sphereTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(2,1,0));
		//sphereTrx.addChild(new vwgl.DrawableNode(sphere));

		// Add the cube map in the same transform node as the sphere. You can create a cube map for each
		// object that have reflactions, or you can create only one cube map. As you will see in display()
		// method, it's almost computationally irrelevant to have various dynamic cube maps in the scene
		//sphereTrx.addChild(this._dynCubeMap);
		//root.addChild(sphereTrx);
		// END OF NEW things

		var floor = new vwgl.Plane(10);
		this.floor = floor;
		var floorMat = new vwgl.GenericMaterial();
		//floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		//floorMat.setTextureScale(new vwgl.Vector2(9,9));
		//floorMat.setNormalMap(loader.loadTexture("bricks_nm.png"));
		//floorMat.setNormalMapScale(new vwgl.Vector2(9,9));
		floorMat.setSpecular(vwgl.Color.white());
		floorMat.setShininess(180.0);
		floorMat.setReceiveProjections(true);
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode();
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		root.addChild(floorTrx);

		vwgl.System.get().setResourcesPath("../../resources/sphere/");
		var sphere = loader.loadDrawable('sphere/sphere.vwglb');
		this.sky = sphere;
		sphere.getGenericMaterial().setCastShadows(false);
		var sphereTrx = new vwgl.TransformNode();
		sphereTrx.addChild(new vwgl.DrawableNode(sphere));
		root.addChild(sphereTrx);


		var cameraNode = this._sceneSet.getMainCameraTransform();
		this._nodeManipulator = new vwgl.MouseTargetManipulator(cameraNode);
		this._nodeManipulator.setCenter(new vwgl.Vector3(0,0.5,0));
		this._nodeManipulator.setDistance(3.5);
		app._nodeManipulator.setPitch(vwgl.Math.degreesToRadians(-12));
		app._nodeManipulator.setYaw(vwgl.Math.degreesToRadians(230));
		this._nodeManipulator.setTransform();

		this._projectionMethod = new vwgl.OpticalProjectionMethod();
		this._sceneSet.getMainCamera().setProjectionMethod(this._projectionMethod);
	},

	display:function() {
		if (this._ready) {
			// Update visitor: optional, but required to specific features such as physics simulation
			this._updateVisitor.visit(this._sceneSet.getSceneRoot());

			// NEW: Update the cube maps. You can make it in two ways
			// 1) This function updates a cube map each 10 frames, and it do it one by one; that is,
			//	  every 10 frames it update one and only one cubemap. The following cubemaps (if there
			//	  are more than one) will be updated after 20, 30, 40,... etc.
			vwgl.DynamicCubemapManager.get().update(this._sceneSet.getSceneRoot());

			// 2) Update all the cubemaps, each frame. This is only needed if you scene have a lot
			//	  of  movement, and if you use this method, is recommendable to have only a cubemap
			//	  for all the scene
			//vwgl.DynamicCubemapManager.get().updateAllCubemaps(this._sceneSet.getSceneRoot());

			this._renderer.draw();
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		if (this._ready) {
			this._sceneSet.getMainCamera().getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
			this._frend.setViewport(vp);
			this._frend.resize(new vwgl.Size2D(width,height));

			this._drend.setViewport(vp);
			this._drend.resize(new vwgl.Size2D(width,height));

			this._projectionMethod.setViewport(vp);
		}
		else {
			vwgl.State.get().setViewport(vp);
		}
	},

	idle:function() {
		this.postRedisplay();
	},

	mouse:function(button, state, x, y) {
		if (!this._ready) return;
		if (state==jsglut.Mouse.kDownState) {
			if (button==jsglut.Mouse.kLeftButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationRotate);
			}
			else if (button==jsglut.Mouse.kRightButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationDrag);
			}
			else if (button==jsglut.Mouse.kMiddleButton) {
				this._nodeManipulator.mouseDown(new vwgl.Position2D(x,y), vwgl.MouseTargetManipulator.kManipulationZoom);
			}
		}
	},

	motion:function(x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseMove(new vwgl.Position2D(x,y));
	},

	mouseWheel:function(delta, x, y) {
		if (!this._ready) return;
		this._nodeManipulator.mouseWheel(new vwgl.Vector2(0,delta*0.05));
	},

	keyboardUp:function(key,x,y) {
		if (key==' ') {
			if (this._renderer==this._drend) {
				this._renderer = this._frend;
			}
			else {
				this._renderer = this._drend;
			}
		}
	},

	keyboard:function(key,x,y) {
		if (key=='A') {
			this._nodeManipulator.moveLeft(0.2);
		}
		if (key=='D') {
			this._nodeManipulator.moveRight(0.2);
		}
		if (key=='W') {
			this._nodeManipulator.moveForward(-0.2);
		}
		if (key=='S') {
			this._nodeManipulator.moveBackward(-0.2);
		}
	}
});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);

	app = new MyApp();
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(app);

	//vwgl.events.bind(vwgl.events.loadComplete,function(event,params) { console.log("Load completed"); console.log(params); });
	//vwgl.events.bind(vwgl.events.loadCached,function(event,params) { console.log("Load cached"); console.log(params); });
}
