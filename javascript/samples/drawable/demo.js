
Class ("BasicMaterial",vwgl.Material,{
	_color:null,

	setColor:function(c) { this._color = c; },
	getColor:function() { return this._color; },

	initialize:function(name,readyCallback) {
		this._color = new vwgl.Color.white();
		this.parent(name,readyCallback);
	},
	
	getResourceList:function() { return ["basic_color.vsh","basic_color.fsh"]; },

	initShaderSync:function() {
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.Loader.getResource("basic_color.vsh"));
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.Loader.getResource("basic_color.fsh"));
		this.getShader().link();
	
		this.loadVertexAttrib("aVertexPosition");
		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uColor");

		this._initialized = true;
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix",vwgl.State.get().modelViewMatrix());
		this.getShader().setUniform("uPMatrix",vwgl.State.get().projectionMatrix());
		this.getShader().setUniform("uColor",this._color);
	},
});

Class ("MyApp",jsglut.Application,{
	_drawable:null,
	_shader:null,
	_rot:new vwgl.Vector2(0.0),
	
	_ready:false,
	
	_vertexAttribLocation:null,

	getPolyList:function() {
		var polyList = new vwgl.PolyList();
	
		polyList.addVertex(new vwgl.Vector3(-1,-1, 1));	// 0
		polyList.addVertex(new vwgl.Vector3( 1,-1, 1));	// 1
		polyList.addVertex(new vwgl.Vector3( 1, 1, 1));	// 2
		polyList.addVertex(new vwgl.Vector3(-1, 1, 1));	// 3
		polyList.addVertex(new vwgl.Vector3(-1,-1,-1));	// 4
		polyList.addVertex(new vwgl.Vector3( 1,-1,-1));	// 5
		polyList.addVertex(new vwgl.Vector3( 1, 1,-1));	// 6
		polyList.addVertex(new vwgl.Vector3(-1, 1,-1));	// 7

		return polyList;
	},

	buildCube:function() {
		this._drawable = new vwgl.Drawable();
	
		// Front face
		var front = this.getPolyList();
		front.addTriangle(0,1,2);
		front.addTriangle(2,3,0);
		front.buildPolyList();
		this._drawable.addSolid(new vwgl.Solid(front));
		
		// Right face
		var right = this.getPolyList();
		right.addTriangle(1,5,6);
		right.addTriangle(6,2,1);
		right.buildPolyList();
		this._drawable.addSolid(new vwgl.Solid(right));
		
		// Back face
		var back = this.getPolyList();
		back.addTriangle(5,4,7);
		back.addTriangle(7,6,5);
		back.buildPolyList();
		this._drawable.addSolid(new vwgl.Solid(back));
		
		// Left face
		var left = this.getPolyList();
		left.addTriangle(4,0,3);
		left.addTriangle(3,7,4);
		left.buildPolyList();
		this._drawable.addSolid(new vwgl.Solid(left));
		
		// Top face
		var top = this.getPolyList();
		top.addTriangle(3,2,6);
		top.addTriangle(6,7,3);
		top.buildPolyList();
		this._drawable.addSolid(new vwgl.Solid(top));
		
		// Bottom face
		var bottom = this.getPolyList();
		bottom.addTriangle(4,5,1);
		bottom.addTriangle(1,0,4);
		bottom.buildPolyList();
		this._drawable.addSolid(new vwgl.Solid(bottom));
	},

	initGL:function() {
		vwgl.System.get().setDefaultShaderPath("../../shaders/");
		var This = this;
		
		var loader = new vwgl.Loader();
		var resources = vwgl.Material.getResourceList(BasicMaterial);
		loader.loadResourceList(resources,function(loaded,errors) {
			This.doInitGl();
		});		
	},
	
	doInitGl:function() {
		gl.clearColor(0.2,0.5,1,1);
		gl.enable(gl.DEPTH_TEST);
		
		this.buildCube();
		
		var colors = [
			new vwgl.Color.red(),
			new vwgl.Color.blue(),
			new vwgl.Color.green(),
			new vwgl.Color.yellow(),
			new vwgl.Color.violet(),
			new vwgl.Color.orange()
		]
		for (var i=0;i<this._drawable.getSolidList().length;++i) {
			var mat = new BasicMaterial();
			mat.setColor(colors[i]);
			mat.setCullFace(false);
			var solid = this._drawable.getSolidList()[i];
			solid.setMaterial(mat);
		}
		this._ready = true;
		this.postRedisplay();
	},

	display:function() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		var vp = vwgl.State.get().getViewport();
		vwgl.State.get().projectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		
		vwgl.State.get().viewMatrix().identity().translate(0,0,-5);
		vwgl.State.get().modelMatrix()
				.identity()
				.rotate(vwgl.Math.degreesToRadians(this._rot.x()),1,0,0)
				.rotate(vwgl.Math.degreesToRadians(this._rot.y()),0,1,0);
		this._rot.add(new vwgl.Vector2(1));
		
		if (this._ready) {
			this._drawable.draw();
		}
	},

	reshape:function(width,height) {
		vwgl.State.get().setViewport(new vwgl.Viewport(0,0,width,height));
	},

	idle:function() {
		this.postRedisplay();
	}

});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
