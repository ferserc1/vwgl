
Class("MyApp",jsglut.Application,{
	initGL:function() {
		// Enable console output using the ?debug=true parameter in the site URL
		// IMPORTANT: Ensure to load this demo with the ?debug=true parameter in the URL
		base.debug.init();

		// Will be used to call this."method()" inside the loader callback function
		var This = this;

		var system = vwgl.System.get();
		
		// 1) Set the resources and shader path
		system.setResourcesPath("../../resources/");
		system.setDefaultShaderPath("../../shaders/");
		
		// 	-- optional --
		// Use this to specify binary extensions (other than images).
		// The supported 3D binary file extensions are already tracked, so
		// it is not necessary to add theese file types
		vwgl.Loader.addBinaryExtension("vwglb","Native VitaminewGL binary file");
		
		// 2) Create an array with the resources you want to access in a synchronous way
		var resourcesToLoad = [
			"bricks_nm.png",
			"bricks.jpg",
			"simple_cube.vwglb",
			"basic.fsh",
			"basic.vsh",
			"fail.txt"	// This resource does not exist, and will cause an error
		];
		
		// 3) Create a loader and use loadResourceList to load all resources
		var loader = new vwgl.Loader();
		loader.loadResourceList(resourcesToLoad,function(loadedItems,errors) {
			if (errors>0) base.debug.log("There were errors loading data");
			
			// 4) use loader.getResource(name) to get the resource data in a synchronous way
			var normalMapImg = loader.getResource("bricks_nm.png");
			var bricksImage = loader.getResource("bricks.jpg");
			var modelData = loader.getResource("simple_cube.vwglb");
			var vertexShader = loader.getResource("basic.vsh");
			var fragmentShader = loader.getResource("basic.fsh");
			
			// 5.a) Initialize your scene here
			gl.clearColor(0.2,0.5,1,1.0);
			
			// 5.b) Or use another function to do it.
			This.doInitGL(loader);
		});
	},
	
	doInitGL:function(loader) {
		// 5.c) You can pass the loader instance to use the dynamic version of getResource()
		var normalMapImg = loader.getResource("bricks_nm.png");
		
		// 5.d) But the loaded resource list is a global array, and you can also use the static version of getResource()
		var modelData = vwgl.Loader.getResource("simple_cube.vwglb");
		
		// 6) getResource() will return
		//	- A resource (image, text, object, etc) if the resource is loaded
		var vertexShader = vwgl.Loader.getResource("basic.vsh");
		
		//	- null if there were an error loading this resource
		var errorResource = vwgl.Loader.getResource("fail.txt");	// See the resourcesToLoad array: we ask for this resource, but it does not exist
		
		// 	- undefined if this resource is not loaded
		var whatTheHellResource = vwgl.Loader.getResource("undefined.txt");	// We didn't ask to the loader for this resource, so the loader returns undefined
		
		// 7) IMPORTANT: draw again
		this.postReshape();
		this.postRedisplay();
		
		// 8) The loaded files are cached globally, so if you try to load a cached file it will not be downloaded again.
		// If you execute this demo, you will see this message in the console output:
		//		File already cached: bricks_nm.png
		var otherLoader = new vwgl.Loader();
		// 8.b) You can also load files one by one
		otherLoader.loadResource('bricks_nm.png',function(data) {
			base.debug.log("Image loaded, but not downloaded");
		});
	},

	display:function() {
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	},

	reshape:function(width,height) {
		gl.viewport(0,0,width,height);
	}
});

function main(canvasId) {
	alert('Please, open your Javascript console to understand this demo');

	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
