Class ("MyApp",jsglut.Application,{
	_sceneRoot:null,	// Scene root
	_drawVisitor:null,	// DrawNodeVisitor to draw our scene
	_cubeTrx:null,		// TransformNode to rotate the cube
	_camera:null,		// Camera
	_lightElevation:-45.0,

	_ready:false,

	// NEW: Color picker
	_colorPicker:null,
	_selectedDrawable:null,
	_selectedSolid:null,

	initGL:function() {
		window.app = this;
		vwgl.System.get().setResourcesPath("../../resources");
		vwgl.System.get().setDefaultShaderPath("../../shaders");
		var loader = new vwgl.Loader();
		var resources = vwgl.Material.getResourceList(vwgl.GenericMaterial);
		resources.push("bricks.jpg");
		resources.push("bricks_nm.png");
		resources.push("wood_01.png");
		resources.push("leather_03.jpg");
		resources.push("leather_03_nm.png");
		var This = this;
		loader.loadResourceList(resources,function(loaded,errors) {
			This.doInitGL(loader);
			This._ready = true;
			This.postReshape();
			This.postRedisplay();
		})
	},

	doInitGL:function(loader) {	
		gl.enable(gl.DEPTH_TEST);
	
		this._sceneRoot = new vwgl.Group();
		this._drawVisitor = new vwgl.DrawNodeVisitor();
	
		var light = new vwgl.Light();
		var lightTrx = new vwgl.TransformNode(vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(this._lightElevation),1,0,0).translate(0,0,10));
		lightTrx.addChild(light);
		this._sceneRoot.addChild(lightTrx);

	
		var sphere = new vwgl.Sphere(0.5,30);
		var sphereMat = new vwgl.GenericMaterial();
		sphereMat.setTexture(loader.loadTexture("leather_03.jpg"));
		sphereMat.setNormalMap(loader.loadTexture("leather_03_nm.png"));
		sphere.setMaterial(sphereMat);
		var sphereTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,1.5,0));
		sphereTrx.addChild(new vwgl.DrawableNode(sphere));
		this._sceneRoot.addChild(sphereTrx);
		
		var cube = new vwgl.Cube(1,1,1);
		var cubeMat = new vwgl.GenericMaterial();
		cubeMat.setTexture(loader.loadTexture("wood_01.png"));
		cube.setMaterial(cubeMat);
		this._cubeTrx = new vwgl.TransformNode();
		this._cubeTrx.addChild(new vwgl.DrawableNode(cube));
		this._sceneRoot.addChild(this._cubeTrx);
		
		var floor = new vwgl.Plane(5);
		var floorMat = new vwgl.GenericMaterial();
		floorMat.setTexture(loader.loadTexture("bricks.jpg"));
		floorMat.setNormalMap(loader.loadTexture("bricks_nm.png"));
		floor.setMaterial(floorMat);
		var floorTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(0,-1.5,0));
		floorTrx.addChild(new vwgl.DrawableNode(floor));
		this._sceneRoot.addChild(floorTrx);
		
		
		
		////// Color pick part 1: Create the color picker and assign a pick identifier to each object
		////// that you want to select
		// NEW: Create color picker and assign a pick identifier to the scene each object
		// NEW: Create color picker
		this._colorPicker = new vwgl.ColorPicker();
		
		this._colorPicker.assignPickId(sphere);
		this._colorPicker.assignPickId(cube);
		this._colorPicker.assignPickId(floor);


		this._camera = new vwgl.Camera();
		var cameraNode = new vwgl.TransformNode();
		cameraNode.addChild(this._camera);
		this._camera.getTransform()
					.identity()
					.rotate(vwgl.Math.degreesToRadians(45),0,1,0)
					.translate(0,0,5);
		this._cameraRot++;
		this._sceneRoot.addChild(cameraNode);
	},

	display:function() {
		// NEW: set the clear color each frame: the color picker will change the clear color, so
		// we need to restore it here
		gl.clearColor(0.2,0.5,1,1);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		
		if (this._ready) {
			vwgl.LightManager.get().prepareFrame();
			vwgl.CameraManager.get().applyTransform();
			this._drawVisitor.visit(this._sceneRoot);			
		}
	},

	reshape:function(width,height) {
		var vp = new vwgl.Viewport(0,0,width,height);
		vwgl.State.get().setViewport(vp);
		
		if (this._ready) {
			this._camera.getProjectionMatrix().perspective(45.0,vp.aspectRatio(),0.1,100.0);
		}
	},

	idle:function() {
		if (this._ready) {
			this._cubeTrx.getTransform().rotate(vwgl.Math.degreesToRadians(1),1,0,0);
		}
		
		this.postRedisplay();
	},
	
	/// Color picker part 2: Use a mouse event to begin the pick operation
	mouse:function(button, state, x, y) {
		if (button==jsglut.Mouse.kLeftButton && state==jsglut.Mouse.kUpState) {
			// if there is a previous selection, disable it
			if (this._selectedSolid) {
				var mat = this._selectedSolid.getGenericMaterial();
				if (mat) {
					mat.setSelectedMode(false);
				}
			}
			this._selectedDrawable = null;
			this._selectedSolid = null;

			// The most interesting step: the pick function will generate a frame using the object's color
			// pick identifier, and will take a sample in the supplied (x,y) position. Then it will check
			// if any object in the scene match with this pick identifier. And it make all with a single 
			// call to pick() function!!!
			var viewport = vwgl.State.get().getViewport();
			this._colorPicker.pick(this._sceneRoot,			// Scene
								   x,						// x sample coord
								   viewport.height() - y,	// y sample coord: note that the gl buffer is flipped respect the web canvas
								   viewport.width(),		// buffer width
								   viewport.height(),		// buffer height
								   vwgl.CameraManager.get().getMainCamera());	// camera
								  
			// that's all!: check the selection with ColorPicker.getDrawable() and ColorPicker.getSolid() functions
			if (this._selectedDrawable = this._colorPicker.getDrawable()) {
				base.debug.log("Object selected");
			}
			if (this._selectedSolid = this._colorPicker.getSolid()) {
				var mat = this._colorPicker.getSolid().getGenericMaterial();
				if (mat) {
					mat.setSelectedMode(true);
				}
			}
			this.display(); // Draw again
		}
	}

});

function main(canvasId) {
	var canvas = new jsglut.Canvas(canvasId);
	canvas.setScaleMode(jsglut.Canvas.kScaleModeEqual);
	canvas.setResizeMode(jsglut.Canvas.kResizeModeFitToWindow);
	
	jsglut.MainLoop.singleton().setCanvas(canvas);
	jsglut.MainLoop.singleton().run(new MyApp());
}
