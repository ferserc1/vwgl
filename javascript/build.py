#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import subprocess
import shutil
import json
import argparse
from subprocess import call

srcDir = 'src/'
javascriptFile = 'javascript/vwgl.js'

arguments = argparse.ArgumentParser(description="Compile plugins, javascript and style sheet files.")
arguments.add_argument('--src',help='Source directory')
arguments.add_argument('--js',help='Javascript output file, with path')
arguments.add_argument('--debug',action='store_true',help='do not minimize output javascript code')
arguments.add_argument('--install',action='store_true',help='generate production output files')

intermediatePath = 'tmp'
if (not os.path.exists(intermediatePath)):
	os.makedirs(intermediatePath)
	
args = arguments.parse_args()
if args.src:
	pluginDir = args.src

if args.js:
	javascriptFile = args.js

if args.install:
	jsOut = open(javascriptFile,'w')
else:
	jsOut = open(os.path.join(intermediatePath,'javascript_output.o'),'w')

srcFiles = os.listdir(srcDir)
srcFiles.sort()

for file in srcFiles:
	outPath = os.path.join(intermediatePath,file)
	outFile = open(outPath,'w')
	jsPath = srcDir + file
	outFile.write(open(jsPath).read())
	outFile.write('\n\n')
	outFile.close()

intermediateFiles = os.listdir(intermediatePath)
intermediateFiles.sort()

for file in intermediateFiles:
	filePath = os.path.join(intermediatePath,file)
	fileName, fileExtension = os.path.splitext(filePath)
	if not args.debug and fileExtension=='.js':
		command = "java -jar yuicompressor.jar " + filePath + " -o " + filePath
		print command
		subprocess.check_call(command,shell=True)
	print "adding " + filePath + " to " + javascriptFile
	jsOut.write(open(filePath).read())

jsOut.close()
shutil.rmtree(intermediatePath)

