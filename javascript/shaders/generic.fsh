#ifdef GL_ES
precision highp float;
precision highp	int;
#endif
varying vec3 vPosition;
varying vec2 vTextureCoord;
varying vec2 vLightmapCoord;
varying vec3 vNormal;
varying vec3 vTangent;
varying vec3 vBitangent;
varying vec3 vSurfaceToView;

uniform mat4 uVMatrix;

// Material data
uniform vec4 uDiffuseColor;
uniform vec4 uSpecularColor;
uniform float uShininess;
uniform float uLightEmission;

// Textures
uniform bool uUseTexture;
uniform sampler2D uTexture;
uniform bool uUseLightMap;
uniform sampler2D uLightMap;
uniform bool uUseNormalMap;
uniform sampler2D uNormalMap;
uniform vec2 uTextureOffset;
uniform vec2 uTextureScale;
uniform vec2 uLightmapOffset;
uniform vec2 uLightmapScale;
uniform vec2 uNormalMapOffset;
uniform vec2 uNormalMapScale;

uniform bool uUseCubeMap;
uniform samplerCube uCubeMap;
uniform float uReflectionAmount;

// Lighting
const int kDirectionalLightType = 4;
const int kSpotLightType = 1;
const int kPointLightType = 5;
const int kDisabledLightType = 10;


uniform vec4 uAmbientLight;
uniform vec4 uDiffuseLight;
uniform vec4 uSpecularLight;
uniform int uLightType;
uniform vec3 uLightPosition;
uniform vec3 uLightDirection;
uniform vec3 uAttenuation;	// constant, linear, exp
uniform float uSpotCutoff;
uniform float uSpotExponent;
uniform vec3 uSpotDirection;
uniform bool uSelectMode;


// Render settings
uniform float uHue;
uniform float uSaturation;
uniform float uLightness;
uniform float uBrightness;
uniform float uContrast;

vec3 getNormal(vec3 normalCoord, vec3 normalMapValue, vec3 tangent, vec3 bitangent, bool useNormalMap) {
	vec3 normal = normalCoord;
	if (useNormalMap) {
		mat3 tbnMat = mat3(	tangent.x, bitangent.x, normalCoord.x,
							tangent.y, bitangent.y, normalCoord.y,
							tangent.z, bitangent.z, normalCoord.z);
		normal = normalize(normalMapValue * tbnMat);
	}
	normalize(normal);
	return normal;
}

vec4 getTexColor() {
	vec4 color = vec4(1.0);
	if (uUseTexture) {
		color *= texture2D(uTexture, vTextureCoord * uTextureScale + uTextureOffset);
	}
	return color;
}

vec4 getCubeColor(vec3 normal) {
	vec3 surfaceToView = normalize(vSurfaceToView);
	vec3 reflected = reflect(surfaceToView,normal);
	return vec4((textureCube(uCubeMap,reflected)*uReflectionAmount).rgb,0.0);
}

vec4 getLightmapColor() {
	if (uUseLightMap) {
		return texture2D(uLightMap, vLightmapCoord * uLightmapScale + uLightmapOffset);
	}
	return vec4(1.0,1.0,1.0,1.0);
}

vec4 getLightingColor(	vec4 lightAmb, vec4 lightDiff, vec4 lightSpec,
						vec3 lightDir, vec3 lightPos, vec3 vertexPos,
						vec3 att, float spotCutoff, vec3 spotDirection, float spotExponent, int lightType,
						vec4 matDiff, vec4 matSpec, vec3 normal, float shininess) {
 	if (lightType==kDisabledLightType) return vec4(0.0,0.0,0.0,1.0);
	float attenuation;
	float spotlight = 1.0;
	if (lightType==kSpotLightType) {
		vec3 temp = lightDir;
		lightDir = spotDirection;
		spotDirection = lightDir;
	}
	if (lightType==kDirectionalLightType) {
		attenuation = 1.0;
	}
	else {
		vec3 posToLightSource = vec3(lightPos - vertexPos);
		float distance = length(posToLightSource);
		lightDir = normalize(posToLightSource);
		attenuation = 1.0 / (att.x + att.y * distance + att.z * distance * distance);
		if (lightType==kSpotLightType) {	
			float clampedCos = max(0.0, dot(lightDir,spotDirection));
			if (clampedCos<cos(radians(spotCutoff))) {
				spotlight = 0.0;
			}
			else {
				spotlight = pow(clampedCos, spotExponent);
			}
		}
	}
	vec3 color = (uLightEmission + lightAmb.rgb) * matDiff.rgb;
	vec3 diffuseWeight = max(0.0,dot(normal,lightDir)) * lightDiff.rgb * attenuation;
	color += (diffuseWeight + uLightEmission) * matDiff.rgb;
	if (shininess>0.0) {
		vec3 eyeDirection = normalize(-vertexPos);
		vec3 reflectionDirection = normalize(reflect(-lightDir, normal));
		float specularWeight = clamp(pow(max(dot(reflectionDirection, eyeDirection),0.0), shininess), 0.0, 1.0);
		color += specularWeight * lightSpec.rgb * matSpec.rgb * attenuation;
	}
	color *= spotlight;
	return vec4(color,1.0);
}

vec3 rgb2hsv(vec3 c) {
	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
	vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec4 applyBrightness(vec4 color, float brightness) {
	return clamp(vec4(color.rgb + brightness - 0.5,1.0),0.0,1.0);
}

vec4 applyContrast(vec4 color, float contrast) {
	return clamp(vec4((color.rgb * max(contrast + 0.5,0.0)),1.0),0.0,1.0);
}

vec4 applySaturation(vec4 color, float hue, float saturation, float lightness) {
	vec3 fragRGB = clamp(color.rgb + vec3(0.001),0.0,1.0);
	vec3 fragHSV = rgb2hsv(fragRGB);
	lightness -= 0.01;
	float h = hue;
	fragHSV.x *= h;
	fragHSV.yz *= vec2(saturation,lightness);
	fragHSV.x = mod(fragHSV.x, 1.0);
	fragHSV.y = mod(fragHSV.y, 1.0);
	fragHSV.z = mod(fragHSV.z, 1.0);
	fragRGB = hsv2rgb(fragHSV);
	return clamp(vec4(hsv2rgb(fragHSV), color.w),0.0,1.0);
}

vec4 postprocess(vec4 fragColor) {
	return applyContrast(applyBrightness(applySaturation(fragColor,uHue,uSaturation,uLightness),uBrightness),uContrast);
}
	
void main(void) {
	vec4 color = vec4(1.0);
	vec3 normalMap = texture2D(uNormalMap, vTextureCoord * uNormalMapScale + uNormalMapOffset).xyz * 2.0 - 1.0;
	// TODO: lightNormalMap con vLightmapCoord
	vec3 normal = getNormal(vNormal, normalMap, vTangent, vBitangent, uUseNormalMap);
	gl_FragColor = vec4(normal,1.0);
	vec4 diffuseColor = getTexColor() * getLightmapColor() * uDiffuseColor;
	
	vec4 totalLightColor = getLightingColor(
		uAmbientLight, uDiffuseLight, uSpecularLight,
		uLightDirection, uLightPosition, vPosition,
		uAttenuation, uSpotCutoff, uSpotDirection, uSpotExponent,
		uLightType, diffuseColor, uSpecularColor, normal, uShininess);
	
	color *= totalLightColor;
	if (uSelectMode) {
		color = clamp(color * vec4(0.5,0.5,0.5,1.0) + vec4(0.0,1.0,0.0,1.0),0.0,1.0);
	}
	if (uUseCubeMap) {
		color = color + getCubeColor(normal);
	}
	gl_FragColor = postprocess(color);
}