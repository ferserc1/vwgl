#ifdef GL_ES
precision highp float;
precision highp	int;
#endif
attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute vec3 aVertexTangent;
attribute vec2 aTextureCoord;
attribute vec2 aLightmapCoord;


uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uNMatrix;
uniform mat4 uVMatrix;
uniform mat4 uMMatrix;
uniform mat4 uVMatrixInv;

varying vec2 vTextureCoord;
varying vec2 vLightmapCoord;
varying vec3 vNormal;
varying vec3 vTangent;
varying vec3 vBitangent;
varying vec3 vPosition;
varying vec3 vSurfaceToView;


void main() {
	gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aVertexPosition,1.0);
	
	vNormal = normalize((uNMatrix * vec4(aVertexNormal,1.0)).rgb);
	vTangent = normalize((uNMatrix * vec4(aVertexTangent,1.0)).rgb);
	vBitangent = cross(vNormal,vTangent);
	
	vTextureCoord = aTextureCoord;
	vLightmapCoord = aLightmapCoord;
	vPosition = (uMVMatrix * vec4(aVertexPosition,1.0)).xyz;
		
	vSurfaceToView = (uVMatrixInv[3] - (uMMatrix * vec4(aVertexPosition,1.0))).xyz;
}
