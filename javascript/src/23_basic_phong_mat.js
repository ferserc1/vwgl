	
Class ("vwgl.BasicPhongMaterial", vwgl.Material, {
	_ambient:null, 			// vwgl.Vector4
	_diffuse:null, 			// vwgl.Vector4
	_lightDirection:null, 	// vwgl.Vector3
	_texture:null,
	
	// This material have built-in shaders, so it only have a synchronous load
	initialize:function(name) {
		this.parent(name);
		this._ambient = new vwgl.Color(0.2,0.2,0.2,1.0);
		this._diffuse = new vwgl.Color(1.0);
		this._lightDirection = new vwgl.Vector3(0,0,-1);
		this.initShader();
	},
	
	initShader:function() {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.BasicPhongMaterial._vshader);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.BasicPhongMaterial._fshader);
		this.getShader().link("basic_phong_material");
		this.loadVertexAttrib("aVertexPosition");
		this.loadNormalAttrib("aVertexNormal");
		this.loadTexCoord0Attrib("aTexCoord0");
		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uNMatrix");
		this.getShader().initUniformLocation("uAmbient");
		this.getShader().initUniformLocation("uDiffuse");
		this.getShader().initUniformLocation("uLightDirection");
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uUseTexture");
		
	},
	
	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		this.getShader().setUniform("uNMatrix", this.normalMatrix());
		this.getShader().setUniform("uLightDirection", this._lightDirection);
		this.getShader().setUniform("uAmbient", this._ambient);
		this.getShader().setUniform("uDiffuse", this._diffuse);
		this.getShader().setUniform1i("uUseTexture", this._texture!=null);
		this.getShader().setUniform("uTexture", this._texture, vwgl.Texture.kTexture0);
	},
	
	ambient:function() { return this._ambient; },
	diffuse:function() { return this._diffuse; },
	texture:function() { return this._texture; },
	setAmbient:function(/* vwgl.Vector4 */ ambient) { this._ambient = ambient; },
	setDiffuse:function(/* vwgl.Vector4 */ diffuse) { this._diffuse = diffuse; },
	setTexture:function(/* vwgl.Texture */ tex) {
		if (this._texture) vwgl.TextureManager.unrefTexture(this._texture);
		this._texture = tex;
	},
	
	setLightDirection:function(/* vwgl.Vector3 */ dir) {
		this._lightDirection.assign(dir);
		this._lightDirection.normalize();
	}
});

vwgl.BasicPhongMaterial._vshader = "\
	attribute vec3 aVertexPosition;\n\
	attribute vec3 aVertexNormal;\n\
	attribute vec2 aTexCoord0;\n\
	\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
	uniform mat4 uNMatrix;\n\
	\n\
	varying vec4 vNormal;\n\
	varying vec2 vTexCoord;\n\
	void main() {\n\
		vNormal = uNMatrix * vec4(aVertexNormal,1.0);\n\
		vTexCoord = aTexCoord0;\n\
		gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	}";

vwgl.BasicPhongMaterial._fshader =  "\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	\n\
	uniform vec4 uAmbient;\n\
	uniform vec4 uDiffuse;\n\
	uniform vec3 uLightDirection;\n\
	uniform sampler2D uTexture;\n\
	uniform bool uUseTexture;\n\
	varying vec4 vNormal;\n\
	varying vec2 vTexCoord;\n\
	void main() {\n\
		float diffuseLightWeight = max(dot(normalize(vNormal.xyz), -uLightDirection), 0.0);\n\
		vec4 texColor = vec4(1.0);\n\
		if (uUseTexture) {\n\
			texColor = texture2D(uTexture,vTexCoord);\n\
		}\n\
		gl_FragColor = clamp(uAmbient + diffuseLightWeight * uDiffuse * texColor,0.0,1.0);\n\
	}";
