
Class ("vwgl.Plane", vwgl.Drawable, {
	initialize:function(a,b) {
		this.parent();
		if (a && !b) this.buildPlane(a,a);
		else if (a && b) this.buildPlane(a,b);
		else this.buildPlane(1,1);
	},

	buildPlane:function(width, height) {
		var x = width / 2.0;
		var y = height / 2.0;
		
		var v =[	-x,0.000000,-y,
					 x,0.000000,-y,
					 x,0.000000, y,
					 x,0.000000, y,
					-x,0.000000, y,
					-x,0.000000,-y];
		var n = [	0.000000,1.000000,0.000000,
					0.000000,1.000000,0.000000,
					0.000000,1.000000,0.000000,
					0.000000,1.000000,0.000000,
					0.000000,1.000000,0.000000,
					0.000000,1.000000,0.000000];
		var t0 = [	0.000000,0.000000,
					1.000000,0.000000,
					1.000000,1.000000,
					1.000000,1.000000,
					0.000000,1.000000,
					0.000000,0.000000];
		var t1 = [	0.000000,0.000000,
					1.000000,0.000000,
					1.000000,1.000000,
					1.000000,1.000000,
					0.000000,1.000000,
					0.000000,0.000000];
		var i = [2,1,0,5,4,3];
		
		var plist = new vwgl.PolyList();
		plist.addVertexVector(v);
		plist.addNormalVector(n);
		plist.addTexCoord0Vector(t0);
		plist.addTexCoord1Vector(t1);
		plist.addIndexVector(i);

		plist.buildPolyList();

		this.addSolid(new vwgl.Solid(plist));
	}
});