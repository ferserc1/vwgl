
Class ("vwgl.Projector", vwgl.Node, {
	_projection:null,			// vwgl.Matrix4
	_transform:null,			// vwgl.Matrix4
	_attenuation:0,
	_transformVisitor:null,		// vwgl.TransformVisitor
	_fooTransform:null,			// vwgl.Matrix4
	_texture:null,				// vwgl.Texture
	_visible:true,				// bool

	initialize:function() {
		this._projection = vwgl.Matrix4.makePerspective(vwgl.Math.degreesToRadians(45.0), 1.0, 0.1, 100.0);
		this._transform = vwgl.Matrix4.makeIdentity();
		this._transformVisitor = new vwgl.TransformVisitor();
		this._attenuation = 1.0;
		vwgl.ProjectorManager.get()._addProjector(this);
	},

	setProjection:function(/* Matrix4 */ proj) { this._projection.assign(proj); },
	getProjection:function() { return this._projection; },
	getProjection:function() { return this._projection; },
	
	setTransform:function(/* Matrix4 */ trans) { this._transform.assign(trans); },
	getRawTransform:function() { return this._transform; },
	
	setAttenuation:function(att) { this._attenuation = att; },
	getAttenuation:function() { return this._attenuation; },
	
	getPosition:function() {
		this._transformVisitor.visit(this);
		var tr = this._transformVisitor.getTransform();
		return tr.getPosition();
	},

	getDirection:function() {
		var look = new vwgl.Vector3(0.0,0.0,1.0);
		var cam = vwgl.CameraManager.get().getMainCamera();
		this._transformVisitor.visit(this);
		var tr = this._transformVisitor.getTransform();
		if (cam) {
			var vm = new vwgl.Matrix4(cam.getViewMatrix());
			vm.setRow(3, vwgl.Vector4(0.0, 0.0, 0.0, 1.0));
			vm.mult(tr);
			look = vm.multVector(look).xyz();
		}
		else {
			look = tr.multVector(look).xyz();
		}
		look.normalize();
		return look;
	},
	
	getTransform:function() {
		this._transformVisitor.visit(this);
		return this._transformVisitor.getTransform().mult(this._transform);
	},
	
	setTexture:function(/* Texture */ tex) { this._texture = tex; },
	getTexture:function() { return this._texture; },

	setVisible:function(v) { this._visible = v; },
	isVisible:function() { return this._visible; },
	show:function() { this._visible = true; },
	hide:function() { this._visible = false; },
	
	destroy:function() {
		vwgl.ProjectorManager.get()._removeProjector(this);
		this.parent();
	}
});

Class ("vwgl.ProjectorManager", {	
	_projectorList:null,		// vwgl.Projector []

	initialize:function() {
		this._projectorList = [];
	},
	
	getProjectorList:function() { return this._projectorList; },

	
	_addProjector:function(/* Projector */ proj) {
		var i = this._projectorList.indexOf(proj);
		if (i==-1) this._projectorList.push(proj);
	},

	_removeProjector:function(/* Projector */ proj) {
		var i = this._projectorList.indexOf(proj);
		if (i>-1) this._projectorList.splice(i,1);
	}
});

vwgl.ProjectorManager.s_singleton = null;
vwgl.ProjectorManager.get = function() {
	if (vwgl.ProjectorManager.s_singleton==null) {
		vwgl.ProjectorManager.s_singleton = new vwgl.ProjectorManager();
	}
	return vwgl.ProjectorManager.s_singleton;
}