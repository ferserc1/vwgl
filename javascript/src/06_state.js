Class ("vwgl.State",{
	_modelMatrix:null,
	_viewMatrix:null,
	_projectionMatrix:null,

	_modelViewMatrix:null,
	_normalMatrix:null,

	_modelMatrixStack:null,
	_viewMatrixStack:null,
	_projectionMatrixStack:null,
	
	_viewport:null,
	
	initialize:function() {
		this._modelMatrix = vwgl.Matrix4.makeIdentity();
		this._viewMatrix = vwgl.Matrix4.makeIdentity();
		this._projectionMatrix = vwgl.Matrix4.makeIdentity();
		
		this._modelMatrixStack = [];
		this._viewMatrixStack = [];
		this._projectionMatrixStack = [];
		
		this._viewport = new vwgl.Viewport();
	},

	viewMatrix:function() {
		this._modelViewMatrix = null;
		this._normalMatrix = null;
		return this._viewMatrix;
	},

	modelMatrix:function() {
		this._modelViewMatrix = null;
		this._normalMatrix = null;
		return this._modelMatrix;
	},

	projectionMatrix:function() { return this._projectionMatrix; },
	
	modelViewMatrix:function() {
		if (this._modelViewMatrix==null) {
			this._modelViewMatrix = new vwgl.Matrix4(this._viewMatrix);
			this._modelViewMatrix.mult(this._modelMatrix);
		}
		return this._modelViewMatrix;
	},

	normalMatrix:function() {
		if (this._normalMatrix==null) {
			this._normalMatrix = new vwgl.Matrix4(this.modelViewMatrix());
			this._normalMatrix.invert();
			this._normalMatrix.traspose();
		}
		return this._normalMatrix;
	},

	pushViewMatrix:function() {
		this._viewMatrixStack.push(new vwgl.Matrix4(this._viewMatrix));
	},

	popViewMatrix:function() {
		this._viewMatrix.assign(this._viewMatrixStack.pop());
		this._modelViewMatrix = null;
		this._normalMatrix = null;
		return this._viewMatrix;
	},
	
	pushModelMatrix:function() {
		this._modelMatrixStack.push(new vwgl.Matrix4(this._modelMatrix));
	},
	
	popModelMatrix:function() {
		this._modelMatrix.assign(this._modelMatrixStack.pop());
		this._modelViewMatrix = null;
		this._normalMatrix = null;
		return this._modelMatrix;
	},

	pushProjectionMatrix:function() {
		this._projectionMatrixStack.push(new vwgl.Matrix4(this._projectionMatrix));
	},
	
	popProjectionMatrix:function() {
		this._projectionMatrix.assign(this._projectionMatrixStack.pop());
		return this._projectionMatrix;
	},
	
	setOpenGLState:function() {
		base.debug.log("vwgl.State.setOpenGLState() called. This function does nothing in JavaScript API.");
	},
	
	setViewport:function(vp) {
		this._viewport.assign(vp);
		gl.viewport(vp.x(),vp.y(),vp.width(),vp.height());
	},

	getViewport:function() { return this._viewport; }
});

vwgl.State.s_singleton = null;
vwgl.State.get = function() {
	if (vwgl.State.s_singleton==null) {
		vwgl.State.s_singleton = new vwgl.State();
	}
	return vwgl.State.s_singleton;
}

vwgl.State.set = function(state) {
	vwgl.State.s_singleton = state;
}

