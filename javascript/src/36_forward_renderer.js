
Class ("vwgl.ForwardRenderer", vwgl.Renderer, {
	_clearColor:null,			// vwgl.Color
	_sceneRenderer:null,		// vwgl.SceneRenderer
	_initVisitor:null,			// vwgl.InitVisitor
	_projectorMat:null,			// vwgl.ProjTextureDeferredMaterial
	_mapSize:null,				// vwgl.Size2D

	initialize:function() {
		this._clearColor = vwgl.Color.black();
		this._initVisitor = new vwgl.InitVisitor();
		this._mapSize = new vwgl.Size2D();
		this.parent();
	},
	
	build:function(/* Group */ sceneRoot, /* Size2D | number */ sizeOrW, height) {
		if (dynamic_cast("vwgl.Size2D",sizeOrW)) {
			this._mapSize.assign(sizeOrW);
		}
		else {
			this._mapSize.set(sizeOrW,height);
		}
		this._sceneRoot = sceneRoot;
		this._projectorMat = new vwgl.ProjTextureDeferredMaterial();
		this._sceneRenderer = new vwgl.SceneRenderer();
	},
	
	destroy:function() {
		
	},

	draw:function(/* Camera | null */ cam) {
		vwgl.RenderSettings.setCurrentRenderSettings(this.getRenderSettings());
		this._initVisitor.visit(this._sceneRoot);

		vwgl.LightManager.get().prepareFrame(true);

		vwgl.State.get().setViewport(this._viewport);
		gl.clearColor(this._clearColor.r(), this._clearColor.g(), this._clearColor.b(), this._clearColor.a());
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		vwgl.CameraManager.get().applyTransform(cam);

		this._sceneRenderer.getNodeVisitor().setMaterial(null);
		this._sceneRenderer.getNodeVisitor().setUseMaterialSettings(false);

		gl.enable(gl.POLYGON_OFFSET_FILL);
		gl.polygonOffset(1.0, 1.0);
		this._sceneRenderer.draw(this._sceneRoot,true);
		gl.disable(gl.POLYGON_OFFSET_FILL);

		// Render projectors
		this._sceneRenderer.getNodeVisitor().setMaterial(this._projectorMat);
		this._sceneRenderer.getNodeVisitor().setUseMaterialSettings(true);

		gl.depthMask(false);
		var proj = vwgl.ProjectorManager.get().getProjectorList();
		for (var i=0;i<proj.length;++i) {
			if (proj[i].isVisible()) {
				gl.enable(gl.BLEND);
				gl.blendFunc(gl.DST_COLOR, gl.ONE_MINUS_SRC_ALPHA);
				this._projectorMat.setProjector(proj[i]);
				this._sceneRenderer.draw(this._sceneRoot,true);
			}
		}

		gl.disable(gl.BLEND);
		gl.depthMask(true);
	},
	
	resize:function(/* Size2D */ size) {
		
	},

	setClearColor:function(/* Color */ color) { this._clearColor.assign(color); }

});
