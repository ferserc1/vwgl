Class ("vwgl.ProjTextureDeferredMaterial", vwgl.DeferredMaterial, {
	_projector:null, // vwgl.Projector

	initialize:function() {
		this.initShaderSync(null);
	},
	
	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.ProjTextureDeferredMaterial.s_vshader);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.ProjTextureDeferredMaterial.s_fshader);
		this.getShader().link();

		this.loadVertexAttrib("aVertexPosition");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uMMatrix");

		this.getShader().initUniformLocation("uTexCoordMatrix");

		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uReceiveProjections");
	},
	
	setupUniforms:function() {
		var mat = this.getSettingsMaterial();
		if (this._projector && mat) {
			this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
			this.getShader().setUniform("uPMatrix", this.projectionMatrix());
			this.getShader().setUniform("uMMatrix", this.modelMatrix());
	
			var texCoordMatrix = new vwgl.Matrix4(vwgl.ProjTextureDeferredMaterial.s_texCoordGenMatrix);
			var projMatrix = this._projector.getProjection();
			var modelMatrix = this._projector.getTransform();
			modelMatrix.invert();
	
			texCoordMatrix.mult(projMatrix).mult(modelMatrix);
			this.getShader().setUniform("uTexCoordMatrix", texCoordMatrix);
			this.getShader().setUniform("uTexture", this._projector.getTexture(), vwgl.Texture.kTexture0);
			this.getShader().setUniform1i("uReceiveProjections", mat.getReceiveProjections());
		}
	},

	setProjector:function(/* Projector */ proj) { this._projector = proj; },
	getProjector:function() { return this._projector; },	
});

vwgl.ProjTextureDeferredMaterial.s_texCoordGenMatrix = new vwgl.Matrix4(0.5,0.0,0.0,0.0,
																  	 	0.0,0.5,0.0,0.0,
																  	  	0.0,0.0,0.5,0.0,
																  	  	0.5,0.5,0.5,1.0);

vwgl.ProjTextureDeferredMaterial.s_vshader = "#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	attribute vec3 aVertexPosition;\n\
	attribute vec2 aTexturePosition;\n\
	\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
	uniform mat4 uMMatrix;\n\
	\n\
	uniform mat4 uTexCoordMatrix;\n\
	//uniform mat4 uInvViewMatrix;\n\
	\n\
	//varying vec2 vTexturePosition;\n\
	varying vec4 vPosition;\n\
	varying vec4 vTexturePosition;\n\
	\n\
	void main() {\n\
		vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
		gl_Position = vPosition;\n\
		\n\
		vec3 direction = vec3(0.0,0.0,-1.0);\n\
	\n\
		vec4 posWorld = uMMatrix * vec4(aVertexPosition,1.0);\n\
		//vTexturePosition = aTexturePosition;\n\
		vTexturePosition = uTexCoordMatrix * posWorld;\n\
	}";

vwgl.ProjTextureDeferredMaterial.s_fshader = "#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	\n\
	//varying vec2 vTexturePosition;\n\
	varying vec4 vTexturePosition;\n\
	varying vec4 vPosition;\n\
	\n\
	uniform sampler2D uTexture;\n\
	uniform bool uReceiveProjections;\n\
	\n\
	void main(void) {\n\
		if (uReceiveProjections) {\n\
			vec4 texColor = vec4(1.0);\n\
		\n\
			if (vTexturePosition.q>0.0) {\n\
				texColor = texture2DProj(uTexture,vTexturePosition);\n\
			}\n\
			gl_FragColor = texColor;\n\
		}\n\
		else {\n\
			discard;\n\
		}\n\
	}";
