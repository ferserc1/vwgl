
Class ("vwgl.PolyList",{
	
	_name:"",
	_materialName:"",
	_vertex:null,
	_normal:null,
	_texCoord0:null,
	_texCoord1:null,
	_texCoord2:null,
	_color:null,
	_index:null,
	_vertexBuffer:null,
	_normalBuffer:null,
	_texCoord0Buffer:null,
	_texCoord1Buffer:null,
	_texCoord2Buffer:null,
	_colorBuffer:null,
	_indexBuffer:null,
	_drawMode:null,
	
	_tangent:null,
	_tangentBuffer:null,
	
	initialize:function(cloneObj) {
		this._vertex = [];
		this._normal = [];
		this._texCoord0 = [];
		this._texCoord1 = [];
		this._texCoord2 = [];
		this._color = [];
		this._index = [];

		if (cloneObj && dynamic_cast("vwgl.PolyList",cloneObj)) {
			this._name = cloneObj._name;
			this._materialName = cloneObj._materialName;
			this._drawMode = cloneObj._drawMode;

			for (var i=0; i<cloneObj._vertex; ++i) { this._vertex.push(cloneObj._vertex[i]); }
			for (var i=0; i<cloneObj._normal; ++i) { this._normal.push(cloneObj._normal[i]); }
			for (var i=0; i<cloneObj._texCoord0; ++i) { this._texCoord0.push(cloneObj._texCoord0[i]); }
			for (var i=0; i<cloneObj._texCoord1; ++i) { this._texCoord1.push(cloneObj._texCoord1[i]); }
			for (var i=0; i<cloneObj._texCoord2; ++i) { this._texCoord2.push(cloneObj._texCoord2[i]); }
			for (var i=0; i<cloneObj._color; ++i) { this._color.push(cloneObj._color[i]); }
			for (var i=0; i<cloneObj._index; ++i) { this._index.push(cloneObj._index[i]); }
			
			this.buildPolyList();
		}
		else {
			this._drawMode = vwgl.PolyList.kTriangles;
		}
		
	},
	
	setName:function(name) { this._name = name; },
	getName:function() { return this._name; },
	setMaterialName:function(name) { this._materialName = name; },
	getMaterialName:function() { return this._materialName; },

	addVertex:function(/* v: vwgl.Vector3 */ v) { this._vertex.push(v.x()); this._vertex.push(v.y()); this._vertex.push(v.z()); },
	addNormal:function(/* v: vwgl.Vector3 */ n) { this._normal.push(n.x()); this._normal.push(n.y()); this._normal.push(n.z()); },
	addTexCoord0:function(/* v: vwgl.Vector2 */ tc) { this._texCoord0.push(tc.x()); this._texCoord0.push(tc.y()); },
	addTexCoord1:function(/* v: vwgl.Vector2 */ tc) { this._texCoord1.push(tc.x()); this._texCoord1.push(tc.y()); },
	addTexCoord2:function(/* v: vwgl.Vector2 */ tc) { this._texCoord2.push(tc.x()); this._texCoord2.push(tc.y()); },
	addColor:function(/* vwgl.Color */ c) { this._color.push(c.r()); this._color.push(c.g()); this._color.push(c.b()); this._color.push(c.a()); },
	addIndex:function(/* number */ i) { this._index.push(i); },
	addTriangle:function(/* number */ v0, /* number */ v1, /* number */ v2) { this._index.push(v0); this._index.push(v1); this._index.push(v2); },
	numberOfIndices:function() { return this._index.length; },
	
	addVertexVector:function( /* float [] */ v) { for (var i=0;i<v.length;++i) this._vertex.push(v[i]); },
	addNormalVector:function( /* float [] */ v) { for (var i=0;i<v.length;++i) this._normal.push(v[i]); },
	addTexCoord0Vector:function( /* float [] */ v) { for (var i=0;i<v.length;++i) this._texCoord0.push(v[i]); },
	addTexCoord1Vector:function( /* float [] */ v) { for (var i=0;i<v.length;++i) this._texCoord1.push(v[i]); },
	addTexCoord2Vector:function( /* float [] */ v) { for (var i=0;i<v.length;++i) this._texCoord2.push(v[i]); },
	addColorVector:function( /* float[] */ v) { for (var i=0; i<v.length;++i) this._color.push(v[i]); },
	addIndexVector:function( /* number [] */ v) { for (var i=0;i<v.length;++i) this._index.push(v[i]); },

	
	buildPolyList:function() {
		this.destroyGlBuffers();
		if (this._vertex.length>0) {
			this._vertexBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER,this._vertexBuffer);
			gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(this._vertex),gl.STATIC_DRAW);
			this._vertexBuffer.itemSize = 3;
			this._vertexBuffer.numItems = this._vertex.length/3;
		}

		if (this._normal.length>0) {
			this._normalBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this._normalBuffer);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this._normal), gl.STATIC_DRAW);
			this._normalBuffer.itemSize = 3;
			this._normalBuffer.numItems = this._normal.length/3;
		}

		if (this._texCoord0.length>0) {
			this._texCoord0Buffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this._texCoord0Buffer);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this._texCoord0), gl.STATIC_DRAW);
			this._texCoord0Buffer.itemSize = 2;
			this._texCoord0Buffer.numItems = this._texCoord0.length/2;
		}

		if (this._texCoord1.length>0) {
			this._texCoord1Buffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this._texCoord1Buffer);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this._texCoord1), gl.STATIC_DRAW);
			this._texCoord1Buffer.itemSize = 2;
			this._texCoord1Buffer.numItems = this._texCoord1.length/2;
		}
		// TODO: Arreglar esto. En deferred render, tal y como está, es imprescindible tener coordenadas de textura
		// en la unidad 1. Si no hay coordenadas de textura se tendría que pintar siempre en blanco, y para eso hay que
		// crear otro shader que ignore el atributo de coordenadas de textura
		else if (this._texCoord0.length>0) {
			this._texCoord1Buffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this._texCoord1Buffer);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this._texCoord0), gl.STATIC_DRAW);
			this._texCoord1Buffer.itemSize = 2;
			this._texCoord1Buffer.numItems = this._texCoord0.length/2;
		}

		if (this._texCoord2.length>0) {
			this._texCoord2Buffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this._texCoord2Buffer);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this._texCoord2), gl.STATIC_DRAW);
			this._texCoord2Buffer.itemSize = 2;
			this._texCoord2Buffer.numItems = this._texCoord2.length/2;
		}
		
		if (this._color.length>0) {
			this._colorBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, this._colorBuffer);
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this._color),gl.STATIC_DRAW);
			this._colorBuffer.itemSize = 4;
			this._colorBuffer.numItems = this._color.length/4;
		}

		if (this._index.length>0) {
			this._indexBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._indexBuffer);
			
			// Check: Esto puede ser el problema de que algunos modelos no se pintaran, en la versión anterior el array era Uint16Array.
			// Comprobar que esto funciona bien con Uint32Array
			// Mirar también la función drawElements, que se le dice también el formato. Ahora está en gl.UNSIGNED_INT
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this._index),gl.STATIC_DRAW);
			this._indexBuffer.itemSize = 3;
			this._indexBuffer.numItems = this._index.length;
		}
		this._calculateTangent();
	},

	destroyGlBuffers:function() {
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

		if (this._vertexBuffer) {
			gl.deleteBuffer(this._vertexBuffer);
			this._vertexBuffer = null;
		}
		if (this._normalBuffer) {
			gl.deleteBuffer(this._normalBuffer);
			this._normalBuffer = null;
		}
		if (this._texCoord0Buffer) {
			gl.deleteBuffer(this._texCoord0Buffer);
			this._texCoord0Buffer = null;
		}
		if (this._texCoord1Buffer) {
			gl.deleteBuffer(this._texCoord1Buffer);
			this._texCoord1Buffer = null;
		}
		if (this._texCoord2Buffer) {
			gl.deleteBuffer(this._texCoord2Buffer);
			this._texCoord2Buffer = null;
		}
		if (this._colorBuffer) {
			gl.deleteBuffer(this._colorBuffer);
			this._colorBuffer = null;
		}
		if (this._indexBuffer) {
			gl.deleteBuffer(this._indexBuffer);
			this._indexBuffer = null;
		}
		if (this._tangentBuffer) {
			gl.deleteBuffer(this._tangentBuffer);
			this._tangentBuffer = null;
		}
	},

	destroy:function() {
		this.destroyGlBuffers();
		this._vertex = [];
		this._normal = [];
		this._texCoord0 = [];
		this._texCoord1 = [];
		this._texCoord2 = [];
		this._color = [];
		this._index = [];
		this._tangent = [];
	},
	
	// Access to the VBOs
	getVertexBuffer:function() { return this._vertexBuffer; },
	getNormalBuffer:function() { return this._normalBuffer; },
	getTexCoord0Buffer:function() { return this._texCoord0Buffer; },
	getTexCoord1Buffer:function() { return this._texCoord1Buffer; },
	getTexCoord2Buffer:function() { return this._texCoord2Buffer; },
	getColorBuffer:function() { return this._colorBuffer; },
	getIndexBuffer:function() { return this._indexBuffer; },
	getTangentBuffer:function() { return this._tangentBuffer; },
	
	// Access to the arrays
	getVertexPointer:function() { return this._vertex; },
	getNormalPointer:function() { return this._vertex; },
	getTexCoord0Pointer:function() { return this._texCoord0; },
	getTexCoord1Pointer:function() { return this._texCoord1; },
	getTexCoord2Pointer:function() { return this._texCoord2; },
	getColorPointer:function() { return this._color; },
	getIndexPointer:function() { return this._index; },
	getTangentPointer:function() { return this._tangent; },
	
	// In C++ this functions returns the std::vector objects, and the "Access to arrays" functions defined
	// above returns the c pointers to that vectors.
	// In Javascript API the following functions returns the same as the "access to array" functions, but both
	// versions are included to make easy to port the c++ code to Javascript
	getVertexVector:function() { return this._vertex; },
	getNormalVector:function() { return this._normal; },
	getTexCoord0Vector:function() { return this._texCoord0; },
	getTexCoord1Vector:function() { return this._texCoord1; },
	getTexCoord2Vector:function() { return this._texCoord2; },
	getColorVector:function() { return this._color; },
	getIndexVector:function() { return this._index; },
	getTangentVector:function() { return this._index; },
	
	getVertexCount:function() { return this._vertex.length; },
	getNormalCount:function() { return this._normal.length; },
	getTexCoord0Count:function() { return this._texCoord0.length; },
	getTexCoord1Count:function() { return this._texCoord1.length; },
	getTexCoord2Count:function() { return this._texCoord2.length; },
	getColorCount:function() { return this._color.length; },
	getIndexCount:function() { return this._index.length; },
	getTangentCount:function() { return this._tangent.length; },
	
	setDrawMode:function(mode) { this._drawMode = mode; },
	getDrawMode:function() { return this._drawMode; },
	
	drawElements:function() {
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.getIndexBuffer());
		gl.drawElements(vwgl.PolyList.getGLDrawMode(this._drawMode), this.numberOfIndices(), gl.UNSIGNED_SHORT, 0);
	},
	
	clone:function() {
		return new vwgl.PolyList(this);
	},

	applyTransform:function(/* vwgl.Matrix4 */ trx) {
		var transform = new vwgl.Matrix4(trx);
		var rotation = new vwgl.Matrix4(trx.getMatrix3());

		if (this._normal.length>0 && this._normal.length!=this._vertex.length) throw new Error("Unexpected number of normal coordinates found in polyList");
		for (var i=0;i<this._vertex.length-2;i+=3) {
			var vertex = new vwgl.Vector4(this._vertex[i],this._vertex[i+1], this._vertex[i+2], 1.0);
			vertex = transform.multVector(vertex);
			this._vertex[i] = vertex.x();
			this._vertex[i+1] = vertex.y();
			this._vertex[i+2] = vertex.z();
	
			if (this._normal.length) {
				var normal = new vwgl.Vector4(this._normal[i],this._normal[i+1], this._normal[i+2], 1.0);
				normal = rotation.multVector(normal);
				this._normal[i] = normal.x();
				this._normal[i+1] = normal.y();
				this._normal[i+2] = normal.z();
			}
		}
		this.buildPolyList();
	},
	
	_calculateTangent:function() {
		this._tangent = null;
		if (this._tangentBuffer) {
			gl.deleteBuffer(this._tangentBuffer);
			this._tangentBuffer = null;
		}

		if (!this._texCoord0 || !this._vertex) return;
		this._tangent = [];
		for (var i=0; i<this._index.length - 2; i+=3) {
			var v0i = this._index[i] * 3;
			var v1i = this._index[i + 1] * 3;
			var v2i = this._index[i + 2] * 3;
			
			var t0i = this._index[i] * 2;
			var t1i = this._index[i + 1] * 2;
			var t2i = this._index[i + 2] * 2;
			
			var v0 = new vwgl.Vector3(this._vertex[v0i], this._vertex[v0i + 1], this._vertex[v0i + 2]);
			var v1 = new vwgl.Vector3(this._vertex[v1i], this._vertex[v1i + 1], this._vertex[v1i + 2]);
			var v2 = new vwgl.Vector3(this._vertex[v2i], this._vertex[v2i + 1], this._vertex[v2i + 2]);
			
			var t0 = new vwgl.Vector2(this._texCoord0[t0i], this._texCoord0[t0i + 1]);
			var t1 = new vwgl.Vector2(this._texCoord0[t1i], this._texCoord0[t1i + 1]);
			var t2 = new vwgl.Vector2(this._texCoord0[t2i], this._texCoord0[t2i + 1]);
			
			var edge1 = (new vwgl.Vector3(v1)).sub(v0);
			var edge2 = (new vwgl.Vector3(v2)).sub(v0);
			
			var deltaU1 = t1.x() - t0.x();
			var deltaV1 = t1.y() - t0.y();
			var deltaU2 = t2.x() - t0.x();
			var deltaV2 = t2.y() - t0.y();
			
			var f = 1.0 / (deltaU1 * deltaV2 - deltaU2 * deltaV1);
			
			var tangent = new vwgl.Vector3(f * (deltaV2 * edge1.x() - deltaV1 * edge2.x()),
										  f * (deltaV2 * edge1.y() - deltaV1 * edge2.y()),
										  f * (deltaV2 * edge1.z() - deltaV1 * edge2.z()));
			tangent.normalize();
			this._tangent.push(tangent.x());
			this._tangent.push(tangent.y());
			this._tangent.push(tangent.z());
			
			this._tangent.push(tangent.x());
			this._tangent.push(tangent.y());
			this._tangent.push(tangent.z());
			
			this._tangent.push(tangent.x());
			this._tangent.push(tangent.y());
			this._tangent.push(tangent.z());
		}
		
		if (this._tangent.length>0) {
			this._tangentBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER,this._tangentBuffer);
			gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(this._tangent),gl.STATIC_DRAW);
			this._tangentBuffer.itemSize = 3;
			this._tangentBuffer.numItems = this._tangent.length/3;
		}
	}
});

vwgl.PolyList.kTriangles = 1;
vwgl.PolyList.kTriangleFan = 2;
vwgl.PolyList.kTriangleStrip = 3;
vwgl.PolyList.kDefault = 0;
	
vwgl.PolyList.getGLDrawMode = function(mode) {
	switch (mode) {
		case vwgl.PolyList.kTriangles:
			return gl.TRIANGLES; 
		case vwgl.PolyList.kTriangleFan:
			return gl.TRIANGLE_FAN;
		case vwgl.PolyList.kTriangleStrip:
			return gl.TRIANGLE_STRIP;
		case vwgl.PolyList.kDefault:
		default:
			return gl.TRIANGLES;
	}
}
