
// In c++ API there is a <Image> class, but here in Javascript version we have the standard javascript Image
// class that will use as our image data structure. All functions that work with images in Texture and TextureManager
// receive a standard Javascript Image class.

Class ("vwgl.Texture", {

	_textureName:null,
	_minFilter:null,
	_magFilter:null,
	_fileName:null,
	
	_references:0,
	
	initialize:function() { },
	
	createTexture:function() { this._textureName = gl.createTexture(); },

	bindTexture:function(target) { gl.bindTexture(vwgl.Texture.getGLTarget(target), this._textureName); },
	setMinFilter:function(target,filter) { this._minFilter = filter; gl.texParameteri(vwgl.Texture.getGLTarget(target), gl.TEXTURE_MIN_FILTER, vwgl.Texture.getGLFilter(filter)); },
	setMagFilter:function(target,filter) { this._magFilter = filter; gl.texParameteri(vwgl.Texture.getGLTarget(target), gl.TEXTURE_MAG_FILTER, vwgl.Texture.getGLFilter(filter)); },
	setTextureWrapS:function(target,wrap) { gl.texParameteri(vwgl.Texture.getGLTarget(target), gl.TEXTURE_WRAP_S, vwgl.Texture.getGLWrap(wrap)); },
	setTextureWrapT:function(target,wrap) { gl.texParameteri(vwgl.Texture.getGLTarget(target), gl.TEXTURE_WRAP_T, vwgl.Texture.getGLWrap(wrap));  },
	setImage:function(target, img, flipY) {
		if (flipY===undefined) flipY = true;
		if (flipY) gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(vwgl.Texture.getGLTarget(target),0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE, img);
		if (this._minFilter==vwgl.Texture.kFilterLinearMipmapLinear 	||
			this._minFilter==vwgl.Texture.kFilterLinearMipmapNearest 	||
			this._minFilter==vwgl.Texture.kFilterNearestMipmapLinear 	||
			this._minFilter==vwgl.Texture.kFilterNearestMipmapNearest	||
			this._magFilter==vwgl.Texture.kFilterLinearMipmapLinear		||
			this._magFilter==vwgl.Texture.kFilterLinearMipmapNearest 	||
			this._magFilter==vwgl.Texture.kFilterNearestMipmapLinear	||
			this._magFilter==vwgl.Texture.kFilterNearestMipmapNearest) {
			gl.generateMipmap(vwgl.Texture.getGLTarget(target));
		}
	},
	setImageRaw:function(target,width,height,data) {
		gl.texImage2D(vwgl.Texture.getGLTarget(target),0, gl.RGBA,width,height,0,gl.RGBA,gl.UNSIGNED_BYTE, data);
		if (this._minFilter==vwgl.Texture.kFilterLinearMipmapLinear 	||
			this._minFilter==vwgl.Texture.kFilterLinearMipmapNearest 	||
			this._minFilter==vwgl.Texture.kFilterNearestMipmapLinear 	||
			this._minFilter==vwgl.Texture.kFilterNearestMipmapNearest	||
			this._magFilter==vwgl.Texture.kFilterLinearMipmapLinear		||
			this._magFilter==vwgl.Texture.kFilterLinearMipmapNearest 	||
			this._magFilter==vwgl.Texture.kFilterNearestMipmapLinear	||
			this._magFilter==vwgl.Texture.kFilterNearestMipmapNearest) {
			gl.generateMipmap(vwgl.Texture.getGLTarget(target));
		}
	},

	// If you load a texture with TextureManager, destroy it using TextureManager.unrefTexture().
	// Use only Texture.destroy() if the texture hasn't image (example: a FBO texture)
	destroy:function() {
		gl.deleteTexture(this._textureName);
		this._textureName = null;
		this._minFilter = null;
		this._magFilter = null;
		this._fileName = null;
	},

	valid:function() { return this._textureName!=null; },
	
	getTextureName:function() { return this._textureName; },
	
	getFileName:function() { return this._fileName; },
	setFileName:function(fileName) { this._fileName = fileName; }
});

vwgl.Texture.getRandomTexture = function() {
	var randomTexture = new vwgl.Texture();
	randomTexture.createTexture();
	randomTexture.bindTexture(vwgl.Texture.kTargetTexture2D);
	
	var textureSize = vwgl.Texture.s_randomTextureSize;
	var dataSize = textureSize * textureSize * 3;
	var textureData = []
	for (var i=0; i<dataSize; i+=3) {
		var r = vwgl.Math.random() * 2.0 - 1.0;
		var g = vwgl.Math.random() * 2.0 - 1.0;
		var b = 0.0;
		
		var randVector = new vwgl.Vector3(r,g,b);
		randVector.normalize();
		
		textureData.push(randVector.x() * 255.0);
		textureData.push(randVector.y() * 255.0);
		textureData.push(randVector.z() * 255.0);
	}
	
	textureData = new Uint8Array(textureData);
	
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, textureSize, textureSize, 0, gl.RGB, gl.UNSIGNED_BYTE, textureData);
	
	randomTexture.setMagFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
	randomTexture.setMinFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
	
	
	vwgl.Texture.unbindTexture(vwgl.Texture.kTargetTexture2D);
	return randomTexture;
}

vwgl.Texture.getRandomTextureSize = function() {
	return vwgl.Texture.s_randomTextureSize;
}

vwgl.Texture.getColorTexture = function(/* Color */ color, /* Size2D */ size) {
	var colorTexture = new vwgl.Texture();
	colorTexture.createTexture();
	colorTexture.bindTexture(vwgl.Texture.kTargetTexture2D);

	var dataSize = size.x() * size.y() * 4;
	var textureData = [];
	for (var i = 0; i < dataSize; i+=4) {
		textureData[i]   = color.r() * 255;
		textureData[i+1] = color.g() * 255;
		textureData[i+2] = color.b() * 255;
		textureData[i+3] = color.a() * 255;
	}
	
	textureData = new Uint8Array(textureData);

	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, size.x(), size.y(), 0, gl.RGBA, gl.UNSIGNED_BYTE, textureData);
	
	colorTexture.setMagFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
	colorTexture.setMinFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);

	vwgl.Texture.unbindTexture(vwgl.Texture.kTargetTexture2D);
	return colorTexture;
}

vwgl.Texture.s_randomTextureSize = 128;

Class ("vwgl.CubeMap", vwgl.Texture,{

	_positiveXFile:null,
	_negativeXFile:null,
	_positiveYFile:null,
	_negativeYFile:null,
	_positiveZFile:null,
	_negativeZFile:null,
	
	setPositiveXFile:function(file) { this._positiveXFile = file; },
	setNegativeXFile:function(file) { this._negativeXFile = file; },
	setPositiveYFile:function(file) { this._positiveYFile = file; },
	setNegativeYFile:function(file) { this._negativeYFile = file; },
	setPositiveZFile:function(file) { this._positiveZFile = file; },
	setNegativeZFile:function(file) { this._negativeZFile = file; },
	
	getPositiveXFile:function() { return this._positiveXFile; },
	getNegativeXFile:function() { return this._negativeXFile; },
	getPositiveYFile:function() { return this._positiveYFile; },
	getNegativeYFile:function() { return this._negativeYFile; },
	getPositiveZFile:function() { return this._positiveZFile; },
	getNegativeZFile:function() { return this._negativeZFile; },

});

//vwgl.CubeMap.

vwgl.Texture.kTargetTexture2D = 110;
vwgl.Texture.kTargetTextureCubeMap = 112;
vwgl.Texture.kTexturePositiveXFace = 100;
vwgl.Texture.kTextureNegativeXFace = 101;
vwgl.Texture.kTexturePositiveYFace = 102;
vwgl.Texture.kTextureNegativeYFace = 103;
vwgl.Texture.kTexturePositiveZFace = 104;
vwgl.Texture.kTextureNegativeZFace = 105;
vwgl.Texture.getGLTarget = function(target) {
	if (target==vwgl.Texture.kTargetTexture2D) return gl.TEXTURE_2D;
	else if (target==vwgl.Texture.kTargetTextureCubeMap) return gl.TEXTURE_CUBE_MAP;
	return gl.TEXTURE_CUBE_MAP_POSITIVE_X + (target - vwgl.Texture.kTexturePositiveXFace);
}

vwgl.Texture.kFilterNearestMipmapNearest = 209;
vwgl.Texture.kFilterLinearMipmapNearest = 210;
vwgl.Texture.kFilterNearestMipmapLinear = 211;
vwgl.Texture.kFilterLinearMipmapLinear = 212;
vwgl.Texture.kFilterNearest = 213;
vwgl.Texture.kFilterLinear = 214;
vwgl.Texture.getGLFilter = function(filter) {
	switch (filter) {
	case vwgl.Texture.kFilterNearestMipmapNearest:
		return gl.NEAREST_MIPMAP_NEAREST;
	case vwgl.Texture.kFilterLinearMipmapNearest:
		return gl.LINEAR_MIPMAP_NEAREST;
	case vwgl.Texture.kFilterNearestMipmapLinear:
		return gl.NEAREST_MIPMAP_LINEAR;
	case vwgl.Texture.kFilterLinearMipmapLinear:
		return gl.LINEAR_MIPMAP_LINEAR;
	case vwgl.Texture.kFilterNearest:
		return gl.NEAREST;
	case vwgl.Texture.kFilterLinear:
		return gl.LINEAR;
	}
}

vwgl.Texture.kWrapRepeat = 301;
vwgl.Texture.kWrapClampToEdge = 302;
vwgl.Texture.getGLWrap = function(wrap) {
	return wrap==vwgl.Texture.kWrapRepeat ? gl.REPEAT:gl.CLAMP_TO_EDGE;
}

vwgl.Texture.kTexture0 = 0;
vwgl.Texture.kTexture1 = 1;
vwgl.Texture.kTexture2 = 2;
vwgl.Texture.kTexture3 = 3;
vwgl.Texture.kTexture4 = 4;
vwgl.Texture.kTexture5 = 5;
vwgl.Texture.kTexture6 = 6;
vwgl.Texture.kTexture7 = 7;
vwgl.Texture.kTexture8 = 8;
vwgl.Texture.kTexture9 = 9;
vwgl.Texture.kTexture10 = 10;
vwgl.Texture.kTexture11 = 11;
vwgl.Texture.kTexture12 = 12;
vwgl.Texture.kTexture13 = 13;
vwgl.Texture.kTexture14 = 14;
vwgl.Texture.kTexture15 = 15;
vwgl.Texture.kTexture16 = 16;
vwgl.Texture.kTexture17 = 17;
vwgl.Texture.kTexture18 = 18;
vwgl.Texture.kTexture19 = 19;
vwgl.Texture.kTexture20 = 20;
vwgl.Texture.kTexture21 = 21;
vwgl.Texture.kTexture22 = 22;
vwgl.Texture.kTexture23 = 23;
vwgl.Texture.kTexture24 = 24;
vwgl.Texture.kTexture25 = 25;
vwgl.Texture.kTexture26 = 26;
vwgl.Texture.kTexture27 = 27;
vwgl.Texture.kTexture28 = 28;
vwgl.Texture.kTexture29 = 29;
vwgl.Texture.kTexture30 = 30;

vwgl.Texture.setActiveTexture = function(textureUnit) {
	gl.activeTexture(gl.TEXTURE0 + textureUnit);
}

vwgl.Texture.unbindTexture = function(target) {
	gl.bindTexture(vwgl.Texture.getGLTarget(target),null);
}

vwgl.TextureManager = {

	s_minFilter:vwgl.Texture.kFilterLinear,
	s_magFilter:vwgl.Texture.kFilterLinear,
	s_wrapS:vwgl.Texture.kWrapRepeat,
	s_wrapT:vwgl.Texture.kWrapRepeat,
	s_colorTextureSize:32,
	
	_textures:{},
	_randTexture:null,
	_whiteTexture:null,
	_blackTexture:null,
	_transparentTexture:null,
	
	get:function() {
		return this;
	},
	
	loadTexture:function(img) {
		if (!this._isImageValid(img)) return null;
		var tex = null;
		
		if ((tex=this._getTexture(img.src))==null) {
			base.debug.log("Load texture: texture not found in cache: " + img.src);
			tex = new vwgl.Texture();
	
			tex.createTexture();
			tex.bindTexture(vwgl.Texture.kTargetTexture2D);
			tex.setMinFilter(vwgl.Texture.kTargetTexture2D, vwgl.TextureManager.s_minFilter);
			tex.setMagFilter(vwgl.Texture.kTargetTexture2D, vwgl.TextureManager.s_magFilter);
			tex.setTextureWrapS(vwgl.Texture.kTargetTexture2D, vwgl.TextureManager.s_wrapS);
			tex.setTextureWrapT(vwgl.Texture.kTargetTexture2D, vwgl.TextureManager.s_wrapT);
			tex.setImage(vwgl.Texture.kTargetTexture2D, img);
			tex.setFileName(img.src);
			this._addTexture(img.src,tex);
		}
		else {
			base.debug.log("Load texture: texture already cached: " + img.src);
		}
		return tex;
	},
	
	// This function is exclusive of the JavaScript API. It creates a texture, and load the associated file asyncrhonously
	loadTextureDeferred:function(url) {
		var tex = null;
		if ((tex=this._getTexture(url))==null) {
			var This = this;
			base.debug.log("Load texture: texture not found in cache: " + url);
			tex = new vwgl.Texture();
	
			tex.createTexture();
			tex.bindTexture(vwgl.Texture.kTargetTexture2D);
			tex.setMinFilter(vwgl.Texture.kTargetTexture2D, vwgl.TextureManager.s_minFilter);
			tex.setMagFilter(vwgl.Texture.kTargetTexture2D, vwgl.TextureManager.s_magFilter);
			tex.setTextureWrapS(vwgl.Texture.kTargetTexture2D, vwgl.TextureManager.s_wrapS);
			tex.setTextureWrapT(vwgl.Texture.kTargetTexture2D, vwgl.TextureManager.s_wrapT);
			var whiteImage = new Uint8Array([255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255]);
			tex.setImageRaw(vwgl.Texture.kTargetTexture2D,2,2,whiteImage);
			this._addTexture(url,tex);
			var loader = new vwgl.Loader();
			loader.loadResource(url,function(resource) {
				tex.bindTexture(vwgl.Texture.kTargetTexture2D);
				tex.setImage(vwgl.Texture.kTargetTexture2D,resource);
				tex.setFileName(resource.src);
			});
		}
		else {
			base.debug.log("Load texture: texture already cached: " + url);
		}
		return tex;
	},

	loadCubeMap:function(posX,negX,posY,negY,posZ,negZ) {
		if (this._isImageListValid([posX,negX,posY,negY,posZ,negZ])) {
			var cubeMap = new vwgl.CubeMap();
	
			cubeMap.createTexture();
			cubeMap.bindTexture(vwgl.Texture.kTargetTextureCubeMap);
			cubeMap.setMinFilter(vwgl.Texture.kTargetTextureCubeMap, vwgl.Texture.kFilterLinear);
			cubeMap.setMagFilter(vwgl.Texture.kTargetTextureCubeMap, vwgl.Texture.kFilterLinear);
			cubeMap.setTextureWrapS(vwgl.Texture.kTargetTextureCubeMap, vwgl.Texture.kWrapClampToEdge);
			cubeMap.setTextureWrapT(vwgl.Texture.kTargetTextureCubeMap, vwgl.Texture.kWrapClampToEdge);
			cubeMap.setImage(vwgl.Texture.kTexturePositiveXFace, posX);
			cubeMap.setImage(vwgl.Texture.kTextureNegativeXFace, negX);
			cubeMap.setImage(vwgl.Texture.kTexturePositiveYFace, posY);
			cubeMap.setImage(vwgl.Texture.kTextureNegativeYFace, negY);
			cubeMap.setImage(vwgl.Texture.kTexturePositiveZFace, posZ);
			cubeMap.setImage(vwgl.Texture.kTextureNegativeZFace, negZ);
	
			cubeMap.setPositiveXFile(posX.src);
			cubeMap.setNegativeXFile(negX.src);
			cubeMap.setPositiveYFile(posY.src);
			cubeMap.setNegativeYFile(negY.src);
			cubeMap.setPositiveZFile(posZ.src);
			cubeMap.setNegativeZFile(posZ.src);
	
			return cubeMap;
		}
		return null;
	},

	setMinFilter:function(minFilter) { this.s_minFilter = minFilter; },
	setMagFilter:function(magFilter) { this.s_magFilter = magFilter; },
	setWrapS:function(w) { this.s_wrapS = w; },
	setWrapT:function(w) { this.s_wrapT = w; },

	// There aren't a clearUnused function in Javascript, you must to track your textures manually using the unrefTexture method
	// clearUnused:function();
	
	unrefTexture:function(texture) {
		texture._references--;
		if (texture._references<=0) {
			this._removeTexture(texture);
			texture.destroy();
		}
	},
	
	randomTexture:function() {
		return this._randTexture;
	},
	
	colorTextureSize:function() {
		return this.s_colorTextureSize;
	},

	whiteTexture:function() {
		if (!this._whiteTexture) {
			var size = this.colorTextureSize();
			this._whiteTexture = vwgl.Texture.getColorTexture(vwgl.Color.white(), new vwgl.Size2D(size));
		}
		return this._whiteTexture;
	},
	
	blackTexture:function() {
		if (!this._whiteTexture) {
			var size = this.colorTextureSize();
			this._whiteTexture = vwgl.Texture.getColorTexture(vwgl.Color.black(), new vwgl.Size2D(size));
		}
		return this._whiteTexture;
	},
	
	transparentTexture:function() {
		if (!this._whiteTexture) {
			var size = this.colorTextureSize();
			this._whiteTexture = vwgl.Texture.getColorTexture(vwgl.Color.transparent(), new vwgl.Size2D(size));
		}
		return this._whiteTexture;
	},

	_addTexture:function(key,texture) {
		if (!this._randTexture) {
			this._randTexture = texture;
			texture._references++;
		}
		texture.key = key;
		this._textures[key] = texture;
	},
	
	_getTexture:function(key) {
		return this._textures[key];
	},
	
	_removeTexture:function(texture) {
		delete this._textures[texture.key];
	},
	
	_isImageValid:function(image) {
		return image.width>0 && image.height>0;
	},
	
	_isImageListValid:function(imageList) {
		var valid = true;
		for (var i=0;i<imageList.length && valid;++i) {
			valid = this._isImageValid(imageList[i]);
		}
		return valid;
	}
};



