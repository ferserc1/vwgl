var vwgl = {}

vwgl.WebGL = {
	_glSupport:false,
	_glExperimental:false,

	glSupport:function() { return this._glSupport; },
	isExperimental:function() { return this._glExperimental; },

	initWebGLContext:function(canvas) {
		var gl = null;
		try {
			gl = canvas.getContext("webgl",{preserveDrawingBuffer: true});
		}
		catch (x) {
			gl = null;
		}

		if (gl === null) {
			try {
				gl = canvas.getContext("experimental-webgl",{preserveDrawingBuffer: true});
				this._glExperimental = true;
			}
			catch (x) {
				gl = null;
			}
		}

		if (gl) {
			this._glSupport = true;
		}

		return gl;
	},
}

vwgl.events = {
	loadComplete:"vwgl:loadcomplete",
	loadError:"vwgl:loaderror",
	loadCached:"vwgl:loadcached",

	trigger:function(eventName,params) {
		$(document).trigger(eventName,params);
	},

	bind:function(eventName,callback) {
		$(document).bind(eventName,function(event,params) {
			if (typeof(callback)=='function') callback(event,params);
		});
	}
}
