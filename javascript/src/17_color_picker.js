// TODO: El ColorPicker no ha sido probado, esta es la implementación directa traducida de C++

Class ("vwgl.PickId", {
	group:0,
	section:0,
	item:0,
	part:0,
	
	assign:function(/* PickId */ other) {
		this.group = other.group;
		this.section = other.section;
		this.item = other.item;
		this.part = other.part;
	},
	
	equals:function(/* PickId */ other) {
		return this.group==other.group && this.section==other.section && this.item==other.item && this.part==other.part;
	},
	
	isZero:function() {
		return this.group==0 && this.section==0 && this.item==0 && this.part==0;
	},
		
	toString:function() {
		return this.group + ":" + section + ":" + item + ":" + part;
	}
});

Class ("vwgl.ColorPickMaterial", vwgl.Material, {
	_pickId:null,	// PickId

	// This material have built-in shaders, so it only have a synchronous load
	initialize:function(name) {
		this.parent(name);
		this._pickId = new vwgl.PickId();
	},
	
	initShader:function() {
		if (this._initialized) return;
		this._initialized = true;
		
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.ColorPickMaterial._vshader);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.ColorPickMaterial._fshader);
		this.getShader().link("color_pick_material");

		this.loadVertexAttrib("aVertexPosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uPickId");
	},

	setupUniforms:function() {
		var colorPickId = new vwgl.Vector4(this._pickId.group/255.0,
									  	   this._pickId.section/255.0,
									  	   this._pickId.item/255.0,
									  	   this._pickId.part/255.0);
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		this.getShader().setUniform("uPickId", colorPickId);
	},

	setColorPickId:function(/* PickId */ id) { this._pickId.assign(id); }
});

vwgl.ColorPickMaterial._vshader = "\
	attribute vec3 aVertexPosition;\n\
	\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
	\n\
	void main() {\n\
		gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	}";
vwgl.ColorPickMaterial._fshader = "\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	\n\
	uniform vec4 uPickId;\n\
	\n\
	void main() {\n\
		gl_FragColor = uPickId;\n\
	}";

Class ("vwgl.ColorPicker", {
	_findNodeVisitor:null, 		// vwgl.PickIdFindVisitor
	_drawPickIdsVisitor:null,	// vwgl.PickDrawVisitor
	_resultId:null,				// vwgl.PickId
	
	initialize:function() {
		this._resultId = new vwgl.PickId();
		this._findNodeVisitor = new vwgl.PickIdFindVisitor();
		this._drawPickIdsVisitor = new vwgl.PickDrawVisitor();
	},
	
	assignPickId:function(/* Solid | Drawable */ obj) {
		if (dynamic_cast("vwgl.Solid",obj)) {
			this.assignPickIdSolid(obj);
		}
		else if (dynamic_cast("vwgl.Drawable",obj)) {
			this.assignPickIdDrawable(obj);
		}
	},
	
	assignPickIdSolid:function(/* Solid */ solid) {
		vwgl.ColorPicker.s_newPickId.part++;
		if (vwgl.ColorPicker.s_newPickId.part==256) {
			vwgl.ColorPicker.s_newPickId.part = 0;
			vwgl.ColorPicker.s_newPickId.item++;
			if (vwgl.ColorPicker.s_newPickId.item==256) {
				vwgl.ColorPicker.s_newPickId.item = 0;
				vwgl.ColorPicker.s_newPickId.section++;
				if (vwgl.ColorPicker.s_newPickId.section==256) {
					vwgl.ColorPicker.s_newPickId.group++;
				}
			}
		}
		solid._pickId = new vwgl.PickId();
		solid._pickId.assign(vwgl.ColorPicker.s_newPickId);
	},
	
	assignPickIdDrawable:function(/* Drawable */ drawable) {
		for (var i=0; i<drawable.getSolidList().length; ++i) {
			this.assignPickIdSolid(drawable.getSolidList()[i]);
		}
	},
	
	pick:function(/* Node */ sceneRoot, mouseX, mouseY, viewportW, viewportH, /* Camera */ cam) {
		gl.viewport(0, 0, viewportW, viewportH);
		gl.clearColor(0.0, 0.0, 0.0, 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		vwgl.CameraManager.get().applyTransform(cam);

		this._drawPickIdsVisitor.visit(sceneRoot);
		var pixels = new Uint8Array(4);

		gl.readPixels(mouseX, mouseY, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
		this._resultId.group 	= pixels[0];
		this._resultId.section	= pixels[1];
		this._resultId.item 	= pixels[2];
		this._resultId.part 	= pixels[3];
		this._findNodeVisitor.setTarget(this._resultId);
		this._findNodeVisitor.visit(sceneRoot);
	},

	getDrawable:function() {
		return this._findNodeVisitor.getDrawable();
	},

	getSolid:function() {
		return this._findNodeVisitor.getSolid();
	},

	getResultId:function() { return this._resultId; },

	destroy:function() {
		this._findNodeVisitor = null;
		this._drawPickIdsVisitor = null;
	}
});

vwgl.ColorPicker.s_newPickId = new vwgl.PickId();

Class ("vwgl.PickIdFindVisitor", vwgl.NodeVisitor, {
	_target:null,	// vwgl.PickId
	_drawable:null,	// vwgl.Drawable
	_solid:null,		// vwgl.Solid

	initialize:function() {
		
	},

	visit:function(/* Node */ node) {
		this._drawable = null;
		this._solid = null;
		var dn = dynamic_cast("vwgl.DrawableNode",node);
		var g = dynamic_cast("vwgl.Group",node);

		if (dn && dn.getDrawable()) {
			var d = dn.getDrawable();
			for (var i=0; i<d.getSolidList().length; ++i) {
				var solid = d.getSolidList()[i];
				if (solid._pickId && solid._pickId.equals(this._target)) {
					this._solid = solid;
					this._drawable = d;
					return;
				}
			}
		}
		else if (g) {
			for (var i=0; i<g.getNodeList().length; ++i) {
				this.visit(g.getNodeList()[i]);
				if (this._drawable) {
					return;
				}
			}
		}
	},
	
	setTarget:function(/* PickId */ target) {
		this._target = target;
	},
	
	getDrawable:function() { return this._drawable; },
	getSolid:function() { return this._solid; }
});

Class ("vwgl.PickDrawVisitor", vwgl.NodeVisitor, {
	_material:null,

	initialize:function() {
		this._material = new vwgl.ColorPickMaterial();
	},
	
	visit:function(/* Node */ node) {
		var gn = dynamic_cast("vwgl.Group",node);
		var tn = dynamic_cast("vwgl.TransformNode",node);
		var dn = dynamic_cast("vwgl.DrawableNode",node);

		vwgl.State.get().pushModelMatrix();
		if (tn) {
			vwgl.State.get().modelMatrix().mult(tn.getTransform());
		}
		if (gn) {
			for (var i=0; i<gn.getNodeList().length; ++i) {
				this.visit(gn.getNodeList()[i]);
			}
		}
		if (dn && dn.getDrawable()) {
			var solids = dn.getDrawable().getSolidList();
			for (var i=0; i<solids.length; ++i) {
				var solid = solids[i];
				if (solid.getPickId() && !solid.getPickId().isZero() && solid.isVisible()) {
					vwgl.State.get().pushModelMatrix();
					vwgl.State.get().modelMatrix().mult(solid.getTransform());
					this._material.setColorPickId(solid.getPickId());
					this._material.bindPolyList(solid.getPolyList());
					this._material.activate();
					solid.getPolyList().drawElements();
					this._material.deactivate();
					vwgl.State.get().popModelMatrix();
				}
			}
		}
		vwgl.State.get().popModelMatrix();
	}
});

	