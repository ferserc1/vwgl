
Class ("vwgl.DeferredPostprocess", vwgl.Material, {
	_diffuseMap:null,
	_positionMap:null,
	_selectionMap:null,
	_ssaoMap:null,
	
	_ssaoBlur:0,
	
	_targetDistance:0,
	_dofAmount:0,
	
	_renderSettings:null,
	_clearColor:null,

	setDiffuseMap:function(texture) { this._diffuseMap = texture; },
	setPositionMap:function(texture) { this._positionMap = texture; },
	setSelectionMap:function(texture) { this._selectionMap = texture; },
	setSSAOMap:function(texture) { this._ssaoMap = texture; },
	setTargetDistance:function(dist) { this._targetDistance = dist; },
	setDOFAmount:function(amount) { this._dofAmount = amount; },

	getDiffuseMap:function(texture) { return this._diffuseMap; },
	getPositionMap:function(texture) { return this._positionMap; },
	getSSAOMap:function(texture) { return this._ssaoMap; },
	getTargetDistance:function(dist) { return this._targetDistance; },
	getDOFAmount:function() { return this._dofAmount; },
	
	getRenderSettings:function(settings) { return this._renderSettings; },

	setSSAOBlur:function(blurIterations) { this._ssaoBlur = blurIterations; },
	getSSAOBlur:function() { return this._ssaoBlur; },
	
	setClearColor:function(color) { this._color.assign(color); },
	getClearColor:function() { return this._color; },
	
	initialize:function() {
		this._renderSettings = new vwgl.RenderSettings();
		this._ssaoBlur = 4;
		this._clearColor = new vwgl.Color.black();
		this.initShaderSync(null);
	},

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.postprocess_mix.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.postprocess_mix.fragment);
		this.getShader().link("deferred_postprocess");

		this.loadVertexAttrib("aVertexPos");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uDiffuseMap");
		this.getShader().initUniformLocation("uPositionMap");
		this.getShader().initUniformLocation("uSelectionMap");
		this.getShader().initUniformLocation("uSSAOMap");
		
		this.getShader().initUniformLocation("uSize");
		this.getShader().initUniformLocation("uBlurKernel");
		this.getShader().initUniformLocation("uSelectionKernel");
		this.getShader().initUniformLocation("uTargetDistance");
		this.getShader().initUniformLocation("uDOFAmount");
		this.getShader().initUniformLocation("uSSAOBlur");

		this.getShader().initUniformLocation("uBrightness");
		this.getShader().initUniformLocation("uContrast");
		this.getShader().initUniformLocation("uHue");
		this.getShader().initUniformLocation("uSaturation");
		this.getShader().initUniformLocation("uLightness");
		
		this.getShader().initUniformLocation("uClearColor");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uDiffuseMap",this._diffuseMap,vwgl.Texture.kTexture0);
		this.getShader().setUniform("uPositionMap",this._positionMap,vwgl.Texture.kTexture1);
		this.getShader().setUniform("uSelectionMap",this._selectionMap,vwgl.Texture.kTexture2);
		this.getShader().setUniform("uSSAOMap",this._ssaoMap,vwgl.Texture.kTexture3);
		
		this.getShader().setUniform("uSize",new vwgl.Vector2(2048));
		var blurKernel = [
			0.045, 0.122, 0.045,
			0.122, 0.332, 0.122,
			0.045, 0.122, 0.045
		];
		var selectionKernel = [
			0.0, 1.0, 0.0,
			1.0,-4.0, 1.0,
			0.0, 1.0, 0.0
		]
		this.getShader().setUniform1fv("uBlurKernel", blurKernel);
		this.getShader().setUniform1fv("uSelectionKernel", selectionKernel);
		this.getShader().setUniform1f("uTargetDistance", this._targetDistance);
		this.getShader().setUniform1f("uDOFAmount", this._dofAmount);
		this.getShader().setUniform1f("uBrightness", this._renderSettings.getBrightness());
		this.getShader().setUniform1f("uContrast", this._renderSettings.getContrast());
		this.getShader().setUniform1f("uHue", this._renderSettings.getHue());
		this.getShader().setUniform1f("uSaturation", this._renderSettings.getSaturation());
		this.getShader().setUniform1f("uLightness", this._renderSettings.getLightness());
		this.getShader().setUniform("uClearColor", this._clearColor);
		this.getShader().setUniform1i("uSSAOBlur", this._ssaoBlur);
	}

});

