
Class ("vwgl.Material",{

	/* Override this functions to create new materials */
	getResourceList:function() { return []; },	// Returns the resources that the material needs to load (shader, images, etc)
	initShaderSync:function(loader) {},	// This is the Javascript equivalent to initShader in C++ API
	setupUniforms:function() {},

	// Override this function to use the settings of the supplied material
	useSettingsOf:function(/* Material */ mat) {},
	
	_readyCallback:null,
	_name:"",
	_shader:null,		// vwgl.Shader
	_currentShader:-1,
	_cullFace:true,
	
	_vertexVBO:null,
	_normalVBO:null,
	_colorVBO:null,
	_texCoord0VBO:null,
	_texCoord1VBO:null,
	_texCoord2VBO:null,
	_tangentVBO:null,
	
	_vertexAttrib:-1,
	_normalAttrib:-1,
	_colorAttrib:-1,
	_texCoord0Attrib:-1,
	_texCoord1Attrib:-1,
	_texCoord2Attrib:-1,
	_tangentAttrib:-1,
	
	_initialized:false,
	
	_reuseKey:null,
	
	_setReuseKey:function(key) {
		this._reuseKey = key;
	},
		
	initialize:function(a,b) {
		if (typeof(a)=="string") this._name = name;
		else if (typeof(a)=="function") this._readyCallback = a;
		if (typeof(b)=="function") this._readyCallback = b;
		this.initShader();
	},
	
	// Do not override this function, use initShaderSync
	initShader:function() {
		var This = this;
		
		var resourceList = this.getResourceList();
		var loader = new vwgl.Loader();
		if (typeof(This._readyCallback)=="function" && resourceList.length>0) {
			loader.loadResourceList(resourceList,function(loaded,errors) {
				if (errors!=0) {
					base.debug.log("There were errors loading basic material");
				}
				else {
					This.initShaderSync(loader);
					This._readyCallback(This);
				}
			});
		}
		else {
			This.initShaderSync(loader);
		}
	},
	
	getShader:function(index) {
		if (!this._shader) {
			if (this._reuseKey) {
				this._shader = vwgl.ShaderLibrary.get().getShader(this._reuseKey);
				if (!this._shader) {
					this._shader = new vwgl.Shader();
					vwgl.ShaderLibrary.get().addShader(this._reuseKey,this._shader);
				}
			}
			else {
				this._shader = new vwgl.Shader();
			}
		}
		return this._shader;
	},

	bindPolyList:function(polyList) {
		this.deactivate();	// Por si acaso teníamos activos los VBOs de un objeto anterior
		this._vertexVBO = polyList.getVertexBuffer();
		this._normalVBO = polyList.getNormalBuffer();
		this._colorVBO = polyList.getColorBuffer();
		this._texCoord0VBO = polyList.getTexCoord0Buffer();
		this._texCoord1VBO = polyList.getTexCoord1Buffer();
		this._texCoord2VBO = polyList.getTexCoord2Buffer();
		this._tangentVBO = polyList.getTangentBuffer();
	},
	
	isTransparent:function() { return false; },

	loadVertexAttrib:function(name) { this._vertexAttrib = this.getShader().getAttribLocation(name); },
	loadNormalAttrib:function(name) { this._normalAttrib = this.getShader().getAttribLocation(name); },
	loadColorAttrib:function(name) { this._colorAttrib = this.getShader().getAttribLocation(name); },
	loadTexCoord0Attrib:function(name) { this._texCoord0Attrib = this.getShader().getAttribLocation(name); },
	loadTexCoord1Attrib:function(name) { this._texCoord1Attrib = this.getShader().getAttribLocation(name); },
	loadTexCoord2Attrib:function(name) { this._texCoord2Attrib = this.getShader().getAttribLocation(name); },
	loadTangentArray:function(name) { this._tangentAttrib = this.getShader().getAttribLocation(name); },

	activate:function() {
		if (this._cullFace) {
			gl.enable(gl.CULL_FACE);
		}
		else {
			gl.disable(gl.CULL_FACE);
		}
	
		this.getShader().bind();
		this.enableAttribs();
		this.setupUniforms();
	},

	deactivate:function() {
		this.disableAttribs();
		this.getShader().unbind();
	},

	destroy:function() {
		this.deactivate();
		this._shader.destroy();
		this._shader = null;
		this._vertexVBO = -1;
		this._normalVBO = -1;
		this._colorVBO = -1;
		this._texCoord0VBO = -1;
		this._texCoord1VBO = -1;
		this._texCoord2VBO = -1;
		this._tangentVBO = -1;
	},
	
	enableAttribs:function() {
		this._enableVertexAttrib();
		this._enableNormalAttrib();
		this._enableColorAttrib();
		this._enableTexCoord0Attrib();
		this._enableTexCoord1Attrib();
		this._enableTexCoord2Attrib();
		this._enableTangentAttrib();
	},

	disableAttribs:function() {
		this._disableVertexAttrib();
		this._disableNormalAttrib();
		this._disableColorAttrib();
		this._disableTexCoord0Attrib();
		this._disableTexCoord1Attrib();
		this._disableTexCoord2Attrib();
		this._disableTangentAttrib();
	},

	modelViewMatrix:function() { return vwgl.State.get().modelViewMatrix(); },
	modelMatrix:function() { return vwgl.State.get().modelMatrix(); },
	viewMatrix:function() { return vwgl.State.get().viewMatrix(); },
	projectionMatrix:function() { return vwgl.State.get().projectionMatrix(); },
	normalMatrix:function() { return vwgl.State.get().normalMatrix(); },

	// Generic OpenGL material properties
	// In WebGL and OpenGL ES polygon mode is not available, so in Javascript API this functions are not implemented
	//inline void setPolygonMode(PolygonMode p) { _polygonMode = p; }
	//inline PolygonMode getPolygonMode() const { return _polygonMode; }
	//inline void setLineWidth(float w) { _lineWidth = w; }
	//inline float getLineWidth() const { return _lineWidth; }
	
	setCullFace:function(h) { this._cullFace = h; },
	getCullFace:function() { return this._cullFace; },
	
	
	_enableVertexAttrib:function() {
		if (this._vertexVBO!==null && this._vertexAttrib!=-1) {
			gl.bindBuffer(gl.ARRAY_BUFFER, this._vertexVBO);
			gl.enableVertexAttribArray(this._vertexAttrib);
			gl.vertexAttribPointer(this._vertexAttrib,3,gl.FLOAT,false,0,0);
		}
	},

	_enableNormalAttrib:function() {
		if (this._normalVBO!==null && this._normalAttrib!=-1) {
			gl.bindBuffer(gl.ARRAY_BUFFER, this._normalVBO);
			gl.enableVertexAttribArray(this._normalAttrib);
			gl.vertexAttribPointer(this._normalAttrib,3,gl.FLOAT,false,0,0);
		}
	},
	
	_enableColorAttrib:function() {
		if (this._colorVBO!==null && this._colorAttrib!=-1) {
			gl.bindBuffer(gl.ARRAY_BUFFER, this._colorVBO);
			gl.enableVertexAttribArray(this._colorAttrib);
			gl.vertexAttribPointer(this._colorAttrib,4,gl.FLOAT,false,0,0);
		}
	},

	_enableTexCoord0Attrib:function() {
		if (this._texCoord0VBO!==null && this._texCoord0Attrib!=-1) {
			gl.bindBuffer(gl.ARRAY_BUFFER, this._texCoord0VBO);
			gl.enableVertexAttribArray(this._texCoord0Attrib);
			gl.vertexAttribPointer(this._texCoord0Attrib,2,gl.FLOAT,false,0,0);
		}
	},

	_enableTexCoord1Attrib:function() {
		if (this._texCoord1VBO!==null && this._texCoord1Attrib!=-1) {
			gl.bindBuffer(gl.ARRAY_BUFFER, this._texCoord1VBO);
			gl.enableVertexAttribArray(this._texCoord1Attrib);
			gl.vertexAttribPointer(this._texCoord1Attrib,2,gl.FLOAT,false,0,0);
		}
	},

	_enableTexCoord2Attrib:function() {
		if (this._texCoord2VBO!==null && this._texCoord2Attrib!=-1) {
			gl.bindBuffer(gl.ARRAY_BUFFER, this._texCoord2VBO);
			gl.enableVertexAttribArray(this._texCoord2Attrib);
			gl.vertexAttribPointer(this._texCoord2Attrib,2,gl.FLOAT,false,0,0);
		}
	},
	
	_enableTangentAttrib:function() {
		if (this._tangentVBO!==null && this._tangentAttrib!=-1) {
			gl.bindBuffer(gl.ARRAY_BUFFER, this._tangentVBO);
			gl.enableVertexAttribArray(this._tangentAttrib);
			gl.vertexAttribPointer(this._tangentAttrib,3,gl.FLOAT,false,0,0);
		}
	},

	_disableVertexAttrib:function() {
		if (this._vertexVBO!==null && this._vertexAttrib!=-1) {
			gl.disableVertexAttribArray(this._vertexAttrib);
		}
	},

	_disableNormalAttrib:function() {
		if (this._normalVBO!==null && this._normalAttrib!=-1) {
			gl.disableVertexAttribArray(this._normalAttrib);
		}
	},
	
	_disableColorAttrib:function() {
		if (this._colorVBO!==null && this._colorAttrib!=-1) {
			gl.disableVertexAttribArray(this._colorAttrib);
		}
	},

	_disableTexCoord0Attrib:function() {
		if (this._texCoord0VBO!==null && this._texCoord0Attrib!=-1) {
			gl.disableVertexAttribArray(this._texCoord0Attrib);
		}
	},

	_disableTexCoord1Attrib:function() {
		if (this._texCoord1VBO!==null && this._texCoord1Attrib!=-1) {
			gl.disableVertexAttribArray(this._texCoord1Attrib);
		}
	},

	_disableTexCoord2Attrib:function() {
		if (this._texCoord2VBO!==null && this._texCoord2Attrib!=-1) {
			gl.disableVertexAttribArray(this._texCoord2Attrib);
		}
	},
	
	_disableTangentAttrib:function() {
		if (this._tangentVBO!==null && this._tangentAttrib!=-1) {
			gl.disableVertexAttribArray(this._tangentAttrib);
		}
	}
});

vwgl.Material.getResourceList = function(material) {
	if (material.length) {
		var resources = [];
		for (var i=0;i<material.length;++i) {
			resources = resources.concat(material.prototype.getResourceList());
		}
	}
	else {
		return material.prototype.getResourceList();
	}
}

