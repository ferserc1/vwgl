Class ("vwgl.FramebufferObject", {
	_width:0,
	_height:0,
	_type:0,
	
	_fbo:null,
	_texture:null,		// vwgl.Texture
	_renderBuffer:0,
	_depthTexture:null,	// vwgl.Texture
	
	initialize:function(width,height, type) {
		if (dynamic_cast("vwgl.Vector2",width)) {
			var type = height!==undefined ? height:vwgl.FramebufferObject.kTypeDefault;
			this.create(width.width(),width.height(),type);
		}
		else if (width && !height) {
			this.create(width,width,vwgl.FramebufferObject.kTypeDefault);
		}
		else if (width && height && !type) {
			this.create(width,height,vwgl.FramebufferObject.kTypeDefault);
		}
		else if (width && height && type) {
			this.create(width,height,type);
		}
	},

	create:function(width, height, type) {
		this.destroy();
		this._width = width;
		this._height = height;
		this._type = type;
		this._fbo = gl.createFramebuffer();
		gl.bindFramebuffer(gl.FRAMEBUFFER, this._fbo);

		if (type==vwgl.FramebufferObject.kDepthTexture && gl.getExtension("WEBGL_depth_texture")) {
			this._texture = new vwgl.Texture();
			this._texture.createTexture();
			this._texture.bindTexture(vwgl.Texture.kTargetTexture2D);
			this._texture.setMinFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
			this._texture.setMagFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
			this._texture.setTextureWrapS(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClamp);
			this._texture.setTextureWrapT(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClamp);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
	
			this._depthTexture = new vwgl.Texture();
			this._depthTexture.createTexture();
			this._depthTexture.bindTexture(vwgl.Texture.kTargetTexture2D);
			this._depthTexture.setMinFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
			this._depthTexture.setMagFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
			this._depthTexture.setTextureWrapS(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClamp);
			this._depthTexture.setTextureWrapT(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClamp);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, width, height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
	
	
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this._texture.getTextureName(), 0);
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, this._depthTexture.getTextureName(), 0);
		}
		else if (type==vwgl.FramebufferObject.kFloatTexture && gl.getExtension("OES_texture_float")) {
			this._texture = new vwgl.Texture();
			this._texture.createTexture();
			this._texture.bindTexture(vwgl.Texture.kTargetTexture2D);
			this._texture.setMinFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
			this._texture.setMagFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
			this._texture.setTextureWrapS(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClamp);
			this._texture.setTextureWrapT(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClamp);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.FLOAT,null);
			
			this._renderBuffer = gl.createRenderbuffer();
			gl.bindRenderbuffer(gl.RENDERBUFFER, this._renderBuffer);
			gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this._width, this._height);
	
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this._texture.getTextureName(), 0);
			gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this._renderBuffer);
			
			gl.bindRenderbuffer(gl.RENDERBUFFER,null);
		}
		else if (type==vwgl.FramebufferObject.kTypeDefault) {
			this._texture = new vwgl.Texture();
			this._texture.createTexture();
			this._texture.bindTexture(vwgl.Texture.kTargetTexture2D);
			this._texture.setMinFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
			this._texture.setMagFilter(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kFilterNearest);
			this._texture.setTextureWrapS(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClampToEdge);
			this._texture.setTextureWrapT(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClampToEdge);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this._width, this._height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

			this._renderBuffer = gl.createRenderbuffer();
			gl.bindRenderbuffer(gl.RENDERBUFFER, this._renderBuffer);
			gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this._width, this._height);
			
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this._texture.getTextureName(),0);
			gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this._renderBuffer);
		
			gl.bindRenderbuffer(gl.RENDERBUFFER, null);
		}
		else if (type==vwgl.FramebufferObject.kCubeMap) {
			var cm = new vwgl.CubeMap();
			cm.createTexture();
			cm.bindTexture(vwgl.Texture.kTargetTextureCubeMap);
			cm.setMinFilter(vwgl.Texture.kTargetTextureCubeMap, vwgl.Texture.kFilterLinear);
			cm.setMagFilter(vwgl.Texture.kTargetTextureCubeMap, vwgl.Texture.kFilterLinear);
			cm.setTextureWrapS(vwgl.Texture.kTargetTextureCubeMap, vwgl.Texture.kWrapClamp);
			cm.setTextureWrapT(vwgl.Texture.kTargetTextureCubeMap, vwgl.Texture.kWrapClamp);
			this._texture = cm;
	
			for (var i=0; i<6; ++i) {
				gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGBA, this._width, this._height, 0, gl.RGBA, gl.UNSIGNED_BYTE,null);
			}
	
		    this._renderBuffer = gl.createRenderbuffer();
			gl.bindRenderbuffer(gl.RENDERBUFFER, this._renderBuffer);
			gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this._width, this._height);
			
			gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_CUBE_MAP_POSITIVE_X,this._texture.getTextureName(),0);
			gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this._renderBuffer);
		
			gl.bindRenderbuffer(gl.RENDERBUFFER, null);
		}

		if (this.getCubeMap()) vwgl.Texture.unbindTexture(vwgl.Texture.kTargetTextureCubeMap);
		else if (this._texture) vwgl.Texture.unbindTexture(vwgl.Texture.kTargetTexture2D);
		this.unbind();
	},

	resize:function(width, height) {
		if (dynamic_cast("vwgl.Vector2",width)) {
			height = width.height();
			width = width.width();
		}
		this._width = width;
		this._height = height;
		this.bind();

		if (this._type==vwgl.FramebufferObject.kDepthTexture) {
			this._texture.bindTexture(vwgl.Texture.kTargetTexture2D);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this._width, this._height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
	
			this._depthTexture.bindTexture(vwgl.Texture.kTargetTexture2D);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, this._width, this._height, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_SHORT, null);
		}
		else if (this._type==vwgl.FramebufferObject.kFloatTexture) {
			this._texture.bindTexture(vwgl.Texture.kTargetTexture2D);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this._width, this._height, 0, gl.RGBA, gl.FLOAT,null);
			
			gl.bindRenderbuffer(gl.RENDERBUFFER, this._renderBuffer);
			gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this._width, this._height);
			
			gl.bindRenderbuffer(gl.RENDERBUFFER, null);
		}
		else if (this._type==vwgl.FramebufferObject.kTypeDefault) {
			this._texture.bindTexture(vwgl.Texture.kTargetTexture2D);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this._width, this._height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

			gl.bindRenderbuffer(gl.RENDERBUFFER, this._renderBuffer);
			gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this._width, this._height);
	
			gl.bindRenderbuffer(gl.RENDERBUFFER, null);
		}
		else if (this._type==vwgl.FramebufferObject.kCubeMap && dynamic_cast("vwgl.CubeMap",this._texture)) {
			var cm = dynamic_cast("vwgl.CubeMap",_texture);
			cm.bindTexture(vwgl.Texture.kTargetTextureCubeMap);
	
			for (var i=0; i<6; ++i) {
				gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGBA, this._width, this._height, 0, gl.RGBA, gl.UNSIGNED_BYTE,null);
			}
			
			gl.bindRenderbuffer(gl.RENDERBUFFER, this._renderBuffer);
			gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this._width, this._height);
	
			gl.bindRenderbuffer(gl.RENDERBUFFER, null);
		}

		if (this.getCubeMap()) vwgl.Texture.unbindTexture(vwgl.Texture.kTargetTextureCubeMap);
		else if (this._texture) vwgl.Texture.unbindTexture(vwgl.Texture.kTargetTexture2D);
		
		this.unbind();
	},

	bind:function() {
		gl.bindFramebuffer(gl.FRAMEBUFFER,this._fbo);
	},

	unbind:function() {
		gl.bindFramebuffer(gl.FRAMEBUFFER,null);
	},

	destroy:function() {
		this.unbind();
		if (this._fbo) {
			gl.deleteFramebuffer(this._fbo);
			this._fbo = null;
		}
		if (this._texture) {
			this._texture.destroy();
			this._texture = null;
		}
		if (this._renderBuffer) {
			gl.deleteRenderbuffer(this._renderBuffer);
			this._renderBuffer = null;
		}
		if (this._depthTexture) {
			this._depthTexture.destroy();
			this._depthTexture = null;
		}
	},

	getWidth:function() { return this._width; },
	getHeight:function() { return this._height; },

	getTexture:function() { return this._texture; },
	getCubeMap:function() { return dynamic_cast("vwgl.CubeMap",this._texture); },
	getDepthTexture:function() { return this._depthTexture; }
});

vwgl.FramebufferObject.kTypeDefault = 1;
vwgl.FramebufferObject.kDepthTexture = 2;
vwgl.FramebufferObject.kFloatTexture = 3;
vwgl.FramebufferObject.kCubeMap = 4;
