
Class ("vwgl.Drawable",{

	_name:"",
	_solidList:null,
	_renderOpaque:true,
	_renderTransparent:true,
	_hidden:false,
	_parent:null,
	_shadowProjector:null,
	_cubeMap:null,
	_inputJoint:null,		// physics.Joint
	_outputJoint:null,		// physics.Joint

	initialize:function(/* string */ name) {
		if (name) this._name = name;
		this._solidList = [];
	},

	getSolidList:function() { return this._solidList; },
	addSolid:function(/* Solid */ solid) {
		this._solidList.push(solid);
		solid._drawable = this;
	},

	getSolid:function(plistName) {
		var result = null;

		for (var i=0; i<this._solidList.length && result==null; ++i) {
			if (this._solidList[i].getPolyList().getName()==plistName) {
				result = this._solidList[i];
			}
		}

		return result;
	},

	getPolyList:function(plistName) {
		var result = this.getSolid(plistName);
		if (result) result = result.getPolyList();
		return result;
	},

	// Apply material to all solids
	setMaterial:function(/* Material */ mat) {
		for (var i=0; i<this._solidList.length; ++i) {
			this._solidList[i].setMaterial(mat);
		}
	},

	// Get first material in solid list
	getMaterial:function() {
		if (this._solidList.length) return this._solidList[0].getMaterial();
		else return null;
	},
	getGenericMaterial:function() { return dynamic_cast("vwgl.GenericMaterial",this.getMaterial()); },

	init:function() {
		this.addProjectorToScene();
		// TODO: It's not a good idea to automatically create the cube map. Remove this code from the C++ API
		//this.createCubeMap();
	},

	draw:function(/* Material | null */ mat, useMaterialSettings) {
		if (!this._hidden ) {
			for (var i=0;i<this._solidList.length;++i) {
				var solid = this._solidList[i];
				solid.setRenderTransparent(this._renderTransparent);
				solid.setRenderOpaque(this._renderOpaque);
				solid.draw(mat, useMaterialSettings);
			}
		}
	},

	destroy:function() {
		this.removeProjectorFromScene();
		for (var i=0;i<this._solidList.length;++i) {
			this._solidList[i].destroy();
		}
		this._solidList = []
	},

	setRenderOpaque:function(opaque) { this._renderOpaque = opaque; },
	getRenderOpaque:function() { return this._renderOpaque; },

	setRenderTransparent:function(transparent) { this._renderTransparent = transparent; },
	getRenderTransparent:function() { return this._renderTransparent; },

	show:function() { this._hidden = false; },
	hide:function() { this._hidden = true; },
	setHidden:function(h) { this._hidden = h; },
	isHidden:function() { return this._hidden; },

	hideGroup:function(/* string */ groupName) {
		for (var i=0; i<this._solidList.length; ++i) {
			if (this._solidList[i].getGroupName()==groupName) {
				this._solidList[i].hide();
			}
		}
	},

	showGroup:function(/* string */ groupName) {
		for (var i=0; i<this._solidList.length; ++i) {
			if (this._solidList[i].getGroupName()==groupName) {
				this._solidList[i].show();
			}
		}
	},

	showByName:function(/* string */ name) {
		for (var i=0; i<this._solidList.length; ++i) {
			if (this._solidList[i].getPolyList().getName()==name) {
				this._solidList[i].show();
			}
		}
	},

	hideByName:function(/* string */ name) {
		for (var i=0; i<this._solidList.length; ++i) {
			if (this._solidList[i].getPolyList().getName()==name) {
				this._solidList[i].hide();
			}
		}
	},

	setInputJoint:function(/* physics::Joint */ joint) { this._inputJoint = joint; },
	setOutputJoint:function(/* physics::Joint */ joint) { this._outputJoint = joint; },
	getInputJoint:function() { return this._inputJoint; },
	getOutputJoint:function() { return this._outputJoint; },

	// initJoints(): visible joints are only available in C++ API, so, initJoints() only
	// include an empty implementation
	initJoints:function() { },

	getParent:function() { return this._parent; },

	applyTransform:function(/* vwgl.Matrix4 */ transform) {
		this._solidList.forEach(function(solid) {
			solid.getPolyList().applyTransform(transform);
		});
	},

	setCubemap:function(cm) {
		for (var i=0; i<this._solidList.length; ++i) {
			var mat = this._solidList[i].getGenericMaterial();
			mat.setCubeMap(cm);
		}
	},

	setShadowProjector:function(/* vwgl.Projector */ proj) {
		if (proj) {
			this._shadowProjector = proj;
			var tex = this._shadowProjector.getTexture();
			tex.bindTexture(vwgl.Texture.kTargetTexture2D);
			tex.setTextureWrapS(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClampToEdge);
			tex.setTextureWrapT(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClampToEdge);
			if (this.getParent()) this.getParent().setInitialized(false);
		}
		else {
			this.removeProjectorFromScene();
			if (this._shadowProjector) this._shadowProjector.destroy();
			this._shadowProjector = null;
		}
	},
	getShadowProjector:function() { return this._shadowProjector; },

	addProjectorToScene:function() {
		if (this._shadowProjector && !this._shadowProjector.getParent() && this.getParent()) {
			var group = dynamic_cast("vwgl.Group",this.getParent().getParent());
			if (group) {
				group.addChild(this._shadowProjector);
			}
		}
		for (var i=0; i<this._solidList.length;++i) {
			var proj = this._solidList[i].getShadowProjector();
			if (proj && !proj.getParent() && this.getParent()) {
				var group = dynamic_cast("vwgl.Group",this.getParent().getParent());
				if (group) {
					group.addChild(proj);
				}
			}
		}
	},

	removeProjectorFromScene:function() {
		if (this._shadowProjector && this._shadowProjector.getParent()) {
			this._shadowProjector.getParent().removeChild(this._shadowProjector);
			this._shadowProjector.destroy();
		}
		for (var i=0;i<this._solidList.length;++i) {
			this._solidList[i].removeProjectorFromScene();
		}
	},

	addToScene:function(/* Node */ n, /* Node */ parent) {
		if (!parent) return;
		if (n && !n.getParent()) {
			var grp = dynamic_cast("vwgl.Group",parent);
			if (grp) {
				grp.addChild(n);
			}
			else {
				this.addToScene(n, parent.getParent());
			}
		}
	}
});
