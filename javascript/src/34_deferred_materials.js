

Class ("vwgl.TextureDeferredMaterial", vwgl.DeferredMaterial, {
	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.texture.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.texture.fragment);
		this.getShader().link("def_texture");

		this.loadVertexAttrib("aVertexPosition");
		this.loadNormalAttrib("aNormalPosition");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uMMatrix");
		this.getShader().initUniformLocation("uNMatrix");
		this.getShader().initUniformLocation("uVMatrixInv");

		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uSelectMode");
		this.getShader().initUniformLocation("uColorTint");
		this.getShader().initUniformLocation("uIsEnabled");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");

		this.getShader().initUniformLocation("uNormalMap");
		this.getShader().initUniformLocation("uNormalMapOffset");
		this.getShader().initUniformLocation("uNormalMapScale");

		this.getShader().initUniformLocation("uCubeMap");
		this.getShader().initUniformLocation("uUseCubeMap");
		this.getShader().initUniformLocation("uReflectionAmount");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		this.getShader().setUniform("uMMatrix", this.modelMatrix());
		this.getShader().setUniform("uNMatrix", this.normalMatrix());
		var vMatrix = new vwgl.Matrix4(this.viewMatrix());
		vMatrix.invert();
		this.getShader().setUniform("uVMatrixInv", vMatrix);

		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform("uTexture", mat.getTexture(),vwgl.Texture.kTexture0);
			this.getShader().setUniform1i("uIsEnabled", mat.getTexture()!=null);
			this.getShader().setUniform("uColorTint", mat.getDiffuse());
			this.getShader().setUniform("uTextureOffset", mat.getTextureOffset());
			this.getShader().setUniform("uTextureScale", mat.getTextureScale());

			if (mat.getNormalMap()) {
				this.getShader().setUniform1i("uUseNormalMap", true);
				this.getShader().setUniform("uNormalMap", mat.getNormalMap(),vwgl.Texture.kTexture1);
				this.getShader().setUniform("uNormalMapScale", mat.getNormalMapScale());
				this.getShader().setUniform("uNormalMapOffset", mat.getNormalMapOffset());
			}
			else {
				this.getShader().setUniform1i("uUseNormalMap", false);
			}

			if (mat.getCubeMap()) {
				this.getShader().setUniform1i("uUseCubeMap", true);
				this.getShader().setUniform("uCubeMap", mat.getCubeMap(),vwgl.Texture.kTexture2);
				this.getShader().setUniform1f("uReflectionAmount", mat.getReflectionAmount());
			}
			else {
				this.getShader().setUniform1i("uUseCubeMap", false);
				vwgl.Texture.setActiveTexture(vwgl.Texture.kTexture2);
				gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
				this.getShader().setUniform1i("uCubeMap", 2);
				this.getShader().setUniform1f("uReflectionAmount", mat.getReflectionAmount());
			}
		}
	},
});

Class ("vwgl.LightmapDeferredMaterial", vwgl.DeferredMaterial, {
	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.lightmap.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.lightmap.fragment);
		this.getShader().link("def_texture");

		this.loadVertexAttrib("aVertexPosition");
		this.loadTexCoord1Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uSelectMode");
		this.getShader().initUniformLocation("uColorTint");
		this.getShader().initUniformLocation("uIsEnabled");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());

		var mat = this.getSettingsMaterial();
		if (mat) {
			var color = vwgl.Vector4.white();
			color.a(mat.getDiffuse().a());
			this.getShader().setUniform("uTexture", mat.getLightMap(),vwgl.Texture.kTexture0);
			this.getShader().setUniform1i("uIsEnabled", mat.getLightMap()!=null);
			this.getShader().setUniform("uColorTint", color);
			this.getShader().setUniform("uTextureOffset", mat.getLightmapOffset());
			this.getShader().setUniform("uTextureScale", mat.getLightmapScale());
		}
	}
});

Class ("vwgl.NormalMapDeferredMaterial", vwgl.DeferredMaterial, {
	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.normal.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.normal.fragment);
		this.getShader().link("def_normalmap");

		this.loadVertexAttrib("aVertexPos");
		this.loadNormalAttrib("aNormalPos");
		this.loadTexCoord0Attrib("aNormalMapPos");
		this.loadTexCoord1Attrib("aLightNormalMapPos");
		this.loadTangentArray("aTangentPos");

		this.getShader().initUniformLocation("uMMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uVMatrix");
		this.getShader().initUniformLocation("uNMatrix");
		this.getShader().initUniformLocation("uNormalMap");
		this.getShader().initUniformLocation("uUseNormalMap");
		this.getShader().initUniformLocation("uLightNormalMap");
		this.getShader().initUniformLocation("uUseLightNormalMap");
		this.getShader().initUniformLocation("uNormalMapScale");
		this.getShader().initUniformLocation("uNormalMapOffset");

		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");
		this.getShader().initUniformLocation("uAlphaCutoff");
		
		this.getShader().initUniformLocation("uLightEmission");
		this.getShader().initUniformLocation("uLightEmissionMask");
		this.getShader().initUniformLocation("uLightEmissionMaskChannel");
		this.getShader().initUniformLocation("uLightEmissionMaskInvert");
	},

	setupUniforms:function() {
		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform("uMMatrix",this.modelMatrix());
			this.getShader().setUniform("uPMatrix",this.projectionMatrix());
			this.getShader().setUniform("uVMatrix",this.viewMatrix());
			this.getShader().setUniform("uNMatrix",this.normalMatrix());
			this.getShader().setUniform("uNormalMap",mat.getNormalMap(),vwgl.Texture.kTexture0);
			this.getShader().setUniform1i("uUseNormalMap",mat.getNormalMap()!=null);
			this.getShader().setUniform("uNormalMapOffset",mat.getNormalMapOffset());
			this.getShader().setUniform("uNormalMapScale",mat.getNormalMapScale());
			this.getShader().setUniform("uLightNormalMap",mat.getLightNormalMap(),vwgl.Texture.kTexture1);
			this.getShader().setUniform1i("uUseLightNormalMap",mat.getLightNormalMap()!=null);


			var tex = mat.getTexture() ? mat.getTexture() : vwgl.TextureManager.get().whiteTexture();
			this.getShader().setUniform("uTexture",tex,vwgl.Texture.kTexture2);
			this.getShader().setUniform("uTextureOffset", mat.getTextureOffset());
			this.getShader().setUniform("uTextureScale", mat.getTextureScale());
			this.getShader().setUniform1f("uAlphaCutoff", mat.getAlphaCutoff());

			this.getShader().setUniform1f("uLightEmission",mat.getLightEmission());
			var lightEmissionMask = mat.getLightEmissionMask() ? mat.getLightEmissionMask() : vwgl.TextureManager.get().whiteTexture();
			this.getShader().setUniform("uLightEmissionMask", lightEmissionMask, vwgl.Texture.kTexture3);
			this.getShader().setUniform1i("uLightEmissionMaskChannel", mat.getLightEmissionMaskChannel());
			this.getShader().setUniform1f("uLightEmissionMaskInvert", mat.getInvertLightEmissionMask() ? 1.0:0.0);
		}
	}
});

Class ("vwgl.SelectionDeferredMaterial", vwgl.DeferredMaterial, {
	_r:1,
	_g:1,
	_b:1,

	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.selection.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.selection.fragment);
		this.getShader().link("def_selection");

		this.loadVertexAttrib("aVertexPosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uSelected");
		this.getShader().initUniformLocation("uColor");
	},

	setupUniforms:function() {
		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
			this.getShader().setUniform("uPMatrix", this.projectionMatrix());
			this.getShader().setUniform1i("uSelected", mat.getSelectedMode());
			var color = this.getColor();
			this.getShader().setUniform("uColor", color);
		}
	},
	
	getColor:function() {
		this._r += 20;
		this._g += 30;
		this._b += 30;
		if (this._r>250) this._r = 0;
		if (this._g>250) this._g = 0;
		if (this._b>250) this._b = 0;
		return new vwgl.Color(this._r/255.0,this._g/255.0,this._b/255.0,1.0);
	}
});

Class ("vwgl.ShadowDeferredMaterial", vwgl.DeferredMaterial, {
	_shadowOffset:0.9,

	setShadowOffset:function(o) { this._shadowOffset = o; },
	getShadowOffset:function() { return this._shadowOffset; },

	initialize:function() {
		this._shadowOffset = 0.5;
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.shadow.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.shadow.fragment);
		this.getShader().link("def_shadow");

		this.loadVertexAttrib("aVertexPosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uMMatrix");

		this.getShader().initUniformLocation("uDepthMap");
		this.getShader().initUniformLocation("uDepthMapSize");
		this.getShader().initUniformLocation("uLightProjectionMatrix");
		this.getShader().initUniformLocation("uLightViewMatrix");

		this.getShader().initUniformLocation("uReceiveShadows");

		this.getShader().initUniformLocation("uShadowOffset");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		this.getShader().setUniform("uMMatrix", this.modelMatrix());

		var list = vwgl.LightManager.get().getLightList();
		var sl = null;
		for (var i=0;i<list.length;++i) {
			sl = dynamic_cast("vwgl.ShadowLight",list[i]);
			if (sl && sl.isEnabled()) break;
		}

		if (sl) {
			this.getShader().setUniform("uDepthMap", sl.getRenderPass().getFbo().getTexture(),vwgl.Texture.kTexture0);
			this.getShader().setUniform("uLightProjectionMatrix", sl.getProjectionMatrix());
			this.getShader().setUniform("uLightViewMatrix", sl.getViewMatrix());
		}



		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform1i("uReceiveShadows", mat.getReceiveShadows());
		}
	}
});

Class ("vwgl.WPositionDeferredMaterial", vwgl.DeferredMaterial, {
	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.wposition.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.wposition.fragment);
		this.getShader().link("def_wposition");

		this.loadVertexAttrib("aVertexPosition");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uMMatrix");
		
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");
		this.getShader().initUniformLocation("uAlphaCutoff");
	},

	setupUniforms:function() {
		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
			this.getShader().setUniform("uPMatrix", this.projectionMatrix());
			this.getShader().setUniform("uMMatrix", this.modelMatrix());
			
			var tex = mat.getTexture() ? mat.getTexture() : vwgl.TextureManager.get().whiteTexture();
			this.getShader().setUniform("uTexture", tex, vwgl.Texture.kTexture0);
			this.getShader().setUniform("uTextureOffset", mat.getTextureOffset());
			this.getShader().setUniform("uTextureScale", mat.getTextureScale());
			this.getShader().setUniform1f("uAlphaCutoff", mat.getAlphaCutoff());		
		}
	}
});

Class ("vwgl.MaterialDeferredMaterial", vwgl.DeferredMaterial, {
	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.material.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.material.fragment);
		this.getShader().link("def_material");

		this.loadVertexAttrib("aVertexPosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uShininess");
		this.getShader().initUniformLocation("uRefractionAmount");
		this.getShader().initUniformLocation("uLightEmission");
	},

	setupUniforms:function() {
		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
			this.getShader().setUniform("uPMatrix", this.projectionMatrix());
			this.getShader().setUniform1f("uShininess", mat.getShininess());
			this.getShader().setUniform1f("uRefractionAmount", mat.getRefractionAmount());
			this.getShader().setUniform1f("uLightEmission", mat.getLightEmission());
		}
	}
});


//////// New materials deferred V2

/// Geometry passes


Class ("vwgl.DiffuseRenderPassMaterial", vwgl.DeferredMaterial, {
	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.diffuse.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.diffuse.fragment);
		this.getShader().link("def_texture");

		this.loadVertexAttrib("aVertexPosition");
		this.loadNormalAttrib("aNormalPosition");
		this.loadTexCoord0Attrib("aTexturePosition");
		this.loadTexCoord1Attrib("aLightmapPosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uMMatrix");
		this.getShader().initUniformLocation("uNMatrix");
		this.getShader().initUniformLocation("uVMatrixInv");

		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uSelectMode");
		this.getShader().initUniformLocation("uColorTint");
		this.getShader().initUniformLocation("uIsEnabled");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");

		this.getShader().initUniformLocation("uUseLightmap");
		this.getShader().initUniformLocation("uLightmap");
		this.getShader().initUniformLocation("uLightmapOffset");
		this.getShader().initUniformLocation("uLightmapScale");

		this.getShader().initUniformLocation("uUseNormalMap");
		this.getShader().initUniformLocation("uNormalMap");
		this.getShader().initUniformLocation("uNormalMapOffset");
		this.getShader().initUniformLocation("uNormalMapScale");

		this.getShader().initUniformLocation("uCubeMap");
		this.getShader().initUniformLocation("uUseCubeMap");
		this.getShader().initUniformLocation("uReflectionAmount");
		
		this.getShader().initUniformLocation("uAlphaCutoff");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		this.getShader().setUniform("uMMatrix", this.modelMatrix());
		this.getShader().setUniform("uNMatrix", this.normalMatrix());
		var vMatrix = new vwgl.Matrix4(this.viewMatrix());
		vMatrix.invert();
		this.getShader().setUniform("uVMatrixInv", vMatrix);

		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform("uTexture", mat.getTexture(),vwgl.Texture.kTexture0);
			this.getShader().setUniform1i("uIsEnabled", mat.getTexture()!=null);
			this.getShader().setUniform("uColorTint", mat.getDiffuse());
			this.getShader().setUniform("uTextureOffset", mat.getTextureOffset());
			this.getShader().setUniform("uTextureScale", mat.getTextureScale());

			if (mat.getLightMap()) {
				this.getShader().setUniform1i("uUseLightmap",true);
				this.getShader().setUniform("uLightmap", mat.getLightMap(), vwgl.Texture.kTexture1);
				this.getShader().setUniform("uLightmapScale", mat.getLightmapScale());
				this.getShader().setUniform("uLightmapOffset", mat.getLightmapOffset());
			}
			else {
				this.getShader().setUniform1i("uUseLightmap",false);
			}

			if (mat.getNormalMap()) {
				this.getShader().setUniform1i("uUseNormalMap", true);
				this.getShader().setUniform("uNormalMap", mat.getNormalMap(),vwgl.Texture.kTexture2);
				this.getShader().setUniform("uNormalMapScale", mat.getNormalMapScale());
				this.getShader().setUniform("uNormalMapOffset", mat.getNormalMapOffset());
			}
			else {
				this.getShader().setUniform1i("uUseNormalMap", false);
			}

			if (mat.getCubeMap()) {
				this.getShader().setUniform1i("uUseCubeMap", true);
				this.getShader().setUniform("uCubeMap", mat.getCubeMap(),vwgl.Texture.kTexture3);
				this.getShader().setUniform1f("uReflectionAmount", mat.getReflectionAmount());
			}
			else {
				this.getShader().setUniform1i("uUseCubeMap", false);
				vwgl.Texture.setActiveTexture(vwgl.Texture.kTexture3);
				gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
				this.getShader().setUniform1i("uCubeMap", 3);
				this.getShader().setUniform1f("uReflectionAmount", 0.0);
			}
			
			this.getShader().setUniform1f("uAlphaCutoff",mat.getAlphaCutoff());
		}
	},
});



Class ("vwgl.SpecularRenderPassMaterial", vwgl.DeferredMaterial, {
	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.specular.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.specular.fragment);
		this.getShader().link("def_specular");

		this.loadVertexAttrib("aVertexPosition");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");

		this.getShader().initUniformLocation("uSpecularColor");
		this.getShader().initUniformLocation("uShininess");
		
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");
		this.getShader().initUniformLocation("uAlphaCutoff");
		this.getShader().initUniformLocation("uShininessMask");
		this.getShader().initUniformLocation("uShininessMaskChannel");
		this.getShader().initUniformLocation("uShininessMaskInvert");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());

		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform("uSpecularColor", mat.getSpecular());
			this.getShader().setUniform1f("uShininess", mat.getShininess());
			
			var tex = mat.getTexture() ? mat.getTexture() : vwgl.TextureManager.get().whiteTexture();
			var shininessMask = mat.getShininessMask() ? mat.getShininessMask() : vwgl.TextureManager.get().whiteTexture();
			this.getShader().setUniform("uTexture", tex, vwgl.Texture.kTexture0);
			this.getShader().setUniform("uTextureOffset", mat.getTextureOffset());
			this.getShader().setUniform("uTextureScale", mat.getTextureScale());
			this.getShader().setUniform1f("uAlphaCutoff", mat.getAlphaCutoff());
			this.getShader().setUniform("uShininessMask", shininessMask, vwgl.Texture.kTexture1);
			this.getShader().setUniform1i("uShininessMaskChannel", mat.getShininessMaskChannel());
			this.getShader().setUniform1f("uShininessMaskInvert", mat.getInvertShininessMask() ? 1:0);
			
		}
	},
});

Class ("vwgl.ShadowRenderPassMaterial", vwgl.DeferredMaterial, {
	_light:null,
	_shadowTexture:null,
	_shadowType:1,	// 1: soft shadows
	_blurIterations:1,

	setLight:function(/* vwgl.ShadowLight */ light) { this._light = dynamic_cast("vwgl.ShadowLight",light); },
	getLight:function() { return this._light; },
	setShadowTexture:function(tex) { this._shadowTexture = tex; },
	getShadowTexture:function() { return this._shadowTexture; },
	setShadowType:function(type) { this._shadowType = type; },
	getShadowType:function() { return this._shadowType; },
	setBlurIterations:function(it) { this._blurIterations = it; },
	getBlurIterations:function() { return this._blurIterations; },

	initialize:function() {
		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.shadow.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.shadow.fragment);
		this.getShader().link("def_shadow");

		this.loadVertexAttrib("aVertexPosition");
		this.loadNormalAttrib("aNormalPosition");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uMMatrix");

		this.getShader().initUniformLocation("uDepthMap");
		this.getShader().initUniformLocation("uDepthMapSize");
		this.getShader().initUniformLocation("uLightProjectionMatrix");
		this.getShader().initUniformLocation("uLightViewMatrix");
		this.getShader().initUniformLocation("uShadowColor");
		this.getShader().initUniformLocation("uShadowStrength");
		this.getShader().initUniformLocation("uShadowBias");
		
		this.getShader().initUniformLocation("uLightDirection");

		this.getShader().initUniformLocation("uReceiveShadows");
		
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uTextureScale");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uAlphaCutoff");
		this.getShader().initUniformLocation("uShadowType");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		this.getShader().setUniform("uMMatrix", this.modelMatrix());

		this.getShader().setUniform1i("uShadowType",this._shadowType);

		if (this._light) {
			this.getShader().setUniform("uDepthMap", this._shadowTexture,vwgl.Texture.kTexture0);
			
			this.getShader().setUniform("uLightProjectionMatrix", this._light.getProjectionMatrix());
			this.getShader().setUniform("uLightViewMatrix", this._light.getViewMatrix());
			this.getShader().setUniform("uShadowColor",this._light.getAmbient());
			this.getShader().setUniform1f("uShadowStrength",this._light.getShadowStrength());
			this.getShader().setUniform("uLightDirection", this._light.getDirection());
			this.getShader().setUniform1f("uShadowBias", this._light.getShadowBias());
		}

		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform1i("uReceiveShadows", mat.getReceiveShadows());
			
			var tex = mat.getTexture() ? mat.getTexture() : vwgl.TextureManager.get().whiteTexture();
			this.getShader().setUniform("uTexture", tex, vwgl.Texture.kTexture1);
			this.getShader().setUniform("uTextureScale", mat.getTextureScale());
			this.getShader().setUniform("uTextureOffset", mat.getTextureOffset());
			this.getShader().setUniform1f("uAlphaCutoff", mat.getAlphaCutoff());
		}
	}
});

vwgl.ShadowRenderPassMaterial.kShadowTypeHard = 0;
vwgl.ShadowRenderPassMaterial.kShadowTypeSoft = 1;
vwgl.ShadowRenderPassMaterial.kShadowTypeSoftStratified = 2;


////// END geometry passes


/// Lighting pass
Class ("vwgl.DeferredLighting", vwgl.Material, {
	_diffuseMap:null,
	_normalMap:null,
	_specularMap:null,
	_depthMap:null,
	_positionMap:null,
	_shadowMap:null,
	_enabledLights:1,

	_light:null,

	setDiffuseMap:function(texture) { this._diffuseMap = texture; },
	setNormalMap:function(texture) { this._normalMap = texture; },
	setSpecularMap:function(texture) { this._specularMap = texture; },
	setDepthMap:function(texture) { this._depthMap = texture; },
	setPositionMap:function(texture) { this._positionMap = texture; },
	setShadowMap:function(texture) { this._shadowMap = texture; },
	setEnabledLights:function(l) { this._enabledLights = l; },

	setLight:function(light) {
		this._light = light;
	},

	initialize:function() {
		this.initShaderSync(null);
	},

	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;

		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.lighting_pass.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.lighting_pass.fragment);
		this.getShader().link("deferred_lighting");

		this.loadVertexAttrib("aVertexPos");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uDiffuseMap");
		this.getShader().initUniformLocation("uNormalMap");
		this.getShader().initUniformLocation("uSpecularMap");
		this.getShader().initUniformLocation("uPositionMap");
		this.getShader().initUniformLocation("uShadowMap");
		this.getShader().initUniformLocation("uShadowMapSize");

		this.getShader().initUniformLocation("uLightType");
		this.getShader().initUniformLocation("uLightDirection");
		this.getShader().initUniformLocation("uLightPosition");
		this.getShader().initUniformLocation("uLightAmbient");
		this.getShader().initUniformLocation("uLightDiffuse");
		this.getShader().initUniformLocation("uLightSpecular");
		this.getShader().initUniformLocation("uLightAttenuation");
		this.getShader().initUniformLocation("uSpotDirection");
		this.getShader().initUniformLocation("uSpotExponent");
		this.getShader().initUniformLocation("uSpotCutoff");
		this.getShader().initUniformLocation("uCutoffDistance");
		this.getShader().initUniformLocation("uLightEmissionMult");

		this.getShader().initUniformLocation("uBlurKernel");
	},

	setupUniforms:function() {

		this.getShader().setUniform("uDiffuseMap",this._diffuseMap,vwgl.Texture.kTexture0);
		this.getShader().setUniform("uNormalMap",this._normalMap,vwgl.Texture.kTexture1);
		this.getShader().setUniform("uSpecularMap",this._specularMap,vwgl.Texture.kTexture2);
		this.getShader().setUniform("uPositionMap",this._positionMap,vwgl.Texture.kTexture3);
		this.getShader().setUniform("uShadowMap",this._shadowMap,vwgl.Texture.kTexture4);
		this.getShader().setUniform("uShadowMapSize",new vwgl.Vector2(1024,1024));

		var blurKernel = [
			0.045, 0.122, 0.045,
			0.122, 0.332, 0.122,
			0.045, 0.122, 0.045
		];
		this.getShader().setUniform1fv("uBlurKernel",blurKernel);
		var lightEmissionMult = 1.0 / this._enabledLights;
		if (this._light) {
			var viewMatrix = vwgl.CameraManager.get().getMainCamera().getViewMatrix();
			var lightPos = new vwgl.Vector4(this._light.getPosition());
			lightPos.w(1.0);
			lightPos = viewMatrix.multVector(lightPos);

			this.getShader().setUniform1i("uLightType",this._light.getType());
			this.getShader().setUniform("uLightDirection",this._light.getDirection());
			this.getShader().setUniform("uLightPosition",lightPos.xyz());
			this.getShader().setUniform("uLightAmbient",this._light.getAmbient());
			this.getShader().setUniform("uLightDiffuse",this._light.getDiffuse());
			this.getShader().setUniform("uLightSpecular",this._light.getSpecular());
			var attenuation = new vwgl.Vector3(	this._light.getConstantAttenuation(),
												this._light.getLinearAttenuation(),
												this._light.getExpAttenuation());
			this.getShader().setUniform("uLightAttenuation",attenuation);
			this.getShader().setUniform("uSpotDirection",this._light.getSpotDirection());
			this.getShader().setUniform1f("uSpotExponent",this._light.getSpotExponent());
			this.getShader().setUniform1f("uSpotCutoff",this._light.getSpotCutoff());
			this.getShader().setUniform1f("uCutoffDistance",this._light.getCutoffDistance());
			this.getShader().setUniform1f("uLightEmissionMult", lightEmissionMult);
		}
		else {
			this.getShader().setUniform1i("uLightType",vwgl.Light.kTypeDisabled);
			this.getShader().setUniform("uLightDirection",vwgl.Vector3(0.0,1.0,0.0));
			this.getShader().setUniform("uLightPosition",vwgl.Vector3(0.0,0.0,0.0));
			this.getShader().setUniform("uLightAmbient",vwgl.Color.black());
			this.getShader().setUniform("uLightDiffuse",vwgl.Color.black());
			this.getShader().setUniform("uLightSpecular",vwgl.Color.black());
			this.getShader().setUniform("uLightAttenuation",vwgl.Vector3(0.0));
			this.getShader().setUniform("uSpotDirection",vwgl.Vector3(0.0,1.0,0.0));
			this.getShader().setUniform1f("uSpotExponent",0);
			this.getShader().setUniform1f("uSpotCutoff",0);
			this.getShader().setUniform1f("uCutoffDistance",0);
			this.getShader().setUniform1f("uLightEmissionMult", lightEmissionMult);
		}
	}
});
// END Lighting pass



////// Postprocess
Class ("vwgl.SSAOMaterial", vwgl.DeferredMaterial,{
	_positionMap:null,		// Texture
	_normalMap:null,		// Texture
	_randomTexture:null,	// Texture

	_enabled:false,
	
	_currentKernelSize:0,	// int
	_kernelSize:0,			// int
	_viewportSize:null,		// Vector2
	_sampleRadius:0.0,		// float
	_kernelOffsets:null,	// float []
	_color:null,			// Color
	_maxDistance:0.0,		// float
	
	
	setPositionMap:function(/* Texture */ map) { this._positionMap = map; },
	setNormalMap:function(/* Texture */ map) { this._normalMap = map; },
	
	setKernelSize:function(/* int */ size) { this._kernelSize = size<=vwgl.SSAOMaterial.s_maxKernelSize ? size:vwgl.SSAOMaterial.s_maxKernelSize; },
	setSampleRadius:function(/* float */ r) { this._sampleRadius = r; },
	setColor:function(/* Color */ c) { this._color = c; },
	setViewportSize:function(/* Size2D */ size) { this._viewportSize = size; },
	setEnabled:function(/* bool */ e) { this._enabled = e; },
	setBlurIterations:function(/* int */ it) { this._blur = it; },
	setMaxDistance:function(/* float */ dist) { this._maxDistance = dist; },

	getKernelSize:function() { return this._kernelSize; },
	getSampleRadius:function() { return this._sampleRadius; },
	getColor:function() { return this._color; },
	isEnabled:function() { return this._enabled; },
	getBlurIterations:function() { return this._blur; },
	getMaxDistance:function() { return this._maxDistance; },

	getRandomTexture:function() {
		if (!this._randomTexture) {
			this._randomTexture = vwgl.Texture.getRandomTexture();
		}
		return this._randomTexture;
	},
	
	setRandomTexture:function( /* Texture */ tex) { this._randomTexture = tex; },
	

	initialize:function() {
		this._kernelOffsets = [];
		
		this._enabled = true;
		this._currentKernelSize = 0;
		this._kernelSize = 16;
		this._sampleRadius = 2.0;
		this._color = vwgl.Color.black();
		this._blur = 4;
		this._maxDistance = 100000.0;

		this.initShaderSync(null);
	},

	getResourceList:function() { return []; },

	initShaderSync:function() {
		if (this._initialized) return;
		this._initialized = true;
		
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.deferred_shaders.ssao_pass.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.deferred_shaders.ssao_pass.fragment);
		this.getShader().link("def_ssao");

		this.loadVertexAttrib("aVertexPos");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uPositionMap");
		this.getShader().initUniformLocation("uNormalMap");

		this.getShader().initUniformLocation("uProjectionMat");
	
		this.getShader().initUniformLocation("uSSAORandomMap");
		this.getShader().initUniformLocation("uRandomTexSize");
		this.getShader().initUniformLocation("uViewportSize");
		this.getShader().initUniformLocation("uSSAOSampleRadius");
		this.getShader().initUniformLocation("uSSAOKernelOffsets");
		this.getShader().initUniformLocation("uSSAOKernelSize");
		this.getShader().initUniformLocation("uSSAOColor");
		this.getShader().initUniformLocation("uEnabled");
		this.getShader().initUniformLocation("uMaxDistance");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uPositionMap", this._positionMap, vwgl.Texture.kTexture0);
		this.getShader().setUniform("uNormalMap", this._normalMap, vwgl.Texture.kTexture1);

		var proj = this.projectionMatrix();
		var cam = vwgl.CameraManager.get().getMainCamera();
		proj = cam.getProjectionMatrix();
		this.getShader().setUniform("uProjectionMat", this.projectionMatrix());

		if (this._currentKernelSize != this._kernelSize) {
			this._kernelOffsets = [];
			
			for (var i=0; i<this._kernelSize*3; i+=3) {
				var kernel = new vwgl.Vector3(vwgl.Math.random() * 2.0 - 1.0,
								   			  vwgl.Math.random() * 2.0 - 1.0,
								   		   	  vwgl.Math.random());
				kernel.normalize();

				var scale = (i/3.0) / this._kernelSize;
				scale = vwgl.Math.lerp(0.1, 1.0, scale * scale);
			
				kernel.scale(scale);
			
				this._kernelOffsets.push(kernel.x());
				this._kernelOffsets.push(kernel.y());
				this._kernelOffsets.push(kernel.z());
			}
			this._currentKernelSize = this._kernelSize;
		}
	
		this.getShader().setUniform("uSSAORandomMap", this.getRandomTexture(), vwgl.Texture.kTexture2);
		this.getShader().setUniform("uRandomTexSize", new vwgl.Vector2(vwgl.Texture.getRandomTextureSize()));
		this.getShader().setUniform("uViewportSize", this._viewportSize);
		this.getShader().setUniform1f("uSSAOSampleRadius", this._sampleRadius);
		this.getShader().setUniform3fv("uSSAOKernelOffsets", this._kernelOffsets);
		this.getShader().setUniform1i("uSSAOKernelSize", this._kernelSize);
		this.getShader().setUniform("uSSAOColor", this._color);
		this.getShader().setUniform1i("uEnabled", this._enabled);
		this.getShader().setUniform1f("uMaxDistance", this._maxDistance);
	}
});

vwgl.SSAOMaterial.s_maxKernelSize = 64;
