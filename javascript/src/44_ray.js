Class ("vwgl.physics.Ray", {
	_start:null,			//	vwgl.Vector3
	_end:null,			//	vwgl.Vector3
	_vector:null,		//	vwgl.Vector3	
	_magnitude:null,		//	number

	initialize:function() {
		this._start = new vwgl.Vector3(0);
		this._end = new vwgl.Vector3(1);
		this._calculateVector();
	},

	setWithPoints:function(/* Vector3 */ p0, /* Vector3 */ p1) {
		this._start.assign(p0);
		this._end.assign(p1);
		this._calculateVector();
		return this;
	},

	setWithVector:function(/* Vector3 */ vec, /* Vector3 */ origin, /* float */ maxDepth) {
		this._start.assign(origin);
		this._end.assign(origin);
		var vector = new vwgl.Vector3(vec);
		vector.normalize().scale(maxDepth);
		this._end.add(vector);
		this._calculateVector();
		return this;
	},

	setWithScreenPoint:function(/* Position2D */ screenPoint, /* Matrix4 */ projMatrix, /* Matrix4 */ viewMatrix, /* Viewport */ viewport) {
		var start = vwgl.Matrix4.unproject(screenPoint.x(), screenPoint.y(), 0.0, viewMatrix, projMatrix, viewport);
		var end = vwgl.Matrix4.unproject(screenPoint.x(), screenPoint.y(), 1.0, viewMatrix, projMatrix, viewport);
		this._start = start.xyz();
		this._end = end.xyz();
		this._calculateVector();
		return this;
	},

	assign:function(/* Ray */ ray) {
		this._start.assign(ray._start);
		this._end.assign(ray._end);
		this._vector.assign(ray._vector);
		this._magnitude = ray._magnitude;
	},

	setStart:function(/* Vector3 */ s) { this._start.assign(s); this._calculateVector(); },
	getStart:function() { return this._start; },

	setEnd:function(/* Vector3 */ e) { this._end.assign(e); this._calculateVector(); },
	getEnd:function() { return this._end; },

	getVector:function() { return this._vector; },
	getMagnitude:function() { return this._magnitude; },
	
	equals:function(/* vwgl.Ray */ r) {
		return this._start.equals(r._start) && this._end.equals(r._end);
	},
	
	toString:function() {
		return "start:" + this._start.toString() + ", end:" + this._end.toString();
	},

	_calculateVector:function() {
		this._vector = new vwgl.Vector3(this._end);
		this._vector.sub(this._start);
		this._magnitude = this._vector.magnitude();
		this._vector.normalize();
	}
});

vwgl.physics.Ray.rayWithPoints = function(/* Vector3 */ p0, /* Vector3 */ p1) {
	var ray = new vwgl.physics.Ray();
	ray.setWithPoints(p0, p1);
	return ray;
}

vwgl.physics.Ray.rayWithVector = function(/* Vector3 */ vec, /* Vector3 */ origin, /* float */ maxDepth) {
	var ray = new vwgl.physics.Ray();
	ray.setWithVector(vec, origin, maxDepth);
	return ray;
}

vwgl.physics.Ray.rayWithScreenPoint = function(/* Position2D */ screenPoint, /* Matrix4 */ projMatrix, /* Matrix4 */ viewMatrix, /* Viewport */ viewport) {
	var ray = new vwgl.physics.Ray();
	ray.setWithScreenPoint(screenPoint, projMatrix, viewMatrix, viewport);
	return ray;
}
