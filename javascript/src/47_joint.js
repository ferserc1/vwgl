Class ("vwgl.physics.Joint", {
	initialize:function() {
		
	},
	
	applyTransform:function(/* vwgl.Matrix4 */ modelview) {}
	
	// Gizmo functions are only available in C++ API:
	// 	initDrawable()
	// 	getDrawable()
});

Class ("vwgl.physics.LinkJoint", vwgl.physics.Joint, {
	_offset:null, 			// vwgl.Vector3
	_eulerRotation:null,	// vwgl.Vector3
	_transform:null,		// vwgl.Matrix4

	initialize:function() {
		this._offset = new vwgl.Vector3();
		this._eulerRotation = new vwgl.Vector3();
		this._transform = new vwgl.Matrix4();
		this._transform.identity();
	},
	
	applyTransform:function(/* Matrix4 */ modelview) {
		modelview.translate(this._offset.x(), this._offset.y(), this._offset.z());
		modelview.mult(this._transform);
	},

	offset:function() { return this._offset; },
	setOffset:function( /* vwgl.Vector3 */ offset) { this._offset.assign(offset); this._calculateTransform(); },
	
	eulerRotation:function() {
		return this._eulerRotation;
	},
	
	getYaw:function() { return this._eulerRotation.x(); },
	getPitch:function() { return this._eulerRotation.y(); },
	getRoll:function() { return this._eulerRotation.z(); },
	
	setYaw:function(/* float */ x) { this._eulerRotation.x(x); this._calculateTransform(); },
	setPitch:function(/* float */ y) { this._eulerRotation.y(y); this._calculateTransform(); },
	setRoll:function(/* float */ z) { this._eulerRotation.z(z); this._calculateTransform();  },
	
	transform:function() { return this._transform; },
	
	_calculateTransform:function() {
		this._transform.identity()
					.rotate(this._eulerRotation.x(), 1.0, 0.0, 0.0)
					.rotate(this._eulerRotation.y(), 0.0, 1.0, 0.0)
					.rotate(this._eulerRotation.z(), 0.0, 0.0, 1.0);
	}
});

