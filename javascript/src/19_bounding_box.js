
Class ("vwgl.BuildBoundingBoxVisitor", vwgl.NodeVisitor, {
	_boundingBox:null,		// vwgl.BoundingBox
	_currentMatrix:null,

	initialize:function(/* vwgl.BoundingBox */ bbox) { this._boundingBox = bbox; },

	visit:function(/* vwgl.Node */ node) {
		if (this._currentMatrix==null) this._currentMatrix = vwgl.Matrix4.makeIdentity();

		var grp = dynamic_cast("vwgl.Group",node);
		var drw = dynamic_cast("vwgl.DrawableNode",node);
		var trx = dynamic_cast("vwgl.TransformNode",node);

		if (trx) {
			this._currentMatrix.mult(trx.getTransform());
		}
		if (grp) {
			for (var i=0; i < grp.getNodeList().length; i++) {
				this.visit(grp.getNodeList()[i]);
			}
		}
		if (drw) {
			var drawable = drw.getDrawable();
			var localBBox = new vwgl.BoundingBox();
			localBBox.setVertexData(drawable);
			var min = new vwgl.Vector4(localBBox.min().raw());
			min.w(1.0);
			var max = new vwgl.Vector4(localBBox.max().raw());
			max.w(1.0);

			min = this._currentMatrix.multVector(min);
			max = this._currentMatrix.multVector(max);

			this._boundingBox.addVertexData(min.raw());
			this._boundingBox.addVertexData(max.raw());
		}
		if (trx) {
			var invM = new vwgl.Matrix4(trx.getTransform());
			invM.invert();
			this._currentMatrix.mult(invM);
		}
	}
});

Class ("vwgl.BoundingBox", {
	_left:0,		// float
	_right:0,		// float
	_top:0,			// float
	_bottom:0,		// float
	_back:0,		// float
	_front:0,		// float

	_max:null,		// vwgl.Vector3
	_min:null,		// vwgl.Vector3
	_size:null,		// vwgl.Vector3
	_halfSize:null,	// vwgl.Vector3
	_center:null,	// vwgl.Vector3

	initialize:function(a) {
		this.reset();
		if (a) this.setVertexData(a);
	},

	setVertexData:function(/* float [] | vwgl.PolyList | vwgl.Drawable */ a) {
		this.reset();
		this.addVertexData(a);
	},

	addVertexData:function(/* float [] | vwgl.PolyList | vwgl.Drawable | vwgl.Group */ a) {
		if (typeof(a)=='object' && a.length) this.addVertexDataWithArray(a);
		else if (dynamic_cast("vwgl.PolyList",a)) this.addVertexDataWithPolyList(a);
		else if (dynamic_cast("vwgl.Drawable",a)) this.addVertexDataWithDrawable(a);
		else if (dynamic_cast("vwgl.Group",a)) this.addVertexDataWithGroup(a);
	},

	addVertexDataWithArray:function(vertexData) {
		if (typeof(vertexData)=='object' && vertexData.length>0) {
			for (var i=0; i<vertexData.length-2; i+=3) {
				this.addVertex(new vwgl.Vector3(vertexData[i],vertexData[i+1],vertexData[i+2]));
			}
			this._calculateBounds();
		}
	},

	addVertexDataWithPolyList:function(/* PolyList */ plist) {
		if (dynamic_cast("vwgl.PolyList",plist)) this.addVertexData(plist.getVertexPointer());
	},

	addVertexDataWithDrawable:function(/* Drawable */ drawable) {
		for (var i=0; i<drawable.getSolidList().length; ++i) {
			if (drawable.getSolidList()[i].isVisible()) {
				this.addVertexData(drawable.getSolidList()[i].getPolyList());
			}
		}
	},

	addVertexDataWithGroup:function(/* vwgl.Group */ group) {
		var visitor = new vwgl.BuildBoundingBoxVisitor(this);
		visitor.visit(group);
	},

	addVertex:function(/* vwgl.Vector3 */ v) {
		this._left	 = this._left<v._v[0] ? this._left:v._v[0];
		this._right	 = this._right>v._v[0] ? this._right:v._v[0];
		this._top	 = this._top>v._v[1] ? this._top:v._v[1];
		this._bottom = this._bottom<v._v[1] ? this._bottom:v._v[1];
		this._back	 = this._back<v._v[2] ? this._back:v._v[2];
		this._front	 = this._front>v._v[2] ? this._front:v._v[2];
	},

	reset:function() {
		this._left 		= Number.MAX_VALUE;
		this._right 	= -Number.MAX_VALUE;
		this._top 		= -Number.MAX_VALUE;
		this._bottom 	= Number.MAX_VALUE;
		this._back 		= Number.MAX_VALUE;
		this._front 	= -Number.MAX_VALUE;
		this._max 		= new vwgl.Vector3(0);
		this._min 		= new vwgl.Vector3(0);
		this._size 		= new vwgl.Vector3(0);
		this._halfSize 	= new vwgl.Vector3(0);
		this._center 	= new vwgl.Vector3(0);
	},

	max:function() { return this._max; },
	min:function() { return this._min; },
	size:function() { return this._size; },
	halfSize:function() { return this._halfSize; },
	center:function() { return this._center; },

	_calculateBounds:function() {
		this._max.set(this._right,this._top,this._front);
		this._min.set(this._left,this._bottom,this._back);
		this._size.assign(this._max); this._size.sub(this._min);
		this._halfSize.assign(this._size); this._halfSize.scale(0.5);
		this._center.assign(this._halfSize); this._center.add(this._min);
	}
});
