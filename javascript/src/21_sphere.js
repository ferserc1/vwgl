
Class ("vwgl.Sphere", vwgl.Drawable, {
	initialize:function(radius, slices, stacks) {
		this.parent();
		if (!radius) radius = 1;
		if (!slices) slices = 15;
		if (!stacks) stacks = slices;
		this.buildSphere(radius,slices,stacks);
	},
	
	buildSphere:function(radius, slices, stacks) {
		var plist = new vwgl.PolyList();
	
		var latitudeBands = stacks;
		var longitudeBands = slices;
		for (var latNumber = 0; latNumber <= latitudeBands; latNumber++) {
			var theta = latNumber *  vwgl.Math.kPi / latitudeBands;
			var sinTheta = vwgl.Math.sin(theta);
			var cosTheta = vwgl.Math.cos(theta);
	
			for (var longNumber = 0; longNumber <= longitudeBands; longNumber++) {
				var phi = longNumber * 2 * vwgl.Math.kPi / longitudeBands;
				var sinPhi = vwgl.Math.sin(phi);
				var cosPhi = vwgl.Math.cos(phi);
		
				var x = cosPhi * sinTheta;
				var y = cosTheta;
				var z = sinPhi * sinTheta;
				var u = 1.0 - (longNumber / longitudeBands);
				var v = 1.0 - (latNumber / latitudeBands);
		
				plist.addNormal(new vwgl.Vector3(x,y,z));
				plist.addTexCoord0(new vwgl.Vector2(u,v));
				plist.addVertex(new vwgl.Vector3(radius * x, radius * y, radius * z));
			}
		}

		for (var latNumber = 0; latNumber < latitudeBands; latNumber++) {
			for (var longNumber = 0; longNumber < longitudeBands; longNumber++) {
				var first = (latNumber * (longitudeBands + 1)) + longNumber;
				var second = first + longitudeBands + 1;
				plist.addTriangle(first + 1, second, first);
				plist.addTriangle(first + 1, second + 1, second);
			}
		}

		plist.buildPolyList();
		this.addSolid(new vwgl.Solid(plist));
	}
});
