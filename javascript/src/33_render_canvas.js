
Class ("vwgl.RenderCanvas", {
	_material:null,		// vwgl.Material
	_plist:null,		// vwgl.PolyList
	_projection:null,	// vwgl.Matrix4

	initialize:function() {
		this._plist = new vwgl.PolyList();
	
		this._plist.addVertex(new vwgl.Vector3( 0.5, 0.5, 0.0));
		this._plist.addVertex(new vwgl.Vector3(-0.5, 0.5, 0.0));
		this._plist.addVertex(new vwgl.Vector3(-0.5,-0.5, 0.0));
		this._plist.addVertex(new vwgl.Vector3( 0.5,-0.5, 0.0));
	
		this._plist.addTexCoord0(new vwgl.Vector2(1.0,1.0));
		this._plist.addTexCoord0(new vwgl.Vector2(0.0,1.0));
		this._plist.addTexCoord0(new vwgl.Vector2(0.0,0.0));
		this._plist.addTexCoord0(new vwgl.Vector2(1.0,0.0));
	
		this._plist.addTriangle(0,1,2);
		this._plist.addTriangle(2,3,0);
	
		this._plist.buildPolyList();
	
		this._plist.setDrawMode(vwgl.PolyList.kTriangles);
	},

	setMaterial:function(/* Material */ m) { this._material = m; },
	getMaterial:function() { return this._material; },

	draw:function(w, h) {
		if (this._material) {
			var s = vwgl.State.get();
			gl.viewport(0,0,w,h);
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
			s.projectionMatrix().assign(vwgl.Matrix4.makeOrtho(-0.5, 0.5, -0.5, 0.5, 0.1, 100.0));
			s.viewMatrix().assign(vwgl.Matrix4.makeTranslation(0.0, 0.0, -1.0));
			s.modelMatrix().assign(vwgl.Matrix4.makeIdentity());

			this._material.bindPolyList(this._plist);
			this._material.activate();
			this._plist.drawElements();
			this._material.deactivate();
		}
	},

	destroy:function() {
		this._plist.destroy();
	}
});

Class ("vwgl.HomogeneousRenderCanvas", vwgl.RenderCanvas, {
	_material:null,		// vwgl.Material
	_plist:null,		// vwgl.PolyList
	_projection:null,	// vwgl.Matrix4

	_clear:true,

	initialize:function() {
		this._plist = new vwgl.PolyList();

		this._plist.addVertex(new vwgl.Vector3( 1.0, 1.0, 0.0));
		this._plist.addVertex(new vwgl.Vector3(-1.0, 1.0, 0.0));
		this._plist.addVertex(new vwgl.Vector3(-1.0,-1.0, 0.0));
		this._plist.addVertex(new vwgl.Vector3( 1.0,-1.0, 0.0));

		this._plist.addTexCoord0(new vwgl.Vector2(1.0,1.0));
		this._plist.addTexCoord0(new vwgl.Vector2(0.0,1.0));
		this._plist.addTexCoord0(new vwgl.Vector2(0.0,0.0));
		this._plist.addTexCoord0(new vwgl.Vector2(1.0,0.0));

		this._plist.addTriangle(0,1,2);
		this._plist.addTriangle(2,3,0);

		this._plist.buildPolyList();

		this._plist.setDrawMode(vwgl.PolyList.kTriangles);
	},

	setMaterial:function(/* Material */ m) { this._material = m; },
	getMaterial:function() { return this._material; },

	setClearBuffer:function(clear) { this._clear = clear; },
	getClearBuffer:function() { return this._clear; },

	draw:function(w, h) {
		if (this._material) {
			var s = vwgl.State.get();
			gl.viewport(0,0,w,h);
			if (this._clear) gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

			this._material.bindPolyList(this._plist);
			this._material.activate();
			this._plist.drawElements();
			this._material.deactivate();
		}
	},

	destroy:function() {
		this._plist.destroy();
	}
});
