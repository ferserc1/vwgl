
Class ("vwgl.ProjectionFactoryMethod", {
	_viewport:null, 	// vwgl.Viewport
	_matrix:null,		// vwgl.Matrix4
	_near:0.1,			// float
	_far:100.0,			// float
	
	initialize:function() {
		this._near = 0.1;
		this._far = 100.0;
		this._viewport = new vwgl.Viewport();
	},
	
	updateMatrix:null,		// virtual function
	
	setViewport:function(/* vwgl.Viewport */ vp) { this._viewport.assign(vp); },
	getViewport:function() { return this._viewport; },
	setNear:function(near) { this._near = near; },
	setFar:function(far) { this._far = far; },
	getNear:function() { return this._near; },
	getFar:function() { return this._far; },
	
	valid:function() { return this._matrix!=null; },
	setMatrixRef:function(/* vwgl.Matrix4 */ m) { this._matrix = m; },
	getMatrix:function() { return this._matrix; }
});

Class ("vwgl.PerspectiveProjectionMethod", vwgl.ProjectionFactoryMethod, {
	_fov:45.0,		// float
	
	initialize:function(fov) {
		this.parent();
		if (fov===undefined) fov = 45.0;
		this._fov = fov;
	},
	
	setFov:function(fov) { this._fov = fov; },
	getFov:function() { return this._fov; },
	
	updateMatrix:function() { if (this.valid()) this.getMatrix().perspective(_fov, this.getViewport().aspectRatio(), this.getNear(), this.getFar()); }
});

Class ("vwgl.OpticalProjectionMethod", vwgl.ProjectionFactoryMethod, {
	_focalLength:0,
	_frameSize:0,

	initialize:function(focalLength, frameSize) {
		this.parent();
		if (focalLength===undefined) focalLength = 50.0;
		if (frameSize===undefined) frameSize = 35.0;
		this._focalLength = focalLength;
		this._frameSize = frameSize;
	},
	
	setFocalLength:function(fl) { this._focalLength = fl; },
	setFilmSize:function(fs) { this._frameSize = fs; },
	getFocalLength:function() { return this._focalLength; },
	getFilmSize:function() { return this._frameSize; },

	updateMatrix:function() {
		if (this.valid()) {
			var fov = 2.0 * vwgl.Math.atan(this._frameSize / (this._focalLength * 2.0));
			this.getMatrix().perspective(vwgl.Math.radiansToDegrees(fov), this.getViewport().aspectRatio(), this.getNear(), this.getFar());
		}
	}
});

Class ("vwgl.Camera", vwgl.Node, {
	_projection:null,		// vwgl.Matrix4
	_transformVisitor:null,	// vwgl.TransformVisitor
	_fooTransform:null,
	
	_projectionMethod:null,	// vwgl.ProjectionFactoryMethod
	
	initialize:function() {
		this._projection = vwgl.Matrix4.makeIdentity();
		this._fooTransform = vwgl.Matrix4.makeIdentity();
		vwgl.CameraManager.get().addCamera(this);
		this._transformVisitor = new vwgl.TransformVisitor();
	},

	getProjectionMatrix:function() {
		if (this._projectionMethod) this._projectionMethod.updateMatrix();
		return this._projection;
	},

	getViewMatrix:function() {
		this._transformVisitor.visit(this);
		this._transformVisitor.getTransform().invert();
		return this._transformVisitor.getTransform();
	},

	getTransform:function() {
		var trNode = dynamic_cast("vwgl.TransformNode",this.getParent());
		if (trNode) {
			return trNode.getTransform();
		}
		return this._fooTransform;
	},
	
	setProjectionMethod:function(method) {
		this._projectionMethod = method;
		if (this._projectionMethod) {
			this._projectionMethod.setMatrixRef(this._projection);
		}
	},
	
	getProjectionMethod:function() { return this._projectionMethod; },

	destroy:function() {
		vwgl.CameraManager.get().removeCamera(this);
		this.parent();
	}
});

Class ("vwgl.CameraManager", {
	_cameraList:null,		// vwgl.Camera []
	_mainCamera:null,		// vwgl.Camera

	initialize:function() {
		this._cameraList = [];
	},

	getMainCamera:function() { return this._mainCamera; },
	setMainCamera:function(/* Camera */ cam) { if (cam) this._mainCamera = cam; },
	
	applyTransform:function(/* Camera */ cam) {
		if (!cam) cam = this._mainCamera;
		if (cam) {
			vwgl.State.get().projectionMatrix().assign(cam.getProjectionMatrix());
			vwgl.State.get().viewMatrix().assign(cam.getViewMatrix());
		}
	},

	addCamera:function(/* Camera */ cam) {
		var i = this._cameraList.indexOf(cam);
		if (i<0) {
			this._cameraList.push(cam);
		}
		if (!this._mainCamera) this._mainCamera = cam;
	},

	removeCamera:function(/* Camera */ cam) {
		var i = this._cameraList.indexOf(cam);
		if (i<-1) {
			this._cameraList.splice(i,1);
		}
		if (this._mainCamera==cam && this._cameraList.length>0) {
			this._mainCamera = this._cameraList[0]
		}
		else if (this._cameraList.length==0) {
			this._mainCamera = null;
		}
	}
});

vwgl.CameraManager.s_singleton = null;
vwgl.CameraManager.get = function() {
	if (!vwgl.CameraManager.s_singleton) {
		vwgl.CameraManager.s_singleton = new vwgl.CameraManager();
	}
	return vwgl.CameraManager.s_singleton;
}