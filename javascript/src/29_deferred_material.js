Class ("vwgl.DeferredMaterial", vwgl.Material, {
	_settingsMaterial:null,		// vwgl.GenericMaterial
	_cullFaceBkp:null,
	
	initialize:function(readyCallback) {
		this.parent(readyCallback);
	},

	useSettingsOf:function(/* Material */ mat) { this._settingsMaterial = dynamic_cast("vwgl.GenericMaterial",mat); },

	activate:function() {
		this._cullFaceBkp = this._cullFace;

		if (this._settingsMaterial) {
			this.setCullFace(this._settingsMaterial.getCullFace());
		}
		this.parent();
	},

	deactivate:function() {
		this.parent();
		this._cullFace = this._cullFaceBkp;
	},
	
	getSettingsMaterial:function() { return this._settingsMaterial; }
});