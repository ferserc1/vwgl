Class ("vwgl.NodeVisitor", {
	initialize:function() {},

	visit:function(/* Node */ node) {}
});

Class ("vwgl.DrawNodeVisitor", vwgl.NodeVisitor, {
	_material:null,					// vwgl.Material
	_useMaterialSettings:false,		// bool
	_renderOpaque:true,				// bool
	_renderTransparent:true,		// bool

	setMaterial:function(/* Material */ mat) { this._material = mat; },
	getMaterial:function() { return this._material; },
	setUseMaterialSettings:function(s) { this._useMaterialSettings = s; },

	setRenderOpaque:function(opaque) { this._renderOpaque = opaque; },
	getRenderOpaque:function() { return this._renderOpaque; },
	
	setRenderTransparent:function(transparent) { this._renderTransparent = transparent; },
	getRenderTransparent:function() { return this._renderTransparent; },
	
	visit:function(/* Node */ node) {
		var gn = dynamic_cast("vwgl.Group",node);
		var tn = dynamic_cast("vwgl.TransformNode",node);
		var dn = dynamic_cast("vwgl.DrawableNode",node);

		vwgl.State.get().pushModelMatrix();
		if (tn) {
			vwgl.State.get().modelMatrix().mult(tn.getTransform());
		}
		if (gn) {
			for (var i=0; i<gn.getNodeList().length;++i) {
				this.visit(gn.getNodeList()[i]);
			}
		}
		if (dn && dn.getDrawable()) {
			dn.getDrawable().setRenderTransparent(this._renderTransparent);
			dn.getDrawable().setRenderOpaque(this._renderOpaque);
			dn.getDrawable().draw(this._material,this._useMaterialSettings);
		}
		vwgl.State.get().popModelMatrix();
	},
	
	destroy:function() {}
});

Class ("vwgl.InitVisitor", vwgl.NodeVisitor, {
	_initNodes:null,	// vwgl.Node []

	visit:function(/* Node */ node) {
		this._initNodes = [];
		this.doVisit(node);
		this.init();
	},
	
	doVisit:function(/* Node */ node) {
		var grp = dynamic_cast("vwgl.Group",node);
	
		if (grp) {
			var list = grp.getNodeList();
			for (var i=0;i<list.length;++i) {
				this.doVisit(list[i]);
			}
		}
		if (!node.isInitialized()) {
			this._initNodes.push(node);
		}
	},
	
	init:function() {
		for (var i=0; i<this._initNodes.length; ++i) {
			var node = this._initNodes[i];
			if (!node.isInitialized()) {
				node.init();
				node.setInitialized(true);
			}
		}
	}
});

Class ("vwgl.TransformVisitor", vwgl.NodeVisitor, {
	_transform:null, 	// vwgl.Matrix4
	
	visit:function(/* Node */ node) {
		var trn = dynamic_cast("vwgl.TransformNode",node);
		var parent = node.getParent();

		if (parent) {
			this.visit(parent);
		}
		else {
			this._transform = vwgl.Matrix4.makeIdentity();
		}

		if (trn) {
			this._transform.mult(trn.getTransform());
		}
	},

	getTransform:function() { return this._transform; }
});

Class ("vwgl.UpdateVisitor", vwgl.NodeVisitor, {
	visit:function(/* Node */ node) {
		var group = dynamic_cast("vwgl.Group",node);
		
		node.update();
		if (group) {
			for (var i=0; i<group.getNodeList().length; ++i) {
				this.visit(group.getNodeList()[i]);
			}
		}
	}
});
