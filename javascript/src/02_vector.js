
Class ("vwgl.Vector2", {
	_v:null,

	initialize:function(a,b) {
		if (typeof(a)=='object' && a.length>=2) {
			this._v = [a[0],a[1]];
		}
		else if (typeof(a)=='object' && a._v!==undefined && a._v.length>=2) {
			this._v = [a._v[0],a._v[1]];
		}
		else if (a!==undefined && b===undefined) {
			this._v = [a,a];
		}
		else {
			this._v = [0,0];
			if (a!==undefined) { this._v[0] = a; this._v[1] = a; }
			if (b!==undefined) this._v[1] = b;
		}
	},

	raw:function() { return this._v; },

	// In c++ API this function is called "length", in Javascript has been renamed to "size"
	// to distinguish it from array.length function
	size:function() { return this._v.length; },

	magnitude:function() { return vwgl.Math.sqrt(this._v[0] * this._v[0] + this._v[1] * this._v[1]); },

	distance:function(other) {
		var v3 = new vwgl.Vector2(this._v[0] - other._v[0],
															this._v[1] - other._v[1]);
		return v3.magnitude();
	},

	normalize:function() {
		var m = this.magnitude();
		this._v[0] = this._v[0]/m; this._v[1]=this._v[1]/m;
		return this;
	},

	add:function(v2) {
		this._v[0] += v2._v[0];
		this._v[1] += v2._v[1];
		return this;
	},

	sub:function(v2) {
		this._v[0] -= v2._v[0];
		this._v[1] -= v2._v[1];
		return this;
	},

	dot:function(v2) {
		return this._v[0] * v2._v[0] + this._v[1] * v2._v[1];
	},

	scale:function(scale) {
		this._v[0] *= scale; this._v[1] *= scale;
		return this;
	},

	// Operators: the following functions are operators in C++ version
	//	operator[]()
	elemAtIndex:function(i) { return this._v[i]; },
	// operator==()
	equals:function(v) { return this._v[0]==v._v[0] && this._v[1]==v._v[1]; },
	// operator!=()
	notEquals:function(v) { return this._v[0]!=v._v[0] || this._v[1]!=v._v[1]; },
	// operator=()
	assign:function(v) { this._v[0]=v._v[0]; this._v[1]=v._v[1]; },

	set:function(x, y) {
		if (y===undefined) { this._v[0] = x; this._v[1] = x; }
		else { this._v[0] = x; this._v[1] = y; }
	},

	x:function(v) { if (v!==undefined) this._v[0] = v; return this._v[0]; },
	y:function(v) { if (v!==undefined) this._v[1] = v; return this._v[1]; },

	width:function(v) { return this.x(v); },
	height:function(v) { return this.y(v); },

	aspectRatio:function() { return this._v[0]/this._v[1]; },

	isNan:function() { return isNaN(this._v[0]) || isNaN(this._v[1]); },

	toString:function() {
		return "[" + this._v + "]";
	}
});

Class ("vwgl.Vector3",{
	_v:null,

	initialize:function(a,b,c) {
		if (typeof(a)=='object' && a.length>=3) {
			this._v = [a[0],a[1],a[2]];
		}
		else if (typeof(a)=='object' && a.length==2) {
			this._v = [a[0],a[1],0.0];
		}
		else if (typeof(a)=='object' && a._v!==undefined) {
			if (a._v.length==2) this._v = [a._v[0],a._v[1],0];
			else if (a._v.length>=3) this._v = [a._v[0],a._v[1],a._v[2]];
		}
		else {
			this._v = [0,0,0];
			if (a!==undefined) {this._v[0] = a; this._v[1] = a; this._v[2] = a;}
			if (b!==undefined) this._v[1] = b;
			if (c!==undefined) this._v[2] = c;
		}
	},

	raw:function() { return this._v; },

	// In c++ API this function is called "length", in Javascript has been renamed to "size"
	// to distinguish it from array.length function
	size:function() { return this._v.length; },

	magnitude:function() { return vwgl.Math.sqrt(this._v[0] * this._v[0] + this._v[1] * this._v[1] + this._v[2] * this._v[2]); },

	normalize:function() {
		var m = this.magnitude();
		this._v[0] = this._v[0]/m; this._v[1]=this._v[1]/m; this._v[2]=this._v[2]/m;
		return this;
	},

	distance:function(other) {
		var v3 = new vwgl.Vector3(this._v[0] - other._v[0],
															this._v[1] - other._v[1],
															this._v[2] - other._v[2]);
		return v3.magnitude();
	},

	add:function(v2) {
		this._v[0] += v2._v[0];
		this._v[1] += v2._v[1];
		this._v[2] += v2._v[2];
		return this;
	},

	sub:function(v2) {
		this._v[0] -= v2._v[0];
		this._v[1] -= v2._v[1];
		this._v[2] -= v2._v[2];
		return this;
	},

	dot:function(v2) {
		return this._v[0] * v2._v[0] + this._v[1] * v2._v[1] + this._v[2] * v2._v[2];
	},

	scale:function(scale) {
		this._v[0] *= scale; this._v[1] *= scale; this._v[2] *= scale;
		return this;
	},

	cross:function(/* Vector3 */ v2) {
		var x = this._v[1] * v2._v[2] - this._v[2] * v2._v[1];
		var y = this._v[2] * v2._v[0] - this._v[0] * v2._v[2];
		var z = this._v[0] * v2._v[1] - this._v[1] * v2._v[0];
		this._v[0]=x; this._v[1]=y; this._v[2]=z;
		return this;
	},

	// Operators: the following functions are operators in C++ version
	//	operator[]()
	elemAtIndex:function(i) { return this._v[i]; },
	// operator==()
	equals:function(v) { return this._v[0]==v._v[0] && this._v[1]==v._v[1] && this._v[2]==v._v[2]; },
	// operator!=()
	notEquals:function(v) { return this._v[0]!=v._v[0] || this._v[1]!=v._v[1] || this._v[2]!=v._v[2]; },
	// operator=()
	assign:function(v) { this._v[0]=v._v[0]; this._v[1]=v._v[1]; if (v._v.length>=3) this._v[2]=v._v[2]; },

	set:function(x, y, z) {
		this._v[0] = x;
		this._v[1] = (y===undefined) ? x:y;
		this._v[2] = (y===undefined) ? x:(z===undefined ? y:z);
	},

	x:function(v) { if (v!==undefined) this._v[0] = v; return this._v[0]; },
	y:function(v) { if (v!==undefined) this._v[1] = v; return this._v[1]; },
	z:function(v) { if (v!==undefined) this._v[2] = v; return this._v[2]; },

	r:function(v) { return this.x(v); },
	g:function(v) { return this.y(v); },
	b:function(v) { return this.z(v); },

	width:function(v) { return this.x(v); },
	height:function(v) { return this.y(v); },
	depth:function(v) { return this.z(v); },

	xy:function() { return new vwgl.Vector2(this._v[0],this._v[1]); },
	yz:function() { return new vwgl.Vector2(this._v[1],this._v[2]); },
	xz:function() { return new vwgl.Vector2(this._v[0],this._v[2]); },

	isNan:function() { return isNaN(this._v[0]) || isNaN(this._v[1]) || isNaN(this._v[2]); },

	toString:function() {
		return "[" + this._v + "]";
	}
});

Class ("vwgl.Vector4", {
	_v:null,

	initialize:function(a,b,c,d) {
		if (typeof(a)=='object' && a.length>=4) {
			this._v = [a[0],a[1],a[2],a[3]];
		}
		else if (typeof(a)=='object' && a.length==3) {
			this._v = [a[0],a[1],a[2],0.0];
		}
		else if (typeof(a)=='object' && a.length==2) {
			this._v = [a[0],a[1],0.0,0.0];
		}
		else if (typeof(a)=='object' && a._v!==undefined) {
			if (a._v.length==2) this._v = [a._v[0],a._v[1],0,0];
			else if (a._v.length==3) this._v = [a._v[0],a._v[1],a._v[2],0];
			else if (a._v.length>=4) this._v = [a._v[0],a._v[1],a._v[2],a._v[3]];
		}
		else {
			this._v = [0,0,0,0];
			if (a!==undefined) { this._v[0] = a; this._v[1] = a; this._v[2] = a; this._v[3] = a; }
			if (b!==undefined) this._v[1] = b;
			if (c!==undefined) this._v[2] = c;
			if (d!==undefined) this._v[3] = d;
		}
	},

	raw:function() { return this._v; },

	// In c++ API this function is called "length", in Javascript has been renamed to "size"
	// to distinguish it from array.length function
	size:function() { return this._v.length; },

	magnitude:function() { return vwgl.Math.sqrt(this._v[0] * this._v[0] + this._v[1] * this._v[1] + this._v[2] * this._v[2] + this._v[3] * this._v[3]); },

	normalize:function() {
		var m = this.magnitude();
		this._v[0] = this._v[0]/m; this._v[1]=this._v[1]/m; this._v[2]=this._v[2]/m; this._v[3]=this._v[3]/m;
		return this;
	},

	distance:function(other) {
		var v3 = new vwgl.Vector4(this._v[0] - other._v[0],
															this._v[1] - other._v[1],
															this._v[2] - other._v[2],
															this._v[3] - other._v[3]);
		return v3.magnitude();
	},

	add:function(v2) {
		this._v[0] += v2._v[0];
		this._v[1] += v2._v[1];
		this._v[2] += v2._v[2];
		this._v[3] += v2._v[3];
		return this;
	},

	sub:function(v2) {
		this._v[0] -= v2._v[0];
		this._v[1] -= v2._v[1];
		this._v[2] -= v2._v[2];
		this._v[3] -= v2._v[3];
		return this;
	},

	dot:function(v2) {
		return this._v[0] * v2._v[0] + this._v[1] * v2._v[1] + this._v[2] * v2._v[2] + this._v[3] * v2._v[3];
	},

	scale:function(scale) {
		this._v[0] *= scale; this._v[1] *= scale; this._v[2] *= scale; this._v[3] *= scale;
		return this;
	},

	// Operators: the following functions are operators in C++ version
	//	operator[]()
	elemAtIndex:function(i) { return this._v[i]; },
	// operator==()
	equals:function(v) { return this._v[0]==v._v[0] && this._v[1]==v._v[1] && this._v[2]==v._v[2] && this._v[3]==v._v[3]; },
	// operator!=()
	notEquals:function(v) { return this._v[0]!=v._v[0] || this._v[1]!=v._v[1] || this._v[2]!=v._v[2] || this._v[3]!=v._v[3]; },
	// operator=()
	assign:function(v) { this._v[0]=v._v[0]; this._v[1]=v._v[1]; if (v._v.length>=3) this._v[2]=v._v[2]; if (v._v.length==4) this._v[3]=v._v[3]; },

	set:function(x, y, z, w) {
		this._v[0] = x;
		this._v[1] = (y===undefined) ? x:y;
		this._v[2] = (y===undefined) ? x:(z===undefined ? y:z);
		this._v[3] = (y===undefined) ? x:(z===undefined ? y:(w===undefined ? z:w));
	},

	x:function(v) { if (v!==undefined) this._v[0] = v; return this._v[0]; },
	y:function(v) { if (v!==undefined) this._v[1] = v; return this._v[1]; },
	z:function(v) { if (v!==undefined) this._v[2] = v; return this._v[2]; },
	w:function(v) { if (v!==undefined) this._v[3] = v; return this._v[3]; },

	r:function(v) { return this.x(v); },
	g:function(v) { return this.y(v); },
	b:function(v) { return this.z(v); },
	a:function(v) { return this.w(v); },

	xy:function() { return new vwgl.Vector2(this._v[0],this._v[1]); },
	yz:function() { return new vwgl.Vector2(this._v[1],this._v[2]); },
	xz:function() { return new vwgl.Vector2(this._v[0],this._v[2]); },
	xyz:function() { return new vwgl.Vector3(this); },

	width:function(v) { return this.z(v); },
	height:function(v) { return this.w(v); },

	isNan:function() { return isNaN(this._v[0]) || isNaN(this._v[1]) || isNaN(this._v[2]) || isNaN(this._v[3]); },

	aspectRatio:function() { return this._v[3]!=0 ? this._v[2]/this._v[3]:1.0; },

	toString:function() {
		return "[" + this._v + "]";
	}
});

vwgl.Vector4.yellow = function() { return new vwgl.Vector4(1.0,1.0,0.0,1.0); }
vwgl.Vector4.orange = function() { return new vwgl.Vector4(1.0,0.5,0.0,1.0); }
vwgl.Vector4.red = function() { return new vwgl.Vector4(1.0,0.0,0.0,1.0); }
vwgl.Vector4.violet = function() { return new vwgl.Vector4(0.5,0.0,1.0,1.0); }
vwgl.Vector4.blue = function() { return new vwgl.Vector4(0.0,0.0,1.0,1.0); }
vwgl.Vector4.green = function() { return new vwgl.Vector4(0.0,1.0,0.0,1.0); }
vwgl.Vector4.white = function() { return new vwgl.Vector4(1.0,1.0,1.0,1.0); }
vwgl.Vector4.lightGray = function() { return new vwgl.Vector4(0.8,0.8,0.8,1.0); }
vwgl.Vector4.gray = function() { return new vwgl.Vector4(0.5,0.5,0.5,1.0); }
vwgl.Vector4.darkGray = function() { return new vwgl.Vector4(0.2,0.2,0.2,1.0); }
vwgl.Vector4.black = function() { return new vwgl.Vector4(0.0,0.0,0.0,1.0); }
vwgl.Vector4.brown = function() { return new vwgl.Vector4(0.4,0.2,0.0,1.0); }
vwgl.Vector4.transparent = function() { return new vwgl.Vector4(0.0); }


vwgl.Size2D = vwgl.Vector2;
vwgl.Size2D.prototype["vwgl.Size2D"] = true;
vwgl.Size3D = vwgl.Vector3;
vwgl.Size3D.prototype["vwgl.Size3D"] = true;
vwgl.Position2D = vwgl.Vector2;
vwgl.Position2D.prototype["vwgl.Position2D"] = true;
vwgl.Viewport = vwgl.Vector4;
vwgl.Viewport.prototype["vwgl.Viewport"] = true;
vwgl.Color = vwgl.Vector4;
vwgl.Color.prototype["vwgl.Color"] = true;

Class ("vwgl.Bounds", {
	_v:null,

	initialize:function(a,b,c,d,e,f) {
		if (typeof(a)=='object' && a.length>=6) {
			this._v = [a[0],a[1],a[2],a[3],a[4],a[5]];
		}
		else if (typeof(a)=='object' && a._v!==undefined && a._v.length==6) {
			this._v = [a._v[0],a._v[1],a._v[2],a._v[3],a._v[4],a._v[5]];
		}
		else {
			this._v = [-Number.MAX_VALUE,Number.MAX_VALUE,-Number.MAX_VALUE,Number.MAX_VALUE,-Number.MAX_VALUE,Number.MAX_VALUE];
			if (a!==undefined) { this._v[0] = a; this._v[1] = a; this._v[2] = a; this._v[3] = a; }
			if (b!==undefined) this._v[1] = b;
			if (c!==undefined) this._v[2] = c;
			if (d!==undefined) this._v[3] = d;
			if (e!==undefined) this._v[4] = e;
			if (f!==undefined) this._v[5] = f;
		}
	},

	raw:function() { return this._v; },

	// In c++ API this function is called "length", in Javascript has been renamed to "size"
	// to distinguish it from array.length function
	size:function() { return this._v.length; },

	// Operators: the following functions are operators in C++ version
	//	operator[]()
	elemAtIndex:function(i) { return this._v[i]; },
	// operator==()
	equals:function(v) { return this._v[0]==v._v[0] && this._v[1]==v._v[1] && this._v[2]==v._v[2] && this._v[3]==v._v[3] && this._v[4]==v._v[4] && this._v[5]==v._v[5]; },
	// operator!=()
	notEquals:function(v) { return this._v[0]!=v._v[0] || this._v[1]!=v._v[1] || this._v[2]!=v._v[2] || this._v[3]!=v._v[3] || this._v[4]!=v._v[4] || this._v[5]!=v._v[5]; },
	// operator=()
	assign:function(v) { this._v[0]=v._v[0]; this._v[1]=v._v[1]; this._v[2]=v._v[2]; this._v[3]=v._v[3]; this._v[4]=v._v[4]; this._v[5]=v._v[5]; },

	set:function(left, right, bottom, top, back, front) {
		this._v[0] = left;
		this._v[1] = (right===undefined) ? left:right;
		this._v[2] = (right===undefined) ? left:bottom;
		this._v[3] = (right===undefined) ? left:top;
		this._v[4] = (right===undefined) ? left:back;
		this._v[5] = (right===undefined) ? left:front;
	},

	left:function(v) { if (v!==undefined) this._v[0] = v; return this._v[0]; },
	right:function(v) { if (v!==undefined) this._v[1] = v; return this._v[1]; },
	bottom:function(v) { if (v!==undefined) this._v[2] = v; return this._v[2]; },
	top:function(v) { if (v!==undefined) this._v[3] = v; return this._v[3]; },
	back:function(v) { if (v!==undefined) this._v[4] = v; return this._v[4]; },
	front:function(v) { if (v!==undefined) this._v[5] = v; return this._v[5]; },

	width:function() { return Math.abs(this._v[1] - this._v[0]); },
	height:function() { return Math.abs(this._v[3] - this._v[2]); },
	depth:function() { return Math.abs(this._v[5] - this._v[4]); },

	isNan:function() { return isNaN(this._v[0]) || isNaN(this._v[1]) || isNaN(this._v[2]) || isNaN(this._v[3]) || isNaN(this._v[4]) || isNaN(this._v[5]); },

	toString:function() {
		return "[" + this._v + "]";
	},

	isInBounds:function(/* vwgl.Vector3*/ v) {
		return v.x()>=this._v[0] && v.x()<=this._v[1] &&
				v.y()>=this._v[2] && v.y()<=this._v[3] &&
				v.z()>=this._v[4] && v.z()<=this._v[5];
	}
});
