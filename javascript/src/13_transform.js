
Class ("vwgl.Transform", {
	_matrix: null,				// vwgl.Matrix4
	_transformStrategy:null,	// vwgl.TransformStrategy

	initialize:function(matrix) {
		this._matrix = vwgl.Matrix4.makeIdentity();
		if (dynamic_cast("vwgl.Matrix4",matrix)) {
			this._matrix.assign(matrix);
		}
	},

	setMatrix:function(matrix) { this._matrix.assign(matrix); },
	getMatrix:function() { return this._matrix; },

	getTransformStrategy:function() { return this._transformStrategy; },
	setTransformStrategy:function(strategy) {
		this._transformStrategy = strategy;
		if (this._transformStrategy) {
			this._transformStrategy.setTransformable(this);
			this._transformStrategy.updateTransform();
		}
	},

	assign:function(transform) {
		this._matrix.assign(transform._matrix);
		this._transformStrategy = transform._transformStrategy;
		return this;
	}
});
