
var jsglut = {}
var gl = null;

Class("jsglut.Application",{
	initGL:function() {
	},

	display:function() {
	},

	reshape:function(width,height) {
	},

	idle:function() {
	},

	keyboard:function(key,x,y) {
	},

	keyboardUp:function(key, x, y) {
	},

	special:function(key, x, y) {
	},

	specialUp:function(key, x, y) {
	},

	mouse:function(button, state, x, y) {
	},

	motion:function(x, y) {
	},

	motionPassive:function(x, y) {
	},

	mouseWheel:function(delta, x, y) {

	},

	entry:function(state) {
	},

	destroy:function() {
	},

	postRedisplay:function() {
		jsglut.MainLoop.singleton().postRedisplay();
	},

	postReshape:function() {
		jsglut.MainLoop.singleton().postReshape();
	}
});

Class("jsglut.Canvas",{
	domElem:null,
	width:0,
	height:0,
	gl:null,
	resizeMode:null,
	scale:null,

	initialize:function(canvasNodeOrId) {
		if (typeof(canvasNodeOrId)=='string') {
			canvasNodeOrId = document.getElementById(canvasNodeOrId);
		}
		this.domElem = canvasNodeOrId;
		this.domElem.style.MozUserSelect = 'none';
		this.domElem.style.WebkitUserSelect = 'none';
		this.domElem.setAttribute("onselectstart", "return false");
		this.resizeMode = jsglut.Canvas.kResizeModeFitToWindow;
		this.scale = jsglut.Canvas.kScaleModeEqual;
		if (!this._initGlContext()) {
			throw new Error("Sorry, your browser does not support WebGL. Try another browser or check if you can enable WebGL compatibility in yours.");
		}
	},

	screenshot:function(format, width, height) {
		var canvasStyle = "";
		var prevSize = {}
		if (width) {
			height = height ? height:width;
			canvasStyle = this.domElem.style.cssText;
			prevSize.width = this.domElem.width;
			prevSize.height = this.domElem.height;

			this.domElem.style.cssText = "top:auto;left:auto;bottom:auto;right:auto;width:" + width + "px;height:" + height + "px;";
			this.domElem.width = width;
			this.domElem.height = height;
			jsglut.MainLoop.singleton().application.reshape(width,height);
			jsglut.MainLoop.singleton().application.postRedisplay();
			jsglut.MainLoop.singleton().application.display();
		}
		var data = this.domElem.toDataURL(format);
		if (width) {
			this.domElem.style.cssText = canvasStyle;
			this.domElem.width = prevSize.width;
			this.domElem.height = prevSize.height;
			jsglut.MainLoop.singleton().application.reshape(prevSize.width,prevSize.height);
			jsglut.MainLoop.singleton().application.postRedisplay();
			jsglut.MainLoop.singleton().application.display();
		}
		return data;
	},

	setScaleMode:function(mode) {
		this.scale = mode;
	},

	setResizeMode:function(mode) {
		this.resizeMode = mode;
	},

	_initGlContext:function() {
		this.gl = vwgl.WebGL.initWebGLContext(this.domElem);
		if (this.gl) {
			gl = this.gl;
			this._initStyle();
			return true;
		}
		return false;
	},

	_initStyle:function() {
		if (this.resizeMode==jsglut.Canvas.kResizeModeFitToWindow) {
			this.domElem.style["position"] = 'fixed';
			this.domElem.style["top"] = '0px';
			this.domElem.style["left"] = '0px';
			this.domElem.style["right"] = '0px';
			this.domElem.style["bottom"] = '0px';
		}
	},

	_onResize:function(width,height) {
		if (this.resizeMode==jsglut.Canvas.kResizeModeFitToWindow) {
			this.width = width / this.scale;
			this.height = height / this.scale;
			this.domElem.width = this.width;
			this.domElem.height = this.height;
		}
	}
});
jsglut.Canvas.kScaleModeQuarter = 4;
jsglut.Canvas.kScaleModeHalf = 2;
jsglut.Canvas.kScaleModeEqual = 1;
jsglut.Canvas.kScaleModeDouble = 0.5;
jsglut.Canvas.kResizeModeFitToWindow = 1;
jsglut.Canvas.kResizeModeUnchanged;

window.requestAnimFrame=(function() { return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(callback,lement){window.setTimeout(callback,1000/60);};})();
jsglut._animationLoopFunction = function() {
	requestAnimFrame(jsglut._animationLoopFunction);
	jsglut.MainLoop.singleton()._onUpdate();
}

Class("jsglut.MainLoop",{
	canvas:null,
	application:null,
	updateMode:0,
	redisplay:null,

	initialize:function() {

	},

	setCanvas:function(canvas) {
		this.canvas = canvas;
	},

	run:function(application) {
		this.application = application;
		this.redisplay = true;
		this.application.initGL();
		this._initEvents();
		jsglut._animationLoopFunction();
	},

	postRedisplay:function() {
		this.redisplay = true;
	},

	postReshape:function() {
		var fakeEvent = {}
		this._onResize(fakeEvent);
	},

	_initEvents:function() {
		var This = this;
		this._onResize();
		window.addEventListener("resize",function(event) { This._onResize(); });
		window.addEventListener("click",function(event) { This._onClick(event); });

		this.canvas.domElem.addEventListener("mousedown",function(event) { This._onMouseDown(event); event.preventDefault(); return false;});
		this.canvas.domElem.addEventListener("mousemove",function(event) { This._onMouseMove(event); event.preventDefault(); return false;});
		this.canvas.domElem.addEventListener("mouseout",function(event) { This._onMouseOut(event); event.preventDefault(); return false;});
		this.canvas.domElem.addEventListener("mouseover",function(event) { This._onMouseOver(event); event.preventDefault(); return false;});
		this.canvas.domElem.addEventListener("mouseup",function(event) { This._onMouseUp(event); event.preventDefault(); return false;});

		this.canvas.domElem.addEventListener("touchstart",function(event) { This._onMouseDown(event); event.preventDefault(); return false; });
		this.canvas.domElem.addEventListener("touchmove",function(event) { This._onMouseMove(event); event.preventDefault(); return false; });
		this.canvas.domElem.addEventListener("touchend",function(event) { This._onMouseUp(event); event.preventDefault(); return false; });

		var mousewheelevt = (/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel"
		this.canvas.domElem.addEventListener(mousewheelevt,function(event) { This._onMouseWheel(event); });

		window.addEventListener("keydown",function(event) { This._onKeyDown(event); });
		window.addEventListener("keyup",function(event) { This._onKeyUp(event); });

		this.canvas.domElem.oncontextmenu = function(e) { return false; }
	},

	_onResize:function(event) {
		this.canvas._onResize(window.innerWidth,window.innerHeight);
		var cW = this.canvas.width;
		var cH = this.canvas.height;
		this.application.reshape(cW,cH);
	},

	_onUpdate:function() {
		this.application.idle();
		if (this.redisplay) {
			this.application.display();
			this.redisplay = false;
		}
	},

	_onClick:function(event) {

	},

	_onMouseDown:function(event) {
		var button = -1;
		if (event.button!==undefined) {
			button = event.button;
			switch (event.button) {
			case 0:
				this._mouseStatus.left = true;
				break;
			case 1:
				this._mouseStatus.middle = true;
				break;
			case 2:
				this._mouseStatus.right = true;
				break;
			}
			this._mouseStatus.x = event.clientX;
			this._mouseStatus.y = event.clientY;
		}
		else if (event.touches) {
			if (event.touches.length==1) {
				this._mouseStatus.left = true;
				this._mouseStatus.right = false;
				this._mouseStatus.middle = false;
				button = 0;
			}
			else if (event.touches.length==2) {
				this._mouseStatus.left = false;
				this._mouseStatus.right = true;
				this._mouseStatus.middle = false;
				button = 1;
			}
			else if (event.touches.length==3) {
				this._mouseStatus.left = false;
				this._mouseStatus.right = false;
				this._mouseStatus.middle = true;
				button = 2;
			}

			if (event.touches.length>0) {
				this._mouseStatus.x = event.touches.item(0).clientX;
				this._mouseStatus.y = event.touches.item(0).clientY;
			}
		}

		this.application.mouse(button,jsglut.Mouse.kDownState,this._mouseStatus.x,this._mouseStatus.y);
	},

	_onMouseMove:function(event) {
		if (event.touches && event.touches.length>0) {
			this._mouseStatus.x = event.touches.item(0).clientX;
			this._mouseStatus.y = event.touches.item(0).clientY;
		}
		else {
			this._mouseStatus.x = event.clientX;
			this._mouseStatus.y = event.clientY;
		}
		if (this._mouseStatus.left || this._mouseStatus.middle || this._mouseStatus.right) {
			this.application.motion(this._mouseStatus.x,this._mouseStatus.y);
		}
		else {
			this.application.motionPassive(this._mouseStatus.x,this._mouseStatus.y);
		}
	},

	_onMouseOut:function(event) {
		var x = this._mouseStatus.x;
		var y = this._mouseStatus.y;
		if (this._mouseStatus.left) {
			this.application.mouse(jsglut.Mouse.kLeftButton,jsglut.Mouse.kUpState,x,y);
			this._mouseStatus.left = false;
		}
		if (this._mouseStatus.middle) {
			this.application.mouse(jsglut.Mouse.kMiddleButton,jsglut.Mouse.kUpState,x,y);
			this._mouseStatus.middle = false;
		}
		if (this._mouseStatus.right) {
			this.application.mouse(jsglut.Mouse.kRightButton,jsglut.Mouse.kUpState,x,y);
			this._mouseStatus.right = false;
		}
		this.application.entry(jsglut.Mouse.kMouseLeave);
	},

	_onMouseOver:function(event) {
		this.application.entry(jsglut.Mouse.kMouseEntry);
	},

	_onMouseUp:function(event) {
		var button = -1;
		if (event.button!==undefined) {
			button = event.button;
			switch (event.button) {
			case 0:
				this._mouseStatus.left = false;
				break;
			case 1:
				this._mouseStatus.middle = false;
				break;
			case 2:
				this._mouseStatus.right = false;
				break;
			}
			this._mouseStatus.x = event.clientX;
			this._mouseStatus.y = event.clientY;
		}
		else if (event.touches) {
			button = event.touches.length-1;
			this._mouseStatus.left = false;
			this._mouseStatus.right = false;
			this._mouseStatus.middle = false;

			if (event.touches.length>0) {
				this._mouseStatus.x = event.touches.item(0).clientX;
				this._mouseStatus.y = event.touches.item(0).clientY;
			}
		}

		this.application.mouse(button,jsglut.Mouse.kUpState,this._mouseStatus.x,this._mouseStatus.y);
	},

	_onMouseWheel:function(event) {
		var delta = event.wheelDelta ? event.wheelDelta*-1:event.detail;
		this.application.mouseWheel(delta,this._mouseStatus.x,this._mouseStatus.y);
	},

	_onKeyDown:function(event) {
		if (jsglut.Keyboard.isSpecialKey(event.keyCode)) {
			this.application.special(event.keyCode,this._mouseStatus.x,this._mouseStatus.y);
		}
		else {
			this.application.keyboard(String.fromCharCode(event.keyCode),this._mouseStatus.x,this._mouseStatus.y);
		}
	},

	_onKeyUp:function(event) {
		if (jsglut.Keyboard.isSpecialKey(event.keyCode)) {
			this.application.specialUp(event.keyCode,this._mouseStatus.x,this._mouseStatus.y);
		}
		else {
			this.application.keyboardUp(String.fromCharCode(event.keyCode),this._mouseStatus.x,this._mouseStatus.y);
		}
	},

	_mouseStatus:{
		left:false,
		middle:false,
		right:false,

		x:0,
		y:0
	}
});

jsglut.Mouse = {
	kLeftButton:0,
	kMiddleButton:1,
	kRightButton:2,

	kUpState:0,
	kDownState:1,

	kMouseEntry:1,
	kMouseLeave:0
}

jsglut.Keyboard = {
	specialKeys:{
		kBackspace:8,
		kTab:9,
		kEnter:13,
		kShift:16,
		kCtrl:17,
		kAlt:18,
		kPause:19,
		kCapsLock:20,
		kEscape:27,
		kPageUp:33,
		kPageDown:34,
		kEnd:35,
		kHome:36,
		kLeftArrow:37,
		kUpArrow:38,
		kRightArrow:39,
		kDownArrow:40,
		kInsert:45,
		kDelete:46
	},

	isSpecialKey:function(code) {
		return	code==jsglut.Keyboard.specialKeys.kBackspace ||
				code==jsglut.Keyboard.specialKeys.kTab ||
				code==jsglut.Keyboard.specialKeys.kEnter ||
				code==jsglut.Keyboard.specialKeys.kShift ||
				code==jsglut.Keyboard.specialKeys.kCtrl ||
				code==jsglut.Keyboard.specialKeys.kAlt ||
				code==jsglut.Keyboard.specialKeys.kPause ||
				code==jsglut.Keyboard.specialKeys.kCapsLock ||
				code==jsglut.Keyboard.specialKeys.kEscape ||
				code==jsglut.Keyboard.specialKeys.kPageUp ||
				code==jsglut.Keyboard.specialKeys.kPageDown ||
				code==jsglut.Keyboard.specialKeys.kEnd ||
				code==jsglut.Keyboard.specialKeys.kHome ||
				code==jsglut.Keyboard.specialKeys.kLeftArrow ||
				code==jsglut.Keyboard.specialKeys.kUpArrow ||
				code==jsglut.Keyboard.specialKeys.kRightArrow ||
				code==jsglut.Keyboard.specialKeys.kDownArrow ||
				code==jsglut.Keyboard.specialKeys.kInsert ||
				code==jsglut.Keyboard.specialKeys.kDelete;
	}
}

jsglut.MainLoop.s_singleton = null;
jsglut.MainLoop.singleton = function() {
	if (jsglut.MainLoop.s_singleton==null) {
		jsglut.MainLoop.s_singleton = new jsglut.MainLoop();
	}
	return jsglut.MainLoop.s_singleton;
}
