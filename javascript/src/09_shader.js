
Class ("vwgl.Shader",{

	_shaderProgram:null,
	_uniformLocations:null,
	_linked:false,
	
	getCompileError:function() { return this._compileError; },
	getLinkError:function() { return this._linkError; },
	
	initialize:function() {
		this._shaderProgram = gl.createProgram();
		this._uniformLocations = {};
	},
	
	destroy:function() {
		gl.deleteProgram(this._shaderProgram);
	},

	// in javascript API there is no loadAndAttachShader function. Use loadAndAttachShaders instead
	// loadAndAttachShader:function();
	// 		done callback: function(shader);
	loadAndAttachShaders:function(vshArray,fshArray,done) {
		if (!this._linked) {
			var resources = vshArray.concat(fshArray);
			var This = this;
			var loader = new vwgl.Loader();
			loader.loadResourceList(resources,function(loaded,errors) {
				var status = true;
				for (var i=0;i<vshArray.length && status;++i) {
					status = This.attachShader(vwgl.Shader.kVertexShader,loader.getResource(vshArray[i]),vshArray[i]);
				}
				for (var i=0;i<fshArray.length && status;++i) {
					status = This.attachShader(vwgl.Shader.kFragmentShader,loader.getResource(fshArray[i]),fshArray[i]);
				}
				if (status) status = This.link();
				if (typeof(done)=='function') done(status ? This:null);
			});
		}
		else {
			if (typeof(done)=='function') done(true);
		}
	},

	attachShader:function(shaderType, shaderSource, debugContext) {
		this._compileError = "";
		if (!this._linked) {
			if (!debugContext) debugContext = "";
			shaderType = vwgl.Shader.getGLShaderType(shaderType);
			if (this._shaderProgram==null) {
				base.debug.log("ERROR - " + debugContext + ": Could not attach shader: shaderProgram is null");
				return false;
			}
			var shader = gl.createShader(shaderType);
			gl.shaderSource(shader, shaderSource);
			gl.compileShader(shader);
	
			if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
				this._compileError = gl.getShaderInfoLog(shader);
				base.debug.log("ERROR - " + debugContext + ": " + this._compileError);
				return false;
			}
	
			gl.attachShader(this._shaderProgram, shader);
			gl.deleteShader(shader);
			return true;
		}
		else {
			return true;
		}
	},
	
	link:function(debugContext) {
		this._linkError = "";
		if (!this._linked) {
			if (!debugContext) debugContext = "";
			if (this._shaderProgram==null) {
				base.debug.log("ERROR - " + debugContext + ": Could not link shader: shaderProgram is null");
				return false;
			}
			gl.linkProgram(this._shaderProgram);
			if (!gl.getProgramParameter(this._shaderProgram, gl.LINK_STATUS)) {
				this._linkError = gl.getProgramInfoLog(this._shaderProgram);
				base.debug.log("ERROR - " + debugContext + ": Shader link error: " + this._linkError);
				return false;
			}
			this._linked = true;
			return true;
		}
		else {
			return true;
		}
	},
	
	bind:function() {
		gl.useProgram(this._shaderProgram);
	},
	
	unbind:function() {
		vwgl.Shader.unbind();
	},

	getShaderProgram:function() { return this._shaderProgram; },
	
	getAttribLocation:function(name) { return gl.getAttribLocation(this._shaderProgram,name); },
	
	initUniformLocation:function(name) { this._uniformLocations[name] = gl.getUniformLocation(this._shaderProgram, name); },
	
	getUniformLocation:function(name) { return this._uniformLocations[name]; },

	// Use this function only for objects (Vector2, Vector3, Vector4, Quaternion, Matrix3, Matrix4, Texture and CubeMap)
	setUniform:function(name,obj,texUnit) {
		var v2 	= dynamic_cast("vwgl.Vector2",obj);
		var v3 	= dynamic_cast("vwgl.Vector3",obj);
		var v4 	= dynamic_cast("vwgl.Vector4",obj);
		var m3 	= dynamic_cast("vwgl.Matrix3",obj);
		var m4 	= dynamic_cast("vwgl.Matrix4",obj);
		var cm 	= dynamic_cast("vwgl.CubeMap",obj);
		var t 	= dynamic_cast("vwgl.Texture",obj);
		
		if (v2) {
			this.setUniform2fv(name,v2._v);
		}
		else if (v3) {
			this.setUniform3fv(name,v3._v);
		}
		else if (v4) {
			this.setUniform4fv(name,v4._v);
		}
		else if (m3) {
			this.setUniformMatrix3(name,false,m3._m);
		}
		else if (m4) {
			this.setUniformMatrix4(name,false,m4._m);
		}
		else if (cm) {
			texUnit = texUnit ? texUnit:vwgl.Texture.kTexture0;
			vwgl.Texture.setActiveTexture(texUnit);
			cm.bindTexture(vwgl.Texture.kTargetTextureCubeMap);
			this.setUniform1i(name,texUnit);
		}
		else if (t) {
			texUnit = texUnit ? texUnit:vwgl.Texture.kTexture0;
			vwgl.Texture.setActiveTexture(texUnit);
			t.bindTexture(vwgl.Texture.kTargetTexture2D);
			this.setUniform1i(name,texUnit);
		}
	},

	setUniform1f:function(name,a) { gl.uniform1f(this.getUniformLocation(name),a); },
	setUniform2f:function(name,a,b) { gl.uniform2f(this.getUniformLocation(name),a,b); },
	setUniform3f:function(name,a,b,c) { gl.uniform3f(this.getUniformLocation(name),a,b,c); },
	setUniform4f:function(name,a,b,c,d) { gl.uniform4f(this.getUniformLocation(name),a,b,c,d); },
	setUniform1i:function(name,a) { gl.uniform1i(this.getUniformLocation(name),a); },
	setUniform2i:function(name,a,b) { gl.uniform2i(this.getUniformLocation(name),a,b); },
	setUniform3i:function(name,a,b,c) { gl.uniform3i(this.getUniformLocation(name),a,b,c); },
	setUniform4i:function(name,a,b,c,d) { gl.uniform4i(this.getUniformLocation(name),a,b,c,d); },
	setUniform1fv:function(name,v) { gl.uniform1fv(this.getUniformLocation(name),v); },
	setUniform2fv:function(name,v) { gl.uniform2fv(this.getUniformLocation(name),v); },
	setUniform3fv:function(name,v) { gl.uniform3fv(this.getUniformLocation(name),v); },
	setUniform4fv:function(name,v) { gl.uniform4fv(this.getUniformLocation(name),v); },
	setUniform1iv:function(name,v) { gl.uniform1iv(this.getUniformLocation(name),v); },
	setUniform2iv:function(name,v) { gl.uniform2iv(this.getUniformLocation(name),v); },
	setUniform3iv:function(name,v) { gl.uniform3iv(this.getUniformLocation(name),v); },
	setUniform4iv:function(name,v) { gl.uniform4iv(this.getUniformLocation(name),v); },
	setUniformMatrix3:function(name,transpose,v) { gl.uniformMatrix3fv(this.getUniformLocation(name), transpose, v); },
	setUniformMatrix4:function(name,transpose,v) { gl.uniformMatrix4fv(this.getUniformLocation(name), transpose, v); },
});

vwgl.Shader.kTypeVertex = 1;
vwgl.Shader.kTypeFragment = 2;
vwgl.Shader.getGLShaderType = function(type) {
	if (type==vwgl.Shader.kTypeVertex) return gl.VERTEX_SHADER;
	else if (type==vwgl.Shader.kTypeFragment) return gl.FRAGMENT_SHADER;
}
vwgl.Shader.unbind = function() {
	gl.useProgram(null);
}

Class ("vwgl.ShaderLibrary",{
	_shaderMap:null,
	
	initialize:function() {
		this._shaderMap = {};
	},
	
	addShader:function(key,shader) {
		this._shaderMap[key] = shader;
	},
	
	getShader:function(key) {
		return this._shaderMap[key];
	}
});

vwgl.ShaderLibrary.s_singleton = null;
vwgl.ShaderLibrary.get = function() {
	if (vwgl.ShaderLibrary.s_singleton==null) {
		vwgl.ShaderLibrary.s_singleton = new vwgl.ShaderLibrary();
	}
	return vwgl.ShaderLibrary.s_singleton;
}
vwgl.ShaderLibrary.destroy = function() {
	vwgl.ShaderLibrary.s_singleton = null;
}
