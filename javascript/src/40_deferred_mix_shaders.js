

/////// postprocess shader
vwgl.deferred_shaders.postprocess_mix = {
	vertex:"\
	#ifdef GL_ES\n\
	precision highp float;\n\
	precision highp int;\n\
	#endif\n\
	attribute vec4 aVertexPos;\n\
	attribute vec2 aTexturePosition;\n\
	varying vec2 vTexturePosition;\n\
	void main() {\n\
		gl_Position = aVertexPos;\n\
		vTexturePosition = aTexturePosition;\n\
	}\n\
	",

	fragment:"\
	#ifdef GL_ES\n\
	precision highp float;\n\
	precision highp int;\n\
	#endif\n\
	varying vec2 vTexturePosition;\n\
	uniform sampler2D uDiffuseMap;\n\
	uniform sampler2D uPositionMap;\n\
	uniform sampler2D uSelectionMap;\n\
	uniform sampler2D uSSAOMap;\n\
	\n\
	uniform vec2 uSize;\n\
	uniform float uBlurKernel[9];\n\
	uniform float uSelectionKernel[9];\n\
	uniform float uTargetDistance;\n\
	uniform float uDOFAmount;\n\
	uniform vec4 uClearColor;\n\
	uniform int uSSAOBlur;\n\
	\n\
	uniform float uBrightness;\n\
	uniform float uContrast;\n\
	uniform float uHue;\n\
	uniform float uSaturation;\n\
	uniform float uLightness;\n\
	\n\
	// TODO: Pass this constant through an uniform\n\
	const float MAX_RADIUS = 25.0;\n\
	const int MAX_BLUR_ITERATIONS = 25;\n\
	\n\
	vec4 applyConvolution(sampler2D texture, vec2 texCoord, vec2 texSize, float convMatrix[9], float radius) {\n\
		vec2 onePixel = vec2(1.0, 1.0) / texSize * radius;\n\
		vec4 colorSum =\n\
		texture2D(texture, texCoord + onePixel * vec2(-1, -1)) * convMatrix[0] +\n\
		texture2D(texture, texCoord + onePixel * vec2( 0, -1)) * convMatrix[1] +\n\
		texture2D(texture, texCoord + onePixel * vec2( 1, -1)) * convMatrix[2] +\n\
		texture2D(texture, texCoord + onePixel * vec2(-1,  0)) * convMatrix[3] +\n\
		texture2D(texture, texCoord + onePixel * vec2( 0,  0)) * convMatrix[4] +\n\
		texture2D(texture, texCoord + onePixel * vec2( 1,  0)) * convMatrix[5] +\n\
		texture2D(texture, texCoord + onePixel * vec2(-1,  1)) * convMatrix[6] +\n\
		texture2D(texture, texCoord + onePixel * vec2( 0,  1)) * convMatrix[7] +\n\
		texture2D(texture, texCoord + onePixel * vec2( 1,  1)) * convMatrix[8] ;\n\
		float kernelWeight =\n\
		convMatrix[0] +\n\
		convMatrix[1] +\n\
		convMatrix[2] +\n\
		convMatrix[3] +\n\
		convMatrix[4] +\n\
		convMatrix[5] +\n\
		convMatrix[6] +\n\
		convMatrix[7] +\n\
		convMatrix[8] ;\n\
		\n\
		if (kernelWeight <= 0.0) {\n\
			kernelWeight = 1.0;\n\
		}\n\
		\n\
		return vec4((colorSum / kernelWeight).rgb, 1.0);\n\
	}\n\
	\n\
	vec4 blur(sampler2D textureInput, int size) {\n\
		vec2 texelSize = 1.0 / uSize;\n\
		vec3 result = vec3(0.0);\n\
		vec2 hlim = vec2(float(-size) * 0.5 + 0.5);\n\
		for (int x=0; x<MAX_BLUR_ITERATIONS; ++x) {\n\
			if (x==size) break;\n\
			for (int y=0; y<MAX_BLUR_ITERATIONS; ++y) {\n\
				if (y==size) break;\n\
				vec2 offset = (hlim + vec2(float(x), float(y))) * texelSize;\n\
				result += texture2D(textureInput, vTexturePosition + offset).rgb;\n\
			}\n\
		}\n\
		return vec4(result / float(size * size),1.0);\n\
	}\n\
	\n\
	vec4 blurDepth(sampler2D textureInput, int size, sampler2D posMap, float sourceDepth) {\n\
		vec2 texelSize = 1.0 / uSize;\n\
		vec3 result = vec3(0.0);\n\
		vec2 hlim = vec2(float(-size) * 0.5 + 0.5);\n\
		for (int x=0; x<MAX_BLUR_ITERATIONS; ++x) {\n\
			if (x==size) break;\n\
			for (int y=0; y<MAX_BLUR_ITERATIONS; ++y) {\n\
				if (y==size) break;\n\
				vec2 offset = (hlim + vec2(float(x), float(y))) * texelSize;\n\
				float targetDepth = texture2D(posMap, vTexturePosition + offset).z;\n\
				if (abs(sourceDepth-targetDepth)>2.0) {\n\
					result += texture2D(textureInput, vTexturePosition + offset).rgb;\n\
				}\n\
				else {\n\
					result += texture2D(textureInput, vTexturePosition).rgb;\n\
				}\n\
			}\n\
		}\n\
		return vec4(result / float(size * size),1.0);\n\
	}\n\
	\n\
	vec3 rgb2hsv(vec3 c) {\n\
		vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);\n\
		vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));\n\
		vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));\n\
	\n\
		float d = q.x - min(q.w, q.y);\n\
		float e = 1.0e-10;\n\
		return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);\n\
	}\n\
	\n\
	vec3 hsv2rgb(vec3 c) {\n\
		vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n\
		vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n\
		return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);\n\
	}\n\
	\n\
	vec4 applyBrightness(vec4 color, float brightness) {\n\
		return clamp(vec4(color.rgb + brightness - 0.5,1.0),0.0,1.0);\n\
	}\n\
	\n\
	vec4 applyContrast(vec4 color, float contrast) {\n\
		return clamp(vec4((color.rgb * max(contrast + 0.5,0.0)),1.0),0.0,1.0);\n\
	}\n\
	\n\
	vec4 applySaturation(vec4 color, float hue, float saturation, float lightness) {\n\
		vec3 fragRGB = clamp(color.rgb + vec3(0.001),0.0,1.0);\n\
		vec3 fragHSV = rgb2hsv(fragRGB);\n\
		lightness -= 0.01;\n\
		float h = hue;\n\
		fragHSV.x *= h;\n\
		fragHSV.yz *= vec2(saturation,lightness);\n\
		fragHSV.x = mod(fragHSV.x, 1.0);\n\
		fragHSV.y = mod(fragHSV.y, 1.0);\n\
		fragHSV.z = mod(fragHSV.z, 1.0);\n\
		fragRGB = hsv2rgb(fragHSV);\n\
		return clamp(vec4(hsv2rgb(fragHSV), color.w),0.0,1.0);\n\
	}\n\
	\n\
	vec4 postprocess(vec4 fragColor) {\n\
		return applyContrast(applyBrightness(applySaturation(fragColor,uHue,uSaturation,uLightness),uBrightness),uContrast);\n\
	}\n\
	\n\
	void main() {\n\
		vec4 diffuseColor = texture2D(uDiffuseMap,vTexturePosition);\n\
		if (diffuseColor.a==0.0) {\n\
			gl_FragColor = uClearColor;\n\
		}\n\
		else {\n\
			vec4 ssao = blur(uSSAOMap,uSSAOBlur);\n\
			if (uTargetDistance>0.0) {\n\
				vec3 pos = texture2D(uPositionMap,vTexturePosition).xyz;\n\
				float dist = distance(pos,vec3(0.0));\n\
				float radius = max(min((dist - uTargetDistance) * uDOFAmount,MAX_RADIUS),1.0);\n\
				diffuseColor = dist>=uTargetDistance ? blurDepth(uDiffuseMap,int(radius),uPositionMap,uTargetDistance):texture2D(uDiffuseMap,vTexturePosition);\n\
			}\n\
			else {\n\
				diffuseColor = texture2D(uDiffuseMap,vTexturePosition);\n\
			}\n\
			diffuseColor *= ssao;\n\
			diffuseColor.a = 1.0;\n\
		\n\
			vec4 finalColor = postprocess(diffuseColor);\n\
			vec4 selectionColor = applyConvolution(uSelectionMap,vTexturePosition,uSize,uSelectionKernel,2.0);\n\
			selectionColor = (selectionColor.r!=0.0 && selectionColor.g!=0.0 && selectionColor.b!=0.0) ? vec4(1.0):vec4(0.0);\n\
			gl_FragColor = clamp(finalColor + selectionColor,0.0,1.0);\n\
		}\n\
	}\n\
	"
}

