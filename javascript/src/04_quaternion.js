
Class ("vwgl.Quaternion", vwgl.Vector4, {
	initialize:function(a,b,c,d) {
		this.parent();
		if (a===undefined) this.zero();
		else if (b===undefined) {
			if (a._v && a._v.lenght>=4) this.clone(a);
			else if(a._m && a._m.length==9) this.initWithMatrix3(a);
			else if(a._m && a._m.length==16) this.initWithMatrix4(a);
			else this.zero();
		}
		else if (a!==undefined && b!==undefined && c!==undefined && d!==undefined) {
			this.initWithValues(a,b,c,d);
		}
		else {
			this.zero();
		}
	},
	
	initWithValues:function(alpha,x,y,z) {
		this._v[0] = x * vwgl.Math.sin(alpha/2);
		this._v[1] = y * vwgl.Math.sin(alpha/2);
		this._v[2] = z * vwgl.Math.sin(alpha/2);
		this._v[3] = vwgl.Math.cos(alpha/2);
		return this;
	},
	
	clone:function(q) {
		this._v[0] = q._v[0];
		this._v[1] = q._v[1];
		this._v[2] = q._v[2];
		this._v[3] = q._v[3];
	},
	
	initWithMatrix3:function(m) {
		var w = vwgl.Math.sqrtf(1.0 + m[0] + m[4] + m[8]) / 2.0;
		var w4 = 4.0 * w;
		
		this._v[0] = (m[7] - m[5]) / w;
		this._v[1] = (m[2] - m[6]) / w4;
		this._v[2] = (m[3] - m[1]) / w4;
		this._v[3] = w;
	},
	
	initWithMatrix4:function(m) {
		var w = vwgl.Math.sqrtf(1.0 + m[0] + m[5] + m[10]) / 2.0;
		var w4 = 4.0 * w;
		
		this._v[0] = (m[9] - m[6]) / w;
		this._v[1] = (m[2] - m[8]) / w4;
		this._v[2] = (m[4] - m[1]) / w4;
		this._v[3] = w;	
	},
	
	getMatrix4:function() {
		var m = vwgl.Matrix4.makeIdentity();
		var _v = this._v;
		m.setRow(0, new vwgl.Vector4(1.0 - 2.0*_v[1]*_v[1] - 2.0*_v[2]*_v[2], 2.0*_v[0]*_v[1] - 2.0*_v[2]*_v[3], 2.0*_v[0]*_v[2] + 2.0*_v[1]*_v[3], 0.0));
		m.setRow(1, new vwgl.Vector4(2.0*_v[0]*_v[1] + 2.0*_v[2]*_v[3], 1.0 - 2.0*_v[0]*_v[0] - 2.0*_v[2]*_v[2], 2.0*_v[1]*_v[2] - 2.0*_v[0]*_v[3], 0.0));
		m.setRow(2, new vwgl.Vector4(2.0*_v[0]*_v[2] - 2.0*_v[1]*_v[3], 2.0*_v[1]*_v[2] + 2.0*_v[0]*_v[3], 1.0 - 2.0*_v[0]*_v[0] - 2.0*_v[1]*_v[1], 0.0));
		return m;
	},
	
	getMatrix3:function() {
		var m = vwgl.Matrix3.makeIdentity();
		var _v = this._v;
		
		m.setRow(0, new vwgl.Vector3(1.0 - 2.0*_v[1]*_v[1] - 2.0*_v[2]*_v[2], 2.0*_v[0]*_v[1] - 2.0*_v[2]*_v[3], 2.0*_v[0]*_v[2] + 2.0*_v[1]*_v[3]));
		m.setRow(1, new vwgl.Vector3(2.0*_v[0]*_v[1] + 2.0*_v[2]*_v[3], 1.0 - 2.0*_v[0]*_v[0] - 2.0*_v[2]*_v[2], 2.0*_v[1]*_v[2] - 2.0*_v[0]*_v[3]));
		m.setRow(2, new vwgl.Vector3(2.0*_v[0]*_v[2] - 2.0*_v[1]*_v[3], 2.0*_v[1]*_v[2] + 2.0*_v[0]*_v[3], 1.0 - 2.0*_v[0]*_v[0] - 2.0*_v[1]*_v[1]));
		return m;
	}
});

vwgl.Quaternion.makeWithMatrix = function(m) {
	return new vwgl.Quaternion(m);
}

