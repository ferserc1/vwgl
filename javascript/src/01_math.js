vwgl.Math = {

	kPi:3.141592653589793,
	kPiOver180:0.01745329251994,
	k180OverPi:57.29577951308233,
	kPiOver2:1.5707963267948966,
	kPiOver4:0.785398163397448,
	k2Pi:6.283185307179586,
	
	degreesToRadians:function(d) {
		return this.checkZero(d * this.kPiOver180);
	},
	
	radiansToDegrees:function(r) {
		return this.checkZero(r * this.k180OverPi);
	},
	
	checkZero:function(v) {
		return v>-1e-12 && v<1e-12 ? 0:v;
	},

	sin:function(val) {
		return this.checkZero(Math.sin(val));
	},

	cos:function(val) {
		return this.checkZero(Math.cos(val));
	},

	tan:function(val) {
		return this.checkZero(Math.tan(val));
	},

	cotan:function(val) {
		return this.checkZero(1.0 / this.tan(val));
	},

	atan:function(val) {
		return this.checkZero(Math.atan(val));
	},
	
	atan2:function(i, j) {
		return this.checkZero(Math.atan2f(i, j));
	},

	random:function() {
		return Math.random();
	},
	
	max:function(a,b) {
		return Math.max(a,b);
	},
	
	min:function(a,b) {
		return Math.min(a,b);
	},
	
	abs:function(val) {
		return Math.abs(val);
	},
	
	sqrt:function(val) {
		return Math.sqrt(val);
	},
	
	lerp:function(from, to, t) {
		return (1.0 - t) * from + t * to;
	}
}