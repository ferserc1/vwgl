
Class ("vwgl.Gizmo", {
	_drawable:null,		// vwgl.Drawable

	getResourceList:function() { return []; },

	initialize:function(resourcePath,onSuccess) {
		var loader = new vwgl.Loader();
		var resource = vwgl.Loader.getResource(resourcePath);
		if (resource) {
			this._init(loader,resourcePath);
			if (typeof(onSuccess)=='function') onSuccess(this);
		}
		else {
			var This = this;
			loader.loadResource(resourcePath,function(res) {
				This._init(loader,resourcePath);
				if (typeof(onSuccess)=='function') onSuccess(This);
			})
		}
	},
	
	getDrawable:function() { return dynamic_cast("vwgl.Drawable",this._drawable); },
	
	checkItemPicked:function(/* vwgl.Solid */ solid) { return false; },
	beginMouseEvents:function(/* vwgl.Position2D */ pos, /* vwgl.Camera */ camera, /* vwgl.Viewport */ vp, /* vwgl.Vector3 */ gizmoPosition, /* vwgl.TransformNode */ trx) {},
	mouseEvent:function(/* vwgl.Position2D */ pos, /* vwgl.Camera */ camera, /* vwgl.Vector3 */ gizmoPosition) {},
	endMouseEvents:function() {},
	
	_init:function(loader,resource) {
		this._drawable = loader.loadDrawable(resource);
		if (this._drawable) {
			// Assign pick id's
			var colorPicker = new vwgl.ColorPicker();
			colorPicker.assignPickId(this._drawable);
			this._loadPickIds();
		}
	},
	
	_loadPickIds:function() {
		// implement this function to save the drawable's pick id's
	},

	getScale:function(/* Vector3 */ selectionSize) {
		return vwgl.Math.max(selectionSize.x(),selectionSize.z()) * 2.0;
	},

	getOffset:function(/* Vector3 */ selectionSize) {
		return nvwgl.Vector3(0.0, 0.0, 0.0);
	}
});

vwgl.Gizmo.getResourceList = function(gizmoClass) {
	if (gizmoClass.length) {
		var resources = [];
		for (var i=0;i<gizmoClass.length;++i) {
			resources = resources.concat(gizmoClass.prototype.getResourceList());
		}
	}
	else {
		return gizmoClass.prototype.getResourceList();
	}
}

Class ("vwgl.PlaneGizmo", vwgl.Gizmo, {
	_moveId:null,
	_rotateId:null,
	_rotateFineId:null,
	
	_mode:null,
	_camera:null,
	_viewport:null,
	_plane:null,
	_lastPickPoint:null,
	_translateOffset:null,
	_transformNode:null,
	
	getResourceList:function() { return ["manipulator.vwglb","manipulator.jpg","manipulator_lm.jpg"]; },

	initialize:function(onSuccess,customDrawable) {
		customDrawable = customDrawable || 'manipulator.vwglb';
		this.parent(customDrawable,onSuccess);
	},
	
	getMode:function(/* PickId */ pickedId) {
		if (pickedId.equals(this._moveId)) {
			return vwgl.PlaneGizmo.kGizmoMove;
		}
		else if (pickedId.equals(this._rotateId)) {
			return vwgl.PlaneGizmo.kGizmoRotate;
		}
		else if (pickedId.equals(this._rotateFineId)) {
			return vwgl.PlaneGizmo.kGizmoRotateFine;
		}
		return vwgl.PlaneGizmo.kGizmoNone;
	},

	checkItemPicked:function(/* vwgl.Solid */ solid) {
		this._mode = this.getMode(solid.getPickId());
		return this._mode!=vwgl.PlaneGizmo.kGizmoNone;
	},

	beginMouseEvents:function(/* vwgl.Position2D */ pos, /* vwgl.Camera */ camera, /* vwgl.Viewport */ vp, /* vwgl.Vector3 */ gizmoPosition, /* vwgl.TransformNode */ trx) {
		this._camera = camera;
		this._viewport = vp;
		this._transformNode = trx;
		
		var ray = vwgl.physics.Ray.rayWithScreenPoint(pos, this._camera.getProjectionMatrix(), this._camera.getViewMatrix(), this._viewport);
		var plane = new vwgl.physics.Plane();
		plane.setOrigin(gizmoPosition);
		plane.setNormal(new vwgl.Vector3(0,1,0));
		this._plane = plane;
		var intersection = vwgl.physics.Intersection.rayToPlane(ray,plane);
		
		if (intersection.intersects()) {
			this._lastPickPoint = intersection.getRay().getEnd();
			var transform = this._transformNode.getTransform();
			this._translateOffset = new vwgl.Vector3(transform.getPosition());
			this._translateOffset.sub(intersection.getRay().getEnd());
		}
	},

	rotationBetweenPoints:function(p1,p2,origin,inc) {
		if (!inc) inc = 0;
		var axis = new vwgl.Vector3(0,1,0);
		var v1 = new vwgl.Vector3(p2);
		v1.sub(origin).normalize();
		var v2 = new vwgl.Vector3(p1);
		v2.sub(origin).normalize();
		var dot = v1.dot(v2);
	
		var alpha = Math.acos(dot);
		if (alpha>=inc || inc==0) {
			if (inc!=0) {
				alpha = (alpha>=2*inc) ? 2*inc:inc;
			}
			var sign = axis.dot(v1.cross(v2));
			if (sign<0) alpha *= -1.0;
			var q = new vwgl.Quaternion(alpha,axis.x(),axis.y(),axis.z());
			q.normalize();
	
			if (!isNaN(q.x())) {
				return q;
			}
		}
		return new vwgl.Quaternion(0,0,1,0);
	},

	mouseEvent:function(/* vwgl.Position2D */ pos) {
		var ray = vwgl.physics.Ray.rayWithScreenPoint(pos, this._camera.getProjectionMatrix(), this._camera.getViewMatrix(), this._viewport);
		var intersection = vwgl.physics.Intersection.rayToPlane(ray, this._plane);
		
		if (intersection.intersects()) {
			var transform = new vwgl.Matrix4(this._transformNode.getTransform());
			var rotation = transform.getRotation();
			var origin = transform.getPosition();
			var intersectPoint = intersection.getPoint();
			
			if (this._mode==vwgl.PlaneGizmo.kGizmoMove) {
				var y = origin.y();
				transform = vwgl.Matrix4.makeTranslation(intersectPoint.x() + this._translateOffset.x(),
														y,
														intersectPoint.z() + this._translateOffset.z());
				transform.mult(rotation);
				this._lastPickPoint = intersectPoint;
			}
			else if (this._mode==vwgl.PlaneGizmo.kGizmoRotate) {
				rotation = this.rotationBetweenPoints(this._lastPickPoint, intersectPoint, origin, vwgl.Math.degreesToRadians(22.5));
				if (rotation.x()!=0 || rotation.y()!=0 || rotation.z()!=0 || rotation.w()!=1) {
					transform.mult(rotation.getMatrix4());
					this._lastPickPoint = intersectPoint;
				}
			}
			else if (this._mode==vwgl.PlaneGizmo.kGizmoRotateFine) {
				rotation = this.rotationBetweenPoints(this._lastPickPoint, intersectPoint, origin);
				if (rotation.x()!=0 || rotation.y()!=0 || rotation.z()!=0 || rotation.w()!=1) {
					transform.mult(rotation.getMatrix4());
					this._lastPickPoint = intersectPoint;
				}
			}
			
			this._transformNode.setTransform(transform);
		}
	},

	endMouseEvents:function() {
	},
	
	_loadPickIds:function() {
		var movePlistName = "Material-translate";
		var rotatePlistName = "Material-rotate";
		var rotateFinePlistName = "Material-rotate_fine";
		
		var move = this._drawable.getSolid(movePlistName);
		var rotate = this._drawable.getSolid(rotatePlistName);
		var rotateFine = this._drawable.getSolid(rotateFinePlistName);
		
		this._moveId = move.getPickId();
		this._rotateId = rotate.getPickId();
		this._rotateFineId = rotateFine.getPickId();
	}
});

vwgl.PlaneGizmo.kGizmoNone 		= 0;
vwgl.PlaneGizmo.kGizmoMove 		= 1;
vwgl.PlaneGizmo.kGizmoRotate 	= 2;
vwgl.PlaneGizmo.kGizmoRotateFine	= 3;

Class ("vwgl.GizmoManager", {
	_currentTransform:null,	// vwgl.TransformNode
	_currentGizmo:null,		// vwgl.Gizmo
	_boundingBox:null,
	
	_gizmoNode:null,
	_gizmoTrx:null,
	
	initialize:function() {
		this._boundingBox = new vwgl.BoundingBox();
	},

	enableGizmo:function(/* vwgl.TransformNode */ trx, /* vwgl.Gizmo */ gizmo) {
		this.disableGizmo();
		if (dynamic_cast("vwgl.TransformNode",trx) &&
			dynamic_cast("vwgl.Gizmo",gizmo) &&
			gizmo.getDrawable()) {
			this._currentGizmo = gizmo;
			this._currentTransform = trx;
			this._boundingBox.setVertexData(gizmo.getDrawable());

			var size = this._boundingBox.size();
			var scale = this._currentGizmo.getScale(size);
			var offset = this._currentGizmo.getOffset(size);
			this._gizmoTrx = new vwgl.TransformNode(vwgl.Matrix4.makeScale(scale,scale,scale).translate(offset));
			//this._gizmoTrx = new vwgl.TransformNode(vwgl.Matrix4.makeTranslation(offset).scale(scale,scale,scale));
			this._gizmoNode = new vwgl.DrawableNode(gizmo.getDrawable());
			this._gizmoTrx.addChild(this._gizmoNode);
			trx.addChild(this._gizmoTrx);
		}
	},
	
	disableGizmo:function() {
		if (this._currentGizmo && this._currentTransform) {
			this._currentTransform.removeChild(this._gizmoTrx);
			this._gizmoTrx.removeChild(this._gizmoNode);
			this._gizmoNode.setDrawable(null);
			this._currentTransform = null;
			this._currentGizmo = null;
			this._gizmoNode = null;
			this._gizmoTrx = null;
		}
	},
	
	// Checks if the picked drawable is the current gizmo
	checkItemPicked:function(/* vwgl.Solid */ solid) {
		return this._currentGizmo && dynamic_cast("vwgl.Solid",solid) && this._currentGizmo.checkItemPicked(solid);
	},
	
	beginMouseEvents:function(/* vwgl.Position2D */ point, /* vwgl.Camera */ camera, /* vwgl.Viewport */ vp) {
		var pos = this._boundingBox.center();
		if (this._currentGizmo) this._currentGizmo.beginMouseEvents(point,camera,vp,pos,this._currentTransform);
	},
	
	mouseEvent:function(/* vwgl.Position2D */ point) {
		var pos = this._boundingBox.center();
		if (this._currentGizmo) this._currentGizmo.mouseEvent(point,pos);
	},
	
	endMouseEvents:function() {
		if (this._currentGizmo) this._currentGizmo.endMouseEvents();
	}
});

vwgl.GizmoManager.s_singleton = null;
vwgl.GizmoManager.get = function() {
	if (vwgl.GizmoManager.s_singleton==null) {
		vwgl.GizmoManager.s_singleton = new vwgl.GizmoManager();
	}
	return vwgl.GizmoManager.s_singleton;
}
