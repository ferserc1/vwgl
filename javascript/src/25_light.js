
Class ("vwgl.Light", vwgl.Node, {
	_lightType:0,	// LightType
	_ambient:null,	// vwgl.Vector4
	_diffuse:null,	// vwgl.Vector4
	_specular:null,	// vwgl.Vector4
	_intensity:0,
	_constantAtt:0,
	_linearAtt:0,
	_expAtt:0,
	_spotExponent:0,	// NEW
	_spotCutoff:0,	// NEW
	_shadowStrength:1.0,
	_cutoffDistance:-1.0,

	_enabled:true,

	_transformVisitor:null,
	_fooTransform:null,

	initialize:function() {
		this._lightType = vwgl.Light.kTypeDirectional;
		this._intensity = 1.0;
		this._constantAtt = 1.0;
		this._linearAtt = 0.5;
		this._expAtt = 0.1;
		this._enabled = true;
		this._spotExponent = 30.0;	// NEW
		this._spotCutoff = 20.0;		// NEW

		vwgl.LightManager.get()._addLight(this);
		this._ambient = new vwgl.Vector4(0.2,0.2,0.2,1.0);
		this._diffuse = new vwgl.Vector4(0.9,0.9,0.9,1.0);
		this._specular = new vwgl.Vector4(1.0,1.0,1.0,1.0);
		this._transformVisitor = new vwgl.TransformVisitor();
	},

	destroy:function() {
		vwgl.LightManager.get()._removeLight(this);
		this.parent();
	},

	isEnabled:function() { return this._enabled; },
	setEnabled:function(e) { this._enabled = e; },

	getType:function()  { return this._lightType; },
	getPosition:function() {
		if (this._lightType==vwgl.Light.kTypeDirectional) return this.getDirection();
		this._transformVisitor.visit(this);
		var tr = this._transformVisitor.getTransform();
		return tr.getPosition();
	},

	getDirection:function() {
		var look = new vwgl.Vector3(0.0,0.0,1.0);
		var cam = vwgl.CameraManager.get().getMainCamera();
		this._transformVisitor.visit(this);
		var tr = new vwgl.Matrix4(this._transformVisitor.getTransform());
		tr.setRow(3, new vwgl.Vector4(0.0,0.0,0.0,1.0));
		if (cam) {
			var vm = new vwgl.Matrix4(cam.getViewMatrix());
			vm.setRow(3, new vwgl.Vector4(0.0,0.0,0.0,1.0));
			vm.mult(tr);
			look = vm.multVector(look).xyz();
		}
		else {
			look = tr.multVector(look).xyz();
		}
		look.normalize();
		return look;
	},

	// NEW:
	getSpotDirection:function() {
		return this.getDirection();
	},

	getAmbient:function() { return this._ambient; },
	getDiffuse:function() { return this._diffuse; },
	getSpecular:function() { return this._specular; },
	getIntensity:function()  { return this._intensity; },
	getConstantAttenuation:function() { return this._constantAtt; },
	getLinearAttenuation:function()  { return this._linearAtt; },
	getExpAttenuation:function()  { return this._expAtt; },
	getSpotCutoff:function() { return this._spotCutoff; },			// NEW
	getSpotExponent:function() { return this._spotExponent; },		// NEW
	getSpotAngle:function() { return this._spotCutoff; },				// alias to spot cutoff
	getShadowStrength:function() { return this._shadowStrength; },
	getCutoffDistance:function() { return this._cutoffDistance; },

	setType:function(/* LightType */ type) { this._lightType = type; },
	setAmbient:function(/* Vector4 */ ambient) { this._ambient = ambient; },
	setDiffuse:function(/* Vector4 */ diffuse) { this._diffuse = diffuse; },
	setSpecular:function(/* Vector4 */ specular) { this._specular = specular; },
	setIntensity:function(i) { this._intensity = i; },
	setConstantAttenuation:function(att) { this._constantAtt = att; },
	setLinearAttenuation:function(att) { this._linearAtt = att; },
	setExpAttenuation:function(att) { this._expAtt = att; },
	setSpotCutoff:function(value) { this._spotCutoff = value; },			// NEW
	setSpotExponent:function(value) { this._spotExponent = value; },		// NEW
	setSpotAngle:function(value) { this._spotCutoff = value; },			// alias to spot cutoff
	setShadowStrength:function(s) { this._shadowStrength = s; },
	setCutoffDistance:function(d) { this._cutoffDistance = d; },

	getTransform:function() {
		var trNode = dynamic_cast("vwgl.TransformNode",this.getParent());
		if (trNode) {
			return trNode.getTransform();
		}
		return this._fooTransform;
	}
});

vwgl.Light.kTypeDirectional = 4;
vwgl.Light.kTypeSpot = 1;
vwgl.Light.kTypePoint = 5;
vwgl.Light.kTypeDisabled = 10;

Class ("vwgl.LightManager", {
	_lightList:null,	// vwgl.Light []

	_typeArray:null,
	_positionArray:null,
	_directionArray:null,
	_ambientArray:null,
	_diffuseArray:null,
	_specularArray:null,
	_properties:null,	// r: intensity, g: linearAttenuation b: expAttenuation, a: enabled
	_numberOfLights:0,

	_renderPass:null,		// vwgl.RenderPass
	_shadowMapSize:null,	// vwgl.Size2D
	
	
	initialize:function() {
		this._lightList = [];

		this._typeArray = null;
		this._positionArray = null;
		this._directionArray = null;
		this._ambientArray = null;
		this._diffuseArray = null;
		this._specularArray = null;
		this._properties = null;
		this._numberOfLights = 0;
		this._shadowMapSize = new vwgl.Size2D(512);
	},

	getLightList:function() { return this._lightList; },

	prepareFrame:function(noShadows) {
		if (noShadows===undefined) noShadows = false;
		this._prepareUniforms();
	},

	getLightSourcesCount:function() { return vwgl.Math.min(this._numberOfLights,vwgl.LightManager.s_maxLightSources); },
	getTypeUniformArray:function() { return this._typeArray; },
	getPositionUniformArray:function() { return this._positionArray; },
	getDirectionUniformArray:function() { return this._directionArray; },
	getAmbientUniformArray:function() { return this._ambientArray; },
	getDiffuseUniformArray:function() { return this._diffuseArray; },
	getSpecularUniformArray:function() { return this._specularArray; },
	getPropertiesUniformArray:function() { return this._properties; },

	updateShadowMap:function(/* Group */ sceneRoot, /* ShadowLight */ light) {
		if (!this._renderPass) this._createShadowMap();
		if (this._renderPass && light.getCastShadows()) {
			this._renderPass.setSceneRoot(sceneRoot);
			vwgl.State.get().pushProjectionMatrix();
			vwgl.State.get().pushViewMatrix();
			vwgl.State.get().projectionMatrix().assign(light.getProjectionMatrix());
			vwgl.State.get().viewMatrix().assign(light.getViewMatrix());
			this._renderPass.updateTexture();
			vwgl.State.get().popProjectionMatrix();
			vwgl.State.get().popViewMatrix();
		}
	},
	
	setShadowMapSize:function(/* Size2D */ size) {
		this._shadowMapSize.assign(size); this._createShadowMap();
	},
	getShadowMapSize:function() { return this._shadowMapSize; },
	getShadowTexture:function() {
		var result = null;
		if (this._renderPass && this._renderPass.getFbo()) {
			result = this._renderPass.getFbo().getTexture();
		}
		return result;
	},

	_prepareUniforms:function() {
		if (this._numberOfLights!=this._lightList.length) {
			this._allocUniforms();
		}

		for (var i=0;i<this._lightList.length;++i) {
			var l = this._lightList[i];
			this._ambientArray[i*4] = l.getAmbient().r();
			this._ambientArray[i*4+1] = l.getAmbient().g();
			this._ambientArray[i*4+2] = l.getAmbient().b();
			this._ambientArray[i*4+3] = l.getAmbient().a();

			this._diffuseArray[i*4] = l.getDiffuse().r();
			this._diffuseArray[i*4+1] = l.getDiffuse().g();
			this._diffuseArray[i*4+2] = l.getDiffuse().b();
			this._diffuseArray[i*4+3] = l.getDiffuse().a();

			this._specularArray[i*4] = l.getSpecular().r();
			this._specularArray[i*4+1] = l.getSpecular().g();
			this._specularArray[i*4+2] = l.getSpecular().b();
			this._specularArray[i*4+3] = l.getSpecular().a();

			this._typeArray[i] = l.isEnabled() ? l.getType():vwgl.Light.kTypeDisabled;

			var pos = l.getPosition();
			this._positionArray[i*3] = pos.x();
			this._positionArray[i*3+1] = pos.y();
			this._positionArray[i*3+2] = pos.z();

			var dir = l.getDirection();
			this._directionArray[i*3] = dir.x();
			this._directionArray[i*3+1] = dir.y();
			this._directionArray[i*3+2] = dir.z();

			this._properties[i*4] = l.getIntensity();
			this._properties[i*4+1] = l.getLinearAttenuation();
			this._properties[i*4+2] = l.getExpAttenuation();
			this._properties[i*4+3] = 0.0;
		}
	},

	_addLight:function(/* Light */ light) {
		this._lightList.push(light);
	},

	_removeLight:function(/* Light */ light) {
		var i = this._lightList.indexOf(light);
		if (i>-1) this._lightList.splice(i,1);
	},

	_allocUniforms:function() {
		this._destroyUniforms();
		this._numberOfLights = this._lightList.length;
		if (this._numberOfLights>0) {
			this._typeArray = [];
			this._positionArray = [];
			this._directionArray = [];
			this._ambientArray = [];
			this._diffuseArray = [];
			this._specularArray = [];
			this._properties = [];
		}
	},

	_destroyUniforms:function() {
		this._typeArray = null;
		this._positionArray = null;
		this._directionArray = null;
		this._ambientArray = null;
		this._diffuseArray = null;
		this._specularArray = null;
		this._numberOfLights = null;
		this._properties = null;
	},
	
	_createShadowMap:function() {
		if (this._renderPass) {
			this._renderPass.getFbo().resize(this._shadowMapSize);
		}
		else {
			var renderPassMat = new vwgl.ShadowMapMaterial();
			this._renderPass = new vwgl.RenderPass();
			this._renderPass.create(renderPassMat, new vwgl.FramebufferObject(this._shadowMapSize));
			this._renderPass.setClearColor(vwgl.Color.black());
		}
	},

	_destroyShadowMap:function() {
		if (this._renderPass) {
			this._renderPass.destroy();
			this._renderPass = null;
		}
	}
});

vwgl.LightManager.s_singleton = null;
vwgl.LightManager.s_maxLightSources = 3;
vwgl.LightManager.get = function() {
	if (!vwgl.LightManager.s_singleton) {
		vwgl.LightManager.s_singleton = new vwgl.LightManager();
	}
	return vwgl.LightManager.s_singleton;
}

Class ("vwgl.ShadowLight", vwgl.Light, {
	_projection:null,		// vwgl.Matrix4
	_transformVisitor:null,	// vwgl.TransformVisitor
	_castShadows:true,
	_shadowBias:0.00005,

	initialize:function() {
		this._projection = vwgl.Matrix4.makeOrtho(-1,1,-1,1,0.1,100);
		this.parent();
	},

	destroy:function() {
		this._transformVisitor = null;
		this.parent();
	},

	getProjectionMatrix:function() { return this._projection; },
	getViewMatrix:function() {
		this._transformVisitor.visit(this);
		this._transformVisitor.getTransform().invert();
		return this._transformVisitor.getTransform();
	},

	getRenderPass:function() { return this._renderPass; },

	setCastShadows:function(/* bool */ cs) {
		this._castShadows = cs;
	},

	getCastShadows:function() {
		return this._castShadows;
	},
	
	setShadowBias:function(/* float */ bias) { this._shadowBias = bias; },
	getShadowBias:function() { return this._shadowBias; },

	_applyTransform:function() {
		vwgl.State.get().projectionMatrix().assign(this.getProjectionMatrix());
		vwgl.State.get().viewMatrix().assign(this.getViewMatrix());
	}
});

Class ("vwgl.ShadowMapMaterial", vwgl.Material, {
	// This material have built-in shaders, so it only have a synchronous load
	initialize:function(name) {
		this.parent(name);
	},

	initShader:function() {
		if (this._initialized) return;
		this._initialized = true;

		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.ShadowMapMaterial._vshader);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.ShadowMapMaterial._fshader);
		this.getShader().link("color_pick_material");

		this.loadVertexAttrib("aVertexPosition");
		this.loadTexCoord0Attrib("aTexturePosition");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uCastShadow");
		
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uAlphaCutoff");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");
	},

	setupUniforms:function() {
		var mat = this._settingsMaterial;
		if (mat) {
			this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
			this.getShader().setUniform("uPMatrix", this.projectionMatrix());
			this.getShader().setUniform1i("uCastShadow", mat.getCastShadows());
			
			var tex = mat.getTexture() ? mat.getTexture():vwgl.TextureManager.get().whiteTexture();
			this.getShader().setUniform("uTexture", tex, vwgl.Texture.kTexture0);
			this.getShader().setUniform("uTextureOffset", mat.getTextureOffset());
			this.getShader().setUniform("uTextureScale", mat.getTextureScale());
			this.getShader().setUniform1f("uAlphaCutoff", mat.getAlphaCutoff());
		}
		else {
			this.getShader().setUniform1i("uCastShadows",false);
		}
	},

	useSettingsOf:function(/* Material */ mat) { this._settingsMaterial = dynamic_cast("vwgl.GenericMaterial",mat); }
});

vwgl.ShadowMapMaterial._vshader = "\
		#ifdef GL_ES\n\
		precision highp float;\n\
		#endif\n\
		attribute vec3 aVertexPosition;\n\
		attribute vec2 aTexturePosition;\n\
		\n\
		uniform mat4 uMVMatrix;\n\
		uniform mat4 uPMatrix;\n\
		\n\
		varying vec4 vPosition;\n\
		varying vec2 vTexturePosition;\n\
		\n\
		void main() {\n\
			vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
			gl_Position = vPosition;\n\
			vTexturePosition = aTexturePosition;\n\
		}\
		";

vwgl.ShadowMapMaterial._fshader = "\
		#ifdef GL_ES\n\
		precision highp float;\n\
		#endif\n\
		\n\
		uniform bool uCastShadow;\n\
		\n\
		varying vec4 vPosition;\n\
		\n\
		varying vec2 vTexturePosition;\n\
		\n\
		uniform sampler2D uTexture;\n\
		uniform vec2 uTextureOffset;\n\
		uniform vec2 uTextureScale;\n\
		uniform float uAlphaCutoff;\n\
		\n\
		vec4 pack (float depth)\n\
		{\n\
			const vec4 bitSh = vec4(256 * 256 * 256,\n\
									256 * 256,\n\
									256,\n\
									1.0);\n\
			const vec4 bitMsk = vec4(0,\n\
									 1.0 / 256.0,\n\
									 1.0 / 256.0,\n\
									 1.0 / 256.0);\n\
			vec4 comp = fract(depth * bitSh);\n\
			comp -= comp.xxyz * bitMsk;\n\
			return comp;\n\
		}\n\
		\n\
		void main(void) {\n\
			float alpha = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset).a;\n\
			if (uCastShadow && alpha>uAlphaCutoff) {\n\
				gl_FragColor = pack(gl_FragCoord.z);\n\
			}\n\
			else {\n\
				discard;\n\
			}\n\
		}\
		";
