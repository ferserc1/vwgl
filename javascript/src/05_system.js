//La clase System será un clon de la de C++,
//salvo por el echo de que los directorios resources y shaders podrán modificarse en tiempo de ejecución

//Aquí irá también la información del UserAgent para saber en qué plataforma estamos o si es compatible con WebGL

Class ("vwgl.System", {
	_defaultShaderPath:null,
	_resourcesPath:null,

	initialize:function() {
		this._defaultShaderPath = 'shaders/';
		this._resourcesPath = 'resources/';
	},
	
	hasGLSupport:function() {
		return vwgl.WebGL.glSupport();
	},
	
	isWebGLExperimental:function() {
		return vwgl.WebGL.isExperimental();
	},

	isMac:function() {
		return base.userAgent.system.MacOS;
	},

	isWindows:function() {
		return base.userAgent.system.Windows;
	},

	isIOS:function() {
		return base.userAgent.system.iOS;
	},
	
	isIPad:function() {
		return base.userAgent.system.iPad;
	},
	
	isIPhone:function() {
		return base.userAgent.system.iPhone;
	},
	
	isAndroid:function() {
		return base.userAgent.system.Android;
	},

	getDefaultShaderPath:function() {
		return this._defaultShaderPath;
	},

	getResourcesPath:function() {
		return this._resourcesPath;
	},
	
	setDefaultShaderPath:function(p) {
		this._defaultShaderPath = p;
		if (p.length>0 && p[p.length-1]!='/') this._defaultShaderPath += '/';
	},
	
	setResourcesPath:function(p) {
		this._resourcesPath = p;
		if (p.length>0 && p[p.length-1]!='/') this._resourcesPath += '/';
	},

	isBigEndian:function() {
		var arr32 = new Uint32Array(1);
		var arr8 = new Uint8Array(arr32.buffer);
		arr32[0] = 255;
		return arr32[3]==255;
	},

	isLittleEndian:function() {
		var arr32 = new Uint32Array(1);
		var arr8 = new Uint8Array(arr32.buffer);
		arr32[0] = 255;
		return arr32[0]==255;
	}
});

vwgl.System.s_singleton = null;
vwgl.System.get = function() {
	if (vwgl.System.s_singleton==null) {
		vwgl.System.s_singleton = new vwgl.System();
	}
	return vwgl.System.s_singleton;
}

