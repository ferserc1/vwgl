
Class ("vwgl.VwglbLoader", vwgl.LoaderPlugin, {
	initialize:function() {
		vwgl.Loader.addBinaryExtension("vwglb","Native VitaminewGL binary model file");
	},

	acceptFileType:function(path) {
		return path.split(".").pop()=="vwglb";
	},
	
	loadDrawable:function(data) {
		this._jointData = null;
		this._clearWarningMessages();
		try {
			var parsedData = this._parseData(data);
		}
		catch (e) {
			this._addWarningMessage(e)
		}
		if (parsedData) return this._createDrawable(parsedData);
		else return null;
	},
	
	_parseData:function(data) {
		var loader = new vwgl.Loader();		// It will be used to load model textures

		var projector = null;
		var polyLists = [];
		var materials = null;
		
		var thisClass = this;

		var offset = 0;
		var header = new Uint8Array(data,0,8);
		offset = 8;
		var hdr = String.fromCharCode(header[4]) + String.fromCharCode(header[5]) + String.fromCharCode(header[6]) + String.fromCharCode(header[7]);
		
		if (header[0]==1) throw "Could not open the model file. This file has been saved as computer (little endian) format, try again saving it in network (big endian) format";
		if (hdr!='hedr') throw "File format error. Expecting header";
		
		var version = {maj:header[1],min:header[2],rev:header[3]};
		base.debug.log("vwglb file version: " + version.maj + "." + version.min + "." + version.rev + ", big endian");
		
		var numberOfPolyList = this._readInt(data,offset);
		offset += 4;
		
		var mtrl = this._readBlock(data,offset);
		offset += 4;
		if (mtrl!='mtrl') throw "File format error. Expecting materials definition";
		
		var matResult = this._readString(data,offset);
		offset += matResult.offset;
		materials = JSON.parse(matResult.data);
		
		var proj = this._readBlock(data,offset);
		if (proj=='proj') {
			offset += 4;
			
			var shadowTexFile = this._readString(data,offset);
			offset += shadowTexFile.offset;
			
			var attenuation = this._readFloat(data,offset);
			offset +=4;
			
			var projectionMatData = this._readMatrix4(data,offset);
			offset += projectionMatData.offset;
			var projMatrix = projectionMatData.data;
			
			var transformMatData = this._readMatrix4(data,offset);
			offset += transformMatData.offset;
			var transMatrix = transformMatData.data;
			
			projector = new vwgl.Projector();
			projector.setProjection(new vwgl.Matrix4(projMatrix));
			projector.setTransform(new vwgl.Matrix4(transMatrix));
			projector.setTexture(loader.loadTexture(shadowTexFile.data))
		}
		
		var join = this._readBlock(data,offset);
		if (join=='join') {
			offset += 4;
			
			var jointData = this._readString(data,offset);
			offset += jointData.offset;
			
			var jointText = jointData.data;
			try {
				this._jointData = JSON.parse(jointText);
			}
			catch (e) {
				base.debug.log("VWGLB file format reader: Error parsing joint data");
			}
		}
		
		var block = this._readBlock(data,offset);
		if (block!='plst') throw "File format error. Expecting poly list";
		var done = false;
		offset += 4;
		var plistName;
		var matName;
		var vArray;
		var nArray;
		var t0Array;
		var t1Array;
		var t2Array;
		var iArray;
		while (!done) {
			block = this._readBlock(data,offset);
			offset += 4;
			switch (block) {
			case 'pnam':
				var strData = this._readString(data,offset);
				offset += strData.offset;
				plistName = strData.data;
				break;
			case 'mnam':
				var strData = this._readString(data,offset);
				offset += strData.offset;
				matName = strData.data;
				break;
			case 'varr':
				var varr = this._readFloatArray(data,offset);
				offset += varr.offset;
				vArray = varr.data;
				break;
			case 'narr':
				var narr = this._readFloatArray(data,offset);
				offset += narr.offset;
				nArray = narr.data;
				break;
			case 't0ar':
				var tarr = this._readFloatArray(data,offset);
				offset += tarr.offset;
				t0Array = tarr.data;
				break;
			case 't1ar':
				var tarr = this._readFloatArray(data,offset);
				offset += tarr.offset;
				t1Array = tarr.data;
				break;
			case 't2ar':
				var tarr = this._readFloatArray(data,offset);
				offset += tarr.offset;
				t2Array = tarr.data;
				break;
			case 'indx':
				var iarr = this._readIndexArray(data,offset);
				offset += iarr.offset;
				iArray = iarr.data;
				break;
			case 'plst':
			case 'endf':
				var plistData = {
					name:plistName,
					matName:matName,
					vertices:vArray,
					normal:nArray,
					texcoord0:t0Array,
					texcoord1:t1Array,
					texcoord2:t2Array,
					indices:iArray
				}
				polyLists.push(plistData)
				plistName = "";
				matName = "";
				vArray = null;
				nArray = null;
				t0Array = null;
				t1Array = null;
				t2Array = null;
				iArray = null;
				break;
			default:
				throw "File format exception. Unexpected poly list member found";
			}
			done = block=='endf';
		}

		var parsedData =  {
			polyList:polyLists,
			materials:materials,
			projector:projector
		}
		return parsedData;
	},
	
	_createDrawable:function(data) {
		var drawable = new vwgl.Drawable();

		if (data.projector) {
			drawable.setShadowProjector(data.projector);
		}

		for (var i=0; i<data.polyList.length; ++i) {
			var plist = data.polyList[i];
			var polyList = new vwgl.PolyList();
			var solid = new vwgl.Solid(polyList);
			drawable.addSolid(solid);
			
			polyList.setName(plist.name);
			polyList.setMaterialName(plist.matName);
			polyList.addVertexVector(plist.vertices);
			if (plist.normal) polyList.addNormalVector(plist.normal);
			if (plist.texcoord0) polyList.addTexCoord0Vector(plist.texcoord0);
			if (plist.texcoord1) polyList.addTexCoord1Vector(plist.texcoord1);
			if (plist.texcoord2) polyList.addTexCoord2Vector(plist.texcoord2);
			polyList.addIndexVector(plist.indices);
			
			polyList.buildPolyList();
		}
		
		vwgl.GenericMaterial.setMaterialsWithJson(drawable,data.materials);
		this._applySolidSettings(drawable,data.materials);
		
		if (this._jointData) {
			if (this._jointData.input) {
				base.debug.log("Adding input joint");
				var joint = null;
				var jData = this._jointData.input;
				if (jData.type=="LinkJoint") {
					joint = new vwgl.physics.LinkJoint();
					joint.setPitch(jData.pitch);
					joint.setRoll(jData.roll);
					joint.setYaw(jData.yaw);
					joint.setOffset(new vwgl.Vector3(jData.offset[0],
													jData.offset[1],
													jData.offset[2]));
				}
				if (joint) {
					drawable.setInputJoint(joint);
				}
			}
			if (this._jointData.output && this._jointData.output.length>0) {
				base.debug.log("Adding output joint");
				var joint = null;
				var jData = this._jointData.output[0];
				if (jData.type=="LinkJoint") {
					joint = new vwgl.physics.LinkJoint();
					joint.setPitch(jData.pitch);
					joint.setRoll(jData.roll);
					joint.setYaw(jData.yaw);
					joint.setOffset(new vwgl.Vector3(jData.offset[0],
													jData.offset[1],
													jData.offset[2]));
				}
				if (joint) {
					drawable.setOutputJoint(joint);
				}
				// TODO: Support for multiple input joint
			}
		}
		
		return drawable;
	},
	
	_readBlock:function(arrayBuffer,offset) {
		var block = new Uint8Array(arrayBuffer,offset,4);
		block = String.fromCharCode(block[0]) + String.fromCharCode(block[1]) + String.fromCharCode(block[2]) + String.fromCharCode(block[3]);
		return block;
	},

	_readInt:function(arrayBuffer,offset) {
		var dataView = new DataView(arrayBuffer,offset,4);
		return dataView.getInt32(0);
	},
	
	_readFloat:function(arrayBuffer,offset) {
		var dataView = new DataView(arrayBuffer,offset,4);
		return dataView.getFloat32(0);
	},
	
	_readMatrix4:function(arrayBuffer,offset) {
		var response = {offset:0,data:[]}
		var size = 16;
		var dataView = new DataView(arrayBuffer,offset, size*4);
		var littleEndian = false;
		for (var i=0;i<size;++i) {
			response.data[i] = dataView.getFloat32(i*4,littleEndian);
		}
		response.offset += size * 4;
		return response;
	},

	_readString:function(arrayBuffer,offset) {
		var response = {offset:0,data:""}
		var size = this._readInt(arrayBuffer,offset);
		response.offset += 4;
		var strBuffer = new Uint8Array(arrayBuffer, offset + 4, size);
		for (var i=0;i<size;++i) {
			response.data += String.fromCharCode(strBuffer[i]);
		}
		response.offset += size;
		return response;
	},
	
	_readFloatArray:function(arrayBuffer,offset) {
		var response = {offset:0,data:[]}
		var size = this._readInt(arrayBuffer,offset);
		response.offset += 4;
		var dataView = new DataView(arrayBuffer,offset + 4, size*4);
		var littleEndian = false;
		for (var i=0;i<size;++i) {
			response.data[i] = dataView.getFloat32(i*4,littleEndian);
		}
		response.offset += size * 4;
		return response;
	},
	
	_readIndexArray:function(arrayBuffer,offset) {
		var response = {offset:0,data:[]}
		var size = this._readInt(arrayBuffer,offset);
		response.offset += 4;
		var dataView = new DataView(arrayBuffer,offset + 4, size*4);
		var littleEndian = false;
		for (var i=0;i<size;++i) {
			response.data[i] = dataView.getInt32(i*4,littleEndian);
		}
		response.offset += size * 4;
		return response;
	},

	_applySolidSettings:function(/* vwgl.Drawable */ drawable, /* object */ projectorData) {
		var loader = new vwgl.Loader();
		var indexedMaterials = {}
		for (var i=0;i<projectorData.length;++i) {
			var proj = projectorData[i];
			indexedMaterials[proj.name] = proj;
		}
		
		var solidList = drawable.getSolidList();
		for (var i=0; i<solidList.length; ++i) {
			var solid = solidList[i];
			var name = solid.getPolyList().getMaterialName();
			var settings = indexedMaterials[name];
			if (settings) {
				if (settings["shadowTexture"]) {
					var shadowTexFile = settings["shadowTexture"];
					var attenuation = settings["shadowAttenuation"];
					var projection = this._stringToFloatArray(settings["shadowProjection"]);
					var transform = this._stringToFloatArray(settings["shadowTransform"]);
					
					projector = new vwgl.Projector();
					projector.setProjection(new vwgl.Matrix4(projection));
					projector.setTransform(new vwgl.Matrix4(transform));
					projector.setAttenuation(attenuation);
					var texture = loader.loadTexture(shadowTexFile);
					projector.setTexture(texture);
					
					solid.setShadowProjector(projector);
				}
				var visible = settings["visible"]!==undefined ? settings["visible"]:true;
				var groupName = settings["groupName"]!==undefined ? settings["groupName"]:"";
				solid.setVisible(visible);
				solid.setGroupName(groupName);
			}
		}
	},
	
	_stringToFloatArray:function(strArray) {
		var values = strArray.split(" ");
		var result = [];
		for (var i=0; i<values.length; ++i) {
			result.push(parseFloat(values[i]));
		}
		return result;
	}
});