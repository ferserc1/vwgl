

Class ("vwgl.scene.SceneObject", vwgl.scene.ILifeCycle, {
	_componentMap:null,	// map of components
	_name:"",			// string
	_enabled:true,		// bool

	initialize:function(name) {
		if (name) {
			this._name = name;
		}
		this._componentMap = {};
    },

    setName:function(name) { this._name = name; },
    getName:function() { return this._name; },

	setEnabled:function(/* bool */ enabled) { this._enabled = enabled!==undefined ? enabled:true; },
	setDisabled:function() { this._enabled = false; },
	isEnabled:function() { return this._enabled; },

	addComponent:function(/* scene.Component */ component) {
		if (dynamic_cast("vwgl.Component",component)) {
			if (component._sceneObject) {
				component._sceneObject.removeComponent(component);
			}
			this._componentMap[Class.typeid(component)] = component;
			component._sceneObject = this;
		}
		else {
			base.log.error("The specified object is not a component");
		}
	},

	removeComponent:function(/* scene.Component */ component) {
		if (component) {
			if (!component._typeid) {
				base.log.error("The specified object is not a component: _typeid not found");
				return;
			}
			delete this._componentMap[Class.typeid(component)];
		}
	},

	getComponent:function(componentClass) {
		return this._componentMap[Class.typeid(componentClass)];
	},

	eachComponent:function(callback) {
		for (var key in this._componentMap) {
			callback(this._componentMap[key]);
		}
	}
});

Class ("vwgl.scene.Node", vwgl.scene.SceneObject, {
	_children:null,		// vwgl.scene.Node []
	_parent:null,		// vwgl.scene.Node

	initialize:function(name) {
		this.parent(name);
		this._children = [];
	},

	addChild:function(/* scene.Node */ child) {
		if (!child || child._parent==this) return;
    	if (child._parent) child._parent.removeChild(child);
		if (!this.haveChild(child)) {
			this._children.push(child);
			child._parent = this;
		}
	},

	removeChild:function(/* scene.Node */ child) {
		var pos = -1;
		for (var i=0; i<this._children.length; ++i) {
    		if (this._children[i]==child) {
    			pos = i;
    			break;
    		}
    	}
    	if  (pos!=-1) {
  			this._children.splice(pos,1);
  			child._parent = null;
    	}
	},

	haveChild:function(/* scene.Node */ child) {
		for (var i=0; i<this._children.length; ++i) {
			if (this._children[i]==child) return true;
		}
		return false;
	},

	nextChildOf:function(child) {
		var childIndex = -1;
		if (this._children && (childIndex=this._children.indexOf(child))!=-1) {
			if (childIndex==this._children.length-1) return null;
			return this._children[childIndex+1];
		}
		return null;
	},

	prevChildOf:function(child) {
		var childIndex = -1;
		if (this._children && (childIndex=this._children.indexOf(child))!=-1) {
			if (childIndex==0) return null;
			return this._children[childIndex-1];
		}
		return null;
	},

	swapChildren:function(childA,childB) {
		if (!this._children) return false;
		var childAIndex = this._children.indexOf(childA);
		var childBIndex = this._children.indexOf(childB);
		if (childAIndex!=-1 && childBIndex!=-1 && childA!=childB) {
			this._children[childAIndex] = childB;
			this._children[childBIndex] = childA;
			return true;
		}
		return false;
	},

	moveChildNextTo:function(/* Node */ nextToNode, /* Node */ movingNode) {
		if (!this._children) return false;
		var aIndex = this._children.indexOf(nextToNode);
		var bIndex = this._children.indexOf(movingNode);
		if (nextToNode!=movingNode && aIndex!=-1 && bIndex!=-1) {
			if (aIndex==this._children.length-1) {
				this._children.splice(bIndex,1);
				this._children.push(movingNode);
			}
			else {
				this._children.splice(bIndex,1);
				this._children.splice(aIndex+1,0,movingNode);
			}
			return true;
		}
		return false;
	},

	iterateChildren:function(callback) {
		for (var i=0; i<this._children.length; ++i) {
			callback(this._children[i]);
		}
	},

	accept:function(/* scene.NodeVisitor */ visitor) {
		visitor.visit(this);
		this.iterateChildren(function(node) {
			node.accept(visitor);
		});
		visitor.didVisit(this);
	},

	acceptReverse:function(/* scene.NodeVisitor */ visitor) {
		if (this._parent) {
			this._parent.acceptReverse(visitor);
		}
		visitor.visit();
	}
});