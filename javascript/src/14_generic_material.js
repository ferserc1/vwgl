
Class ("vwgl.MaterialModifier",{
	_modifierFlags:0,

	_diffuse:null,					// Color
	_specular:null,					// Color
	_shininess:null,				// float
	_lightEmission:null,			// float
	_refractionAmount:null,			// float
	_reflectionAmount:null,			// float
	_texture:null,					// std::string
	_lightmap:null,					// std::string
	_normalMap:null,				// std::string
	_lightNormalMap:null,			// std::string
	_textureOffset:null,			// Vector2
	_textureScale:null,				// Vector2
	_lightmapOffset:null,			// Vector2
	_lightmapScale:null,			// Vector2
	_normalMapOffset:null,			// Vector2
	_normalMapScale:null,			// Vector2
	_cubeMapPosX:null,				// std::string
	_cubeMapNegX:null,				// std::string
	_cubeMapPosY:null,				// std::string
	_cubeMapNegY:null,				// std::string
	_cubeMapPosZ:null,				// std::string
	_cubeMapNegZ:null,				// std::string
	_castShadows:null,				// bool
	_receiveShadows:null,			// bool
	_receiveProjections:null,		// bool
	_alphaCutoff:0.5,				// float
	_shininessMask:null,			// string
	_shininessMaskChannel:0,		// int
	_shininessMaskInvert:false,		// bool
	_lightEmissionMask:null,		// string
	_lightEmissionMaskChannel:0,	// int
	_lightEmissionMaskInvert:false,	// bool
	

	initialize:function() {
		this._diffuse = new vwgl.Color(1.0);
		this._specular = new vwgl.Color(1.0);
		this._textureOffset = new vwgl.Vector2(0.0);
		this._textureScale = new vwgl.Vector2(1.0);
		this._lightmapOffset = new vwgl.Vector2(0.0);
		this._lightmapScale = new vwgl.Vector2(1.0);
		this._normalMapOffset = new vwgl.Vector2(0.0);
		this._normalMapScale = new vwgl.Vector2(1.0);		
	},

	setFlags:function(flags) { this._modifierFlags = flags; },
	isEnabled:function(flag) { return (this._modifierFlags & flag)!=0; },
	setEnabled:function(flag) { this._modifierFlags = this._modifierFlags | flag; },

	setDiffuse:function(d) { this.setEnabled(vwgl.MaterialModifier.kDiffuse); this._diffuse.assign(d); },
	getDiffuse:function() { return this._diffuse; },

	setSpecular:function(d) { this.setEnabled(vwgl.MaterialModifier.kSpecular); this._specular.assign(d); },
	getSpecular:function() { return this._specular; },

	setShininess:function(s) { this.setEnabled(vwgl.MaterialModifier.kShininess); this._shininess = s; },
	getShininess:function() { return this._shininess; },

	setLightEmission:function(e) { this.setEnabled(vwgl.MaterialModifier.kLightEmission); this._lightEmission = e; },
	getLightEmission:function() { return this._lightEmission; },

	setRefractionAmount:function(r) { this.setEnabled(vwgl.MaterialModifier.kRefractionAmount); this._refractionAmount = r; },
	getRefractionAmount:function() { return this._refractionAmount; },

	setReflectionAmount:function(r) { this.setEnabled(vwgl.MaterialModifier.kReflectionAmount); this._reflectionAmount = r; },
	getReflectionAmount:function() { return this._reflectionAmount; },

	setTexture:function(/* string */ t) { this.setEnabled(vwgl.MaterialModifier.kTexture); this._texture = t; },
	getTexture:function() { return this._texture; },

	setLightMap:function(/* string */ t) { this.setEnabled(vwgl.MaterialModifier.kLightmap); this._lightmap = t; },
	getLightMap:function() { return this._lightmap; },

	setNormalMap:function(/* string */ t) { this.setEnabled(vwgl.MaterialModifier.kNormalMap); this._normalMap = t; },
	getNormalMap:function() { return this._normalMap; },

	setLightNormalMap:function(/* string */ t) { this.setEnabled(vwgl.MaterialModifier.kLightNormalMap); this._lightNormalMap = t; },
	getLightNormalMap:function() { return this._lightNormalMap; },

	setTextureOffset:function(o) { this.setEnabled(vwgl.MaterialModifier.kTextureOffset); this._textureOffset = o; },
	getTextureOffset:function() { return this._textureOffset; },

	setTextureScale:function(o) { this.setEnabled(vwgl.MaterialModifier.kTextureScale); this._textureScale = o; },
	getTextureScale:function() { return this._textureScale; },

	setLightmapOffset:function(o) { this.setEnabled(vwgl.MaterialModifier.kLightmapOffset); this._lightmapOffset = o; },
	getLightmapOffset:function() { return this._lightmapOffset; },

	setLightmapScale:function(o) { this.setEnabled(vwgl.MaterialModifier.kLightmapScale); this._lightmapScale = o; },
	getLightmapScale:function() { return this._lightmapScale; },

	setNormalMapScale:function(o) { this.setEnabled(vwgl.MaterialModifier.kNormalMapScale); this._normalMapScale = o; },
	getNormalMapScale:function() { return this._normalMapScale; },

	setNormalMapOffset:function(o) { this.setEnabled(vwgl.MaterialModifier.kNormalMapOffset); this._normalMapOffset = o; },
	getNormalMapOffset:function() { return this._normalMapOffset; },

	setCubeMapPosX:function(cubeMap) { this.setEnabled(vwgl.MaterialModifier.kCubeMap); this._cubeMapPosX = cubeMap; },
	getCubeMapPosX:function() { return this._cubeMapPosX; },

	setCubeMapNegX:function(cubeMap) { this.setEnabled(vwgl.MaterialModifier.kCubeMap); this._cubeMapNegX = cubeMap; },
	getCubeMapNegX:function() { return this._cubeMapNegX; },

	setCubeMapPosY:function(cubeMap) { this.setEnabled(vwgl.MaterialModifier.kCubeMap); this._cubeMapPosY = cubeMap; },
	getCubeMapPosY:function() { return this._cubeMapPosY; },

	setCubeMapNegY:function(cubeMap) { this.setEnabled(vwgl.MaterialModifier.kCubeMap); this._cubeMapNegY = cubeMap; },
	getCubeMapNegY:function() { return this._cubeMapNegY; },

	setCubeMapPosZ:function(cubeMap) { this.setEnabled(vwgl.MaterialModifier.kCubeMap); this._cubeMapPosZ = cubeMap; },
	getCubeMapPosZ:function() { return this._cubeMapPosZ; },

	setCubeMapNegZ:function(cubeMap) { this.setEnabled(vwgl.MaterialModifier.kCubeMap); this._cubeMapNegZ = cubeMap; },
	getCubeMapNegZ:function() { return this._cubeMapNegZ; },

	setCastShadows:function(s) { this.setEnabled(vwgl.MaterialModifier.kCastShadows); this._castShadows = s; },
	getCastShadows:function() { return this._castShadows; },

	setReceiveShadows:function(s) { this.setEnabled(vwgl.MaterialModifier.kReceiveShadows); this._receiveShadows = s; },
	getReceiveShadows:function() { return this._receiveShadows; },

	setReceiveProjections:function(p) { this.setEnabled(vwgl.MaterialModifier.kReceiveProjections); this._receiveProjections = p; },
	getReceiveProjections:function() { return this._receiveProjections; },
	

	setAlphaCutoff:function(/* float */ c) { this.setEnabled(vwgl.MaterialModifier.kAlphaCutoff); this._alphaCutoff = c; },
	getAlphaCutoff:function() { return this._alphaCutoff; },
	
	setShininessMask:function(/* string */ shininessMask, /* int */ channel, /* bool*/ invert) {
		if (channel===undefined) channel = 0;
		if (invert===undefined) invert = false;
		this.setEnabled(vwgl.MaterialModifier.kShininessMask);
		this._shininessMask = shininessMask;
		this._shininessMaskChannel = channel;
		this._shininessMaskInvert = invert;
	},
	getShininessMask:function() { return this._shininessMask; },
	getShininessMask:function() { return this._shininessMask; },
	getShininessMaskChannel:function() { return this._shininessMaskChannel; },
	getInvertShininessMask:function() { return this._shininessMaskInvert; },
	setInvertShininessMask:function(/* bool */ invert) { this._shininessMaskInvert = invert; },
	
	setLightEmissionMask:function(/* string */ leMask, /* int */ channel, /* bool */ invert) {
		if (channel===undefined) channel = 0;
		if (invert===undefined) invert = false;
		this.setEnabled(vwgl.MaterialModifier.kLightEmissionMask);
		this._lightEmissionMask = leMask;
		this._lightEmissionMaskChannel = channel;
		this._lightEmissionMaskInvert = invert;
	},
	getLightEmissionMask:function() { return this._lightEmissionMask; },
	getLightEmissionMask:function() { return this._lightEmissionMask; },
	getLightEmissionMaskChannel:function() { return this._lightEmissionMaskChannel; },
	getInvertLightEmissionMask:function() { return this._lightEmissionMaskInvert; },
	setInvertLightEmissionMask:function(/* bool */ invert) { this._lightEmissionMaskInvert = invert; },

	assign:function(mod) {
		this._modifierFlags = mod._modifierFlags;
		this._diffuse = mod._diffuse;
		this._specular = mod._specular;
		this._shininess = mod._shininess;
		this._lightEmission = mod._lightEmission;
		this._refractionAmount = mod._refractionAmount;
		this._reflectionAmount = mod._reflectionAmount;
		this._texture = mod._texture;
		this._lightmap = mod._lightmap;
		this._normalMap = mod._normalMap;
		this._lightNormalMap = mod._lightNormalMap;
		this._textureOffset = mod._textureOffset;
		this._textureScale = mod._textureScale;
		this._lightmapOffset = mod._lightmapOffset;
		this._lightmapScale = mod._lightmapScale;
		this._normalMapOffset = mod._normalMapOffset;
		this._normalMapScale = mod._normalMapScale;
		this._cubeMapPosX = mod._cubeMapPosX;
		this._cubeMapNegX = mod._cubeMapNegX;
		this._cubeMapPosY = mod._cubeMapPosY;
		this._cubeMapNegY = mod._cubeMapNegY;
		this._cubeMapPosZ = mod._cubeMapPosZ;
		this._cubeMapNegZ = mod._cubeMapNegZ;
		this._castShadows = mod._castShadows;
		this._receiveShadows = mod._receiveShadows;
		this._receiveProjections = mod._receiveProjections;
		this._alphaCutoff = mod._alphaCutoff;
		this._shininessMask = mod._shininessMask;
		this._shininessMaskChannel = mod._shininessMaskChannel;
		this._shininessMaskInvert = mod._shininessMaskInvert;
		this._lightEmissionMask = mod._lightEmissionMask;
		this._lightEmissionMaskChannel = mod._lightEmissionMaskChannel;
		this._lightEmissionMaskInvert = mod._lightEmissionMaskInvert;
	}
});

vwgl.MaterialModifier.kDiffuse					= 1 << 0;
vwgl.MaterialModifier.kSpecular 				= 1 << 1;
vwgl.MaterialModifier.kShininess				= 1 << 2;
vwgl.MaterialModifier.kLightEmission			= 1 << 3;
vwgl.MaterialModifier.kRefractionAmount			= 1 << 4;
vwgl.MaterialModifier.kReflectionAmount			= 1 << 5;
vwgl.MaterialModifier.kTexture					= 1 << 6;
vwgl.MaterialModifier.kLightmap					= 1 << 7;
vwgl.MaterialModifier.kNormalMap				= 1 << 8;
vwgl.MaterialModifier.kLightNormalMap			= 1 << 9;
vwgl.MaterialModifier.kTextureOffset			= 1 << 10;
vwgl.MaterialModifier.kTextureScale				= 1 << 11;
vwgl.MaterialModifier.kLightmapOffset			= 1 << 12;
vwgl.MaterialModifier.kLightmapScale			= 1 << 13;
vwgl.MaterialModifier.kNormalMapOffset			= 1 << 14;
vwgl.MaterialModifier.kNormalMapScale			= 1 << 15;
vwgl.MaterialModifier.kCubeMap					= 1 << 16;
vwgl.MaterialModifier.kCastShadows				= 1 << 17;
vwgl.MaterialModifier.kReceiveShadows			= 1 << 18;
vwgl.MaterialModifier.kReceiveProjections		= 1 << 19;
vwgl.MaterialModifier.kAlphaCutoff				= 1 << 20,
vwgl.MaterialModifier.kShininessMask			= 1 << 21,
vwgl.MaterialModifier.kLightEmissionMask		= 1 << 22


Class ("vwgl.GenericMaterial",vwgl.Material,{
	_diffuse:null,
	_specular:null,
	_shininess:null,
	_refractionAmount:null,
	_reflectionAmount:null,
	_lightEmission:null,
	_texture:null,
	_lightMap:null,
	_normalMap:null,
	_lightNormalMap:null,
	_textureOffset:null,
	_textureScale:null,
	_lightmapOffset:null,
	_lightmapScale:null,
	_normalMapOffset:null,
	_normalMapScale:null,
	_cubeMap:null,
	_castShadows:true,
	_receiveShadows:true,
	_selectedMode:false,
	_receiveProjections:false,
	_alphaCutoff:0.5,
	_shininessMask:null,
	_shininessMaskChannel:0,
	_shininessMaskInvert:false,
	_lightEmissionMask:null,
	_lightEmissionMaskChannel:0,
	_lightEmissionMaskInver:false,
	
	_settingsMaterial:null,

	initialize:function(nameOrReadyCB,readyCallback) {
		this._setReuseKey("generic_material_shader");

		this._diffuse = new vwgl.Color(0.8,0.8,0.8,1.0);
		this._specular = vwgl.Color.white();
		this._shininess = 0;
		this._refractionAmount = 0;
		this._reflectionAmount = 0;
		this._lightEmission = 0;
		this._texture = null;
		this._lightMap = null;
		this._normalMap = null;
		this._lightNormalMap = null;
		this._textureOffset = new vwgl.Vector2(0);
		this._textureScale = new vwgl.Vector2(1);
		this._lightmapOffset = new vwgl.Vector2(0);
		this._lightmapScale = new vwgl.Vector2(1);
		this._normalMapOffset = new vwgl.Vector2(0);
		this._normalMapScale = new vwgl.Vector2(1);
		this._cubeMap = null;
		this._castShadows = true;
		this._receiveShadows = true;
		this._selectedMode = false;
		this._receiveProjections = false;
		this._alphaCutoff = 0.5;
		this._shininessMaskChannel = 0;
		this._shininessMaskInvert = false;
		this._lightEmissionMaskChannel = 0;
		this._lightEmissionMaskInvert = false;
		
		
		this.parent(nameOrReadyCB,readyCallback);
	},

	getResourceList:function() {
		return ["generic.vsh","generic.fsh"];
	},

	initShaderSync:function(loader) {
		if (this._initialized) return;

		this.getShader().attachShader(vwgl.Shader.kTypeVertex,loader.getResource("generic.vsh"));
		this.getShader().attachShader(vwgl.Shader.kTypeFragment,loader.getResource("generic.fsh"));
		this.getShader().link("generic_material");

		this.loadVertexAttrib("aVertexPosition");
		this.loadNormalAttrib("aVertexNormal");
		this.loadTangentArray("aVertexTangent");
		this.loadTexCoord0Attrib("aTextureCoord");
		this.loadTexCoord1Attrib("aLightmapCoord");

		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		this.getShader().initUniformLocation("uNMatrix");
		this.getShader().initUniformLocation("uVMatrix");
		this.getShader().initUniformLocation("uMMatrix");
		this.getShader().initUniformLocation("uVMatrixInv");

		this.getShader().initUniformLocation("uDiffuseColor");
		this.getShader().initUniformLocation("uSpecularColor");
		this.getShader().initUniformLocation("uShininess");
		this.getShader().initUniformLocation("uLightEmission");
		this.getShader().initUniformLocation("uUseTexture");
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uUseLightMap");
		this.getShader().initUniformLocation("uLightMap");
		this.getShader().initUniformLocation("uUseNormalMap");
		this.getShader().initUniformLocation("uNormalMap");
		this.getShader().initUniformLocation("uNormalMapOffset");
		this.getShader().initUniformLocation("uNormalMapScale");

		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");
		this.getShader().initUniformLocation("uLightmapOffset");
		this.getShader().initUniformLocation("uLightmapScale");

		this.getShader().initUniformLocation("uCubeMap");
		this.getShader().initUniformLocation("uUseCubeMap");
		this.getShader().initUniformLocation("uReflectionAmount");

		this.getShader().initUniformLocation("uAmbientLight");
		this.getShader().initUniformLocation("uDiffuseLight");
		this.getShader().initUniformLocation("uSpecularLight");
		this.getShader().initUniformLocation("uLightType");
		this.getShader().initUniformLocation("uLightPosition");
		this.getShader().initUniformLocation("uLightDirection");

		this.getShader().initUniformLocation("uAttenuation");
		this.getShader().initUniformLocation("uSpotCutoff");
		this.getShader().initUniformLocation("uSpotExponent");
		this.getShader().initUniformLocation("uSpotDirection");

		this.getShader().initUniformLocation("uSelectMode");

		this.getShader().initUniformLocation("uHue");
		this.getShader().initUniformLocation("uSaturation");
		this.getShader().initUniformLocation("uLightness");
		this.getShader().initUniformLocation("uBrightness");
		this.getShader().initUniformLocation("uContrast");

		this._initialized = true;
	},

	setupUniforms:function() {
		var settings = (this._settingsMaterial) ? this._settingsMaterial:this;

		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		this.getShader().setUniform("uNMatrix", this.normalMatrix());
		this.getShader().setUniform("uVMatrix", this.viewMatrix());
		this.getShader().setUniform("uMMatrix", this.modelMatrix());
		var vMat = new vwgl.Matrix4(this.viewMatrix());
		vMat.invert();
		this.getShader().setUniform("uVMatrixInv", vMat);

		this.getShader().setUniform("uDiffuseColor", settings._diffuse);
		this.getShader().setUniform("uSpecularColor", settings._specular);

		this.getShader().setUniform1f("uShininess", settings._shininess);
		this.getShader().setUniform1f("uLightEmission", settings._lightEmission);
		if (settings._texture) {
			this.getShader().setUniform1i("uUseTexture", settings._texture!=null);
			this.getShader().setUniform("uTexture", settings._texture, vwgl.Texture.kTexture0);
		}
		else {
			this.getShader().setUniform1i("uUseTexture", false);
		}

		if (settings._lightMap) {
			this.getShader().setUniform1i("uUseLightMap", settings._lightMap!=null);
			this.getShader().setUniform("uLightMap", settings._lightMap, vwgl.Texture.kTexture1);
		}
		else {
			this.getShader().setUniform1i("uUseLightMap", false);
		}

		if (settings._normalMap) {
			this.getShader().setUniform1i("uUseNormalMap", settings._normalMap!=null);
			this.getShader().setUniform("uNormalMap", settings._normalMap, vwgl.Texture.kTexture2);
		}
		else {
			this.getShader().setUniform1i("uUseNormalMap", false);
		}

		if (settings._cubeMap) {
			this.getShader().setUniform1i("uUseCubeMap", true);
			this.getShader().setUniform("uCubeMap", settings._cubeMap, vwgl.Texture.kTexture3);
			this.getShader().setUniform1f("uReflectionAmount", settings._reflectionAmount);
		}
		else {
			this.getShader().setUniform1i("uUseCubeMap", false);
			vwgl.Texture.setActiveTexture(vwgl.Texture.kTexture3);
			gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
			this.getShader().setUniform1i("uCubeMap", 3);
		}

		var lm = vwgl.LightManager.get();
		var light = lm.getLightSourcesCount()>0 ? lm.getLightList()[0]:null;
		if (light) {
			this.getShader().setUniform("uAmbientLight", light.getAmbient());
			this.getShader().setUniform("uDiffuseLight", light.getDiffuse());
			this.getShader().setUniform("uSpecularLight", light.getSpecular());
			this.getShader().setUniform1i("uLightType", light.getType());

			var viewMatrix = vwgl.CameraManager.get().getMainCamera().getViewMatrix();
			var lightPos = new vwgl.Vector4(light.getPosition());
			lightPos.w(1.0);
			lightPos = viewMatrix.multVector(lightPos);
			this.getShader().setUniform("uLightPosition",lightPos.xyz());

			this.getShader().setUniform("uLightDirection", light.getDirection());

			var attenuation = new vwgl.Vector3(	light.getConstantAttenuation(),
												light.getLinearAttenuation(),
												light.getExpAttenuation());
			this.getShader().setUniform("uAttenuation",attenuation);
			this.getShader().setUniform1f("uSpotCutoff",light.getSpotCutoff());
			this.getShader().setUniform1f("uSpotExponent",light.getSpotExponent());
			this.getShader().setUniform("uSpotDirection",light.getSpotDirection());
		}

		this.getShader().setUniform("uTextureOffset",settings._textureOffset);
		this.getShader().setUniform("uTextureScale",settings._textureScale);
		this.getShader().setUniform("uLightmapOffset",settings._lightmapOffset);
		this.getShader().setUniform("uLightmapScale",settings._lightmapScale);
		this.getShader().setUniform("uNormalMapOffset", settings._normalMapOffset);
		this.getShader().setUniform("uNormalMapScale", settings._normalMapScale);

		this.getShader().setUniform1i("uSelectMode", this._selectedMode);

		var renderSettings = vwgl.RenderSettings.getCurrentRenderSettings();
		if (renderSettings) {
			this.getShader().setUniform1f("uHue",renderSettings.getHue());
			this.getShader().setUniform1f("uSaturation",renderSettings.getSaturation());
			this.getShader().setUniform1f("uLightness",renderSettings.getLightness());
			this.getShader().setUniform1f("uBrightness",renderSettings.getBrightness());
			this.getShader().setUniform1f("uContrast",renderSettings.getContrast());
		}
		else {
			this.getShader().setUniform1f("uHue",1.0);
			this.getShader().setUniform1f("uSaturation",1.0);
			this.getShader().setUniform1f("uLightness",1.0);
			this.getShader().setUniform1f("uBrightness",0.5);
			this.getShader().setUniform1f("uContrast",0.5);
		}
	},

	isTransparent:function() { return this._diffuse.a()!=1; },

	setDiffuse:function(/* vwgl.Vector4 */ d) { this._diffuse.assign(d); },
	getDiffuse:function() { return this._diffuse; },

	setSpecular:function(/* vwgl.Vector4 */ d) { this._specular.assign(d); },
	getSpecular:function() { return this._specular; },

	setShininess:function(s) { this._shininess = s; },
	getShininess:function() { return this._shininess; },

	setLightEmission:function(e) { this._lightEmission = e; },
	getLightEmission:function() { return this._lightEmission; },

	setRefractionAmount:function(r) { this._refractionAmount = r; },
	getRefractionAmount:function() { return this._refractionAmount; },

	setReflectionAmount:function(r) { this._reflectionAmount = r; },
	getReflectionAmount:function() { return this._reflectionAmount; },

	setTexture:function(/* Texture */ t) { this._texture = t; },
	getTexture:function() { return this._texture; },
	getTexturePath:function(useImageKeysInsteadPath) {
		if (this._texture) {
			return useImageKeysInsteadPath ? this._texture.key:this._texture._fileName;
		}
	},

	setLightMap:function(/* Texture */ t) { this._lightMap = t; },
	getLightMap:function() { return this._lightMap; },
	getLightMapPath:function(useImageKeysInsteadPath) {
		if (this._lightMap) {
			return useImageKeysInsteadPath ? this._lightMap.key:this._lightMap._fileName;
		}
	},

	setNormalMap:function(/* Texture */ t) { this._normalMap = t; },
	getNormalMap:function() { return this._normalMap; },
	getNormalMapPath:function(useImageKeysInsteadPath) {
		if (this._normalMap) {
			return useImageKeysInsteadPath ? this._normalMap.key:this._normalMap._fileName;
		}
	},

	setLightNormalMap:function(/* Texture */ t) { this._lightNormalMap = t; },
	getLightNormalMap:function() { return this._lightNormalMap; },
	getLightNormalMapPath:function(useImageKeysInsteadPath) {
		if (this._lightNormalMap) {
			return useImageKeysInsteadPath ? this._lightNormalMap.key:this._lightNormalMap._fileName;
		}
	},

	setTextureOffset:function(/* Vector2 */ o) { this._textureOffset = o; },
	getTextureOffset:function() { return this._textureOffset; },

	setTextureScale:function(/* Vector2 */ o) { this._textureScale = o; },
	getTextureScale:function() { return this._textureScale; },

	setLightmapOffset:function(/* Vector2 */ o) { this._lightmapOffset = o; },
	getLightmapOffset:function() { return this._lightmapOffset; },

	setLightmapScale:function(/* Vector2 */ o) { this._lightmapScale = o; },
	getLightmapScale:function() { return this._lightmapScale; },

	setNormalMapScale:function(/* Vector2 */ o) { this._normalMapScale = o; },
	getNormalMapScale:function() { return this._normalMapScale; },

	setNormalMapOffset:function(/* Vector2 */ o) { this._normalMapOffset = o; },
	getNormalMapOffset:function() { return this._normalMapOffset; },

	setCubeMap:function(/* CubeMap */ cubeMap) { this._cubeMap = cubeMap; },
	getCubeMap:function() { return this._cubeMap; },

	setCastShadows:function(s) { this._castShadows = s; },
	getCastShadows:function() { return this._castShadows; },

	setReceiveShadows:function(s) { this._receiveShadows = s; },
	getReceiveShadows:function() { return this._receiveShadows; },

	setSelectedMode:function(selected) { this._selectedMode = selected; },
	getSelectedMode:function() { return this._selectedMode; },

	setReceiveProjections:function(p) { this._receiveProjections = p; },
	getReceiveProjections:function() { return this._receiveProjections; },
	
	setAlphaCutoff:function(/* float */ c) { this._alphaCutoff = c; },
	getAlphaCutoff:function() { return this._alphaCutoff; },
	
	setShininessMask:function(/* Texture */ t, /* int */ channel, /* bool */ invert) {
		if (channel===undefined) channel = 0;
		if (invert===undefined) invert = false;
		this._shininessMask = t;
		this._shininessMaskChannel = channel;
		this._shininessMaskInvert = invert;
	},
	getShininessMask:function() { return this._shininessMask; },
	getShininessMaskPath:function() {
		if (this._shininessMask) {
			return this._shininessMask.getFileName();
		}
		return "";
	},
	
	getShininessMaskChannel:function() { return this._shininessMaskChannel; },
	getInvertShininessMask:function() { return this._shininessMaskInvert; },
	setInvertShininessMask:function(/* bool */ invert) { this._shininessMaskInvert = invert; },
	
	setLightEmissionMask:function(/* Texture */ tex, /* int */ channel, /* bool */ invert) {
		if (channel===undefined) channel = 0;
		if (invert===undefined) invert = 0;
		this._lightEmissionMask = tex;
		this._lightEmissionMaskChannel = channel;
		this._lightEmissionMaskInvert = invert;
	},
	getLightEmissionMask:function() { return this._lightEmissionMask; },
	getLightEmissionMaskPath:function() {
		if (this._lightEmissionMask) {
			return this._lightEmissionMask.getFileName();
		}
		return "";
	},

	getLightEmissionMaskChannel:function() { return this._lightEmissionMaskChannel; },
	getInvertLightEmissionMask:function() { return this._lightEmissionMaskInvert; },
	setInvertLightEmissionMask:function(/* bool */ invert) { this._lightEmissionMaskInvert = invert; },


	destroy:function() {
		if (this._texture) vwgl.TextureManager.unrefTexture(this._texture);
		if (this._lightMap) vwgl.TextureManager.unrefTexture(this._lightMap);
		if (this._normalMap) vwgl.TextureManager.unrefTexture(this._normalMap);
		this._texture = null;
		this._lightMap = null;
		this._normalMap = null;
	},

	useSettingsOf:function(/* Material */ tex) { this._settingsMaterial = dynamic_cast("vwgl.GenericMaterial",tex); },

	applySettings:function(/* Drawable */ drawable, settingsMask) {
		var slist = drawable.getSolidList();
		for (var i=0;i<slist.length;++i) {
			var s = slist[i];
			this.applyMaterialSettings(dynamic_cast("vwtl.GenericMaterial",s.getMaterial()),settingsMask);
		}
	},

	applySettingsWithJson:function(jsonStringOrObject) {
		// TODO: implement this
	},

	applyMaterialSettings:function(/* vwgl.GenericMaterial */ mat, settingsMask) {
		if (mat) {
			if (settingsMask | vwgl.GenericMaterial.kDiffuseColor) {
				mat.setDiffuse(this._diffuse);
			}
			if (settingsMask | vwgl.GenericMaterial.kShininess) {
				mat.setShininess(this._shininess);
			}
			if (settingsMask | vwgl.GenericMaterial.kRefractionAmount) {
				mat.setRefractionAmount(this._refractionAmount);
			}
			if (settingsMask | vwgl.GenericMaterial.kLightEmission) {
				mat.setLightEmission(this._lightEmission);
			}
			if (settingsMask | vwgl.GenericMaterial.kTexture) {
				mat.setTexture(this._texture);
			}
			if (settingsMask | vwgl.GenericMaterial.kLightMap) {
				mat.setLightMap(this._lightMap);
			}
			if (settingsMask | vwgl.GenericMaterial.kNormalMap) {
				mat.setNormalMap(this._normalMap);
			}
			if (settingsMask | vwgl.GenericMaterial.kTextureOffset) {
				mat.setTextureOffset(this._textureOffset);
			}
			if (settingsMask | vwgl.GenericMaterial.kTextureScale) {
				mat.setTextureScale(this._textureScale);
			}
			if (settingsMask | vwgl.GenericMaterial.kLightmapOffset) {
				mat.setLightmapOffset(this._lightmapOffset);
			}
			if (settingsMask | vwgl.GenericMaterial.kLightmapScacle) {
				mat.setLightmapScale(this._lightmapScale);
			}
			if (settingsMask | vwgl.GenericMaterial.kCastShadows) {
				mat.setCastShadows(this._castShadows);
			}
			if (settingsMask | vwgl.GenericMaterial.kReceiveShadows) {
				mat.setReceiveShadows(this._receiveShadows);
			}
			if (settingsMask | vwgl.GenericMaterial.kSelectedMode) {
				mat.setSelectedMode(this._selectedMode);
			}
			if (settingsMask | vwgl.GenericMaterial.kAlphaCutoff) {
				mat.setAlphaCutoff(this._alphaCutoff);
			}
			if (settingsMask | vwgl.GenericMaterial.kShininessMask) {
				mat.setShininessMask(this._shininessMask,this._shininessMaskChannel);
			}
			if (settingsMask | vwgl.GenericMaterial.kLightEmissionMask) {
				mat.setLightEmissionMask(this._lightEmissionMask,this._lightEmissionMaskChannel);
			}			
		}
	},

	isAbsolutePath:function(path) {
		return /^(f|ht)tps?:\/\//i.test(path);
	},

	applyModifier:function(/* MaterialModifier */ mod, /* string */ resourcePath) {
		if (resourcePath===undefined) resourcePath = vwgl.System.get().getResourcePath();
		var loader = new vwgl.Loader();
		if (mod.isEnabled(vwgl.MaterialModifier.kDiffuse)) this.setDiffuse(mod.getDiffuse());
		if (mod.isEnabled(vwgl.MaterialModifier.kSpecular)) this.setSpecular(mod.getSpecular());
		if (mod.isEnabled(vwgl.MaterialModifier.kShininess)) this.setShininess(mod.getShininess());
		if (mod.isEnabled(vwgl.MaterialModifier.kLightEmission)) this.setLightEmission(mod.getLightEmission());
		if (mod.isEnabled(vwgl.MaterialModifier.kRefractionAmount)) this.setRefractionAmount(mod.getRefractionAmount());
		if (mod.isEnabled(vwgl.MaterialModifier.kReflectionAmount)) this.setReflectionAmount(mod.getReflectionAmount());
		if (mod.isEnabled(vwgl.MaterialModifier.kTexture)) {
			var fullPath = mod.getTexture();
			var image = null;
			if (fullPath) {
				if (!this.isAbsolutePath(mod.getTexture())) {
					fullPath = mod.getTexture();
					vwgl.System.get().setResourcesPath(resourcePath);
				}
				else {
					vwgl.System.get().setResourcesPath("");
				}
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
				image = loader.loadTexture(fullPath);
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinear);
			}
			this.setTexture(image);
		}
		if (mod.isEnabled(vwgl.MaterialModifier.kLightmap)) {
			var fullPath = mod.getLightMap();
			var image = null;
			if (fullPath) {
				if (!this.isAbsolutePath(mod.getLightMap())) {
					fullPath = mod.getLightMap();
					vwgl.System.get().setResourcesPath(resourcePath);
				}
				else {
					vwgl.System.get().setResourcesPath("");
				}
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinear);
				image = loader.loadTexture(fullPath);
			}
			this.setLightMap(image);
		}
		if (mod.isEnabled(vwgl.MaterialModifier.kNormalMap)) {
			var fullPath = mod.getNormalMap();
			var image = null;
			if (fullPath) {
				if (!this.isAbsolutePath(mod.getNormalMap())) {
					fullPath = mod.getNormalMap();
					vwgl.System.get().setResourcesPath(resourcePath);
				}
				else {
					vwgl.System.get().setResourcesPath("");
				}
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
				image = loader.loadTexture(fullPath);
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinear);
			}
			this.setNormalMap(image);
		}
		if (mod.isEnabled(vwgl.MaterialModifier.kLightNormalMap)) {
			var fullPath = mod.getLightNormalMap();
			var image = null;
			if (fullPath) {
				if (!this.isAbsolutePath(mod.getLightNormalMap())) {
					fullPath = mod.getLightNormalMap();
					vwgl.System.get().setResourcesPath(resourcePath);
				}
				else {
					vwgl.System.get().setResourcesPath("");
				}
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
				image = loader.loadTexture(fullPath);
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinear);
			}
			this.setLightNormalMap(image);
		}
		if (mod.isEnabled(vwgl.MaterialModifier.kTextureScale)) this.setTextureScale(mod.getTextureScale());
		if (mod.isEnabled(vwgl.MaterialModifier.kTextureOffset)) this.setTextureOffset(mod.getTextureOffset());
		if (mod.isEnabled(vwgl.MaterialModifier.kLightmapScale)) this.setLightmapScale(mod.getLightmapScale());
		if (mod.isEnabled(vwgl.MaterialModifier.kLightmapOffset)) this.setLightmapOffset(mod.getLightmapOffset());
		if (mod.isEnabled(vwgl.MaterialModifier.kNormalMapScale)) this.setNormalMapScale(mod.getNormalMapScale());
		if (mod.isEnabled(vwgl.MaterialModifier.kNormalMapOffset)) this.setNormalMapOffset(mod.getNormalMapOffset());
		if (mod.isEnabled(vwgl.MaterialModifier.kCubeMap)) {
			var This = this;
			var resList = [	resourcePath + mod.getCubeMapPosX(),
							resourcePath + mod.getCubeMapNegX(),
							resourcePath + mod.getCubeMapPosY(),
							resourcePath + mod.getCubeMapNegY(),
							resourcePath + mod.getCubeMapPosZ(),
							resourcePath + mod.getCubeMapNegZ()];
			loader.loadResourceList(resList,function(loaded,errors) {
				This.setCubeMap(loader.loadCubemap(	resourcePath + mod.getCubeMapPosX(),
													resourcePath + mod.getCubeMapNegX(),
													resourcePath + mod.getCubeMapPosY(),
													resourcePath + mod.getCubeMapNegY(),
													resourcePath + mod.getCubeMapPosZ(),
													resourcePath + mod.getCubeMapNegZ()));
			})
		}
		if (mod.isEnabled(vwgl.MaterialModifier.kCastShadows)) this.setCastShadows(mod.getCastShadows());
		if (mod.isEnabled(vwgl.MaterialModifier.kReceiveShadows)) this.setReceiveShadows(mod.getReceiveShadows());
		if (mod.isEnabled(vwgl.MaterialModifier.kReceiveProjections)) this.setReceiveProjections(mod.getReceiveProjections());
		if (mod.isEnabled(vwgl.MaterialModifier.kAlphaCutoff)) this.setAlphaCutoff(mod.getAlphaCutoff());
		if (mod.isEnabled(vwgl.MaterialModifier.kShininessMask)) {
			var fullPath = mod.getShininessMask();
			var image = null;
			if (fullPath) {
				if (!this.isAbsolutePath(mod.getShininessMask())) {
					fullPath = mod.getShininessMask();
					vwgl.System.get().setResourcesPath(resourcePath);
				}
				else {
					vwgl.System.get().setResourcesPath("");
				}
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
				image = loader.loadTexture(fullPath);
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinear);
			}
			this.setShininessMask(image,
								mod.getShininessMaskChannel(),
								mod.getInvertShininessMask());
		}
		if (mod.isEnabled(vwgl.MaterialModifier.kLightEmissionMask)) {
			var fullPath = mod.getLightEmissionMask();
			var image = null;
			if (fullPath) {
				if (!this.isAbsolutePath(mod.getLightEmissionMask())) {
					fullPath = mod.getLightEmissionMask();
					vwgl.System.get().setResourcesPath(resourcePath);
				}
				else {
					vwgl.System.get().setResourcesPath("");
				}
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
				image = loader.loadTexture(fullPath);
				vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinear);
			}
			this.setLightEmissionMask(image,
								mod.getShininessMaskChannel(),
								mod.getInvertShininessMask());
		}
		
	},

	getModifierWithMask:function(modifierMask,useImageKeysInsteadPath) {
		var mod = new vwgl.MaterialModifier();

		mod.setFlags(modifierMask);
		if (mod.isEnabled(vwgl.MaterialModifier.kDiffuse)) mod.setDiffuse(this.getDiffuse());
		if (mod.isEnabled(vwgl.MaterialModifier.kSpecular)) mod.setSpecular(this.getSpecular());
		if (mod.isEnabled(vwgl.MaterialModifier.kShininess)) mod.setShininess(this.getShininess());
		if (mod.isEnabled(vwgl.MaterialModifier.kLightEmission)) mod.setLightEmission(this.getLightEmission());
		if (mod.isEnabled(vwgl.MaterialModifier.kRefractionAmount)) mod.setRefractionAmount(this.getRefractionAmount());
		if (mod.isEnabled(vwgl.MaterialModifier.kReflectionAmount)) mod.setReflectionAmount(this.getReflectionAmount());
		if (mod.isEnabled(vwgl.MaterialModifier.kTexture)) mod.setTexture(this.getTexturePath(useImageKeysInsteadPath));
		if (mod.isEnabled(vwgl.MaterialModifier.kLightmap)) mod.setLightMap(this.getLightMapPath(useImageKeysInsteadPath));
		if (mod.isEnabled(vwgl.MaterialModifier.kNormalMap)) mod.setNormalMap(this.getNormalMapPath(useImageKeysInsteadPath));
		if (mod.isEnabled(vwgl.MaterialModifier.kLightNormalMap)) mod.setLightNormalMap(this.getLightNormalMapPath(useImageKeysInsteadPath));
		if (mod.isEnabled(vwgl.MaterialModifier.kTextureScale)) mod.setTextureScale(this.getTextureScale());
		if (mod.isEnabled(vwgl.MaterialModifier.kTextureOffset)) mod.setTextureOffset(this.getTextureOffset());
		if (mod.isEnabled(vwgl.MaterialModifier.kLightmapScale)) mod.setLightmapScale(this.getLightmapScale());
		if (mod.isEnabled(vwgl.MaterialModifier.kLightmapOffset)) mod.setLightmapOffset(this.getLightmapOffset());
		if (mod.isEnabled(vwgl.MaterialModifier.kNormalMapScale)) mod.setNormalMapScale(this.getNormalMapScale());
		if (mod.isEnabled(vwgl.MaterialModifier.kNormalMapOffset)) mod.setNormalMapOffset(this.getNormalMapOffset());
		if (mod.isEnabled(vwgl.MaterialModifier.kCubeMap)) {
			mod.setCubeMapPosX(this.getCubeMapPosXPath(useImageKeysInsteadPath));
			mod.setCubeMapNegX(this.getCubeMapNegXPath(useImageKeysInsteadPath));
			mod.setCubeMapPosY(this.getCubeMapPosYPath(useImageKeysInsteadPath));
			mod.setCubeMapNegY(this.getCubeMapNegYPath(useImageKeysInsteadPath));
			mod.setCubeMapPosZ(this.getCubeMapPosZPath(useImageKeysInsteadPath));
			mod.setCubeMapNegZ(this.getCubeMapNegZPath(useImageKeysInsteadPath));
		}
		if (mod.isEnabled(vwgl.MaterialModifier.kCastShadows)) mod.setCastShadows(this.getCastShadows());
		if (mod.isEnabled(vwgl.MaterialModifier.kReceiveShadows)) mod.setReceiveShadows(this.getReceiveShadows());
		if (mod.isEnabled(vwgl.MaterialModifier.kReceiveProjections)) mod.setReceiveProjections(this.getReceiveProjections());
		if (mod.isEnabled(vwgl.MaterialModifier.kAlphaCutoff)) mod.setAlphaCutoff(this.getAlphaCutoff());
		if (mod.isEnabled(vwgl.MaterialModifier.kShininessMask))
			mod.setShininessMask(this.getShininessMaskPath(),this.getShininessMaskChannel(),this.getInvertShininessMask());
		if (mod.isEnabled(vwgl.MaterialModifier.kLightEmissionMask))
			mod.setLightEmissionMask(this.getLightEmissionMaskPath(),this.getLightEmissionMaskChannel(),this.getInvertLightEmissionMask());

		return mod;
	}
});

// Use this function to get the GenericMaterial resource list array
//vwgl.GenericMaterial.getResourceList = function() {
//	return ["generic.vsh","generic.fsh"];
//}

// Use this function to load the GenericMaterial resources
//vwgl.GenericMaterial.loadResources = function(done) {
//	var resources = vwgl.GenericMaterial.getResourceList();
//	loader.loadResourceList(resources,function(loaded,errors) {
//		if (errors!=0) {
//			base.debug.log("There were errors loading generic material shaders");
//			if (typeof(done)=='function') done(false);
//		}
//		else {
//			if (typeof(done)=='function') done(true);
//		}
//	});
//}

// To use this function, ensure to previously load the resources using the above function, or manually adding the generic material resources
// to you load resource list using vwgl.GenericMaterial.getResourceList()
vwgl.GenericMaterial.setMaterialsWithJson = function(/* Drawable */ drawable, jsonStringOrObj, /* string | null */ currentDir) {
	if (!currentDir) currentDir = "";

	var json = null;
	if (typeof(jsonStringOrObj)=='string') {
		json = JSON.parse(jsonStringOrObj);
	}
	else {
		json = jsonStringOrObj;
	}

	var materials = {};
	for (var i=0;i<json.length;++i) {
		var materialItem = json[i];
		if (typeof(materialItem)=="object") {
			var name = materialItem["name"];
			materials[name] = materialItem;
		}
	}

	var solidList = drawable.getSolidList();
	for (var i=0;i<solidList.length;++i) {
		var s = solidList[i];
		var p = s.getPolyList();
		var matJson = materials[p.getMaterialName()];
		vwgl.GenericMaterial.applyJson(s, matJson, currentDir);
	}

}

$gmUtils = {}
$gmUtils.val = function(value,defaultVal) {
	return value!==undefined ? value:defaultVal;
}

$gmUtils.tex = function(value,path) {
	var tex = null;

	if (typeof(value)=="string") {
		var fileName = value;
		if (fileName[0]!='/') fileName = path + fileName;
		tex = vwgl.TextureManager.loadTextureDeferred(fileName);
	}

	return tex;
}

vwgl.GenericMaterial.applyJson = function(solid,matJson,path,done) {
	var mat = new vwgl.GenericMaterial();
	solid.setMaterial(mat);
	if (typeof(matJson)=='object') {
		var diffuseColor = new vwgl.Vector4($gmUtils.val(matJson.colorTintR,1),
											$gmUtils.val(matJson.colorTintG,1),
											$gmUtils.val(matJson.colorTintB,1),
											$gmUtils.val(matJson.colorTintA,1));

		diffuseColor 	 = new vwgl.Vector4($gmUtils.val(matJson.diffuseR,diffuseColor.r()),
											$gmUtils.val(matJson.diffuseG,diffuseColor.g()),
											$gmUtils.val(matJson.diffuseB,diffuseColor.b()),
											$gmUtils.val(matJson.diffuseA,diffuseColor.a()));

		mat.setDiffuse(diffuseColor);
		mat.setShininess($gmUtils.val(matJson["shininess"],0));

		vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
		mat.setTexture($gmUtils.tex(matJson["texture"],path));
		vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinear);
		mat.setLightMap($gmUtils.tex(matJson["lightmap"],path));
		vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinearMipmapLinear);
		mat.setNormalMap($gmUtils.tex(matJson["normalMap"],path));
		vwgl.TextureManager.setMinFilter(vwgl.Texture.kFilterLinear);

		// castShadows
		mat.setCastShadows($gmUtils.val(matJson["castShadows"],true));
		mat.setReceiveShadows($gmUtils.val(matJson["receiveShadows"],true));
		mat.setReceiveProjections($gmUtils.val(matJson["receiveProjections"],false));

		// receiveShadows
		mat.setRefractionAmount($gmUtils.val(matJson["refractionAmount"],0));
		mat.setLightEmission($gmUtils.val(matJson["lightEmission"],0));

		var offx   = $gmUtils.val(matJson["textureOffsetX"],0);
		var offy   = $gmUtils.val(matJson["textureOffsetY"],0);
		var scalex = $gmUtils.val(matJson["textureScaleX"],1);
		var scaley = $gmUtils.val(matJson["textureScaleY"],1);
		mat.setTextureOffset(new vwgl.Vector2(offx,offy));
		mat.setTextureScale(new vwgl.Vector2(scalex,scaley));

		offx   = $gmUtils.val(matJson["lightmapOffsetX"],0);
		offy   = $gmUtils.val(matJson["lightmapOffsetY"],0);
		scalex = $gmUtils.val(matJson["lightmapScaleX"],1);
		scaley = $gmUtils.val(matJson["lightmapScaleY"],1);
		mat.setLightmapOffset(new vwgl.Vector2(offx,offy));
		mat.setLightmapScale(new vwgl.Vector2(scalex,scaley));

		offx   = $gmUtils.val(matJson["normalMapOffsetX"],0);
		offy   = $gmUtils.val(matJson["normalMapOffsetY"],0);
		scalex = $gmUtils.val(matJson["normalMapScaleX"],1);
		scaley = $gmUtils.val(matJson["normalMapScaleY"],1);
		mat.setNormalMapOffset(new vwgl.Vector2(offx,offy));
		mat.setNormalMapScale(new vwgl.Vector2(scalex,scaley));

		mat.setReflectionAmount($gmUtils.val(matJson["reflectionAmount"],0));
		
		
		mat.setAlphaCutoff($gmUtils.val(matJson["alphaCutoff"], 0.5));
		
		var shininessMaskChannel = $gmUtils.val(matJson["shininessMaskChannel"],0);
		var invertShininessMask = $gmUtils.val(matJson["invertShininessMask"],false);
		var shininessMaskText = $gmUtils.tex(matJson["shininessMask"],path);
		mat.setShininessMask(shininessMaskText,shininessMaskChannel,invertShininessMask);

		var lightEmissionMaskChannel = $gmUtils.val(matJson["lightEmissionMaskChannel"],0);
		var invertLightEmissionMask = $gmUtils.val(matJson["invertLightEmissionMask"],false);
		var lightEmissionMask = $gmUtils.tex(matJson["lightEmissionMask"],path);
		mat.setLightEmissionMask(lightEmissionMask, lightEmissionMaskChannel, invertLightEmissionMask);

		// General properties
		mat.setCullFace($gmUtils.val(matJson["cullFace"],true));
	}
}

vwgl.GenericMaterial.kDiffuseColor			= 1 << 0;
vwgl.GenericMaterial.kSpecularColor			= 1 << 1;
vwgl.GenericMaterial.kShininess				= 1 << 2;
vwgl.GenericMaterial.kRefractionAmount		= 1 << 3;
vwgl.GenericMaterial.kLightEmission			= 1 << 4;
vwgl.GenericMaterial.kTexture				= 1 << 5;
vwgl.GenericMaterial.kLightMap				= 1 << 6;
vwgl.GenericMaterial.kNormalMap				= 1 << 7;
vwgl.GenericMaterial.kTextureOffset			= 1 << 8;
vwgl.GenericMaterial.kTextureScale			= 1 << 9;
vwgl.GenericMaterial.kLightmapOffset		= 1 << 10;
vwgl.GenericMaterial.kLightmapScacle		= 1 << 11;
vwgl.GenericMaterial.kCastShadows			= 1 << 12;
vwgl.GenericMaterial.kReceiveShadows		= 1 << 13;
vwgl.GenericMaterial.kSelectedMode			= 1 << 14;
vwgl.GenericMaterial.kAlphaCutoff			= 1 << 14;
vwgl.GenericMaterial.kShininessMask			= 1 << 15;
vwgl.GenericMaterial.kLightEmissionMask		= 1 << 16;

