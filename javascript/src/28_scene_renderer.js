Class ("vwgl.SceneRenderer", {
	_drawVisitor:null,		// vwgl.DrawNodeVisitor
	_renderTransparency:true,
	_renderOpaque:true,

	initialize:function() {
		this._drawVisitor = new vwgl.DrawNodeVisitor();
	},

	draw:function(/* Group */ sceneRoot, renderAlpha) {
		if (renderAlpha) {
			if (this._renderOpaque) {
				this._drawVisitor.setRenderTransparent(false);
				this._drawVisitor.setRenderOpaque(true);
				this._drawVisitor.visit(sceneRoot);
			}
	
			if (this._renderTransparency) {
				gl.enable(gl.BLEND);
				gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
				this._drawVisitor.setRenderTransparent(true);
				this._drawVisitor.setRenderOpaque(false);
				this._drawVisitor.visit(sceneRoot);
				gl.disable(gl.BLEND);
			}
		}
		else {
			this._drawVisitor.setRenderTransparent(true);
			this._drawVisitor.setRenderOpaque(true);
			this._drawVisitor.visit(sceneRoot);
		}
	},
	
	getNodeVisitor:function() { return this._drawVisitor; },
	
	setRenderTransparency:function(r) { this._renderTransparency = r; },
	getRenderTransparency:function() { return this._renderTransparency; },
	
	setRenderOpaque:function(r) { this._renderOpaque = r; },
	getRenderOpaque:function() { return this._renderOpaque; },
	
	destroy:function() {
		
	}

});
