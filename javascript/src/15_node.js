
Class ("vwgl.Node", {
	_parent:null,
	_initialized:false,
	_enabled:true,
	_name:"",	// string

	initialize:function() {

	},

	init:function() {},
	isInitialized:function() { return this._initialized; },
	setInitialized:function(i) { this._initialized = i; },
	
	setEnabled:function(/* bool */ enabled) { this._enabled = enabled; },
	isEnabled:function() { return this._enabled; },

	getParent:function() { return this._parent; },

	destroy:function() {},

	update:function() {},

	setName:function(name) { this._name = name; },
	getName:function() { return this._name; },
	
	branchEnabled:function() {
		if (this.parent==null) {
			return this._enabled;
		}
		else {
			return this._enabled && this._parent.branchEnabled();
		}
	}
});

Class ("vwgl.Group", vwgl.Node, {
	_children:null,

	initialize:function() {
		this.parent();
		this._children = [];
	},

	addChild:function(/* Node */ child) {
		if (child._parent==this) return;
		if (child._parent) child._parent.removeChild(child);
		this._children.push(child);
		child._parent = this;
	},

	removeChild:function(/* Node */ child) {
		var i = this._children.indexOf(child);
		if (i>-1) {
			this._children.splice(i,1);
			child._parent = null;
		}
	},

	getNodeList:function() { return this._children; },

	destroy:function() {
		for (var i=0; i<this._children.length; ++i) {
			this._children[i].destroy();
		}
		this._children = [];
		this.parent();
	},

	haveChild:function(/* Node */ node) {
		return this._children && this._children.indexOf(node)!=-1;
	},

	nextChildOf:function(child) {
		var childIndex = -1;
		if (this._children && (childIndex=this._children.indexOf(child))!=-1) {
			if (childIndex==this._children.length-1) return null;
			return this._children[childIndex+1];
		}
		return null;
	},

	prevChildOf:function(child) {
		var childIndex = -1;
		if (this._children && (childIndex=this._children.indexOf(child))!=-1) {
			if (childIndex==0) return null;
			return this._children[childIndex-1];
		}
		return null;
	},

	swapChildren:function(childA,childB) {
		if (!this._children) return false;
		var childAIndex = this._children.indexOf(childA);
		var childBIndex = this._children.indexOf(childB);
		if (childAIndex!=-1 && childBIndex!=-1 && childA!=childB) {
			this._children[childAIndex] = childB;
			this._children[childBIndex] = childA;
			return true;
		}
		return false;
	},

	moveChildNextTo:function(/* Node */ nextToNode, /* Node */ movingNode) {
		if (!this._children) return false;
		var aIndex = this._children.indexOf(nextToNode);
		var bIndex = this._children.indexOf(movingNode);
		if (nextToNode!=movingNode && aIndex!=-1 && bIndex!=-1) {
			if (aIndex==this._children.length-1) {
				this._children.splice(bIndex,1);
				this._children.push(movingNode);
			}
			else {
				this._children.splice(bIndex,1);
				this._children.splice(aIndex+1,0,movingNode);
			}
			return true;
		}
		return false;
	}
});

Class ("vwgl.DrawableNode", vwgl.Node, {
	_drawable:null,		// vwgl.Drawable

	initialize:function(drawable) {
		this.parent();
		if (drawable) {
			this.setDrawable(drawable);
		}
	},

	setDrawable:function(/* Drawable */ drawable) {
		if (drawable && drawable._parent==this) return;
		if (drawable && drawable._parent) drawable._parent.setDrawable(null);
		if (this._drawable) this._drawable._parent = null;
		this._drawable = drawable;
		if (this._drawable) this._drawable._parent = this;
	},

	getDrawable:function() { return this._drawable; },

	init:function() {
		if (this._drawable) this._drawable.init();
	},

	destroy:function() {
	}
});

Class ("vwgl.TransformNode", vwgl.Group, {
	_transform:null,	// vwgl.Matrix4

	initialize:function(/* Matrix4 | null */ t) {
		this.parent();
		if (t) {
			this._transform = new vwgl.Matrix4(t);
		}
		else {
			this._transform = vwgl.Matrix4.makeIdentity();
		}
	},

	setTransform:function(/* Matrix4 */ t) { this._transform.assign(t); },
	getTransform:function() { return this._transform; }
});

Class ("vwgl.DockNode", vwgl.Group, {

	update:function() {
		var transform = vwgl.Matrix4.makeIdentity();
		for (var i=0; i<this.getNodeList().length; ++i) {
			var node = this.getNodeList()[i];
			var trx = dynamic_cast("vwgl.TransformNode",node);
			var drw = this.getDrawable(trx);
			if (drw) {
				if (drw.getInputJoint()) {
					drw.getInputJoint().applyTransform(transform);
					trx.setTransform(transform);
				}
				if (drw.getOutputJoint()) {
					drw.getOutputJoint().applyTransform(transform);
				}
			}
		}
	},

	getDrawable:function(/* vwgl.TransformNode */ trx) {
		var drawable = null;
		if (trx) {
			for (var i=0; i<trx.getNodeList().length; ++i) {
				var node = trx.getNodeList()[i];
				var drw = dynamic_cast("vwgl.DrawableNode",node);
				if (drw) {
					drawable = drw.getDrawable();
					break;
				}
			}
		}
		return drawable;
	}
});
