
Class ("vwgl.scene.ILifeCycle", {
	init:function() {},
	update:function() {},
	frame:function(/* number */ delta) {},
	willDraw:function() {},
	draw:function(/* Material */ mat, /* bool */ useMaterialSettings) {},
	didDraw:function() {}
});

Class ("vwgl.scene.Component", vwgl.scene.ILifeCycle, {
	_sceneObject:null,

	sceneObject:function() { return this._sceneObject; },
	node:function() { return dynamic_cast("vwgl.scene.Node",this._sceneObject); }
});

Class ("vwgl.scene.Transform", vwgl.scene.Component, {
	_transform:null,	// vwgl.Transform

	initialize:function(/* Matrix4 | vwgl.TransformStrategy */ aParam) {
		this._transform = new vwgl.Transform();
		if (dynamic_cast("vwgl.Matrix4",aParam)) {
			this._transform.setMatrix(aParam);
		}
		else if (dynamic_cast("vwgl.TransformStrategy",aParam)) {
			this._transform.setTransformStrategy(aParam);
		}
	},

	willDraw:function() {
		if (this.sceneObject() && this.sceneObject().isEnabled()) {
			vwgl.State.get().pushModelMatrix();
			vwgl.State.get().modelMatrix().mult(this._transform.getMatrix());
		}
	},

	didDraw:function() {
		if (this.sceneObject() && this.sceneObject().isEnabled()) {
			vwgl.State.get().popModelMatrix();
		}
	}
});

var DrawableElementException = "Drawable element exception";
var DrawableIndexOutOfBoundsException = "Drawable element index out of bounds";
var DrawableElementNotFoundException = "Drawable element not found";
var DrawableInvalidParameterException = "Invalid parameter supplied to drawable object";

Class ("vwgl.scene.Drawable", vwgl.scene.Component, {
	_polyList:null,			// vwgl.PolyList[]
    _material:null,			// vwgl.Material[]
    _transform:null,		// vwgl.Transform[]
    _renderFlags:null,		// kRenderTransparent | kRenderOpaque
	_observerVector:null,	// drawableObserver[]

	initialize:function() {
	},

	addPolyList:function(/* PolyList */ plist, /* Material */ mat, /* Transform */ trx) {
		if (!mat) {
		}
		if (!trx) {
		}
	},

	getPolyListVector:function() { return this._polyList; },

	removePolyList:function(/* PolyList */ plist) {
		throw DrawableElementNotFoundException;
	},

	removePolyList:function(/* int */ index) {
		throw DrawableIndexOutOfBoundsException;
	},

	getPolyListIndex:function(/* PolyList */ plist) {
		throw DrawableElementNotFoundException;
	},

	setPolyList:function(/* int */ index, /* PolyList */ plist) {
		throw DrawableIndexOutOfBoundsException;
	},

	setMaterial:function(/* int */ index, /* Material */ mat) {
		throw DrawableIndexOutOfBoundsException;
	},

	setTransform:function(/* int */ index, /* Transform */ trx) {
		throw DrawableIndexOutOfBoundsException;
	},

	getPolyList:function(/* int */ index) {
		throw DrawableIndexOutOfBoundsException;
	},

	getMaterial:function(/* int */ index) {
		throw DrawableIndexOutOfBoundsException;
	},

	getTransform:function(/* int */ index) {
		throw DrawableIndexOutOfBoundsException;
	},

	getMaterial:function(/* PolyList */ plist) {
		throw DrawableElementNotFoundException;
	},

	getTransform:function(/* PolyList */ plist) {
		throw DrawableElementNotFoundException;
	},

	registerObserver:function(/* IDrawableObserver */ observer) {

	},

	unregisterObserver:function(/* IDrawableObserver */ observer) {

	},

	eachElement:function(callback) {
		for (var i = 0; i < this._polyList.length; ++i) {
			callback(this._polyList[i], this._material[i], this._transform[i]);
		}
	},

	eachObserver:function(callback) {
		for (var i = 0; i < this._observerVector.length; ++i) {
			callback(this._observerVector[i]);
		}
	},

	draw:function(/* Material */ forcedMaterial, /* bool */ useMaterialSettings) {
		var This = this;
		if (this.sceneObject() && !this.sceneObject().isEnabled()) return;

    	this.eachElement(function(polyList, plistMaterial, transform) {
    		if (polyList.isVisible()) {
    			vwgl.State.get().pushModelMatrix();
    			vwgl.State.get().modelMatrix().mult(transform.getMatrix());

    			var currentMaterial = forcedMaterial ? forcedMaterial : plistMaterial;
    			var isTransparent = currentMaterial.isTransparent();
    			if (forcedMaterial && useMaterialSettings) {
    				forcedMaterial.useSettingsOf(plistMaterial);
    				isTransparent = plistMaterial.isTransparent();
    			}

    			if ((This.isRenderFlagEnabled(vwgl.Drawable.kRenderTransparent) && isTransparent) ||
    				(This.isRenderFlagEnabled(vwgl.Drawable.kRenderOpaque) && !isTransparent)) {
    				currentMaterial.bindPolyList(polyList);
    				currentMaterial.activate();

    				polyList.drawElements();
    				currentMaterial.deactivate();
    			}

    			if (forcedMaterial) {
    				forcedMaterial.useSettingsOf(null);
    			}
    			vwgl.State.get().popModelMatrix();
    		}
    	});
	},

	enableRenderFlag:function(/* RenderFlag */ flag) { this._renderFlags = this._renderFlags | flag; },
	disableRenderFlag:function(/* RenderFlag */ flag) { this._renderFlags = this._renderFlags & ~flag; },
	isRenderFlagEnabled:function(/* RenderFlag */ flag) { return (this._renderFlags & flag) != 0; },
	getRenderFlags:function() { return this._renderFlags; },
	setRenderFlags:function(flags) { this._renderFlags = flags; },

	setCubemap:function(/* vwgl.CubeMap */ cm) {
		this.eachElement(function(polyList, material, trx) {
    		var mat = dynamic_cast("vwgl.GenericMaterial",material);
    		if (mat) {
    			mat.setCubeMap(cm);
    		}
    	});
	},

	applyTransform:function(/* vwgl.Matrix4 */ transform) {
		this.eachElement(function(polyList, material, trx) {
    		polyList.applyTransform(transform);
    	});

    	this.eachObserver(function(observer) {
    		observer.applyMatrix(transform);
    	});
	},

	hideGroup:function(groupName) {
		this.eachElement(function(plist, mat, trx) {
    		if (plist.getGroupName() == groupName) {
    			plist.hide();
    		}
    	});
    	this.setDirty();
	},

	showGroup:function(groupName) {
		this.eachElement(function(plist, mat, trx) {
    		if (plist.getGroupName() == groupName) {
    			plist.show();
    		}
    	});
    	this.setDirty();

	},

	showByName:function(name) {
		this.eachElement(function(plist, mat, trx) {
    		if (plist.getName()==name) {
    			plist.show();
    		}
    	});
    	this.setDirty();
	},

	hideByName:function(name) {
		this.eachElement(function(plist, mat, trx) {
    		if (plist.getName() == name) {
    			plist.hide();
    		}
    	});
    	this.setDirty();
	},


	setDirty:function() {
		var This = this;
		this.eachObserver(function(observer) {
			observer.drawableChanged(This);
		});
	},
});

vwgl.scene.Drawable.kRenderTransparent	= 1 << 0;
vwgl.scene.Drawable.kRenderOpaque		= 1 << 1;

Class ("vwgl.scene.DrawableFactory", {
	getDrawable:function() {}
});

