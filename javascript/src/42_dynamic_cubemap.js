
Class ("vwgl.DynamicCubemapMaterial", vwgl.DeferredMaterial, {
	initialize:function() {
		this.initShaderSync(null);
	},
	
	getResourceList:function() { return []; },
	
	initShaderSync:function(loader) {
		if (this._initialized) return;
		this._initialized = true;
		this.getShader().attachShader(vwgl.Shader.kTypeVertex, vwgl.DynamicCubemapMaterial.shaders.vertex);
		this.getShader().attachShader(vwgl.Shader.kTypeFragment, vwgl.DynamicCubemapMaterial.shaders.fragment);
		this.getShader().link("def_texture");
		
		this.loadVertexAttrib("aVertexPosition");
		this.loadTexCoord0Attrib("aTexturePosition");
		
		this.getShader().initUniformLocation("uMVMatrix");
		this.getShader().initUniformLocation("uPMatrix");
		
		this.getShader().initUniformLocation("uTexture");
		this.getShader().initUniformLocation("uColorTint");
		this.getShader().initUniformLocation("uIsEnabled");
		this.getShader().initUniformLocation("uTextureOffset");
		this.getShader().initUniformLocation("uTextureScale");
	},

	setupUniforms:function() {
		this.getShader().setUniform("uMVMatrix", this.modelViewMatrix());
		this.getShader().setUniform("uPMatrix", this.projectionMatrix());
		
		var mat = this.getSettingsMaterial();
		if (mat) {
			this.getShader().setUniform("uTexture", mat.getTexture(),vwgl.Texture.kTexture0);
			this.getShader().setUniform1i("uIsEnabled", mat.getTexture()!=null);
			this.getShader().setUniform("uColorTint", mat.getDiffuse());
			this.getShader().setUniform("uTextureOffset", mat.getTextureOffset());
			this.getShader().setUniform("uTextureScale", mat.getTextureScale());
		}
	}

});

vwgl.DynamicCubemapMaterial.shaders = {
	vertex:"\
		#ifdef GL_ES\n\
		precision highp float;\n\
		#endif\n\
		attribute vec3 aVertexPosition;\n\
		attribute vec2 aTexturePosition;\n\
		\n\
		uniform mat4 uMVMatrix;\n\
		uniform mat4 uPMatrix;\n\
		\n\
		varying vec2 vTexturePosition;\n\
		varying vec4 vPosition;\n\
		\n\
		void main() {\n\
			vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
			gl_Position = vPosition;\n\
			vTexturePosition = aTexturePosition;\n\
		}\n\
		",
	
	fragment:"\
		#ifdef GL_ES\n\
		precision highp float;\n\
		#endif\n\
		varying vec2 vTexturePosition;\n\
		varying vec4 vPosition;\n\
		\n\
		uniform sampler2D uTexture;\n\
		uniform vec4 uColorTint;\n\
		uniform bool uIsEnabled;\n\
		\n\
		uniform vec2 uTextureOffset;\n\
		uniform vec2 uTextureScale;\n\
		\n\
		void main(void) {\n\
			vec4 texColor = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset);\n\
			if (!uIsEnabled) {\n\
				texColor = vec4(1.0);\n\
			}\n\
			\n\
			gl_FragColor = texColor * uColorTint;\n\
		}"
};

Class ("vwgl.DynamicCubemap", vwgl.Node, {
	_fbo:null,			// vwgl.FramebufferObject
	_trxVisitor:null,	// vwgl.TransformVisitor
	_drawVisitor:null,	// vwgl.DrawNodeVisitor
	_clearColor:null,	// vwgl.Color
	
	initialize:function() {
		this._trxVisitor = new vwgl.TransformVisitor();
		this._drawVisitor = new vwgl.DrawNodeVisitor();
		this._clearColor = new vwgl.Color.black();
		vwgl.DynamicCubemapManager.get()._addCubemap(this);
	},

	createCubemap:function(/* vwgl.Size2D */ size) {
		this._fbo = new vwgl.FramebufferObject();
		this._fbo.create(size.width(), size.height(),vwgl.FramebufferObject.kCubeMap);
		this._drawVisitor.setMaterial(new vwgl.DynamicCubemapMaterial());
		this._drawVisitor.setUseMaterialSettings(true);
	},

	getCubeMap:function() { return this._fbo.getCubeMap(); },
	
	beginDraw:function() {
		if (this._fbo) this._fbo.bind();
	},

	drawToFace:function(target) {
		if (!this._fbo) return;
		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, vwgl.Texture.getGLTarget(target), this._fbo.getTexture().getTextureName(), null);

		vwgl.State.get().setViewport(new vwgl.Viewport(0,0,this._fbo.getWidth(),this._fbo.getHeight()));

		vwgl.State.get().projectionMatrix().assign(vwgl.Matrix4.makePerspective(90.0, 1, 0.01, 100.0));

		switch (target) {
			case vwgl.Texture.kTexturePositiveXFace:
				vwgl.State.get().viewMatrix().lookAt(new vwgl.Vector3(0.0), new vwgl.Vector3(1.0,0.0,0.0), new vwgl.Vector3(0.0,1.0,0.0));
				break;
			case vwgl.Texture.kTextureNegativeXFace:
				vwgl.State.get().viewMatrix().lookAt(new vwgl.Vector3(0.0), new vwgl.Vector3(-1.0,0.0,0.0), new vwgl.Vector3(0.0,1.0,0.0));
				break;
			case vwgl.Texture.kTexturePositiveYFace:
				vwgl.State.get().viewMatrix().lookAt(new vwgl.Vector3(0.0), new vwgl.Vector3(0.0,-10.0,0.0), new vwgl.Vector3(0.0,0.0,-1.0));
				break;
			case vwgl.Texture.kTextureNegativeYFace:
				vwgl.State.get().viewMatrix().lookAt(new vwgl.Vector3(0.0), new vwgl.Vector3(0.0,10.0,0.0), new vwgl.Vector3(0.0,0.0,-1.0));
				break;
			case vwgl.Texture.kTexturePositiveZFace:
				vwgl.State.get().viewMatrix().lookAt(new vwgl.Vector3(0.0), new vwgl.Vector3(0.0,0.0,-10.0), new vwgl.Vector3(0.0,1.0,0.0));
				break;
			case vwgl.Texture.kTextureNegativeZFace:
				vwgl.State.get().viewMatrix().lookAt(new vwgl.Vector3(0.0), new vwgl.Vector3(0.0,0.0,10.0), new vwgl.Vector3(0.0,1.0,0.0));
				break;
			default:
				break;
		}

		gl.clearColor(this._clearColor.r(), this._clearColor.g(), this._clearColor.b(), this._clearColor.a());
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		this._trxVisitor.visit(this);
		var pos = this._trxVisitor.getTransform().getPosition();

		vwgl.State.get().viewMatrix().translate(-pos.x(), -pos.y(), -pos.z());
	},

	endDraw:function() {
		if (this._fbo) this._fbo.unbind();
	},

	// You can use this function directly instead the three previous functions
	drawScene:function(/* vwgl.Node */ sceneRoot) {
		if (this._fbo) {
			this.beginDraw();
			this.drawToFace(vwgl.Texture.kTexturePositiveXFace);
			this._drawVisitor.visit(sceneRoot);
	
			this.drawToFace(vwgl.Texture.kTextureNegativeXFace);
			this._drawVisitor.visit(sceneRoot);
	
			this.drawToFace(vwgl.Texture.kTexturePositiveYFace);
			this._drawVisitor.visit(sceneRoot);
	
			this.drawToFace(vwgl.Texture.kTextureNegativeYFace);
			this._drawVisitor.visit(sceneRoot);
	
			this.drawToFace(vwgl.Texture.kTexturePositiveZFace);
			this._drawVisitor.visit(sceneRoot);
	
			this.drawToFace(vwgl.Texture.kTextureNegativeZFace);
			this._drawVisitor.visit(sceneRoot);
			this.endDraw();
		}
	},
	
	setClearColor:function(/* vwgl.Color */ color) { this._clearColor = color; },
	getClearColor:function() { return this._clearColor; },
	
	destroy:function() {
		if (this._fbo) this._fbo.destroy();
		this._fbo = [];
		vwgl.DynamicCubemapManager._removeCubemap(this);
	}

});

Class ("vwgl.DynamicCubemapManager", {
	_cubeMapList:null,			// vwgl.DynamicCubemap []
	_updateIterator:0,
	_frameCount:0,
	
	initialize:function() {
		this._cubeMapList = [];
		this._updateIterator = 0;
	},
	
	update:function(/* Node */ sceneRoot) {
		if (this._frameCount%10==0) this.updateCubemaps();
		if (this._cubeMapList.length>0 && this._updateIterator<this._cubeMapList.length && sceneRoot) {
			this._cubeMapList[this._updateIterator].drawScene(sceneRoot);
			++this._updateIterator;
		}
		++this._frameCount;
	},
	
	updateCubemaps:function() { this._updateIterator = 0; },
	
	updateAllCubemaps:function(sceneRoot) {
		for (var i=0;i<this._cubeMapList.length && sceneRoot;++i) {
			this._cubeMapList[i].drawScene(sceneRoot);
		}
		this._updateIterator = this._cubeMapList.length;
	},

	_addCubemap:function(/* DynamicCubemap */ cm) {
		this._cubeMapList.push(cm);
		this.updateCubemaps();
	},

	_removeCubemap:function(/* DynamicCubemap */ cm) {
		var index = this._cubeMapList.indexOf(cm);
		if (index>-1) this._cubeMapList.splice(index,1);
		this.updateCubemaps();
	}
});

vwgl.DynamicCubemapManager.s_singleton = null;
vwgl.DynamicCubemapManager.get = function() {
	if (!vwgl.DynamicCubemapManager.s_singleton) {
		vwgl.DynamicCubemapManager.s_singleton = new vwgl.DynamicCubemapManager();
	}
	return vwgl.DynamicCubemapManager.s_singleton;
}

vwgl.DynamicCubemapManager.s_defaultCubeMapSize = new vwgl.Size2D(512);
vwgl.DynamicCubemapManager.defaultCubeMapSize = function() {
	return vwgl.DynamicCubemapManager.s_defaultCubeMapSize;
}
vwgl.DynamicCubemapManager.setDefaultCubeMapSize = function(/* Size2Di */ s) {
	vwgl.DynamicCubemapManager.s_defaultCubeMapSize.assign(s);
}
