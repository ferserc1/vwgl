Class ("vwgl.NodeManipulator", {
	_transformNode:null,	// vwgl.TransformNode

	initialize:function(/* TransformNode */ node) {
		this._transformNode = node;
	},

	setTransform:function() {},

	getTransformNode:function() { return this._transformNode; }

});


Class ("vwgl.TargetNodeManipulator", vwgl.NodeManipulator, {
	_yaw:0,			// float
	_pitch:0,		// float
	_distance:0,	// float
	_center:null,	// vwgl.Vector3
	_minDistance:0,	// float
	_maxDistance:0,	// float
	_minPitch:0,	// float
	_maxPitch:0,	// float
	_centerBounds:null,	// vwgl.Bounds

	initialize:function(/* vwgl::TransformNode */ node) {
		this.parent(node);
		this._yaw = 0.0;
		this._pitch = 0.0;
		this._distance = 10.0;
		this._minDistance = 0.1;
		this._maxDistance = 50.0;
		this._minPitch = vwgl.Math.kPiOver2 * -1.0;
		this._maxPitch = 0.0;
		this._center = new vwgl.Vector3(0);
		this._centerBounds = new vwgl.Bounds();
	},

	setTransform:function() {
		if (this._yaw>vwgl.Math.k2Pi) this._yaw = this._yaw - vwgl.Math.k2Pi;
		if (this._yaw<0) this._yaw = vwgl.Math.k2Pi - this._yaw;

		if (this._pitch<this._minPitch) this._pitch = this._minPitch;
		if (this._pitch>this._maxPitch) this._pitch = this._maxPitch;
		if (this._distance<this._minDistance) this._distance = this._minDistance;
		if (this._distance>this._maxDistance) this._distance = this._maxDistance;
		var node = this.getTransformNode();
		node.getTransform().identity();
		node.getTransform().translate(this._center);
		node.getTransform().rotate(this._yaw, 0.0, 1.0, 0.0);
		node.getTransform().rotate(this._pitch, 1.0, 0.0, 0.0);
		node.getTransform().translate(0.0, 0.0, this._distance);
	},

	yaw:function() { return this._yaw; },
	pitch:function() { return this._pitch; },
	distance:function() { return this._distance; },
	center:function() { return this._center; },
	centerBounds:function() { return this._centerBounds; },
	maxDistance:function() { return this._maxDistance; },
	minDistance:function() { return this._minDistance; },
	setYaw:function(value) { this._yaw = value; },
	setPitch:function(value) { this._pitch = value; },
	setDistance:function(value) { this._distance = value; },
	setCenter:function(/* vwgl.Vector3 */ value) { this._center.assign(value); },
	setCenterBounds:function(/* vwgl.Bounds */ value) { this._centerBounds.assign(value); },
	setMaxDistance:function(value) { this._maxDistance = value; },
	setMinDistance:function(value) { this._minDistance = value; },

	moveForward:function(/* float */ increment) {
		var fw = new vwgl.Vector3(this.getTransformNode().getTransform().forwardVector().scale(increment));
		var newCenter = new vwgl.Vector3(this._center);
		newCenter.add(fw);
		if (this._centerBounds.isInBounds(newCenter)) {
			this._center.assign(newCenter);
		}
		this.setTransform();
	},

	moveBackward:function(/* float */ increment) {
		var bw = new vwgl.Vector3(this.getTransformNode().getTransform().backwardVector().scale(increment));
		var newCenter = new vwgl.Vector3(this._center);
		newCenter.add(bw);
		if (this._centerBounds.isInBounds(newCenter)) {
			this._center.assign(newCenter);
		}
		this.setTransform();
	},

	moveLeft:function(/* float */ increment) {
		var left = new vwgl.Vector3(this.getTransformNode().getTransform().leftVector().scale(increment));
		var newCenter = new vwgl.Vector3(this._center);
		newCenter.add(left);
		if (this._centerBounds.isInBounds(newCenter)) {
			this._center.assign(newCenter);
		}
		this.setTransform();
	},

	moveRight:function(/* float */ increment) {
		var right = new vwgl.Vector3(this.getTransformNode().getTransform().rightVector().scale(increment));
		var newCenter = new vwgl.Vector3(this._center);
		newCenter.add(right);
		if (this._centerBounds.isInBounds(newCenter)) {
			this._center.assign(newCenter);
		}
		this.setTransform();
	}
});

Class ("vwgl.MouseTargetManipulator", vwgl.TargetNodeManipulator, {
	_lastCoords:null,		// vwgl.Position2D
	_type:null,				// ManipulationType
	_scrollVelocity:0,
	_initZoomDistance:0,

	initialize:function(/* vwgl::TransformNode */ node) {
		this.parent(node);
		this._scrollVelocity = 0.1;
		this._lastCoords = new vwgl.Position2D();
	},

	mouseDown:function(/* vwgl::Position2D */ mousePos, /* ManipulationType */ type) {
		this._lastCoords.assign(mousePos);
		this._type = type;
	},

	mouseMove:function(/* vwgl.Position2Di */ mousePos) {
		if (this._type==vwgl.MouseTargetManipulator.kManipulationRotate) {
			this.setYaw(this.yaw() + (this._lastCoords.x() - mousePos.x()) * 0.01);
			this.setPitch(this.pitch() + (this._lastCoords.y() - mousePos.y()) * 0.01);
		}
		else if (this._type==vwgl.MouseTargetManipulator.kManipulationDrag) {
			var center = new vwgl.Vector3(this.center());
			if (this.pitch()<vwgl.Math.kPiOver4*-1.0) {
				center.x(this.center().x() + (this._lastCoords.x() - mousePos.x()) * 0.01 * vwgl.Math.cos(this.yaw()) +
					 	  (this._lastCoords.y() - mousePos.y()) * 0.01 * vwgl.Math.sin(this.yaw()));
				center.z(this.center().z() - (this._lastCoords.x() - mousePos.x()) * 0.01 * vwgl.Math.sin(this.yaw()) +
					 	  (this._lastCoords.y() - mousePos.y()) * 0.01 * vwgl.Math.cos(this.yaw()));
			}
			else {
				center.x(this.center().x() + (this._lastCoords.x() - mousePos.x()) * 0.01 * vwgl.Math.cos(this.yaw()));
				center.z(this.center().z() - (this._lastCoords.x() - mousePos.x()) * 0.01 * vwgl.Math.sin(this.yaw()));
				center.y(this.center().y() - (this._lastCoords.y() - mousePos.y()) * 0.01);
			}
			if (this._centerBounds.isInBounds(center)) {
				this.center().assign(center);
			}
		}
		else if (this._type==vwgl.MouseTargetManipulator.kManipulationZoom) {
			this.setDistance(this.distance() + (this._lastCoords.y() - mousePos.y()) * 0.1);
		}
		this.setTransform();
		this._lastCoords.assign(mousePos);
	},

	mouseWheel:function(/* vwgl::Vector2*/ delta) {
		this.setDistance(this.distance() + delta.y() * this._scrollVelocity);
		this.setTransform();
	},

	beginZoom:function(scale) {
		this._initZoomDistance = this.distance();
		this.setDistance(this.distance() / scale);
	},

	zoom:function(scale) {
		this.setDistance(this._initZoomDistance / scale);
		this.setTransform();
	}
});

vwgl.MouseTargetManipulator.kManipulationRotate = 0;
vwgl.MouseTargetManipulator.kManipulationDrag = 1;
vwgl.MouseTargetManipulator.kManipulationZoom = 2;
