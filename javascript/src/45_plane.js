
Class ("vwgl.physics.Plane", {
	_origin:null,	// vwgl.Vector3
	_normal:null,	// vwgl.Vector3
	
	// a = normal: create a plane with origin=(0,0,0) and normal=a
	// a = normal, b = origin: create a plane with origin=b and normal=a
	// a = p1, b = p2, c = p3: create a plane that contains the points p1, p2 and p3
	initialize:function(/* Vector3 */ a, /* Vector3 */ b, /* Vector3 */ c) {
		a = dynamic_cast("vwgl.Vector3",a);
		b = dynamic_cast("vwgl.Vector3",b);
		c = dynamic_cast("vwgl.Vector3",c);
		if (a && !b) {
			this._normal = new vwgl.Vector3(a);
			this._origin = new vwgl.Vector3(0);
		}
		if (a && b && !c) {
			this._normal = new vwgl.Vector3(a);
			this._origin = new vwgl.Vector3(b);
		}
		if (a && b && c) {
			var vec1 = new vwgl.Vector3(a); vec1.sub(b);
			var vec2 = new vegl.Vector3(c); vec2.sub(a);
			this._origin = new vwgl.Vector3(p1);
			this._normal = new vwgl.Vector3(vec1);
			this._normal.cross(vec2).normalize();
		}
		else {
			this._origin = new vwgl.Vector3(0);
			this._normal = new vwgl.Vector3(0,1,0);
		}
	},

	getNormal:function() { return this._normal; },
	setNormal:function(/* Vector3 */ n) { this._normal.assign(n); },

	getOrigin:function() { return this._origin; },
	setOrigin:function(/* Vector3 */ o) { this._origin.assign(o); },

	toString:function() {
		return "P0:" + this._origin.toString() + ", normal: " + this._normal.toString();
	},

	valid:function() { return !this._origin.isNan() && !this._normal.isNan(); },

	assign:function(/* vwgl.physics.Plane */ p) {
		this._origin.assign(p._origin);
		this._normal.assign(p._normal);
		return this;
	},
	
	equals:function(/* vwgl.physics.Plane */ p) {
		return this._origin.equals(p._origin) && this._normal.equals(p._normal);
	}
});
