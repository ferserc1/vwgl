
Class ("vwgl.scene.NodeVisitor", {
	visit:function(/* scene.Node */ node) {
	},

	didVisit:function(/* scene.Node */ node) {
	}
});

Class ("vwgl.scene.InitVisitor", vwgl.scene.NodeVisitor, {
	visit:function(node) {
		node.init();
	}
});

Class ("vwgl.scene.FrameVisitor", vwgl.scene.NodeVisitor, {
	_delta:0.0,		// float

	setDelta:function(d) { this._delta = d; },
	getDelta:function() { return this._delta; },

	visit:function(node) {
		node.frame(this._delta);
	}
});

Class ("vwgl.scene.DrawVisitor", vwgl.scene.NodeVisitor, {
	visit:function(node) {
		node.willDraw();
		node.draw();
	},

	didVisit:function(node) {
		node.didDraw();
	}
});

Class ("vwgl.scene.TransformVisitor", vwgl.scene.NodeVisitor, {
	_matrix:null,		// vwgl.Matrix4

	initialize:function() {
		this._matrix = vwgl.Matrix4.makeIdentity();
	},

	getMatrix:function() { return this._matrix; },

	visit:function(node) {
		var transform = node.getComponent(vwgl.scene.Transform);

		if (transform) {
			this._matrix.mult(transform.getTransform().getMatrix());
		}
	}
});