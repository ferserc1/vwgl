Class ("vwgl.Matrix3",{
	_m:null,
	
	initialize:function(a,b,c, d,e,f, g,h,i) {
		if (a===undefined) this.zero();
		else if (a!==undefined && b!==undefined && c!==undefined &&
				 d!==undefined && e!==undefined && f!==undefined &&
				 g!==undefined && h!==undefined && i!==undefined ) {
			 this._m = [ a,b,c, d,e,f, g,h,i ];
		}
		else this.assign(a,b,c);
	},
	
	zero:function() {
		this._m = [ 0,0,0, 0,0,0, 0,0,0, 0,0,0];
		return this;
	},

	identity:function() {
		this._m = [ 1,0,0, 0,1,0, 0,0,1 ];
		return this;
	},
	
	isZero:function() {
		return	this._m[0]==0.0 && this._m[1]==0.0 && this._m[2]==0.0 &&
				this._m[3]==0.0 && this._m[4]==0.0 && this._m[5]==0.0 &&
				this._m[6]==0.0 && this._m[7]==0.0 && this._m[8]==0.0;
	},
	
	isIdentity:function() {
		return	this._m[0]==1.0 && this._m[1]==0.0 && this._m[2]==0.0 &&
				this._m[3]==0.0 && this._m[4]==1.0 && this._m[5]==0.0 &&
				this._m[6]==0.0 && this._m[7]==0.0 && this._m[8]==1.0;
	},

	row:function(i) { return [this._m[i*3],this._m[i*3 + 1],this._m[i*3 + 2]]; },
	setRow:function(i, row) { this._m[i*3]=row._v[0]; this._m[i*3 + 1]=row._v[1]; this._m[i*3 + 2]=row._v[2]; return this; },
	set:function(s) { this.assign(s); return this; },
	
	raw:function() { return this._m; },
	// In c++ version this function is called 'length'.
	size:function() { return 9; },
	
	traspose:function() {
		var r0 = new vwgl.Vector3(this._m[0], this._m[3], this._m[6]);
		var r1 = new vwgl.Vector3(this._m[1], this._m[4], this._m[7]);
		var r2 = new vwgl.Vector3(this._m[2], this._m[5], this._m[8]);
		
		this.setRow(0, r0);
		this.setRow(1, r1);
		this.setRow(2, r2);

		return this;
	},
	
	
	// Operators
	//operator[]()
	elemAtIndex:function(i) { return this._m[i]; },
	
	//operator=()
	assign:function(a,b,c) {
		if (c===undefined) {
			if (typeof(a)=='object' && a.length>=9) {
				this._m = [ a[0],a[1],a[2], a[3],a[4],a[5], a[6],a[7],a[8] ];
			}
			else if (typeof(a)=='object' && a._m && a._m.lenght>=9) {
				this._m = [ a._m[0],a._m[1],a._m[2], a._m[3],a._m[4],a._m[5], a._m[6],a._m[7],a._m[8] ];
			}
			else {
				this._m = [ a,a,a, a,a,a, a,a,a ];
			}
		}
		else if (typeof(a)=='object' && a._v && a._v.length>=3 &&
				 typeof(b)=='object' && b._v && b._v.length>=3 &&
			 	 typeof(c)=='object' && c._v && c._v.length>=3 ) {
			 this._m = [ a.x(),a.y(),a.z(), b.x(), b.y(), b.z(), c.x(), c.y(), c.z() ];
		}
		else {
			this.zero();
		}
		return this;
	},
	
	//operator==(const Matrix3 & m) {
	equals:function(m) {
		return	this._m[0] == m._m[0] && this._m[1] == m._m[1]  && this._m[2] == m._m[2] &&
				this._m[3] == m._m[3] && this._m[4] == m._m[4]  && this._m[5] == m._m[5] &&
				this._m[6] == m._m[6] && this._m[7] == m._m[7]  && this._m[8] == m._m[8];
	},
	
	// operator!=
	notEquals:function(m) {
		return	this._m[0] != m._m[0] || this._m[1] != m._m[1]  || this._m[2] != m._m[2] &&
				this._m[3] != m._m[3] || this._m[4] != m._m[4]  || this._m[5] != m._m[5] &&
				this._m[6] != m._m[6] || this._m[7] != m._m[7]  || this._m[8] != m._m[8];
	},
		
	mult:function(a) {
		if (typeof(a)=="number") {
			this._m[0] *= a; this._m[1] *= a; this._m[2] *= a;
			this._m[3] *= a; this._m[4] *= a; this._m[5] *= a;
			this._m[6] *= a; this._m[7] *= a; this._m[8] *= a;
			return this;
		}

		var rm = this._m;
		var lm = a._m;
		
		var res = [];
		res.push(lm[0] * rm[0] + lm[1] * rm[1] + lm[2] * rm[2]);
		res.push(lm[0] * rm[1] + lm[1] * rm[4] + lm[2] * rm[7]);
		res.push(lm[0] * rm[2] + lm[1] * rm[5] + lm[2] * rm[8]);
		
		res.push(lm[3] * rm[0] + lm[4] * rm[3] + lm[5] * rm[6]);
		res.push(lm[3] * rm[1] + lm[4] * rm[4] + lm[5] * rm[7]);
		res.push(lm[3] * rm[2] + lm[4] * rm[5] + lm[5] * rm[8]);
		
		res.push(lm[6] * rm[0] + lm[7] * rm[3] + lm[8] * rm[6]);
		res.push(lm[6] * rm[1] + lm[7] * rm[4] + lm[8] * rm[7]);
		res.push(lm[6] * rm[2] + lm[7] * rm[5] + lm[8] * rm[8]);
		this._m = res;

		return this;
	},
	
	set00:function(v) { this._m[0] = v; return this; },
	set01:function(v) { this._m[1] = v; return this; },
	set02:function(v) { this._m[2] = v; return this; },
	
	set10:function(v) { this._m[3] = v; return this; },
	set11:function(v) { this._m[4] = v; return this; },
	set12:function(v) { this._m[5] = v; return this; },
	
	set20:function(v) { this._m[6] = v; return this; },
	set21:function(v) { this._m[7] = v; return this; },
	set22:function(v) { this._m[8] = v; return this; },
	
	get00:function() { return this._m[0]; },
	get01:function() { return this._m[1]; },
	get02:function() { return this._m[2]; },
	
	get10:function() { return this._m[3]; },
	get11:function() { return this._m[4]; },
	get12:function() { return this._m[5]; },
	
	get20:function() { return this._m[6]; },
	get21:function() { return this._m[7]; },
	get22:function() { return this._m[8]; },
	
	isNan:function() {
		return	!Math.isNaN(_m[0]) && !Math.isNaN(_m[1]) && !Math.isNaN(_m[2]) &&
				!Math.isNaN(_m[3]) && !Math.isNaN(_m[4]) && !Math.isNaN(_m[5]) &&
				!Math.isNaN(_m[6]) && !Math.isNaN(_m[7]) && !Math.isNaN(_m[8]);
	},

	toString:function() {
		return "[" + this._m[0] + ", " + this._m[1] + ", " + this._m[2] + "]\n" +
			   " [" + this._m[3] + ", " + this._m[4] + ", " + this._m[5] + "]\n" +
			   " [" + this._m[6] + ", " + this._m[7] + ", " + this._m[8] + "]";
	}
});

vwgl.Matrix3.makeIdentity = function() {
	return new vwgl.Matrix3(1,0,0, 0,1,0, 0,0,1);
}

Class ("vwgl.Matrix4", {
	_m:null,
	
	initialize:function(a,b,c,d, e,f,g,h, i,j,k,l, m,n,o,p) {
		if (a===undefined) this.zero();
		else if (a!==undefined && b!==undefined && c!==undefined && d!==undefined &&
				 e!==undefined && f!==undefined && g!==undefined && h!==undefined &&
				 i!==undefined && j!==undefined && k!==undefined && l!==undefined &&
				 m!==undefined && n!==undefined && o!==undefined && p!==undefined) {
			 this._m = [ a,b,c,d, e,f,g,h, i,j,k,l, m,n,o,p ];
		}
		else this.assign(a,b,c,d);
	},
	
	zero:function() {
		this._m = [ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 ];
		return this;
	},

	identity:function() {
		this._m = [ 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 ];
		return this;
	},
	
	isZero:function() {
		return	this._m[ 0]==0.0 && this._m[ 1]==0.0 && this._m[ 2]==0.0 && this._m[ 3]==0.0 &&
				this._m[ 4]==0.0 && this._m[ 5]==0.0 && this._m[ 6]==0.0 && this._m[ 7]==0.0 &&
				this._m[ 8]==0.0 && this._m[ 9]==0.0 && this._m[10]==0.0 && this._m[11]==0.0 &&
				this._m[12]==0.0 && this._m[13]==0.0 && this._m[14]==0.0 && this._m[15]==0.0;
	},
	
	isIdentity:function() {
		return	this._m[ 0]==1.0 && this._m[ 1]==0.0 && this._m[ 2]==0.0 && this._m[ 3]==0.0 &&
				this._m[ 4]==0.0 && this._m[ 5]==1.0 && this._m[ 6]==0.0 && this._m[ 7]==0.0 &&
				this._m[ 8]==0.0 && this._m[ 9]==0.0 && this._m[10]==1.0 && this._m[11]==0.0 &&
				this._m[12]==0.0 && this._m[13]==0.0 && this._m[14]==0.0 && this._m[15]==1.0;
	},

	row:function(i) { return [this._m[i*4],this._m[i*4 + 1],this._m[i*4 + 2],this._m[i*4 + 3]]; },
	setRow:function(i, row) { this._m[i*4]=row._v[0]; this._m[i*4 + 1]=row._v[1]; this._m[i*4 + 2]=row._v[2]; this._m[i*4 + 3]=row._v[3]; return this; },
	set:function(s) { this.assign(s); return this; },
	
	raw:function() { return this._m; },
	// In c++ version this function is called 'length'.
	size:function() { return 16; },
	
	getMatrix3:function() {
		return new vwgl.Matrix3(this._m[0], this._m[1], this._m[ 2],
								this._m[4], this._m[5], this._m[ 6],
								this._m[8], this._m[9], this._m[10]);
	},
	
	perspective:function(fovy, aspect, nearPlane, farPlane) {
		this.assign(vwgl.Matrix4.makePerspective(fovy, aspect, nearPlane, farPlane));
		return this;
	},
	
	frustum:function(left, right, bottom, top, nearPlane, farPlane) {
		this.assign(vwgl.Matrix4.makeFrustum(left, right, bottom, top, nearPlane, farPlane));
		return this;
	},
	
	ortho:function(left, right, bottom, top, nearPlane, farPlane) {
		this.assign(vwgl.Matrix4.makeOrtho(left, right, bottom, top, nearPlane, farPlane));
		return this;
	},
	
	lookAt:function(origin, target, up) {
		this.assign(vwgl.Matrix4.makeLookAt(origin,target,up));
		return this;
	},
	
	translate:function(x, y, z) {
		this.mult(vwgl.Matrix4.makeTranslation(x, y, z));
		return this;
	},
	
	rotate:function(alpha, x, y, z) {
		this.mult(vwgl.Matrix4.makeRotation(alpha, x, y, z));
		return this;
	},
	
	scale:function(x, y, z) {
		this.mult(vwgl.Matrix4.makeScale(x, y, z));
		return this;
	},

	// Operators
	//operator[]()
	elemAtIndex:function(i) { return this._m[i]; },
	
	//operator=()
	assign:function(a,b,c,d) {
		if (typeof(a)=='object' && a._m) {
			if (a._m.length==9) {
				this._m = [ a._m[0], a._m[1], a._m[2], 0, 
							a._m[3], a._m[4], a._m[5], 0, 
							a._m[6], a._m[7], a._m[8], 0, 
							0,       0,       0,       1 ];
			}
			else if (a._m.length==16) {
				this._m = [ a._m[ 0], a._m[ 1], a._m[ 2], a._m[ 3],
							a._m[ 4], a._m[ 5], a._m[ 6], a._m[ 7],
							a._m[ 8], a._m[ 9], a._m[10], a._m[11],
							a._m[12], a._m[13], a._m[14], a._m[15] ];
			}
			else {
				this.zero();
			}
		}
		else if (d===undefined) {
			if (typeof(a)=='object' && a.length>=16) {
				this._m = [ a[0],a[1],a[2],a[3], a[4],a[5],a[6],a[7], a[8],a[9],a[10],a[11], a[12],a[13],a[14],a[15] ];
			}
			else if (typeof(a)=='object' && a._m && a._m.lenght>=16) {
				this._m = [ a._m[0],a._m[1],a._m[2],a._m[3], a._m[4],a._m[5],a._m[6],a._m[7], a._m[8],a._m[9],a._m[10],a._m[11], a._m[12],a._m[13],a._m[14],a._m[15] ];
			}
			else {
				this._m = [ a,a,a,a, a,a,a,a, a,a,a,a, a,a,a,a ];
			}
		}
		else if (typeof(a)=='object' && a._v && a._v.length>=4 &&
				 typeof(b)=='object' && b._v && b._v.length>=4 &&
			 	 typeof(c)=='object' && c._v && c._v.length>=4 &&
				 typeof(d)=='object' && d._v && d._v.length>=4 ) {
			 this._m = [ a.x(),a.y(),a.z(),a.w(),
						 b.x(),b.y(),b.z(),b.w(),
						 c.x(),c.y(),c.z(),c.w(),
						 d.x(),d.y(),d.z(),d.w() ];
		}
		else {
			this.zero();
		}
		return this;
	},
	
	//operator==(const Matrix3 & m) {
	equals:function(m) {
		return	this._m[ 0]==m._m[ 0] && this._m[ 1]==m._m[ 1] && this._m[ 2]==m._m[ 2] && this._m[ 3]==m._m[ 3] &&
				this._m[ 4]==m._m[ 4] && this._m[ 5]==m._m[ 5] && this._m[ 6]==m._m[ 6] && this._m[ 7]==m._m[ 7] &&
				this._m[ 8]==m._m[ 8] && this._m[ 9]==m._m[ 9] && this._m[10]==m._m[10] && this._m[11]==m._m[11] &&
				this._m[12]==m._m[12] && this._m[13]==m._m[13] && this._m[14]==m._m[14] && this._m[15]==m._m[15];
	},
	
	// operator!=
	notEquals:function(m) {
		return	this._m[ 0]!=m._m[ 0] || this._m[ 1]!=m._m[ 1] || this._m[ 2]!=m._m[ 2] || this._m[ 3]!=m._m[ 3] ||
				this._m[ 4]!=m._m[ 4] || this._m[ 5]!=m._m[ 5] || this._m[ 6]!=m._m[ 6] || this._m[ 7]!=m._m[ 7] ||
				this._m[ 8]!=m._m[ 8] || this._m[ 9]!=m._m[ 9] || this._m[10]!=m._m[10] || this._m[11]!=m._m[11] ||
				this._m[12]!=m._m[12] || this._m[13]!=m._m[13] || this._m[14]!=m._m[14] || this._m[15]!=m._m[15];
	},
	
	mult:function(a) {
		if (typeof(a)=='number') {
			this._m[ 0] *= a; this._m[ 1] *= a; this._m[ 2] *= a; this._m[ 3] *= a;
			this._m[ 4] *= a; this._m[ 5] *= a; this._m[ 6] *= a; this._m[ 7] *= a;
			this._m[ 8] *= a; this._m[ 9] *= a; this._m[10] *= a; this._m[11] *= a;
			this._m[12] *= a; this._m[13] *= a; this._m[14] *= a; this._m[15] *= a;
			return this;
		}

		var rm = this._m;
		var lm = a._m;
		var res = [];
	
		res.push(lm[ 0] * rm[ 0] + lm[ 1] * rm[ 4] + lm[ 2] * rm[ 8] + lm[ 3] * rm[12]);
		res.push(lm[ 0] * rm[ 1] + lm[ 1] * rm[ 5] + lm[ 2] * rm[ 9] + lm[ 3] * rm[13]);
		res.push(lm[ 0] * rm[ 2] + lm[ 1] * rm[ 6] + lm[ 2] * rm[10] + lm[ 3] * rm[14]);
		res.push(lm[ 0] * rm[ 3] + lm[ 1] * rm[ 7] + lm[ 2] * rm[11] + lm[ 3] * rm[15]);
		
		res.push(lm[ 4] * rm[ 0] + lm[ 5] * rm[ 4] + lm[ 6] * rm[ 8] + lm[ 7] * rm[12]);
		res.push(lm[ 4] * rm[ 1] + lm[ 5] * rm[ 5] + lm[ 6] * rm[ 9] + lm[ 7] * rm[13]);
		res.push(lm[ 4] * rm[ 2] + lm[ 5] * rm[ 6] + lm[ 6] * rm[10] + lm[ 7] * rm[14]);
		res.push(lm[ 4] * rm[ 3] + lm[ 5] * rm[ 7] + lm[ 6] * rm[11] + lm[ 7] * rm[15]);
		
		res.push(lm[ 8] * rm[ 0] + lm[ 9] * rm[ 4] + lm[10] * rm[ 8] + lm[11] * rm[12]);
		res.push(lm[ 8] * rm[ 1] + lm[ 9] * rm[ 5] + lm[10] * rm[ 9] + lm[11] * rm[13]);
		res.push(lm[ 8] * rm[ 2] + lm[ 9] * rm[ 6] + lm[10] * rm[10] + lm[11] * rm[14]);
		res.push(lm[ 8] * rm[ 3] + lm[ 9] * rm[ 7] + lm[10] * rm[11] + lm[11] * rm[15]);
		
		res.push(lm[12] * rm[ 0] + lm[13] * rm[ 4] + lm[14] * rm[ 8] + lm[15] * rm[12]);
		res.push(lm[12] * rm[ 1] + lm[13] * rm[ 5] + lm[14] * rm[ 9] + lm[15] * rm[13]);
		res.push(lm[12] * rm[ 2] + lm[13] * rm[ 6] + lm[14] * rm[10] + lm[15] * rm[14]);
		res.push(lm[12] * rm[ 3] + lm[13] * rm[ 7] + lm[14] * rm[11] + lm[15] * rm[15]);
	
		this._m = res;
		return this;
	},

	multVector:function(vec) {
		if (typeof(vec)=='object' && vec._v && vec._v.length>=3) {
			vec = vec._v;
		}

		res = new vwgl.Vector4();
		var x=vec[0];
		var y=vec[1];
		var z=vec[2];
		var w=1.0;
	
		return new vwgl.Vector4(this._m[0]*x + this._m[4]*y + this._m[ 8]*z + this._m[12]*w,
								this._m[1]*x + this._m[5]*y + this._m[ 9]*z + this._m[13]*w,
								this._m[2]*x + this._m[6]*y + this._m[10]*z + this._m[14]*w,
								this._m[3]*x + this._m[7]*y + this._m[11]*z + this._m[15]*w);
	},
	
	invert:function() {
		var inv = [];
		var _m = this._m;
	
        inv[0] = _m[5] * _m[10] * _m[15] - _m[5] * _m[11] * _m[14] - _m[9] * _m[6] * _m[15] + _m[9] * _m[7] * _m[14] + _m[13] * _m[6] * _m[11] - _m[13] * _m[7] * _m[10];
        inv[4] = -_m[4] * _m[10] * _m[15] + _m[4] * _m[11] * _m[14] + _m[8] * _m[6] * _m[15] - _m[8] * _m[7] * _m[14] - _m[12] * _m[6] * _m[11] + _m[12] * _m[7] * _m[10];
        inv[8] = _m[4] * _m[9] * _m[15] - _m[4] * _m[11] * _m[13] - _m[8] * _m[5] * _m[15] + _m[8] * _m[7] * _m[13] + _m[12] * _m[5] * _m[11] - _m[12] * _m[7] * _m[9];
        inv[12] = -_m[4] * _m[9] * _m[14] + _m[4] * _m[10] * _m[13] + _m[8] * _m[5] * _m[14] - _m[8] * _m[6] * _m[13] - _m[12] * _m[5] * _m[10] + _m[12] * _m[6] * _m[9];
        inv[1] = -_m[1] * _m[10] * _m[15] + _m[1] * _m[11] * _m[14] + _m[9] * _m[2] * _m[15] - _m[9] * _m[3] * _m[14] - _m[13] * _m[2] * _m[11] + _m[13] * _m[3] * _m[10];
        inv[5] = _m[0] * _m[10] * _m[15] - _m[0] * _m[11] * _m[14] - _m[8] * _m[2] * _m[15] + _m[8] * _m[3] * _m[14] + _m[12] * _m[2] * _m[11] - _m[12] * _m[3] * _m[10];
        inv[9] = -_m[0] * _m[9] * _m[15] + _m[0] * _m[11] * _m[13] + _m[8] * _m[1] * _m[15] - _m[8] * _m[3] * _m[13] - _m[12] * _m[1] * _m[11] + _m[12] * _m[3] * _m[9];
        inv[13] = _m[0] * _m[9] * _m[14] - _m[0] * _m[10] * _m[13] - _m[8] * _m[1] * _m[14] + _m[8] * _m[2] * _m[13] + _m[12] * _m[1] * _m[10] - _m[12] * _m[2] * _m[9];
        inv[2] = _m[1] * _m[6] * _m[15] - _m[1] * _m[7] * _m[14] - _m[5] * _m[2] * _m[15] + _m[5] * _m[3] * _m[14] + _m[13] * _m[2] * _m[7] - _m[13] * _m[3] * _m[6];
        inv[6] = -_m[0] * _m[6] * _m[15] + _m[0] * _m[7] * _m[14] + _m[4] * _m[2] * _m[15] - _m[4] * _m[3] * _m[14] - _m[12] * _m[2] * _m[7] + _m[12] * _m[3] * _m[6];
        inv[10] = _m[0] * _m[5] * _m[15] - _m[0] * _m[7] * _m[13] - _m[4] * _m[1] * _m[15] + _m[4] * _m[3] * _m[13] + _m[12] * _m[1] * _m[7] - _m[12] * _m[3] * _m[5];
        inv[14] = -_m[0] * _m[5] * _m[14] + _m[0] * _m[6] * _m[13] + _m[4] * _m[1] * _m[14] - _m[4] * _m[2] * _m[13] - _m[12] * _m[1] * _m[6] + _m[12] * _m[2] * _m[5];
        inv[3] = -_m[1] * _m[6] * _m[11] + _m[1] * _m[7] * _m[10] + _m[5] * _m[2] * _m[11] - _m[5] * _m[3] * _m[10] - _m[9] * _m[2] * _m[7] + _m[9] * _m[3] * _m[6];
        inv[7] = _m[0] * _m[6] * _m[11] - _m[0] * _m[7] * _m[10] - _m[4] * _m[2] * _m[11] + _m[4] * _m[3] * _m[10] + _m[8] * _m[2] * _m[7] - _m[8] * _m[3] * _m[6];
        inv[11] = -_m[0] * _m[5] * _m[11] + _m[0] * _m[7] * _m[9] + _m[4] * _m[1] * _m[11] - _m[4] * _m[3] * _m[9] - _m[8] * _m[1] * _m[7] + _m[8] * _m[3] * _m[5];
        inv[15] = _m[0] * _m[5] * _m[10] - _m[0] * _m[6] * _m[9] - _m[4] * _m[1] * _m[10] + _m[4] * _m[2] * _m[9] + _m[8] * _m[1] * _m[6] - _m[8] * _m[2] * _m[5];
	
        var det = _m[0] * inv[0] + _m[1] * inv[4] + _m[2] * inv[8] + _m[3] * inv[12];
	
        if (det==0) {
			this.set(0);
        }
	
        det = 1 / det;
	
        for (var i=0; i<16; ++i) {
            this._m[i] = inv[i] * det;
        }

		return this;
	},
	
	traspose:function() {
		var _m = this._m;
		var r0 = new vwgl.Vector4(_m[0], _m[4], _m[ 8], _m[12]);
		var r1 = new vwgl.Vector4(_m[1], _m[5], _m[ 9], _m[13]);
		var r2 = new vwgl.Vector4(_m[2], _m[6], _m[10], _m[14]);
		var r3 = new vwgl.Vector4(_m[3], _m[7], _m[11], _m[15]);
	
		this.setRow(0, r0);
		this.setRow(1, r1);
		this.setRow(2, r2);
		this.setRow(3, r3);
		return this;
	},
	
	getRotation:function() {
		var _m = this._m;
		return new vwgl.Matrix4(
				_m[0], _m[1], _m[ 2], 0,
				_m[4], _m[5], _m[ 6], 0,
				_m[8], _m[9], _m[10], 0,
				0,	   0,	  0, 	1
			);
	},

	getPosition:function() {
		return new vwgl.Vector3(this._m[12], this._m[13], this._m[14]);
	},
	
	transformDirection:function(/* Vector3 */ dir) {
		var direction = new vwgl.Vector3(dir);
		var trx = new vwgl.Matrix4(this);
		trx.setRow(3, new vwgl.Vector4(0.0, 0.0, 0.0, 1.0));
		direction.assign(trx.multVector(direction).xyz());
		direction.normalize();
		return direction;
	},
	
	forwardVector:function() {
		return this.transformDirection(new vwgl.Vector3(0.0, 0.0, 1.0));
	},
	
	rightVector:function() {
		return this.transformDirection(new vwgl.Vector3(1.0, 0.0, 0.0));
	},
	
	upVector:function() {
		return this.transformDirection(new vwgl.Vector3(0.0, 1.0, 0.0));
	},
	
	backwardVector:function() {
		return this.transformDirection(new vwgl.Vector3(0.0, 0.0, -1.0));
	},
	
	leftVector:function() {
		return this.transformDirection(new vwgl.Vector3(-1.0, 0.0, 0.0));
	},
	
	downVector:function() {
		return this.transformDirection(new vwgl.Vector3(0.0, -1.0, 0.0));
	},
	
	set00:function(v) { this._m[0] = v; },
	set01:function(v) { this._m[1] = v; },
	set02:function(v) { this._m[2] = v; },
	set03:function(v) { this._m[3] = v; },
	set10:function(v) { this._m[4] = v; },
	set11:function(v) { this._m[5] = v; },
	set12:function(v) { this._m[6] = v; },
	set13:function(v) { this._m[7] = v; },
	set20:function(v) { this._m[8] = v; },
	set21:function(v) { this._m[9] = v; },
	set22:function(v) { this._m[10] = v; },
	set23:function(v) { this._m[11] = v; },
	set30:function(v) { this._m[12] = v; },
	set31:function(v) { this._m[13] = v; },
	set32:function(v) { this._m[14] = v; },
	set33:function(v) { this._m[15] = v; },
	
	get00:function() { return this._m[0]; },
	get01:function() { return this._m[1]; },
	get02:function() { return this._m[2]; },
	get03:function() { return this._m[3]; },
	get10:function() { return this._m[4]; },
	get11:function() { return this._m[5]; },
	get12:function() { return this._m[6]; },
	get13:function() { return this._m[7]; },
	get20:function() { return this._m[8]; },
	get21:function() { return this._m[9]; },
	get22:function() { return this._m[10]; },
	get23:function() { return this._m[11]; },
	get30:function() { return this._m[12]; },
	get31:function() { return this._m[13]; },
	get32:function() { return this._m[14]; },
	get33:function() { return this._m[15]; },
	
	isNan:function() {
		return	Number.isNaN(this._m[ 0]) || Number.isNaN(this._m[ 1]) || Number.isNaN(this._m[ 2]) || Number.isNaN(this._m[ 3]) ||
				Number.isNaN(this._m[ 4]) || Number.isNaN(this._m[ 5]) || Number.isNaN(this._m[ 6]) || Number.isNaN(this._m[ 7]) ||
				Number.isNaN(this._m[ 8]) || Number.isNaN(this._m[ 9]) || Number.isNaN(this._m[10]) || Number.isNaN(this._m[11]) ||
				Number.isNaN(this._m[12]) || Number.isNaN(this._m[13]) || Number.isNaN(this._m[14]) || Number.isNaN(this._m[15]);
	},
	
	getOrthoValues:function() {
		return [ (1+get23())/get22(),
				-(1-get23())/get22(),
				 (1-get13())/get11(),
				-(1+get13())/get11(),
				-(1+get03())/get00(),
				 (1-get03())/get00() ];
	},

	getPerspectiveValues:function() {
		return [ get23()/(get22()-1),
				 get23()/(get22()+1),
				 near * (get12()-1)/get11(),
				 near * (get12()+1)/get11(),
				 near * (get02()-1)/get00(),
				 near * (get02()+1)/get00() ];
	},

	toString:function() {
		return "[" + this._m[ 0] + ", " + this._m[ 1] + ", " + this._m[ 2] + ", " + this._m[ 3] + "]\n" +
			  " [" + this._m[ 4] + ", " + this._m[ 5] + ", " + this._m[ 6] + ", " + this._m[ 7] + "]\n" +
			  " [" + this._m[ 8] + ", " + this._m[ 9] + ", " + this._m[10] + ", " + this._m[11] + "]\n" +
			  " [" + this._m[12] + ", " + this._m[13] + ", " + this._m[14] + ", " + this._m[15] + "]";
	}
});


vwgl.Matrix4.unproject = function(x, y, depth, mvMat, pMat, viewport) {
	var mvp = new vwgl.Matrix4(pMat);
	mvp.mult(mvMat);
	mvp.invert();

	var vin = new vwgl.Vector4(((x - viewport.y()) / viewport.width()) * 2.0 - 1.0,
							   ((y - viewport.x()) / viewport.height()) * 2.0 - 1.0,
							   depth * 2.0 - 1.0,
							   1.0);
	
	var result = new vwgl.Vector4(mvp.multVector(vin));
	if (result.z()==0) {
		result.set(0.0);
	}
	else {
		result.set(result.x()/result.w(),
				   result.y()/result.w(),
				   result.z()/result.w(),
				   result.w()/result.w());
	}

	return result;
}

vwgl.Matrix4.makeIdentity = function() {
	var i = new vwgl.Matrix4();
	return i.identity();
}
	
vwgl.Matrix4.makePerspective = function(fovy, aspect, nearPlane, farPlane) {
	var fovy2 = vwgl.Math.tan(fovy * vwgl.Math.kPi / 360.0) * nearPlane;
	var fovy2aspect = fovy2 * aspect;

	return vwgl.Matrix4.makeFrustum(-fovy2aspect,fovy2aspect,-fovy2,fovy2,nearPlane,farPlane);
}

vwgl.Matrix4.makeFrustum = function(left, right, bottom, top, nearPlane, farPlane) {
	var res = new vwgl.Matrix4();
	var A = right-left;
	var B = top-bottom;
	var C = farPlane-nearPlane;
	
	res.setRow(0, new vwgl.Vector4(nearPlane*2.0/A,	0.0,	0.0,	0.0));
	res.setRow(1, new vwgl.Vector4(0.0,	nearPlane*2.0/B,	0.0,	0.0));
	res.setRow(2, new vwgl.Vector4((right+left)/A,	(top+bottom)/B,	-(farPlane+nearPlane)/C,	-1.0));
	res.setRow(3, new vwgl.Vector4(0.0,	0.0,	-(farPlane*nearPlane*2.0)/C,	0.0));
	
	return res;
}

vwgl.Matrix4.makeOrtho = function(left, right, bottom, top, nearPlane, farPlane) {
	var p = new vwgl.Matrix4();
	
	var m = right-left;
	var l = top-bottom;
	var k = farPlane-nearPlane;;
	
	p._m[0] = 2/m; p._m[1] = 0;   p._m[2] = 0;     p._m[3] = 0;
	p._m[4] = 0;   p._m[5] = 2/l; p._m[6] = 0;     p._m[7] = 0;
	p._m[8] = 0;   p._m[9] = 0;   p._m[10] = -2/k; p._m[11]= 0;
	p._m[12]=-(left+right)/m; p._m[13] = -(top+bottom)/l; p._m[14] = -(farPlane+nearPlane)/k; p._m[15]=1;

	return p;
}
	
vwgl.Matrix4.makeLookAt = function(p_eye, p_center, p_up) {
	var result = new vwgl.Matrix4();
	var forward = new vwgl.Vector3(p_center);
	forward.sub(p_eye);
	var up = new vwgl.Vector3(p_up);
	
	forward.normalize();
	
	var side = new vwgl.Vector3(forward);
	side.cross(up);
	up.assign(side);
	up.cross(forward);
	
	result.identity();
	result.set00(side.x()); result.set10(side.y()); result.set20(side.z());
	result.set01(up.x()); result.set11(up.y()); result.set21(up.z());
	result.set02(-forward.x()); result.set12(-forward.y()); result.set22(-forward.z());
	result.setRow(3, new vwgl.Vector4(-p_eye.x(),-p_eye.y(),-p_eye.z(),1.0));
	
	return result;
}

vwgl.Matrix4.makeTranslation = function(x, y, z) {
	if (typeof(x)=='object' && x._v && x._v.length>=3) {
		y = x._v[1];
		z = x._v[2];
		x = x._v[0];
	}
	var t = new vwgl.Matrix4();
	t._m[ 0] = 1.0; t._m[ 1] = 0.0; t._m[ 2] = 0.0; t._m[ 3] = 0.0;
	t._m[ 4] = 0.0; t._m[ 5] = 1.0; t._m[ 6] = 0.0; t._m[ 7] = 0.0;
	t._m[ 8] = 0.0; t._m[ 9] = 0.0; t._m[10] = 1.0; t._m[11] = 0.0;
	t._m[12] =   x; t._m[13] =   y; t._m[14] =   z; t._m[15] = 1.0;
	return t;
}
	
vwgl.Matrix4.makeRotation = function(alpha, x, y, z) {
	var axis = new vwgl.Vector3(x,y,z);
	axis.normalize();
	var rot = new vwgl.Matrix4();
	
	rot.identity();
	
	var cosAlpha = vwgl.Math.cos(alpha);
	var acosAlpha = 1.0 - cosAlpha;
	var sinAlpha = vwgl.Math.sin(alpha);
	
	rot._m[0] = axis.x() * axis.x() * acosAlpha + cosAlpha;
	rot._m[1] = axis.x() * axis.y() * acosAlpha + axis.z() * sinAlpha;
	rot._m[2] = axis.x() * axis.z() * acosAlpha - axis.y() * sinAlpha;
	
	rot._m[4] = axis.y() * axis.x() * acosAlpha - axis.z() * sinAlpha;
	rot._m[5] = axis.y() * axis.y() * acosAlpha + cosAlpha;
	rot._m[6] = axis.y() * axis.z() * acosAlpha + axis.x() * sinAlpha;
	
	rot._m[8] = axis.z() * axis.x() * acosAlpha + axis.y() * sinAlpha;
	rot._m[9] = axis.z() * axis.y() * acosAlpha - axis.x() * sinAlpha;
	rot._m[10] = axis.z() * axis.z() * acosAlpha + cosAlpha;
	
	return rot;
}

vwgl.Matrix4.makeScale = function(x, y, z) {
	if (typeof(x)=='object' && x._v && x._v.length>=3) {
		x = x._v[0];
		y = x._v[1];
		z = x._v[2];
	}
	var s = new vwgl.Matrix4();
	
	s._m[ 0] =   x; s._m[ 1] = 0.0; s._m[ 2] = 0.0; s._m[ 3] = 0.0;
	s._m[ 4] = 0.0; s._m[ 5] =   y; s._m[ 6] = 0.0; s._m[ 7] = 0.0;
	s._m[ 8] = 0.0; s._m[ 9] = 0.0; s._m[10] =   z; s._m[11] = 0.0;
	s._m[12] = 0.0; s._m[13] = 0.0; s._m[14] = 0.0; s._m[15] = 1.0;
	
	return s;
}

