// In c++ API, the loaders receive the path. In Javascript API the loaders receive directly the loaded data
Class ("vwgl.LoaderPlugin",{

	getFileTypes:function() {
		return [];
	},
	
	acceptFileType:function(type) {
		var ext = type.toLowerCase().split('?')[0].split('.').pop();
		var types = this.getFileTypes();
		for (var i=0;i<types.length;++i) {
			if (types[i]==ext) return true;
		}
		return false;
	},
	
	loadDrawable:function(data) {
		// Implement this function to load your file
		// 1) clear warnings
		this._clearWarningMessages();
		
		// 2) Load your file
		
		// 3) Add warning messages
		this._addWarningMessage("Default implementation: no data loaded");
		
		// 4) Return data
		return null;
	},

	getWarningMessages:function() { return this._warningMessages; },
	
	_addWarningMessage:function(msg) { this._warningMessages.push(msg); },
	_clearWarningMessages:function() { this._warningMessages = []; }
});

// Loader doesn't work like in c++ version because JavaScript load is asynchronous.
// This version of loader is designed to simulate a synchronous load, to keep
// the code as similar as possible than the c++ version
Class ("vwgl.Loader", {
	_warningMessages:null,
	
	initialize:function() {
		this._warningMessages = [];
	},
	
	clearWarningMessages:function() {
		this._warningMessages = [];
	},

	// The functions loadDrawable(), loadTexture(), loadImage() and loadCubemap() must be used syncrhonously.
	// To do it, the resources must be previously loaded, using loadResource or loadResourceList
	loadDrawable:function(url) {
		this.clearWarningMessages();
		var selectedPlugin = null;
		
		for (var i=0;i<vwgl.Loader.s_readerPluginList.length;++i) {
			var plugin = vwgl.Loader.s_readerPluginList[i];
			if (plugin.acceptFileType(url)) {
				selectedPlugin = plugin;
				break;
			}
		}
		if (selectedPlugin) {
			var resource = this.getResource(url);
			if (resource===undefined) {
				this._warningMessages.push("Error loading drawable (" + url + "): The resource is not loaded. To use Loader.loadDrawable() the resource must be previously loaded using loadResource() or loadResourceList()");
			}
			else if (resource===null) {
				this._warningMessages.push("Error loading drawable (" + url + "): resource not found in the requested url");
			}
			else {
				var result = selectedPlugin.loadDrawable(resource);
				var warningMessages = selectedPlugin.getWarningMessages();
				for (var i=0;i<warningMessages.length;++i) {
					this._warningMessages.push(warningMessages[i]);
				}
				return result;
			}
		}
		return null;
	},
	
	loadImage:function(url) {
		if (this._isImage(url)) return this.getResource(url);
		else return null;
	},
	
	loadCubemap:function(posX,negX,posY,negY,posZ,negZ) {
		var posXimg = this.getResource(posX);
		var negXimg = this.getResource(negX);
		var posYimg = this.getResource(posY);
		var negYimg = this.getResource(negY);
		var posZimg = this.getResource(posZ);
		var negZimg = this.getResource(negZ);	

		if (posXimg===undefined || negXimg===undefined || 
			posYimg===undefined || negYimg===undefined ||
			posZimg===undefined || negZimg===undefined) {
			this._warningMessages.push("Error loading cubemap: The resources are not loaded. To use Loader.loadCubemap() all the resources must be previously loaded using loadResource() or loadResourceList()");
		}
		else if (posXimg===null || negXimg===null || 
				 posYimg===null || negYimg===null ||
				 posZimg===null || negZimg===null) {
			this._warningMessages.push("Error loading cubemap: resource not found in the requested url");
		}
		else if (!this._isValidImage(posXimg) || !this._isValidImage(negXimg) ||
				 !this._isValidImage(posYimg) || !this._isValidImage(negYimg) ||
				 !this._isValidImage(posZimg) || !this._isValidImage(negZimg) ) {
			this._warningMessages.push("Error loading cubemap: the resource is loaded but it seems that is not a valid image.");
		}
		else {
			return vwgl.TextureManager.loadCubeMap(posXimg,negXimg,posYimg,negYimg,posZimg,negZimg);
		}
	},
	// End of syncrhonous functions

	// This function can be used synchronously: the texture will be initialized with a black image
	// and updated when the final image is loaded
	loadTexture:function(url) {
		var resource = this.getResource(url);
		if (!resource) {
			return vwgl.TextureManager.loadTextureDeferred(url);
		}
		else if (!this._isValidImage(resource)) {
			this._warningMessages.push("Error loading texture (" + url + "): the resource is loaded but it seems that is not a valid image.");
		}
		else {
			return vwgl.TextureManager.loadTexture(resource);
		}
	},
	
	loadResource:function(url,success) {
		if (this.getResource(url) && typeof(success)=='function') {
			base.debug.log("File already cached: " + url);
			success(this.getResource(url));
			vwgl.events.trigger(vwgl.events.loadCached,{resource:url,data:this.getResource(url)});
		}
		else if (this._isImage(url)){
			this.doLoadImage(url,success);
		}
		else if (this._isBinary(url)) {
			this.doLoadBinary(url,success);
		}
		else {
			this.doLoadResource(url,success);
		}
	},
	
	doLoadResource:function(url,/* function(resource) */ success) {
		var This = this;
		var key = url;
		url = this.getFullUrl(url);
		base.ajax.get({url:url},
			function(data,type,status,raw) {
				vwgl.Loader.s_resources[key] = data;
				base.debug.log("Data loaded: " + key);
				if (typeof(success)=='function') success(vwgl.Loader.s_resources[key]);
				vwgl.events.trigger(vwgl.events.loadComplete,{resource:key,data:data,type:'text'});
			},
			function(data,type,status,raw) {
				vwgl.Loader.s_resources[key] = null;
				base.debug.log("Data load error: " + key);
				if (typeof(success)=='function') success(vwgl.Loader.s_resources[key]);
				vwgl.events.trigger(vwgl.events.loadError,{resource:key,data:null,type:'text'});
			});
	},
	
	doLoadImage:function(url,success) {
		var This = this;
		var key = url;
		url = this.getFullUrl(url);
		var img = new Image();
		img.crossOrigin = "";
		img.onload = function(event) {
			vwgl.Loader.s_resources[key] = event.target;
			base.debug.log("Image loaded: " + key);
			if (typeof(success)=='function') success(vwgl.Loader.s_resources[key]);
			vwgl.events.trigger(vwgl.events.loadComplete,{resource:key,data:event.target,type:'image'});
		}
		img.onerror = function(event) {
			vwgl.Loader.s_resources[key] = null;
			base.debug.log("Image load error: " + key);
			if (typeof(success)=='function') success(vwgl.Loader.s_resources[key]);
			vwgl.events.trigger(vwgl.events.loadError,{resource:key,data:null,type:'image'});
		}
		img.onabort = function(event) {
			vwgl.Loader.s_resources[key] = null;
			base.debug.log("Image load error: " + key);
			if (typeof(success)=='function') success(vwgl.Loader.s_resources[key]);
			vwgl.events.trigger(vwgl.events.loadError,{resource:key,data:null,type:'image'});
		}
		img.src = url;
	},
	
	doLoadBinary:function(url,success) {
		var key = url;
		url = this.getFullUrl(url);
		var This = this;
		var xhr = new XMLHttpRequest();  
		xhr.open("GET", url, true);  
		xhr.responseType = "arraybuffer"; 
		xhr.onload = function(e) { 
			vwgl.Loader.s_resources[key] = xhr.response;
			base.debug.log("Binary file loaded: " + key);
			if (typeof(success)=='function') success(vwgl.Loader.s_resources[key]);
			vwgl.events.trigger(vwgl.events.loadComplete,{resource:key,data:xhr.response,type:'binary'});
		}
		xhr.onerror = function(e) {
			vwgl.Loader.s_resources[key] = null;
			base.debug.log("Binary file load error: " + key);
			if (typeof(success)=='function') success(vwgl.Loader.s_resources[key]);
			vwgl.events.trigger(vwgl.events.loadError,{resource:key,data:null,type:'binary'});
		}
		xhr.onerror = function(e) {
			vwgl.Loader.s_resources[key] = null;
			base.debug.log("Binary file load error: " + key);
			if (typeof(success)=='function') success(vwgl.Loader.s_resources[key]);
			vwgl.events.trigger(vwgl.events.loadError,{resource:key,data:null,type:'binary'});
		}
		xhr.send();
	},
	
	// Success callback: function(numOfLoadedResources,numOfErrors)
	loadResourceList:function(resourceList,success) {
		var numberOfResources = resourceList.length;
		var loadedResources = 0;
		var numberOfErrors = 0;
		function doneCallback(res) {
			loadedResources++;
			if (!res) numberOfErrors++;
			if (loadedResources==numberOfResources && typeof(success)=='function') {
				success(numberOfResources,numberOfErrors);
			}
		}
		
		for (var i=0;i<resourceList.length;++i) {
			var resUrl = resourceList[i];
			this.loadResource(resUrl,doneCallback);
		}
	},
	
	// This function returns:
	//	undefined: if the resource has not been loaded
	//	null: if there was an error loading the resource
	getResource:function(url) {
		return vwgl.Loader.getResource(url);
	},
	
	_isImage:function(url) {
		var ext = url.toLowerCase().split('?')[0].split('.').pop();
		if (ext=='jpg' || ext=='jpeg' || ext=='png' || ext=='gif' || ext=='tif' || ext=='tiff') {
			return true;
		}
		return false;
	},
	
	_isShader:function(url) {
		var ext = url.toLowerCase().split('?')[0].split('.').pop();
		if (ext=='vsh' || ext=='fsh') {
			return true;
		}
		return false;
	},

	_isBinary:function(url) {
		var ext = url.toLowerCase().split('?')[0].split('.').pop();
		if (vwgl.Loader.s_binaryExtensions[ext]==undefined) return false;
		return true;
	},
	
	getFullUrl:function(url) {
		if (url[0]=='/') return url;
		if (this._isShader(url)) {
			return vwgl.System.get().getDefaultShaderPath() + url;
		}
		else {
			return vwgl.System.get().getResourcesPath() + url;
		}
	},
	
	_isValidImage:function(img) {
		return img.width>0 && img.height>0;
	}
});

vwgl.Loader.s_readerPluginList = []
vwgl.Loader.s_resources = {}
vwgl.Loader.s_binaryExtensions = {}
vwgl.Loader.addBinaryExtension = function(ext,description) {
	vwgl.Loader.s_binaryExtensions[ext] = description;
}
vwgl.Loader.getResource = function(url) {
	return vwgl.Loader.s_resources[url];
}
vwgl.Loader.registerReader = function(readerPlugin) {
	vwgl.Loader.s_readerPluginList.push(readerPlugin);
}

