
Class ("vwgl.physics.Intersection",{
	_type:null,
	_p0:null,		//	vwgl.Vector3
	_p1:null,		//	vwgl.Vector3

	getType:function() { return this._type; },
	getPoint:function() { return this._p0; },
	getEndPoint:function() { return this._p1; },
	
	intersects:function() { return false; }
});

vwgl.physics.Intersection.kTypeNone = 0;
vwgl.physics.Intersection.kTypePoint = 1;
vwgl.physics.Intersection.kTypeLine = 2;

Class ("vwgl.physics.RayToPlaneIntersection", vwgl.physics.Intersection, {
	_ray:null,		// vwgl.Ray
	
	initialize:function(ray,plane) {
		this._type = vwgl.physics.Intersection.kTypePoint;
		var p0 = new vwgl.Vector3(plane.getOrigin());
		var n = new vwgl.Vector3(plane.getNormal());
		var l0 = new vwgl.Vector3(ray.getStart());
		var l = new vwgl.Vector3(ray.getVector());
		var num = p0.sub(l0).dot(n);
		var den = l.dot(n);
		
		if (den==0) return;
		var d = num/den;
		if (d>ray.getMagnitude()) return;
		
		this._ray = vwgl.physics.Ray.rayWithVector(ray.getVector(),ray.getStart(),d);
		this._p0 = this._ray.getEnd();
	},
	
	getRay:function() {
		return this._ray;
	},
	
	intersects:function() {
		return (this._ray!=null && this._p0!=null);
	}
});

vwgl.physics.Intersection.rayToPlane = function(ray,plane) {
	return new vwgl.physics.RayToPlaneIntersection(ray,plane);
}

