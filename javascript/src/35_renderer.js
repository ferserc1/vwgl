
Class ("vwgl.RenderSettings", {
	_brightness:0.5,
	_contrast:0.5,
	_hue:1.0,
	_saturation:1.0,
	_lightness:1.0,

	setBrightness:function(b) { this._brightness = b; },
	setContrast:function(c) { this._contrast = c; },
	setHue:function(h) { this._hue = h; },
	setSaturation:function(s) { this._saturation = s; },
	setLightness:function(l) { this._lightness = l; },
	
	getBrightness:function() { return this._brightness; },
	getContrast:function() { return this._contrast; },
	getHue:function(h) { return this._hue; },
	getSaturation:function(s) { return this._saturation; },
	getLightness:function(l) { return this._lightness; },
	
	initialize:function() {
		
	}
});

vwgl.RenderSettings.s_renderSettings = null;
vwgl.RenderSettings.getCurrentRenderSettings = function() {
	return vwgl.RenderSettings.s_renderSettings;
}
vwgl.RenderSettings.setCurrentRenderSettings = function(settings) {
	vwgl.RenderSettings.s_renderSettings = settings;
}

Class ("vwgl.Renderer", {
	_viewport:null,		// vwgl.Viewport
	_mapSize:null,		// vwgl.Size2D
	_sceneRoot:null,	// vwgl.Group
	
	_renderSettings:null,
	
	_filterList:null,
	
	_updateBuffers:0,
	
	initialize:function() {
		this._renderSettings = new vwgl.RenderSettings();
		this._viewport = new vwgl.Viewport();
		this._filterList = [];
		this._updateBuffers = 	vwgl.Renderer.kDiffuseBuffer  |
								vwgl.Renderer.kSpecularBuffer |
								vwgl.Renderer.kNormalBuffer   |
								vwgl.Renderer.kPositionBuffer |
								vwgl.Renderer.kLightingBuffer |
								vwgl.Renderer.kSSAOBuffer;
	},
	
	setUpdateBuffers:function(buffers) { this._updateBuffers = buffers; },
	getUpdateBuffers:function(buffers) { return this._updateBuffers; },
	
	setViewport:function(/* Viewport | number */ x, y, w, h) {
		if (dynamic_cast("vwgl.Viewport",x)) { this._viewport.assign(x); }
		else this._viewport.set(x,y,w,h);
	},

	getRenderSettings:function() {
		return this._renderSettings;
	},

	getFilterList:function() { return this._filterList; },
	
	getFilter:function(type) {
		var result = null;
		for (var i=0; result==null && i<this._filterList.length; ++i) {
			result = dynamic_cast(type,this._filterList[i]);
		}
		return result;
	},
	
	_addFilter:function(filter) {
		if (dynamic_cast("vwgl.Material",filter)) {
			this._filterList.push(filter);
		}
	},

	// Pure virtual functions
	build:null,			// function(/* Group */ sceneRoot, width, height) {},
	destroy:null,		// function() {},
	draw:null,			// function(/* Camera */ cam) {},
	setClearColor:null,	// function(/* Vector4 */ color)
	resize:null			// function(/* vwgl,Size2D */ size)
		
});

vwgl.Renderer.kDiffuseBuffer 	= 1 << 0;
vwgl.Renderer.kSpecularBuffer  	= 1 << 1;
vwgl.Renderer.kNormalBuffer 	= 1 << 2;
vwgl.Renderer.kPositionBuffer	= 1 << 3;
vwgl.Renderer.kLightingBuffer	= 1 << 4;
vwgl.Renderer.kSSAOBuffer		= 1 << 5;


