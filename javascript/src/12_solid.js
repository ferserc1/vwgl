/*
	Transformable interface:
		setTransform:function(vwgl.Matrix)
		getTransform:function() > vwgl.Matrix
*/

Class ("vwgl.TransformStrategy", {
	_transformable:null,	// Transformable object
	
	updateTransform:null, 	// Virtual method: Update the _transformable transformation matrix
	
	setTransformable:function(/* Transformable */ transformable) {
		this._transformable = transformable;
	}
});

Class ("vwgl.TRSTransformStrategy", vwgl.TransformStrategy, {
	_translate:null, 		// Vector3
	_rotateX:0.0, 			// float
	_rotateY:0.0, 			// float
	_rotateZ:0.0, 			// float
	_scale:null, 			// Vector3
	_rotationOrder:null, 	// RotationOrder
	
	initialize:function() {
		this._translate = new vwgl.Vector3(0.0);
		this._scale = new vwgl.Vector3(1.0);
		this._rotationOrder = vwgl.TRSTransformStrategy.kOrderXYZ;
	},
	
	translate:function(/* Vector3 */ t) { this._translate.assign(t); this.updateTransform(); },
	rotateX:function(/* float */ x) { this._rotateX = x; this.updateTransform(); },
	rotateY:function(/* float */ y) { this._rotateY = y; this.updateTransform(); },
	rotateZ:function(/* float */ z) { this._rotateZ = z; this.updateTransform(); },
	scale:function(/* Vector3 */ s) { this._scale.assign(s); this.updateTransform(); },
	
	getTranslate:function() { return this._translate; },
	getRotateX:function() { return this._rotateX; },
	getRotateY:function() { return this._rotateY; },
	getRotateZ:function() { return this._rotateZ; },
	getScale:function() { return this._scale; },
	
	setRotationOrder:function(/* RotationOrder */ order) {
		this._rotationOrder = order;
		this.updateTransform();
	},

	getRotationOrder:function() {
		return this._rotationOrder;
	},
	
	updateTransform:function() {
		if (!this._transformable) return;
		this._transformable.getTransform()
				.identity()
				.translate(this._translate.x(), this._translate.y(), this._translate.z());
		switch (this._rotationOrder) {
			case vwgl.TRSTransformStrategy.kOrderXYZ:
				this._transformable.getTransform()
					.rotate(this._rotateX, 1.0, 0.0, 0.0)
					.rotate(this._rotateY, 0.0, 1.0, 0.0)
					.rotate(this._rotateZ, 0.0, 0.0, 1.0);
				break;
			case vwgl.TRSTransformStrategy.kOrderXZY:
				this._transformable.getTransform()
					.rotate(this._rotateX, 1.0, 0.0, 0.0)
					.rotate(this._rotateZ, 0.0, 0.0, 1.0)
					.rotate(this._rotateY, 0.0, 1.0, 0.0);
				break;
			case vwgl.TRSTransformStrategy.kOrderYXZ:
				this._transformable.getTransform()
					.rotate(this._rotateY, 0.0, 1.0, 0.0)
					.rotate(this._rotateX, 1.0, 0.0, 0.0)
					.rotate(this._rotateZ, 0.0, 0.0, 1.0);
				break;
			case vwgl.TRSTransformStrategy.kOrderYZX:
				this._transformable.getTransform()
					.rotate(this._rotateY, 0.0, 1.0, 0.0)
					.rotate(this._rotateZ, 0.0, 0.0, 1.0)
					.rotate(this._rotateX, 1.0, 0.0, 0.0);
				break;
			case vwgl.TRSTransformStrategy.kOrderZYX:
				this._transformable.getTransform()
					.rotate(this._rotateZ, 0.0, 0.0, 1.0)
					.rotate(this._rotateY, 0.0, 1.0, 0.0)
					.rotate(this._rotateX, 1.0, 0.0, 0.0);
				break;
			case vwgl.TRSTransformStrategy.kOrderZXY:
				this._transformable.getTransform()
					.rotate(this._rotateZ, 0.0, 0.0, 1.0)
					.rotate(this._rotateX, 1.0, 0.0, 0.0)
					.rotate(this._rotateY, 0.0, 1.0, 0.0);
				break;
		}
		
		this._transformable.getTransform().scale(this._scale.x(), this._scale.y(), this._scale.z());
	}
});

vwgl.TRSTransformStrategy.kOrderXYZ = 0;
vwgl.TRSTransformStrategy.kOrderXZY = 1;
vwgl.TRSTransformStrategy.kOrderYXZ = 2;
vwgl.TRSTransformStrategy.kOrderYZX = 3;
vwgl.TRSTransformStrategy.kOrderZYX = 4;
vwgl.TRSTransformStrategy.kOrderZXY = 5;

Class ("vwgl.PolarTransformStrategy", vwgl.TransformStrategy, {
	_coords:null,	//	vwgl.Vector2 
	_distance:0.0,	//	float 
	_origin:null,	//	vwgl.Vector3 
	
	initialize:function() {
		this._coords = new vwgl.Vector2();
		this._origin = new vwgl.Vector3();
	},
	
	setOrientation:function(/* float */ o) { this._coords.x(o); this.updateTransform(); },
	setElevation:function(/* float */ e) { this._coords.y(e); this.updateTransform(); },
	setDistance:function(/* float */ d) { this._distance = d; this.updateTransform(); },
	setCoords:function(/* vwgl.Vector2 */ c) { this._coords.assign(c); this.updateTransform(); },
	setOrigin:function(/* vwgl.Vector3 */ o) { this._origin.assign(o); this.updateTransform(); },
	getOrientation:function() { return this._coords.x(); },
	getElevation:function() { return this._coords.y(); },
	getDistance:function() { return this._distance; },
	getCoords:function() { return this._coords; },
	getOrigin:function() { return this._origin; },
	
	updateTransform:function() {
		if (!this._transformable) return;
		this._transformable.getTransform()
			.identity()
			.translate(this._origin.x(), this._origin.y(), this._origin.z())
			.rotate(this._coords.x(),0.0,1.0,0.0)
			.rotate(this._coords.y(),1.0,0.0,0.0)
			.translate(0.0,0.0,-this._distance);
	}
});

Class ("vwgl.Solid",{

	_polyList:null,
	_material:null,
	_shadowProjector:null,
	_pickId:null,
	_visible:true,
	_groupName:"",

	_renderOpaque:true,
	_renderTransparent:true,

	_drawable:null,
	
	_transform:null,
	_transformStrategy:null,

	// In C++ API this constructor creates the material if it's not specified, but in JavaScript API the user
	// must to supply poly list and material because the material's setup can be asynchronous
	initialize:function( /* vwgl.PolyList | vwgl.Material | null */ a, /* vwgl.Material | null */ b) {
		if (dynamic_cast("vwgl.PolyList",a)) {
			this._polyList = a;
		}
		else if (dynamic_cast("vwgl.Material",a)) {
			this._material = a;
		}
		if (dynamic_cast("vwgl.Material",b)) {
			this._material = b;
		}

		if (!this._material) {
			this._material = new vwgl.GenericMaterial();
		}
		
		this._transform = vwgl.Matrix4.makeIdentity();
	},

	setTransform:function(/* vwgl.Matrix4 */ trx) { this._transform = trx; },
	getTransform:function() { return this._transform; },
	
	setTransformStrategy:function(/* vwgl.TransformStrategy */ strategy) {
		this._transformStrategy = strategy;
		this._transformStrategy.setTransformable(this);
		this._transformStrategy.updateTransform();
	},
	
	getTransformStrategy:function() { return this._transformStrategy; },
	
	getDrawable:function() { return this._drawable; },

	setPolyList:function( /* PolyList */ plist) { this._polyList = plist; },
	setMaterial:function( /* Material */ mat) { this._material = mat; },

	getPolyList:function() { return this._polyList; },
	getMaterial:function() { return this._material; },
	getGenericMaterial:function() { return dynamic_cast("vwgl.GenericMaterial",this.getMaterial()); },

	draw:function(/* Material | null */ mat, /* bool */ useMaterialSettings) {
		if (this._polyList && this._visible) {
			vwgl.State.get().pushModelMatrix();
			vwgl.State.get().modelMatrix().mult(this._transform);
			var currentMaterial = null;
			var isTransparent = false;
			if (mat) {
				if (useMaterialSettings && this._material) {
					mat.useSettingsOf(this._material);
					isTransparent = this._material.isTransparent();
				}
				else {
					isTransparent = mat.isTransparent();
				}
				currentMaterial = mat;
			}
			else {
				currentMaterial = this._material;
			}

			if (currentMaterial &&
				((isTransparent && this._renderTransparent) ||
				(!isTransparent && this._renderOpaque)) ) {
				currentMaterial.bindPolyList(this._polyList);
				currentMaterial.activate();

				this._polyList.drawElements();
				currentMaterial.deactivate();
			}

			if (mat) {
				mat.useSettingsOf(null);
			}
			
			vwgl.State.get().popModelMatrix();
		}
	},

	getPickId:function() { return this._pickId; },

	setRenderOpaque:function(opaque) { this._renderOpaque = opaque; },
	getRenderOpaque:function() { return this._renderOpaque; },

	setRenderTransparent:function(transparent) { this._renderTransparent = transparent; },
	getRenderTransparent:function() { return this._renderTransparent; },

	setShadowProjector:function(/* Projector */ proj) {
		if (proj!=null) {
			this._shadowProjector = proj;
			this._shadowProjector.setVisible(this._visible);
			var tex = this._shadowProjector.getTexture();
			if (tex) {
				tex.bindTexture(vwgl.Texture.kTargetTexture2D);
				tex.setTextureWrapS(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClampToEdge);
				tex.setTextureWrapT(vwgl.Texture.kTargetTexture2D, vwgl.Texture.kWrapClampToEdge);
				if (this._drawable && this._drawable.getParent()) this._drawable.getParent().setInitialized(false);
			}
		}
		else {
			this.removeProjectorFromScene();
			if (this._shadowProjector) this._shadowProjector.destroy();
			this._shadowProjector = null;
		}
	},

	getShadowProjector:function() { return this._shadowProjector; },

	setVisible:function(visible) {
		this._visible = visible;
		if (this._shadowProjector) {
			this._shadowProjector.setVisible(this._visible);
		}
	},

	isVisible:function() { return this._visible; },
	show:function() { this.setVisible(true); },
	hide:function() { this.setVisible(false); },

	setGroupName:function(name) { this._groupName = name; },
	getGroupName:function() { return this._groupName; },

	destroy:function() {
		if (this._material) {
			this._material.destroy();
		}
		if (this._polyList) {
			this._polyList.destroy();
		}
		this.removeProjectorFromScene();
		this._material = null;
		this._polyList = null;
		this._shadowProjector = null;
	},

	removeProjectorFromScene:function() {
		if (this._shadowProjector && this._shadowProjector.getParent()) {
			this._shadowProjector.getParent().removeChild(this._shadowProjector);
			this._shadowProjector.destroy();
		}
	}
});
