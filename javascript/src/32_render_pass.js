
Class ("vwgl.RenderPass", {
	_sceneRoot:null,		// vwgl.Group
	_fbo:null,				// vwgl.FramebufferObject
	_renderer:null,			// vwgl.SceneRenderer
	_clearColor:null,		// vwgl.Vector4
	_clearBuffers:true,
	_renderTransparentObjects:true,
	
	initialize:function(/* Group */ sceneRoot, /* Material */ mat, /* FramebufferObject */ fbo) {
		this._clearColor = vwgl.Color.white();
		if (sceneRoot && mat && fbo) {
			this.create(sceneRoot,mat,fbo);
		}
	},

	setClearColor:function(/* Vector4 */ color) { this._clearColor = color; },
	getClearColor:function() { return this._clearColor; },
	
	setSceneRoot:function(/* Group */ sceneRoot) { this._sceneRoot = sceneRoot; },

	create:function(param1, param2, param3) {
		var material = null;
		var fbo = null;
		if (dynamic_cast("vwgl.Group",param1)) {
			this._sceneRoot = param1;
			material = param2;
			fbo = param3;
		}
		else if (dynamic_cast("vwgl.Material",param1)){
			material = param1;
			fbo = param2;
		}
		if (material && fbo) {
			this.createRenderer(material,fbo);
			return true;
		}
		return false;
	},
	
	createRenderer:function(/* Material */ mat, /* FramebufferObject */ fbo) {
		this._renderer = new vwgl.SceneRenderer();
		this._renderer.getNodeVisitor().setMaterial(mat);
		this._renderer.getNodeVisitor().setUseMaterialSettings(true);
		this._fbo = fbo;
	},

	clear:function() {
		if (this._fbo && this._sceneRoot && this._renderer) {
			this._fbo.bind();
			gl.viewport(0, 0, this._fbo.getWidth(), this._fbo.getHeight());
			gl.clearColor(this._clearColor.r(), this._clearColor.g(), this._clearColor.b(), this._clearColor.a());
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
			this._fbo.unbind();
		}
	},

	updateTexture:function(alphaRender) {
		if (this._fbo && this._sceneRoot && this._renderer) {
			this._fbo.bind();
			gl.viewport(0, 0, this._fbo.getWidth(), this._fbo.getHeight());
			gl.clearColor(this._clearColor.r(), this._clearColor.g(), this._clearColor.b(), this._clearColor.a());
			if (this._clearBuffers) gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
			this._renderer.setRenderTransparency(this._renderTransparentObjects);
			this._renderer.draw(this._sceneRoot,alphaRender);
			this._fbo.unbind();
		}
	},

	destroy:function() {
		this._sceneRoot = null;
		this._fbo.destroy();
		this._fbo = null;
		this._renderer.destroy();
		this._renderer = null;
	},

	getFbo:function() { return this._fbo; },
	setFbo:function(/* FramebufferObject */ fbo) { this._fbo = fbo; },

	setMaterial:function(/* vwgl.Material */ mat) { this._renderer.getNodeVisitor().setMaterial(mat); },
	getMaterial:function() { return this._renderer.getNodeVisitor().getMaterial(); },
	
	setClearBuffers:function(clear) { this._clearBuffers = clear; },
	getClearBuffers:function() { return this._clearBuffers; },
	
	setRenderTransparentObjects:function(r) { this._renderTransparentObjects = r; },
	getRenderTransparentObjects:function() { return this._renderTransparentObjects; },
	
	setRenderOpaqueObjects:function(r) { this._renderer.setRenderOpaque(r); },
	getRenderOpaqueObjects:function() { return this._renderer.getRenderOpaque(); },
});
