

Class ("vwgl.DeferredRenderer", vwgl.Renderer, {
	_sceneRoot:null,		// vwgl.Group

	_initVisitor:null,		// vwgl.InitVisitor

	_clearColor:null,

	// Render pass
	_diffuseRP:null,
	_normalRP:null,
	_specularRP:null,
	_depthRP:null,
	_positionRP:null,
	_shadowRP:null,
	_selectionRP:null,

	_projectorMat:null,

	_lightingMaterial:null,
	_lightingCanvas:null,
	_lightingFbo:null,

	_ssaoMaterial:null,
	_homogeneousRenderCanvas:null,
	_ssaoFbo:null,

	_canvas:null,			// vwgl.RenderCanvas

	_postprocessMat:null,	// vwgl.DeferredPostprocess

	getRenderSettings:function() {
		return this._postprocessMat.getRenderSettings();
	},

	initialize:function() {
		this.parent();
		this._initVisitor = new vwgl.InitVisitor();
		this._mapSize = new vwgl.Size2D();
		this._clearColor = vwgl.Color.black();
	},

	build:function(/* Group */ sceneRoot, /* Size2D | number */ sizeOrW, height) {
		if (dynamic_cast("vwgl.Size2D",sizeOrW)) {
			this._mapSize.assign(sizeOrW);
		}
		else {
			this._mapSize.set(sizeOrW,height);
		}
		this.destroy();
		this._sceneRoot = sceneRoot;

		var diffuseMat = new vwgl.DiffuseRenderPassMaterial();
		this._diffuseRP = new vwgl.RenderPass(sceneRoot,diffuseMat,new vwgl.FramebufferObject(this._mapSize.width(),this._mapSize.height()));
		var normalMat = new vwgl.NormalMapDeferredMaterial();
		this._normalRP = new vwgl.RenderPass(sceneRoot,normalMat,new vwgl.FramebufferObject(this._mapSize.width(), this._mapSize.height()));
		this._normalRP.setClearColor(new vwgl.Color(0,0,0,0));
		var specularMat = new vwgl.SpecularRenderPassMaterial();
		this._specularRP = new vwgl.RenderPass(sceneRoot,specularMat,new vwgl.FramebufferObject(this._mapSize.width(), this._mapSize.height()));
		this._specularRP.setClearColor(new vwgl.Color(0,0,0,0));
		var positionMat = new vwgl.WPositionDeferredMaterial();
		this._positionRP = new vwgl.RenderPass(sceneRoot,positionMat, new vwgl.FramebufferObject(this._mapSize.width(),this._mapSize.height(),vwgl.FramebufferObject.kFloatTexture));
		var shadowMat = new vwgl.ShadowRenderPassMaterial();
		this._shadowRP = new vwgl.RenderPass(sceneRoot, shadowMat, new vwgl.FramebufferObject(this._mapSize.width(),this._mapSize.height()));
		this._addFilter(shadowMat);

		var selectionMat = new vwgl.SelectionDeferredMaterial();
		this._selectionRP = new vwgl.RenderPass(sceneRoot,selectionMat, new vwgl.FramebufferObject(this._mapSize.width(),this._mapSize.height()));

		
		this._projectorMat = new vwgl.ProjTextureDeferredMaterial();

		this._lightingMaterial = new vwgl.DeferredLighting();
		this._lightingMaterial.setDiffuseMap(this._diffuseRP.getFbo().getTexture());
		this._lightingMaterial.setNormalMap(this._normalRP.getFbo().getTexture());
		this._lightingMaterial.setSpecularMap(this._specularRP.getFbo().getTexture());
		this._lightingMaterial.setPositionMap(this._positionRP.getFbo().getTexture());
		this._lightingMaterial.setShadowMap(this._shadowRP.getFbo().getTexture());

		this._lightingCanvas = new vwgl.HomogeneousRenderCanvas();
		this._lightingCanvas.setMaterial(this._lightingMaterial);
		this._lightingFbo = new vwgl.FramebufferObject(this._mapSize.width(), this._mapSize.height());

		this._ssaoMaterial = new vwgl.SSAOMaterial();
		this._ssaoMaterial.setPositionMap(this._positionRP.getFbo().getTexture());
		this._ssaoMaterial.setNormalMap(this._normalRP.getFbo().getTexture());
		this._ssaoMaterial.setViewportSize(new vwgl.Vector2(this._viewport.width(), this._viewport.height()));

		this._addFilter(this._ssaoMaterial);
	
		this._ssaoCanvas = new vwgl.HomogeneousRenderCanvas();
		this._ssaoCanvas.setMaterial(this._ssaoMaterial);
		this._ssaoFbo = new vwgl.FramebufferObject(this._mapSize.width(), this._mapSize.height());
		
		this._postprocessMat = new vwgl.DeferredPostprocess();
		this._postprocessMat.setDiffuseMap(this._lightingFbo.getTexture());
		this._postprocessMat.setPositionMap(this._positionRP.getFbo().getTexture());
		this._postprocessMat.setSelectionMap(this._selectionRP.getFbo().getTexture());
		this._postprocessMat.setSSAOMap(this._ssaoFbo.getTexture());
		
		this._addFilter(this._postprocessMat);
		
		this._canvas = new vwgl.HomogeneousRenderCanvas();
		this._canvas.setMaterial(this._postprocessMat);
	},

	resize:function(/* vwgl,Size2D */ size) {
		this._mapSize = new vwgl.Size2D(size);

		this._diffuseRP.getFbo().resize(this._mapSize.width(),this._mapSize.height());
		this._normalRP.getFbo().resize(this._mapSize.width(),this._mapSize.height());
		this._specularRP.getFbo().resize(this._mapSize.width(),this._mapSize.height());
		this._positionRP.getFbo().resize(this._mapSize.width(),this._mapSize.height());
		this._shadowRP.getFbo().resize(this._mapSize.width(),this._mapSize.height());
		this._selectionRP.getFbo().resize(this._mapSize.width(),this._mapSize.height());

		this._lightingFbo.resize(this._mapSize.width(),this._mapSize.height());
		
		this._ssaoMaterial.setViewportSize(new vwgl.Vector2(this._viewport.width(),this._viewport.height()));
		this._ssaoFbo.resize(this._mapSize.width(), this._mapSize.height());
	},

	destroy:function() {
		if (!this._canvas) return;
		this._diffuseRP.destroy();
		this._normalRP.destroy();
		this._specularRP.destroy();
		this._positionRP.destroy();
		this._shadowRP.destroy();
		this._selectionRP.destroy();

		this._projectorMat.destroy();

		this._lightingMaterial.destroy();
		this._lightingCanvas.destroy();
		this._lightingFbo.destroy
		
		this._ssaoMaterial = null;
		this._ssaoCanvas = null;
		this._ssaoFbo = null;

		this._postprocessMat.destroy();
		this._canvas.destroy();
		this._canvas = null;
	},

	draw:function(/* Camera | null */ cam) {
		vwgl.RenderSettings.setCurrentRenderSettings(this.getRenderSettings());
		
		this._postprocessMat.setSSAOBlur(this._ssaoMaterial.getBlurIterations());
		
		this._initVisitor.visit(this._sceneRoot);

		vwgl.LightManager.get().prepareFrame();

		vwgl.CameraManager.get().applyTransform(cam);

		this.geometryPass();

		if ((this._updateBuffers & vwgl.Renderer.kLightingBuffer)!=0) {
			var lightList = vwgl.LightManager.get().getLightList();
			this._lightingCanvas.setClearBuffer(true);
			var setBlend = false;
		
			var enabledLights = [];
			for (var i=0; i<lightList.length; ++i) {
				if (lightList[i].branchEnabled()) {
					enabledLights.push(lightList[i]);
				}
			}
		
			this._shadowRP.clear();
			for (var i=0; i<enabledLights.length; ++i) {
				var shadowLight = dynamic_cast("vwgl.ShadowLight",enabledLights[i]);
				var shadowMat = dynamic_cast("vwgl.ShadowRenderPassMaterial",this._shadowRP.getMaterial());
				if (shadowLight && shadowLight.getCastShadows() && shadowMat) {
					gl.enable(gl.DEPTH_TEST);
					gl.disable(gl.BLEND);
					vwgl.LightManager.get().updateShadowMap(this._sceneRoot, shadowLight);
					shadowMat.setLight(shadowLight);
					shadowMat.setShadowTexture(vwgl.LightManager.get().getShadowTexture());
					this._shadowRP.setClearBuffers(true);
					this._shadowRP.updateTexture();
					this._lightingMaterial.setShadowMap(this._shadowRP.getFbo().getTexture());
				}
				else {
					this._lightingMaterial.setShadowMap(vwgl.TextureManager.get().whiteTexture());
				}
				this._lightingFbo.bind();
				if (setBlend) {
					gl.disable(gl.DEPTH_TEST);
					gl.enable(gl.BLEND);
					gl.blendFunc(gl.ONE, gl.ONE);
				}
				this._lightingMaterial.setLight(enabledLights[i]);
				this._lightingMaterial.setEnabledLights(enabledLights.length);
				this._lightingCanvas.draw(this._viewport.width(), this._viewport.height());
				this._lightingCanvas.setClearBuffer(false);
				setBlend = true;
				this._lightingFbo.unbind();
			}
		
			gl.enable(gl.DEPTH_TEST);
			gl.disable(gl.BLEND);
			if (!setBlend) {
				// The scene does not contain any light or there are all disabled
				this._lightingFbo.bind();
				this._lightingMaterial.setLight(null);
				this._lightingCanvas.draw(this._viewport.width(), this._viewport.height());
				this._lightingCanvas.setClearBuffer(false);
				this._lightingFbo.unbind();
			}
		}
		

		if ((this._updateBuffers & vwgl.Renderer.kSSAOBuffer)!=0) {
			this._ssaoFbo.bind();
			this._ssaoCanvas.setClearBuffer(true);
			this._ssaoCanvas.draw(this._viewport.width(), this._viewport.height());
			this._ssaoFbo.unbind();
		}
		
		this._canvas.draw(this._viewport.width(), this._viewport.height());
	},

	geometryPass:function() {
		this._clearColor.a(0.0);

		if ((this._updateBuffers & vwgl.Renderer.kDiffuseBuffer) !=0) {
			gl.enable(gl.POLYGON_OFFSET_FILL);
			gl.polygonOffset(1.0, 1.0);
			this._diffuseRP.setClearColor(this._clearColor);
			this._diffuseRP.updateTexture(true);
			gl.disable(gl.POLYGON_OFFSET_FILL);

			var proj = vwgl.ProjectorManager.get().getProjectorList();
			var diffuseMat = this._diffuseRP.getMaterial();
			this._diffuseRP.setMaterial(this._projectorMat);
			this._diffuseRP.setClearBuffers(false);
			gl.depthMask(false);
			for (var i=0; i<proj.length;++i) {
				if (proj[i].isVisible()) {
					gl.enable(gl.BLEND);
					gl.blendFunc(gl.DST_COLOR, gl.ONE_MINUS_SRC_ALPHA);
					this._projectorMat.setProjector(proj[i]);
					this._diffuseRP.updateTexture(true);
				}
			}
			gl.disable(gl.BLEND);
			gl.depthMask(true);
			this._diffuseRP.setClearBuffers(true);
			this._diffuseRP.setMaterial(diffuseMat);
		}
		
		if ((this._updateBuffers & vwgl.Renderer.kNormalBuffer)!=0) {
			this._normalRP.updateTexture();
		}
		
		if ((this._updateBuffers & vwgl.Renderer.kSpecularBuffer)!=0) {
			this._specularRP.updateTexture();
		}

		if ((this._updateBuffers & vwgl.Renderer.kPositionBuffer)!=0) {
			this._positionRP.updateTexture();
		}
		
		this._selectionRP.updateTexture();
	},

	setClearColor:function(/* Vector4 */ color) { this._clearColor = color; }
});
