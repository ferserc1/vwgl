
Class ("vwgl.SceneSet", {
	_lights:null,
	_cameras:null,
	_sceneRoot:null,
	
	initialize:function() {
		this._sceneRoot = new vwgl.Group();
		this._lights = {};
		this._cameras = {};
	},

	buildDefault:function() {
		var cam = new vwgl.Camera();
		var trx = vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(20.0), 0.0, 1.0, 0.0);
		trx.rotate(vwgl.Math.degreesToRadians(20.0), 1.0, 0.0, 0.0);
		trx.translate(0.0, 0.0, 5.0);
		this.addCamera(vwgl.SceneSet.s_mainCameraName, cam, trx);

		var light = new vwgl.ShadowLight();
		light.getProjectionMatrix().ortho(-5.0, 5.0, -5.0, 5.0, 0.1, 100.0);
		var ltrx = vwgl.Matrix4.makeRotation(vwgl.Math.degreesToRadians(-20.0), 0.0, 1.0, 0.0);
		ltrx.rotate(vwgl.Math.degreesToRadians(-20.0), 1.0, 0.0, 0.0);
		ltrx.translate(0.0, 0.0, 5.0);
		this.addLight(vwgl.SceneSet.s_mainLightName, light, ltrx);
	},
	
	addLight:function(name, /* Light */ light, /* Matrix4 */ transform) {
		if (!this._lights[name]) {
			var trx = new vwgl.TransformNode(transform);
			this._sceneRoot.addChild(trx);
			trx.addChild(light);
			this._lights[name] = light;
			return true;
		}
		return false;
	},
	
	addCamera:function(name, /* Camera */ cam, /* Matrix4 */ transform) {
		if (!this._cameras[name]) {
			var trx = new vwgl.TransformNode(transform);
			this._sceneRoot.addChild(trx);
			trx.addChild(cam);
			this._cameras[name] = cam;
			return true;
		}
		return false;
	},
	
	getLight:function(name) {
		return this._lights[name];
	},

	getMainLight:function() {
		return  this.getLight(vwgl.SceneSet.s_mainLightName);
	},

	getCamera:function(name) {
		return this._cameras[name];
	},

	getMainCamera:function() {
		return vwgl.CameraManager.get().getMainCamera(); 
	},

	getLightTransform:function(name) {
		if (this._lights[name]) {
			return dynamic_cast("vwgl.TransformNode",this._lights[name].getParent());
		}
		return null;
	},

	getMainLightTransform:function() {
		return this.getLightTransform(vwgl.SceneSet.s_mainLightName);
	},

	getCameraTransform:function(name) {
		if (this._cameras[name]) {
			return dynamic_cast("vwgl.TransformNode",this._cameras[name].getParent());
		}
		return null;
	},

	getMainCameraTransform:function() {
		return this.getCameraTransform(vwgl.SceneSet.s_mainCameraName);
	},

	removeLight:function(name) {
		if (this._lights[name]) {
			var light = this._lights[name];
			var trx = dynamic_cast("vwgl.TransformNode",light.getParent());
			if (trx) {
				this._sceneRoot.removeChild(trx);
			}
			delete this._lights[name];
			return true;
		}
		return false;
	},

	removeCamera:function(name) {
		if (this._cameras[name]) {
			var cam = this._cameras[name];
			var trx = dynamic_cast("vwgl.TransformNode",cam.getParent());
			if (trx) {
				this._sceneRoot.removeChild(trx);
			}
			delete this._cameras[name];
			return true;
		}
		return false;
	},

	getSceneRoot:function() { return this._sceneRoot; },
	
	getLightList:function() { return this._lights; },
	getCameraList:function() { return this._cameras; },

	destroy:function() {
		this._lights = {};
		this._cameras = {};
	}
});

vwgl.SceneSet.s_mainCameraName = "mainCamera";
vwgl.SceneSet.s_mainLightName = "mainLight";
