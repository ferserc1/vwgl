vwgl.deferred_shaders ={
	texture:{
		vertex:"",
		fragment:""
	}
}


vwgl.deferred_shaders.texture = {
	vertex:"\
			#ifdef GL_ES\n\
			precision highp float;\n\
			#endif\n\
			attribute vec3 aVertexPosition;\n\
			attribute vec3 aNormalPosition;\n\
			attribute vec2 aTexturePosition;\n\
			\n\
			uniform mat4 uMVMatrix;\n\
			uniform mat4 uPMatrix;\n\
			uniform mat4 uMMatrix;\n\
			uniform mat4 uNMatrix;\n\
			uniform mat4 uVMatrixInv;\n\
			\n\
			varying vec2 vTexturePosition;\n\
			varying vec4 vPosition;\n\
			varying vec3 vNormal;\n\
			varying vec3 vSurfaceToView;\n\
			\n\
			void main() {\n\
				vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
				gl_Position = vPosition;\n\
				vTexturePosition = aTexturePosition;\n\
				\n\
				vec4 transformedNormal = uNMatrix * vec4(aNormalPosition,1.0);\n\
				vNormal = normalize(transformedNormal.xyz);\n\
				vSurfaceToView = (uVMatrixInv[3] - (uMMatrix * vec4(aVertexPosition,1.0))).xyz;\n\
			}\n\
			",

	fragment:"\
			#ifdef GL_ES\n\
			precision highp float;\n\
			#endif\n\
			\n\
			varying vec2 vTexturePosition;\n\
			varying vec4 vPosition;\n\
			varying vec3 vNormal;\n\
			varying vec3 vSurfaceToView;\n\
			\n\
			uniform sampler2D uTexture;\n\
			uniform vec4 uColorTint;\n\
			uniform bool uSelectMode;\n\
			uniform bool uIsEnabled;\n\
			\n\
			uniform mat4 uNMatrix;\n\
			uniform sampler2D uNormalMap;\n\
			uniform bool uUseNormalMap;\n\
			uniform vec2 uNormalMapScale;\n\
			uniform vec2 uNormalMapOffset;\n\
			\n\
			uniform vec2 uTextureOffset;\n\
			uniform vec2 uTextureScale;\n\
			\n\
			uniform samplerCube uCubeMap;\n\
			uniform bool uUseCubeMap;\n\
			uniform float uReflectionAmount;\n\
			\n\
			vec3 getNormal() {\n\
				vec3 normal = vNormal;\n\
				if (uUseNormalMap) {\n\
					vec3 tangent = normalize(uNMatrix[0].xyz);\n\
					vec3 binormal = normalize(uNMatrix[1].xyz);\n\
					mat3 tangentToWorld = mat3(tangent.x, binormal.x, normal.x,\n\
											   tangent.y, binormal.y, normal.y,\n\
											   tangent.z, binormal.z, normal.z);\n\
					normal = (texture2D(uNormalMap, vTexturePosition * uNormalMapScale + uNormalMapOffset).rgb * 2.0 - 1.0) * tangentToWorld;\n\
				}\n\
				normalize(normal);\n\
				return normal;\n\
			}\n\
			\n\
			vec4 getCubeColor(vec3 normal) {\n\
				vec3 surfaceToView = normalize(vSurfaceToView);\n\
				vec3 reflected = reflect(surfaceToView,-normal);\n\
				return textureCube(uCubeMap,reflected);\n\
			}\n\
			\n\
			vec4 getSelectionColor() {\n\
				vec4 selectionColor = vec4(0.0,0.0,0.0,0.0);\n\
				float size = 0.9;\n\
			\n\
				if (uSelectMode) {\n\
					float intValueX = floor(vPosition.x);\n\
					float intValueY = floor(vPosition.y);\n\
					float decValueX = vPosition.x - intValueX;\n\
					float decValueY = vPosition.y - intValueY;\n\
					if (!((decValueX>size && decValueY>size) ||\n\
						  (decValueX<size && decValueY<size))) {\n\
						selectionColor = vec4(0.5,0.5,0.5,0.0);\n\
					}\n\
				}\n\
				return selectionColor;\n\
			}\n\
			\n\
			void main(void) {\n\
				vec4 texColor = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset);\n\
				if (!uIsEnabled) {\n\
					texColor = vec4(1.0);\n\
				}\n\
				\n\
				vec4 cmColor = vec4(0.0,0.0,0.0,0.0);\n\
				if (uUseCubeMap) {\n\
					cmColor = getCubeColor(getNormal());\n\
				}\n\
				\n\
				vec4 color = uColorTint * vec4(1.0 - uReflectionAmount);\n\
				gl_FragColor = texColor * color + cmColor * uReflectionAmount + getSelectionColor();\n\
			}"
}

vwgl.deferred_shaders.lightmap = {
	vertex : "\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	attribute vec3 aVertexPosition;\n\
	attribute vec2 aTexturePosition;\n\
	attribute vec2 aLightmapPosition;\n\
\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
\n\
	varying vec2 vTexturePosition;\n\
	varying vec4 vPosition;\n\
\n\
	void main() {\n\
		vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
		gl_Position = vPosition;\n\
		vTexturePosition = aTexturePosition;\n\
	}",

	fragment : "\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
\n\
	varying vec2 vTexturePosition;\n\
	varying vec4 vPosition;\n\
\n\
	uniform sampler2D uTexture;\n\
	uniform vec4 uColorTint;\n\
	uniform bool uSelectMode;\n\
	uniform bool uIsEnabled;\n\
\n\
	uniform vec2 uTextureOffset;\n\
	uniform vec2 uTextureScale;\n\
\n\
\n\
	void main(void) {\n\
		vec4 texColor = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset);\n\
		if (!uIsEnabled) {\n\
			texColor = vec4(1.0);\n\
		}\n\
\n\
		gl_FragColor = texColor * uColorTint;\n\
	}"
}

vwgl.deferred_shaders.normal = {
	vertex:"#ifdef GL_ES\n\
			precision highp float;\n\
			#endif\n\
			attribute vec3 aVertexPos;\n\
			attribute vec3 aNormalPos;\n\
			attribute vec2 aNormalMapPos;\n\
			attribute vec2 aLightNormalMapPos;\n\
			attribute vec3 aTangentPos;\n\
			\n\
			uniform mat4 uMMatrix;\n\
			uniform mat4 uVMatrix;\n\
			uniform mat4 uPMatrix;\n\
			uniform mat4 uNMatrix;\n\
			\n\
			varying vec2 vNormalMapPos;\n\
			varying vec2 vLightNormalMapPos;\n\
			varying vec3 vNormal;\n\
			varying vec3 vTangent;\n\
			varying vec3 vBitangent;\n\
			\n\
			void main() {\n\
				gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aVertexPos,1.0);\n\
				vNormalMapPos = aNormalMapPos;\n\
				vLightNormalMapPos = aLightNormalMapPos;\n\
				vNormal = normalize((uNMatrix * vec4(aNormalPos,1.0)).rgb);\n\
				vTangent = normalize((uNMatrix * vec4(aTangentPos,1.0)).rgb);\n\
				vBitangent = cross(vNormal,vTangent);\n\
			}\n\
			",

	fragment:"\
			#ifdef GL_ES\n\
			precision highp float;\n\
			precision highp int;\n\
			#endif\n\
			\n\
			varying vec2 vNormalMapPos;\n\
			varying vec2 vLightNormalMapPos;\n\
			varying vec3 vNormal;\n\
			varying vec3 vTangent;\n\
			varying vec3 vBitangent;\n\
			\n\
			uniform sampler2D uNormalMap;\n\
			uniform sampler2D uLightNormalMap;\n\
			uniform bool uUseNormalMap;\n\
			uniform bool uUseLightNormalMap;\n\
			uniform vec2 uNormalMapScale;\n\
			uniform vec2 uNormalMapOffset;\n\
			uniform float uLightEmission;\n\
			uniform sampler2D uLightEmissionMask;\n\
			uniform int uLightEmissionMaskChannel;\n\
			uniform float uLightEmissionMaskInvert;\n\
			\n\
			// use the alpha texture component to know if we discard the fragment\n\
			uniform sampler2D uTexture;\n\
			uniform vec2 uTextureOffset;\n\
			uniform vec2 uTextureScale;\n\
			uniform float uAlphaCutoff;\n\
			\n\
			vec3 getNormal(vec3 normalCoord, vec3 normalMapValue, vec3 tangent, vec3 bitangent, bool useNormalMap) {\n\
				vec3 normal = normalCoord;\n\
				if (useNormalMap) {\n\
					mat3 tbnMat = mat3(	tangent.x, bitangent.x, normalCoord.x,\n\
									   tangent.y, bitangent.y, normalCoord.y,\n\
									   tangent.z, bitangent.z, normalCoord.z);\n\
					normal = normalize(normalMapValue * tbnMat);\n\
				}\n\
				normalize(normal);\n\
				return normal;\n\
			}\n\
			\n\
			void main() {\n\
				vec4 tex = texture2D(uTexture,vNormalMapPos * uTextureScale + uTextureOffset);\n\
				if (tex.a<=uAlphaCutoff) {\n\
					discard;\n\
				}\n\
				else {\n\
					vec3 nmap = normalize(texture2D(uNormalMap, vNormalMapPos * uNormalMapScale + uNormalMapOffset).rgb * 2.0 - 1.0);\n\
					vec3 lnmap = normalize(texture2D(uLightNormalMap, vLightNormalMapPos).rgb * 2.0 - 1.0);\n\
					vec3 normal = getNormal(vNormal,\n\
											nmap,\n\
											vTangent, vBitangent, uUseNormalMap);\n\
					normal = normal * 0.5 + 0.5;\n\
					\n\
					vec4 lightEmissionMaskColor = texture2D(uLightEmissionMask,vNormalMapPos * uTextureScale + uTextureOffset);\n\
					float lightEmissionMask = lightEmissionMaskColor.r;\n\
					if (uLightEmissionMaskChannel==1) {\n\
						lightEmissionMask = lightEmissionMaskColor.g;\n\
					}\n\
					else if (uLightEmissionMaskChannel==2) {\n\
						lightEmissionMask = lightEmissionMaskColor.b;\n\
					}\n\
					else if (uLightEmissionMaskChannel==3) {\n\
						lightEmissionMask = lightEmissionMaskColor.a;\n\
					}\n\
					\n\
					lightEmissionMask = 1.0 - lightEmissionMask;\n\
					\n\
					float lightEmission = uLightEmission * abs(uLightEmissionMaskInvert - lightEmissionMask);\n\
					gl_FragColor = vec4(normal,lightEmission);\n\
				}\n\
			}\n\
			"
}

vwgl.deferred_shaders.selection = {
	vertex:"\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	attribute vec3 aVertexPosition;\n\
\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
\n\
	void main() {\n\
		gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	}",

	fragment:"\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	uniform bool uSelected;\n\
	uniform vec4 uColor;\n\
\n\
	void main() {\n\
		if (uSelected) {\n\
			gl_FragColor = uColor;\n\
		}\n\
		else {\n\
			discard;\n\
		}\n\
	}"
}

vwgl.deferred_shaders.wposition = {
	vertex:"\
		#ifdef GL_ES\n\
		precision highp float;\n\
		#endif\n\
		attribute vec3 aVertexPosition;\n\
		attribute vec2 aTexturePosition;\n\
		\n\
		uniform mat4 uMVMatrix;\n\
		uniform mat4 uPMatrix;\n\
		uniform mat4 uMMatrix;\n\
		\n\
		varying vec2 vTexturePosition;\n\
		\n\
		varying vec4 vPosition;\n\
		\n\
		void main() {\n\
			vTexturePosition = aTexturePosition;\n\
			vPosition = uMVMatrix * vec4(aVertexPosition,1.0);\n\
			gl_Position = uPMatrix * vPosition;\n\
		}\
		",

	fragment:"\
		#ifdef GL_ES\n\
		precision highp float;\n\
		#endif\n\
		\n\
		varying vec4 vPosition;\n\
		varying vec2 vTexturePosition;\n\
		\n\
		uniform sampler2D uTexture;\n\
		uniform vec2 uTextureScale;\n\
		uniform vec2 uTextureOffset;\n\
		uniform float uAlphaCutoff;\n\
		\n\
		void main() {\n\
			float alpha = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset).a;\n\
			if (alpha<=uAlphaCutoff) {\n\
				discard;\n\
			}\n\
			else {\n\
				gl_FragColor = vec4(vPosition.xyz,gl_FragCoord.z);\n\
			}\n\
		}\
		"
}

vwgl.deferred_shaders.material = {
	vertex:"\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
	attribute vec3 aVertexPosition;\n\
\n\
	uniform mat4 uMVMatrix;\n\
	uniform mat4 uPMatrix;\n\
\n\
	void main() {\n\
		gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
	}",

	fragment:"\
	#ifdef GL_ES\n\
	precision highp float;\n\
	#endif\n\
\n\
	uniform float uShininess;\n\
	uniform float uRefractionAmount;\n\
	uniform float uLightEmission;\n\
\n\
	void main() {\n\
		gl_FragColor = vec4(uShininess/255.0,uRefractionAmount,uLightEmission,0.0);\n\
	}"
}



/////// New shaders
// Diffuse gbuffer shaders
vwgl.deferred_shaders.diffuse = {
	vertex:"\
			#ifdef GL_ES\n\
			precision highp float;\n\
			#endif\n\
			attribute vec3 aVertexPosition;\n\
			attribute vec3 aNormalPosition;\n\
			attribute vec2 aTexturePosition;\n\
			attribute vec2 aLightmapPosition;\n\
			\n\
			uniform mat4 uMVMatrix;\n\
			uniform mat4 uPMatrix;\n\
			uniform mat4 uMMatrix;\n\
			uniform mat4 uNMatrix;\n\
			uniform mat4 uVMatrixInv;\n\
			\n\
			varying vec2 vTexturePosition;\n\
			varying vec2 vLightmapPosition;\n\
			varying vec4 vPosition;\n\
			varying vec3 vNormal;\n\
			varying vec3 vSurfaceToView;\n\
			\n\
			void main() {\n\
				vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
				gl_Position = vPosition;\n\
				vTexturePosition = aTexturePosition;\n\
				vLightmapPosition = aLightmapPosition;\n\
				\n\
				vec4 transformedNormal = uNMatrix * vec4(aNormalPosition,1.0);\n\
				vNormal = normalize(transformedNormal.xyz);\n\
				vSurfaceToView = (uVMatrixInv[3] - (uMMatrix * vec4(aVertexPosition,1.0))).xyz;\n\
			}\n\
			",

	fragment:"\
			#ifdef GL_ES\n\
			precision highp float;\n\
			#endif\n\
			\n\
			varying vec2 vTexturePosition;\n\
			varying vec2 vLightmapPosition;\n\
			varying vec4 vPosition;\n\
			varying vec3 vNormal;\n\
			varying vec3 vSurfaceToView;\n\
			\n\
			uniform sampler2D uTexture;\n\
			uniform vec4 uColorTint;\n\
			uniform bool uSelectMode;\n\
			uniform bool uIsEnabled;\n\
			\n\
			uniform bool uUseLightmap;\n\
			uniform sampler2D uLightmap;\n\
			uniform vec2 uLightmapOffset;\n\
			uniform vec2 uLightmapScale;\n\
			\n\
			uniform mat4 uNMatrix;\n\
			uniform sampler2D uNormalMap;\n\
			uniform bool uUseNormalMap;\n\
			uniform vec2 uNormalMapScale;\n\
			uniform vec2 uNormalMapOffset;\n\
			\n\
			uniform vec2 uTextureOffset;\n\
			uniform vec2 uTextureScale;\n\
			\n\
			uniform samplerCube uCubeMap;\n\
			uniform bool uUseCubeMap;\n\
			uniform float uReflectionAmount;\n\
			uniform float uAlphaCutoff;\n\
			\n\
			vec3 getNormal() {\n\
				vec3 normal = vNormal;\n\
				if (uUseNormalMap) {\n\
					vec3 tangent = normalize(uNMatrix[0].xyz);\n\
					vec3 binormal = normalize(uNMatrix[1].xyz);\n\
					mat3 tangentToWorld = mat3(tangent.x, binormal.x, normal.x,\n\
					tangent.y, binormal.y, normal.y,\n\
					tangent.z, binormal.z, normal.z);\n\
					normal = (texture2D(uNormalMap, vTexturePosition * uNormalMapScale + uNormalMapOffset).rgb * 2.0 - 1.0) * tangentToWorld;\n\
				}\n\
				normalize(normal);\n\
				return normal;\n\
			}\n\
			\n\
			vec4 getCubeColor(vec3 normal) {\n\
				vec3 surfaceToView = normalize(vSurfaceToView);\n\
				vec3 reflected = reflect(surfaceToView,-normal);\n\
				return textureCube(uCubeMap,reflected);\n\
			}\n\
			\n\
			vec4 getSelectionColor() {\n\
				vec4 selectionColor = vec4(0.0,0.0,0.0,0.0);\n\
				float size = 0.9;\n\
				\n\
				if (uSelectMode) {\n\
					float intValueX = floor(vPosition.x);\n\
					float intValueY = floor(vPosition.y);\n\
					float decValueX = vPosition.x - intValueX;\n\
					float decValueY = vPosition.y - intValueY;\n\
					if (!((decValueX>size && decValueY>size) ||\n\
						  (decValueX<size && decValueY<size))) {\n\
						selectionColor = vec4(0.5,0.5,0.5,0.0);\n\
					}\n\
				}\n\
				return selectionColor;\n\
			}\n\
			\n\
			void main(void) {\n\
				vec4 texColor = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset);\n\
				if (!uIsEnabled) {\n\
					texColor = vec4(1.0);\n\
				}\n\
				\n\
				if (texColor.a<=uAlphaCutoff) {\n\
					discard;\n\
				}\n\
				else {\n\
					if (uUseLightmap) {\n\
						texColor *= texture2D(uLightmap,vLightmapPosition * uLightmapScale + uLightmapOffset);\n\
					}\n\
					vec4 cmColor = vec4(0.0,0.0,0.0,0.0);\n\
					if (uUseCubeMap) {\n\
						cmColor = getCubeColor(getNormal());\n\
					}\n\
					\n\
					vec4 color = uColorTint * vec4(1.0 - uReflectionAmount);\n\
					gl_FragColor = texColor * color + cmColor * uReflectionAmount + getSelectionColor();\n\
				}\n\
			}\n\
			"
}

// Specular gbuffer shader
vwgl.deferred_shaders.specular = {
	vertex:"#ifdef GL_ES\n\
			precision highp float;\n\
			#endif\n\
			attribute vec3 aVertexPosition;\n\
			attribute vec2 aTexturePosition;\n\
			\n\
			uniform mat4 uMVMatrix;\n\
			uniform mat4 uPMatrix;\n\
			\n\
			varying vec2 vTexturePosition;\n\
			void main() {\n\
				gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
				vTexturePosition = aTexturePosition;\n\
			}\n\
			",

	fragment:"#ifdef GL_ES\n\
			precision highp float;\n\
			#endif\n\
			\n\
			uniform vec4 uSpecularColor;\n\
			uniform float uShininess;\n\
			uniform sampler2D uTexture;\n\
			uniform vec2 uTextureOffset;\n\
			uniform vec2 uTextureScale;\n\
			uniform float uAlphaCutoff;\n\
			uniform sampler2D uShininessMask;\n\
			uniform int uShininessMaskComponent;\n\
			uniform float uShininessMaskInvert;\n\
			\n\
			varying vec2 vTexturePosition;\n\
			\n\
			void main(void) {\n\
				vec4 tex = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset);\n\
				if (tex.a<=uAlphaCutoff) {\n\
					discard;\n\
				}\n\
				else {\n\
					vec4 shininessMaskColor = texture2D(uShininessMask,vTexturePosition * uTextureScale + uTextureOffset);\n\
					float shininessMask = shininessMaskColor.r;\n\
					if (uShininessMaskComponent==1) {\n\
						shininessMask = shininessMaskColor.g;\n\
					}\n\
					else if (uShininessMaskComponent==2) {\n\
						shininessMask = shininessMaskColor.b;\n\
					}\n\
					else if (uShininessMaskComponent==3) {\n\
						shininessMask = shininessMaskColor.a;\n\
					}\n\
					vec4 color = uSpecularColor  * abs(uShininessMaskInvert - shininessMask);\n\
					color.a = uShininess / 255.0;\n\
					gl_FragColor = color;\n\
				}\n\
			}"
}


// Shadow gbuffer
vwgl.deferred_shaders.shadow = {
	vertex:"\
		#ifdef GL_ES\n\
		precision highp float;\n\
		#endif\n\
		attribute vec3 aVertexPosition;\n\
		attribute vec3 aNormalPosition;\n\
		attribute vec2 aTexturePosition;\n\
		uniform mat4 uMVMatrix;\n\
		uniform mat4 uPMatrix;\n\
		uniform mat4 uMMatrix;\n\
		uniform mat4 uNMatrix;\n\
		uniform mat4 uLightProjectionMatrix;\n\
		uniform mat4 uLightViewMatrix;\n\
		uniform vec3 uLightDirection;\n\
		varying vec4 vVertexPosFromLight;\n\
		varying vec4 vWorldVertex;\n\
		varying vec4 vPosition;\n\
		varying vec3 vTransformedNormal;\n\
		varying vec2 vTexturePosition;\n\
		\n\
		const mat4 ScaleMatrix = mat4(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.5, 1.0);\n\
		\n\
		void main() {\n\
			vPosition = uPMatrix * uMVMatrix * vec4(aVertexPosition,1.0);\n\
			gl_Position = vPosition;\n\
		\n\
			vWorldVertex = uMMatrix * vec4(aVertexPosition,1.0);\n\
			vVertexPosFromLight = ScaleMatrix * uLightProjectionMatrix * uLightViewMatrix * vWorldVertex;\n\
		\n\
			vTransformedNormal = (uNMatrix * vec4(aNormalPosition,1.0)).xyz;\n\
			vTexturePosition = aTexturePosition;\n\
		}\
		",

	fragment:"\
		#ifdef GL_ES\n\
		precision highp float;\n\
		#endif\n\
		\n\
		uniform sampler2D uDepthMap;\n\
		uniform vec2 uDepthMapSize;\n\
		uniform vec3 uLightDirection;\n\
		uniform bool uReceiveShadows;\n\
		uniform vec4 uShadowColor;\n\
		uniform float uShadowStrength;\n\
		\n\
		const float Near = 1.0;\n\
		const float Far = 30.0;\n\
		const float LinearDepthConstant = 1.0 / (Far - Near);\n\
		\n\
		varying vec4 vVertexPosFromLight;\n\
		varying vec4 vWorldVertex;\n\
		varying vec3 vTransformedNormal;\n\
		varying vec4 vPosition;\n\
		\n\
		varying vec2 vTexturePosition;\n\
		\n\
		uniform sampler2D uTexture;\n\
		uniform vec2 uTextureScale;\n\
		uniform vec2 uTextureOffset;\n\
		uniform float uAlphaCutoff;\n\
		uniform float uShadowBias;\n\
		\n\
		/*\n\
		 *	1: hard shadows\n\
		 *	2: soft shadows\n\
		 *	3: soft stratified shadows\n\
		 */\n\
		uniform int uShadowType;\n\
		\n\
		float unpack (vec4 colour) {\n\
			const vec4 bitShifts = vec4(1.0 / (256.0 * 256.0 * 256.0),\n\
										1.0 / (256.0 * 256.0),\n\
										1.0 / 256.0,\n\
										1);\n\
			return dot(colour , bitShifts);\n\
		}\n\
		\n\
		float kShadowBorderOffset = 3.0;\n\
		\n\
		float random(vec3 seed, int i){\n\
			vec4 seed4 = vec4(seed,i);\n\
			float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));\n\
			return fract(sin(dot_product) * 43758.5453);\n\
		}\n\
		\n\
		void main(void) {\n\
			float alpha = texture2D(uTexture,vTexturePosition * uTextureScale + uTextureOffset).a;\n\
			if (alpha<=uAlphaCutoff) {\n\
				discard;\n\
			}\n\
			else {\n\
				float visibility = 1.0;\n\
				vec3 depth = vVertexPosFromLight.xyz / vVertexPosFromLight.w;\n\
				\n\
				float shadowBorderOffset = kShadowBorderOffset / uDepthMapSize.x;\n\
				float bias = uShadowBias;\n\
				vec4 shadow = vec4(1.0);\n\
				if (uShadowType==0) {\n\
					float shadowDepth = unpack(texture2D(uDepthMap,depth.xy));\n\
					if (shadowDepth<depth.z - bias\n\
						&& (depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0) && uReceiveShadows) {\n\
						visibility = 1.0 - uShadowStrength;\n\
					}\n\
					shadow = clamp(uShadowColor + visibility,0.0,1.0);\n\
				}\n\
				//	Stratified soft shadows are not supported in WebGL due to it's necessary the access of non-constant indexes of arrays\n\
				else if (uShadowType==1  || uShadowType==2) {\n\
					vec2 poissonDisk[4];\n\
					poissonDisk[0] = vec2( -0.94201624, -0.39906216 );\n\
					poissonDisk[1] = vec2( 0.94558609, -0.76890725 );\n\
					poissonDisk[2] = vec2( -0.094184101, -0.92938870 );\n\
					poissonDisk[3] = vec2( 0.34495938, 0.29387760 );\n\
					\n\
					for (int i=0; i<4; ++i) {\n\
						float shadowDepth = unpack(texture2D(uDepthMap, depth.xy + poissonDisk[i]/1000.0));\n\
						\n\
						if (shadowDepth<depth.z - bias\n\
							&& (depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0) && uReceiveShadows) {\n\
							visibility -= (uShadowStrength) * 0.25;\n\
						}\n\
					}\n\
					shadow = clamp(uShadowColor + visibility,0.0,1.0);\n\
				}\n\
			//	else if (uShadowType==2) {\n\
			//		vec2 poissonDisk[16];\n\
			//		poissonDisk[0] = vec2( -0.94201624, -0.39906216 );\n\
			//		poissonDisk[1] = vec2( 0.94558609, -0.76890725 );\n\
			//		poissonDisk[2] = vec2( -0.094184101, -0.92938870 );\n\
			//		poissonDisk[3] = vec2( 0.34495938, 0.29387760 );\n\
			//		poissonDisk[4] = vec2( -0.91588581, 0.45771432 );\n\
			//		poissonDisk[5] = vec2( -0.81544232, -0.87912464 );\n\
			//		poissonDisk[6] = vec2( -0.38277543, 0.27676845 );\n\
			//		poissonDisk[7] = vec2( 0.97484398, 0.75648379 );\n\
			//		poissonDisk[8] = vec2( 0.44323325, -0.97511554 );\n\
			//		poissonDisk[9] = vec2( 0.53742981, -0.47373420 );\n\
			//		poissonDisk[10] = vec2( -0.26496911, -0.41893023 );\n\
			//		poissonDisk[11] = vec2( 0.79197514, 0.19090188 );\n\
			//		poissonDisk[12] = vec2( -0.24188840, 0.99706507 );\n\
			//		poissonDisk[13] = vec2( -0.81409955, 0.91437590 );\n\
			//		poissonDisk[14] = vec2( 0.19984126, 0.78641367 );\n\
			//		poissonDisk[15] = vec2( 0.14383161, -0.14100790 );\n\
			//		\n\
			//		for (int i=0; i<4; ++i) {\n\
			//			int index = int(mod(float(16.0*random(gl_FragCoord.xyy, i)),16.0));\n\
			//			float shadowDepth = unpack(texture2D(uDepthMap, depth.xy + poissonDisk[index]/1000.0));\n\
			//		\n\
			//			if (shadowDepth<depth.z - bias\n\
			//				&& (depth.x>0.0 && depth.x<1.0 && depth.y>0.0 && depth.y<1.0) && uReceiveShadows) {\n\
			//				visibility -= (uShadowStrength) * 0.25;\n\
			//			}\n\
			//		}\n\
			//		shadow = clamp(uShadowColor + visibility,0.0,1.0);\n\
			//	}\n\
			\n\
				gl_FragColor = shadow;\n\
			}\n\
		}\
		"
}


//// Lighting pass
vwgl.deferred_shaders.lighting_pass = {
	vertex:"\
		#ifdef GL_ES\n\
		precision highp float;\n\
		precision highp int;\n\
		#endif\n\
		attribute vec4 aVertexPos;\n\
		attribute vec2 aTexturePosition;\n\
		\n\
		varying vec2 vTexturePosition;\n\
		void main() {\n\
			gl_Position = aVertexPos;\n\
			vTexturePosition = aTexturePosition;\n\
		}\n\
		",

	fragment:"\
		#ifdef GL_ES\n\
		precision highp float;\n\
		precision highp int;\n\
		#endif\n\
		varying vec2 vTexturePosition;\n\
		\n\
		uniform sampler2D uDiffuseMap;\n\
		uniform sampler2D uNormalMap;\n\
		uniform sampler2D uSpecularMap;\n\
		uniform sampler2D uPositionMap;\n\
		uniform sampler2D uShadowMap;\n\
		\n\
		uniform float uBlurKernel[9];\n\
		uniform vec2 uShadowMapSize;\n\
		\n\
		\n\
		const int kDirectionalLightType = 4;\n\
		const int kSpotLightType = 1;\n\
		const int kPointLightType = 5;\n\
		const int kDisabledLightType = 10;\n\
		\n\
		// Light properties\n\
		uniform int uLightType;\n\
		uniform vec3 uLightPosition;\n\
		uniform vec3 uLightDirection;\n\
		uniform vec4 uLightAmbient;\n\
		uniform vec4 uLightDiffuse;\n\
		uniform vec4 uLightSpecular;\n\
		uniform vec3 uLightAttenuation;\n\
		uniform vec3 uSpotDirection;\n\
		uniform float uSpotExponent;\n\
		uniform float uSpotCutoff;\n\
		uniform float uCutoffDistance;\n\
		\n\
		// Other properties\n\
		uniform float uLightEmissionMult;\n\
		\n\
		vec3 getNormal() {\n\
			return normalize(texture2D(uNormalMap,vTexturePosition).rgb * 2.0 - 1.0);\n\
		}\n\
		\n\
		vec4 getLightingColor(	vec4 lightAmb, vec4 lightDiff, vec4 lightSpec,\n\
								vec3 lightDir, vec3 lightPos, vec3 vertexPos,\n\
								vec3 att, float spotCutoff, vec3 spotDirection, float spotExponent, int lightType,\n\
								vec4 matDiff, vec4 matSpec, vec3 normal, float shininess, float lightEmission) {\n\
 			if (lightType==kDisabledLightType) return vec4(0.0,0.0,0.0,1.0);\n\
			float attenuation;\n\
			float spotlight = 1.0;\n\
			if (lightType==kDirectionalLightType) {\n\
				attenuation = 1.0;\n\
			}\n\
			else {\n\
				vec3 posToLightSource = vec3(lightPos - vertexPos);\n\
				float distance = length(posToLightSource);\n\
				lightDir = normalize(posToLightSource);\n\
				attenuation = 1.0 / (att.x + att.y * distance + att.z * distance * distance);\n\
				if (lightType==kSpotLightType) {	\n\
					float clampedCos = max(0.0, dot(lightDir,spotDirection));\n\
					if (clampedCos<cos(radians(spotCutoff))) {\n\
						spotlight = 0.0;\n\
					}\n\
					else {\n\
						spotlight = pow(clampedCos, spotExponent);\n\
					}\n\
				}\n\
			}\n\
			vec3 color = (lightEmission + lightAmb.rgb) * matDiff.rgb;\n\
			vec3 diffuseWeight = max(0.0,dot(normal,lightDir)) * lightDiff.rgb * attenuation;\n\
			color += diffuseWeight* matDiff.rgb;\n\
			if (shininess>0.0) {\n\
				vec3 eyeDirection = normalize(-vertexPos);\n\
				vec3 reflectionDirection = normalize(reflect(-lightDir, normal));\n\
				float specularWeight = clamp(pow(max(dot(reflectionDirection, eyeDirection),0.0), shininess), 0.0, 1.0);\n\
				color += specularWeight * lightSpec.rgb * matSpec.rgb * attenuation;\n\
			}\n\
			color *= spotlight;\n\
			return clamp(vec4(color,1.0),0.0,1.0);\n\
		}\n\
		\n\
		vec4 applyConvolution(sampler2D texture, vec2 texCoord, vec2 texSize, float convMatrix[9], float radius) {\n\
		   vec2 onePixel = vec2(1.0, 1.0) / texSize * radius;\n\
		   vec4 colorSum =\n\
				 texture2D(texture, texCoord + onePixel * vec2(-1, -1)) * convMatrix[0] +\n\
				 texture2D(texture, texCoord + onePixel * vec2( 0, -1)) * convMatrix[1] +\n\
				 texture2D(texture, texCoord + onePixel * vec2( 1, -1)) * convMatrix[2] +\n\
				 texture2D(texture, texCoord + onePixel * vec2(-1,  0)) * convMatrix[3] +\n\
				 texture2D(texture, texCoord + onePixel * vec2( 0,  0)) * convMatrix[4] +\n\
				 texture2D(texture, texCoord + onePixel * vec2( 1,  0)) * convMatrix[5] +\n\
				 texture2D(texture, texCoord + onePixel * vec2(-1,  1)) * convMatrix[6] +\n\
				 texture2D(texture, texCoord + onePixel * vec2( 0,  1)) * convMatrix[7] +\n\
				 texture2D(texture, texCoord + onePixel * vec2( 1,  1)) * convMatrix[8] ;\n\
		   float kernelWeight =\n\
				 convMatrix[0] +\n\
				 convMatrix[1] +\n\
				 convMatrix[2] +\n\
				 convMatrix[3] +\n\
				 convMatrix[4] +\n\
				 convMatrix[5] +\n\
				 convMatrix[6] +\n\
				 convMatrix[7] +\n\
				 convMatrix[8] ;\n\
			\n\
			if (kernelWeight <= 0.0) {\n\
				kernelWeight = 1.0;\n\
			}\n\
			\n\
			return vec4((colorSum / kernelWeight).rgb, 1.0);\n\
		}\n\
		\n\
		void main() {\n\
			vec4 diffuseMatColor = texture2D(uDiffuseMap,vTexturePosition);\n\
			vec3 vertexPosition = texture2D(uPositionMap,vTexturePosition).xyz;\n\
			float lightEmission = texture2D(uNormalMap,vTexturePosition).w;\n\
			vec4 emissionColor = diffuseMatColor * lightEmission * uLightEmissionMult;\n\
			\n\
			if (diffuseMatColor.a==0.0) {\n\
				gl_FragColor = vec4(diffuseMatColor.rgb,1.0);\n\
			}\n\
			else if (uCutoffDistance==-1.0 || uCutoffDistance>distance(vertexPosition,uLightPosition)) {\n\
				vec3 normal = getNormal();\n\
				vec4 ambient = uLightAmbient;\n\
				vec4 diffuseColor = uLightDiffuse;\n\
				vec4 specColor = uLightSpecular;\n\
				vec4 specMatColor = texture2D(uSpecularMap,vTexturePosition);\n\
				float matShininess = specMatColor[3] * 255.0;\n\
				specMatColor.a = 1.0;\n\
				float test[9];\n\
				test[0] = 0.0; test[1] = 0.0; test[2] = 0.0;\n\
				test[3] = 0.0; test[4] = 0.0; test[5] = 0.0;\n\
				test[6] = 0.0; test[7] = 0.0; test[8] = 0.0;\n\
				vec4 shadowColor = applyConvolution(uShadowMap, vTexturePosition, uShadowMapSize, uBlurKernel, 1.0);\n\
				\n\
				vec4 light = getLightingColor(	ambient,diffuseColor,specColor,\n\
												uLightDirection,uLightPosition,vertexPosition,\n\
												uLightAttenuation,uSpotCutoff,uSpotDirection,uSpotExponent,uLightType,\n\
												diffuseMatColor,specMatColor,normal,matShininess,lightEmission);\n\
				gl_FragColor = (light * shadowColor * (1.0 - lightEmission)) + emissionColor;\n\
			}\n\
			else {\n\
				gl_FragColor = emissionColor;\n\
			}\n\
		}\n\
		"
}

vwgl.deferred_shaders.ssao_pass = {
	vertex:"#ifdef GL_ES\n\
			precision highp float;\n\
			precision highp int;\n\
			#endif\n\
			attribute vec4 aVertexPos;\n\
			attribute vec2 aTexturePosition;\n\
			\n\
			varying vec2 vTexturePosition;\n\
			void main() {\n\
				gl_Position = aVertexPos;\n\
				vTexturePosition = aTexturePosition;\n\
			}\n\
			",

	fragment:"\n\
			#ifdef GL_ES\n\
			precision highp float;\n\
			precision highp int;\n\
			#endif\n\
			varying vec2 vTexturePosition;\n\
			\n\
			uniform sampler2D uPositionMap;\n\
			uniform sampler2D uNormalMap;\n\
			\n\
			uniform mat4 uProjectionMat;\n\
			\n\
			const int MAX_KERN_OFFSETS = 64;\n\
			uniform sampler2D uSSAORandomMap;\n\
			uniform vec2 uRandomTexSize;\n\
			uniform vec2 uViewportSize;\n\
			uniform float uSSAOSampleRadius;\n\
			uniform vec3 uSSAOKernelOffsets[MAX_KERN_OFFSETS];\n\
			uniform int uSSAOKernelSize;\n\
			uniform vec4 uSSAOColor;\n\
			uniform bool uEnabled;\n\
			uniform float uMaxDistance;\n\
			\n\
			void main(void) {\n\
				if (!uEnabled) discard;\n\
				vec3 normal = texture2D(uNormalMap,vTexturePosition).xyz * 2.0 - 1.0;\n\
				vec4 vertexPos = texture2D(uPositionMap,vTexturePosition);\n\
				if (distance(vertexPos.xyz,vec3(0))>uMaxDistance || vertexPos.w==1.0) {\n\
					discard;\n\
				}\n\
				else {\n\
					vec2 noiseScale = vec2(uViewportSize.x/uRandomTexSize.x,uViewportSize.y/uRandomTexSize.y);\n\
					vec3 randomVector = texture2D(uSSAORandomMap,vTexturePosition * noiseScale).xyz * 2.0 - 1.0;\n\
					vec3 tangent = normalize(randomVector - normal * dot(randomVector, normal));\n\
					vec3 bitangent = cross(normal,tangent);\n\
					mat3 tbn = mat3(tangent, bitangent, normal);\n\
					\n\
					float occlusion = 0.0;\n\
					for (int i=0; i<MAX_KERN_OFFSETS; ++i) {\n\
						if (uSSAOKernelSize==i) break;\n\
						vec3 samplePos = tbn * uSSAOKernelOffsets[i];\n\
						samplePos = samplePos * uSSAOSampleRadius + vertexPos.xyz;\n\
						\n\
						vec4 offset = uProjectionMat * vec4(samplePos,1.0); // -w, w\n\
						offset.xyz /= offset.w;	// -1, 1\n\
						offset.xyz = offset.xyz * 0.5 + 0.5;	// 0, 1\n\
						\n\
						vec4 sampleRealPos = texture2D(uPositionMap, offset.xy);\n\
						\n\
						if (samplePos.z<sampleRealPos.z) {\n\
							float dist = distance(vertexPos.xyz,sampleRealPos.xyz);\n\
							occlusion += dist<uSSAOSampleRadius ? 1.0:0.0;\n\
						}\n\
					}\n\
					occlusion = 1.0 - (occlusion / float(uSSAOKernelSize));\n\
					\n\
					gl_FragColor = clamp(vec4(occlusion,occlusion,occlusion,1.0) + uSSAOColor,0.0,1.0);\n\
				}\n\
			}\n\
			"
};
